$('body').on('click', '.btn_trash', function(event) {
    $id = $(this).data('id');
    $url = $(this).data('url');
    event.preventDefault();
    if(window.confirm('Trash record: are you sure?')) {
        $.ajax({
            url: $url,
            type: 'POST',
            data: {id: $id, _token: csrf_token},
            beforeSend:function(){
                //loading
            }
        }).done(function(response) {
            var res = JSON.parse(response);
            if(res.status=='success') {
                $('#tr_'+$id).hide('slow');
                var $msg = 'Record has been trashed successfully';
                
                notify($msg, 'alert');
            } else {
                notify('Some issues display result message', 'error');
            }
        }).fail(function() {
            alert('Error Restoring or Trashing record');
        }).always(function() {
        });
    }
});