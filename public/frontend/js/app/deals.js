$(document).ready(function() {
	$('.offer_for').on('click', function(event) {
		//event.preventDefault();
		$('.product_select').hide();
		$('.selectproduct .popup').hide();
		if($(this).val()=='existing') {
			$('.product_select').show();
		} else {
			$('.selectproduct .popup').show();
		}
	});
	$('.offer_type').trigger('change');

	$('.request_for').on('click', function(event) {
		//event.preventDefault();
		$('.order_quantity').hide();
		$('.durations').hide();
		$('.port').hide();
		$('.annual_purchase').hide();
		$('.unit_price').hide();

		if($(this).val()=='service') {
			$('.durations').show();
		} else {
			$('.order_quantity').show();
			$('.port').show();
			$('.annual_purchase').show();
			$('.unit_price').show();
		}
	});
	$('.offer_type').trigger('change');
	$('.specific_countries').hide();
});

$('body').on('change', '.offer_type', function(event) {
	event.preventDefault();
	var val = $(this).val();

	//Items to show by default
	$('.available_units').show();
	$('.fob_price').show();
	$('.port').show();
	$('.shipping_terms').show();
	$('.payment_terms').show();
	$('.brands').show();
	$('.min_quantity').show();

	if(val=='clearance') {
		$('.available_units').slideDown();
		$('.service_product_name').hide();
	} else if(val=='service') {
		$('.available_units').hide();
		$('.service_product_name').slideDown();

		//Items to hide
		$('.available_units').hide();
		$('.fob_price').hide();
		$('.port').hide();
		$('.shipping_terms').hide();
		$('.payment_types').hide();
		$('.brands').hide();
		$('.min_quantity').hide();

	} else {
		$('.available_units').hide();
		$('.service_product_name').hide();
	}
});

$(function() {
    $(".expired_datepicker" ).datepicker({ 
    	minDate: 0, maxDate: "+6M", 
    	dateFormat: "yy-mm-dd" 
   	});
});

$('body').on('blur', '.prod_cat_search', function(event) {
	return false;
	var $q = $(this).val();
	event.preventDefault();
	$.ajax({
		url: catSearchURL,
		type: 'GET',
		data:{q:$q},
	}).done(function(results) {
		$('.cat_search_results').html(results);
	}).fail(function(error) {
		alert('error');
	}).always(function() {
		$('.cat_cord').hide();
		$('.select_category').slideDown('slow');
	});
});
$('body').on('click', '.cat_set_fsearch', function(event) {
	event.preventDefault();
	$('#selected_category_id').val($(this).data('cat_id'));
	$('.select_category').slideUp('fast');
	var $cat_path = $(this).find('.cpath').html();
	$('.selected_cat_path').html($cat_path);
});


$('body').on('change', '.alert_from', function(event) {
	event.preventDefault();
	var val = $(this).val();
	$('.specific_countries').hide();

	if(val=='Specific') {
		$('.specific_countries').show();
	} else {
		$('.specific_countries').hide();
	}
});