$('body').on('click', '.submit_form', function(event) {
    //event.preventDefault();
    /*if($('#selected_category_id').val()=='0') {
        alert('Please select category'); $('.toggle_selectcat').focus(); 
        return false;
    }*/
    //$(this).button('loading');
});
if($('.tags').length>0) {
    $('.tags').tagsInput({
        'width':'95%',
        'height':'60px',
        'defaultText':'Enter keywords',
    });    
}
$(document).ready(function() {
    $('body').on('click','.btn_quickAddGroup', function(event) {
        event.preventDefault();
        $.ajax({
            url: $addGroupRoute,
            data: $("#popup_form").serializeArray(),
            type:'POST',
            dataType:'json',
        }).done(function(output) {
            if (output.status == 'success') {
                var row = output.data;
                $("#group_id").append('<option value="'+row.id+'" selected="selected">'+row.name+'</option>');
                $.magnificPopup.close();
            } else {
                alert('Some errors happened');
            }
        }).fail(function() {
            alert('error');
        }).always(function() {
            //$(".btnTarget").button('reset');
        });
    });
});

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $('body').on('click', '.add_field_button', function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div style="margin-top:10px;">'+
                '<input type="text" name="more_details[attr][]" class="form-control" placeholder="Attribute" /> '+
                '<input type="text" name="more_details[value][]" class="form-control" placeholder="Value" /> '+
                '<a class="btn btn-success add_field_button">+</a> <a href="#" class="btn btn-danger remove_field">-</a></div>'); //add input box
        } else {
            alert('Sorry, you cannot add more than '+max_fields+' inputs');
        }
    });
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    //Add fields for Additional Images
    var max_img_fields = 5; //maximum input boxes allowed
    var z = 1; //initlal text box count
    $('body').on('click', '.add_imgfile_button', function(e){ //on add input button click
        e.preventDefault();
        if(z < max_img_fields){ //max input box allowed
            z++; //text box increment
            $('.input_imgfiles_wrap').append('<div class="media">'+
                '<a class="pull-left" href="#"><img src="'+$no_thumb_url+'" class="media-object img_preview imgfield'+z+'" /></a>'+
                '<a class="pull-right btn btn-success add_imgfile_button">+</a> <a href="#" class="btn btn-danger pull-right remove_imgfield">-</a>'+
                '<div class="media-body"><input type="file" name="add_images[]" class="form-control img_browse" data-fid="'+z+'"></div></div>');
        } else {
            alert('Sorry, you cannot add more than '+max_img_fields+' images');
        }
    });
    $('.input_imgfiles_wrap').on("click",".remove_imgfield", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); z--;
    });
});

//Trash a Product
$('body').on('click', '.btn_del', function(event) {
    $id = $(this).data('id');
    $url = $(this).data('url');
    event.preventDefault();
    if(window.confirm('Trash record: are you sure?')) {
        $.ajax({
            url: $url,
            type: 'POST',
            data: {id: $id, _token: csrf_token},
            beforeSend:function(){
                //loading
            }
        }).done(function(response) {
            var res = JSON.parse(response);
            if(res.status=='success') {
                $('#tr_'+$id).hide('slow');
                var $msg = 'Record has been trashed successfully';
                
                notify($msg, 'alert');
            } else {
                notify('Some issues display result message', 'error');
            }
        }).fail(function() {
            alert('Error Restoring or Trashing record');
        }).always(function() {
        });
    }
});
//Trash a Product
$('body').on('click', '.btn_restore', function(event) {
    $id = $(this).data('id');
    $url = $(this).data('url');
    event.preventDefault();
    if(window.confirm('Restore record back: are you sure?')) {
        $.ajax({
            url: $url,
            type: 'POST',
            data: {id: $id, _token: csrf_token},
            beforeSend:function(){
                //loading
            }
        }).done(function(response) {
            var res = JSON.parse(response);
            if(res.status=='success') {
                $('#tr_'+$id).hide('slow');
                notify('Record has been restored successfully');
            } else {
                $msg = 'Some issues display result message';
                notify($msg, 'alert');
            }
        }).fail(function() {
            alert('Error restoring product');
        }).always(function() {
        });
    }
});

$(document).ready(function() {
    if($('.other_paytype').val()!='') {
        $('#other_payment_type').show();
    } else {
        $('#other_payment_type').hide();
    }
});
$('body').on('change', '.pay_types', function(event) {
    event.preventDefault();
    if($(this).val()=='Other Payment type') {
        $('#other_payment_type').toggle('400');
    }
});