$('body').on('click', '.btn_starred', function(event) {
    event.preventDefault();
    
    $id = $(this).data('id');
    $field = $(this).data('field');
    $model = $(this).data('model');
    $element = $('.elem'+$field+$id);
    $.ajax({
        url: $switch_bool_url,
        type: 'GET',
        data: {id: $id, model: $model, field: $field},
        beforeSend:function() {
            $element.html('loading');
        }
    }).done(function(response) {
        $res = $.parseJSON(response);
        $elem_wrapper = $element.parent('span');
        $element.remove();
        if($res.status==1) {
            var $msg = 'Message starred successfully';
            if($res.newval==1) {
                $color = 'text-warning';
                $icon = 'star';
            } else {
                $color = '';
                $icon = 'star-o';
                $msg = 'Message unstarred successfully';
            }
            $elem_wrapper.html('<span><a href="#" class="'+$color+' btn_starred elem'+$field+$id+'" data-id="'+$id+'" data-model="'+$model+'" data-field="'+$field+'"><i class="fa fa-'+$icon+'"></i></a></span>');
            notify($msg);            
        } else {
            alert('خطا حدث في تغيير التفعيل');
        }
    }).fail(function() {
        console.log("خطا حدث في تغيير التفعيل");
    }).always(function() {
    });
});

//Trash a Product
$('body').on('click', '.btn_spam', function(event) {
    $id = $(this).data('id');
    $url = $(this).data('url');
    event.preventDefault();
    $.ajax({
        url: $url,
        type: 'POST',
        data: {id: $id, _token: csrf_token},
        beforeSend:function(){
            //loading
        }
    }).done(function(response) {
        var res = JSON.parse(response);
        if(res.status=='success') {
            $('#tr_'+$id).hide('slow');
            var $msg = 'Spam';
            if(res.data.spammed==0) {
              $msg = 'Not Spam';
            }
            notify('Message marked as '+$msg);
        } else {
          notify('Some issues display result message', 'error');
        }
    }).fail(function() {
        alert('Error');
    }).always(function() {
    });
});

$('body').on('click', '.btn_reply', function(event) {
    event.preventDefault();
    $btn = $(this);
    $('.reply_box').removeClass('hidden');
    $('.reply_box').slideDown('', function() {
        $btn.hide();
    });
});

$(document).on('click', '.btn_perma_del', function(event) {
    event.preventDefault();
    var $msg_id = $(this).data('id');
    var $url = $(this).data('url');
    if(window.confirm('Delete message permanent: Are you sure?')) {
        $.ajax({
            url: $url,
            type: 'POST',
            dataType: 'json',
            data: {id: $msg_id, _token: csrf_token},
            beforeSend: function() {

            }
        }).done(function(response) {
            window.location.reload();
        }).fail(function() {
            alert('Error occured');
        }).always(function() {
            
        });
    }
});