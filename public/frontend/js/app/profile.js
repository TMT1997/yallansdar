var max_fields      = 10;
var wrapper         = $(".input_fields_wrap");
var add_button      = $(".add_field_button");
var x = 1;

var template1 = '<div style="margin-top:10px;">'+
            '<input type="text" name="more_details[attr][]" class="form-control" placeholder="Attribute" /> '+
            '<input type="text" name="more_details[value][]" class="form-control" placeholder="Value" /> '+
            '<a class="btn btn-success add_field_button">+</a> <a href="#" class="btn btn-danger remove_field">-</a></div>';

$('body').on('click', '.add_field_button', function(e){
    e.preventDefault();
    if(x < max_fields){
        x++;
        $(wrapper).append(template1); //add input box
    } else {
        alert('Sorry, you cannot add more than '+max_fields+' inputs');
    }
});
$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
});