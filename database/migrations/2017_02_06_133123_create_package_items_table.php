<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePackageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_items', function(Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('package_id');
            $table->string('section');
            $table->string('name');
            $table->string('description');
            $table->string('value');
            $table->string('value_hint');
            $table->integer('sort_order');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_items');
    }
}
