<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('contact_person', 100);
            $table->string('contact_department', 100);
            $table->string('company_name', 100);
            $table->string('logo', 100);
            $table->string('address', 255);
            $table->string('zip', 30);
            $table->string('phone', 30);
            $table->string('mobile', 30);
            $table->string('fax', 30);
            $table->string('city', 100);
            $table->string('state', 100);
            $table->mediumInteger('country_id');
            $table->mediumInteger('business_type');
            $table->string('establish_year', 4);
            $table->string('turnover', 255);
            $table->string('main_products', 255);
            $table->boolean('active')->default(1);
            $table->boolean('verified')->default(0);
            $table->boolean('premium')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_company');
    }
}
