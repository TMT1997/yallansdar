<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name', 180);
            $table->string('name_en', 180);
            $table->string('image', 180);
            $table->string('price', 20);
            $table->integer('min_order_quantity');
            $table->string('payment_methods', 40);
            $table->text('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
