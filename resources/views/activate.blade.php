<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

    <div style="width:70%; margin:20px auto; text-align: center;">
        <a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
    </div>

    <div style="width: 70%;
    color: white;
    background: #00509f;
    margin: 20px auto;
    padding: 20px;
    text-align: center;
    border: 10px solid white;
    border-radius: 10px;
    font-family: sans-serif, arial;
    font-size: 20px;">

        أهلا بك, <strong>{{$data['name']}}</strong>
        <br><br>

        شكرا على تسجيلك بموقع يلا نصدر.
        <br><br>

        @if(isset($data['password']))
            كلمة المرور الخاصة بحسابك هي: {{$data['password']}} <br>
            <span style="font-style: italic;">ملحوظة: يفضل تغيير كلمة المرور بعد تفعيل حسابك</span><br>
        @endif

        لتفعيل حسابك قم بالضغط على الرابط أدناه
        <br>

        {{-- <p style="font-size:14px; color: #0069FF; text-align: center; direction: ltr;">
            <a href="{!!$data['activation_url']!!}" target="_blank">{!!$data['activation_url']!!}</a>
        </p> --}}
        <p style="direction: ltr; text-align: center;">
        <table cellspacing="0" cellpadding="0" style="margin:0 auto; "><tr>
                <td align="center" width="300" height="40" bgcolor="#d62828" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block;">
                    <a href="{!!$data['activation_url']!!}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
                        <span style="color: #ffffff;">@lang('front.verify_email')</span>
                    </a>
                </td></tr></table>
        </p>

        <br><br>

        <strong>شكرا  </strong><br>
        فريق يلا نصدر <br>
        <a href="{{url('/')}}">www.yallansadar.com</a>
    </div>

    <div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
        <div style="margin:0 auto; width:70%;">

            تم إرسال هذه الرسالة إلى البريد الإلكتروني <a href="mailto:{{$data['email']}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$data['email']}}</a>.
            <br>
            إذا اردت عدم استقبال رسائل أخرى في المستقبل من الموقع من فضلك اضغط <a href="{{url('registration/unsubscribe?email='.$data['email'].'&idn='.$data['indentifier'])}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">إلغاء خدمة الرسائل</a>.

            <br>

            &copy; 2017 - جميع الحقوق محفوظة لدى موقع يلانصدر
        </div>
    </div>
</div>