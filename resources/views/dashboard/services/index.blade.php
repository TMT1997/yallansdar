@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-4">
                    {{trans('dash.services')}}
                </div>
                <div class="col-md-8 text-right">
                    <a href="{{ url('/dashboard/services/create') }}" class="btn btn-primary">{{trans('dash.add_new_service')}}</a>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div role="tabpanel">
                <ul class="nav nav-tabs product_status" role="tablist">
                    @foreach(App\Product::$product_status as $state_id=>$status)
                        <?php $counter = $services->filter(function($row) use($state_id){
                                return $row->status==$state_id;
                            })->count(); ?>
                        @if($status=='All')
                            <?php $counter = $services->count(); ?>
                        @endif
                        <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('products.statuses')[$state_id]}} ({{$counter}})</a></li>
                    @endforeach
                </ul>
            
                <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
            </div>            
        </div>
    </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/services.js')!!}
    <script type="text/javascript">
        $(document).on('click', '.product_status a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var status = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/services/grid')}}?status='+status,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('{{trans('services.loading')}}');
                }
            }).done(function(response) {
                $('.data_grid').html(response);
            }).fail(function() {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying services grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
        $(document).ready(function() {
            $('.product_status a[data-toggle="tab"]:first').trigger('click');
        });
    </script>
@endsection