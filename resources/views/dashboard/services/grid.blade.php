<div class="label label-primary">{{trans('dash.no_services')}} {{$services->count()}}</div>
<table class="table table-bordered table-striped table-hover dataTable">
    <thead>
        <tr>
            <th>{{trans('dash.id')}}</th>
            <th>{{trans('dash.active')}}</th>
            <th>{{trans('dash.name')}}</th>
            <th>{{trans('dash.image')}}</th>
            <th>{{trans('dash.status')}}</th>
            <th>{{trans('dash.last_update')}}</th>
            <th>{{trans('dash.actions')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($services as $item)
        <?php
            $lang_data=$item->lang->filter(function($row) use ($lng_id) { return $row->language_id==$lng_id; })->first();
        ?>
        <tr id="tr_{{$item->id}}">
            <td>{{ $item->id }}</td>
            <td>{!!App\Helper::sw($item,'active','Service')!!}</td>
            <td>{{ (isset($lang_data))?$lang_data->name:'-' }}</td>
            <td>{!!App\Helper::image($item->image, ['style'=>'max-width:90px;'])!!}</td>
            <td>{{$item->status}}</td>
            <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
            <td>
                <a href="{{url('dashboard/services/'.$item->id)}}" class="btn btn-success btn-sm" title="{{trans('dash.view')}}"><span class="glyphicon glyphicon-eye-open"/></a>
                <?php $dis = ($item->status!=2)?'disabled="disabled"':''; ?>
                <a href="{{route('dashboard.services.edit',$item->id)}}" class="btn btn-primary btn-sm" title="{{trans('dash.edit')}}"><span class="glyphicon glyphicon-pencil"/></a>

                @if($item->status!=4)
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/services/trash')}}' class="btn btn-danger btn-sm btn_del" title="{{trans('dash.trash')}}"><i class="fa fa-trash"></i></a>
                @else
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/services/restore')}}' class="btn btn-warning btn-sm btn_restore" title="{{trans('dash.restore')}}"><i class="fa fa-undo"></i></a>
                    {!! Form::open([
                        'method'=>'DELETE','url'=>['dashboard/services', $item->id],'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash" title="'.trans("dash.delete").'"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Service Permenantly',
                            'onclick'=>'return confirm("'.trans("dash.confirm_delete").'")'
                    )) !!}
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($services->count()==0)
    {{trans('dash.no_results_found')}}
@endif