@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.add_new_service')}}</legend>

    <div class="help-block">{{trans('dash.service_desc')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/services','class'=>'form-horizontal','id'=>'service_form','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.services.form')
    {!! Form::close() !!}

@endsection