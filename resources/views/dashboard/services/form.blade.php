<?php
use App\Deals\SellOffer;
use App\Helper;
?>
<div class="container">
  <div class="row">
    <div class="col-md-6 name {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', trans('dash.service_name'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6 name {{ $errors->has('english_name') ? 'has-error' : ''}}">
        {!! Form::label('english_name', trans(''), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('english_name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
  </div>
</div>

    <!-- <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
        {!! Form::label('category_id', trans('dash.category'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div> -->
    {{-- @include('dashboard.categories.select_category', ['selected_category_id'=>@$service->category_id]) --}}

    <div class="form-group">
        {!! Form::label('description', trans('deals.description'), ['class'=>'col-md-1 control-label required']) !!}
        <div class="col-md-12">
            <div role="tabpanel">
                <?php $count = 0;
$count2 = 0;?>
                <!-- <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count == 0) ? 'class="active"' : '';?>
                        <li {!!$active!!}><a href="#sec{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++?>
                    @endforeach
                </ul> -->
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <div class="col-md-6">
                    <?php $active = ($count2 == 0) ? 'active' : '';?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        @if($lang->iso2=="en")
                        {!! Form::textarea('lang['.$lang->id.'][description]', null, ['class'=>'form-control check_english','rows'=>3, 'placeholder'=>Lang::get('dash.service_desc_info',[],$lang->iso2)]) !!}
                        @else
                        {!! Form::textarea('lang['.$lang->id.'][description]', null, ['class'=>'form-control','rows'=>3, 'placeholder'=>Lang::get('dash.service_desc_info',[],$lang->iso2)]) !!}
                        @endif
                       </div>
                    </div>
                    <?php $count2++?>
                </div>
            </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <?php $field = 'image';?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.image'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="media">
                <?php $filepath = (isset($service)) ? 'uploads/' . $service->$field : 'frontend/images/no_thumb.jpg';?>
                <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1'])!!}</a>
                <div class="media-body">
                    <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
                    <div class="help-block">{{trans('dash.imgrules')}}</div>
                </div>
            </div>
            {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('details', trans('deals.details'), ['class' => 'col-md-1 control-label']) !!}
        <div class="col-md-12">
            <div role="tabpanel">
                <?php $count = 0;
$count2 = 1;?>
                <!--<ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count == 0) ? 'class="active"' : '';?>
                        <li {!!$active!!}><a href="#det{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++?>
                    @endforeach
                </ul>-->
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <div class="col-md-6">
                    <?php $active = ($count2 == 1) ? 'active' : '';?>
                    <div class="tab-pane fade in {!!$active!!}" id="det{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        {!! Form::textarea('lang['.$lang->id.'][details]', null, ['class'=>'form-control','rows'=>3, 'id'=>'editor'.$count2]) !!}
                        </div></div>
                    </div>
                    <?php $count2++?>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('price_min') ? 'has-error' : ''}}">
        {!! Form::label('price_min', trans('dash.price_range'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::number('price_min', null, ['class' => 'form-control', 'placeholder'=>trans('dash.min')]) !!}
            {!! Form::number('price_max', null, ['class' => 'form-control', 'placeholder'=>trans('dash.max')]) !!}
            {!! $errors->first('price_min', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <?php $field = 'document';?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.document_if_any'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!!Form::file($field,['class'=>'form-control'])!!}
            {!! $errors->first($field, '<p class="help-block">:message</p>') !!}

            @if(isset($service) && !empty($service->document))
                <a class="pull-right btn btn-info" href="{{url('uploads/'.$service->document)}}">{{trans('dash.preview')}}</a>
            @endif
            <div class="help-block">{{trans('dash.imgdoc')}}</div>
        </div>
    </div>

    {!!Form::hidden('locations', '')!!}
    <div class="form-group {{ $errors->has('locations') ? 'has-error' : ''}}">
        {!! Form::label('locations', trans('dash.service_offered_location'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('locations[]',$countries,null,['class'=>'form-control select2c required','multiple'=>'multiple']) !!}
            {!!Helper::popover(trans('dash.service_offered_location'),trans('dash.leave_empty_c'))!!}
            {!! $errors->first('locations', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.valid_till'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class' => 'form-control expired_datepicker']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            {!! $errors->first('expired_at', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@if(!isset($show_submit))
<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText :trans('dash.add_service'), ['class'=>'btn btn-lg btn-primary submit_form']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('dash.cancel')}}</a>
    </div>
</div>
@endif

@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
@include('dashboard.layouts.popup');
<script type="text/javascript">
    @if(isset($category_tree))
        $(document).ready(function() {
            $(".select_category").show();
            $('.cat_selector').val('{{$category_tree[0]->id}}');
            $('.cat_selector').trigger('change');
            $('.sub').attr('data-id', {{$category_tree[1]->id}});
            @if(!empty($category_tree[2]->id))
                $('.subsub').attr('data-id', {{$category_tree[2]->id}});
            @endif
            $('#selected_category_id').val('{{$service->category_id}}');
        });
    @endif
    $('#service_form').validate({
        rules: {
            "selected_category_id":"required",
            'name':'required',
            "lang[1][description]": "required",
            "lang[2][description]": "required",
            "lang[1][details]": "required",
            "lang[2][details]": "required",
        },
        messages: {
            "selected_category_id":"اختر التصنيف",
            'name':'من فضلك ادخل اسم الخدمة',
            "lang[1][description]": "من فضلك ادخل وصف الخدمة",
            "lang[2][description]": "من فضلك ادخل وصف الخدمة",
            "lang[1][details]": "من فضلك ادخل تفاصيل الخدمة",
            "lang[2][details]": "من فضلك ادخل تفاصيل الخدمة",
        }
    });
</script>
@endsection
