@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('services.edit_service')}}: {{ $service->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($service, [
        'method' => 'PATCH',
        'url' => ['/dashboard/services', $service->id],
        'class' => 'form-horizontal', 'id'=>'service_form',
        'files' => true
    ]) !!}
    @include ('dashboard.services.form', ['submitButtonText' => trans('dash.update')])
    {!! Form::close() !!}
@endsection