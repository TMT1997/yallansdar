<?php
    use App\Category;
    $category_tree = null;
    if(isset($selected_category_id)) {
        $category_tree = Category::get_parents($selected_category_id);
        if($category_tree) {
            $category_tree = array_reverse($category_tree);
        }
    }

    $parent_catfield = 'category_id';
    if(isset($keep_parent_id) && $keep_parent_id==true) {
        $parent_catfield = 'parent_cat_id';
    }
?>
<div class="selected_cat_path"></div>
<div class="select_category">
    <div class="well">
        <div class="cat_select_plugin">
            {!!Form::hidden('selected_category_id', 0, ['id'=>'selected_category_id'])!!}
            <div class="form-group">
                <div class="col-md-12 form-inline">
                    <input type="text" class="form-control cat_q" placeholder="{{trans('products.select_cat')}}" />
                    <a href="javascript:void(0)" class="btn btn-info btn_cat_search">{{trans('products.search')}}</a>
                    {{trans('products.or')}}
                    <a href="javascript:void(0)" class="btn btn-primary btn_cat_browse">{{trans('products.browse_cat')}}</a>
                </div>
            </div>
            <div class="form-group cat_cord"> 
                <div class="col-md-4 parent">
                    <select required="required" name="{{$parent_catfield}}" class="form-control cat_selector" data-parent="1" size="7">
                        @if(isset($categories) && $categories->count()>0)
                        @foreach($categories as $category)
                            <?php $selected=(isset($parent_cat_id) && $category['id']==$parent_cat_id)?'selected="selected"':''; ?>
                            <option value="{{$category->id}}" {{$selected}}>{{$category->$c_name}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-4 sub" data-id=""></div>
                <div class="col-md-4 subsub" data-id=""></div>
            </div>
            <div class="form-group cat_search_results"></div>
            <div class="well well-sm selected_cat"></div>
        </div>
        <div class="clearfix text-center">
            <a href="#" class="btn btn-primary selectcat_ok">{{trans('products.ok')}}</a>
            <a href="#" class="btn btn-default selectcat_cancel">{{trans('products.cancel')}}</a>
        </div>
    </div>
</div>

@section('script2')
<script type="text/javascript">
    $(document).ready(function() {            
        $(".select_category").hide();
        $(document).on('click', '.toggle_selectcat', function(event) {
            event.preventDefault();
            $(".select_category").slideToggle('400');
        });
        $(document).on('click', '.selectcat_cancel', function(event) {
            event.preventDefault();
            $('.select_category').slideUp('fast', function() {
            });
        });
        $(document).on('click', '.selectcat_ok', function(event) {
            event.preventDefault();
            $('.selected_cat_path').html($('.selected_cat').html());
            $('.select_category').slideUp('fast', function() {
            });
        });
        $(document).on('click', '.btn_cat_browse', function(event) {
            event.preventDefault();
            $('.cat_search_results').html('');
            $('.cat_cord').show();
            $('.toggle_selectcat').trigger('click');
            $('.select_category').show('fast', function() {
            });
        });
        $(document).on('click', '.toggle_selectcat ', function(event) {
            event.preventDefault();
            if($('#selected_category_id').val()!=0) {
                return false; //if already any category selected then don't reload categories
            }
            $('.sub').html(''); $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/0',
                type: 'GET',
                beforeSend:function(){
                    $('.cat_select_plugin .parent').html('{{trans('dash.loading_categories')}}');
                }
            }).done(function(response) {
                //$('.selected_cat').html(getSelectedText('.cat_selector', $cat_id));
                if(response!=='') {
                    $('.parent').html('<select required="required" name="category_id" class="form-control cat_selector" data-parent="1" size="7">'+response+'</select>');
                } else {
                    $('.parent').html('nothing to preview');
                }
            }).fail(function() {
                alert('Error while process categories');
            }).always(function() {
            });             
        });
        $(document).on('change', '.cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $('.sub').html(''); $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/'+ $cat_id,
                type: 'GET',
            }).done(function(response) {
                $('.selected_cat').html(getSelectedText('.cat_selector', $cat_id));

                if(response!=='') {
                    $('.sub').append('<select name="category_id" size="7" class="form-control sub_cat_selector" style="display:inline-block;">'+response+'</select>');
                } else {
                    $('.selected_cat').append(' ... DONE');
                    $("#selected_category_id").val($cat_id);
                }
            }).fail(function() {
                alert('Error while process sub categories');
            }).always(function() {
                //check if sub has data-id, this is used in the Edit Product
                if($('.sub').data('id')!='') {
                    $('.sub_cat_selector').val($('.sub').data('id'));
                    $('.sub_cat_selector').trigger('change');
                }
            });
        });
        $(document).on('change', '.sub_cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $parent_id = $('.cat_selector').val();

            $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/'+ $cat_id,
                type: 'GET',
            }).done(function(response) {

                $('.selected_cat').html(getSelectedText('.cat_selector', $parent_id));
                $('.selected_cat').append(' >> ' + getSelectedText('.sub_cat_selector', $cat_id));

                if(response!=='') {
                    $('.subsub').append('<select name="category_id" size="7" class="form-control subsub_cat_selector" style="display:inline-block;">'+response+'</select>');
                } else {
                    $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                    $("#selected_category_id").val($cat_id);
                }
            }).fail(function() {
                alert('Error while process sub of sub categories');
            }).always(function() {
                //check if subsub has data-id, this is used in the Edit Product
                if($('.subsub').data('id')!='') {
                    $('.subsub_cat_selector').val($('.subsub').data('id'));

                    $('.selected_cat').append(' >> ' + getSelectedText('.subsub_cat_selector', $('.subsub').data('id')));
                    $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                }
            });
        });
        $(document).on('change', '.subsub_cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $parent_cat_id = $('.cat_selector').val();
            $sub_cat_id = $('.sub_cat_selector').val();

            $('.selected_cat').html(getSelectedText('.cat_selector', $parent_cat_id));
            $('.selected_cat').append(' >> ' + getSelectedText('.sub_cat_selector', $sub_cat_id));
            $('.selected_cat').append(' >> ' + getSelectedText('.subsub_cat_selector', $cat_id));
            $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');

            $("#selected_category_id").val($cat_id);
        });

        $(".cat_select").on('change', function(event) {
            event.preventDefault();
            $(".selectcatForm").submit();
        });
    });
    $(document).on('click', '.btn_cat_search', function(event) {
        event.preventDefault();
        $('.cat_set_fsearch').trigger('blur');
    });
    $(document).on('click', '.cat_set_fsearch', function(event) {
        event.preventDefault();
        var $cat_id = $(this).data('id');
        $("#selected_category_id").val($cat_id);
        $('.selected_cat_path').addClass('well well-sm');
    });
    $(document).on("keypress", '.cat_q', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            $('.btn_cat_search').trigger('click');
            return false;
        }
    });
    function getSelectedText($obj, $val) {
        return $($obj + " option[value='"+$val+"']").text();
    }

    $(document).ready(function() {
        $(".select_category").show();
        @if(isset($category_tree) && !empty($category_tree))
            $('.cat_selector').val('{{@$category_tree[0]->id}}');
            $('.cat_selector').trigger('change');
            @if(isset($category_tree[1]->id))
                $('.sub').attr('data-id', {{$category_tree[1]->id}});
            @endif
            @if(isset($category_tree[2]) && !empty($category_tree[2]->id))
                $('.subsub').attr('data-id', {{@$category_tree[2]->id}});
                $('.subsub_cat_selector').trigger('change');
            @endif
            $('#selected_category_id').val('{{@$selected_category_id}}');
        @endif 
    });

    //Search in categories
    $(document).on('click', '.btn_cat_search', function(event) {
        event.preventDefault();
        var $q = $('.cat_q ').val();
        $.ajax({
            url: catSearchURL,
            type: 'GET',
            data:{q:$q},
            beforeSend:function() {
                var $loader_new = '<div class="text-center"><i class="fa fa-spinner fa-spin fa-fw"></i><span class="">@lang('front.loading')</span></div>';
                $('.cat_search_results').html($loader_new);
            }
        }).done(function(results) {
            if(results=='') {
                $('.cat_search_results').html('لا يوجد نتائج لبحثك');
            } else {
                $('.cat_search_results').html(results);                
            }
        }).fail(function(error) {
            alert('Error while searching');
        }).always(function() {
            $('.cat_cord').hide();
            $('.select_category').slideDown('slow');
        });
    });
</script>
@endsection