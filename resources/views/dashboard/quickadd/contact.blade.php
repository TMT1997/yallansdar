<div class="white-popup-block">
<legend>{{trans('dash.add_new_contact')}}</legend>
{!!Form::open(['id'=>'popup_form', 'class'=>'form-horizontal'])!!}
	<div class="form-group">
		{!!Form::label('first_name', trans('dash.contact_name'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9">
			{!!Form::text('first_name',null,['class'=>'form-control','required'=>'required','placeholder'=>trans('dash.name')])!!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('email', trans('dash.email'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9">
			{!!Form::text('email', null, ['class'=>'form-control', 'required'=>'required','placeholder'=>trans('dash.email')])!!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('phone', trans('dash.phone'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9 form-inline">
			{!!Form::text('dailing_code', null, ['class'=>'form-control','placeholder'=>trans('dash.code'),'size'=>4])!!}
			{!!Form::text('phone', null, ['class'=>'form-control','placeholder'=>trans('dash.phone')])!!}
		</div>
	</div>
	<div class="form-group">
		{!!Form::label('mobile', trans('dash.mobile'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9 form-inline">
			{!!Form::text('dailing_code_m', null, ['class'=>'form-control','placeholder'=>trans('dash.code'),'size'=>4])!!}
			{!!Form::text('mobile', null, ['class'=>'form-control','placeholder'=>trans('dash.mobile')])!!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-3"></div>
		<div class="col-md-9"> 
			{!!Form::submit(trans('dash.add_contact'), ['class'=>'btn btn-success btn_quickAddContact'])!!}
		</div>
	</div>
{!!Form::close()!!}
</div>