<div class="white-popup-block">
<legend>{{trans('dash.add_new_mediaalbum')}}</legend>
{!!Form::open(['id'=>'popup_form', 'class'=>'form-horizontal'])!!}
	<div class="form-group">
		{!!Form::label('name', trans('dash.album_name'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9">
			{!!Form::text('name', null, ['class'=>'form-control', 'required'=>'required'])!!}
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-9 col-md-offset-3"> 
			{!!Form::submit(trans('dash.add_album'), ['class'=>'btn btn-success btn_quickAddAlbum'])!!}
		</div>
	</div>
{!!Form::close()!!}
</div>