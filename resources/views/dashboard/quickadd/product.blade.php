<div class="white-popup-block">
<legend>{{trans('dash.add_new_product')}}</legend>
{!!Form::open(['id'=>'popup_form', 'class'=>'form-horizontal', 'url'=>'dashboard/quickadd/product', 'method'=>'POST', 'files'=>true])!!}
{!!Form::hidden('ref', 'sell_offer')!!}
	<div role="tabpanel">
        <?php $count=0; ?>
        <ul class="nav nav-tabs" role="tablist">
            @foreach($languages as $lang)
                <?php $active = ($count==0) ? 'class="active"':''; ?>
                <li {!!$active!!}><a href="#addprod{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                <?php $count++ ?>
            @endforeach
        </ul>
        <?php $count=0; ?>
        <div class="tab-content">
        @foreach($languages as $lang)
            <?php $active = ($count==0) ? 'active':''; ?>
            <div role="tabpanel" class="tab-pane fade in {!!$active!!}" id="addprod{{$lang->id}}">
                <div class="panel panel-default" style="border-top: none;">
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', Lang::get('products.prod_name',[],$lang->iso2), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('lang['.$lang->id.'][name]', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            {!!Form::label('description',Lang::get('products.prod_desc',[],$lang->iso2), ['class'=>'col-md-3 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::textarea('lang['.$lang->id.'][description]', null, ['class' => 'form-control', 'id'=>'editor', 'rows'=>4, 'placeholder'=>trans('products.desc_help')]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $count++ ?>
        @endforeach
       </div>
    </div>

	<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('products.prod_cat'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div>
    @include('dashboard.categories.select_category_popup')

    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', trans('products.prod_img'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="media">
                <a class="pull-left" href="#">{!!HTML::image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfieldmain','style'=>'max-width:120px;'])!!}</a>
                <div class="media-body">
                    <h4 class="media-heading">{!!Form::file('image', ['class'=>'form-control img_browse','data-fid'=>'main'])!!}</h4>
                    <p><div class="help-block">{{trans('products.img_info')}}</div></p>
                </div>
            </div>
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

	<div class="form-group">
        <div class="col-md-3">Add to products</div>
        <div class="col-md-9"> 
            <label>{!!Form::checkbox('addToProds', null, false)!!} Add this product to My Listings and Website</label>
        </div>
    </div>

    <div class="form-group">
		<div class="col-md-3"></div>
		<div class="col-md-9"> 
			{!!Form::submit(trans('dash.add_product'), ['class'=>'btn btn-success btn_quickAddProduct', 'onclick'=>'quickAdd("'.route('dashboard.products.store').'", "#product_id")'])!!}
		</div>
	</div>
{!!Form::close()!!}
</div>

{!!HTML::script('frontend/js/app/products.js')!!}
<script type="text/javascript">
</script>