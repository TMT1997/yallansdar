<div class="white-popup-block">
<legend>{{trans('dash.add_new_group')}}</legend>
{!!Form::open(['id'=>'popup_form', 'class'=>'form-horizontal'])!!}
	<div class="form-group">
		{!!Form::label('name', trans('dash.group_name'), ['class'=>'control-label col-md-3'])!!}
		<div class="col-md-9">
			{!!Form::text('name', null, ['class'=>'form-control', 'required'=>'required'])!!}
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-3"></div>
		<div class="col-md-9"> 
			{!!Form::submit(trans('dash.add_group'), ['class'=>'btn btn-success btn_quickAddGroup'])!!}
		</div>
	</div>
{!!Form::close()!!}
</div>