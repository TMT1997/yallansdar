<?php $hide_menu = true;
$show_home_icon = true;
use App\Helper;
use App\Messages\Message;
?>
@extends('dashboard.layouts.master_reg')

@section('content')
  <aside class="col-xs-12 col-sm-12 col-md-2 col-lg-2 left-op-col" style="padding: 1px;">
      <header style="text-align: center;">

        <h4>{{trans('dash.account_overview')}}</h4>
        @if($user_profile)
        @if($user_profile->co_logo)
        <img src="{{asset('uploads/'.$user_profile->co_logo)}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        @else
        <img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        @endif
        <legend style="color:#00509f;">{{$user_profile->company_name}}</legend>
        @else
        <img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        @endif

        @if($user_package)
        <p style="font-size: 15px;">{{trans('dash.package_in')}}<span style="color:#00509f;font-weight:bold;font-size: 20px;"><br>  {{$packages->name_ar}}</span><br></p>
        <!--package detials-->
        @if($user_package)
        <p>{{trans('dash.package_type')}} : <span style="font-size: 15px;">  @if($packages->brief_en=="The Free Package")
        مجانية
        @else
        مدفوعة
        @endif</span></p>
        <p>{{trans('dash.start_date')}} : <span style="font-size: 15px;">  {{date('d-m-Y',strtotime($user_package->updated_at))}}</span></p>
        <p>{{trans('dash.package_time')}} : <span style="font-size: 15px;">  {{$packages->period}}</span></p>
        @endif
        @endif

        <!--end package detials-->

        <a href="upgrade">
        <button type="button" class="btn" style="background-color: #00509f;color: white;">{{trans('dash.upgrade_account')}}</button>
        </a>
      </header>
      <!--Dashboard icons-->
      <br>
              @if($user->importer)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
              @if($user->exporter)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @endif
              @if($user->service_provider)
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
            <!--Accordians-->
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/messages']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/messages']['label']))}}<span class="glyphicon glyphicon-envelope" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                 <div id="mssgs-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                    <a href="{{url('dashboard/messages/inbox')}}" class="inbx-btn">{{trans('dash.inbox')}}</a>
                    <a href="{{url('dashboard/messages/draft')}}" class="drfts-btn">{{trans('dash.draft')}}</a>
                    <a href="{{url('dashboard/messages/compose')}}" class="cmps-btn">{{trans('dash.compose')}}</a>
                    <a href="{{url('dashboard/messages/sent')}}" class="rmndr-btn">{{trans('dash.sent')}}</a>
                    <a href="{{url('dashboard/messages/contacts')}}" class="contcs-btn">{{trans('dash.my_contacts')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/profile']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/profile']['label']))}}<span class="glyphicon glyphicon-user" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                 <div id="pofl-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                    <a href="{{url('dashboard/profile')}}" class="bsnsprfl-btn">{{trans('dash.my_profile')}}</a>
                    <a href="{{url('dashboard/profile/trade')}}" class="trdinfo-btn">{{trans('dash.trade_information')}}</a>
                    <a href="{{url('dashboard/profile/factory')}}" class="facinfo-btn">{{trans('dash.factory_information')}}</a>
                    <a href="{{url('dashboard/profile/registration')}}" class="rstdtls-btn">{{trans('dash.registration_details')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
              <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/products']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/products']['label']))}}<span class="glyphicon glyphicon-shopping-cart" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                 <div id="prdcts-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                    <a href="{{url('dashboard/products/create')}}" class="adprdct-btn">{{trans('dash.add_product')}}</a>
                    <a href="{{url('dashboard/products')}}" class="mngprdct-btn">{{trans('dash.manage_products')}}</a>
                    <a href="{{url('dashboard/groups')}}" class="mnggrp-btn">{{trans('dash.groups')}}</a>
                    <a href="{{url('dashboard/media')}}" class="mdybnk-btn">{{trans('dash.media')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/services']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/services']['label']))}}<span class="glyphicon glyphicon-cloud" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                 <div id="services-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                    <a href="{{url('dashboard/services/create')}}" class="adprdct-btn">{{trans('dash.add_service')}}</a>
                    <a href="{{url('dashboard/services')}}" class="mngprdct-btn">{{trans('dash.manage_services')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/deals']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/deals']['label']))}}<span class="glyphicon glyphicon-list-alt" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                  <div id="dl-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                     <a href="{{url('dashboard/sell_offers/create')}}" class="spcilofr-btn">{{trans('dash.sell_offers')}}</a>
                      <a href="{{url('dashboard/request_quotes/create')}}" class="rqstqot-btn">{{trans('dash.request_for_quote')}}</a>
                      <a href="{{url('dashboard/partnerships/create')}}" class="prtnshp-btn">{{trans('dash.partnership_deals')}}</a>
                      <a href="{{url('dashboard/deals')}}" class="mngmydls-btn">{{trans('dash.manage_my_deals')}}</a>
                      <a href="{{url('dashboard/alerts/create')}}" class="crtdlsalrt-btn">{{trans('dash.create_deal_alert')}}</a>
                      <a href="#d" class="mdybnk-btn">{{trans('dash.manage_deal_alerts')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/website']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/website']['label']))}}<span class="glyphicon glyphicon-globe" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                  <div id="ws-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                     <a href="{{url('dashboard/website')}}" class="qiklunch-btn">{{trans('dash.quick_launch')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/settings']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/settings']['label']))}}<span class="glyphicon glyphicon-wrench" style="float: left;"></span></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
               <div id="dash-brd-tbs" style="border-style: none;">
                  <div id="stngs-tab" class="tb-div">
                  <div style="border-style: none;background-color: #eeeff0;">
                     <a href="{{url('dashboard/settings')}}" class="spcilofr-btn">{{trans('dash.account_settings')}}</a>
                      <a href="{{url('dashboard/settings/account-verification')}}" class="rqstqot-btn">{{trans('dash.account_verification')}}</a>
                      <a href="{{url('dashboard/reviews')}}" class="prtnshp-btn">{{trans('dash.reviews')}}</a>
                 </div>
                </div>
              </div>
            </div>
            </div>
            @endif
            <!--End Accordian-->
      <!---->
      <div class="p-txt" >
         <h5><a href="{{url('dashboard/support')}}">{{trans('dash.help_us')}}</a></h5>
         <header style="text-align: center;">
            <img src="{{asset('uploads/call.png')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        </header>
         <button onclick="send_call()" type="button" class="call_bt btn col-xs-12 col-sm-12 col-md-12 col-lg-12 left-op-col" style="background-color: #00509f;color: white;" >{{trans('front.contact_us')}}</button>
         <p>{{trans('dash.have_trouble')}}</p>
      </div>
  </aside>
@yield('page')
@endsection

@section('script')
  @include('dashboard.layouts.popup')
  <script type="text/javascript">
    //accoridian js
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active_accordion");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }
    //end accordian js
    @if(isset($template_steps) && !$template_steps->completed)
      $.magnificPopup.open({
        items: {
          src: '{{url('dashboard/website/checklist')}}'
        },
        type: 'ajax'
      });
    @endif

    function notify($message, $status) {
        var $bgcolor = '#7BAF1E'; //green
        var $fontcolor = '#fff';
        var $ineffect = 'slideBottom';
        var $outeffect = 'slideBottom';

        if(null !== $status) {
            if($status=='error') {
                $bgcolor = '#C9302C';
            }
            if($status=='alert') {
                $bgcolor = '#F8A605';
            }
        }
        $.amaran({
          content:{bgcolor:$bgcolor, color:$fontcolor, message:$message},
          'inEffect':$ineffect, 'outEffect':$outeffect, theme:'colorful'
        });
    }
     function send_call()
     {
       $.ajax({
              url: '{{url('dashboard/send-message')}}',
              type: 'GET',
              beforeSend:function() {

              }
          }).done(function(response) {
             notify('شكر علي طلب الاتصال .... ساتصل بك قريبا')
        });
    }
    //slider 1 adv
    jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 3000);
  var slideCount = $('#slider ul li').length;
  var slideWidth = $('#slider ul li').width();
  var slideHeight = $('#slider ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider').css({ width: slideWidth, height: slideHeight });

  $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

  });
  //slider 2 news
  jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 3000);
  var slideCount = $('#slider2 ul li').length;
  var slideWidth = $('#slider2 ul li').width();
  var slideHeight = $('#slider2 ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider2').css({ width: slideWidth, height: slideHeight });

  $('#slider2 ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider2 ul li:last-child').prependTo('#slider2 ul');

    function moveLeft() {
        $('#slider2 ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider2 ul li:last-child').prependTo('#slider2 ul');
            $('#slider2 ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider2 ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider2 ul li:first-child').appendTo('#slider2 ul');
            $('#slider2 ul').css('left', '');
        });
    };

    $('a.control_prev2').click(function () {
        moveLeft();
    });

    $('a.control_next2').click(function () {
        moveRight();
    });

  });
  //slider 3 Recent Posts
  jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 3000);
  var slideCount = $('#slider3 ul li').length;
  var slideWidth = $('#slider3 ul li').width();
  var slideHeight = $('#slider3 ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider3').css({ width: slideWidth, height: slideHeight });

  $('#slider3 ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider3 ul li:last-child').prependTo('#slider3 ul');

    function moveLeft() {
        $('#slider3 ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider3 ul li:last-child').prependTo('#slider3 ul');
            $('#slider3 ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider3 ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider3 ul li:first-child').appendTo('#slider3 ul');
            $('#slider3 ul').css('left', '');
        });
    };

    $('a.control_prev3').click(function () {
        moveLeft();
    });

    $('a.control_next3').click(function () {
        moveRight();
    });

  });
  </script>
@endsection