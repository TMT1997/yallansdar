<div class="white-popup-block">
	<div class="row">
		<div class="col-md-12" style="margin-bottom: 20px;">
			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-7">
					<div class="row">
						@foreach($packages as $package)
							<div class="col-md-4" style="{!!$package->style!!};padding:0 0 10px; text-align: center;">
								<h3>{{$package->name}}</h3>
								<h6>{{$package->brief}}</h6>
								<h4>
									@if($package->free)
										@lang('front.free')
									@else
										{{$package->price}} <sup>{{$package->currency}}</sup>
									@endif
								</h4>
								@if(Auth::check())
									@if(Auth::user()->package=='Free' && $package->free==1)
										<i class="fa fa-check-square text-success"></i>
									@else
									<a href="{{url('upgrade/payment/'.$package->id)}}" class="btn btn-default">Start</a>
									@endif
								@else
									<a href="{{url('upgrade/payment/'.$package->id)}}" class="btn btn-default">Start</a>
								@endif
							</div>
						@endforeach
					</div>
				</div>
			</div>

			@foreach($sections as $section)
				<?php
					$items_list = $items->filter(function($row) use ($section){
						return $row->section == $section;
					});
				?>
				@if($items_list->count()!=0)
					<div class="row" style="background:#e0e0e0">
						<div class="col-md-12" style="padding: 10px; text-align: center;"><strong>{{$section}}</strong></div>
					</div> 
					@foreach($items_list as $item)
						<?php $data = unserialize($item->values); ?>

						<div class="row" style="border-bottom: 2px solid #e0e0e0; padding:0px">
							<div class="col-md-5" style="padding:4px 10px">
								<strong>{{$item->name}}</strong> <br>
								<div class="help-block" style="font-size: 13px;">{{$item->description}}</div>
							</div>
							<div class="col-md-7">
								<div class="row">
									@foreach($packages as $package)
										<div class="col-md-4" style="{!!$package->style_cols!!};border:1px solid #f0f0f0;text-align: center; min-height: 60px; padding-top: 10px;">
											{{$data[$package->id]['value']}}
										</div>
									@endforeach
								</div>
							</div>
						</div>
					@endforeach
				@endif
			@endforeach

			<div class="row">
				<div class="col-md-5"></div>
				<div class="col-md-7">
					<div class="row">
						@foreach($packages as $package)
							<div class="col-md-4" style="{!!$package->style!!}; padding:5px 0; text-align: center;">
								<a href="{{url('upgrade/payment/'.$package->id)}}" class="btn btn-default">Start</a>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>