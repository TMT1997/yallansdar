<?php use App\Settings; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
	<legend>{{trans('dash.account_settings')}}</legend>
	<div class="help-block">
		{{trans('dash.settings_intro')}}
	</div>

	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active">
				<a href="#notific" aria-controls="notific" role="tab" data-toggle="tab">{{trans('dash.notification_settings')}}</a>
			</li>
			<li role="presentation">
				<a href="#security" aria-controls="security" role="tab" data-toggle="tab">{{trans('dash.security_settings')}}</a>
			</li>
			<li role="presentation">
				<a href="#membership" aria-controls="membership" role="tab" data-toggle="tab">{{trans('dash.membership_settings')}}</a>
			</li>
		</ul>
	
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="notific">
				{!!Form::open(['class'=>'form-horizontal'])!!}
				<div class="" style="margin-top: 20px;"> 

{!!Settings::option_switch('opt1',$settings['notifications'],trans('dash.notific_opt1'))!!}
{!!Settings::option_switch('opt2',$settings['notifications'],trans('dash.notific_opt2'))!!}
{!!Settings::option_switch('opt3',$settings['notifications'],trans('dash.notific_opt3'))!!}
{!!Settings::option_switch('opt4',$settings['notifications'],trans('dash.notific_opt4'))!!}
{!!Settings::option_switch('opt5',$settings['notifications'],trans('dash.notific_opt5'))!!}
{!!Settings::option_switch('opt6',$settings['notifications'],trans('dash.notific_opt6'))!!}
{!!Settings::option_switch('opt7',$settings['notifications'],trans('dash.notific_opt7'))!!}
{!!Settings::option_switch('opt8',$settings['notifications'],trans('dash.notific_opt8'))!!}

					<div class="form-group">
						<div class="col-md-offset-5 col-md-7">
						{!!Form::submit(trans('dash.save'), ['class'=>'btn btn-primary btn-loading'])!!}
						</div>
					</div>
				</div>
				{!!Form::close()!!}	
			</div>

			<div role="tabpanel" class="tab-pane" id="security">
				<div class="form-horizontal" style="margin-top: 20px;">
					<div class="form-group">
						{!!Form::label('pass', trans('dash.change_password'), ['class'=>'control-label col-md-3'])!!}
						<div class="col-md-5">
							<a href="{{url('dashboard/profile/password')}}" class="btn popup">{{trans('dash.change_password')}}</a>
						</div>
					</div>
					<div class="form-group">
						{!!Form::label('pass', trans('dash.change_email'), ['class'=>'control-label col-md-3'])!!}
						<div class="col-md-5" style="color: #555555;">
							<a href="{{url('dashboard/profile/change-email')}}" class="btn popup">{{trans('dash.change')}}</a>
							({{Auth::user()->email}}) 
						</div>
					</div>
					@if(Auth::user()->active==1)
						<div class="col-md-offset-3 col-md-4" style="color: #555555;">
							<a href="{{url('dashboard/profile/deactivate')}}" class="btn btn-danger popup">{{trans('dash.deactivate_account')}}</a>
							{!!App\Helper::popover(trans('dash.deactivate_account'),'Initiates the deactivation process of your YallaNsadar account.')!!}
						</div>
					@endif
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="membership">
				<div class="form-horizontal" style="margin-top: 20px;color: #555555;" >
					<a href="{{url('dashboard')}}"><p style="font-size: 15px;">اضغط هنا لمعرفة معلومات عن العضوية والباقة </p></a>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
@include('dashboard.layouts.popup')
@endsection