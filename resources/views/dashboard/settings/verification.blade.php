@extends('dashboard.layouts.master_reg')

@section('content')
	
	<legend>{{trans('dash.account_verification')}}</legend>
	<div class="help-block">
		{{trans('dash.verification_intro')}}
	</div>

	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>{{trans('dash.primary_email')}}</th>
				<th>{{trans('dash.mobile')}}</th>
				<th>{{trans('dash.address')}}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{Auth::user()->email}}
				<br><span class="text-success">{{trans('dash.verified')}}</span></td>
				<td>{{Auth::user()->mobile}} <br><span class="text-danger">{{trans('dash.not_verified')}}</span></td>
				<td>{{Auth::user()->address}} <br><a href="{{url('dashboard/profile')}}">{{trans('dash.add_now')}}</a></td>
			</tr>
		</tbody>
	</table>
	
@endsection