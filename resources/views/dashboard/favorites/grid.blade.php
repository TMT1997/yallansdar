<?php
    use App\Helper;
?>
<table class="table table-bordered table-striped table-hover dataTable">
    <thead>
        <tr>
            <th>{{trans('dash.id')}}</th>
            <th>{{trans('dash.name')}}</th>
            <th>{{trans('dash.image')}}</th>
            <th>{{trans('dash.date')}}</th>
            <th>{{trans('dash.actions')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($favorites as $item)
        <?php
            $name=$item->record_id;
            $image = '';
            if($item->type=='Products') {
                $lang_data = null;
                if($item->product) {
                    $lang_data=$item->product->lang->filter(function($row) use ($lng_id) { 
                        return $row->language_id==$lng_id; 
                    })->first();                    
                }
                $name = ($lang_data) ? $lang_data->name : '';
                $image = ($item->product) ? Helper::image($item->product->image):'';
                $url = ($item->product) ? App\Product::url($item->product) : '';
            }
        ?>
        <tr id="tr_{{$item->id}}">
            <td>{{ $item->id }}</td>
            <td>{{ $name }}</td>
            <td>{!!$image!!}</td>
            <td>{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</td>
            <td>
                <a href="{{$url}}" target="_blank" class="btn btn-success btn-sm" title="{{trans('dash.view')}}"><span class="glyphicon glyphicon-eye-open"/></a>

                {!!Form::open(['method'=>'DELETE','url'=>['dashboard/favorites',$item->id],'style'=>'display:inline'
                ])!!}
                {!! Form::button('<i class="fa fa-trash" title="'.trans("dash.delete").'"></i>',array(
                    'type' => 'submit','class' => 'btn btn-danger btn-sm','title'=>'Delete Permenantly',
                    'onclick'=>'return confirm("{{trans("dash.confirm_delete")}}")'
                )) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($favorites->count()==0)
    No results found
@endif