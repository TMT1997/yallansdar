@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{trans('dash.my_fav')}}
        </div>
        <div class="panel-body">
            <div role="tabpanel">
                <ul class="nav nav-tabs fav_types" role="tablist">
                    @foreach(App\Favorite::$types as $type)
                        <?php $counter = $favorites->filter(function($row) use($type){
                            return $row->type==$type;
                        })->count(); ?>

                        <li role="presentation"><a href="#f{{$type}}" data-sid="{{$type}}" aria-controls="{{$type}}" role="tab" data-toggle="tab">{{trans('dash.'.snake_case($type))}} ({{$counter}})</a></li>
                    @endforeach
                </ul>
                <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
            </div>            
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.fav_types a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var type = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/favorites/grid')}}?type='+type,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('{{trans('dash.loading')}}');
                }
            }).done(function(response) {
                $('.data_grid').html(response);
            }).fail(function() {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying favorites grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
        $(document).ready(function() {
            $('.fav_types a[data-toggle="tab"]:first').trigger('click');
        });
    </script>
@endsection