@extends('dashboard.layouts.master_reg')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{trans('dash.groups')}}

        <a href="{{ url('/dashboard/groups/create') }}" class="btn btn-success pull-left btn-sm" title="Add New Group">{{trans('dash.add_new_group')}}</a>
    </div>
    <div class="panel-body">
        <div class="well well-sm">
            {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                {!!App\Helper::select('parent',$parents,[''=>trans('dash.parent_group')],Request::get('parent'))!!}
            {!! Form::close() !!}
        </div>

        <table class="table table-bordered table-hover table-striped dataTable">
            <thead>
                <tr>
                    <th>{{trans('dash.id')}}</th>
                    <th>{{trans('dash.active')}}</th>
                    <th>{{trans('dash.parent')}}</th>
                    <th>{{trans('dash.group_name')}}</th>
                    <th>{{trans('dash.no_of_products')}}</th>
                    <th>{{trans('dash.tasks')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($groups as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{!!App\Helper::sw($item,'active','Group')!!}</td>
                    <td>{{ ($item->parent_group_id!=0)?$parents[$item->parent_group_id]:'' }}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->products->count()}}</td>
                    <td>
                        <a href="{{url('dashboard/products?group_id='.$item->id)}}" class="btn btn-default btn-sm">{{trans('dash.view_products')}}</a>
                        <a href="{{ url('/dashboard/groups/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="Edit Group"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE', 'url' => ['dashboard/groups', $item->id], 'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Group" />', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete Group',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection