@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{trans('dash.add_new_group')}}</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!!Form::open(['url'=>'/dashboard/groups','class'=>'form-horizontal','files' => true]) !!}
            @include ('dashboard.groups.form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection