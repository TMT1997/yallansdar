@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{trans('dash.edit_group')}}: {{$group->title}}</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($group, [
                'method' => 'PATCH',
                'url' => ['/dashboard/groups', $group->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include ('dashboard.groups.form', ['submitButtonText' => trans('dash.save_group')])
            {!! Form::close() !!}
        </div>
    </div>
@endsection