<div class="form-group {{ $errors->has('parent_group_id') ? 'has-error' : ''}}">
    {!! Form::label('parent_group_id', trans('dash.parent_group'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!App\Helper::select('parent_group_id', $parents, ['0'=>trans('dash.select_parent')])!!}
        {!! $errors->first('parent_group_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('dash.group_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required'=>'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-3 col-md-4">
        {!!Form::submit(isset($submitButtonText) ? $submitButtonText:trans('dash.add_group'),['class'=>'btn btn-primary'])!!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('dash.cancel')}}</a>
    </div>
</div>