<?php use App\Messages\Folder; ?>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('dash.folder_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('dash.folder_name')])!!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-4">
        {!!Form::submit(isset($submitButtonText) ? $submitButtonText : trans('dash.add'), ['class' => 'btn btn-loading btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('dash.cancel')}}</a>
    </div>
</div>