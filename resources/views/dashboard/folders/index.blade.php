<?php use App\Messages\Contact; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{trans('dash.folders')}}
    <a href="{{ url('/dashboard/folders/create') }}" class="btn btn-success pull-right" title="Add New Group">{{trans('dash.add_new_folder')}}</a>
    </div>
    <div class="panel-body">            
        <div class="row">
            @foreach($folders as $item)
                <div class="col-md-4">

                    <div class="well well-sm">
                        {{$item->name}}
                        <a href="{{ url('dashboard/folders/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm" title="Edit Contact"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['dashboard/folders', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="fa fa-trash-o" aria-hidden="true" title="Delete Contact" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Contact',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection