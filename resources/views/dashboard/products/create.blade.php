@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('products.create_new_product')}}</legend>

    <div class="help-block">{{trans('products.add_prod_help')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/products','class'=>'form-horizontal','id'=>'product_form2','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.products.form')
    {!! Form::close() !!}

@endsection