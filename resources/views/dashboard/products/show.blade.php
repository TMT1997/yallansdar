<?php
    use App\Helper;
?>
@extends('dashboard.layouts.master_reg')

@section('content')
    
    <legend>@lang('products.view_prod'): {{Helper::field($product,'name')}}</legend>

    <a href="{{ url('dashboard/products/' . $product->id . '/edit') }}" class="btn btn-primary btn-sm" title="@lang('products.edit_prod')"><span class="fa fa-pencil" aria-hidden="true"/></a>
    
    @if($product->approved!=4)
        <a href="javascript:void(0)" data-id='{{$product->id}}' data-url='{{url('dashboard/products/trash')}}' class="btn btn-danger btn-sm btn_del" title="@lang('products.trash_prod')"><i class="fa fa-trash"></i></a>
    @else
        <a href="javascript:void(0)" data-id='{{$product->id}}' data-url='{{url('dashboard/products/restore')}}' class="btn btn-warning btn-sm btn_restore" title="@lang('products.restore_prod')"><i class="fa fa-undo"></i></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['dashboard/products', $product->id],
            'style' => 'display:inline'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash" title="'.trans("products.del_prod").'"></i>', array(
                'type' => 'submit',
                'class' => 'btn btn-danger btn-sm',
                'title' => 'Delete Product Permenantly',
                'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
        )) !!}
        {!! Form::close() !!}
    @endif
    <br/>
    <br/>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $product->id }}</td>
                </tr>
                <tr><th> @lang('dash.category') </th><td> {{Helper::field($product->category,'name')}} </td></tr>
                <tr><th> Name </th><td> {{ Helper::field($product, 'name') }} </td></tr>
                <tr><th> Description </th><td> {!! Helper::field($product, 'description') !!} </td></tr>
                <tr><th> Details </th><td> {!! Helper::field($product, 'details') !!} </td></tr>
                <tr><th> Image </th><td> {!! Helper::image($product->image) !!} </td></tr>
                <tr><th> Packaging </th><td> {{ Helper::field($product, 'packaging') }} </td></tr>
                <tr><th> Detials Arabic </th><td> {!!$product->details_ar!!} </td></tr>
                <tr><th> Detials English </th><td> {!!$product->details_en!!} </td></tr>
                <tr><th> Production Capacity</th><td> {!!$product->production_capacity!!} </td></tr>
                <tr><th> Capacity Unit </th><td> {!!$product->capacity_unit!!} </td></tr>
                <tr><th> Minimum Quantity </th><td> {!!$product->min_quantity!!} </td></tr>
                <tr><th> Minimum Quantity Unit</th><td> {!!$product->min_quantity_unit!!} </td></tr>
                <tr><th> Fob Price</th><td> {!!$product->fob_price!!}   {!!$product->fob_price_unit!!}</td></tr>
                <tr><th> Payment Type</th><td> {!!$product->payment_type!!} </td></tr>
                <tr><th> Port </th><td> {!!$product->port!!} </td></tr>
                <tr><th> Delivery Time </th><td> {!!$product->delivery_time!!} </td></tr>
                <tr><th> Delivery Duration </th><td> {!!$product->delivery_duration!!} </td></tr>
            </tbody>
        </table>
    </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection