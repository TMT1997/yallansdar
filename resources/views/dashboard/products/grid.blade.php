<?php
    use App\Helper;
?>
<div class="label label-primary pull-left">@lang('products.no_products') {{$products->count()}}</div>
<table class="table table-bordered table-striped table-hover dataTable">
    <thead>
        <tr>
            <th>@lang('dash.private_id')</th>
            <th>@lang('dash.id')</th>
            <!-- <th>@lang('products.active')</th> -->
            <th>@lang('dash.category')</th>
            <th>@lang('products.prod_name')</th>
            <th>@lang('products.prod_img')</th>
            <th>@lang('products.last_update')</th>
            <th>@lang('products.actions')</th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $index=>$item)
        <?php
            //$lang_data=$item->lang->filter(function($row) use ($lng_id) { return $row->language_id==$lng_id; })->first();
        ?>
        <tr id="tr_{{$item->id}}">
            <td>{{ $index+1 }}</td>
            <td>{{ $item->id }}</td>
            <!-- <td>{!!Helper::sw($item,'active','Product')!!}</td> -->
            <td>{{ Helper::field($item->category, 'name') }}</td>
            <td><strong>{{Helper::field($item, 'name')}}</strong> </td>
            <td>{!!Helper::image($item->image)!!}</td>
            <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
            <td nowrap="">
                <a href="{{url('dashboard/products/'.$item->id)}}" class="btn btn-success btn-sm" title="@lang('products.view_prod')"><span class="fa fa-eye"/></a>
                <?php $dis = ($item->approved!=2)?'disabled="disabled"':''; ?>
                <a href="{{route('dashboard.products.edit',$item->id)}}" class="btn btn-primary btn-sm" {{$dis}} title="@lang('products.edit_prod')"><span class="fa fa-pencil"/></a>

                @if($item->approved!=4)
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/products/trash')}}' class="btn btn-danger btn-sm btn_del" title="@lang('products.trash_prod')"><i class="fa fa-trash"></i></a>
                @else
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/products/restore')}}' class="btn btn-warning btn-sm btn_restore" title="@lang('products.restore_prod')"><i class="fa fa-undo"></i></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['dashboard/products', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash" title="'.trans("products.del_prod").'"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Product Permenantly',
                            'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
                    )) !!}
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
$(".dataTable").DataTable($datatable_config);
</script>