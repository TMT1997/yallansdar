@extends('dashboard.layouts.master_reg')
@section('title') @lang('dash.products') @endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                @lang('dash.products')
            </div>
            <div class="col-md-8 text-left">
                <a href="{{ url('dashboard/products/create') }}" class="btn btn-sm btn-success">@lang('products.add_new_product')</a>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div role="tabpanel">
            <ul class="nav nav-tabs product_status" role="tablist">
                @foreach(App\Product::$product_status as $state_id=>$status)
                    <?php $counter = $products->filter(function($row) use($state_id){
                            return $row->approved==$state_id;
                        })->count(); ?>
                    @if($status=='All')
                        <?php $counter = $products->count(); ?>
                    @endif
                    <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('products.statuses')[$state_id]}} ({{$counter}})</a></li>
                @endforeach
            </ul>
            <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
        </div>
    </div>
</div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/products.js')!!}
    <script type="text/javascript">
        $('body').on('click', '.product_status a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var status = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/productss/grid')}}?status='+status,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('@lang('products.loading')');
                }
            }).done(function(response) {
                 $('.data_grid').html(response);
            }).fail(function(error) {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying products grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
        $(document).ready(function() {
            $('.product_status a[data-toggle="tab"]:first').trigger('click');
        });
    </script>
@endsection