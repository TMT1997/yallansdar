@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('products.edit_product')}}: {{ $product->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($product, [
        'method' => 'PATCH',
        'url' => ['dashboard/products', $product->id],
        'class' => 'form-horizontal', 'id'=>'product_form_edit',
        'files' => true
    ]) !!}
    @include ('dashboard.products.form', ['submitButtonText' => trans('products.update')])
    {!! Form::close() !!}
@endsection