<!doctype html>
<html><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title') | @lang('dash.yalla_dash')</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="{{trans('front.about_yallansadar')}}" />
<style type="text/css">
.next_form_submit
{
  float: left;width: 100px;
  background: #27AE60;
  font-weight: bold;
  color: white;
  border: 0 none;
  border-radius: 1px;
  cursor: pointer;
  padding: 10px 5px;
  margin: 10px 5px;
}
.carousel-inner img {
      width: 100%;
      height: 200px;
  }
  .accordion {
    background-color: #eeeff0 ;
    color: #555555;
    cursor: pointer;
    padding: 15px;
    width: 100%;
    border: none;
    text-align: center;
    outline: none;
    font-size: 17px;
    transition: 0.4s;
    border: 1px solid #555555;
}
.accordion_dashboard {
    background-color: #eeeff0 ;
    color: #555555;
    cursor: pointer;
    padding: 15px;
    width: 100%;
    border: none;
    text-align: center;
    outline: none;
    font-size: 17px;
    transition: 0.4s;
    border: 1px solid #555555;
}
.accordion_dashboard:hover {
    background-color: #00509f;
    color: white;
}
.acc_elem
{
    cursor: pointer;
    padding: 15px;
    border: none;
    text-align: center;
    font-size: 17px;
    color: #555555;
    display: block;
}
.acc_elem:hover
{
  color: #00509f;
    text-decoration: none;
}
.img_icon_dash
{
  float: left;
  width: 30px;
  height: 30px;
}
.active{
    color: white;
}
.active_accordion, .accordion:hover {
    background-color: #00509f;
    color: white;
}

.accordion:after {
    content: '\002B';
    color: #555555;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active_accordion:after {
    content: "\2212";
    color: white;
}

.panel_acc {
    margin: 10px;
    background-color: #eeeff0;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    position: relative;
}
#slider,#slider2 ,#slider3{
  position: relative;
  overflow: hidden;
  margin: 20px auto 0 auto;
  border-radius: 4px;
}

#slider ul ,#slider2 ul,#slider3 ul{
  position: relative;
  margin: 0;
  padding: 0;
  height: 200px;
  list-style: none;
}

#slider ul li , #slider2 ul li,#slider3 ul li{
  position: relative;
  float: left;
  margin: 0;
  padding: 0;
  width: 280px;
  height: 500px;
  background: white;
  text-align: center;
}

a.control_prev, a.control_next ,a.control_prev2, a.control_next2,a.control_prev3, a.control_next3{
  position: absolute;
  top: 40%;
  z-index: 999;
  border-radius: 50% !important;
  padding: 4% 4%;
  width: auto;
  height: auto;
  color: white;
  font-weight: 600;
  font-size: 18px;
  opacity: 0.7;
  cursor: pointer;
}
a.control_prev, a.control_next
{
  background: #5eb51a;
}
a.control_prev2, a.control_next2
{
  background: #7ccafa;
}
a.control_prev3, a.control_next3
{
  background:#f79434;
}
a.control_prev:hover, a.control_next:hover,a.control_prev2:hover, a.control_next2:hover,a.control_prev3:hover, a.control_next3:hover {
  opacity: 1;
  text-decoration: none;
  -webkit-transition: all 0.2s ease;
}

a.control_prev , a.control_prev2,a.control_prev3{
  border-radius: 0 2px 2px 0;
  right: 0;
}

a.control_next , a.control_next2 , a.control_next3{
  left: 0;
  border-radius: 2px 0 0 2px;
}

.slider_option {
  position: relative;
  margin: 10px auto;
  width: 160px;
  font-size: 18px;
}
label
{
  color: black;
}

</style>
{{HTML::style('frontend/css/bootstrap.min.css')}}
{{HTML::style('dash/css/bootstrap.min.css')}}
{{HTML::style("frontend/css/owl.carousel.css")}}
{{HTML::style("frontend/css/owl.theme.css")}}
{{HTML::style('frontend/css/font-awesome.min.css')}}
{{HTML::style('frontend/css/dashboard/dashboard.css')}}
{{HTML::style('frontend/css/jquery-ui.min.css')}}
{{HTML::style('dash/css/amaran.min.css')}}
{{HTML::style('backend/plugins/select2/select2.min.css')}}
{{HTML::style('backend/plugins/select2/select2-bootstrap.min.css')}}
{{HTML::style('dash/packages/switch/switch.min.css')}}
{{HTML::style('dash/packages/fileUpload/css/jquery.fileupload.css')}}
{{HTML::style('frontend/css/dashboard/style.css')}}
{{HTML::style('frontend/css/dashboard/registeration.css')}}
@if(App::isLocale('ar'))
    {!!HTML::style('backend/dist/css/bootstrap-rtl.min.css')!!}
    {!!HTML::style('frontend/css/dashboard/rtl.css')!!}
@endif
<script type="text/javascript">
    var catSearchURL = '{{url('dashboard/settings/category-search')}}';
    var $no_thumb_url = '{{url('frontend/images/no_thumb.jpg')}}';
    var $addGroupRoute = '{{route('dashboard.groups.store') }}';
    var $switch_bool_url = '{{url('admin/settings/switch-boolean')}}';
    var $datatable_config = {
        "language": {
          "sProcessing":   "@lang('dash.sProcessing')",
          "sLengthMenu":   "@lang('dash.sLengthMenu')",
          "sZeroRecords":  "@lang('dash.sZeroRecords')",
          "sInfo":         "@lang('dash.sInfo')",
          "sInfoEmpty":    "@lang('dash.sInfoEmpty')",
          "sInfoFiltered": "@lang('dash.sInfoFiltered')",
          "sInfoPostFix":  "",
          "sSearch":       "@lang('dash.sSearch')",
          "sUrl":          "",
          "oPaginate": {
              "sFirst":    "@lang('dash.sFirst')",
              "sPrevious": "@lang('dash.sPrevious')",
              "sNext":     "@lang('dash.sNext')",
              "sLast":     "@lang('dash.sLast')"
          }
        },
        "lengthMenu": [[5,10,25,50,-1], [5,10,25,50,"الكل"]],
        "aaSorting": [],
          buttons: [
            'selectAll',
            'selectNone'
        ]
    };
    var $datatable2_config = {
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "aaSorting": []
    }
</script>
</head><body>
<div class="container-fluid" style="margin-top:8px; border-bottom: 1px solid #f0f0f0">
    <nav class="navbar navbar-default navbar_top" role="navigation" style="min-height: auto; padding:1px 0; margin-bottom: 10px;">
      <!--<a href=""><img style="min-height: 20px;padding: 0px;margin: 0px;" src="http://localhost/yallansdar/public/frontend/images/logo/logo-ar.png" class="navbar-brand" alt="Yalla Nsadar"></a>-->

        <ul class="collapse navbar-collapse nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{Auth::user()->name}} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('dashboard/profiles/personal')}}"><i class="fa fa-user"></i> {{trans('dash.my_profile')}}</a></li>
                    <li><a href="{{url('dashboard/settings')}}"><i class="fa fa-gear"></i> {{trans('dash.settings')}}</a></li>
                    <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> {{trans('front.logout')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="#" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                    @foreach ($languages as $lang)
                        <?php $sel = (Session::get('yalla_locale')==$lang->iso2) ? 'selected="selected"':''; ?>
                        <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="#" id="buy-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('dash.buy')}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                    <li><a href="{{url('dashboard/request_quotes/create')}}">{{trans('front.post_buying_req')}}</a></li>
                    <li><a href="{{url('dashboard/alerts')}}">{{trans('front.create_deal_alert')}}</a></li>
                    <li><a href="{{url('category')}}" target="_blank">{{trans('front.find_products')}}</a></li>
                    <li><a href="{{url('deals')}}" target="_blank">{{trans('front.find_new_deals')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="#" id="sell-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('dash.sell')}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="sell-drp-mnu">
                    <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.new_rfqs')</a></li>
                    <li><a href="{{url('dashboard/products')}}">@lang('front.add_products')</a></li>
                    <li><a href="{{url('dashboard/sell_offers/create')}}">@lang('front.post_sell_offer')</a></li>
                    <li><a href="{{url('dashboard/partnerships/create')}}">@lang('front.post_partnership_deal')</a></li>
                </ul>
            </li>
            <li>

                 @if($unread_messages->count()>0)
                    <a href="#" id="sell-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('front.mymessages')}} <span class="label label-danger">{{$unread_messages->count()}}</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu dash_messages_list">
                        @foreach($unread_messages as $msg)
                            <li><a href="{{App\Messages\Message::url($msg)}}">
                                <span>{{App\Messages\Message::since($msg)}}</span><br> {{$msg->subject}}</a>
                            </li>
                        @endforeach
                    </ul>
                 @else
                    <a href="{{url('dashboard/messages')}}">@lang('dash.view_all')</a>
                @endif
            </li>
            <li><a href="{{url('dashboard/favorites')}}">@lang('front.myfavorites')</a></li>
        </ul>
        <div class="collapse navbar-collapse nav navbar-nav navbar-left" id="menu_nav" style="padding: 0px;">
            <ul class="nav navbar-nav">
              <li ><a href="{{url('home')}}" style="padding-top: 2px;">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar',['class'=>'navbar-brand'])}}<br>{{trans('dash.back_to_home')}}</a></li>
            </ul>
        </div>
        @if(isset($hide_dashboard_icon))
        @else
        <div class="collapse navbar-collapse nav navbar-nav navbar-left" id="menu_nav" style="padding: 0px;">
            <ul class="nav navbar-nav">
              <li >
                <a href="{{url('dashboard')}}" style="color:#F8A605;padding-top: 35PX;">
                <img style="width: 50px;height: 45px;" src="{{asset('uploads/control_dash.png')}}"><br>
                {{trans('dash.dashboard')}}
                </a>
              </li>
            </ul>
        </div>
        @endif
    </nav>
</div>
<main>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          {{--Delete خطوات العمل--}}
      {{--@include('dashboard.steps.bar')--}}
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        @if(Session::has('flash_message'))
            <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {!!Session::get('flash_message')!!}
            </div>
        @endif
      </div>
    </div>
    <div class="row">
      @if($side_menu)
          <aside class="col-md-2 col-lg-2">
              @include('dashboard.layouts.sidebar')
          </aside>
      @endif
      <div class="main_panel col-md-{{($side_menu)?10:12}}">
          @yield('content')
      </div>
    </div>
  </div>
</main>
<footer class="main-ftr">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ftr-mnu">
      <ul class="ftr-ul">
          <li><a href="{{url('site')}}" target="_blank">{{trans('front.get_to_know')}}</a></li>
          <li><a href="{{url('site/join-us')}}">{{trans('front.join_us')}}</a></li>
          <li><a href="{{url('testimontials')}}">{{trans('front.feedback')}}</a></li>
          <li><a href="{{url('site/contact')}}">{{trans('front.contact_us')}}</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ftr-cpy-rths">
      <ul class="ftr-ul">
          <li>{{trans('front.copyrights')}}</li>
          <li><a href="#d">{{trans('front.terms')}}</a></li>
          <li><a href="#d">{{trans('front.privacy_policy')}}</a></li>
      </ul>
    </div>
</footer>
</main>
{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
{!!HTML::script('frontend/js/jquery-ui.min.js')!!}
{!!HTML::script('frontend/js/bootstrap.min.js')!!}
{!!HTML::script('frontend/js/registerationjs.js')!!}
{!!HTML::script('frontend/js/jquery.dataTables.min.js')!!}
{!!HTML::script('frontend/js/dataTables.bootstrap.min.js')!!}
{!!HTML::script('backend/plugins/select2/select2.min.js')!!}
{!!HTML::script('backend/ckeditor/ckeditor.js')!!}
{!!HTML::script('dash/js/jquery.amaran.min.js')!!}
{{HTML::script('frontend/js/pgwslider.min.js')}}
{{HTML::script('frontend/js/owl.carousel.min.js')}}
{!!HTML::script('dash/packages/switch/switch.min.js')!!}
{!!HTML::script('dash/packages/fileUpload/js/jquery.fileupload.js')!!}
{!!HTML::script('dash/packages/validator/jquery.validate.min.js')!!}
{!!HTML::script('dash/packages/validator/additional-methods.min.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
<script>
    var csrf_token = '{{csrf_token()}}';
   // $('.switch').bootstrapSwitch();
    $(function () {
      if($("#editor").length>0) {
          CKEDITOR.replace('editor',{
            toolbar : 'Full'
        });
      }
      if($("#editor1").length>0) {
          CKEDITOR.replace('editor1');
      }
      if($("#editor2").length>0) {
          CKEDITOR.replace('editor2');
      }
      if($('#editor_ar').length) {
        CKEDITOR.replace('editor_ar',{
          toolbar : 'Full',
          filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
      if($('#editor_en').length) {
        CKEDITOR.replace('editor_en',{
          toolbar : 'Full',
          filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
    });
    $(function() {
        $( "#dash-brd-tbs" ).tabs();
    });
    $(function() {
        $(".datepicker").datepicker();
    });
    $(function() {
        $(".select2").select2({
          theme: "bootstrap",
          width: '100%'
        });
    });

    $(".lvl-one-mnu-itm ").hover(function() {
        $sideMnuWdth = $('.sid-mnu').outerWidth();
        console.log( $sideMnuWdth );
        $('ul.sub-lvl-two-itm').css('left', $sideMnuWdth-10);
        //$(".lvl-one-mnu-itm > a").css('opacity', 0.6);
        //$(this).find(' a ').css('opacity', 1);
    });
    $(function () {
      $('[data-toggle="popover"]').popover()
    })

    //M2: Preview Image browsed for upload
    function readURL(input, fID) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.img_preview.imgfield'+fID).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('body').on('change', '.img_browse', function(){
        var fid = $(this).data('fid');
        readURL(this, fid);
    });
    $('.btn-loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
    $('body').on('change', '.form-filter select', function(event) {
      event.preventDefault();
      $('.form-filter').submit();
    });

    $('body').on('click', '.btn_boolean', function(event) {
        event.preventDefault();
        if(window.confirm('{{trans('dash.change_state_confirm')}}'))
        {
          $id = $(this).data('id');
          $field = $(this).data('field');
          $model = $(this).data('model');
          $element = $('.elem'+$field+$id);
          $.ajax({
              url: '{{url('admin/settings/switch-boolean')}}',
              type: 'GET',
              data: {id: $id, model: $model, field: $field},
              beforeSend:function() {
                  $element.html('loading');
              }
          }).done(function(response) {
              $res = $.parseJSON(response);
              $elem_wrapper = $element.parent('td');
              $element.remove();
              if($res.status==1) {
                  if($res.newval==1) {
                      $color = 'success';
                      $icon = 'check-circle';
                  } else {
                      $color = 'danger';
                      $icon = 'minus-circle';
                  }

                  $elem_wrapper.html('<a href="#" class="text-'+$color+' btn_boolean elem'+$field+$id+'" data-id="'+$id+'" data-model="'+$model+'" data-field="'+$field+'"><i class="fa fa-2x fa-'+$icon+'"></i></a>');

              } else {
                  alert('خطا حدث في تغيير التفعيل');
              }
          }).fail(function() {
              console.log("خطا حدث في تغيير التفعيل");
          }).always(function() {
          });
        }
    });
    $(".dataTable").DataTable($datatable_config);
    $('.dataTable2').DataTable($datatable2_config);

    $('body').on('click', '.btn_quickAddContact', function(event) {
        event.preventDefault();
        quickAdd('{{route('dashboard.contacts.store')}}', '.contactsList', 'first_name');
    });

    function quickAdd(URL, selectObj, $display_val)
    {
        if(null==$display_val) {
            $display_val = 'name';
        }
        $.ajax({
            url: URL,
            data: $("#popup_form").serializeArray(),
            type:'POST',
            dataType:'json',
        }).done(function(output) {
            if (output.status == 'success') {
                var row = output.data;
                var display_name = row[$display_val];
                $(selectObj).append('<option value="'+row.id+'" selected="selected">'+display_name+'</option>');
                $.magnificPopup.close();
                notify('Record has been added successfully');
            } else {
                alert('Some errors happened');
            }
        }).fail(function() {
            //notify('Error occured, please try again', 'error'); return false;
        }).always(function() {
            //$(".btnTarget").button('reset');
        });
    }
    function delete_img($obj, $model, $field) {
        var $id = $obj.data('id');
        if(window.confirm('Delete this file: Are you sure?')) {
            $.ajax({
                url: '{{url('dashboard/settings/deleteimage')}}?id='+$id+'&model='+$model+'&field='+$field,
                type: 'POST',
                data: {_token: csrf_token},
                beforeSend:function() {
                    $obj.append('<span class="del_load">deleting...</span>');
                }
            }).done(function(response) {
                output = jQuery.parseJSON(response);
                if(output.status=='success') {
                    $obj.parent().parent().parent('.infile').slideUp('400');
                } else {
                    alert('Error deleting file');
                }
            }).fail(function() {
                console.log("Error deleting file");
            }).always(function() {
                $obj.find('.del_load').remove();
            });
        }
    }
    function auth_check(response) {
        var status = response.status;
        if (status == 401) {
          window.location = "{{url('login?continue='.Request::fullUrl())}}"; return false;
          return;
        }
    }
    function notify($message, $status) {
        var $bgcolor = '#7BAF1E'; //green
        var $fontcolor = '#fff';
        var $ineffect = 'slideBottom';
        var $outeffect = 'slideBottom';

        if(null !== $status) {
            if($status=='error') {
                $bgcolor = '#C9302C';
            }
            if($status=='alert') {
                $bgcolor = '#F8A605';
            }
        }
        $.amaran({
          content:{bgcolor:$bgcolor, color:$fontcolor, message:$message},
          'inEffect':$ineffect, 'outEffect':$outeffect, theme:'colorful'
        });
    }
    $('body').on('click', '.btn-changepass', function(event) {
        event.preventDefault();
        var $btn = $(this);
        var oldpass = $('.oldpass');
        var newpass = $('.newpass');
        var confnewpass = $('.confnewpass');
        if(oldpass.val()=='') {
            notify('Please enter your current password', 'error'); oldpass.focus(); return false;
        }
        if(newpass.val()=='') {
            notify('Please enter your new password', 'error'); newpass.focus(); return false;
        }
        if(confnewpass.val()=='') {
            notify('Please enter your confirm password', 'error'); confnewpass.focus(); return false;
        }
        if(newpass.val() != confnewpass.val()) {
            notify('Password & confirm password doens\'t match', 'error'); confnewpass.focus(); return false;
        }
        $.ajax({
            url: '{{url('dashboard/profile/password')}}',
            type: 'POST',
            data: $(".changePassForm").serialize(),
            beforeSend:function() {
                $btn.button('loading');
            }
        }).done(function(response) {
            output = jQuery.parseJSON(response);
            $msg = output.message;
            if(output.status=='success') {
                notify($msg);
            } else if(output.status=='error') {
                notify('Error: '+$msg, 'error');
            } else {
                notify('Unknwon Error occured, <br>'+$msg, 'alert');
            }
        }).fail(function() {
            notify('Error changing Password', 'error');
        }).always(function() {
            $btn.button('reset');
        });
    });

    $('body').on('click', '.btn-changeemail', function(event) {
        event.preventDefault();
        var $btn = $(this);
        var newemail = $('.newemail');
        var pass = $('.pass');
        if(newemail.val()=='') {
            notify('Please enter your new email', 'error'); newemail.focus(); return false;
        }
        if(pass.val()=='') {
            notify('Please enter your password', 'error'); pass.focus(); return false;
        }
        $.ajax({
            url: '{{url('dashboard/profile/change-email')}}',
            type: 'POST',
            data: $(".changeEmailForm").serialize(),
            beforeSend:function() {
                $btn.button('loading');
            }
        }).done(function(response) {
            output = jQuery.parseJSON(response);
            $msg = output.message;
            if(output.status=='success') {
                notify($msg);
            } else if(output.status=='error') {
                notify('Error: '+$msg, 'error');
            } else {
                notify('Unknwon Error occured, <br>'+$msg, 'alert');
            }
        }).fail(function() {
            notify('Error changing Email', 'error');
        }).always(function() {
            $btn.button('reset');
        });
    });

    /*jslint unparam: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var upload_url = '{{url('dashboard/uploader')}}';
        $('.fileupload').fileupload({
            url: upload_url,
            dataType: 'json',
            done: function (e, data) {
                var action = $(this).data('action');
                var filesWrap = $(this).parent('.fileinput-button').siblings('.files');
                $.each(data.result.files, function (index, file) {
                    $('<p/>').html('<img src='+file.url+' width="90">').appendTo(filesWrap);
                    saveUploadedFile(file.full_name, action, csrf_token);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(this).parent('.fileinput-button').siblings('.progress').find('.progress-bar').css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
    function saveUploadedFile(fileName, actionStr, csrf_token) {
        $.ajax({
            url: '{{url("dashboard/uploader/serve")}}',
            type: 'POST',
            data: {_token: csrf_token, file: fileName, action: actionStr},
        }).done(function(response) {
            console.log(response);
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }
    jQuery.extend(jQuery.validator.messages, {
        @foreach(trans('dash.validations') as $key=>$valid_msg)
            '{{$key}}':'{{$valid_msg}}',
        @endforeach
    });
    $(document).on('click', '.btn_read_reason', function(event) {
      event.preventDefault();
      var $id = $(this).data('id');
      var $wrap = $('.res_'+$id);
      $.ajax({
        url: '{{url('dashboard/products/readreason')}}',
        type: 'GET',
        dataType: 'JSON',
        data: {id: $id},
        beforeSend: function() {
        }
      }).done(function(response) {
        if(response.status=='success') {
          $wrap.hide('slow', function() {
          });
        } else {
          alert('Error');
        }
      }).fail(function() {
        alert('Error');
      }).always(function() {
        console.log("complete");
      });

    });
     //accoridian js
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active_accordion");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }


    $( window ).on( "load", function() {
        var url   = window.location.href;
       if(url.indexOf("dashboard/messages") != -1)
       {
          $('.accordion.mess').click();
          if(url.indexOf("dashboard/messages/inbox") != -1)
            $('.acc_elem.inbx').css({'color': '#00509f','font-weight':'bold'});
          if(url.indexOf("dashboard/messages/draft") != -1)
            $('.acc_elem.drft').css({'color': '#00509f','font-weight':'bold'});
          if(url.indexOf("dashboard/messages/compose") != -1)
            $('.acc_elem.cmps').css({'color': '#00509f','font-weight':'bold'});
          if(url.indexOf("dashboard/messages/sent") != -1)
            $('.acc_elem.sen').css({'color': '#00509f','font-weight':'bold'});
       }//end message
       if(url.indexOf("dashboard/profiles") != -1)
       {
          $('.accordion.prof').click();
          if(url.indexOf("dashboard/profiles/personal") != -1)
            $('.acc_elem.prof.pers').css({'color': '#00509f','font-weight':'bold'});
          if(url.indexOf("dashboard/profiles/company") != -1)
            $('.acc_elem.prof.cmp').css({'color': '#00509f','font-weight':'bold'});
       }//end profile
       if(url.indexOf("dashboard/prod") != -1)
       {
          $('.accordion.prod').click();
          if(url.indexOf("dashboard/products/create") != -1)
            $('.acc_elem.cre').css({'color': '#00509f','font-weight':'bold'});
          else if(url.indexOf("dashboard/products") != -1)
            $('.acc_elem.mng').css({'color': '#00509f','font-weight':'bold'});
       }//end product
       if(url.indexOf("dashboard/services") != -1)
       {
          $('.accordion.serv').click();
          if(url.indexOf("dashboard/services/create") != -1)
            $('.acc_elem.cre_serv').css({'color': '#00509f','font-weight':'bold'});
          else if(url.indexOf("dashboard/services") != -1)
            $('.acc_elem.mng_serv').css({'color': '#00509f','font-weight':'bold'});
       }//end services
       if(url.indexOf("dashboard/sell_offers") != -1)
       {
          $('.accordion.deal').click();
          $('.acc_elem.cre_sell').css({'color': '#00509f','font-weight':'bold'});
       }//end sell_offers
       if(url.indexOf("dashboard/request_quotes") != -1)
       {
          $('.accordion.deal').click();
          $('.acc_elem.cre_req').css({'color': '#00509f','font-weight':'bold'});
       }//end request_quotes
       if(url.indexOf("dashboard/partnerships") != -1)
       {
          $('.accordion.deal').click();
          $('.acc_elem.cre_part').css({'color': '#00509f','font-weight':'bold'});
       }//end partnership
       if(url.indexOf("dashboard/deals") != -1)
       {
          $('.accordion.deal').click();
          $('.acc_elem.deal').css({'color': '#00509f','font-weight':'bold'});
       }//end deals
       if(url.indexOf("dashboard/settings") != -1)
       {
          $('.accordion.sett').click();
          if(url.indexOf("dashboard/settings/account-verification") != -1)
            $('.acc_elem.acc_ver').css({'color': '#00509f'});
          else if(url.indexOf("dashboard/settings") != -1)
            $('.acc_elem.set').css({'color': '#00509f','font-weight':'bold'});
       }//end settings
       if(url.indexOf("dashboard/quotations-send") != -1)
       {
          $('.accordion.prof').click();
          $('.acc_elem.off').css({'color': '#00509f','font-weight':'bold'});
       }//end settings
    });
    
    function notify($message, $status) {
        var $bgcolor = '#7BAF1E'; //green
        var $fontcolor = '#fff';
        var $ineffect = 'slideBottom';
        var $outeffect = 'slideBottom';

        if(null !== $status) {
            if($status=='error') {
                $bgcolor = '#C9302C';
            }
            if($status=='alert') {
                $bgcolor = '#F8A605';
            }
        }
        $.amaran({
          content:{bgcolor:$bgcolor, color:$fontcolor, message:$message},
          'inEffect':$ineffect, 'outEffect':$outeffect, theme:'colorful'
        });
    }
     function send_call()
     {
       $.ajax({
              url: '{{url('dashboard/send-message')}}',
              type: 'GET',
              beforeSend:function() {

              }
          }).done(function(response) {
             // notify('شكر علي طلب الاتصال .... ساتصل بك قريبا');
             document.getElementById("call").style.backgroundColor = '#28a745';
             document.getElementById("call").style.color = 'white';
              document.getElementById("call").innerHTML  = 'شكر علي طلب الاتصال .... ساتصل بك قريبا';
        });
    }
    function ValidURL(str) {
    var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
      '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
      '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
      '(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
      '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
      '(\#[-a-z\d_]*)?$','i'); // fragment locater
    if(!pattern.test(str)) {
      alert("Please enter a valid URL.");
      return false;
    } else {
      return true;
    }
  }
    //=======================================registeration====================================================
     
    @if(Session::has('back'))
          $(window).on('load', function() {
        current_fs = $('.profile')
        next_fs = $('.profile').next();
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        //show the next fieldset
        current_fs.slideUp();
        current_fs.hide();  next_fs.slideDown(1000);
        //hide the current fieldset with style
         window.scrollTo(0, 0);
      });
    @endif
    @if(Session::has('prod_done'))
          $(window).on('load', function() {
            prof_fs = $('.profile');
        current_fs = $('.prod');
        next_fs = $('.prod').next();
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        $(".2").addClass("active");
        //show the next fieldset
        current_fs.slideUp();
        current_fs.hide();
        prof_fs.hide();
          next_fs.slideDown(1000);
        //hide the current fieldset with style
         window.scrollTo(0, 0);
      });
    @endif
    @if(Session::has('done_template'))
          $(window).on('load', function() {
            prof_fs = $('.profile');
        current_fs = $('.prod');
        next_fs = $('.prod').next();
        //activate next step on progressbar using the index of next_fs
        $(".1").addClass("active");
        $(".2").addClass("active");
        $(".3").addClass("active");
        //show the next fieldset
        current_fs.slideUp();
        current_fs.hide();
        prof_fs.hide();
          next_fs.slideDown(1000);
        //hide the current fieldset with style
         window.scrollTo(0, 0);
      });
    @endif
    @if(Session::has('done_publish'))
          $(window).on('load', function() {
            prof_fs = $('.profile');
        current_fs = $('.choose');
        next_fs = $('.choose').next();
        prod_fs = $('.prod');
        //activate next step on progressbar using the index of next_fs
        $(".1").addClass("active");
        $(".2").addClass("active");
        $(".3").addClass("active");
        $(".4").addClass("active");
        //show the next fieldset
        current_fs.slideUp();
        current_fs.hide();
        prof_fs.hide();
        prod_fs.hide();
          next_fs.slideDown(1000);
        //hide the current fieldset with style
         window.scrollTo(0, 0);
      });
    @endif

    $(document).ready(function() {
      $(".owl-itms01").owlCarousel({
            /*rtl:true,*/
            lazyLoad : true,
            nav : true,
            items : 1,
            navText: ["",""],
            dots:false,
            loop: true,
            scrollPerPage : true,
            rewindNav : true,
        });
//    $('.popup').magnificPopup({type:'ajax'});
    if($(".check_user_type").text() == "0");
    else
    {
      $(".user_type_div").hide();
    }
    if($('.importer').is(':checked') || $('.exporter').is(':checked') || $('.service_provider').is(':checked'));
    else
    {$('.exporter').prop('checked', true);}
    $('.alert_message').hide();
        function onSubmit()
        {
            var fields = $("input[name='business_type[]']").serializeArray();
            if (fields.length === 0)
            {
                alert('من فضلك اختر نشاط الشركة ');
                // cancel submit
                return false;
            }
            else
            {

            }
        }

    // register event on form, not submit button
        $('#profileForm').submit(onSubmit)
  });

    $( ".check_english_url" ).keyup(function() {
      var arabic = /[\u0600-\u06FF]/;
      var data = this.value;
    var check= arabic.test(data);
    if(check)
    {
      alert('@lang("dash.alert_english")');
        this.value='';
    }
    //regular expression for URL
    if(data)
    {
      var pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
      if(pattern.test(data)){
          $('.alert_message').hide();
      } else {
          $('.alert_message').show();
      }
    }
    else
      $('.alert_message').hide();
  });

    $( ".check_english" ).keyup(function() {
      var arabic = /[\u0600-\u06FF]/;
      var data = this.value;
    var check= arabic.test(data);
    if(check)
    {
      alert('@lang("dash.alert_english")');
        this.value='';
    }
  });
    $( ".check_space" ).keyup(function() {
      this.value = $.trim(this.value);
  });
    $(document).on('change', '.select2', function(event) {
    var $country = $('.select2').val();
    $.ajax({
              url: '{{url('dashboard/countryget')}}',
              type: 'post',
              data: {country: $country,'_token':'{{csrf_token()}}'},
              beforeSend:function() {
                 
              }
          }).done(function(response) {
              if(response.length === 0)
              {
                $( "div.country_img" )
                .html('');
                $('.tel_cd').val('');
                $('.mob_cd').val('');
              }
              else
              {
                $( "div.country_img" )
                .html('<i class="flag"><img src="http://www.yallansadar.com/flags/48/'+response.iso2+'.png"</i>');
                $('.tel_cd').val(response.calling_code);
                $('.mob_cd').val(response.calling_code);
              }
          }).fail(function(error){
              alert(error);
          })
  });

    $(document).on('click', '.sameContactInfo', function(event) {
    if($(this).prop('checked')) {
      $('input[name="contact_f_name"]').val($('input[name="first_name"]').val());
      $('input[name="contact_l_name"]').val($('input[name="last_name"]').val());
      $('input[name="co_address_line1"]').val($('input[name="address_line1"]').val());
      $('input[name="co_address_line2"]').val($('input[name="address_line2"]').val());
      $('input[name="co_state"]').val($('input[name="state"]').val());
      $('input[name="co_city"]').val($('input[name="city"]').val());
      $('#co_country_id')
         .append($("<option selected='selected'></option>")
                    .attr("value",$('select[name=country_id]').val())
                    .text($('#country_id').find(":selected").text())); 
      $('input[name="co_zipcode"]').val($('input[name="zipcode"]').val());
    } else  {
      //do nothing
    }
  });

$("#show_form").click(function(){
        $("#show_form").css("display", "none");
        $("#comp_form").css("display", "block");
        $("#comp_form").fadeOut();
        $("#comp_form").fadeIn(4000);
      });
//submit forms by ajax
$("#profileForm").submit(function(e){
    if($('.sector_validate').val())
    {
      $.ajax({
          url: '{{url('dashboard/profile_form_done')}}',
          type: 'POST',
          data: $("#profileForm").serializeArray(),
      }).done(function(response) {
          //get user type from profile form
          var array = $("#profileForm").serializeArray();
          var user_type = array[1].value;
          if (user_type == "importer")
          {
            $("#change_Step").text(`@lang('dash.requestfor_quote')`);
            $("#choose_Step").remove();
            $(".choose").remove();
            $( "#service_provider_section" ).remove();
            $( "#product_section" ).remove();
          }
          else if(user_type == "service_provider")
          {
            $("#change_Step").text(`@lang('dash.add_new_service')`);
            $("#choose_Step").remove();
            $(".choose").remove();
            $( "#product_section" ).remove();
            $( "#importer_section" ).remove();
          }
          else
          {
            $("#change_Step").text(`@lang('dash.add_products')`);
            $( "#service_provider_section" ).remove();
            $( "#importer_section" ).remove();
          }
          //-----------------------------------------------
          $(".user_type_div").hide();
          current_fs = $('.profile')
          next_fs = $('.profile').next();
          //activate next step on progressbar using the index of next_fs
          $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
          //show the next fieldset
          current_fs.slideUp();
          current_fs.hide();  next_fs.slideDown(1000);
          //hide the current fieldset with style
           window.scrollTo(0, 0);
           //------------------------------------------------
      }).fail(function() {
      }).always(function() {
      });
   }
   else
   {
    alert('@lang("dash.choose_sector")')
   }
   e.preventDefault();
});

$("#product_form").submit(function(e){
    var form_data = new FormData($("#product_form")[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{url('dashboard/proudct_form_done')}}',
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: form_data,

    }).done(function(response) {
      console.log(response);
        $("#product_step").click();
    }).fail(function(response) {
      console.log(response.responseJson.messages);
    }).always(function() {
    });
     e.preventDefault();
});

$("#service_form_ajax").submit(function(e){
    var form_data = new FormData($("#service_form_ajax")[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{url('dashboard/service_form_done')}}',
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: form_data,

    }).done(function(response) {
      console.log(response);
        $("#product_step").click();
    }).fail(function(response) {
      alert('Fill Required Data');
      console.log(response.responseJson.messages);
    }).always(function() {
    });
     e.preventDefault();
});

$(".request_ajax").submit(function(e){
    var form_data = new FormData($(".request_ajax")[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{url('dashboard/request_quotes_done')}}',
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: form_data,

    }).done(function(response) {
      console.log(response);
        $("#product_step").click();
    }).fail(function(response) {
      alert('Fill Required Data');
      console.log(response.responseJson.messages);
    }).always(function() {
    });
     e.preventDefault();
});

$(".launch_form").submit(function(e){
    $.ajax({
        url: '{{url('dashboard/launch_form_done')}}',
        type: 'POST',
        data: $(".launch_form").serializeArray(),
    }).done(function(response) {
        current_fs = $('.launch')
        next_fs = $('.launch').next();
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        //show the next fieldset
        current_fs.slideUp();
        current_fs.hide();  next_fs.slideDown(1000);
        //hide the current fieldset with style
         window.scrollTo(0, 0);
    }).fail(function() {
    }).always(function() {
    });
     e.preventDefault();
});

$('.importer').click(function() {
   if($('.importer').is(':checked'))
   {
    $('.exporter').prop('checked', false);
    $('.service_provider').prop('checked', false);
    $("#change_Step").text(`@lang('dash.requestfor_quote')`)
   }
   else
   {
    $('.importer').prop('checked', true);
   }
});
$('.exporter').click(function() {
   if($('.exporter').is(':checked'))
   {
    $('.importer').prop('checked', false);
    $('.service_provider').prop('checked', false);
    $("#change_Step").text(`@lang('dash.add_products')`);
   }
   else
   {
    $('.exporter').prop('checked', true);
   }
});
$('.service_provider').click(function() {
   if($('.service_provider').is(':checked'))
   {
    $('.exporter').prop('checked', false);
    $('.importer').prop('checked', false);
    $("#change_Step").text(`@lang('dash.add_new_service')`);
   }
   else
   {
    $('.service_provider').prop('checked', true);
   }
});
  function getTemplate($sec)
  {
    $output = '';
    if($sec=='products') {
      $output='<div class="frec">'+
        '{!!Form::text('main_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name'), 'required'=>'required'])!!} '+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
    if($sec=='oproducts') {
      $output='<div class="frec">'+
        '{!!Form::text('other_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
    if($sec=='website') {
      $output='<div class="frec">'+
        '{!!Form::text('website[]',null,['class'=>'form-control','placeholder'=>trans('dash.website')])!!} '+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
    if($sec=='product_we_source') {
        $output='<div class="frec">'+
        '{!!Form::text('product_we_source[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!} '+
        '{!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!} '+
        '{!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!} <span>({{trans("dash.yearly")}})</span>'+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
      if($sec=='telephone') {
        $output='<div class="frec">'+
        '{!!Form::number('tel_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
        '{!!Form::number('telephone[]', null,['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!} '+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
    if($sec=='mobile') {
        $output='<div class="frec">'+
        '{!!Form::number('mob_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
        '{!!Form::number('mobile[]', null,['class'=>'form-control','placeholder'=>trans('dash.mobile')]) !!} '+
        '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
    }
    return $output;
  }
  $(document).on('click', '.add_new_field', function(e){
      e.preventDefault();
      var $ft = $(this).data('ft');
      var max_fields = $(this).data('max');
      var count_childs = $(this).siblings('.more_fields').children('.frec').length;
      count_childs = count_childs+1;
      if(count_childs < max_fields){
          $wrapper = $(this).siblings('.more_fields');
          $wrapper.append(getTemplate($ft));
      } else {
          alert('Sorry, you cannot add more than '+max_fields+' inputs');
      }
  });
  $(document).on("click",".remove_field", function(e){
      e.preventDefault(); $(this).parent('div').remove();
  });
</script>
@yield('script')
@yield('script2')
</body></html>