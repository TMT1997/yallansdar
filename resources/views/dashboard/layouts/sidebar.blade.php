<?php
use App\TemplateSteps;
use App\UserProfile;
use App\User;
use App\Packages\UserPackage;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Product;
use App\Deals\QuoteRequest;
use App\Deals\Quote;
use App\Messages\Message;
use App\Advs;
use App\UserWebsite;
$dash_adv = Advs::whereActive(1)->get();
$products = Product::whereUser_id(\Auth::id())->get();
$all_admins = User::whereAdmin(1)->get();
$user_profile = UserProfile::whereUser_id(\Auth::id())->first();
$user = User::whereId(\Auth::id())->first();
$user_package = UserPackage::whereUser_id(\Auth::id())->orderBy('id', 'desc')
->first();
$quote = Quote::whereFrom_user_id(\Auth::id())->get();
$template_steps = TemplateSteps::whereUser_id(\Auth::id())->first();
$quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->get();
$items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
$my_website = UserWebsite::whereUser_id(\Auth::id())->first();
$sections = Package::$sections;
if($user_package)
{ 
    $packages = Package::where('id',"=",$user_package->package_id)->first();
    if($products->count() > 0)
    {
        $date_package =$user_package->created_at;
        $products = Product::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
        $quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
        $quote = Quote::whereFrom_user_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
    }//end has products
}//end user_package
else
    $user_package = 0; 
?>
<!---->
<aside class=" left-op-col" style="padding: 1px;">
      <header style="text-align: center;">

         <h4>{{trans('dash.account_overview')}}</h4>
        @if($user_profile)
        @if($user_profile->co_logo)
        <a href="#myModal2" data-toggle="modal"><img src="{{asset('uploads/'.$user_profile->co_logo)}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo"></a>
        @else
        <a href="#myModal2" data-toggle="modal"><img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo"></a>
        @endif
        <legend style="color:#00509f;">{{$user_profile->company_name}}</legend>
        @else
        <img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        @endif

        @if($user_package)
        <p style="font-size: 15px;">{{trans('dash.package_in')}}<span style="color:#00509f;font-weight:bold;font-size: 20px;"><br>
        <a href="{{url('upgrade')}}" style="color:#00509f;font-weight:bold;font-size: 20px;">  {{$packages->name_ar}}</a></span><br></p>
        <!--package detials-->
        @if($user_package)
        <p>{{trans('dash.account_type')}} :  <span style="font-size: 15px;">  @if($packages->brief_en=="The Free Package")
        مجاني
        @else
        مدفوع
        @endif</span></p>
         @if($packages->brief_en!="The Free Package")
        <p>{{trans('dash.start_date')}} : <span style="font-size: 15px;">  {{date('d-m-Y',strtotime($user_package->updated_at))}}</span></p>
        <input type="hidden" name="" value="{{$end_date = strtotime('+ 1 year', strtotime($user_package->updated_at))}}">
        <p>{{trans('dash.end_time')}} : <span style="font-size: 15px;">  {{date('d-m-Y', $end_date)}}</span></p>
        @endif
        @endif
        <div class="p-txt" style="margin: 5px;padding: 0px;"></div>
        <p>{{trans('dash.reg_date')}} : <span style="font-size: 15px;">  {{date('d-m-Y', strtotime($user->created_at))}}</span></p>
        <p>{{trans('dash.perc_reg')}} </p>
        <div class="progress" >
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="40" id="prog_bar_reg"
        aria-valuemin="0" aria-valuemax="100" style="width:{{($template_steps->personal_profile+$template_steps->add_products+$template_steps->select_template+$template_steps->completed)/4*100}}%;background-color:#28a745;height: 15px;border-radius: 15px;">{{($template_steps->personal_profile+$template_steps->add_products+$template_steps->select_template+$template_steps->completed)/4*100}}%
        </div>
      </div>
        @endif

        <!--end package detials-->

        <a href="{{url('upgrade')}}">
        <button type="button" class="btn" style="background-color: #00509f;color: white;">{{trans('dash.upgrade_account')}}</button>
        </a>
      </header>
      <!--Dashboard icons-->
         <a href="{{url('dashboard')}}"><button class="accordion_dashboard" style="margin-bottom: 15px;margin-top: 10px;">{{trans('dash.dashboard')}}<img  src="{{asset('uploads/control_dash.png')}}" class="img_icon_dash"></button></a>
        
              @if($user->importer)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
              @if($user->exporter)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @endif
              @if($user->service_provider)
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
            <!--Accordians-->
           <!--accordian element-->
           @if(isset(App\Menu::$dashboard_main['dashboard/messages']))
            <button class="accordion mess" >{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/messages']['label']))}}
              <img  src="{{asset('uploads/cmps-btn-bg-50.png')}}" class="img_icon_dash"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/messages/inbox')}}" class="acc_elem inbx"> <img  src="{{asset('uploads/icons/inbx-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.inbox')}}</a>
                    <a href="{{url('dashboard/messages/draft')}}" class="acc_elem drft"> <img  src="{{asset('uploads/icons/drfts-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.draft')}}</a>
                    <a href="{{url('dashboard/messages/compose')}}" class="acc_elem cmps"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.compose')}}</a>
                    <a href="{{url('dashboard/messages/sent')}}" class="acc_elem sen"> <img  src="{{asset('uploads/icons/rmndr-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.sent')}}</a>
                    <a href="{{url('dashboard/contacts')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/contcs-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.my_contacts')}}</a>
                    <!-- <a href="{{url('dashboard/messages/inbox')}}" class="inbx-btn">{{trans('dash.inbox')}}</a>
                    <a href="{{url('dashboard/messages/draft')}}" class="drfts-btn">{{trans('dash.draft')}}</a>
                    <a href="{{url('dashboard/messages/compose')}}" class="cmps-btn">{{trans('dash.compose')}}</a>
                    <a href="{{url('dashboard/messages/sent')}}" class="rmndr-btn">{{trans('dash.sent')}}</a>
                    <a href="{{url('dashboard/contacts')}}" class="contcs-btn">{{trans('dash.my_contacts')}}</a> -->
            </div>
            </div>
            @endif
            @if(isset(App\Menu::$dashboard_main['dashboard/profile']))
            <button class="accordion prof">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/profile']['label']))}}<img  src="{{asset('uploads/bsnsprfl-btn-bg-32.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                  <a href="{{url('dashboard/profiles/personal')}}" class="acc_elem prof pers"> <img  src="{{asset('uploads/icons/pofl-tab-bg-50.png')}}" class="img_icon_dash">{{trans('dash.personal_profile')}}</a>
                  <a href="{{url('dashboard/profiles/company')}}" class="acc_elem prof cmp"> <img  src="{{asset('uploads/icons/pofl-tab-bg-50.png')}}" class="img_icon_dash">{{trans('dash.company_profile')}}</a>
                  <a  href="{{url('dashboard/quotations-send')  }}" class="acc_elem off"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.offers_opp')}}</a>
                  <!-- <a href="{{url('dashboard/profile/trade')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/trdinfo-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.trade_information')}}</a>
                  <a href="{{url('dashboard/profile/factory')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.factory_information')}}</a>
                  <a href="{{url('dashboard/profile/registration')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/rstdtls-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.registration_details')}}</a> -->
                  
                    <!-- <a href="{{url('dashboard/profile')}}" class="bsnsprfl-btn">{{trans('dash.my_profile')}}</a>
                    <a href="{{url('dashboard/profile/trade')}}" class="trdinfo-btn">{{trans('dash.trade_information')}}</a>
                    <a href="{{url('dashboard/profile/factory')}}" class="facinfo-btn">{{trans('dash.factory_information')}}</a>
                    <a href="{{url('dashboard/profile/registration')}}" class="rstdtls-btn">{{trans('dash.registration_details')}}</a> -->
            </div>
            </div>
            @endif
              <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/products']))
            <button class="accordion prod">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/products']['label']))}}<img  src="{{asset('uploads/prdcts-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/products/create')}}" class="acc_elem cre"> <img  src="{{asset('uploads/icons/adprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.add_product')}}</a>
                    <a href="{{url('dashboard/products')}}" class="acc_elem mng"> <img  src="{{asset('uploads/icons/mngprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_products')}}</a>
                    <a href="{{url('dashboard/media')}}" class="acc_elem md"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.media')}}</a>

                    <!-- <a href="{{url('dashboard/products/create')}}" class="adprdct-btn">{{trans('dash.add_product')}}</a>
                    <a href="{{url('dashboard/products')}}" class="mngprdct-btn">{{trans('dash.manage_products')}}</a> -->
                    <!-- <a href="{{url('dashboard/groups')}}" class="mnggrp-btn">{{trans('dash.groups')}}</a> -->
                    <!-- <a href="{{url('dashboard/media')}}" class="mdybnk-btn">{{trans('dash.media')}}</a> -->
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/services']))
            <button class="accordion serv">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/services']['label']))}}<img  src="{{asset('uploads/ppp-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/services/create')}}" class="acc_elem cre_serv"> <img  src="{{asset('uploads/icons/adprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.add_service')}}</a>
                    <a href="{{url('dashboard/services')}}" class="acc_elem mng_serv"> <img  src="{{asset('uploads/icons/mngprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_services')}}</a>


                    <!-- <a href="{{url('dashboard/services/create')}}" class="adprdct-btn">{{trans('dash.add_service')}}</a>
                    <a href="{{url('dashboard/services')}}" class="mngprdct-btn">{{trans('dash.manage_services')}}</a> -->
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/deals']))
            <button class="accordion deal">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/deals']['label']))}}<img  src="{{asset('uploads/mngmydls-btn-bg-32.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                       <a href="{{url('dashboard/sell_offers/create')}}" class="acc_elem cre_sell"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.sell_offers')}}</a>
                        <a href="{{url('dashboard/request_quotes/create')}}" class="acc_elem cre_req"> <img  src="{{asset('uploads/icons/wbsits-tb-bg.png')}}" class="img_icon_dash">{{trans('dash.request_for_quote')}}</a>
                         <a href="{{url('dashboard/partnerships/create')}}" class="acc_elem cre_part"> <img  src="{{asset('uploads/icons/prtnshp-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.partnership_deals')}}</a>
                          <a href="{{url('dashboard/deals')}}" class="acc_elem deal"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_my_deals')}}</a>
                           <a href="{{url('dashboard/alerts/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/crtdlsalrt-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.create_deal_alert')}}</a>
                            <!-- <a href="#d" class="mdybnk-btn">{{trans('dash.manage_deal_alerts')}}</a> -->

                     <!-- <a href="{{url('dashboard/sell_offers/create')}}" class="spcilofr-btn">{{trans('dash.sell_offers')}}</a>
                      <a href="{{url('dashboard/request_quotes/create')}}" class="rqstqot-btn">{{trans('dash.request_for_quote')}}</a>
                      <a href="{{url('dashboard/partnerships/create')}}" class="prtnshp-btn">{{trans('dash.partnership_deals')}}</a>
                      <a href="{{url('dashboard/deals')}}" class="mngmydls-btn">{{trans('dash.manage_my_deals')}}</a>
                      <a href="{{url('dashboard/alerts/create')}}" class="crtdlsalrt-btn">{{trans('dash.create_deal_alert')}}</a>
                      <a href="#d" class="mdybnk-btn">{{trans('dash.manage_deal_alerts')}}</a> -->
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/website']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/website']['label']))}}<img  src="{{asset('uploads/ws-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                      <a href="#myModal" data-toggle="modal" class="acc_elem"> <img  src="{{asset('uploads/icons/qiklunch-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.quick_launch')}}</a>
                       @if($my_website)
                        <a href="{{url('store/'.$my_website->domain)}}" class="acc_elem"> <img  src="{{asset('uploads/icons/qiklunch-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.website')}}</a>
                        @endif
                     <!-- <a class="qiklunch-btn" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">{{trans('dash.quick_launch')}}</a> -->
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/settings']))
            <button class="accordion sett">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/settings']['label']))}}<img  src="{{asset('uploads/stngs-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                     <a href="{{url('dashboard/settings')}}" class="acc_elem set"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.account_settings')}}</a>
                     <a href="{{url('dashboard/settings/account-verification')}}" class="acc_elem acc_ver" > <img  src="{{asset('uploads/icons/wbsits-tb-bg.png')}}" class="img_icon_dash">{{trans('dash.account_verification')}}</a>
                     <a href="{{url('dashboard/reviews')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/prtnshp-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.reviews')}}</a>

                     <!-- <a href="{{url('dashboard/settings')}}" class="spcilofr-btn">{{trans('dash.account_settings')}}</a>
                      <a href="{{url('dashboard/settings/account-verification')}}" class="rqstqot-btn">{{trans('dash.account_verification')}}</a>
                      <a href="{{url('dashboard/reviews')}}" class="prtnshp-btn">{{trans('dash.reviews')}}</a> -->
            </div>
            </div>
            @endif
            <!--End Accordian-->
      <!---->
      <div class="p-txt" >
         <h5><a href="{{url('dashboard/support')}}">{{trans('dash.help_us')}}</a></h5>
         <header style="text-align: center;">
            <img src="{{asset('uploads/call.png')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        </header>
         <button onclick="send_call()" type="button" class="call_bt btn col-xs-12 col-sm-12 col-md-12 col-lg-12 left-op-col" style="background-color: #00509f;color: white;" >{{trans('front.contact_us')}}</button>
         <p>{{trans('dash.have_trouble')}}</p>
      </div>
  </aside>
   <!---===========================================================================--->
      <!-- Modal My Website-->
      <div id="myModal" class="modal fade" role="dialog" style="text-align: center;">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              <?php $data = ['store_name' => $my_website->domain ]?>
              @include('dashboard.website.update_website')
            </div>
          </div>

        </div>
      </div>
      <!---===========================================================================--->
      <!---===========================================================================--->
      <!-- Modal My Website-->
      <div id="myModal2" class="modal fade" role="dialog" style="text-align: center;">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              {!!Form::model($user_profile, [
                'class'=>'form-horizontal',
                'url'=>'dashboard/profile',
                'method' => 'POST',
                'files'=>true
              ])!!}
              <!---->
              <?php $field = 'co_logo'; ?>
                <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
                    {!! Form::label($field, trans('dash.company_logo'), ['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-8">
                        <div class="media">
                            <?php $filepath = (isset($user_profile))? 'uploads/'.$user_profile->co_logo:'frontend/images/no_thumb.jpg';?>
                            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield2'])!!}</a>
                            <div class="media-body">
                                <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'2'])!!}</h4>
                                <div class="help-block">{{trans('dash.imgrules')}}</div>
                            </div>
                        </div>
                        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                {!!Form::submit(trans('dash.profile_submit'),['class'=>'btn btn-primary btn_submit','name'=>'btn_submit'])!!}
                {!!Form::close()!!} 
                <!---->
            </div>
          </div>

        </div>
      </div>
      <!---===========================================================================--->