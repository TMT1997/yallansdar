<!doctype html>
<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | @lang('dash.yalla_dash')</title>
    <meta name="description" content="{{trans('front.about_yallansadar')}}" />
    {{HTML::style('frontend/css/bootstrap.min.css')}}
    {{HTML::style('dash/css/bootstrap.min.css')}}
    {{HTML::style('frontend/css/font-awesome.min.css')}}
    {{HTML::style('frontend/css/dashboard/dashboard.css')}}
    {{HTML::style('frontend/css/jquery-ui.min.css')}}
    {{HTML::style('dash/css/amaran.min.css')}}
    {{HTML::style('backend/plugins/select2/select2.min.css')}}
    {{HTML::style('backend/plugins/select2/select2-bootstrap.min.css')}}
    {{HTML::style('dash/packages/switch/switch.min.css')}}
    {{HTML::style('dash/packages/fileUpload/css/jquery.fileupload.css')}}
    {{HTML::style('frontend/css/dashboard/style.css')}}
    {{HTML::style('frontend/css/dashboard/registeration.css')}}
    @if(App::isLocale('ar'))
        {!!HTML::style('backend/dist/css/bootstrap-rtl.min.css')!!}
        {!!HTML::style('frontend/css/dashboard/rtl.css')!!}
    @endif
    <script type="text/javascript">
        var catSearchURL = '{{url('dashboard/settings/category-search')}}';
        var $no_thumb_url = '{{url('frontend/images/no_thumb.jpg')}}';
        var $addGroupRoute = '{{route('dashboard.groups.store') }}';
        var $switch_bool_url = '{{url('admin/settings/switch-boolean')}}';
        var $datatable_config = {
            "language": {
                "sProcessing":   "@lang('dash.sProcessing')",
                "sLengthMenu":   "@lang('dash.sLengthMenu')",
                "sZeroRecords":  "@lang('dash.sZeroRecords')",
                "sInfo":         "@lang('dash.sInfo')",
                "sInfoEmpty":    "@lang('dash.sInfoEmpty')",
                "sInfoFiltered": "@lang('dash.sInfoFiltered')",
                "sInfoPostFix":  "",
                "sSearch":       "@lang('dash.sSearch')",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "@lang('dash.sFirst')",
                    "sPrevious": "@lang('dash.sPrevious')",
                    "sNext":     "@lang('dash.sNext')",
                    "sLast":     "@lang('dash.sLast')"
                }
            },
            "lengthMenu": [[5,10,25,50,-1], [5,10,25,50,"الكل"]],
            "aaSorting": [],
            buttons: [
                'selectAll',
                'selectNone'
            ]
        };
        var $datatable2_config = {
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aaSorting": []
        }
    </script>
</head><body>
<div class="container-fluid" style="margin-top:8px; border-bottom: 1px solid #f0f0f0">
    <nav class="navbar navbar-default navbar_top" role="navigation" style="min-height: auto; padding:1px 0; margin-bottom: 10px;">
        <ul class="collapse navbar-collapse nav navbar-nav navbar-left">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{Auth::user()->name}} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('dashboard/profile')}}"><i class="fa fa-user"></i> {{trans('dash.my_profile')}}</a></li>
                    <li><a href="{{url('dashboard/settings')}}"><i class="fa fa-gear"></i> {{trans('dash.settings')}}</a></li>
                    <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> {{trans('front.logout')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="#" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                    @foreach ($languages as $lang)
                        <?php $sel = (Session::get('yalla_locale')==$lang->iso2) ? 'selected="selected"':''; ?>
                        <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                    @endforeach
                </ul>
            </li>
            {{-- <li><a href="{{url('home')}}" target="_blank"><i class="fa fa-rocket"></i> {{trans('dash.goto_website')}}</a></li> --}}
        </ul>
        <ul class="collapse navbar-collapse nav navbar-nav navbar-right">
            <li>
                <a href="#" id="buy-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('dash.buy')}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                    <li><a href="{{url('dashboard/request_quotes/create')}}">{{trans('front.post_buying_req')}}</a></li>
                    <li><a href="{{url('dashboard/alerts')}}">{{trans('front.create_deal_alert')}}</a></li>
                    <li><a href="{{url('category')}}" target="_blank">{{trans('front.find_products')}}</a></li>
                    <li><a href="{{url('deals')}}" target="_blank">{{trans('front.find_new_deals')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="#" id="sell-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('dash.sell')}} <span class="caret"></span></a>
                <ul class="dropdown-menu" aria-labelledby="sell-drp-mnu">
                    <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.new_rfqs')</a></li>
                    <li><a href="{{url('dashboard/products')}}">@lang('front.add_products')</a></li>
                    <li><a href="{{url('dashboard/sell_offers/create')}}">@lang('front.post_sell_offer')</a></li>
                    <li><a href="{{url('dashboard/partnerships/create')}}">@lang('front.post_partnership_deal')</a></li>
                </ul>
            </li>
            <li>
                @if($unread_messages->count()>0)
                    <a href="#" id="sell-drp-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('front.mymessages')}} <span class="label label-danger">{{$unread_messages->count()}}</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu dash_messages_list">
                        @foreach($unread_messages as $msg)
                            <li><a href="{{App\Messages\Message::url($msg)}}">
                                    <span>{{App\Messages\Message::since($msg)}}</span><br> {{$msg->subject}}</a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <a href="{{url('dashboard/messages')}}">@lang('dash.view_all')</a>
                @endif
            </li>
            <li><a href="{{url('dashboard/favorites')}}">@lang('front.myfavorites')</a></li>
        </ul>
    </nav>
</div>
<main>
    <div class="container-fluid">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header text-center">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar',['class'=>'navbar-brand'])}}</a>
                <div>
                    <a href="{{url('dashboard')}}" style="color:#F8A605">{{trans('dash.dashboard')}}</a>
                </div>
            </div>
            @if(isset($hide_menu) && $hide_menu)
            @else
                <div class="collapse navbar-collapse navbar-ex1-collapse" id="menu_nav">
                    <ul class="nav navbar-nav">
                        @foreach(App\Menu::$dashboard_main as $url=>$item)
                            <?php
                            $active = ($url==Request::path())?'active':'';
                            $nav = (@$item['disabled'])? 'javascript:void(0)' : url($url);
                            $disabled = (@$item['disabled'])?'disabled':'';
                            $class = (@$item['disabled'])?'disabledMenuItem':'';
                            ?>
                            <li class="{{$item['class']}} {{$class}} {{$active}}"><a href="{{$nav}}" class="{{$disabled}}">{{trans('dash.'.strtolower($item['label']))}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </nav>
        <div class="row">
            <div class="col-md-12">
                {{--Delete خطوات العمل--}}
                {{--@include('dashboard.steps.bar')--}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('flash_message'))
                    <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!!Session::get('flash_message')!!}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            @if($reject_reasons && $reject_reasons->count()>0)
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <ul class="reject_items">
                            @foreach($reject_reasons as $res)
                                <il class="res_{{$res->id}}">تم رفض المنتج <strong>"{{App\Helper::field($res->product, 'name')}}"</strong>, وذلك بسبب
                                    <strong>{{App\Helper::field($res->reason, 'title')}}</strong>
                                    <a href="#" data-id="{{$res->id}}" class="btn btn-xs btn-default btn_read_reason">@lang('dash.ok_read')</a>
                                </il>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            @if($side_menu)
                <aside class="col-md-2 col-lg-2">
                    @include('dashboard.layouts.sidebar')
                </aside>
            @endif
            <div class="main_panel col-md-{{($side_menu)?10:12}}">
                @yield('content')
            </div>
        </div>
    </div>
</main>
<footer class="main-ftr">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ftr-mnu">
        <ul class="ftr-ul">
            <li><a href="{{url('site')}}" target="_blank">{{trans('front.get_to_know')}}</a></li>
            <li><a href="{{url('site/join-us')}}">{{trans('front.join_us')}}</a></li>
            <li><a href="{{url('testimontials')}}">{{trans('front.feedback')}}</a></li>
            <li><a href="{{url('site/contact')}}">{{trans('front.contact_us')}}</a></li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ftr-cpy-rths">
        <ul class="ftr-ul">
            <li>{{trans('front.copyrights')}}</li>
            <li><a href="#d">{{trans('front.terms')}}</a></li>
            <li><a href="#d">{{trans('front.privacy_policy')}}</a></li>
        </ul>
    </div>
</footer>
</main>
{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
{!!HTML::script('frontend/js/jquery-ui.min.js')!!}
{!!HTML::script('frontend/js/bootstrap.min.js')!!}
{!!HTML::script('frontend/js/registerationjs.js')!!}
{!!HTML::script('frontend/js/jquery.dataTables.min.js')!!}
{!!HTML::script('frontend/js/dataTables.bootstrap.min.js')!!}
{!!HTML::script('backend/plugins/select2/select2.min.js')!!}
{!!HTML::script('backend/ckeditor/ckeditor.js')!!}
{!!HTML::script('dash/js/jquery.amaran.min.js')!!}
{!!HTML::script('dash/packages/switch/switch.min.js')!!}
{!!HTML::script('dash/packages/fileUpload/js/jquery.fileupload.js')!!}
{!!HTML::script('dash/packages/validator/jquery.validate.min.js')!!}
{!!HTML::script('dash/packages/validator/additional-methods.min.js')!!}
<script>
    var csrf_token = '{{csrf_token()}}';
    $('.switch').bootstrapSwitch();
    $(function () {
        if($("#editor").length>0) {
            CKEDITOR.replace('editor',{
                toolbar : 'Full'
            });
        }
        if($("#editor1").length>0) {
            CKEDITOR.replace('editor1');
        }
        if($("#editor2").length>0) {
            CKEDITOR.replace('editor2');
        }
        if($('#editor_ar').length) {
            CKEDITOR.replace('editor_ar',{
                toolbar : 'Full',
                filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
            });
        }
        if($('#editor_en').length) {
            CKEDITOR.replace('editor_en',{
                toolbar : 'Full',
                filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
            });
        }
    });
    $(function() {
        $( "#dash-brd-tbs" ).tabs();
    });
    $(function() {
        $(".datepicker").datepicker();
    });
    $(function() {
        $(".select2").select2({
            theme: "bootstrap",
            width: '100%'
        });
    });

    $(".lvl-one-mnu-itm ").hover(function() {
        $sideMnuWdth = $('.sid-mnu').outerWidth();
        console.log( $sideMnuWdth );
        $('ul.sub-lvl-two-itm').css('left', $sideMnuWdth-10);
        //$(".lvl-one-mnu-itm > a").css('opacity', 0.6);
        //$(this).find(' a ').css('opacity', 1);
    });
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    //M2: Preview Image browsed for upload
    function readURL(input, fID) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.img_preview.imgfield'+fID).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('body').on('change', '.img_browse', function(){
        var fid = $(this).data('fid');
        readURL(this, fid);
    });
    $('.btn-loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
    $('body').on('change', '.form-filter select', function(event) {
        event.preventDefault();
        $('.form-filter').submit();
    });

    $('body').on('click', '.btn_boolean', function(event) {
        event.preventDefault();
        if(window.confirm('{{trans('dash.change_state_confirm')}}'))
        {
            $id = $(this).data('id');
            $field = $(this).data('field');
            $model = $(this).data('model');
            $element = $('.elem'+$field+$id);
            $.ajax({
                url: '{{url('admin/settings/switch-boolean')}}',
                type: 'GET',
                data: {id: $id, model: $model, field: $field},
                beforeSend:function() {
                    $element.html('loading');
                }
            }).done(function(response) {
                $res = $.parseJSON(response);
                $elem_wrapper = $element.parent('td');
                $element.remove();
                if($res.status==1) {
                    if($res.newval==1) {
                        $color = 'success';
                        $icon = 'check-circle';
                    } else {
                        $color = 'danger';
                        $icon = 'minus-circle';
                    }

                    $elem_wrapper.html('<a href="#" class="text-'+$color+' btn_boolean elem'+$field+$id+'" data-id="'+$id+'" data-model="'+$model+'" data-field="'+$field+'"><i class="fa fa-2x fa-'+$icon+'"></i></a>');

                } else {
                    alert('خطا حدث في تغيير التفعيل');
                }
            }).fail(function() {
                console.log("خطا حدث في تغيير التفعيل");
            }).always(function() {
            });
        }
    });
    $(".dataTable").DataTable($datatable_config);
    $('.dataTable2').DataTable($datatable2_config);

    $('body').on('click', '.btn_quickAddContact', function(event) {
        event.preventDefault();
        quickAdd('{{route('dashboard.contacts.store')}}', '.contactsList', 'first_name');
    });

    function quickAdd(URL, selectObj, $display_val)
    {
        if(null==$display_val) {
            $display_val = 'name';
        }
        $.ajax({
            url: URL,
            data: $("#popup_form").serializeArray(),
            type:'POST',
            dataType:'json',
        }).done(function(output) {
            if (output.status == 'success') {
                var row = output.data;
                var display_name = row[$display_val];
                $(selectObj).append('<option value="'+row.id+'" selected="selected">'+display_name+'</option>');
                $.magnificPopup.close();
                notify('Record has been added successfully');
            } else {
                alert('Some errors happened');
            }
        }).fail(function() {
            notify('Error occured, please try again', 'error'); return false;
        }).always(function() {
            //$(".btnTarget").button('reset');
        });
    }
    function delete_img($obj, $model, $field) {
        var $id = $obj.data('id');
        if(window.confirm('Delete this file: Are you sure?')) {
            $.ajax({
                url: '{{url('dashboard/settings/deleteimage')}}?id='+$id+'&model='+$model+'&field='+$field,
                type: 'POST',
                data: {_token: csrf_token},
                beforeSend:function() {
                    $obj.append('<span class="del_load">deleting...</span>');
                }
            }).done(function(response) {
                output = jQuery.parseJSON(response);
                if(output.status=='success') {
                    $obj.parent().parent().parent('.infile').slideUp('400');
                } else {
                    alert('Error deleting file');
                }
            }).fail(function() {
                console.log("Error deleting file");
            }).always(function() {
                $obj.find('.del_load').remove();
            });
        }
    }
    function auth_check(response) {
        var status = response.status;
        if (status == 401) {
            window.location = "{{url('login?continue='.Request::fullUrl())}}"; return false;
            return;
        }
    }
    function notify($message, $status) {
        var $bgcolor = '#7BAF1E'; //green
        var $fontcolor = '#fff';
        var $ineffect = 'slideBottom';
        var $outeffect = 'slideBottom';

        if(null !== $status) {
            if($status=='error') {
                $bgcolor = '#C9302C';
            }
            if($status=='alert') {
                $bgcolor = '#F8A605';
            }
        }
        $.amaran({
            content:{bgcolor:$bgcolor, color:$fontcolor, message:$message},
            'inEffect':$ineffect, 'outEffect':$outeffect, theme:'colorful'
        });
    }
    $('body').on('click', '.btn-changepass', function(event) {
        event.preventDefault();
        var $btn = $(this);
        var oldpass = $('.oldpass');
        var newpass = $('.newpass');
        var confnewpass = $('.confnewpass');
        if(oldpass.val()=='') {
            notify('Please enter your current password', 'error'); oldpass.focus(); return false;
        }
        if(newpass.val()=='') {
            notify('Please enter your new password', 'error'); newpass.focus(); return false;
        }
        if(confnewpass.val()=='') {
            notify('Please enter your confirm password', 'error'); confnewpass.focus(); return false;
        }
        if(newpass.val() != confnewpass.val()) {
            notify('Password & confirm password doens\'t match', 'error'); confnewpass.focus(); return false;
        }
        $.ajax({
            url: '{{url('dashboard/profile/password')}}',
            type: 'POST',
            data: $(".changePassForm").serialize(),
            beforeSend:function() {
                $btn.button('loading');
            }
        }).done(function(response) {
            output = jQuery.parseJSON(response);
            $msg = output.message;
            if(output.status=='success') {
                notify($msg);
            } else if(output.status=='error') {
                notify('Error: '+$msg, 'error');
            } else {
                notify('Unknwon Error occured, <br>'+$msg, 'alert');
            }
        }).fail(function() {
            notify('Error changing Password', 'error');
        }).always(function() {
            $btn.button('reset');
        });
    });

    $('body').on('click', '.btn-changeemail', function(event) {
        event.preventDefault();
        var $btn = $(this);
        var newemail = $('.newemail');
        var pass = $('.pass');
        if(newemail.val()=='') {
            notify('Please enter your new email', 'error'); newemail.focus(); return false;
        }
        if(pass.val()=='') {
            notify('Please enter your password', 'error'); pass.focus(); return false;
        }
        $.ajax({
            url: '{{url('dashboard/profile/change-email')}}',
            type: 'POST',
            data: $(".changeEmailForm").serialize(),
            beforeSend:function() {
                $btn.button('loading');
            }
        }).done(function(response) {
            output = jQuery.parseJSON(response);
            $msg = output.message;
            if(output.status=='success') {
                notify($msg);
            } else if(output.status=='error') {
                notify('Error: '+$msg, 'error');
            } else {
                notify('Unknwon Error occured, <br>'+$msg, 'alert');
            }
        }).fail(function() {
            notify('Error changing Email', 'error');
        }).always(function() {
            $btn.button('reset');
        });
    });

    /*jslint unparam: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var upload_url = '{{url('dashboard/uploader')}}';
        $('.fileupload').fileupload({
            url: upload_url,
            dataType: 'json',
            done: function (e, data) {
                var action = $(this).data('action');
                var filesWrap = $(this).parent('.fileinput-button').siblings('.files');
                $.each(data.result.files, function (index, file) {
                    $('<p/>').html('<img src='+file.url+' width="90">').appendTo(filesWrap);
                    saveUploadedFile(file.full_name, action, csrf_token);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(this).parent('.fileinput-button').siblings('.progress').find('.progress-bar').css('width', progress + '%');
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
    function saveUploadedFile(fileName, actionStr, csrf_token) {
        $.ajax({
            url: '{{url("dashboard/uploader/serve")}}',
            type: 'POST',
            data: {_token: csrf_token, file: fileName, action: actionStr},
        }).done(function(response) {
            console.log(response);
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }
    jQuery.extend(jQuery.validator.messages, {
        @foreach(trans('dash.validations') as $key=>$valid_msg)
        '{{$key}}':'{{$valid_msg}}',
        @endforeach
    });
    $(document).on('click', '.btn_read_reason', function(event) {
        event.preventDefault();
        var $id = $(this).data('id');
        var $wrap = $('.res_'+$id);
        $.ajax({
            url: '{{url('dashboard/products/readreason')}}',
            type: 'GET',
            dataType: 'JSON',
            data: {id: $id},
            beforeSend: function() {
            }
        }).done(function(response) {
            if(response.status=='success') {
                $wrap.hide('slow', function() {
                });
            } else {
                alert('Error');
            }
        }).fail(function() {
            alert('Error');
        }).always(function() {
            console.log("complete");
        });

    });
</script>
@yield('script')
@yield('script2')
</body></html>