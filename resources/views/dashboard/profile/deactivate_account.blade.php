<div class="white-popup-block">
	<div class="row">
		<legend>Feedback</legend>
	    <div class="col-md-12"> 
	    	{!!Form::open(['url'=>'dashboard/profile/deactivate', 'class'=>'form-horizontal changeEmailForm'])!!}
	            <div class="form-group">
	                {!!Form::label('email', 'Reason for leaving', ['class'=>'control-label col-md-3'])!!}
	                <div class="col-md-9">
		                @foreach(App\User::$leaving_reasons as $item)
		                	<div class="help-block">
		                		<label>{!!Form::radio('reason', $item, false)!!} {{$item}}</label>
		                	</div>
		                @endforeach
	                </div>
	            </div>

	            <div class="form-group">
	                {!!Form::label('explanation', 'Please explain further', ['class'=>'control-label col-md-3'])!!}
	                <div class="col-md-9">
	                	{!!Form::textarea('explanation', null, ['class'=>'form-control', 'rows'=>3])!!}
	                </div>
	            </div>

	            <div class="form-group">
	                {!!Form::label('opt_out', 'Email Opt Out', ['class'=>'control-label col-md-3'])!!}
	                <div class="col-md-9">
	                	<label class="control-label">{!!Form::checkbox('opt_out', '', false)!!} Opt out of receiving future emails from Yalla Nsadar</label>
	                </div>
	            </div>

	            <div class="form-group">
	            	<div class="col-md-offset-3"> 
	                	{!!Form::submit('Deactivate Account', ['class'=>'btn btn-danger btn-deactivateAcc'])!!}
	                	<a href="javascript:$.magnificPopup.close();" class="btn btn-default">Cancel</a>
	                </div>
	            </div>
	        {!!Form::close()!!}
	    </div>
	</div>
</div>