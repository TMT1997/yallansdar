<?php use App\UserCompanyRegistration;
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
?>
@extends('dashboard.layouts.master_reg')
@section('content')
	
	<legend>{{trans('dash.registration_details')}}</legend>
	<div class="help-block">
		{{trans('dash.registration_details_desc')}}
	</div>

	{!!Form::model($user_company_reg, [
		'class'=>'form-horizontal',
		'url'=>'dashboard/profile/registration',
		'method' => 'POST',
	])!!}

	@foreach(trans('dash.reg_details_list') as $field=>$type)
		<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
		    {!! Form::label($field, $type, ['class' => 'col-md-3 control-label']) !!}
		    <div class="col-md-8">
		        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>$type]) !!}
		        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
		    </div>
		</div>
	@endforeach
			
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			{!!Form::submit(trans('dash.save'), ['class'=>'btn btn-info btn_save btn-loading', 'value'=>'btn_save'])!!}
			{!!Form::submit(trans('dash.profile_submit'), ['class'=>'btn btn-primary btn_submit btn-loading', 'value'=>'btn_submit'])!!}
			<div class="help-block">
				{{trans('dash.profile_save_note')}}
			</div>
		</div>
	</div>
{!!Form::close()!!}	

@endsection

@section('script')
@include('dashboard.layouts.popup')
@endsection