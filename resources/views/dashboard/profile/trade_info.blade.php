<?php 
	use App\Settings;
	use App\UserTradeInfo;
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
	$countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
@extends('dashboard.layouts.master_reg')
@section('content')
	<legend>@lang('dash.trading_info')</legend>
	<div class="help-block">@lang('dash.trading_info_desc')</div>

	{!!Form::model($user_trade_info, [
		'class'=>'form-horizontal',
		'url'=>'dashboard/profile/trade',
		'method' => 'POST',
		'id'=>'tradeInfoForm',
	])!!}

	<?php $field = 'export_percentage'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field,trans('dash.export_percentages'),null,['class'=>'form-control','required'=>'required']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'market_region'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.main_markets'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	    	<div class="row">
		    	<div class="region" style="margin-bottom: 20px;">
		    		@foreach(trans('dash.market_regions') as $key=>$item)
			        	<div class="col-md-3"><label>{!!Form::radio('market_region',$key,null,['class'=>'region','required'=>'required'])!!} {{$item}}</label></div>
			        @endforeach
		    	</div>
		   	</div>
	    	<div class="row">
		        <div class="opt_region" style="{{($user_trade_info->market_region!='Region')?'display:none;':''}}">
			        @foreach(trans('dash.regions') as $region)
			        	<div class="col-sm-3 col-md-3"><label>{!!Form::checkbox('main_markets[]', $region, null)!!} {{$region}}</label></div>
			        @endforeach
		        </div>
		    </div>
	        <div class="row">
		        <div class="opt_country" style="height: 200px;overflow:auto; {{($user_trade_info->market_region!='Country')?'display:none;':''}}">
			        @foreach($countries as $id=>$country)
			        	<div class="col-sm-3 col-md-3"><label>{!!Form::checkbox('region_countries[]', $id, null)!!} {{$country}}</label></div>
			        @endforeach
		        </div>
		    </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'nearest_port'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	    	<div class="row">
	    	@for($i=1;$i<=3;$i++)
	    		<div class="col-md-4"> 
	        		{!!Form::select($field.'['.$i.']',trans('dash.shipping_ports'),null,['class'=>'form-control'])!!}
	        	</div>
	        @endfor
	        </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<div class="form-group">
		<div class="col-md-3 control-label">{{trans('dash.avg_lead_time')}}</div>
	</div>
	<?php $field = 'avg_lead_time'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.sample_lead_time'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
	        {!! Form::select('avg_lead_time_dur', trans('dash.avg_lead_duration'), null, ['class'=>'form-control','required'=>'required']) !!}
	        {!!App\Helper::popover(trans('dash.sample_lead_time'),trans('dash.sample_lead_info'))!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'ord_delivery_time'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! Form::select('ord_delivery_time_dur', trans('dash.avg_lead_duration'), null, ['class'=>'form-control']) !!}
	        {!!App\Helper::popover(trans('dash.'.$field),trans('dash.delivery_time_info'))!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'overseas_office'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        @foreach(UserTradeInfo::$bool as $val=>$item)
	        	<label>{!!Form::radio('overseas_office', $val, null, [])!!} {{$item}}</label>
	        @endforeach
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<div class="well well-sm overseas_list" style="{{($user_trade_info->overseas_office==0)?'display:none;':''}}">
		<legend>{{trans('dash.overseas_office_addr')}}</legend>
		<div class="row">
			@for($i=1;$i<=2;$i++)
				<div class="col-md-6">
					<?php $field = 'office_name'.$i; ?>
					<div class="form-group">
					    {!! Form::label($field, trans('dash.office_name'), ['class' => 'col-md-3 control-label']) !!}
					    <div class="col-md-8 form-inline">
					        {!! Form::text($field, null, ['class' => 'form-control']) !!}
					    </div>
					</div>
					<?php $field = 'office_country'.$i; ?>
					<div class="form-group">
					    {!! Form::label($field, trans('dash.country'), ['class' => 'col-md-3 control-label']) !!}
					    <div class="col-md-8 form-inline">
					        {!! Form::select($field, $countries, null, ['class' => 'form-control']) !!}
					    </div>
					</div>
					<?php $field = 'office_city'.$i; ?>
					<div class="form-group">
					    {!! Form::label($field, trans('dash.city2'), ['class' => 'col-md-3 control-label']) !!}
					    <div class="col-md-8 form-inline">
					        {!! Form::text($field, null, ['class' => 'form-control']) !!}
					    </div>
					</div>
					<?php $field = 'office_zipcode'.$i; ?>
					<div class="form-group">
					    {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
					    <div class="col-md-8 form-inline">
					        {!! Form::text($field, null, ['class' => 'form-control']) !!}
					    </div>
					</div>
				</div>
			@endfor
		</div>
	</div>

	<?php $field = 'no_trade_staff'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!! Form::select($field, trans('dash.no_trade_staffs'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'minimum_order_value'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!! Form::select($field, trans('dash.minimum_order_values'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'shipment_mode'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
	        @foreach(UserTradeInfo::$shipment_mode as $val)
	        	<div class="col-md-3"><label>{!!Form::checkbox($field.'[]',$val,null)!!} {{trans('dash.'.$val)}}</label></div>
	        @endforeach
	        </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'accepted_delivery_terms'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
		        @foreach(UserTradeInfo::$delivery_terms as $item=>$label)
	            	<div class="col-md-3"><label title="{{$label}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$item}}</label></div>
	            @endforeach
            </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'accepted_payment_currency'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
	        @foreach(App\ProductDetails::$fob_price as $item=>$label)
	        	@if($item=='') <?php continue; ?> @endif
            	<div class="col-md-3"><label title="{{$item}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$label}}</label></div>
            @endforeach
            </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'accepted_payment_type'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.payment_terms'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
	        @foreach(trans('dash.payment_methods_list') as $item=>$label)
            	<div class="col-md-3"><label title="{{$item}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$label}}</label></div>
            @endforeach
            </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'payment_term'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'language_spoken'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
	        @foreach(trans('dash.languages_spoken') as $item=>$label)
            	<div class="col-md-3"><label title="{{$item}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$label}}</label></div>
            @endforeach
            </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			{!!Form::submit(trans('dash.save'), ['class'=>'btn btn-info btn_save'])!!}
			{!!Form::submit(trans('dash.profile_submit'), ['class'=>'btn btn-primary btn_submit'])!!}
			<div class="help-block">
				{{trans('dash.profile_save_note')}}
			</div>
		</div>
	</div>
  {!!Form::close()!!}
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		//$('input[type=radio][name=market_region]').trigger('change');
	});
	$('input[type=radio][name=overseas_office]').trigger('change');
	$('input[type=radio][name=market_region]').on('change', function(event) {
		event.preventDefault();
		var $region = $(this).val();
		$('.opt_country').hide();
		$('.opt_region').hide();

		if($region=='Country') {
			$('.opt_country').slideDown();
		}
		if($region=='Region') {
			$('.opt_region').slideDown();
		}
	});
	$('input[type=radio][name=overseas_office]').on('change', function(event) {
		event.preventDefault();
		var $val = $(this).val();
		if($val=='1') {
			$('.overseas_list').slideDown();
		} else {
			$('.overseas_list').slideUp();
		}
	});
	$('#tradeInfoForm').validate({
		rules: {
			'nearest_port[1]':'required'
		},
	});
</script>
@include('dashboard.layouts.popup')
@endsection