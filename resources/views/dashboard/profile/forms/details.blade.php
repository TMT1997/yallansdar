<?php $field = 'have_brand'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Having Own Brand'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <label>{!!Form::radio($field, 1, null, ['class'=>'rd_havebrand'])!!} {{trans('dash.yes')}}</label>
        <label>{!!Form::radio($field, 0, null, ['class'=>'rd_havebrand'])!!} {{trans('dash.no')}}</label>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'brands'; ?>
<div class="form-group brands {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.brands'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control','placeholder'=>trans('dash.brands')]) !!}
    </div>
</div>

<?php $field = 'awards'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Awards & recognition'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea($field, null, ['class'=>'form-control','rows'=>3,'placeholder'=>trans('dash.Awards & recognition')]) !!}
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i> <span>{{trans('dash.select_files')}}</span>
            <input class="fileupload" data-action='UserProfileDoc|awards|{{$user_profile->id}}|Profile' type="file" name="files[]" multiple>
        </span><br>
        <div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div id="files" class="files"></div>

        {!!App\UserProfileDoc::generate_preview($user_profile->docs, 'awards')!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
        <div class="help-block">{{trans('dash.imgdoc')}}</div>
    </div>
</div>

<?php $field = 'quality_description'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Quality & Certification'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea($field, null, ['class'=>'form-control','rows'=>3,'placeholder'=>trans('dash.details')]) !!}
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i> <span>{{trans('dash.select_files')}}</span>
            <input class="fileupload" data-action='UserProfileDoc|certification|{{$user_profile->id}}|Profile' type="file" name="files[]" multiple>
        </span><br>
        <div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div id="files" class="files"></div>
        {!!App\UserProfileDoc::generate_preview($user_profile->docs, 'certification')!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
        <div class="help-block">{{trans('dash.imgdoc')}}</div>
    </div>
</div>

<?php $field = 'video'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Business Videos'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control','placeholder'=>lab($field)]) !!}
        <div class="help-block">{{trans('dash.video_url_desc')}}</div> 
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_brochure'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Company Brochures'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!!Form::file($field,['class'=>'form-control'])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}

        @if(isset($user_profile) && !empty($user_profile->co_brochure))
            <a class="pull-right btn btn-info" href="{{url('uploads/'.$user_profile->co_brochure)}}">{{trans('dash.preview')}}</a>
        @endif
        <div class="help-block">{{trans('dash.docrules')}}</div>
    </div>
</div>

<?php $field = 'overview'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.overview'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea($field, null, ['class' => 'form-control','id'=>'editor']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

@foreach(App\UserProfile::$social as $field=>$label)
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text($field, null, ['class' => 'form-control','placeholder'=>lab($label)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
@endforeach