<?php
    use App\UserProfile;
    $countries = array_prepend($countries->toArray(), trans('dash.please_select'));
    $bus_categories = array_prepend($categories->lists($c_name,'id')->toArray(), trans('dash.please_select'),0);
?>

<div class="help-block">
    @lang('dash.sourcing_information_desc')
</div>

<?php $field = 'business_category'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.business_category'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $bus_categories, null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'product_we_source'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.products_we_source'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->product_we_source))
            @foreach($user_profile->product_we_source as $index=>$val)
            <div class="margbtn10">
                {!!Form::text('product_we_source[]', $val,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!}
                {!!Form::text('quantity[]',@$user_profile->quantity[$index],['class'=>'form-control short','placeholder'=>trans('dash.qty')])!!}
                {!!Form::select('qtyunit[]',trans('dash.units'),@$user_profile->qtyunit[$index],['class'=>'form-control'])!!}
                <span>({{trans('dash.yearly')}})</span>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
            </div>
            @endforeach
        @else
            {!!Form::text('product_we_source[]', null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!}
            {!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!}
            {!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!}
            <span>(Yearly)</span>
        @endif
        <a href="#" data-ft='{{$field}}' data-max='10' class="btn btn-success btn-sm add_new_field">+</a>
        <div class="more_fields"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'preferred_category'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.preferred_category'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!! Form::select($field, $bus_categories, null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
        {!!App\Helper::popover('Preferred Category','Choose category from where the suppliers of the product you source should connect with you.')!!}
    </div>
</div>

<?php $field = 'buying_frequency'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.buying_frequency'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.buying_frequencies'), null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'supplier_country_id1'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.preferred_supplier_location'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $countries, null, ['class' => 'form-control select2']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'supplier_country_id2'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $countries, null, ['class'=>'form-control select2']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'supplier_country_id3'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $countries, null, ['class' => 'form-control select2']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'preferred_supplier_type'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.preferred_supplier_type'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="row"> 
            @foreach(App\Messages\Contact::$business_type as $type=>$label)
                @if($type=='') <?php continue;?> @endif
            	<div class="col-md-4"><label>{!!Form::checkbox($field.'[]', $type, null)!!} {{trans('dash.business_types')[$type]}}</label></div>
            @endforeach
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>