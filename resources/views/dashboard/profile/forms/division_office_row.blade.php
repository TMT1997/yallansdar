<div class="well well-sm" id="officewrap_{{$office->id}}">
	{!!Form::hidden('divoffice[id][]', $office->id)!!}
	<legend>{{trans('dash.divisional_office_details')}}
		<div class="pull-left">
			<a href="#" class="btn btn-xs btn-danger delDivision" data-id="{{$office->id}}">{{trans('dash.delete')}}</a>
		</div>
	</legend>

	<?php $field = 'division'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::select('divoffice['.$field.'][]',trans('dash.divisions_type'),$office->$field,['class'=>'form-control input-sm'])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'contact_person'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::text('divoffice['.$field.'][]',$office->$field,['class'=>'form-control input-sm','placeholder'=>trans('dash.'.$field)])!!}
	        {!!$errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'address_line1'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.address'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', $office->$field, ['class' => 'form-control input-sm', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'address_line2'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', $office->$field, ['class' => 'form-control input-sm', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'country_id'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.country'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select('divoffice['.$field.'][]', $countries, $office->$field, ['class' => 'form-control input-sm']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'state'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', $office->$field, ['class' => 'form-control input-sm', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'city'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', $office->$field, ['class' => 'form-control input-sm', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'zipcode'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::text('divoffice['.$field.'][]',$office->$field,['class'=>'form-control input-sm','placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'email'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::text('divoffice['.$field.'][]',$office->$field,['class'=>'form-control input-sm','placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'telephone'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.phone'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[tel_code][]', $office->$field, ['class'=>'form-control short', 'placeholder'=>trans('dash.code')])!!}
	        {!!Form::text('divoffice['.$field.'][]', $office->$field, ['class'=>'form-control input-sm', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'mobile'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.mobile'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[mob_code][]', $office->$field, ['class'=>'form-control input-sm short', 'placeholder'=>trans('dash.code')])!!}
	        {!!Form::text('divoffice['.$field.'][]', $office->$field, ['class'=>'form-control input-sm', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'fax'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.fax'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[fax_code][]', $office->$field, ['class'=>'form-control input-sm short', 'placeholder'=>trans('dash.code')])!!}
	        {!!Form::text('divoffice['.$field.'][]', $office->$field, ['class'=>'form-control input-sm', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
</div>