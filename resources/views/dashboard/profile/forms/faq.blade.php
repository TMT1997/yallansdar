<?php $field = 'faq'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.FAQ'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea($field, null, ['class' => 'form-control','id'=>'editor2']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>