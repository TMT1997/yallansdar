<?php use App\Settings; 
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
?>
@extends('dashboard.layouts.master_reg')

@section('content')
	
	<legend>{{trans('dash.profile')}}</legend>
	<div class="help-block">
		{{trans('dash.profile_desc')}}
	</div>

	@if($user_profile->editable==0)
		<div class="alert alert-warning">{{trans('dash.profile_edit_prohibited')}}</div>
	@endif
	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">
			@foreach(App\UserProfile::$profile_tabs as $index=>$tab)
				@if($check_form == 0)
				<li role="presentation" class="{{($index==1)?'active':''}}">
					<a href="#n{{$index}}" aria-controls="n{{$index}}" role="tab" data-toggle="tab">
					{{trans('dash.personal_profile')}}</a>
				</li>
				@break
				@else
				<li role="presentation" class="{{($index==1)?'active':''}}">
					<a href="#n{{$index}}" aria-controls="n{{$index}}" role="tab" data-toggle="tab">
					{{trans('dash.company_profile')}}</a>
				</li>
				@break
				@endif
			@endforeach
		</ul>
	
		{!!Form::model($user_profile, [
			'class'=>'form-horizontal',
			'url'=>'dashboard/profile',
			'method' => 'POST',
			'files'=>true
		])!!}

		<div class="tab-content" style="padding-top: 20px;">
			@foreach(App\UserProfile::$profile_tabs as $index=>$tab)
			@if($check_form == 0)
				<div role="tabpanel" class="tab-pane {{($index==1)?'active':''}}" id="n{{$index}}">
					@include('dashboard.profile.forms.personal')
				</div>
				@break
				@else
				<div role="tabpanel" class="tab-pane {{($index==1)?'active':''}}" id="n{{$index}}">
					@include('dashboard.profile.forms.company')
				</div>
				@break
				@endif
			@endforeach
		</div>

		<div class="row" >
			<div class="col-md-offset-3 col-md-8" style="text-align: center;padding: 10px;">
				<!-- {!!Form::submit(trans('dash.save'),['class'=>'btn btn-info btn_save', 'name'=>'btn_save'])!!} -->
				{!!Form::submit(trans('dash.profile_submit'),['class'=>'btn btn-primary btn_submit','name'=>'btn_submit'])!!}
<!-- 				<div class="help-block">
					{{trans('dash.profile_save_note')}}
				</div> -->
			</div>
		</div>
		{!!Form::close()!!}	
	</div>

@endsection

@section('script')
@include('dashboard.layouts.popup')
<script type="text/javascript">
	function getTemplate($sec)
	{
		$output = '';
		if($sec=='products') {
			$output='<div class="frec">'+
	    	'{!!Form::text('main_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name'), 'required'=>'required'])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='oproducts') {
			$output='<div class="frec">'+
	    	'{!!Form::text('other_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='website') {
			$output='<div class="frec">'+
	    	'{!!Form::text('website[]',null,['class'=>'form-control','placeholder'=>trans('dash.website')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='product_we_source') {
	    	$output='<div class="frec">'+
	    	'{!!Form::text('product_we_source[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!} '+
	    	'{!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!} '+
	    	'{!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!} <span>({{trans("dash.yearly")}})</span>'+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
			if($sec=='telephone') {
	    	$output='<div class="frec">'+
	    	'{!!Form::number('tel_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::number('telephone[]', null,['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='mobile') {
	    	$output='<div class="frec">'+
	    	'{!!Form::number('mob_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::number('mobile[]', null,['class'=>'form-control','placeholder'=>trans('dash.mobile')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		return $output;
	}



	$(document).on('click', '.add_new_field', function(e){
	    e.preventDefault();
	    var $ft = $(this).data('ft');
	    var max_fields = $(this).data('max');
	    var count_childs = $(this).siblings('.more_fields').children('.frec').length;
	    count_childs = count_childs+1;
	    if(count_childs < max_fields){
	        $wrapper = $(this).siblings('.more_fields');
	        $wrapper.append(getTemplate($ft));
	    } else {
	        alert('Sorry, you cannot add more than '+max_fields+' inputs');
	    }
	});
	$(document).on("click",".remove_field", function(e){
	    e.preventDefault(); $(this).parent('div').remove();
	});

	$(document).on('click', '.add_div_office', function(event) {
		event.preventDefault();
		$('.div_offices').append($('.div_office_template').html());
		$('html, body').animate({
		    scrollTop: $(".div_offices").offset().top
		}, 1000);
	});

	$(document).on('click', '.sameContactInfo', function(event) {
		if($(this).prop('checked')) {
			$('input[name="contact_f_name"]').val($('input[name="first_name"]').val());
			$('input[name="contact_l_name"]').val($('input[name="last_name"]').val());
			$('input[name="co_address_line1"]').val($('input[name="address_line1"]').val());
			$('input[name="co_address_line2"]').val($('input[name="address_line2"]').val());
			$('input[name="co_state"]').val($('input[name="state"]').val());
			$('input[name="co_city"]').val($('input[name="city"]').val());
			$('input[name="co_zipcode"]').val($('input[name="zipcode"]').val());
		} else  {
			//do nothing
		}
	});


	$(document).on('click', '.delDivision', function(event) {
		event.preventDefault();
		var $id = $(this).data('id');
		if(window.confirm('Delete Division are you sure?'))
		{
			$.ajax({
				url: '{{url('dashboard/offices/destory')}}/'+$id,
				type: 'POST',
				data: {id: $id, '_token':'{{csrf_token()}}', _method:'DELETE'},
			})
			.done(function(response) {
				alert('Office deleted successfully');
				$(this).parents('#officewrap_'+$id).slideUp('400');
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});			
		}
	});

	function injectTrim(handler) {
	  return function (element, event) {
	    if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
	      element.value = $.trim(element.value);
	    }
	    return handler.call(this, element, event);
	  };
	}
	$('#profileForm').validate({
		onfocusout: injectTrim($.validator.defaults.onfocusout),
		rules: {
            "user_type":"required",
        },
        messages: {
            "user_type":"يرجى اختيار على اقل خيار واحد ليعبر عن طبيعة الحساب الخاص بك",
        }
	});
	$(document).on('change', '.co_nameper', function(event) {
		event.preventDefault();
		$('.co_nameco').val($(this).val());
	});

	$(document).on('change', '.co_nameco', function(event) {
		event.preventDefault();
		$('.co_nameper').val($(this).val());
	});
	$(document).on('click', '.rd_havebrand', function(event) {
		//event.preventDefault();
		if($(this).val()==1) {
			$('.brands').slideDown('fast', function() {
			});
		} else {
			$('.brands').slideUp('fast', function() {
			});
		}
	});
</script>
@endsection