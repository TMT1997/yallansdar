<?php 
	use App\Settings;
	use App\UserFactoryInfo;
	$countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
?>
@extends('dashboard.layouts.master_reg')
@section('content')
	
	<legend>{{trans('dash.factory_info')}}</legend>
	<div class="help-block">{{trans('dash.factory_info_desc')}}</div>
	
	{!!Form::model($user_factory_info, [
		'class'=>'form-horizontal',
		'url'=>'dashboard/profile/factory',
		'method' => 'POST',
	])!!}

	<?php $field = 'factory_location'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field, $countries, null, ['class' => 'form-control select2']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'state'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text($field, null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'city'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.city'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text($field, null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'factory_size'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field, trans('dash.factory_sizes'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'contract_manufacturings'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="row"> 
		        @foreach(UserFactoryInfo::$contract as $item=>$label)
	            	<div class="col-md-12"><label title="{{$item}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$label}}</label></div>
	            @endforeach
            </div>
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'qc_staff_no'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.qc_staff_no'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field, trans('dash.no_trade_staffs'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'rnd_staff_no'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field, trans('dash.no_trade_staffs'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'production_lines'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::number($field, null, ['class'=>'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'annual_output_value'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select($field, trans('dash.annual_output_values'), null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'annual_product_name'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.annual_prod_capactiy'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	    	<div class="basic_inputs">
	    		@if(!empty($user_factory_info->annual_product_name))
		    		@foreach($user_factory_info->annual_product_name as $index=>$name)
		    		<div class="margbtn10">
		    			{!!Form::text('annual_product_name[]',$name,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!}
				    	{!!Form::text('annual_qty[]', @$user_factory_info->annual_qty[$index], ['class'=>'form-control short', 'placeholder'=>trans('dash.qty')]) !!}
				        {!!Form::select('annual_unit[]', trans('dash.units'), @$user_factory_info->annual_unit[$index], ['class' => 'form-control']) !!}
				        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
			        </div>
			        @endforeach
		        @else
		        	{!!Form::text('annual_product_name[]',null,['class'=>'form-control', 'placeholder'=>trans('dash.product_name')]) !!}
			    	{!!Form::text('annual_qty[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.qty')]) !!}
			        {!!Form::select('annual_unit[]', trans('dash.units'), null, ['class' => 'form-control']) !!}
			     @endif
	        	<a href="javascript;" class="btn btn-success btn-sm add_more_field">+</a>
	        </div>
	        <div class="more_fields_wrap"></div>
	    </div>
	</div>

	<?php $field = 'infrastructure'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::textarea($field, null, ['class' => 'form-control', 'rows'=>4,'placeholder'=>trans('dash.infrastructure_desc')]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>

	<?php $field = 'factory_images'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, trans('dash.upload_factory_images'), ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        <span class="btn btn-success fileinput-button">
	            <i class="glyphicon glyphicon-plus"></i> <span>{{trans('dash.select_images')}}</span>
	            <input class="fileupload" data-action='UserProfileDoc|factory|{{$user_factory_info->id}}|Factory' type="file" name="files[]" multiple>
	        </span><br>
	        <div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div><div id="files" class="files"></div>

	        {!!App\UserProfileDoc::generate_preview($user_factory_info->docs, 'factory')!!}

	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	        <div class="help-block">{{trans('dash.imgrules')}}</div>
	    </div>
	</div>

	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			{!!Form::submit(trans('dash.save'), ['class'=>'btn btn-info btn_save btn-loading'])!!}
			{!!Form::submit(trans('dash.profile_submit'), ['class'=>'btn btn-primary btn_submit btn-loading'])!!}
			<div class="help-block">
				{{trans('dash.profile_save_note')}}
			</div>
		</div>
	</div>
{!!Form::close()!!}	
<style type="text/css">
	.margbtn10 {margin-bottom: 10px;}
</style>
@endsection

@section('script')
{!!HTML::script('frontend/js/app/profile.js')!!}
@include('dashboard.layouts.popup')

<script type="text/javascript">
	var max_fields      = 10;
	var wrapper         = $(".more_fields_wrap");
	var add_button      = $(".add_more_field");
	var x = 1;
	$('body').on('click', '.add_more_field', function(e){
	    e.preventDefault();
	    var $template = '<div class="margbtn10">'+
	    	'{!!Form::text('annual_product_name[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.product_name')]) !!} '+
	    	'{!!Form::text('annual_qty[]', null, ['class'=>'form-control short', 'placeholder'=>trans('dash.qty')]) !!} '+
	    	'{!!Form::select('annual_unit[]', trans('dash.units'), null, ['class' => 'form-control']) !!} '+
	    	'<a href="javascript;" class="btn btn-success btn-sm add_more_field">+</a> <a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
	    if(x < max_fields){
	        x++;
	        $(wrapper).append($template);
	    } else {
	        alert('Sorry, you cannot add more than '+max_fields+' inputs');
	    }
	});
	$(document).on("click",".remove_field", function(e){ //user click on remove text
	    e.preventDefault(); $(this).parent('div').remove(); x--;
	});
</script>

@endsection