<div class="white-popup-block">
	<div class="row">
		<legend>Change My Password</legend>
	    <div class="col-md-6 pull-left"> 
	    	{!!Form::open(['url'=>'dashboard/profile/password', 'class'=>'form-horizontal changePassForm'])!!}
	            <div class="form-group">
	                {!!Form::label('old_password', null, ['class'=>'control-label'])!!}
	                {!!Form::password('old_password', ['class'=>'form-control oldpass','autocomplete'=>'off'])!!}
	            </div>

	            <div class="form-group">
	                {!!Form::label('new_password', null, ['class'=>'control-label'])!!}
	                {!!Form::password('new_password', ['class'=>'form-control newpass'])!!}
	            </div>

	            <div class="form-group">
	                {!!Form::label('confirm_password', null, ['class'=>'control-label'])!!}
	                {!!Form::password('confirm_password', ['class'=>'form-control confnewpass'])!!}
	            </div>

	            <div class="form-group">
	                {!!Form::submit('Change my password', ['class'=>'btn btn-primary btn-changepass'])!!}
	                <a href="javascript:$.magnificPopup.close();" class="btn btn-default">Cancel</a>
	            </div>
	        {!!Form::close()!!}
	    </div>
	</div>
</div>