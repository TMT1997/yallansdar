<div class="white-popup-block">
	<div class="row">
		<legend>Change Email</legend>
	    <div class="col-md-6 pull-left"> 
	    	{!!Form::open(['url'=>'dashboard/profile/email', 'class'=>'form-horizontal changeEmailForm'])!!}
	            <div class="form-group">
	                {!!Form::label('email', 'Existing Primary Email', ['class'=>'control-label'])!!}
	                <a href="#">{{Auth::user()->email}}</a>
	            </div>

	            <div class="form-group">
	                {!!Form::label('new_email', null, ['class'=>'control-label'])!!}
	                {!!Form::text('new_email', null, ['class'=>'form-control newemail'])!!}
	                <div class="help-block">
	                	<label>{!!Form::checkbox('alter_email', 1, false)!!} Make existing email as secondary email</label>
	                </div>
	            </div>

	            <div class="form-group">
	                {!!Form::label('password', null, ['class'=>'control-label'])!!}
	                {!!Form::password('password', ['class'=>'form-control pass'])!!}
	            </div>

	            <div class="form-group">
	                {!!Form::submit('Submit', ['class'=>'btn btn-primary btn-changeemail'])!!}
	                <a href="javascript:$.magnificPopup.close();" class="btn btn-default">Cancel</a>
	            </div>
	        {!!Form::close()!!}
	    </div>
	</div>
</div>