<div class="white-popup-block">
    <legend>Review Details</legend>
    <table class="table table-striped table-hover table-bordered">
        <tbody>
            <tr>
                <td class="control-label">ID</td>
                <td>{{$review->id}}</td>
            </tr>
            <tr>
                <td class="control-label">Rate</td>
                <td>{{$review->rate}} of {{App\Review::$rate_max}}</td>
            </tr>
            
            @if($review->type=='Product' && isset($review->product))
            <tr>
                <td class="control-label">Product</td>
                <td>{!!App\Helper::image($review->product->image,['style'=>'width:70px'])!!} 
                {{$review->product->name}}</td>                
            </tr>
            @endif
            <tr>
                <td class="control-label">Date</td>
                <td>{{$review->created_at}} ({{Carbon\Carbon::parse($review->updated_at)->diffForHumans()}})</td>
            </tr>
            <tr>
                <td class="control-label">Subject</td>
                <td>{{$review->subject}}</td>
            </tr>
            <tr>
                <td class="control-label">Body</td>
                <td>{{$review->body}}</td>
            </tr>
        </tbody>
    </table>
</div>