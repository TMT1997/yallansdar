@extends('dashboard.layouts.master')

@section('content')

    <legend>{{trans('dash.reviews')}}</legend>
    <div class="help-block">{{trans('dash.reviews_intro')}}</div>

    <div role="tabpanel">
        <ul class="nav nav-tabs tab_filter" role="tablist">
            @foreach(App\Review::$direction as $state_id=>$status)
                <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('dash.'.$status)}}</a></li>
            @endforeach
        </ul>
        
        <div class="well well-sm">
            {!! Form::open(['method'=>'get','class'=>'form-inline filter_ajax']) !!}
                {!!App\Helper::select('type',App\Review::$types,[''=>trans('dash.filter_by_type')],Request::get('type'), ['id'=>'reviews_type'])!!}
            {!! Form::close() !!}
        </div>
        <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
    </div>        

@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.tab_filter a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var type = $('#reviews_type').val();
            var direction = $(this).data('sid');
            get_data(direction, type);
        });
        $(document).ready(function() {
            $('.tab_filter a[data-toggle="tab"]:first').trigger('click');
            $(document).on('change', '.filter_ajax select', function(event) {
                var type = $(this).val();
                var direction = $('.tab_filter li.active a').data('sid');
                get_data(direction, type);
            });
        });
        function get_data($direction, $type) {
            $.ajax({
                url: '{{url('dashboard/reviews/grid')}}?type='+$type+'&direction='+$direction,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('Loading Reviews...');    
                }
            }).done(function(response) {
                $('.data_grid').html(response);
            }).fail(function() {
                alert("error");
            }).always(function() {
            });
        }
    </script>
@endsection