<table class="table table-bordered table-striped table-hover dataTable">
    <thead>
        <tr>
            <th>{{trans('dash.id')}}</th>
            <th>{{trans('dash.name')}}</th>
            <th>{{trans('dash.rate')}}</th>
            <th>{{trans('dash.subject')}}</th>
            <th>{{trans('dash.brief')}}</th>
            <th>{{trans('dash.date')}}</th>
            <th>{{trans('dash.actions')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reviews as $item)
            <tr id="tr_{{$item->id}}">
                <td>{{ $item->id }}</td>
                <td>
                    @if($item->type=='Product' && isset($item->product))
                        {{$item->product->name}}
                    @endif
                </td>
                <td>{{$item->rate}} of {{App\Review::$rate_max}}</td>
                <td>{{$item->subject}}</td>
                <td>{{str_limit($item->body, 120)}}</td>
                <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
                <td>
                    <a href="{{url('dashboard/reviews/show/'.$item->id)}}" class="btn btn-primary btn-sm popup">{{trans('dash.show')}}</a>

                    {!!Form::open(['method'=>'DELETE','url'=>['dashboard/reviews',$item->id],'style'=>'display:inline'])!!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" title="Delete Category" />', array('type'=>'submit','class'=>'btn btn-danger btn-sm','title'=>'Delete File','onclick'=>'return confirm("Confirm delete?")')) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@if(isset($reviews) && $reviews->count()==0)
    <p class="text-center">
        No reviews here
    </p>
@endif
@include('dashboard.layouts.popup')