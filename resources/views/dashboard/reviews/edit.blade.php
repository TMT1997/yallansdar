@extends('dashboard.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Edit Media File: {{ $media->title }}</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($media, [
                'method' => 'PATCH',
                'url' => ['/dashboard/media', $media->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            @include ('dashboard.media.form', ['submitButtonText' => 'Save Changes'])

            {!! Form::close() !!}
        </div>
    </div>
@endsection