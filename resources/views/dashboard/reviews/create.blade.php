@extends('dashboard.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Add New Media File</div>
        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!!Form::open(['url'=>'/dashboard/media','class'=>'form-horizontal','files' => true]) !!}

            @include ('dashboard.media.form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection