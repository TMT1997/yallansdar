<?php use App\Messages\Contact; ?>
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', trans('dash.first_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('first_name', null, ['class'=>'form-control', 'placeholder'=>trans('dash.first_name')])!!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', trans('dash.last_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('last_name', null, ['class'=>'form-control', 'placeholder'=>trans('dash.last_name')])!!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', trans('dash.gender'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::select('gender', trans('dash.genders'), null, ['class'=>'form-control'])!!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('destination') ? 'has-error' : ''}}">
    {!! Form::label('destination', trans('dash.destination'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('destination', null, ['class'=>'form-control', 'placeholder'=>trans('dash.destination')])!!}
        {!! $errors->first('destination', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address_line1') ? 'has-error' : ''}}">
    {!! Form::label('address_line1', trans('dash.address'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('address_line1', null, ['class'=>'form-control', 'placeholder'=>trans('dash.address')])!!}
        {!! $errors->first('address_line1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address_line2') ? 'has-error' : ''}}">
    {!! Form::label('address_line2', ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('address_line2', null, ['class'=>'form-control', 'placeholder'=>trans('dash.address_line2')])!!}
        {!! $errors->first('address_line2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
    {!! Form::label('country_id', trans('dash.country'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::select('country_id', $countries, null, ['class'=>'form-control'])!!}
        {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', trans('dash.state'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('state', null, ['class'=>'form-control', 'placeholder'=>trans('dash.state')])!!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', trans('dash.city'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('city', null, ['class'=>'form-control', 'placeholder'=>trans('dash.city')])!!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('zipcode') ? 'has-error' : ''}}">
    {!! Form::label('zipcode', trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('zipcode', null, ['class'=>'form-control', 'placeholder'=>trans('dash.zipcode')])!!}
        {!! $errors->first('zipcode', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', trans('dash.email'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        {!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('dash.email')])!!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', trans('dash.phone'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6 form-inline">
        {!!Form::text('dailing_code', null, ['class'=>'form-control','placeholder'=>trans('dash.code'),'size'=>4])!!}
        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>trans('dash.phone')])!!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', trans('dash.mobile'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6 form-inline">
        {!!Form::text('dailing_code_m', null, ['class'=>'form-control','placeholder'=>trans('dash.code'),'size'=>4])!!}
        {!!Form::text('mobile', null, ['class'=>'form-control', 'placeholder'=>trans('dash.mobile')])!!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fax') ? 'has-error' : ''}}">
    {!! Form::label('fax', trans('dash.fax'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6 form-inline">
        {!!Form::text('dailing_code_f', null, ['class'=>'form-control','placeholder'=>trans('dash.code'),'size'=>4])!!}
        {!!Form::text('fax', null, ['class'=>'form-control', 'placeholder'=>trans('dash.fax')])!!}
        {!! $errors->first('fax', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">
    {!! Form::label('file', trans('dash.profile_picture'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath = (isset($media))? 'uploads/'.$media->file:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>            
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('file',['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
                <div class="help-block">{{trans('dash.imgrules')}}</div>
            </div>
        </div>
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="well well-sm">{{trans('dash.business_information')}}</div>
    <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
        {!! Form::label('company_name', trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::text('company_name', null, ['class'=>'form-control', 'placeholder'=>trans('dash.company_name')])!!}
            {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('business_type') ? 'has-error' : ''}}">
        {!! Form::label('business_type', trans('dash.business_type'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::select('business_type', trans('dash.business_types'), null, ['class'=>'form-control'])!!}
            {!! $errors->first('business_type', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('contact_type') ? 'has-error' : ''}}">
        {!! Form::label('contact_type', trans('dash.contact_is'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::select('contact_type',trans('dash.contact_types'), null, ['class'=>'form-control'])!!}
            {!! $errors->first('contact_type', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

<div class="well well-sm">{{trans('dash.other_information')}}</div>

    <div class="form-group {{ $errors->has('source') ? 'has-error' : ''}}">
        {!! Form::label('source', trans('dash.source'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::select('source', trans('dash.contact_source'), null, ['class'=>'form-control'])!!}
            {!! $errors->first('source', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('group_id') ? 'has-error' : ''}}">
        {!! Form::label('group_id', trans('dash.group'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::text('group_id', null, ['class'=>'form-control', 'placeholder'=>trans('dash.group')])!!}
            {!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
        {!! Form::label('facebook', trans('dash.facebook'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::text('facebook', null, ['class'=>'form-control', 'placeholder'=>trans('dash.facebook')])!!}
            {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('linkedin') ? 'has-error' : ''}}">
        {!! Form::label('linkedin', trans('dash.linkedin'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::text('linkedin', null, ['class'=>'form-control', 'placeholder'=>trans('dash.linkedin')])!!}
            {!! $errors->first('linkedin', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('skype') ? 'has-error' : ''}}">
        {!! Form::label('skype', trans('dash.skype'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::text('skype', null, ['class'=>'form-control', 'placeholder'=>trans('dash.skype')])!!}
            {!! $errors->first('skype', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('remarks') ? 'has-error' : ''}}">
        {!! Form::label('remarks', trans('dash.remarks'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!!Form::textarea('remarks', null, ['class'=>'form-control', 'placeholder'=>trans('dash.remarks')])!!}
            {!! $errors->first('remarks', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-4">
        {!!Form::submit(isset($submitButtonText) ? $submitButtonText : trans('dash.add_contact'), ['class' => 'btn btn-loading btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('dash.cancel')}}</a>
    </div>
</div>