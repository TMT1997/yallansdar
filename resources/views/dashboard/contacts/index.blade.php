<?php use App\Messages\Contact; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{trans('dash.contacts')}}
    <a href="{{ url('/dashboard/contacts/create') }}" class="btn btn-success pull-right">{{trans('dash.add_new_contact')}}</a>
    </div>
    <div class="panel-body">

        <div class="well well-sm">
            {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                {!!Form::select('group', $groups->lists('name','id'), null, ['class'=>'form-control'])!!}
            {!! Form::close() !!}
        </div>
            
        <div class="row">
            @foreach($contacts as $item)
                <div class="col-md-4">
                    <div class="well well-sm">
                        {{$item->first_name}} {{$item->last_name}} ({{@$countries[$item->country_id]}})
                        <br>
                        <a href="mailto:{{$item->email}}">{{$item->email}}</a>
                        <br>{{trans('dash.source')}}: {{$item->source}}
                        <br>
                        {!!Contact::flag($item)!!}
                        
                        @if(isset($item->user))
                            <a href="{{url('dashboard/messages/compose?to='.$item->id)}}" class="btn btn-sm btn-primary">{{trans('dash.send_message')}}</a>
                        @endif
                        <a href="{{ url('/dashboard/contacts/' . $item->id . '/edit') }}" class="btn btn-warning btn-sm" title="Edit Contact"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/dashboard/contacts', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Contact" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Contact',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection