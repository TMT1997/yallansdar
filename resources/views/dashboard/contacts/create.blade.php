@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Add New Contact</div>
        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!!Form::open(['url'=>'/dashboard/contacts','class'=>'form-horizontal','files' => true]) !!}

            @include ('dashboard.contacts.form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection