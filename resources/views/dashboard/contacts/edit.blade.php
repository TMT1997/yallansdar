@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{trans('dash.edit_contact')}}: {{ $contact->title }}</div>
        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($contact, [
                'method' => 'PATCH',
                'url' => ['/dashboard/contacts', $contact->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            @include ('dashboard.contacts.form', ['submitButtonText' => trans('dash.save_changes')])

            {!! Form::close() !!}
        </div>
    </div>
@endsection