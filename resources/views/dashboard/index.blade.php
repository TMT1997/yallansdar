<?php
  $hide_menu = true;
  $show_home_icon = true;
  use App\Helper;
  use App\Messages\Message;
  $category_name = App\Category::whereid($user_profile->other_products)->first();
?>
@extends('dashboard.layouts.master_reg')

@section('content')
<div class="row" style="padding: 15px;">
  <aside class="col-xs-12 col-sm-12 col-md-2 col-lg-2 left-op-col" style="padding: 1px;">
      <header style="text-align: center;">

        <h4>{{trans('dash.account_overview')}}</h4>
        @if($user_profile)
        @if($user_profile->co_logo)
        <a href="#myModal2" data-toggle="modal"><img src="{{asset('uploads/'.$user_profile->co_logo)}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo"></a>
        @else
        <a href="#myModal2" data-toggle="modal"><img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo"></a>
        @endif
        <legend style="color:#00509f;">{{$user_profile->company_name}}</legend>
        @else
        <img src="{{asset('uploads/company_logo.jpg')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        @endif

        @if($user_package)
        <p style="font-size: 15px;">{{trans('dash.package_in')}}<span style="color:#00509f;font-weight:bold;font-size: 20px;"><br>
        <a href="{{url('upgrade')}}" style="color:#00509f;font-weight:bold;font-size: 20px;">  {{$packages->name_ar}}</a></span><br></p>
        <!--package detials-->
        @if($user_package)
        <p>{{trans('dash.account_type')}} :  <span style="font-size: 15px;">  @if($packages->brief_en=="The Free Package")
        مجاني
        @else
        مدفوع
        @endif</span></p>
         @if($packages->brief_en!="The Free Package")
        <p>{{trans('dash.start_date')}} : <span style="font-size: 15px;">  {{date('d-m-Y',strtotime($user_package->updated_at))}}</span></p>
        <input type="hidden" name="" value="{{$end_date = strtotime('+ 1 year', strtotime($user_package->updated_at))}}">
        <p>{{trans('dash.end_time')}} : <span style="font-size: 15px;">  {{date('d-m-Y', $end_date)}}</span></p>
        @endif
        @endif
        <div class="p-txt" style="margin: 5px;padding: 0px;"></div>
        <p>{{trans('dash.reg_date')}} : <span style="font-size: 15px;">  {{date('d-m-Y', strtotime($user->created_at))}}</span></p>
        <p>{{trans('dash.perc_reg')}} </p>
        <div class="progress" >
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="40" id="prog_bar_reg"
        aria-valuemin="0" aria-valuemax="100" style="width:{{($template_steps->personal_profile+$template_steps->add_products+$template_steps->select_template+$template_steps->completed)/4*100}}%;background-color:#28a745;height: 15px;border-radius: 15px;">{{($template_steps->personal_profile+$template_steps->add_products+$template_steps->select_template+$template_steps->completed)/4*100}}%
        </div>
      </div>
        @endif

        <!--end package detials-->

        <a href="{{url('upgrade')}}">
        <button type="button" class="btn" style="background-color: #00509f;color: white;">{{trans('dash.upgrade_account')}}</button>
        </a>
      </header>
      <!--Dashboard icons-->
      <a href="{{url('dashboard')}}"><button class="accordion_dashboard" style="margin-bottom: 15px;margin-top: 10px;">{{trans('dash.dashboard')}}<img  src="{{asset('uploads/control_dash.png')}}" class="img_icon_dash"></button></a>
      
              @if($user->importer)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
              @if($user->exporter)
              @unset(App\Menu::$dashboard_main['dashboard/services'])
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @endif
              @if($user->service_provider)
              @unset(App\Menu::$dashboard_main['dashboard/deals'])
              @unset(App\Menu::$dashboard_main['dashboard/products'])
              @endif
            <!--Accordians-->
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/messages']))
            <button class="accordion mess">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/messages']['label']))}}
              <img  src="{{asset('uploads/cmps-btn-bg-50.png')}}" class="img_icon_dash"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/messages/inbox')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/inbx-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.inbox')}}</a>
                    <a href="{{url('dashboard/messages/draft')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/drfts-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.draft')}}</a>
                    <a href="{{url('dashboard/messages/compose')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.compose')}}</a>
                    <a href="{{url('dashboard/messages/sent')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/rmndr-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.sent')}}</a>
                    <a href="{{url('dashboard/contacts')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/contcs-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.my_contacts')}}</a>
                    <!-- <a href="{{url('dashboard/messages/inbox')}}" class="inbx-btn">{{trans('dash.inbox')}}</a>
                    <a href="{{url('dashboard/messages/draft')}}" class="drfts-btn">{{trans('dash.draft')}}</a>
                    <a href="{{url('dashboard/messages/compose')}}" class="cmps-btn">{{trans('dash.compose')}}</a>
                    <a href="{{url('dashboard/messages/sent')}}" class="rmndr-btn">{{trans('dash.sent')}}</a>
                    <a href="{{url('dashboard/contacts')}}" class="contcs-btn">{{trans('dash.my_contacts')}}</a> -->
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/profile']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/profile']['label']))}}<img  src="{{asset('uploads/bsnsprfl-btn-bg-32.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                  <a href="{{url('dashboard/profiles/personal')}}" class="acc_elem prof pers"> <img  src="{{asset('uploads/icons/pofl-tab-bg-50.png')}}" class="img_icon_dash">{{trans('dash.personal_profile')}}</a>
                  <a href="{{url('dashboard/profiles/company')}}" class="acc_elem prof cmp"> <img  src="{{asset('uploads/icons/pofl-tab-bg-50.png')}}" class="img_icon_dash">{{trans('dash.company_profile')}}</a>
                  <a  href="{{url('dashboard/quotations-send')  }}" class="acc_elem off"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.offers_opp')}}</a>
                  <!-- <a href="{{url('dashboard/profile/trade')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/trdinfo-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.trade_information')}}</a>
                  <a href="{{url('dashboard/profile/factory')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/cmps-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.factory_information')}}</a>
                  <a href="{{url('dashboard/profile/registration')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/rstdtls-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.registration_details')}}</a> -->

                    <!-- <a href="{{url('dashboard/profile')}}" class="bsnsprfl-btn">{{trans('dash.my_profile')}}</a>
                    <a href="{{url('dashboard/profile/trade')}}" class="trdinfo-btn">{{trans('dash.trade_information')}}</a>
                    <a href="{{url('dashboard/profile/factory')}}" class="facinfo-btn">{{trans('dash.factory_information')}}</a>
                    <a href="{{url('dashboard/profile/registration')}}" class="rstdtls-btn">{{trans('dash.registration_details')}}</a> -->
            </div>
            </div>
            @endif
              <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/products']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/products']['label']))}}<img  src="{{asset('uploads/prdcts-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/products/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/adprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.add_product')}}</a>
                    <a href="{{url('dashboard/products')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mngprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_products')}}</a>
                    <a href="{{url('dashboard/media')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.media')}}</a>

                    <!-- <a href="{{url('dashboard/products/create')}}" class="adprdct-btn">{{trans('dash.add_product')}}</a>
                    <a href="{{url('dashboard/products')}}" class="mngprdct-btn">{{trans('dash.manage_products')}}</a> -->
                    <!-- <a href="{{url('dashboard/groups')}}" class="mnggrp-btn">{{trans('dash.groups')}}</a> -->
                    <!-- <a href="{{url('dashboard/media')}}" class="mdybnk-btn">{{trans('dash.media')}}</a> -->
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/services']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/services']['label']))}}<img  src="{{asset('uploads/ppp-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="text-align: center;">
                    <a href="{{url('dashboard/services/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/adprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.add_service')}}</a>
                    <a href="{{url('dashboard/services')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mngprdct-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_services')}}</a>


                    <!-- <a href="{{url('dashboard/services/create')}}" class="adprdct-btn">{{trans('dash.add_service')}}</a>
                    <a href="{{url('dashboard/services')}}" class="mngprdct-btn">{{trans('dash.manage_services')}}</a> -->
            </div>
            </div>
            @endif
             <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/deals']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/deals']['label']))}}<img  src="{{asset('uploads/mngmydls-btn-bg-32.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                       <a href="{{url('dashboard/sell_offers/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.sell_offers')}}</a>
                        <a href="{{url('dashboard/request_quotes/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/wbsits-tb-bg.png')}}" class="img_icon_dash">{{trans('dash.request_for_quote')}}</a>
                         <a href="{{url('dashboard/partnerships/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/prtnshp-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.partnership_deals')}}</a>
                          <a href="{{url('dashboard/deals')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.manage_my_deals')}}</a>
                           <a href="{{url('dashboard/alerts/create')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/crtdlsalrt-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.create_deal_alert')}}</a>
                            <!-- <a href="#d" class="mdybnk-btn">{{trans('dash.manage_deal_alerts')}}</a> -->

                     <!-- <a href="{{url('dashboard/sell_offers/create')}}" class="spcilofr-btn">{{trans('dash.sell_offers')}}</a>
                      <a href="{{url('dashboard/request_quotes/create')}}" class="rqstqot-btn">{{trans('dash.request_for_quote')}}</a>
                      <a href="{{url('dashboard/partnerships/create')}}" class="prtnshp-btn">{{trans('dash.partnership_deals')}}</a>
                      <a href="{{url('dashboard/deals')}}" class="mngmydls-btn">{{trans('dash.manage_my_deals')}}</a>
                      <a href="{{url('dashboard/alerts/create')}}" class="crtdlsalrt-btn">{{trans('dash.create_deal_alert')}}</a>
                      <a href="#d" class="mdybnk-btn">{{trans('dash.manage_deal_alerts')}}</a> -->
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/website']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/website']['label']))}}<img  src="{{asset('uploads/ws-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                      <a href="#myModal" data-toggle="modal" class="acc_elem"> <img  src="{{asset('uploads/icons/qiklunch-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.quick_launch')}}</a>
                      @if($my_website)
                      <a href="{{url('store/'.$my_website->domain)}}" class="acc_elem"> <img  src="{{asset('uploads/icons/qiklunch-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.website')}}</a>
                      @endif
                     <!-- <a class="qiklunch-btn" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">{{trans('dash.quick_launch')}}</a> -->
            </div>
            </div>
            @endif
            <!--accordian element-->
            @if(isset(App\Menu::$dashboard_main['dashboard/settings']))
            <button class="accordion">{{trans('dash.'.strtolower(App\Menu::$dashboard_main['dashboard/settings']['label']))}}<img  src="{{asset('uploads/stngs-tab-bg-50.png')}}" style="float: left;width: 30px;height: 30px;"></button>
            <div class="panel_acc">
             <div class="col-md-12" style="padding: 0px;text-align: center;">
                     <a href="{{url('dashboard/settings')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/mdybnk-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.account_settings')}}</a>
                     <a href="{{url('dashboard/settings/account-verification')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/wbsits-tb-bg.png')}}" class="img_icon_dash">{{trans('dash.account_verification')}}</a>
                     <a href="{{url('dashboard/reviews')}}" class="acc_elem"> <img  src="{{asset('uploads/icons/prtnshp-btn-bg-32.png')}}" class="img_icon_dash">{{trans('dash.reviews')}}</a>

                     <!-- <a href="{{url('dashboard/settings')}}" class="spcilofr-btn">{{trans('dash.account_settings')}}</a>
                      <a href="{{url('dashboard/settings/account-verification')}}" class="rqstqot-btn">{{trans('dash.account_verification')}}</a>
                      <a href="{{url('dashboard/reviews')}}" class="prtnshp-btn">{{trans('dash.reviews')}}</a> -->
            </div>
            </div>
            @endif
            <!--End Accordian-->
      <!---->
      <div class="p-txt" >
         <h5><a href="{{url('dashboard/support')}}">{{trans('dash.help_us')}}</a></h5>
         <header style="text-align: center;">
            <img src="{{asset('uploads/call.png')}}" style="width: 200px auto;height: 100px;border-radius:50%;"alt="company_logo">
        </header>
         <button id="call" onclick="send_call()" type="button" class="call_bt btn col-xs-12 col-sm-12 col-md-12 col-lg-12 left-op-col" style="background-color: #00509f;color: white;" ><span id="call_txt">{{trans('front.contact_us')}}</span ></button>
         <p>{{trans('dash.have_trouble')}}</p>
      </div>
  </aside>
<!------------------------------------------------------------------------------->
  <legend>{{trans('dash.account_overview')}}</legend>
  <a href="{{url('upgrade')}}" target="_blank"><legend style="text-align: center;text-decoration: none;">{{trans('dash.upgrade_account')}}</legend></a>
   <div class="Box col-md-1 ">
   </div>
 <!------------------------------------------------------------------------------->
  <a style="color: #333333;cursor: pointer;" href="dashboard/products">
    <div class="Box col-md-2 " style="border: 1px solid #e7e7e7;padding: 15px;margin-left:2px;margin-top:2px;margin-bottom:2px;border-top: 2px solid #f79434;background-color: white;min-height: 290px;min-width: 200px;">
      <img src="{{asset('uploads/Products_Icon.png')}}" style="float: left;width: 60px;height: 70px;">
      <p class="box_text" style="font-size: 50px;font-weight: bold;">{{$products->count()}}</p>
      <p class="box_text" style="font-size: 15px;font-weight: bold;">{{trans('dash.no_products')}}</p>
    <div class="progress" style="height: 12px;">
    <div class="progress-bar" style="width:{{($products->count()/$package_no_product)*100}}%;background-color: #f79434 !important;"></div>
    </div>
    {{$products->count()}}/{{$package_no_product}}
    {{-- <a href="{{url('upgrade')}}"><p style="font-size: 14px;color: #f79434">{{trans('dash.upgrade_for_more_prod')}}</p></a> --}}
    </div>
  </a>
  <!------------------------------------------------------------------------------->

    <div class="Box col-md-2 " style="border: 1px solid #e7e7e7;padding: 15px;margin: 2px;border-top: 2px solid #7ccafa;background-color: white;min-height: 290px;min-width: 200px;">
      <img src="{{asset('uploads/export_chance.png')}}" style="float: left;width: 60px;height: 70px;">
      <p class="box_text" style="font-size: 50px;font-weight: bold;">{{$quoterequest}}</p>
      @if($category_name)
      <p class="box_text" style="font-size: 15px;font-weight: bold;">{{trans('dash.no_opportunity_sent')}} <span style="font-weight: normal;">
               @if($category_name)
                  <a style="color: #333333;cursor: pointer;" href="{{url($category_name->id.'/'.'فرص-تصدير?'  .'/'.$category_name->name_ar)}}">
                  {{$category_name->name_ar}}
                  </a>
               @else
                  {{$category_name->name_ar}}
              @endif
          </span></p>
      @else
      <p class="box_text" style="font-size: 15px;font-weight: bold;">{{trans('dash.no_opportunity_sent')}}</p>
      @endif
    <div class="progress" style="height: 12px;">
    <div class="progress-bar" style="width:50%;background-color: #7ccafa;"></div>
    </div>
   {{$quoterequest}}
    {{-- <a href="{{url('upgrade')}}"><p style="font-size: 14px;color: #7ccafa">{{trans('dash.upgrade_for_more_sec')}}</p></a> --}}
    

    
    </div>
    

  <!------------------------------------------------------------------------------->
  {{--<a style="color: #333333;cursor: pointer;" href="dashboard/quotations-send">--}}
    <div class="Box col-md-2 " style="border: 1px solid #e7e7e7;padding: 15px;margin: 2px;border-top: 2px solid #5eb51a;background-color: white;min-height: 290px;min-width: 200px;">
      <img src="{{asset('uploads/export_view.png')}}" style="float: left;width: 60px;height: 70px;">
      <p class="box_text" style="font-size: 50px;font-weight: bold;">{{$quote_request_seen->count()}}</p>
      <p class="box_text" style="font-size: 15px;font-weight: bold;">{{trans('dash.no_opportunity_seen')}}</p>
    <div class="progress" style="height: 12px;">
    <div class="progress-bar" style="width:{{($quote_request_seen->count()/$quoterequest)*100}}%;background-color: #5eb51a;"></div>
    </div>
    {{$quote_request_seen->count()}}
    {{-- <a href="{{url('upgrade')}}"><p style="font-size: 14px;color: #5eb51a">{{trans('dash.upgrade')}}</p></a> --}}
    </div>
  {{--</a>--}}
  <!------------------------------------------------------------------------------->
  <a style="color: #333333;cursor: pointer;" href="{{url('dashboard/quotations-send')  }}">
    <div class="Box col-md-2" style="border: 1px solid #e7e7e7;padding: 15px;margin: 2px;border-top: 2px solid #d22e63;background-color: white;min-height: 290px;min-width: 200px;">
      <img src="{{asset('uploads/offer_sent.jpg')}}" style="float: left;width: 60px;height: 70px;">
      <p class="box_text" style="font-size: 50px;font-weight: bold;">{{$quote->count()}}</p>
      <p class="box_text" style="font-size: 15px;font-weight: bold;">{{trans('dash.no_opportunity_made')}}</p>
    <div class="progress" style="height: 12px;">
      @if($package_price_offer == 0)
     <div class="progress-bar" style="width:0%;background-color: #d22e63;"></div>
      @else
     <div class="progress-bar" style="width:{{($quote->count()/$package_price_offer)*100}}%;background-color: #d22e63;"></div>
      @endif
    </div>
    {{$quote->count()}}{{-- /{{$package_price_offer}} --}}
    {{-- <a href="{{url('upgrade')}}"><p style="font-size: 14px;color: #d22e63">{{trans('dash.upgrade_for_more_offer')}}</p></a> --}}
    </div>
  </a>
    <!--=========================================================================-->
    <!--test-->
      {{-- <div class="Box col-md-3" style="border: 1px solid #e7e7e7;padding: 15px;margin: 15px;border-top: 4px solid #f79434;background-color: white;min-height: 600px;">
        <p style="font-size: 20px;font-weight: bold;text-align: center">{{trans('dash.adv')}}</p>
          <hr style="color: gray;">
          <div class="owl-itms01">
              @foreach($dash_adv->slice(0,10) as $adv)
                <div class="item">
                  <a href="{{url($adv->link)}}" >
                    <span class="img-hldr"><img src="uploads/{{$adv->image}}"></span>
                  </a>
                </div>
              @endforeach
            </div>
      </div> --}}
        <!------------------------------------------------------------------------------->
    <!--adv-->
      <div class="Box col-md-3" style="border: 1px solid #e7e7e7;padding: 15px;margin: 15px;border-top: 4px solid #f79434;background-color: white;min-height: 600px;">
        <p style="font-size: 20px;font-weight: bold;text-align: center">{{trans('dash.adv')}}</p>
          <hr style="color: gray;">
          <div id="slider3" >
                <a class="control_next3">></a>
                <a class="control_prev3"><</a>
                <ul>
                  @foreach($dash_adv as $adv)
                  @if($adv->type == "أعلانات")
                  <li>
                    <a href="{{url($adv->link)}}" target="blank" style="cursor: pointer;">
                    <img src="uploads/{{$adv->image}}" style="width: 280px;height: 200px;">
                    </a>
                    <div style="padding: 0px;padding: 10px;margin-top: 60px;background-color: white;overflow: hidden;border-radius: 15px;">
                    <p style="font-size: 18px;font-weight: bold;color:  #333333;margin-top: 30px;">{{$adv->title}}</p>
                    <hr>
                    <p style="font-size: 14px;font-weight: bold;color:  #f79434;">{{$adv->subtitle}}</p>
                    </div>
                  </li>
                  @endif
                  @endforeach
                </ul>
            </div>
      </div>
        <!------------------------------------------------------------------------------->

      <div class="Box col-md-3" style="border: 1px solid #e7e7e7;padding: 15px;margin: 15px;border-top: 4px solid #7ccafa;background-color: white;min-height: 600px;">
        <p style="font-size: 20px;font-weight: bold;text-align: center">{{trans('front.news')}}</p>
          <hr style="color: gray;">
           <div id="slider2" >
                <a class="control_next2">></a>
                <a class="control_prev2"><</a>
                <ul>
                  @foreach($dash_adv as $adv)
                  @if($adv->type == "أخبار و فعاليات")
                  <li>
                    <a href="{{url($adv->link)}}" target="blank" style="cursor: pointer;">
                    <img src="uploads/{{$adv->image}}" style="width: 280px;height: 200px;">
                    </a>
                    <div style="padding: 0px;padding: 10px;margin-top: 60px;background-color: white;overflow: hidden;border-radius: 15px;">
                    <p style="font-size: 18px;font-weight: bold;color:  #333333;margin-top: 30px;">{{$adv->title}}</p>
                    <hr>
                    <p style="font-size: 14px;font-weight: bold;color:  #7ccafa;">{{$adv->subtitle}}</p>
                    </div>
                  </li>
                  @endif
                  @endforeach
                </ul>
            </div>
      </div>
        <!------------------------------------------------------------------------------->

      <div class="Box col-md-3" style="border: 1px solid #e7e7e7;padding: 0px;padding-top: 15px;margin: 15px;border-top: 4px solid #5eb51a;background-color: white;min-height: 600px;overflow: hidden;">
        <p style="font-size: 20px;font-weight: bold;text-align: center;">{{trans('front.recent_posts')}}</p>
          <hr style="color: gray;">
                <div id="slider" >
                <a class="control_next">></a>
                <a class="control_prev"><</a>
                <ul>
                  @foreach($blogs as $blog)
                  <li>
                    <p style="font-size: 18px;font-weight: bold;color:  #333333;margin-top: 2px;">{{$blog->title_ar}}</p>
                    <a href="{{url('blog')}}" target="blank" style="cursor: pointer;">
                    <img src="uploads/{{$blog->image}}" style="width: 280px;height: 200px;">
                    </a>
                    <div style="padding: 0px;padding: 10px;margin-top: 20px;background-color: white;overflow: hidden;border-radius: 15px;">
                      @foreach($blogs_cat as $blog_ct)
                    @if($blog->category_id == $blog_ct->id)
                    <p style="font-size: 14px;font-weight: bold;color:  #333333;"><span>{{trans('dash.article_cat')}} : </span>{{$blog_ct->slug}}</p>
                    @endif
                    @endforeach
                    <hr>
                    <p style="font-size: 14px;font-weight: bold;color:  #5eb51a;"><span>{{trans('dash.publish_date')}} : </span>{{date('d-m-Y',strtotime($blog->created_at))}}</p>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
      </div>
     </div>
      <!---===========================================================================--->
      <!-- Modal My Website-->
      <div id="myModal" class="modal fade" role="dialog" style="text-align: center;">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
                <?php $data = ['store_name' => $my_website->domain ]?>
              @include('dashboard.website.update_website' , $data)
            </div>
          </div>

        </div>
      </div>
      <!---===========================================================================--->
      <!---===========================================================================--->
      <!-- Modal My Website-->
      <div id="myModal2" class="modal fade" role="dialog" style="text-align: center;">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-body">
              {!!Form::model($user_profile, [
                'class'=>'form-horizontal',
                'url'=>'dashboard/profile',
                'method' => 'POST',
                'files'=>true
              ])!!}
              <!---->
              <?php $field = 'co_logo'; ?>
                <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
                    {!! Form::label($field, trans('dash.company_logo'), ['class'=>'col-md-3 control-label']) !!}
                    <div class="col-md-8">
                        <div class="media">
                            <?php $filepath = (isset($user_profile))? 'uploads/'.$user_profile->co_logo:'frontend/images/no_thumb.jpg';?>
                            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield2'])!!}</a>
                            <div class="media-body">
                                <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'2'])!!}</h4>
                                <div class="help-block">{{trans('dash.imgrules')}}</div>
                            </div>
                        </div>
                        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                {!!Form::submit(trans('dash.profile_submit'),['class'=>'btn btn-primary btn_submit','name'=>'btn_submit'])!!}
                {!!Form::close()!!} 
                <!---->
            </div>
          </div>

        </div>
      </div>
      <!---===========================================================================--->
      
      
      <!---=============== modal change package============================================================--->
      <!---===========================================================================--->
      
      <!--<div id="package-modal" class="modal fade" role="dialog" style="text-align: center;">-->
      <!--  <div class="modal-dialog">-->

          <!-- Modal content-->
      <!--    <div class="modal-content">-->
      <!--      <div class="modal-body">-->
              
      <!--      </div>-->
      <!--    </div>-->

      <!--  </div>-->
      <!--</div>-->
@endsection

@section('script')
  {{--<!-- @include('dashboard.layouts.popup') -->--}}
  <script type="text/javascript">
    jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 8000);
  var slideCount = $('#slider ul li').length;
  var slideWidth = $('#slider ul li').width();
  var slideHeight = $('#slider ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider').css({ width: slideWidth, height: slideHeight });

  $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

  });
  //slider 2 news
  jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 8000);
  var slideCount = $('#slider2 ul li').length;
  var slideWidth = $('#slider2 ul li').width();
  var slideHeight = $('#slider2 ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider2').css({ width: slideWidth, height: slideHeight });

  $('#slider2 ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider2 ul li:last-child').prependTo('#slider2 ul');

    function moveLeft() {
        $('#slider2 ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider2 ul li:last-child').prependTo('#slider2 ul');
            $('#slider2 ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider2 ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider2 ul li:first-child').appendTo('#slider2 ul');
            $('#slider2 ul').css('left', '');
        });
    };

    $('a.control_prev2').click(function () {
        moveLeft();
    });

    $('a.control_next2').click(function () {
        moveRight();
    });

  });
  //slider 3 Recent Posts
  jQuery(document).ready(function ($) {

    setInterval(function () {
        moveRight();
    }, 8000);
  var slideCount = $('#slider3 ul li').length;
  var slideWidth = $('#slider3 ul li').width();
  var slideHeight = $('#slider3 ul li').height();
  var sliderUlWidth = slideCount * slideWidth;  $('#slider3').css({ width: slideWidth, height: slideHeight });

  $('#slider3 ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider3 ul li:last-child').prependTo('#slider3 ul');

    function moveLeft() {
        $('#slider3 ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider3 ul li:last-child').prependTo('#slider3 ul');
            $('#slider3 ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider3 ul').animate({
            left: - slideWidth
        }, 500, function () {
            $('#slider3 ul li:first-child').appendTo('#slider3 ul');
            $('#slider3 ul').css('left', '');
        });
    };

    $('a.control_prev3').click(function () {
        moveLeft();
    });

    $('a.control_next3').click(function () {
        moveRight();
    });

  });
  </script>
@endsection
