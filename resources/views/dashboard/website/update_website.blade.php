<?php
	use App\Helper;
	use App\Template;
	$templates = Template::whereActive(1)->get();
        $user_website = App\UserWebsite::whereUser_id(Auth::id())->first();
        if(!$user_website) {
            $user_website = UserWebsite::init(Auth::user());
        }	
	$free_templates = $templates->filter(function($row) {
		return ($row->is_free==1);
	});
	$premium_templates = $templates->filter(function($row) {
		return ($row->is_free==0);
	});
?>
<div class="row">
	<div class="col-md-12">
		<legend>اختر القالب التصميمي للمتجر</legend>

		<div class="well">
			<h4>@lang('dash.free_templates')</h4>
			@if($free_templates->count()>0)
				<div class="row"> 
				@foreach($free_templates as $template)
				<div class="col-md-6">
					<div class="thumbnail">
						{!!Helper::image($template->image, ['style'=>'height:190px;'])!!}
						<div class="caption text-center">
							<h4>{{Template::$skins[$template->skin]}}</h4>
							<p>
							@if($user_website->template_id==$template->id)
								<span class="text-success"><i class="fa fa-check-circle"></i> تم اختيار هذا التصميم</span>
							@else
								<a href="{{url('dashboard/website/template-select/'.$template->id)}}" class="btn btn-info btn-sm">اختر هذا القالب</a>
							@endif
							</p>
						</div>
					</div>
				</div>
				@endforeach
				</div>
			@else
				<div class="alert alert-warning">لا يوجد تصميمات متاحة</div>
			@endif
		</div>

		<div class="well">
			<h4>@lang('dash.premium_templates')</h4>
			@if($premium_templates->count()>0)
				<div class="row">
				@foreach($premium_templates as $template)
					<div class="col-md-4">
						<div class="thumbnail">
							<button class="	glyphicon glyphicon-lock" style="text-align: center;"></button>
							{!!Helper::image($template->image, ['style'=>'height:190px;'])!!}
							<div class="caption text-center">
								<h4>{{$template->name}}</h4>
								<p>
								@if($user_website->template_id==$template->id)
									<span class="text-success"><i class="fa fa-check-circle"></i> تم اختيار هذا التصميم</span>
								@else
									<a href="{{url('upgrade/quick/'.$template->id)}}" class="btn btn-success btn-sm popup">@lang('dash.buy_now')</a>
								@endif
								</p>
							</div>
						</div>
					</div>
				@endforeach
				</div>
			@else
				<div class="alert alert-warning">لا يوجد تصميمات متاحة</div>
			@endif
		</div>
	</div>
	<div class="col-md-12">
			{!!Form::open(['class'=>'form-horizontal', 'url'=>'dashboard/website/launch'])!!}
			<div class="well">
				<div class="form-group  {{ $errors->has('domain') ? 'has-error' : ''}}">
					{!!Form::label('domain', 'يرجى ادخال اسم للمتجر الخاص بك', ['class'=>'control-label'])!!}

					{!!Form::text('domain', $data['store_name'], ['class'=>'form-control check_english' ])!!}

					{!! $errors->first('domain', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					{!!Form::submit('نشر المتجر', ['class'=>'btn btn-success'])!!}
				</div>
				<div class="help-block">
					بعد الضغط على نشر الموقع, سيتم مراجعة أحد مشرفي الموقع لأسم المتجر وجميع البيانات وبعدها سيتم تفعيله
				</div>
			</div>
			{!!Form::close()!!}
		</div>
</div>