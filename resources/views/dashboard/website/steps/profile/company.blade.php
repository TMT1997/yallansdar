<?php
    use App\UserProfile;
    use App\Settings;
    use App\UserFactoryInfo;
    use App\Category;
    $countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
    $years = range(1900, 2017);
    $years = array_prepend($years, trans('dash.please_select'), 0);
    if (App::getLocale()=='en') 
    {
        $main_categories = Category::whereParent_category_id(0)->get(['name_en']);
        $parents = Category::whereParent_category_id(0)->orderBy('name_en')->get();
        $category_lang = "name_en";
    }
    else
    {
        $main_categories = Category::whereParent_category_id(0)->get(['name_ar']);
        $parents = Category::whereParent_category_id(0)->orderBy('name_ar')->get();
        $category_lang = "name_ar";
    }
    $parents['please_select'] = '';
    \View::share('parents', $parents);
?>
<legend>{{trans('dash.company_profile')}}</legend>
<?php $field = 'company_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.company_name'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control co_nameco check_english', 'placeholder'=>trans('dash.'.$field) , 'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'business_type'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.business_type'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        <div class="row"> 
            @foreach(App\Messages\Contact::$business_type as $type=>$label)
                @if($type=='') <?php continue;?> @endif
                <div class="col-md-4"><label>{!!Form::checkbox('business_type[]', $type, null)!!} {{trans('dash.business_types')[$type]}}</label></div>
            @endforeach
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_copy_info'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.company_contact_info'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <label>{!!Form::checkbox('co_copy_info',1,null,['class'=>'sameContactInfo'])!!} {{trans('dash.myinfo_same_personalprofile')}}</label>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'contact_f_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.contact_person'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.first_name')]) !!}
        {!! Form::text('contact_l_name', null, ['class'=>'form-control', 'placeholder'=>trans('dash.last_name')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'primary_email'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.primary_email'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field), 'disabled'=>'']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_address_line1'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.contact_address'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.address_line1')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<!--<?php $field = 'co_address_line2'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.address_line2')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'co_country_id'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.co_country'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $countries, null, ['class' => 'form-control select2']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_state'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.co_state'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_city'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.co_city'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'co_zipcode'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::number($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.zipcode')])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<!-- <?php $field = 'main_products'; ?>
<div class="form-group {{ $errors->has('main_products') ? 'has-error' : ''}}">
    {!! Form::label('main_products', trans('dash.main_products'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->main_products))
            @foreach($user_profile->main_products as $index=>$name)
            <div class="margbtn10">
                {!!Form::text($field.'[]', $name, ['class'=>'form-control', 'placeholder'=>trans('dash.product_name') , 'required'=>'required'])!!}
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
            </div>
            @endforeach
        @else
            {!!Form::text($field.'[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.product_name') , 'required'=>'required'])!!}
        @endif
        <a href="#" data-ft='products' data-max='10' class="btn btn-success btn-sm add_new_field">+</a>
        <div class="more_fields"></div>
        <div class="help-block">{{trans('dash.pls_ent_prod_eachbox')}}</div>
    </div>
</div> -->

<?php $field = 'other_products'; ?>
<div class="form-group {{ $errors->has('other_products') ? 'has-error' : ''}}">
    {!! Form::label('other_products', trans('dash.section_prod'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->other_products))
            @foreach($user_profile->other_products as $index=>$name)
            <div class="margbtn10">
<!--                 {!!Form::text($field.'[]', $name, ['class'=>'form-control', 'placeholder'=>trans('dash.product_name')])!!}

 -->                         {!! Form::select($field,$parents->lists($category_lang,'id'),'',['class'=>'form-control sector_validate','required'=>'required']) !!}
                <!-- <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a> -->
            </div>
            @endforeach
        @else
           <!--  {!!Form::text($field.'[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.product_name')])!!} -->
           {!! Form::select($field,$parents->lists($category_lang,'id'),'',['class'=>'form-control sector_validate','required'=>'required']) !!}
        @endif
<!--         <a href="#" data-ft='oproducts' data-max='10' class="btn btn-success btn-sm add_new_field">+</a>
 -->        <div class="more_fields"></div>
<!--         <div class="help-block">{{trans('dash.pls_ent_prod_eachbox')}}</div>
 -->    </div>
</div>


<?php $field = 'website'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.website2'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->website))
            @foreach($user_profile->website as $index=>$name)
            <div class="margbtn10">
                {!!Form::text($field.'[]', $name, ['class'=>'form-control check_english_url','placeholder'=>trans('dash.'.$field)])!!}
                <!-- <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a> -->
            </div>
            @endforeach
        @else
            {!!Form::text($field.'[]', null, ['class'=>'form-control check_english_url'])!!}
        @endif
        <!-- <a href="#" data-ft='website' data-max='3' class="btn btn-success btn-sm add_new_field">+</a> -->
        <br>
        <div class="alert_message btn-danger col-md-3">@lang('dash.validate_website')</div>
        <div class="more_fields"></div>
    </div>
</div>

<?php $field = 'div_offices'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    <!--{!! Form::label($field, trans('dash.divisional_office'), ['class' => 'col-md-3 control-label']) !!}-->
    <div class="col-md-8">
        <!--<div class="help-block">{{trans('dash.division_desc')}}</div>
        <a href="#new_div" class="add_div_office">{{trans('dash.add_new_division')}}</a>-->
        <div class="div_office_template hidden">
            @include('dashboard.profile.forms.division_office')
        </div>
        <a name="new_div"></a>
        @if($user_profile->offices->count()>0)
            @foreach($user_profile->offices as $office)
                @include('dashboard.profile.forms.division_office_row')
            @endforeach
        @endif
        <div class="div_offices"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!--<legend>{{trans('dash.logos_certificates')}}</legend>
<?php $field = 'co_logo'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.company_logo'), ['class'=>'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath = (isset($user_profile))? 'uploads/'.$user_profile->co_logo:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield2'])!!}</a>
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'2'])!!}</h4>
                <div class="help-block">{{trans('dash.imgrules')}}</div>
            </div>
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'identity_proof'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.upload_identity_proof'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!!Form::select('identity_proof_opt', trans('dash.identity_docs'), null, ['class'=>'form-control'])!!}
        {!!Form::file($field,['class'=>'form-control'])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}

        @if(isset($user_profile) && !empty($user_profile->identity_proof))
            <a class="pull-right preview_doc btn btn-info" target="_blank" href="{{url('uploads/'.$user_profile->identity_proof)}}">@lang('dash.perview')</a>
        @endif
    </div>
</div>-->

<?php $field = 'identity_proof2'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!!Form::select('identity_proof2_opt', trans('dash.identity_docs'), null, ['class'=>'form-control'])!!}
        {!!Form::file($field,['class'=>'form-control'])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}

        @if(isset($user_profile) && !empty($user_profile->identity_proof2))
            <a class="pull-right preview_doc btn btn-info" target="_blank" href="{{url('uploads/'.$user_profile->identity_proof2)}}">@lang('dash.perview')</a>
        @endif
        <div class="help-block">{{trans('dash.imgdoc')}}</div>
    </div>
</div>-->

<!--==========================================================-->
<legend>{{trans('dash.fact_details')}}</legend>
<?php $field = 'established_year'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Year of Establishment'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $years, null, ['class' => 'form-control'])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'employees_no'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.No. of Employees'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.employees_nums'), null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'turnover'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Turnover'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.turnovers'), null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'legal_status'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.Legal Status'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.legal_statuses'), null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!--==============================================================-->
{!!Form::model($user_factory_info, [
        'class'=>'form-horizontal',
    ])!!}
<?php $field = 'contract_manufacturings'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        <div class="row"> 
            @foreach(UserFactoryInfo::$contract as $item=>$label)
                <div class="col-md-12"><label title="{{$item}}">{!!Form::checkbox($field.'[]',$item,null)!!} {{$label}}</label></div>
            @endforeach
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->


<?php $field = 'annual_output_value'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.annual_output_values'), null, ['class' => 'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!--==============================================================-->
{!!Form::model($user_trade_info, [
        'class'=>'form-horizontal',
        'id'=>'tradeInfoForm',
    ])!!}
<?php $field = 'export_percentage'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::select($field,trans('dash.export_percentages'),null,['class'=>'form-control','required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>