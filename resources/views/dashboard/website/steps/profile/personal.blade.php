<!-- <?php  ?>

<?php $field = 'user_type'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.user_type'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        @foreach(App\User::$types as $type=>$label)
            <label class="control-label">{!!Form::checkbox($field.'[]', $type, null,['id'=>'user_type'])!!} {{trans('front.'.$label)}}</label>
        @endforeach
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'first_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.first_name'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!!Form::text($field,null,['class'=>'form-control', 'placeholder'=>trans('dash.'.$field),'required'=>'required'])!!}
        {!!$errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'last_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.last_name'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control','placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'gender'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.gender'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.genders') ,null,['class'=>'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'picture'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.profile_picture'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath=(isset($user_profile))? 'uploads/'.$user_profile->picture:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1'])!!}</a>
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
                <div class="help-block">{{trans('dash.imgrules')}}</div>
            </div>
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'company_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control co_nameper', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'destination'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.destination'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.destination')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'department'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.department'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.departments_arr'), null,['class'=>'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'address_line1'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.address'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'address_line2'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'zipcode'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::text($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'telephone'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.telephone'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->telephone))
            @foreach($user_profile->telephone as $index=>$val)
            <div class="margbtn10">
                {!!Form::text('tel_code[]',@$user_profile->tel_code[$index],['class'=>'form-control short','placeholder'=>trans('dash.code')])!!}
                {!!Form::text($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
            </div>
            @endforeach
        @else
            @if(is_array($user_profile->tel_code))
                <?php $user_profile->tel_code = implode($user_profile->tel_code, ','); ?>
            @endif
            {!!Form::text('tel_code[]', null, ['class'=>'form-control short', 'placeholder'=>trans('dash.code')])!!}
            {!!Form::text($field.'[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
        @endif
        <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field">+</a>
        <div class="more_fields"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'mobile'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.mobile'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->mobile) && !empty($user_profile->mob_code) && is_array($user_profile->mobile))
            @foreach($user_profile->mobile as $index=>$val)
            <div class="margbtn10">
                {!!Form::text('mob_code[]',@$user_profile->mob_code[$index],['class'=>'form-control short','placeholder'=>trans('dash.code')])!!}
                {!!Form::text($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
            </div>
            @endforeach
        @elseif(!empty($user_profile->mobile))
            @foreach($user_profile->mobile as $index=>$val)
            <div class="margbtn10">
                <?php $code = (isset($user_profile->mob_code[$index]))? $user_profile->mob_code[$index]:''; ?> 
                {!!Form::text('mob_code[]',$code,['class'=>'form-control short','placeholder'=>trans('dash.code')])!!}
                {!!Form::text($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>
            </div>
            @endforeach
        @else
            {!!Form::text('mob_code[]', null, ['class'=>'form-control short', 'placeholder'=>trans('dash.code')])!!}
            {!!Form::text($field.'[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
        @endif
        <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field">+</a>
        <div class="more_fields"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'fax'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.fax'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!!Form::text('fax_code', null, ['class'=>'form-control short', 'placeholder'=>trans('dash.code')])!!}
        {!!Form::text($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<?php
    use App\UserProfile;
    $countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
<div class="help-block" style="background-color: #17a2b8;color:white;font-weight: bold;font-size: 15px;text-align: center;border-radius: 15px;padding: 5px;">
        {{trans('front.en_data_notify')}}
</div>

<?php $field = 'user_type'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.user_type'), ['class' => 'col-md-3 control-label required']) !!}
    @if($user_profile->user_type)
    <div class="check_user_type" style="display: none;">1</div>
    @else
    <div class="check_user_type" style="display: none;">0</div>
    @endif
    <div class="col-md-8 user_type_div">
        @foreach(App\User::$prof_types as $type=>$label)
            <label class="control-label">{!!Form::checkbox($field.'[]', $type, null,['id'=>'user_type','class'=>'user_type '.$type])!!} {{trans('front.'.$label)}}</label>
            <br>
        @endforeach
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'first_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.first_name'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!!Form::text($field,null,['class'=>'form-control check_english', 'placeholder'=>trans('dash.'.$field),'required'=>'required'])!!}
        {!!$errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'last_name'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.last_name'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control check_english','placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'gender'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.gender'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.genders') ,null,['class'=>'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'picture'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.profile_picture'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath=(isset($user_profile))? 'uploads/'.$user_profile->picture:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1'])!!}</a>
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file($field,['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
                <div class="help-block">{{trans('dash.imgrules')}}</div>
            </div>
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'represent_company'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.do_u_represent_company'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="checkbox">
            <label>{!! Form::radio($field, '1', null) !!} {{trans('dash.yes')}}</label>
            <label>{!! Form::radio($field, '0', null) !!} {{trans('dash.no')}}</label>
        </div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->


<?php //$field = 'company_name'; ?>
<!--
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control co_nameper', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>
-->

<?php $field = 'destination'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.destination'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.destination')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'department'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.department'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select($field, trans('dash.departments_arr'), null,['class'=>'form-control']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'primary_email'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.primary_email'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::email($field, null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field), 'disabled'=>'']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!-- <?php $field = 'alt_email'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.alternative_email'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::email($field, null, ['class' => 'form-control', 
        'placeholder'=>trans('dash.alternative_email')]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<?php $field = 'address_line1'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.address'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control check_english', 'placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<!--<?php $field = 'address_line2'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class' => 'form-control check_english', 'placeholder'=>trans('dash.'.$field)]) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'country_id'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.country'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::select($field, $countries, null, ['class' => 'form-control select2','required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
        <div class="country_img"></div>
    </div>
</div>


<?php $field = 'state'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.state'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null, ['class'=>'form-control','placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'city'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.city'), ['class' => 'col-md-3 control-label required']) !!}
    <div class="col-md-8">
        {!! Form::text($field, null,['class'=>'form-control check_english', 'placeholder'=>trans('dash.'.$field),'required'=>'required']) !!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'zipcode'; ?>
<!--<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::number($field, null, ['class'=>'form-control check_english', 'placeholder'=>trans('dash.'.$field)])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<?php $field = 'telephone'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.telephone'), ['class' => 'col-md-3 control-label']) !!}

    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->telephone))
            @foreach($user_profile->telephone as $index=>$val)
                {!!Form::number('tel_code[]',@$user_profile->tel_code[$index],['class'=>'form-control short tel_cd','placeholder'=>trans('dash.code')])!!}
                {!!Form::number($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
            @if($index==0)
            <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field">+</a><br>
            @else
            <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a><br>
            @endif
            @endforeach
        @else
            @if(is_array($user_profile->tel_code))
                <?php $user_profile->tel_code = implode($user_profile->tel_code, ','); ?>
            @endif

            {!!Form::number('tel_code[]', null, ['class'=>'form-control short tel_cd', 'placeholder'=>trans('dash.code')])!!}
            {!!Form::number($field.'[]', null, ['class'=>'form-control',
            'placeholder'=>trans('dash.'.$field) ])!!}
            <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field">+</a><br>
        @endif
        <div class="more_fields"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<?php $field = 'mobile'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.mobile'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->mobile) && !empty($user_profile->mob_code) && is_array($user_profile->mobile))
            @foreach($user_profile->mobile as $index=>$val)
            
                {!!Form::number('mob_code[]',@$user_profile->mob_code[$index],['class'=>'form-control short mob_cd','placeholder'=>trans('dash.code')])!!}
                {!!Form::number($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
                @if($index==0)
                <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field">+</a><br>
                @else
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a><br>
                @endif
           
            @endforeach
        @elseif(!empty($user_profile->mobile))
            @foreach($user_profile->mobile as $index=>$val)
            
                <?php $code = (isset($user_profile->mob_code[$index]))? $user_profile->mob_code[$index]:''; ?> 
                {!!Form::number('mob_code[]',$code,['class'=>'form-control short mob_cd','placeholder'=>trans('dash.code')])!!}
                {!!Form::number($field.'[]', $val, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
                @if($index==0)
                <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field inline">+</a><br>
                 @else
                <a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a><br>
                @endif
            
            @endforeach
        @else
            {!!Form::number('mob_code[]', null, ['class'=>'form-control short mob_cd', 'placeholder'=>trans('dash.code')])!!}
            {!!Form::number($field.'[]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
             <a href="#" data-ft='{{$field}}' data-max='3' class="btn btn-success btn-sm add_new_field inline">+</a><br>
        @endif
        
        <div class="more_fields"></div>
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!-- <?php $field = 'fax'; ?>
<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
    {!! Form::label($field, trans('dash.fax'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {!!Form::text('fax_code', null, ['class'=>'form-control short', 'placeholder'=>trans('dash.code')])!!}
        {!!Form::text($field, null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
    </div>
</div> -->