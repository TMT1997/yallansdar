<legend>{{trans('dash.profile')}}</legend>
<div class="alert alert-success text-center">
    <i class="fa fa-check-circle-o fa-2x text-info"></i> 
    جيد, لقد قمت باضافة البيانات الخاصة بحسابك وحساب شركتك, وبالتالي فأنت قد اجتزت هذه الخطوة
</div>

<div role="tabpanel">
	<!-- <ul class="nav nav-tabs" role="tablist">
		@foreach(App\UserProfile::$website_profile_tabs as $index=>$tab)
			<li role="presentation" class="{{($index==1)?'active':''}}">
				<a href="#n{{$index}}" aria-controls="n{{$index}}" role="tab" data-toggle="tab">{{trans('dash.'.snake_case($tab[0]))}}</a>
			</li>
		@endforeach
	</ul> -->

	<div class="tab-content" style="padding-top: 20px;">
		@foreach(App\UserProfile::$website_profile_tabs as $index=>$tab)
			<div role="tabpanel" class="tab-pane {{($index==1)?'active':''}}" id="n{{$index}}">
				@include($tab[1])
			</div>
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-offset-3 col-md-8">
			<div class="help-block">
				{{trans('dash.profile_save_note')}}
			</div>
		</div>
	</div>
</div>