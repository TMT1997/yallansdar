<?php use App\UserWebsite; ?>

	<span class="text-success">
		{{trans('dash.ready_publish')}}
	</span>
	<br><br>
	@if(empty($user_website->domain) || $user_website->status==0)
		<div class="col-md-12">
			{!!Form::open(['class'=>'form-horizontal launch_form'])!!}
			<div class="well">
				<div class="form-group  {{ $errors->has('domain') ? 'has-error' : ''}}">
					{!!Form::label('domain', trans('dash.enter_store_name'), ['class'=>'control-label'])!!}
					{!!Form::text('domain', null, ['class'=>'form-control check_english check_space', 'placeholder'=>trans('dash.publish_hint')])!!}
					{!! $errors->first('domain', '<p class="help-block">:message</p>') !!}
				</div>
				<div class="form-group">
					{!!Form::submit(trans('dash.publish'), ['class'=>'btn btn-success'])!!}
				</div>
				<div class="help-block">
					{{trans('dash.publish_desc')}}
				</div>
			</div>
			{!!Form::close()!!}
		</div>
	@else
		<strong>اسم المتجر: </strong> {{$user_website->domain}} <br>
		<strong>موافقة الموقع: </strong> {{UserWebsite::$status_desc[$user_website->status]}} <br>
		<strong>حالة النشر: </strong> {{UserWebsite::$active_desc[$user_website->active]}} <br>
		<hr>
		@if($user_website->status==1)
			<div class="text-primary">
				<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
				المتجر منتظر مراجعة إدارة الموقع لتفعيله
			</div>
		@elseif($user_website->status==2)
			<div class="text-success">
				<i class="fa fa-check-square-o" aria-hidden="true"></i>

			 	المتجر الخاص بك يعمل الأن, لزيارة المتجر قم بالضغط على الزر التالي <br>
			 	<a href="{{UserWebsite::url(null, $user_website->domain)}}" class="btn btn-success" target="_blank">عرض المتجر</a>
			 	<br><hr>
			 	<strong>التحكم في المحتوى</strong><br>
			 	<a href="{{url('dashboard/websitepages')}}" class="btn btn-primary">إدارة الصفحات و السليدر المتحرك</a>
			 	<br>
			</div>
		 	
		 	<hr>
		 	<table><tr>
		 		<td>يمكنك أيضا تغيير حالة المتجر بالتشغيل أو الإيقاف من هنا:</td>
		 		<td>{!!App\Helper::sw($user_website,'active','UserWebsite')!!}</td></tr>
		 	</table>

		@elseif($user_website->status==3)
			<div class="text-danger">
				<i class="fa fa-ban" aria-hidden="true"></i>
			 	عفوا, لقد تم رفض المتجر من قبل مشرفي الموقع, يرجى التحقق من جميع البيانات وإعادة إرسال طلب لإدارة الموقع مرة اخرى
			 	<br>
			 	للتواصل مع إدارة الموقع <a href="{{url('site/contact')}}" target="_blank">اضغط هنا</a>
			</div>
		@endif
	@endif