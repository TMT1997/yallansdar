<?php
	use App\Helper;
	use App\Template;
	$free_templates = $templates->filter(function($row) {
		return ($row->is_free==1);
	});
	$premium_templates = $templates->filter(function($row) {
		return ($row->is_free==0);
	});
?>
<div class="row">
	<div class="col-md-12">
		<legend>اختر القالب التصميمي للمتجر</legend>

		<div class="well">
			<h4>@lang('dash.free_templates')</h4>
			@if($free_templates->count()>0)
				<div class="row"> 
				@foreach($free_templates as $template)
				<div class="col-md-2">
					<div class="thumbnail">
						{!!Helper::image($template->image, ['style'=>'height:190px;'])!!}
						<div class="caption text-center">
							<h4>{{Template::$skins[$template->skin]}}</h4>
							<p>
							@if($user_website->template_id==$template->id)
								<span class="text-success"><i class="fa fa-check-circle"></i> تم اختيار هذا التصميم</span>
							@else
								<!-- <a href="{{url('dashboard/website/template-select/'.$template->id)}}" class="btn btn-info btn-sm">اختر هذا القالب</a> -->
							@endif
							</p>
						</div>
					</div>
				</div>
				@endforeach
				</div>
			@else
				<div class="alert alert-warning">لا يوجد تصميمات متاحة</div>
			@endif
		</div>

		
	</div>
</div>