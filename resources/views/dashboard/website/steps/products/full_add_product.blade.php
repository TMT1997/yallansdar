<?php
    $req = ' <span class="required">*</span>';
    $countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
@if($products->count() > 0)
    
    <div class="alert alert-success text-center">
        <i class="fa fa-check-circle-o fa-2x text-info"></i>
        جيد, لقد قمت باضافة عدد ({{$products->count()}}) من المنتجات, 
        وبالتالي فأنت قد اجتزت هذه الخطوة
    </div>

@else
<div class="well well-sm">{{trans('products.basic_prd_info')}}</div>
    <div class="arrow_pointing" style="position: absolute; top: 10px; left:10px; line-height: 22px;">
        <div class="label label-warning" style="font-size: 12px; background-color:#f7a300">
            <i class="fa fa-2x fa-check-circle" style="vertical-align: middle;"></i> @lang('dash.add_bothlangs_hint')
        </div>
    </div>
    @foreach($langs as $iso=>$lang)
        <?php $lang_name = $lang['name']; ?>
        <div class="col-md-6">
            <div class="{{$errors->has('name_'.$iso)?'has-error':''}}">
                {!! Form::label('name_'.$iso, trans('products.prod_name').' ('.$lang_name.')', ['class'=>'control-label required']) !!}
                <div>
                    {!! Form::text('name_'.$iso, null,['class'=>'form-control']) !!}
                    {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{$errors->has('description_'.$iso)?'has-error':''}}">
                {!! Form::label('description_'.$iso, trans('products.prod_desc').' ('.$lang_name.')', ['class'=>'control-label required']) !!}
                <div>
                    {!! Form::textarea('description_'.$iso, null,['class'=>'form-control','placeholder'=>trans('products.desc_help'), 'rows'=>3])!!}
                    {!! $errors->first('description_'.$iso, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="{{$errors->has('details_'.$iso)?'has-error':''}}">
                {!! Form::label('details_'.$iso, trans('products.detailed_desc').' ('.$lang_name.')', ['class'=>'control-label required']) !!}
                <div>
                    {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso])!!}
                    {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
                {{--<div class="{{$errors->has('keywords_'.$iso)?'has-error':''}}">--}}
                    {{--{!! Form::label('keywords_'.$iso, trans('products.prod_keywords').' ('.$lang_name.')', ['class'=>'control-label']) !!}--}}
                    {{--<div>--}}
                        {{--{!! Form::textarea('keywords_'.$iso, null,['class'=>'form-control tags'])!!}--}}
                        {{--{!! $errors->first('keywords_'.$iso, '<p class="help-block">:message</p>') !!}--}}
                    {{--</div>--}}
            {{--</div>--}}
        </div>
    @endforeach

    <div role="tabpanel" style="position: relative;" class="hidden">
        <div class="arrow_pointing" style="position: absolute; top: 10px; right:200px; line-height: 22px;">
            <div class="label label-warning" style="font-size: 12px; background-color:#f7a300">
                <i class="fa fa-2x fa-arrow-right" style="vertical-align: middle;"></i> @lang('dash.add_bothlangs_hint')
            </div>
        </div>
    <?php $count=0; ?>
    <ul class="nav nav-tabs hidden" role="tablist">
        @foreach($languages as $lang)
            <li class="{{($count==0)?'active':''}}"><a href="#sec{{$lang->id}}" data-toggle="tab" class="required">{{$lang->name}}</a></li>
            <?php $count++ ?>
        @endforeach
    </ul>
    <?php $count=0; ?>
    <div class="tab-content hidden">
        @foreach($languages as $lang)
            <?php $active = ($count==0) ? 'active':''; ?>
            <div role="tabpanel" class="tab-pane fade in {!!$active!!}" id="sec{{$lang->id}}">
                <div class="panel panel-default" style="border-top: none;">
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', Lang::get('products.prod_name',[],$lang->iso2),['class'=>'col-md-3 control-label required']) !!}
                            <div class="col-md-8">
                                {{-- {!! Form::text('lang['.$lang->id.'][name]', null, ['class' => 'form-control']) !!} --}}
                                <input name="lang[{{$lang->id}}][name]" class="form-control" value="{{@$product->lang[$lang->id]['name']}}" /> 
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            {!!Form::label('description',Lang::get('products.prod_desc',[],$lang->iso2), ['class'=>'col-md-3 control-label required']) !!}
                            <div class="col-md-8">
                                {{-- {!! Form::textarea('lang['.$lang->id.'][description]', null, ['class'=>'form-control','rows'=>3,'placeholder'=>trans('products.desc_help')]) !!} --}}
                                <textarea name="lang[{{$lang->id}}][description]" class="form-control" rows='3' placeholder='@lang('products.desc_help')'>{!!@$product->lang[$lang->id]['description']!!}</textarea>
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                            {!! Form::label('details', Lang::get('products.detailed_desc',[],$lang->iso2), ['class'=>'col-md-3 control-label required']) !!}
                            <div class="col-md-8">
                                {{-- {!! Form::textarea('lang['.$lang->id.'][details]', null, ['class' => 'form-control', 'id'=>'editor'.$lang->id, 'rows'=>6]) !!} --}}
                                <textarea name="lang[{{$lang->id}}][details]" class="form-control" id='editor{{$lang->id}}'>{!!@$product->lang[$lang->id]['details']!!}</textarea>
                                {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('keywords') ? 'has-error' : ''}}">
                            {!! Form::label('keywords', Lang::get('products.prod_keywords',[],$lang->iso2), ['class'=>'col-md-3 control-label']) !!}
                            <div class="col-md-8">
                                {{-- {!! Form::text('lang['.$lang->id.'][keywords]', null, ['class'=>'form-control tags']) !!} --}}
                                <input name="lang[{{$lang->id}}][keywords]" class="form-control tags" value="{!!@$product->lang[$lang->id]['keywords']!!}" /> 
                                {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $count++ ?>
            @endforeach
        </div>
    </div>

    <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('products.prod_cat'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div>
    @include('dashboard.categories.select_category', ['selected_category_id'=>@$product->category_id])

    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', trans('products.prod_img'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            <div class="media">
                @if(isset($product) && !empty($product->image))
                    {!!App\Helper::image($product->image,['class'=>'media-object img_preview imgfieldmain','style'=>'max-width:120px'])!!}
                @else
                    <a class="pull-left" href="#">{!!HTML::image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfieldmain','style'=>'max-width:120px'])!!}</a>
                @endif
                <div class="media-body">
                    <h4 class="media-heading">{!!Form::file('image',['class'=>'form-control img_browse','data-fid'=>'main'])!!}</h4>
                    <p><div class="help-block">{{trans('dash.imgrules')}}</div></p>
                </div>
            </div>
            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    {{--Delete add group --}}
    {{--<div class="form-group {{ $errors->has('group_id') ? 'has-error' : ''}}">--}}
        {{--{!! Form::label('group_id', trans('products.prod_group'), ['class' => 'col-md-3 control-label']) !!}--}}
        {{--<div class="col-md-8 form-inline">--}}
            {{--{!! Form::select('group_id',$groups, null, ['class' => 'form-control', 'id'=>'group_id']) !!}--}}
            {{--<a href="{{url('dashboard/quickadd/group')}}" class="btn btn-success popup">{{trans('products.add_group')}}</a>--}}
            {{--{!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}--}}
        {{--</div>--}}
    {{--</div>--}}

<div class="well well-sm">{{trans('products.prod_specifications')}}</div>
    <div class="form-group {{ $errors->has('manufactured_in') ? 'has-error' : ''}}">
        {!! Form::label('manufactured_in', trans('products.prod_manuf_in'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!! Form::select('manufactured_in', $countries, null, ['class' => 'form-control select2','required'=>'required']) !!}
            {!! $errors->first('manufactured_in', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    {{--Delete refrence Code--}}
    {{--<div class="form-group {{ $errors->has('reference_code') ? 'has-error' : ''}}">--}}
        {{--{!! Form::label('reference_code', trans('products.ref_code'), ['class' => 'col-md-3 control-label']) !!}--}}
        {{--<div class="col-md-8">--}}
            {{--{!! Form::text('reference_code', null,['class'=>'form-control','placeholder'=>trans('products.ref_code_help')]) !!}--}}
            {{--{!! $errors->first('reference_code', '<p class="help-block">:message</p>') !!}--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--Delete more details--}}
    {{--<div class="form-group {{ $errors->has('more_details') ? 'has-error' : ''}}">--}}
        {{--{!! Form::label('more_details', trans('products.more_details'), ['class' => 'col-md-3 control-label']) !!}--}}
        {{--<div class="col-md-9 form-inline">--}}
            {{--@if(isset($product) && !empty($product->more_details))--}}
                {{--@foreach($product->more_details['attr'] as $index=>$attr)--}}
                {{--<div class="margbtn10">--}}
                    {{--{!!Form::text('more_details[attr][]',$attr,['class'=>'form-control','placeholder'=>trans('products.attr')])!!}--}}
                    {{--{!!Form::text('more_details[value][]',$product->more_details['value'][$index],['class'=>'form-control','placeholder'=>trans('products.val')])!!}--}}
                    {{--<a href="javascript:void(0)" class="btn btn-danger btn-sm remove_field">-</a>--}}
                {{--</div>--}}
                {{--@endforeach--}}
            {{--@else--}}
                {{--<input type="text" name="more_details[attr][]" class="form-control" placeholder="@lang('products.attr')" />--}}
                {{--<input type="text" name="more_details[value][]" class="form-control" placeholder="@lang('products.val')" /> --}}
                {{-- {!!Form::text('more_details[attr][]',null,['class'=>'form-control','placeholder'=>trans('products.attr')])!!}--}}
                {{--{!!Form::text('more_details[value][]',null,['class'=>'form-control','placeholder'=>trans('products.val')])!!} --}}
            {{--@endif--}}
            {{--<a class="btn btn-success add_field_button">+</a>--}}
            {{--<div class="input_fields_wrap"></div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="form-group {{ $errors->has('add_images') ? 'has-error' : ''}}">
        {!! Form::label('add_images', trans('products.additional_images'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 additional_imgs">
            @if(isset($product))
            {!!App\ProductImage::generate_preview($product->images, 'products')!!}
            @endif

            <div class="media">
                <a class="pull-left" href="#">{!!HTML::Image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfields'])!!}</a>
                <a class="pull-right btn btn-success add_imgfile_button">+</a>
                <div class="media-body">{!!Form::file('add_images[]', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}</div>
            </div>
            <div class="input_imgfiles_wrap"></div>
            <p><div class="help-block">{{trans('dash.imgrules')}}</div></p>
            {!! $errors->first('add_images', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
    {!! Form::label('brochure', trans('products.brochure'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!!Form::file('brochure', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}
            <div class="help-block">{{trans('dash.docrules')}}</div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('video_url') ? 'has-error' : ''}}">
        {!! Form::label('video_url', trans('products.prod_video'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('video_url', null,['class'=>'form-control','placeholder'=>trans('products.video_placeholder')]) !!}
            {!! $errors->first('video_url', '<p class="help-block">:message</p>') !!}
            <div class="help-block">{{trans('products.video_help')}}</div>
        </div>
    </div>

<div class="well well-sm">{{trans('products.prod_trade_details')}}</div>
    <div class="form-group {{ $errors->has('production_capacity') ? 'has-error' : ''}}">
        {!! Form::label('production_capacity', trans('products.capacity'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::number('production_capacity', null, ['class' => 'form-control','required'=>'required']) !!}
                </div>
                <div class="col-md-4">                    
                    {!!Form::select('capacity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required'])!!}
                </div>
                <div class="col-md-4 form-inline">
                    <label>{{trans('products.per')}} {!! Form::select('capacity_unit_per',trans('dash.capacity_durations'),null,['class'=>'form-control','required'=>'required'])!!}
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('min_quantity') ? 'has-error' : ''}}">
        {!! Form::label('min_quantity', trans('products.min_quantity'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::number('min_quantity', null, ['class' => 'form-control','required'=>'required']) !!}
                </div>
                <div class="col-md-4">                    
                    {!! Form::select('min_quantity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('fob_price') ? 'has-error' : ''}}">
        {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::select('fob_price_type', trans('dash.fob_price_list'), null,['class'=>'form-control','required'=>'required']) !!}
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                        {!! Form::number('fob_price_from',null,['class'=>'form-control','placeholder'=>trans('dash.min'),'required'=>'required']) !!}
                        </div>
                        <div class="col-md-6">
                        {!! Form::number('fob_price',null,['class'=>'form-control','placeholder'=>trans('dash.max'),'required'=>'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 form-inline">
                    <label>{{trans('products.per')}} {!!Form::select('fob_price_unit',trans('dash.units'), null,['class' => 'form-control'])!!}
                    </label>
                </div>
            </div>
            <div class="help-block">{{trans('products.type_help')}}</div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('fob_price') ? 'has-error' : ''}}">
        {!! Form::label('fob_price', trans('products.payment_type'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div class="row">
                @foreach(App\ProductDetails::$payment_methods as $item=>$label)
                <div class="col-md-3">
                    <label title="{{$item}}">{!!Form::checkbox('payment_type[]', $item, null, ['class'=>'pay_types'])!!} {{trans('dash.'.$label)}}</label>
                    @if($label=='Other')
                        <span id="other_payment_type">
                            {!!Form::label('other_payment',trans('dash.enter_your_payment_type'),['class'=>'control-label'])!!}
                            {!!Form::text('other_payment_type',null,['class'=>'form-control other_paytype'])!!}
                        </span>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('port') ? 'has-error' : ''}}">
        {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            {!!Form::select('port', trans('dash.shipping_ports'), null, ['class'=>'form-control','required'=>'required'])!!}
            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('delivery_time') ? 'has-error' : ''}}">
        {!! Form::label('delivery_time', trans('products.delivery_time'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::number('delivery_time', null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-3">                    
                    {!!Form::select('delivery_duration',trans('dash.delivery_duration'),null,['class'=>'form-control']) !!}
                </div>
            </div>
        </div>
    </div>


    <div class="form-group brands">
        {!!Form::label('packaging',trans('products.pack_details'), ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div class="row"> 
            @foreach($langs as $iso=>$lang)
                <div class="col-md-6">
                    <div class="{{$errors->has('packaging_'.$iso)?'has-error':''}}">
                        {!! Form::label('packaging_'.$iso, '('.$lang['name'].')', ['class'=>'control-label']) !!}
                        <div>
                            {!! Form::textarea('packaging_'.$iso, null,['class'=>'form-control','rows'=>5,'placeholder'=>trans('products.pack_help')]) !!}
                            {!! $errors->first('packaging_'.$iso, '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>

            <div role="tabpanel" class="hidden">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec2{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec2{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;">
                            <div class="panel-body">                                    
                                {!! Form::textarea('lang['.$lang->id.'][packaging]', null, ['class'=>'form-control','rows'=>4,'placeholder'=>trans('products.pack_help')]) !!}
                            </div>
                        </div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        
        <!-- <a href="javascript:history.back(1);" class="btn btn-default">{{trans('products.cancel')}}</a> -->
    </div>
</div>

@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
@include('dashboard.layouts.popup')
<script type="text/javascript">
    $("#product_form").validate({
        rules: {
            "selected_category_id":"required",
            "name_ar": "required",
            "name_en": "required",
            "description_ar": "required",
            "description_en": "required",
            /*"details_ar": "required",
            "details_en": "required",*/
            @if(!isset($product->image) || empty($product->image))
            image: {required: true, accept: "image/jpg,image/jpeg,image/png,image/gif"}
            @endif
        },
        messages: {
            "selected_category_id":"اختر التصنيف",
            "image": {
                required: '@lang("dash.prod_img_required")', 
                accept: '@lang("dash.prod_img_invalid")'
            }
        }, submitHandler: function (form) {
            $('.submit_form').button('loading');
            return true;
        },
        ignore: [] 
    });
</script>
@endsection
@endif