<?php
    use App\Service;
    use App\Helper;
    use App\Product;
    $side_menu = true;
?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">إدارة صفحات المتجر
        <a href="{{route('dashboard.websitepages.create')}}" class="btn btn-sm btn-primary">اضف صفحة جديدة</a>

        @if($user_website->status==2)
            <a href="{{App\UserWebsite::url(null, $user_website->domain)}}" class="btn btn-sm btn-success pull-left" target="_blank">عرض المتجر</a>
        @endif
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>الصورة</th>
                        <th>العنوان</th>
                        <th>نبذة</th>
                        <th>إظهار بالمتجر</th>
                        <th>مضاف للقائمة</th>
                        <th>مضاف للسليدر</th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($pages as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            @if(($item->default))
                                <a href="{{url('frontend/templates/images/slider/'.$item->image)}}" target="_blank"><img src="{{url('frontend/templates/images/slider/'.$item->image)}}"></a>
                            @else
                                {!!Helper::image($item->image, ['width'=>'80'])!!}
                            @endif
                        </td>
                        <td>{!!$item->title!!}</td>
                        <td>{{ str_limit($item->details, 120) }}</td>
                        <td>{!!Helper::sw($item,'active','UserWebsitePage')!!}</td>
                        <td>{!!Helper::sw($item,'inmenu','UserWebsitePage')!!}</td>
                        <td>{!!Helper::sw($item,'inslider','UserWebsitePage')!!}</td>
                        <td nowrap="">
                            <a href="{{url('dashboard/websitepages/'.$item->id)}}" class="btn btn-success btn-xs popup" title="View"><span class="fa fa-eye" /></a>
                            <a href="{{ url('dashboard/websitepages/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit"><span class="fa fa-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['dashboard/websitepages', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="fa fa-trash-o" aria-hidden="true" title="Delete" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete',
                                    'onclick'=>'return confirm("Delete: Are you sure?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection