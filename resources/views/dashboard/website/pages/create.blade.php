@extends('dashboard.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">اضف صفحة جديدة</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => 'dashboard/websitepages', 'class'=>'form-horizontal', 'files'=>true]) !!}
            @include ('dashboard.website.pages.form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection