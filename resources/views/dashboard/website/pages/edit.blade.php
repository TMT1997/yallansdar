@extends('dashboard.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">تعديل: {{ $page->id }}</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($page, [ 'method' => 'PATCH', 'url' => ['dashboard/websitepages', $page->id], 'class' => 'form-horizontal', 'files' => true]) !!}
            @include ('dashboard.website.pages.form', ['submitButtonText' => trans('dash.update')])
            {!! Form::close() !!}
        </div>
    </div>
@endsection