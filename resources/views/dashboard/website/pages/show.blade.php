<?php
    use App\Category;
    use App\Helper;
    $parents = array_reverse($category_parents);
    $service_locations = (!empty($service->locations)) ? explode(',',$service->locations): [];
?>
<div class="white-popup-block" style="min-width: 900px;">
{!! Form::model($service, ['method' => 'PATCH', 'class' => 'form-horizontal', 'id'=>'service_form']) !!}
    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'العنوان', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
    {!!App\Helper::image_field('image', @$page)!!}
    <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
        {!! Form::label('details', 'التفاصيل', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!! Form::textarea('details', null, ['class' => 'form-control', 'id'=>'editor']) !!}
            {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class='form-group'>
        {!!Form::label('active','إظهار الصفحة بالمتجر',['class'=>'col-md-3 control-label'])!!}
        <div class='col-sm-6'>
            {!!Form::hidden('active',0)!!}
            {!!Form::checkbox('active',true,1,['id'=>'active','class'=>'switch','data-on-text'=>'نعم','data-off-text'=>'لا'])!!}
        </div>
    </div>
    <div class='form-group'>
        {!!Form::label('inmenu','اضف إلى القائمة الرئيسية',['class'=>'col-md-3 control-label'])!!}
        <div class='col-sm-6'>
            {!!Form::hidden('inmenu',0)!!}
            {!!Form::checkbox('inmenu',true,1,['id'=>'inmenu','class'=>'switch','data-on-text'=>'نعم','data-off-text'=>'لا'])!!}
        </div>
    </div>
    <div class='form-group'>
        {!!Form::label('inslider','اضف إلى سليدر الرئيسية',['class'=>'col-md-3 control-label'])!!}
        <div class='col-sm-6'>
            {!!Form::hidden('inslider',0)!!}
            {!!Form::checkbox('inslider',true,1,['id'=>'inslider','class'=>'switch','data-on-text'=>'نعم','data-off-text'=>'لا'])!!}
        </div>
    </div>
        {!!Form::close()!!}
    </div>
{!!HTML::script('frontend/js/app/products.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
<script type="text/javascript">
    @if(isset($category_tree))
        $(document).ready(function() {
            $(".select_category").show();
            $('.cat_selector').val('{{$category_tree[0]->id}}');
            $('.cat_selector').trigger('change');
            $('.sub').attr('data-id', {{$category_tree[1]->id}});
            @if(!empty($category_tree[2]->id))
                $('.subsub').attr('data-id', {{$category_tree[2]->id}});
            @endif
            $('#selected_category_id').val('{{$service->category_id}}');
        });
    @endif
</script>