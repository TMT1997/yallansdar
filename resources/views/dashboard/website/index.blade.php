<?php use App\TemplateSteps;
$steps = TemplateSteps::whereUser_id(Auth::id())->first();
$hide_dashboard_icon = true;
?>

@extends('dashboard.layouts.master_reg')

@section('content')
	{{HTML::style('frontend/css/jquery.steps.css')}}
	@if(!empty($user_website->domain) && $user_website->status==2)
		<a href="{{UserWebsite::url(null,$user_website->domain)}}" class="btn btn-success pull-right open_site" target="_blank">عرض المتجر</a>
	@endif
	</legend>
<div id="msform">
	@if (App::isLocale('en'))
		<ul id="progressbar">
		    <li class="active en 1" style="float:left;">@lang('dash.profile')</li>
		    @if($user_profile->user_type)
			    @if($user_profile->user_type[0] === "importer")
			    <li class="en 2" id="change_Step" style="float:left;">@lang('dash.requestfor_quote')</li>
			    @elseif($user_profile->user_type[0] === "service_provider")
			    <li class="en 2" id="change_Step" style="float:left;">@lang('dash.add_new_service')</li>
			    @else
			    <li class="en 2" id="change_Step" style="float:left;">@lang('dash.add_products')</li>
			    @endif
			@else
				<li class="en 2" id="change_Step" style="float:left;">@lang('dash.add_products')</li>
			@endif
		    <!-- <li class="en 3" id="choose_Step" style="float:left;">@lang('dash.choose_template')</li> -->
		    <li class="en 4" id="publish_Step" style="float:left;">@lang('dash.publish')</li>
		    {{-- <li class="en 5" style="float:left;">@lang('dash.upgrade_account')</li> --}}
  		</ul>
	@else
		<ul id="progressbar">
		    <li class="active ar 1" style="float:right;">@lang('dash.profile')</li>
		    @if($user_profile->user_type)
			    @if($user_profile->user_type[0] === "importer")
			    <li class="ar 2" id="change_Step" style="float:right;">@lang('dash.requestfor_quote')</li>
			    @elseif($user_profile->user_type[0] === "service_provider")
			    <li class="ar 2" id="change_Step" style="float:right;">@lang('dash.add_new_service')</li>
			    @else
			    <li class="ar 2" id="change_Step" style="float:right;">@lang('dash.add_products')</li>
			    @endif
			@else
				<li class="ar 2" id="change_Step" style="float:right;">@lang('dash.add_products')</li>
			@endif
		    <!-- <li class="ar 3" id="choose_Step" style="float:right;">@lang('dash.choose_template')</li> -->
		    <li class="ar 4" id="publish_Step" style="float:right;">@lang('dash.publish')</li>
		   {{--  <li class="ar 5" style="float:right;">@lang('dash.upgrade_account')</li> --}}
  		</ul>
	@endif
	<div id="templateStepsNav">
	    <fieldset class="profile">
	    	{!!Form::model($user_profile, ['class'=>'form-horizontal','files'=>true,'id'=>'profileForm'])!!}
		    <section>
		    	@include('dashboard.website.steps.profile.personal')
		    	<a href="#comp_form"><button type="button" class="btn btn-info" id="show_form" style="float: left;background-color: #27AE60;color: white;font-size: 15px;">{{trans('dash.next')}}</button></a>
			</section>
			<div style="display: none;" id="comp_form">
		    	@include('dashboard.website.steps.profile.company')
		    	<div style="text-align: center;float: left;">
				{!!Form::submit(trans('dash.next'),['class'=>'btn btn-info btn_save btn_profile', 'name'=>'btn_save'])!!}
<!-- 				{!!Form::submit(trans('dash.profile_submit'),['class'=>'btn btn-primary btn_submit','name'=>'btn_submit'])!!} -->
				</div>
			</div>
			{!!Form::close()!!}
	    </fieldset>

	    <fieldset class="prod">
		    <section id="product_section">
		    	<!-- @include('dashboard.website.steps.products.addproduct') -->
		    	  {!!Form::open(['class'=>'form-horizontal','id'=>'product_form','files'=>true,'novalidate'=>'']) !!}
				        @include ('dashboard.website.steps.products.full_add_product')
		        	<input type="submit" name="add_pd" class="submit_form next_form_submit" value="{{trans('dash.next')}}"/>
					{!! Form::close() !!}
		    </section>
		  	<!--service provider section-->
		  	<section id="service_provider_section">
		  		<?PHP $show_submit = 1;?>
			  	{!!Form::open(['url'=>'dashboard/services','class'=>'form-horizontal service_ajax','id'=>'service_form_ajax','files'=>true,'novalidate'=>'']) !!}
			        @include ('dashboard.services.form')
			        <input type="submit" name="add_pd2" class="submit_form next_form_submit" value="{{trans('dash.next')}}"/>
					{!! Form::close() !!}
		  	</section>
		  	<!--importer section-->
		  	<section id="importer_section">
		  		<?PHP $show_submit = 1;?>
		  		{!!Form::open(['url'=>'dashboard/request_quotes','class'=>'form-horizontal request_ajax','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
			        @include ('dashboard.deals.request_quotes.form')
		        <input type="submit" name="add_pd3" class="submit_form next_form_submit" value="{{trans('dash.next')}}"/>
				{!! Form::close() !!}
		  	</section>
		    <input type="button" name="previous" class="previous action-button" value="{{trans('dash.sPrevious')}}" style="float: left;" />
	    		<input type="button" name="next" class="next action-button" value="{{trans('dash.next_step')}}" id="product_step" style="float: left;"/>

	    </fieldset>

	    {{--<fieldset class="choose">
		    <section>
		    @include('dashboard.website.steps.templates.choose')
		    </section>
		    @if($steps->select_template)
		    <input type="button" name="next" class="next action-button" value="{{trans('dash.next')}}" style="float: left;" />
		    <input type="button" name="previous" class="previous action-button" value="{{trans('dash.sPrevious')}}" style="float: left;" />
    		@endif
	    </fieldset>--}}

	    <fieldset class="launch">
		    <section>
		    	@include('dashboard.website.steps.templates.choose')
		    	@include('dashboard.website.steps.launch')
		    </section>
		    @if($steps->completed)
		    <input type="button" name="next" class="next action-button" value="{{trans('dash.next')}}" style="float: left;" />
		    <input type="button" name="previous" class="previous action-button" value="{{trans('dash.sPrevious')}}" style="float: left;" />
	    		@endif
		</fieldset>

		<fieldset>
		    <section>
		    	@include('dashboard.website.steps.upgrade')
			</section>
			{{-- <a href="{{url('dashboard')}}"><button class="btn btn-success" style="float: left;width: 200px;
					background: #27AE60;
					font-weight: bold;
					color: white;
					border: 0 none;
					border-radius: 1px;
					cursor: pointer;
					padding: 10px 5px;
					margin: 10px 5px;">{{trans('dash.gotodashboard')}}</button></a> --}}
			<!-- <input type="button" name="previous" class="previous action-button" value="{{trans('dash.sPrevious')}}" style="float: left;" /> -->
		</fieldset>
	</div>
</div>
@endsection

@section('script')
{{HTML::script('frontend/js/jquery.steps.min.js')}}
<!-- 	@include('dashboard.layouts.popup')
 -->
	<script type="text/javascript">

		@if(Session::has('prod_done'))
	        $(window).on('load', function() {
	        	prof_fs = $('.profile');
			 	current_fs = $('.prod');
				next_fs = $('.prod').next();
				//activate next step on progressbar using the index of next_fs
				$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
				$(".2").addClass("active");
				//show the next fieldset
				current_fs.slideUp();
				current_fs.hide();
				prof_fs.hide();
					next_fs.slideDown(1000);
				//hide the current fieldset with style
				 window.scrollTo(0, 0);
			});
		@endif
		@if(Session::has('done_template'))
	        $(window).on('load', function() {
	        	prof_fs = $('.profile');
			 	current_fs = $('.prod');
				next_fs = $('.prod').next();
				//activate next step on progressbar using the index of next_fs
				$(".1").addClass("active");
				$(".2").addClass("active");
				$(".3").addClass("active");
				//show the next fieldset
				current_fs.slideUp();
				current_fs.hide();
				prof_fs.hide();
					next_fs.slideDown(1000);
				//hide the current fieldset with style
				 window.scrollTo(0, 0);
			});
		@endif
		@if(Session::has('done_publish'))
	        $(window).on('load', function() {
	        	prof_fs = $('.profile');
			 	current_fs = $('.choose');
				next_fs = $('.choose').next();
				prod_fs = $('.prod');
				//activate next step on progressbar using the index of next_fs
				$(".1").addClass("active");
				$(".2").addClass("active");
				$(".3").addClass("active");
				$(".4").addClass("active");
				//show the next fieldset
				current_fs.slideUp();
				current_fs.hide();
				prof_fs.hide();
				prod_fs.hide();
					next_fs.slideDown(1000);
				//hide the current fieldset with style
				 window.scrollTo(0, 0);
			});
		@endif

		function getTemplate($sec)
	{
		$output = '';
		if($sec=='products') {
			$output='<div class="frec">'+
	    	'{!!Form::text('main_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name'), 'required'=>'required'])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='oproducts') {
			$output='<div class="frec">'+
	    	'{!!Form::text('other_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='website') {
			$output='<div class="frec">'+
	    	'{!!Form::text('website[]',null,['class'=>'form-control','placeholder'=>trans('dash.website')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='product_we_source') {
	    	$output='<div class="frec">'+
	    	'{!!Form::text('product_we_source[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!} '+
	    	'{!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!} '+
	    	'{!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!} <span>({{trans("dash.yearly")}})</span>'+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
			if($sec=='telephone') {
	    	$output='<div class="frec">'+
	    	'{!!Form::number('tel_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::number('telephone[]', null,['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='mobile') {
	    	$output='<div class="frec">'+
	    	'{!!Form::number('mob_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::number('mobile[]', null,['class'=>'form-control','placeholder'=>trans('dash.mobile')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		return $output;
	}


	$(document).on('click', '.add_new_field', function(e){
	    e.preventDefault();
	    var $ft = $(this).data('ft');
	    var max_fields = $(this).data('max');
	    var count_childs = $(this).siblings('.more_fields').children('.frec').length;
	    count_childs = count_childs+1;
	    if(count_childs < max_fields){
	        $wrapper = $(this).siblings('.more_fields');
	        $wrapper.append(getTemplate($ft));
	    } else {
	        alert('Sorry, you cannot add more than '+max_fields+' inputs');
	    }
	});
	$(document).on("click",".remove_field", function(e){
	    e.preventDefault(); $(this).parent('div').remove();
	});


	$(document).ready(function() {
//	  $('.popup').magnificPopup({type:'ajax'});
        function onSubmit()
        {
            var fields = $("input[name='business_type[]']").serializeArray();
            if (fields.length === 0)
            {
                alert('من فضلك اختر نشاط الشركة ');
                // cancel submit
                return false;
            }
            else
            {

            }
        }

		// register event on form, not submit button
        $('#profileForm').submit(onSubmit)
	});



	</script>
	{!!HTML::script('frontend/js/registerationjs.js')!!}
@endsection