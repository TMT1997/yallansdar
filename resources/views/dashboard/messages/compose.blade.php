<?php use App\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.compose')}}</legend>

	{!!Form::model($message, ['class'=>'form-horizontal'])!!}
	{!!Form::hidden('id', null)!!}
    <div class="well well-sm">
	    {!!Form::submit(trans('dash.send'), ['class'=>'btn btn-primary'])!!}
	    {!!Form::submit(trans('dash.save_to_drafts'), ['class'=>'btn btn-default', 'name'=>'btn_draft'])!!}
	    <a href="{{url('dashboard/messages')}}" class="btn btn-default">{{trans('dash.cancel')}}</a>
	</div>

	    <div class="form-group">
	    	<div class="col-md-2 control-label">
	    		<a href="#">{{trans('dash.send_to')}}:</a>
	    	</div>
	    	<div class="col-md-8">
	    		{!!Form::select('contacts[]', $contacts->lists('first_name','id'), null, ['class'=>'form-control contactsList select2', 'multiple'=>'multiple'])!!}
	    	</div>
	    	<div class="col-md-2">
	    		<a href="{{url('dashboard/quickadd/contact')}}" class="btn btn-success popup">{{trans('dash.add_contact')}}</a>
	    	</div>
	    </div>

	    <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
		    {!! Form::label('subject', trans('dash.subject'), ['class' => 'col-md-2 control-label']) !!}
		    <div class="col-md-10">
		        {!! Form::text('subject', null, ['class' => 'form-control', 'required' => 'required']) !!}
		        {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
		    </div>
		</div>

		<div class="form-group">
			<div class="col-md-12">
				{!!Form::textarea('body', null, ['class'=>'form-control', 'id'=>'editor'])!!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('attachments') ? 'has-error' : ''}}">
	        {!! Form::label('attachments', trans('dash.attachments'), ['class' => 'col-md-2 control-label']) !!}
	        <div class="col-md-10 form-inline">
	            {!!Form::file('attachment[]', ['class'=>'form-control'])!!}
	            <a class="btn btn-success add_field_button">+</a>

	            <div class="input_fields_wrap"></div>
	        </div>
	    </div>
	{!!Form::close()!!}

@endsection

@section('script')
	@include('dashboard.layouts.popup')
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection