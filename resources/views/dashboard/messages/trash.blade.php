<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.trash')}}</legend>

    <div class="well well-sm">
        {!!Form::open(['class'=>'form-inline'])!!}
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-warning">{{trans('dash.restore')}}</a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-danger">{{trans('dash.delete_forever')}}</a>
            </div>
            <div class="form-group">
                <select class="form-control"><option>{{trans('dash.more')}}</option></select>
            </div>
        {!!Form::close()!!}
    </div>

    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox" value='1' /></th>
                <th>{{trans('dash.from')}}</th>
                <th>{{trans('dash.subject')}}</th>
                <th>{{trans('dash.date')}}</th>
                <th>{{trans('dash.country')}}</th>
                <th>{{trans('dash.actions')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $item)
            <tr id="tr_{{$item->id}}">
                <td><input type="checkbox" value='1' /></td>
                <td>
                    @if(isset($item->contact))
                        {{ $item->contact->name }}
                    @endif
                </td>
                <td>{{ $item->subject }}</td>
                <td>{{ $item->created_at }}</td>
                <td>country</td>
                <td>
                    <a href="{{Message::url($item)}}" class="btn btn-success btn-sm" title="Show">{{trans('dash.show')}}</a>
                    <?php $dis = ($item->approved!=2)?'disabled="disabled"':''; ?>
                
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/inbox')}}' class="btn btn-warning btn-sm btn_restore" title="{{trans('messages.restore_prod')}}"><i class="fa fa-undo"></i></a>
                    {!!Form::open(['method'=>'POST','url'=>['dashboard/messages/perma-delete/'.$item->id],'style'=>'display:inline'])!!}
                    {!!Form::button('<i class="fa fa-trash" title="'.trans("dash.delete").'"></i>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Delete Message Permenantly',
                        'onclick'=>'return confirm("'.trans("dash.confirm_delete").'")'
                    ))!!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if($messages->count()==0)
        <p class="text-center">No messages found</p>
    @endif
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection