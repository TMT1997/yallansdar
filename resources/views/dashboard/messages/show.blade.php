<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')

	    <a href="javascript:history.back(1);" class="btn btn-default">@lang('dash.goback')</a>
	    <a href="#del_msg_perma" class="btn btn-default btn_perma_del" data-id="{{$message->id}}" data-url="{{url('dashboard/messages/perma-delete')}}">@lang('dash.perma_delete')</a>
	    <br><hr>
    <div class="well well-sm">

	    <legend>{{$message->subject}}</legend>

	    <ul>
	    	<li>@lang('dash.from'):	<a href="#">{{Message::from($message)}}</a></li>
	    	<li>@lang('dash.to'): 	<a href="#">{{@$message->user_to->name}}</a></li>
	    	<li>@lang('dash.sent_date'): {{$message->created_at}} - ({{Carbon\Carbon::parse($message->created_at)->diffForHumans()}})</li>
	    </ul>

	    <div class="well">
		    <p>
		    	{!!$message->body!!}
		    </p>

			<hr>
			@if($message->attachments->count()>0) 
				<legend><i class="fa fa-paperclip"></i> Attachments </legend>
				<ul>
				@foreach($message->attachments as $file)
					<li><a href="{{url('uploads/'.$file->file)}}" target="_blank">{{$file->file}}</a></li>
				@endforeach
				</ul>
			@endif 

			<div class="well">
				<a href="javascript:void(0)" class="btn_reply">Click here to Reply</a>
				<div class="reply_box hidden">
				{!!Form::open(['url'=>'dashboard/messages/reply'])!!}
					{!!Form::hidden('replied_on', $message->id)!!}
					<div class="form-group">
						{!!Form::textarea('body', null, ['class'=>'form-control','id'=>'editor'])!!}
					</div>
					<div class="form-group">
						{!!Form::submit('Reply', ['class'=>'btn btn-loading btn-primary'])!!}
					</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
	 </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection