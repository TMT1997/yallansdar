<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')
@section('title') @lang('dash.inbox') @endsection
@section('content')
    <legend>@lang('dash.inbox')
        <a href="{{url('dashboard/messages/compose')}}" class="btn btn-success pull-left">@lang('dash.send_message')</a>
    </legend>

    <div class="well well-sm">
        {!!Form::open(['class'=>'form-inline'])!!}
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-primary">@lang('dash.report_spam')</a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-danger">@lang('dash.trash')</a>
            </div>
            <div class="form-group">
                <select class="form-control">
                    <option>@lang('dash.move_to')</option>
                    @foreach($folders as $folder)
                        <option value="{{$folder->id}}">{{$folder->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control"><option>@lang('dash.more')</option></select>
            </div>
        {!!Form::close()!!}
    </div>

    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox"></th>
                <th>@lang('dash.from')</th>
                <th>@lang('dash.subject')</th>
                <th>@lang('dash.date')</th>
                <th>@lang('dash.actions')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $item)
            <tr id="tr_{{$item->id}}" class="{{($item->read)?'msg_read':'msg_unread'}}">
                <td><input type="checkbox"></td>
                <td>{!!Message::star_sw($item,'starred','Messages\Message')!!} {{Message::from($item)}}</td>
                <td><a href="{{Message::url($item)}}">@if($item->attachments->count()>0) <i class="fa fa-paperclip"></i> @endif {{ $item->subject }}</a></td>
                <td>{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</td>
                <td nowrap="">
                    <a href="{{Message::url($item)}}" class="btn btn-success btn-xs" title="Preview">@lang('dash.preview')</a>

                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/spam')}}' class="btn btn-warning btn-xs btn_spam" title="Mark as Spam">@lang('dash.spam_msg')</a>

                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/trash')}}' class="btn btn-danger btn-xs btn_del" title="Trash">@lang('dash.trash_msg')</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if($messages->count()==0)
        <p class="text-center">No messages found</p>
    @endif
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection