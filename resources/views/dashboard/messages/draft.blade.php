<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.drafts')}}
        <a href="{{url('dashboard/messages/compose')}}" class="btn btn-default pull-right">{{trans('dash.send_message')}}</a>
    </legend>

    <div class="well well-sm">
        {!!Form::open(['class'=>'form-inline'])!!}
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-danger">{{trans('dash.discard')}}</a>
            </div>
            <div class="form-group">
                <select class="form-control"><option>{{trans('dash.more')}}</option></select>
            </div>
        {!!Form::close()!!}
    </div>

    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox"></th>
                <th>{{trans('dash.draft')}}</th>
                <th>{{trans('dash.subject')}}</th>
                <th>{{trans('dash.date')}}</th>
                <th>{{trans('dash.country')}}</th>
                <th>{{trans('products.actions')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $item)
            <tr id="tr_{{$item->id}}" class="{{($item->read)?'msg_read':'msg_unread'}}">
                <td><input type="checkbox"></td>
                <td>{!!Message::star_sw($item,'starred','Messages\Message')!!} {{Message::contact_name($item)}}</td>
                <td><a href="{{url('dashboard/messages/compose/'.$item->id)}}">{{ $item->subject }}</a></td>
                <td>{{Message::date($item)}}</td>
                <td>country</td>
                <td>
                    <a href="{{Message::url($item)}}" class="btn btn-success btn-xs" title="{{trans('messages.view_prod')}}"><span class="glyphicon glyphicon-eye-open"/></a>

                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/trash')}}' class="btn btn-danger btn-xs btn_del" title="{{trans('messages.trash')}}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection