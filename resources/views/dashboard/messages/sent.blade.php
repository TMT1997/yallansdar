<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.sent')}}</legend>

    <div class="well well-sm">
        {!!Form::open(['class'=>'form-inline'])!!}
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-danger">{{trans('dash.delete')}}</a>
            </div>
            <div class="form-group">
                <select class="form-control"><option>{{trans('dash.more')}}</option></select>
            </div>
        {!!Form::close()!!}
    </div>

    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox" value='1' /></th>
                <th>{{trans('dash.to')}}</th>
                <th>{{trans('dash.subject')}}</th>
                <th>{{trans('dash.date')}}</th>
                <th>{{trans('dash.country')}}</th>
                <th>{{trans('products.actions')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $item)
            <tr id="tr_{{$item->id}}">
                <td><input type="checkbox" value='1' /></td>
                <td>{!!Message::star_sw($item,'starred','Messages\Message')!!} {{Message::contact_name($item)}}</td>
                <td>{{ $item->subject }}</td>
                <td>{{ $item->created_at }}</td>
                <td>country</td>
                <td>
                    <a href="{{Message::url($item)}}" class="btn btn-success btn-xs" title="Show">{{trans('dash.preview')}}</a>

                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/trash')}}' class="btn btn-danger btn-xs btn_del" title="{{trans('messages.trash')}}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection