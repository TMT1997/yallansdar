<?php use App\Messages\Message; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.spam')}}</legend>

    <div class="well well-sm">
        {!!Form::open(['class'=>'form-inline'])!!}
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i></a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-primary">{{trans('dash.not_spam')}}</a>
            </div>
            <div class="form-group">
                <a href="#" class="btn btn-sm btn-danger">{{trans('dash.delete_forever')}}</a>
            </div>
            <div class="form-group">
                <select class="form-control"><option>{{trans('dash.more')}}</option></select>
            </div>
        {!!Form::close()!!}
    </div>

    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
            <tr>
                <th><input type="checkbox"></th>
                <th>{{trans('dash.from')}}</th>
                <th>{{trans('dash.subject')}}</th>
                <th>{{trans('dash.date')}}</th>
                <th>{{trans('dash.country')}}</th>
                <th>{{trans('dash.actions')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($messages as $item)
            <tr id="tr_{{$item->id}}" class="{{($item->read)?'msg_read':'msg_unread'}}">
                <td><input type="checkbox"></td>
                <td>{!!Message::star_sw($item,'starred','Messages\Message')!!} {{Message::contact_name($item)}}</td>
                <td>{{ $item->subject }}</td>
                <td>{{ $item->created_at }}</td>
                <td>country</td>
                <td>
                    <a href="{{Message::url($item)}}" class="btn btn-success btn-xs" title="{{trans('messages.view_prod')}}"><span class="glyphicon glyphicon-eye-open"/></a>

                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/messages/move/notspam')}}' class="btn btn-danger btn-xs btn_spam" title="Mark as Not spam">{{trans('dash.not_spam')}}</a>

                    {!! Form::open([
                        'method'=>'DELETE','url' => ['dashboard/messages', $item->id],'style'=>'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash" title="'.trans("messages.del_prod").'"></i>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Message Permenantly',
                        'onclick'=>'return confirm("'.trans("messages.confirm_delete").'")'
                    )) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/messages.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection