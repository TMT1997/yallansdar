@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('deals.edit_offer')}}: {{ $sell_offer->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($sell_offer, [
        'method' => 'PATCH',
        'url' => ['/dashboard/sell_offers', $sell_offer->id],
        'class' => 'form-horizontal', 'id'=>'deals_form',
        'files' => true,'novalidate'=>''
    ]) !!}
    @include ('dashboard.deals.sell_offers.form', ['submitButtonText'=>trans('dash.update')])
    {!! Form::close() !!}
@endsection