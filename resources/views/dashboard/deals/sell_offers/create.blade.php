@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.sell_offers')}}</legend>

    <div class="help-block">{{trans('dash.sell_offer_desc')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/sell_offers','class'=>'form-horizontal','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.deals.sell_offers.form')
    {!! Form::close() !!}

@endsection