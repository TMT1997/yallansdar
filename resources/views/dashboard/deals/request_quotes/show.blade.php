@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    {{-- <div class="panel-heading">Deal {{ $deal->id }}</div> --}}
                    <div class="panel-body">

                        <a href="{{ url('dashboard/deals/' . $deal->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Author"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/deals', $deal->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete deal',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        {{-- <th>ID</th><td>{{ $deal->id }}</td> --}}
                                    </tr>
                                    <tr><th> Name ar</th><td> {{ $deal->name_ar }} </td></tr>
                                    <tr><th> Name en </th><td> {{ $deal->name_en }} </td></tr>
                                    <tr><th> Detials ar </th><td> {!! $deal->details_ar !!} </td></tr>
                                    <tr><th> Detials en </th><td> {!! $deal->details_en !!} </td></tr>
                                    <tr><th> OrderQuantity </th><td> {{ $deal->order_quantity }} </td></tr>
                                    <tr><th> Unit </th><td> {{ $deal->unit }} </td></tr>
                                    <tr><th> Duration From </th><td> {{ $deal->duration_from }} </td></tr>
                                    <tr><th> Duration To </th><td> {{ $deal->duration_to }} </td></tr>
                                    <tr><th> Expired at </th><td> {{ $deal->expired_at }} </td></tr>
                                    <tr><th> Purchase Unit </th><td> {{ $deal->purchase_unit }} </td></tr>
                                    <tr><th> Payment Terms </th><td> {{ $deal->payment_terms }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection