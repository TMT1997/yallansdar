<?php
$countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
<div class="well well-sm">{{trans('dash.request_information')}}</div>
    <div class="form-group {{ $errors->has('request_for') ? 'has-error' : ''}}">
        <div class="col-md-12">
            @foreach(trans('dash.rfq_request_for') as $val=>$item)
                @if($val=="product")
                <label>{!!Form::radio('request_for', $val, 1, ['class'=>'request_for required'])!!} {{$item}}</label>
                @else
                @endif
            @endforeach
        </div>
    </div>
     <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('products.prod_cat'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div> 
    @include('dashboard.categories.select_category', ['selected_category_id'=>@$quote_request->category_id, 'keep_parent_id'=>true])

    <div class="form-group">
    @foreach($langs as $iso=>$lang)
        <div class="col-md-6">
            <div class="{{$errors->has('name_'.$iso)?'has-error':''}}">
                {!! Form::label('name_'.$iso, trans('dash.Service_Product_Name').' ('.$lang['name'].')', ['class'=>'control-label ']) !!}
                <div>
                    @if($iso == "en")
                    {!! Form::text('name_'.$iso, null,['class'=>'form-control prod_cat_search check_english','required'=>'required']) !!}
                    {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!}
                    @else
                    {!! Form::text('name_'.$iso, "",['class'=>'form-control prod_cat_search']) !!}
                    {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!} 
                    @endif
                </div>
            </div>
            <div class="{{$errors->has('details_'.$iso)?'has-error':''}}">

                {!! Form::label('details_'.$iso, trans('dash.req_in_details').' ('.$lang['name'].')', ['class'=>'control-label']) !!}

                <div>
                    {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso]) !!}
                    {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    @endforeach
    </div>

    <div class="form-group {{ $errors->has('add_images') ? 'has-error' : ''}}">
        {!! Form::label('add_images', trans('dash.desc_image'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 additional_imgs">
            @if(isset($quote_request))
                <div class="row">
                    @foreach($quote_request->images as $item)
                    <div class="col-xs-6 col-sm-4 col-md-3 infile">
                        <div class="thumbnail">
                            {!!App\Helper::image($item->image,['style'=>'max-height:60px;'])!!}
                            <div class="caption">
                                <p>{{$item->name}}</p>
                                {!!Form::button('<i class="fa fa-trash" title="Delete"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\Deals\QuoteRequestImage").'")'])!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endif
            <div class="media">
                <a class="pull-left" href="#">{!!HTML::Image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfields'])!!}</a>
                <a class="pull-right btn btn-success add_imgfile_button">+</a>
                <div class="media-body">{!!Form::file('add_images[]', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}</div>
            </div>
            <div class="input_imgfiles_wrap"></div>
            <p><div class="help-block">{{trans('products.img_info')}}</div></p>
            {!! $errors->first('add_images', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group order_quantity {{ $errors->has('order_quantity') ? 'has-error' : ''}}">
        {!! Form::label('order_quantity', trans('dash.est_order_quantity'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('order_quantity', null,['class'=>'form-control','required'=>'required']) !!}
            <label>{!!Form::select('unit',trans('dash.units'),null,['class'=>'form-control select2','required'=>'required']) !!}</label>
        </div>
    </div>

    <div class="form-group durations {{ $errors->has('duration_from') ? 'has-error' : ''}}">
        {!! Form::label('duration_from', trans('dash.service_req_duration'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        {!! Form::text('duration_from',null,['class'=>'form-control expired_datepicker','placeholder'=>trans('dash.from'),'required'=>'required']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        {!! Form::text('duration_to', null, ['class'=>'form-control expired_datepicker','placeholder'=>trans('dash.to'),'required'=>'required']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            {!! $errors->first('duration_from', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.rfq_expiry_date'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class' => 'form-control expired_datepicker','required'=>'required']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            {!! $errors->first('expired_at', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

<div class="well well-sm">{{trans('dash.trade_information')}}</div>
    {!!Form::hidden('supplier_location', '')!!}
    <div class="form-group {{ $errors->has('supplier_location') ? 'has-error' : ''}}">
        {!! Form::label('supplier_location',trans('dash.Preferred_Supplier_Location'),['class'=>'col-md-3 control-label'])!!}
        <div class="col-md-8 form-inline">
            {!! Form::select('supplier_location[]',$countries,null,['class'=>'form-control select2','multiple'=>'multiple'])!!}
            {!! $errors->first('supplier_location', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('frequency') ? 'has-error' : ''}}">
        {!! Form::label('frequency', trans('dash.Sourcing_frequency'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('frequency',trans('dash.source_frequencies'),null,['class'=>'form-control']) !!}
            {!! $errors->first('frequency', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group annual_purchase {{ $errors->has('annual_purchase') ? 'has-error' : ''}}">
        {!! Form::label('annual_purchase', trans('dash.Annual_Purchase_Volume'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('annual_purchase', null,['class'=>'form-control']) !!}
            <label>{!!Form::select('purchase_unit',trans('dash.units'),null,['class'=>'form-control select2']) !!}</label>
        </div>
    </div>

    {{-- <div class="form-group brand {{ $errors->has('brand') ? 'has-error' : ''}}">
        {!! Form::label('brand', trans('dash.Preferred_Brand_Name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('brand', null,['class'=>'form-control']) !!}
        </div>
    </div> --}}

    <div class="form-group unit_price {{ $errors->has('unit_price') ? 'has-error' : ''}}">
        {!! Form::label('unit_price', trans('dash.Preferred_Unit_Price'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('unit_price', null,['class'=>'form-control','placeholder'=>trans('dash.unit_price')]) !!}
            <label>{!!Form::select('currency',trans('dash.fob_price_list'),null,['class'=>'form-control']) !!}</label>
        </div>
    </div>

    <div class="form-group payment_terms {{ $errors->has('payment_terms') ? 'has-error' : ''}}">
        {!!Form::hidden('payment_terms', '')!!}
        {!! Form::label('payment_terms', trans('dash.payment_terms'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                @foreach(trans('dash.payment_methods_list') as $item=>$label)
                <div class="col-md-3">
                    <label title="{{$item}}">{!!Form::checkbox('payment_terms[]',$item,null,['class'=>'pay_types','required'=>'required'])!!} {{$label}}</label>
                    @if($label=='Other')
                        <span id="other_payment_type">
                            {!!Form::label('other_payment', 'Enter your payment type',['class'=>'control-label'])!!}
                            {!!Form::text('other_payment_type', null,['class'=>'form-control other_paytype'])!!}
                        </span>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="form-group port {{ $errors->has('port') ? 'has-error' : ''}}">
        {!! Form::label('port', trans('dash.destination_port'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('port', null, ['class' => 'form-control check_english','placeholder'=>trans('dash.destination_port')]) !!}
            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    @if(Auth::user()->admin)
    <div class="well well-sm">
        <div class="form-group importer_info {{ $errors->has('im_name') ? 'has-error' : ''}}">
            {!! Form::label('im_name', trans('dash.importer_info'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9 form-inline">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name {!!Form::text('im_name',null,['class'=>'form-control'])!!}</label>
                    </div>
                    <div class="form-group">
                        <label>Phone {!!Form::text('im_phone',null,['class'=>'form-control'])!!}</label>
                    </div>
                    <div class="form-group">
                        <label>Country {!!Form::select('im_country',$countries, null,['class'=>'form-control'])!!}</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email {!!Form::email('im_email',null,['class'=>'form-control'])!!}</label>
                    </div>
                    <div class="form-group">
                        <label>Website {!!Form::text('im_website',null,['class'=>'form-control'])!!}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@if(!isset($show_submit))
<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText :trans('dash.add_quote_request'), ['class' => 'btn btn-lg btn-primary submit_form']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('products.cancel')}}</a>
    </div>
</div>
@endif
@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
@include('dashboard.layouts.popup');
<script type="text/javascript">
    $('#deals_form').validate({
        rules: {
            "name_ar": "required",
            "name_en": "required",
            "details_ar": "required",
            "details_en": "required",
        },
        messages: {
            'service_product_name':'من فضلك ادخل اسم المنتج / الخدمة',
            "details_ar": "من فضلك ادخل تفاصيل عرض السعر",
            "details_en": "من فضلك ادخل تفاصيل عرض السعر",
        }
    });
</script>
@endsection