@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('deals.edit_quote_request')}}: {{ $quote_request->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($quote_request, [
        'method' => 'PATCH',
        'url' => ['/dashboard/request_quotes', $quote_request->id],
        'class' => 'form-horizontal', 'id'=>'deals_form',
        'files' => true, 'novalidate'=>''
    ]) !!}
    @include ('dashboard.deals.request_quotes.form', ['submitButtonText' => 'Update'])
    {!! Form::close() !!}
@endsection