@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.request_for_quote')}}</legend>

    <div class="help-block">{{trans('dash.requestForQuote_desc')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/request_quotes','class'=>'form-horizontal','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.deals.request_quotes.form')
    {!! Form::close() !!}

@endsection