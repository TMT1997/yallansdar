@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-4">{{trans('dash.deals')}}</div>
                <div class="col-md-8">
                    <div class="dropdown pull-right">
                      <button class="btn btn-success btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> {{trans('dash.create_new_deal')}} <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="{{url('dashboard/sell_offers/create')}}">{{trans('dash.sell_offers')}}</a></li>
                        <li><a href="{{url('dashboard/request_quotes/create')}}">{{trans('dash.request_for_quote')}}</a></li>
                        <li><a href="{{url('dashboard/partnerships/create')}}">{{trans('dash.partnership_deal')}}</a></li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div role="tabpanel">
                <ul class="nav nav-tabs product_status" role="tablist">
                    @foreach(App\Deals\Deal::$statues as $state_id=>$status)
                        <?php $counter = $deals->filter(function($row) use($state_id){
                                return $row->status==$state_id;
                            })->count(); ?>
                        @if($status=='All')
                            <?php $counter = $deals->count(); ?>
                        @endif
                        <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('dash.deal_status')[$state_id]}} ({{$counter}})</a></li>
                    @endforeach
                </ul>
            
                <div class="tab-content deals_grid" style="margin-top:5px; padding:5px;"></div>
            </div>            
        </div>
    </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/products.js')!!}
    <script type="text/javascript">
        $('body').on('click', '.product_status a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var status = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/deals/grid')}}?status='+status,
                type: 'GET',
                beforeSend: function() {
                    $('.deals_grid').html('{{trans('loading')}}');
                }
            })
            .done(function(response) {
                $('.deals_grid').html(response);
            })
            .fail(function(output) {
                auth_check(output);
                $('.deals_grid').html('Error retreive deals data, please try again');
            })
            .always(function() {
            });
        });
        $(document).ready(function() {
            $('.product_status a[data-toggle="tab"]:first').trigger('click');
        });
    </script>
@endsection