<div class="form-group selectproduct">
    {!!Form::label('product_id', 'Select Product', ['class'=>'col-md-3 control-label'])!!}
    <div class="col-md-8">
        {!!Form::select('product_id', $products->lists('id','id'), null, ['class'=>'form-control'])!!}
    </div>
</div>

<div class="addproductinputs">
    <div class="well">
        <legend>Quick Add Product</legend>
        <div role="tabpanel">
            <?php $count=0; ?>
            <ul class="nav nav-tabs" role="tablist">
                @foreach($languages as $lang)
                    <?php $active = ($count==0) ? 'class="active"':''; ?>
                    <li {!!$active!!}><a href="#addprod{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                    <?php $count++ ?>
                @endforeach
            </ul>
            <?php $count=0; ?>
            <div class="tab-content">
                @foreach($languages as $lang)
                    <?php $active = ($count==0) ? 'active':''; ?>
                    <div role="tabpanel" class="tab-pane fade in {!!$active!!}" id="addprod{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;">
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                    {!! Form::label('name', Lang::get('products.prod_name',[],$lang->iso2), ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-8">
                                        {!! Form::text('langData['.$lang->id.'][name]', null,['class'=>'form-control']) !!}
                                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                    {!!Form::label('description',Lang::get('products.prod_desc',[],$lang->iso2), ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-8">
                                        {!! Form::textarea('langData['.$lang->id.'][description]', null, ['class' => 'form-control', 'id'=>'editor', 'rows'=>4, 'placeholder'=>trans('products.desc_help')]) !!}
                                        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $count++ ?>
                    @endforeach
                </div>
            </div>
            @include('dashboard.categories.select_category')

        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            {!! Form::label('image', trans('products.prod_img'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                <div class="media">
                    <a class="pull-left" href="#">{!!HTML::image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfieldmain','style'=>'max-width:120px;'])!!}</a>
                    <div class="media-body">
                        <h4 class="media-heading">{!!Form::file('image', ['class'=>'form-control img_browse','data-fid'=>'main'])!!}</h4>
                        <p><div class="help-block">{{trans('products.img_info')}}</div></p>
                    </div>
                </div>
                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>