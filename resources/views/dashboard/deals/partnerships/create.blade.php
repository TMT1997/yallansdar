@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.partnership_deals')}}</legend>

    <div class="help-block">{{trans('dash.partnership_deals_desc')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/partnerships','class'=>'form-horizontal','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.deals.partnerships.form')
    {!! Form::close() !!}

@endsection