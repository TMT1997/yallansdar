@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('deals.edit_partnership')}}: {{ $partnership->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($partnership, [
        'method' => 'PATCH',
        'url' => ['/dashboard/partnerships', $partnership->id],
        'class' => 'form-horizontal', 'id'=>'deals_form',
        'files' => true, 'novalidate'=>''
    ]) !!}
    @include ('dashboard.deals.partnerships.form', ['submitButtonText' =>trans('dash.update')])
    {!! Form::close() !!}
@endsection