@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">deal {{ $deal->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('dashboard/deals/' . $deal->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Author"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/deals', $deal->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete deal',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $deal->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $deal->name }} </td></tr><tr><th> Photo </th><td> {{ $deal->photo }} </td></tr><tr><th> Brief </th><td> {{ $deal->brief }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection