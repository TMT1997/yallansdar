@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.edit')}} {{trans('dash.deal_alert')}}: {{ $alert->id }}</legend>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($alert, [
        'method' => 'PATCH',
        'url' => ['/dashboard/alerts', $alert->id],
        'class' => 'form-horizontal', 'id'=>'deals_form',
        'files' => true, 'novalidate'=>''
    ]) !!}
    @include ('dashboard.deals.alerts.form', ['submitButtonText' => 'Update'])
    {!! Form::close() !!}
@endsection