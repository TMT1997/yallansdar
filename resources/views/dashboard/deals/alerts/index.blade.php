<?php use App\Deals\Alert; ?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-4">{{trans('dash.manage_deal_alerts')}}</div>
                <div class="col-md-8">
                    <div class="pull-right">
                      <a href="{{url('dashboard/alerts/create')}}" class="btn btn-success btn-sm"> {{trans('dash.create_new_alert')}}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="help-block">Now manage your deal alerts,to get selective deals set deal type on particular product and receive only focused deals.</div>

            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>{{trans('dash.id')}}</th>
                        <th>{{trans('dash.product_service')}}</th>
                        <th>{{trans('dash.type')}}</th>
                        <th>{{trans('dash.relevancy')}}</th>
                        <th>{{trans('dash.status')}}</th>
                        <th>{{trans('dash.last_update')}}</th>
                        <th>{{trans('products.actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($alerts as $item)
                    <tr id="tr_{{$item->id}}">
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->service_product_name }}</td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->relevancy }}</td>
                        <td>{{ App\Deals\Deal::$statues[$item->status] }} </td>
                        <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
                        <td>
                            <a href="{{ url('/dashboard/alerts/' . $item->id) }}" class="btn btn-success btn-sm" title="{{trans('alerts.view_prod')}}"><span class="glyphicon glyphicon-eye-open"/></a>
                            <?php $dis = ($item->approved!=2)?'disabled="disabled"':''; ?>
                            <a href="{{route('dashboard.alerts.edit',$item->id)}}" class="btn btn-primary btn-sm" {{$dis}} title="{{trans('alerts.edit_prod')}}"><span class="glyphicon glyphicon-pencil"/></a>

                            @if($item->status!=4)
                                <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/alerts/trash')}}' class="btn btn-danger btn-sm btn_del" title="{{trans('alerts.trash_prod')}}"><i class="fa fa-trash"></i></a>
                            @else
                                <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/alerts/restore')}}' class="btn btn-warning btn-sm btn_restore" title="{{trans('alerts.restore_prod')}}"><i class="fa fa-undo"></i></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/dashboard/alerts', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-trash" title="{{trans("alerts.del_prod")}}"></i>', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete Product Permenantly',
                                            'onclick'=>'return confirm("{{trans("alerts.confirm_delete")}}")'
                                    )) !!}
                                {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($alerts->count()==0)
                <p class="text-center">No alerts found</p>
            @endif
        </div>
    </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/products.js')!!}
@endsection