<?php
    use App\Deals\Alert;
    use App\Helper;
?>    
    <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
        {!! Form::label('type', trans('dash.type_of_alert'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-7">
            <div class="row"> 
                @foreach(trans('dash.alert_types') as $type=>$label)
                    <div class="col-md-4">
                        <label>{!!Form::checkbox('type[]', $type, null, [])!!} {{$label}}</label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group service_product_name {{ $errors->has('service_product_name') ? 'has-error' : ''}}">
        {!! Form::label('service_product_name', trans('dash.Service_Product_Name'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('service_product_name', null, ['class'=>'form-control prod_cat_search','required'=>'required']) !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('dash.category'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div>
    @include('dashboard.categories.select_category')

    <div class="form-group {{ $errors->has('relevancy') ? 'has-error' : ''}}">
        {!! Form::label('relevancy', trans('dash.relevancy_of_alerts'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-5 form-inline">
            {!! Form::select('relevancy',trans('dash.cat_relevancy'),null,['class'=>'form-control','required'=>'required']) !!}
            {!!Helper::popover(trans('dash.relevancy_of_alerts'), trans('dash.alert_relevancy_desc'))!!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('receive_from') ? 'has-error' : ''}}">
        {!! Form::label('receive_from', trans('dash.want_toreceive_alertfrom'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('receive_from',trans('dash.alert_receive_from'),null,['class'=>'form-control alert_from']) !!}
            {!! $errors->first('receive_from', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    @for($i=1;$i<=3;$i++)
    <div class="form-group specific_countries">
        {!! Form::label('countries', trans('dash.country').' '.$i, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('countries[]', $countries, null, ['class'=>'form-control select2']) !!}
        </div>
    </div>
    @endfor
    <div class="form-group {{ $errors->has('frequency') ? 'has-error' : ''}}">
        {!! Form::label('frequency', trans('dash.alert_frequency'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-5 form-inline">
            {!! Form::select('frequency',trans('dash.alert_frequencies'),null,['class'=>'form-control','required'=>'required']) !!}
            {!!Helper::popover(trans('dash.frequency'),trans('dash.alert_freq_desc'))!!}
        </div>
    </div>
    <div class="form-group email {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email', trans('dash.alert_send_to'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            <?php $email = (isset($alert))?$alert->email:Auth::user()->email; ?>
            {!! Form::text('email', $email, ['class' => 'form-control','placeholder'=>trans('dash.email'), 'required'=>'required']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('dash.create_deal_alert'), ['class' => 'btn btn-lg btn-primary submit_form']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('products.cancel')}}</a>
    </div>
</div>

@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
@include('dashboard.layouts.popup');
<script type="text/javascript">
    $('#deals_form').validate({
        rules: {
            "type[]": "required",
            "email":'email'
        },
        messages: {
            'service_product_name':'من فضلك ادخل اسم المنتج / الخدمة',
            "type[]": "من فضلك اختر نوع التنبيه",
        }
    });
</script>
@endsection