@extends('dashboard.layouts.master_reg')

@section('content')
    <legend>{{trans('dash.create_deal_alert')}}</legend>

    <div class="help-block">{{trans('dash.alert_desc')}}</div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!!Form::open(['url'=>'dashboard/alerts','class'=>'form-horizontal','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
        @include ('dashboard.deals.alerts.form')
    {!! Form::close() !!}

@endsection