<table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>{{trans('dash.private_id')}}</th>
                            <th>{{trans('dash.id')}}</th>
                            <th>{{trans('dash.name')}}</th>
                            <th>{{trans('dash.subject')}}</th>
                            <th>Quantity</th>
                            <th>{{trans('dash.view')}}</th>
                            <th>{{trans('dash.last_update')}}</th>
                            <th>{{trans('dash.date')}}</th>
                            <th>{{trans('products.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($quotations as $index=>$item)
                        @if(!$item->request) <?php continue; ?> @endif
                        <tr id="tr_{{$item->id}}">
                            <td>{{ $index+1 }}</td>
                            <td>{{ $item->id }}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->subject}}</td>
                            <td>{{$item->request->order_quantity}} {{$item->request->unit}}</td>
                            <td><a href="{{url('dashboard/quotations/show/'.$item->id)}}" class="btn btn-info btn-sm popup">{{trans('dash.view')}}</a></td>
                            <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
                            <td>{{$item->updated_at}}</td>
                            <td>
                                {!! Form::open([
                                    'method'=>'DELETE','url'=>['dashboard/deals',$item->id],'style'=>'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash" title="'.trans("products.del_prod").'"></i>',array(
                                    'type' => 'submit','class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Deal Permenantly',
                                    'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>