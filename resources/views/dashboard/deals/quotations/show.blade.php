<?php
	use App\Helper;
	use App\User;
?>
@extends('dashboard.layouts.master_reg')

@section('content')
<div class="white-popup-block" style="max-width: 850px;">
	<div class="">
		<legend>{{trans('front.quote_now')}}</legend>
		{!!Form::model($quotation, ['url'=>'#','method'=>'POST','class'=>'form-horizontal','files'=>true])!!}			
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				{!!Form::label('name', trans('front.name'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_name')])!!}
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('email')?'has-error':''}}">
				{!!Form::label('email', trans('front.email'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_email')])!!}
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('subject')?'has-error':''}}">
				{!!Form::label('subject', trans('dash.subject'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('subject', trans('front.plz_provide_quotes'),['class'=>'form-control','placeholder'=>'Your Subject Here'])!!}
					{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		    <div class="form-group {{ $errors->has('production_capacity') ? 'has-error' : ''}}">
		        {!! Form::label('production_capacity', trans('products.capacity'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            <div class="row">
		                <div class="col-md-3">
		                    {!! Form::text('production_capacity', null, ['class' => 'form-control','required'=>'required']) !!}
		                </div>
		                <div class="col-md-3">                    
		                    {!!Form::select('capacity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required'])!!}
		                </div>
		                <div class="col-md-4 form-inline">
		                    <label>{{trans('products.per')}} {!! Form::select('capacity_unit_per',trans('dash.capacity_durations'),null,['class'=>'form-control','required'=>'required'])!!}
		                    </label>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="form-group {{ $errors->has('min_quantity') ? 'has-error' : ''}}">
		        {!! Form::label('min_quantity', trans('front.quantity_available'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            <div class="row">
		                <div class="col-md-3">
		                    {!! Form::text('min_quantity', null, ['class' => 'form-control','required'=>'required']) !!}
		                </div>
		                <div class="col-md-3">                    
		                    {!! Form::select('min_quantity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required']) !!}
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="form-group {{ $errors->has('fob_price') ? 'has-error' : ''}}">
		        {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            <div class="row">
		                <div class="col-md-3">
		                    {!! Form::select('fob_price_type', trans('dash.fob_price_list'), null,['class'=>'form-control','required'=>'required']) !!}
		                </div>
		                <div class="col-md-3">                    
		                    {!! Form::text('fob_price',null,['class'=>'form-control','placeholder'=>trans('dash.max'),'required'=>'required']) !!}
		                </div>
		                <div class="col-md-4 form-inline">
		                    <label>{{trans('products.per')}} {!!Form::select('fob_price_unit',trans('dash.units'), null,['class' => 'form-control'])!!}
		                    </label>
		                </div>
		            </div>
		            <div class="help-block">{{trans('products.type_help')}}</div>
		        </div>
		    </div>
		    <div class="form-group {{ $errors->has('fob_price') ? 'has-error' : ''}}">
		        {!! Form::label('fob_price', trans('products.payment_type'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            <div class="row">
		                @foreach(App\ProductDetails::$payment_methods as $item=>$label)
		                <div class="col-md-3">
		                    <label title="{{$item}}">{!!Form::checkbox('payment_type[]', $item, null, ['class'=>'pay_types'])!!} {{trans('dash.'.$label)}}</label>
		                    @if($label=='Other')
		                        <span id="other_payment_type">
		                            {!!Form::label('other_payment',trans('dash.enter_your_payment_type'),['class'=>'control-label'])!!}
		                            {!!Form::text('other_payment_type',null,['class'=>'form-control other_paytype'])!!}
		                        </span>
		                    @endif
		                </div>
		                @endforeach
		            </div>
		        </div>
		    </div>
		    <div class="form-group {{ $errors->has('port') ? 'has-error' : ''}}">
		        {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-8">
		            {!! Form::text('port', null, ['class' => 'form-control','required'=>'required']) !!}
		            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
		        </div>
		    </div>
		    <div class="form-group {{ $errors->has('delivery_time') ? 'has-error' : ''}}">
		        {!! Form::label('delivery_time', trans('products.delivery_time'), ['class' => 'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            <div class="row">
		                <div class="col-md-3">{!! Form::text('delivery_time', null, ['class' => 'form-control']) !!}</div>
		                <div class="col-md-3">                    
		                    {!!Form::select('delivery_duration',trans('dash.delivery_duration'),null,['class'=>'form-control']) !!}
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="form-group brands {{$errors->has('packaging')?'has-error':''}}">
		        {!!Form::label('packaging',trans('products.pack_details'), ['class'=>'col-md-3 control-label']) !!}
		        <div class="col-md-9">
		            {!!Form::textarea('packaging',null,['class'=>'form-control','rows'=>4,'placeholder'=>trans('products.pack_help')])!!}
		            {!! $errors->first('packaging', '<p class="help-block">:message</p>') !!}
		        </div>
		    </div>
			<div class="form-group {{$errors->has('body')?'has-error':''}}">
				{!!Form::label('body', trans('front.message'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::textarea('body', null,['class'=>'form-control','placeholder'=>trans('front.enter_quote_message')])!!}
					{!! $errors->first('body', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('attachment')?'has-error':''}}">
				{!!Form::label('attachment', trans('front.attachment'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
				</div>
			</div>
		{!!Form::close()!!}
	</div>
</div>
@endsection