<?php
    use App\Deals\Deal;
?>
@extends('dashboard.layouts.master_reg')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-4">عروض الأسعار المقدمة</div>
                <div class="col-md-8">
                </div>
            </div>
        </div>
        <div class="panel-body">
                <div role="tabpanel">
                    <ul class="nav nav-tabs product_status" role="tablist">
                        @foreach(App\Deals\Quote::$all_status as $state_id=>$status)
                            <?php $counter = $quotations->filter(function($row) use($state_id){
                                    return $row->status==$state_id;
                                })->count(); ?>
                            @if($status=='All')
                                <?php $counter = $quotations->count(); ?>
                            <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('products.offer_statuses')[$state_id]}} ({{$counter}})</a></li>
                            @endif
                        @endforeach
                        @foreach(App\Deals\Quote::$all_status as $state_id=>$status)
                            @if($state_id!=3)
                            <?php $counter = $quotations->filter(function($row) use($state_id){
                                    return $row->status==$state_id;
                                })->count(); ?>
                            @if($status=='All')
                                <?php $counter = $quotations->count(); ?>
                            @endif
                            <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('products.offer_statuses')[$state_id]}} ({{$counter}})</a></li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
                </div>
            </div>            
        </div>
    </div>
@endsection

@section('script')
    {!!HTML::script('frontend/js/app/products.js')!!}
    <script type="text/javascript">
        $('body').on('click', '.product_status a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var status = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/quotess/grid')}}?status='+status,
                type: 'GET',
                beforeSend: function() {
                   
                }
            }).done(function(response) {
                 $('.data_grid').html(response);
            }).fail(function(error) {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying products grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
        $(document).ready(function() {
            $('.product_status a[data-toggle="tab"]:first').trigger('click');
        });
    </script>
@endsection