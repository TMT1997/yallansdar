<?php use App\Deals\Deal; 
use App\Deals\SellOffer;
use App\Helper;
?>
<div class="label label-primary">{{trans('dash.no_of_deals')}} {{$deals->count()}}</div>
<table class="table table-bordered table-striped table-hover dataTable">
    <thead>
        <tr>
            <th>{{trans('dash.id')}}</th>
            <th>{{trans('dash.name')}}</th>
            <th>{{trans('dash.section')}}</th>
            <th>{{trans('dash.type')}}</th>
            <th>{{trans('dash.status')}}</th>
            <th>{{trans('dash.valid_till')}}</th>
            <th>{{trans('dash.last_update')}}</th>
            <th>{{trans('products.actions')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($deals as $item)
        <tr id="tr_{{$item->id}}">
            <td>{{ $item->id }}</td>
            <td>
                <strong class="text-success">
                @if($item->section=='sell_offer')
                    @if($item->offer)
                        {{Helper::image_single($item->offer->images)}}
                        {{SellOffer::prod_name($item->offer)}}
                    @endif
                @elseif($item->section=='quote_request')
                    @if($item->request)
                        {{Helper::image_single($item->request->images)}}
                        {{Helper::field($item->request,'name')}}
                    @endif
                @elseif($item->section=='partnership')
                    @if($item->partnership)
                        {{Helper::image_single($item->partnership->images)}}
                        {{$item->partnership->service_product_name}}
                    @endif
                @endif
                </strong>
            </td>
            <td>{{ Deal::$sections[$item->section]}}</td>
            <td>{{ $item->type }}</td>
            <td>{{ Deal::$statues[$item->status] }} </td>
            <td>{{ Carbon\Carbon::parse($item->expired_at)->format('M. d, Y') }}</td>
            <td>{{ Carbon\Carbon::parse($item->updated_at)->diffForHumans() }}</td>
            <td>
                <a href="{{ url('dashboard/deals/'.$item->id) }}" class="btn btn-success btn-sm" title="{{trans('dash.view')}}"><span class="fa fa-eye"/></a>
                <?php $dis = ($item->status!=2)?'disabled="disabled"':''; ?>
                <a href="{{route('dashboard.deals.edit',$item->id)}}" class="btn btn-primary btn-sm" {{$dis}} title="{{trans('dash.edit')}}"><span class="fa fa-pencil"/></a>

                @if($item->status!=5)
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/deals/trash')}}' class="btn btn-danger btn-sm btn_del" title="{{trans('dash.trash_item')}}"><i class="fa fa-trash"></i></a>
                @else
                    <a href="javascript:void(0)" data-id='{{$item->id}}' data-url='{{url('dashboard/deals/restore')}}' class="btn btn-warning btn-sm btn_restore" title="{{trans('dash.restore_item')}}"><i class="fa fa-undo"></i></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['dashboard/deals', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::button('<i class="fa fa-trash" title="'.trans("dash.perma_delete").'"></i>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete Deal Permenantly',
                            'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
                    )) !!}
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@if($deals->count()==0)
    <p class="text-center">{{trans('dash.no_results_found')}}</p>
@endif