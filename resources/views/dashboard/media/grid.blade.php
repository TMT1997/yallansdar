{!!Form::hidden('type', $type, ['id'=>'type_hd'])!!}
<div class="well well-sm">
    {!! Form::open(['method'=>'get','class'=>'form-inline filter_ajax']) !!}
        {!!App\Helper::select('album_id',$albums->lists('name','id'),[''=>trans('dash.filter_by_album')],Request::get('album_id'), ['id'=>'album_filter'])!!}
        <a href="{{ url('dashboard/media/create?type=images') }}" class="btn btn-success {{($type=='documents')?'hidden':''}}">{{trans('dash.add_image')}}</a>
        <a href="{{ url('dashboard/media/create?type=documents') }}" class="btn btn-warning {{($type=='images')?'hidden':''}}">{{trans('dash.add_document')}}</a>
    {!! Form::close() !!}
</div>
<div class="row">
    @if(isset($media) && $media->count()>0)
        @foreach($media as $item)
            <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="thumbnail">
                    {!!App\Helper::image($item->file,['style'=>'max-height:100px'])!!}
                    <div class="caption">
                        <p>{{$item->name}}</p>

                        <a href="{{route('dashboard.media.edit',$item->id)}}" class="btn btn-primary btn-sm" title="Edit File"><span class="fa fa-pencil"/></a>
                        {!!Form::open(['method'=>'DELETE','url'=>['dashboard/media',$item->id],'style'=>'display:inline'])!!} {!! Form::button('<span class="fa fa-trash-o" title="Delete" />', array('type'=>'submit','class'=>'btn btn-danger btn-sm','title'=>'Delete File','onclick'=>'return confirm("Confirm delete?")')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p class="text-center">
            {{trans('dash.no_results_found')}}
        </p>
    @endif
</div>