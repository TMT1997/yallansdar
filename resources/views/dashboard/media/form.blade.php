{!!Form::hidden('type', Request::get('type'))!!}
<div class="form-group">
    {!!Form::label('album_id', trans('dash.album'), ['class'=>'col-md-3 control-label'])!!}
    <div class="col-md-9 form-inline">
        {!!Form::select('media_album_id', $albums->lists('name','id'), null,['class'=>'form-control','id'=>'media_album_id'])!!}
        <a href="{{url('dashboard/quickadd/album')}}" class="btn btn-success popup">{{trans('dash.add_album')}}</a>
    </div>
</div>
<div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">
    {!! Form::label('file', trans('dash.select_file'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath = (isset($media))? 'uploads/'.$media->file:'frontend/images/no_thumb.jpg';?>
            @if(Request::get('type')=='images')
                <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>
            @endif
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('file',['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
                <div class="help-block">
                    @if(Request::get('type')=='images')
                        Preffered Image Size: Less than 3MB, <br>Image Format: Jpeg, Jpg, Gif, Png
                    @else
                        Preffered Document Size: Less than 3MB, <br>Document Format: xls, xlsx, ppt, pptx, txt, pdf, doc, docx
                    @endif
                </div>
            </div>
        </div>
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-4">
        {!!Form::submit(isset($submitButtonText)?$submitButtonText:trans('dash.upload_file'),['class'=>'btn btn-primary btn-loading'])!!}
        <a href="javascript:history.back(1);" class="btn btn-default">{{trans('dash.cancel')}}</a>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        $('body').on('click','.btn_quickAddAlbum', function(event) {            
            event.preventDefault();
            $.ajax({
                url: '{{route('dashboard.albums.store') }}',
                data: $("#popup_form").serializeArray(),
                type:'POST',
                dataType:'json',
                beforeSend:function(){
                }
            }).done(function(output) {
                if (output.status == 'success') {
                    var row = output.data;
                    $("#media_album_id").append('<option value="'+row.id+'" selected="selected">'+row.name+'</option>');
                    $.magnificPopup.close();
                } else {
                    alert('Some errors happened');
                }
            }).fail(function() {
                alert('error');
            }).always(function() {
            });
        });
    </script>
    @include('dashboard.layouts.popup');
@endsection