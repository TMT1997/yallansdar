@extends('dashboard.layouts.master_reg')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{trans('dash.media')}}</div>
    <div class="panel-body">
        <div role="tabpanel">
            <ul class="nav nav-tabs tab_filter" role="tablist">
                @foreach(App\Media::$types as $state_id=>$status)
                    <?php $count=$media->filter(function($row) use($state_id){return $row->type==$state_id;})->count(); ?>

                    @if($state_id=='')
                        <?php $count = $media->count(); ?>
                    @endif
                    <li role="presentation"><a href="#f{{$state_id}}" data-sid="{{$state_id}}" aria-controls="{{$state_id}}" role="tab" data-toggle="tab">{{trans('dash.'.$status)}} ({{$count}})</a></li>
                @endforeach
            </ul>
        
            <div class="tab-content data_grid" style="margin-top:5px; padding:5px;"></div>
        </div>        
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('body').on('click', '.tab_filter a[data-toggle="tab"]', function(event) {
            event.preventDefault();
            var type = $(this).data('sid');
            $.ajax({
                url: '{{url('dashboard/media/grid')}}?type='+type,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('{{trans('dash.loading_media')}}');    
                }
            }).done(function(response) {
                $('.data_grid').html(response);
            }).fail(function() {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying data grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
        $(document).ready(function() {
            $('.tab_filter a[data-toggle="tab"]:first').trigger('click');
        });
        $('body').on('change', '.filter_ajax select', function(event) {
            event.preventDefault();
            var album_id = $(this).val();
            var type = $('#type_hd').val();
            $.ajax({
                url: '{{url('dashboard/media/grid')}}?type='+type+'&album_id='+album_id,
                type: 'GET',
                beforeSend: function() {
                    $('.data_grid').html('Loading Media...');    
                }
            }).done(function(response) {
                $('.data_grid').html(response);
            }).fail(function() {
                $('.data_grid').html('<div class="alert alert-danger">Error displaying data grid</div>');
                notify('Error occured','error');
            }).always(function() {
            });
        });
    </script>
@endsection