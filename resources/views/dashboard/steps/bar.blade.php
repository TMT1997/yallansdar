<div class="quick_checklist">
    <div class="row">
        <div class="col-md-1">
            <div class="ice_title">
            @if(isset($type) && $type=='website')
                @lang('dash.website_checklist')
            @else
                @lang('dash.dashboard_checklist')
            @endif
            </div>
        </div>
        <div class="col-md-11"> 
            @foreach(App\User::$checklist as $key=>$item)
                @if($key=='email_verified') <?php continue; ?> @endif
                <div class="col-md-3">
                    <a href="{{url(@$item[0])}}" class="list-group-item">
                        <i class="fa fa-{{(@$steps->$key)?'check-square-o text-success':'square-o text-danger'}}"></i> 
                        <span class="ice text-{{(@$steps->$key)?'success':'danger'}}">
                            {{@trans('dash.checklist')[$key]}}
                        </span> 
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
<style type="text/css">
    .quick_checklist {
        width: 100%;
        padding:7px 10px 4px 10px;
        border:1px solid #c0e9ff;
        margin-top: -10px;
        margin-bottom: 10px;
        border-radius: 6px;
        background:#dfe3ee;
    }
    .quick_checklist .ice_title {margin-bottom: 5px; color: #008bff; font-size: 12px;}
    .quick_checklist .ice {font-size: 12px;}
    .quick_checklist .list-group-item {padding:5px;}
</style>