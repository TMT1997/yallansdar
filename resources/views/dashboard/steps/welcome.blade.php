@extends('dashboard.layouts.master_reg')
@section('content')
<legend>مرحبا بك في يلانصدر</legend>
<div class="help-block infographic-box">
	انت على بعد 4 خطوات فقط من إعداد متجرك الإلكتروني!
	<br>
    <div class="well">
        <div class="media">
        	<a class="pull-right" href="#">
        		<i class="fa fa-gears yellow-bg"></i>
        	</a>
        	<div class="media-body">
        		<h4 class="media-heading">أكمل بياناتك الشخصية</h4>
        		<p>قم بإكمال بياناتك حتى يتمكن زوار الموقع من التواصل معك بسهولة</p>
        		<a href="{{url('dashboard/profile')}}" class="btn btn-primary">تعديل بياناتي</a>
        	</div>
        </div>
    </div>
    <div class="well">
        <div class="media">
        	<a class="pull-right" href="#">
                <i class="fa fa-magic green-bg"></i>
        	</a>
        	<div class="media-body">
        		<h4 class="media-heading">أكمل بياناتك شركتك</h4>
        		<p>اكمل بياناتك نشاطك أو شركتك لإعطاء الثقة في منتجاتك</p>
        		<a href="{{url('dashboard/profile')}}" class="btn btn-primary">تعديل بيانات نشاطي</a>
        	</div>
        </div>
    </div>
    <div class="well">
        <div class="media">
        	<a class="pull-right" href="#">
        		<i class="fa fa-tags purple-bg"></i>
        	</a>
        	<div class="media-body">
        		<h4 class="media-heading">إضافة المنتجات</h4>
        		<p>إبداء بإضافة بعض المنتجات للحصول على مشترين جدد.</p>
        		<a href="{{url('dashboard/products/create')}}" class="btn btn-primary">اضافة منتجات</a>
        	</div>
        </div>
    </div>
    <div class="well">
        <div class="media">
        	<a class="pull-right" href="#">
                <i class="fa fa-dollar emerald-bg"></i>
        	</a>
        	<div class="media-body">
        		<h4 class="media-heading">أنشىء متجرك الإلكتروني</h4>
        		<p>احصل على متجر متكامل لعرض منتجاتك ومعلومات شركتك للحصول على فرص تصدير أكبر</p>
        		<a href="{{url('dashboard/website')}}" class="btn btn-primary">إنشاء المتجر</a>
        	</div>
        </div>
    </div>
    <div class="row text-center">
    	تخطى الخطوات الأن <a href="{{url('dashboard')}}">واذهب إلى لوحة التحكم</a>
    </div>
</div>
<style>
.infographic-box i {font-size: 2.4em; display: block; float: left; margin-left: 15px; width: 60px; height: 60px; line-height: 60px; text-align: center; border-radius: 50%; background-clip: padding-box; color: #fff; }
.emerald-bg {background-color: #03a9f4 !important; }
.red-bg { background-color: #e84e40 !important; }
.yellow-bg { background-color: #ffc107 !important; }
.green-bg { background-color: #8bc34a !important; }
.purple-bg { background-color: #9c27b0 !important; }
.gray-bg { background-color: #90a4ae !important; }
</style>
@endsection