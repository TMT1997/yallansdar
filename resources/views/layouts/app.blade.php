<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') YallaNsadar</title>
    {{HTML::style("frontend/css/bootstrap.min.css")}}
    {{HTML::style('frontend/css/font-awesome.min.css')}}
    @if(App::isLocale('ar'))
        {{HTML::style('backend/dist/css/bootstrap-rtl.min.css')}}
        {{HTML::style('frontend/css/rtl.css')}}
    @endif
    <style>
    html { height: 100%;}
    body{height:100%; margin: 0;
    background-repeat: no-repeat;
    background-attachment: fixed;}
        .fa-btn {margin-right: 6px;}
        body {background: rgba(190,202,178,1);
background: -moz-linear-gradient(top, rgba(190,202,178,1) 0%, rgba(92,167,162,1) 50%, rgba(56,135,149,1) 78%, rgba(28,109,138,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(190,202,178,1)), color-stop(50%, rgba(92,167,162,1)), color-stop(78%, rgba(56,135,149,1)), color-stop(100%, rgba(28,109,138,1)));
background: -webkit-linear-gradient(top, rgba(190,202,178,1) 0%, rgba(92,167,162,1) 50%, rgba(56,135,149,1) 78%, rgba(28,109,138,1) 100%);
background: -o-linear-gradient(top, rgba(190,202,178,1) 0%, rgba(92,167,162,1) 50%, rgba(56,135,149,1) 78%, rgba(28,109,138,1) 100%);
background: -ms-linear-gradient(top, rgba(190,202,178,1) 0%, rgba(92,167,162,1) 50%, rgba(56,135,149,1) 78%, rgba(28,109,138,1) 100%);
background: linear-gradient(to bottom, rgba(190,202,178,1) 0%, rgba(92,167,162,1) 50%, rgba(56,135,149,1) 78%, rgba(28,109,138,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#becab2', endColorstr='#1c6d8a', GradientType=0 );}
        .section_wrap {background: #E6EEEA; margin-top: 50px; }
        .text-white {color: #a4ced5;}
        .login_block, .registration_block {width:500px; margin:0 auto;}
        @media (max-width: 580px) {
            .login_block, .registration_block {width:100%;}
        }
        .fb_login {margin:15px 0;}
        .btn {height: initial;}
        .btn-fb i { vertical-align: middle; }
    </style>
</head>
<body id="app-layout">

@yield('content')
<div class="text-center text-white">
    {{trans('front.copyrights')}}
</div>
{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
{!!HTML::script('frontend/js/bootstrap.min.js')!!}
<script type="text/javascript">
    $('.btn-loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
</script>
</body></html>
<script type="text/javascript">
     $( ".check_english" ).keyup(function() {
      var arabic = /[\u0600-\u06FF]/;
      var data = this.value;
    var check= arabic.test(data);
    if(check)
    {
      alert('@lang("dash.alert_english")');
        this.value='';
    }
  });
</script>