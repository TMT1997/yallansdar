{{--This Create a modal to display messages when pressed on namozg 3rd el s3r
--}}

<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModals" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close end" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('front.account_state')</h4>
            </div>
            <div class="modal-body">
            <ul class="list-group" id="deals_list" style="padding: 15px;">


            </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default end" data-dismiss="modal">Close</button>
                <a href="{{url('deals/enquiry/'.$offer->id.'/requests')}}" class="btn btn-primary conti">@lang('front.continue')</a>
            </div>
        </div>

    </div>
</div>
