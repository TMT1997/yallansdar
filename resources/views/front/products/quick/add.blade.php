@extends('front.layouts.master')
@section('title') اضف منتج | @endsection
@section('content')
	{!!Form::open(['url'=>'product/add', 'method'=>'POST', 'class'=>'form-horizontal'])!!}

	<div class="container">
		<div class="">
			<legend>اضف منتج</legend>

			<div class="well well-sm">بيانات المنتج</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">التصنيف</label>
					<div class="col-md-8">
						<a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
					    @include('dashboard.categories.select_category')
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">المنتج</label>
					<div class="col-md-8">
						<input type="text" name="prod_name" class="form-control" placeholder="اسم المنتج" required="required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">نبذة عن المنتج</label>
					<div class="col-md-8">
						<textarea name="description" class="form-control" placeholder="نبذة عن المنتج" required="required" rows="4"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">تفاصيل المنتج</label>
					<div class="col-md-8">
						<textarea name="details" class="form-control" placeholder="تفاصيل المنتج" required="required" rows="7"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">صورة للمنتج</label>
					<div class="col-md-8">
						<input type="file" name="image" class="form-control" />
					</div>
				</div>
			@if(Auth::guest())
				<div class="well well-sm">بياناتك الشخصية</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">الاسم الشخصي</label>
					<div class="col-md-8">
						<input type="text" name="name" class="form-control" placeholder="الاسم الشخصي" required="required" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label must">البريد الإلكتروني</label>
					<div class="col-md-8">
						<input type="email" name="email" class="form-control" placeholder="البريد الإلكتروني" required="required" />
					</div>
				</div>
			@endif

			<div class="form-group">
				<div class="col-md-offset-3 col-md-8">
					{!!Form::submit('اضافة', ['class'=>'btn btn-primary'])!!}
					@if(Auth::guest())
					<div class="help-block">سيتم فتح حساب خاص بك على يلانصدر لإضافة المزيد من المنتجات واضافة بياناتك وبيانات شركتك لتسهيل عملية التواصل</div>
					@endif
				</div>
			</div>

		</div>
	</div>

	{!!Form::close()!!}
@endsection