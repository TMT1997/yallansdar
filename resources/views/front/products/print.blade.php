<?php
	use App\User;
	use App\Category;
	use App\Product;
	use App\Company;
	use App\Helper;

	$parents = ($category_parents) ? array_reverse($category_parents): [];
	$product_lang = Helper::lang_data($product, $lng_id);
	$profile = $product->user->profile;
	$reviews = $product->reviews;
	$reviews_count = $reviews->count();

	if(isset($product->more_details)) {
		$more_details = unserialize($product->more_details);
	}

	if(!$product_lang) {
		return redirect()->back();
	}
?>
@extends('front.layouts.print')
@section('title') {{$product_lang->name}} | @endsection
@section('content')

<h1>{{$product_lang->name}}</h1>

	<ul class="breadcrumb">
	  	@foreach($parents as $cat)
			<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a>
		@endforeach
	</ul>

	<div class="row">
		<div class="col-md-4">
			{!!Helper::image2($product->image, ['class'=>'img-responsive'])!!}</li>
		</div>

		<div class="col-md-8">
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<td style="width: 140px;">{{trans('front.description')}}</td>
						<td>{!!$product->description!!}</td>
					</tr>
					<tr>
						<td>{{trans('front.product_manufactured_in')}}</td>
						<td>{{App\Country::cname($product->manufactured_in)}}</td>
					</tr>
				  	<tr>
						<td>{{trans('front.supply_ability')}}</td>
						<td>{{Product::capacity($product)}}</td>
					</tr>
					<tr>
						<td>{{trans('front.fob_price')}}</td>
						<td>{{Product::FOB_price($product)}}</td>
					</tr>
					<tr>
						<td>{{trans('front.min_ord_quantity')}}:</td>
						<td>{{$product->min_quantity}} {{$product->min_quantity_unit}}</td>
					</tr>
					@if(is_array($more_details['attr']))
				  		@foreach($more_details['attr'] as $index=>$attr)
				  		<tr>
					  		<td>{{$attr}}:</td>
					  		<td>{{$more_details['value'][$index]}}</td>
					  	</tr>
					  	@endforeach
					@endif
					<tr>
						<td>{{trans('front.payment_terms')}}:</td>
						<td>{{$product->payment_type}}</td>
					</tr>
					<tr>
						<td>{{trans('front.packaging')}}:</td>
						<td>{!!$product_lang->packaging!!}</td>
					</tr>
					<tr>
						<td>{{trans('front.product_details')}}:</td>
						<td>{{$product_lang->detailed_description}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row" style="margin-top: 30px;">
		<div class="col-md-3">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object" src="#" alt="Image">
				</a>
				<div class="media-body">
					<h4 class="media-heading">{{$profile->company_name}}</h4>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<td>{{trans('front.contact_person')}}</td>
						<td>{{$profile->first_name}} {{$profile->last_name}}</td>
					</tr>
					<tr>
						<td>{{trans('front.business_type')}}</td>
						<td>{{$profile->business_type}}</td>
					</tr>
					<tr>
						<td>{{trans('front.city')}}</td>
						<td>{{$profile->city}}</td>
					</tr>
					<tr>
						<td>{{trans('front.state')}}</td>
						<td>{{$profile->state}}</td>
					</tr>
					<tr>
						<td>{{trans('front.country')}}</td>
						<td>{!!User::flag($product->user,[],true)!!}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
@endsection