<div class="white-popup-block">
	<legend>{{trans('front.add_to_favorites')}}</legend>

	@if($status)
		<div class="alert alert-success">{{trans('front.product_added_to_fav')}}</div>
	@else
		@if(!$logged_in)
			<div class="alert alert-warning">Please <a href="{{url('login')}}">login</a> to be able to add to favorites</div>
		@endif

		@if($already_added)
			<div class="alert alert-warning">{{trans('front.already_added_to_fav')}}</div>
		@endif
	@endif

	<div class="text-center">
		<a href="{{url('dashboard/favorites')}}" target="_blank" class="btn btn-sm btn-warning">{{trans('front.check_ur_favorites')}}</a>
	</div>
</div>