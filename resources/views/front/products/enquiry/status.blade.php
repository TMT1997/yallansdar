@extends('front.layouts.contact')

@section('content')

	<a href="{{url('home')}}" class="btn btn-primary">{{trans('front.back_to_home')}}</a>
	<a href="{{url('products')}}" class="btn btn-primary">{{trans('front.back_to_products')}}</a>

@endsection