<?php
	use App\Helper;
	use App\Product;
	use App\User;
	use App\Service;
?>
@extends('front.layouts.master')
@section('content')

<div class="container" style="margin-bottom: 20px;">
	<div class="">
		<div class="col-md-12" style="padding:20px;">
			<span class="brand_title">@lang('front.contact_supplier')</span>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9">
		{!!Form::open(['class'=>'form-horizontal', 'files'=>true, 'url'=>'cart/enquiry'])!!}
			@if(Auth::check())
				<div class="form-group {{$errors->has('name')?'has-error':''}}">
					{!!Form::label('name', trans('front.from'), ['class'=>'col-md-2 control-label'])!!}
					<div class="col-md-3">
						<a href="{{User::url_contact(Auth::user())}}" class="popup">
							{{Auth::user()->name}}
							<br>{{Auth::user()->company_name}}
						</a>
					</div>
				</div>
			@else
				<div class="form-group {{$errors->has('name')?'has-error':''}}">
					{!!Form::label('name', trans('front.name'), ['class'=>'col-md-2 control-label'])!!}
					<div class="col-md-3">
						{!!Form::text('name', null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_name')])!!}
						{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
					</div>

					{!!Form::label('email', trans('front.email'), ['class'=>'col-md-2 control-label'])!!}
					<div class="col-md-3">
						{!!Form::text('email',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_email')])!!}
						{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
					</div>
				</div>
				<div class="form-group {{$errors->has('country')?'has-error':''}}">
					{!!Form::label('country', trans('front.country'), ['class'=>'col-md-2 control-label'])!!}
					<div class="col-md-3">
						{!!Form::select('country',$countries->lists('name','id'),$selected_country,['class'=>'form-control'])!!}
						{!! $errors->first('country', '<p class="help-block">:message</p>') !!}
					</div>

					{!!Form::label('mobile', trans('front.mobile'), ['class'=>'col-md-2 control-label'])!!}
					<div class="col-md-3">
						{!!Form::text('mobile', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_mobile')])!!}
						{!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<label>{!!Form::checkbox('createAccountOnPost',1,true)!!} @lang('front.create_account_also')</label>
					</div>
				</div>
			@endif
			<div class="form-group cart_products">
				{!!Form::label('to', trans('front.send_to'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-8">
				@if(count($cart) > 0)
					@foreach($cart as $user_id=>$products)
						{!!Form::hidden('user_ids[]', $user_id)!!}
						<?php $user = $products[0]->user; ?>
						<div class="pull-right interested">@lang('front.interested_in')</div>
						{{$user->name}}<br>
						<div class="help-block">
							{{isset($user->profile) ? $user->profile->company_name : $user->company_name}}
						</div>
						<div class="well well-sm">
							<div class="row cart_more_products">
							@foreach($products as $product)
								{!!Form::hidden('products_ids['.$user_id.'][]', $product->id)!!}
								<div class="col-md-4">
									<a href="{{Product::url($product)}}" target="_blank">
										<div class="media">
											<div class="pull-left prod_img">{!!Helper::image2($product->image,['class'=>'media-object img-responsive'])!!}</div>
											<div class="media-body"><h5 class="media-heading">{{Helper::field($product,'name')}}</h5></div>
										</div>
									</a>
									<a href="{{url('cart/remove/'.$product->id)}}" title="@lang('front.remove_product')">X</a>
								</div>
							@endforeach
							</div>
						</div>
					@endforeach
				@else
					<div class="alert alert-warning">@lang('front.cart_empty_note')</div>
				@endif
				</div>
			</div>
			<div class="form-group {{$errors->has('subject')?'has-error':''}}">
				{!!Form::label('subject', trans('front.subject'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-8">
					{!!Form::text('subject',null,['class'=>'form-control','placeholder'=>trans('front.plz_provide_quotes'),'required'=>true])!!}
					{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('body')?'has-error':''}}">
				{!!Form::label('body', trans('front.message'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-8">
					{!!Form::textarea('body', null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_message'),'rows'=>6,'required'=>true])!!}
					{!! $errors->first('body', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('attachment')?'has-error':''}}">
				{!!Form::label('attachment', trans('front.attachment'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-8">
					{!!Form::file('attachment', ['class'=>'form-control'])!!}
					{!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
					<div class="help-block">{{trans('front.cart_attach_rules')}}</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-2 col-md-9">
					@if(count($cart))
						{!!Form::submit(trans('front.send_enquiry'),['class'=>'btn btn-lg btn-success btn_loading'])!!}
					@else
						{!!Form::submit(trans('front.send_enquiry'),['class'=>'btn btn-lg btn-success btn_loading','disabled'=>'disabled'])!!}
					@endif
					<div class="help-block">{{trans('front.send_enq_recp')}}</div>
				</div>
			</div>
		{!!Form::close()!!}
		</div>

		<div class="col-md-3">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#moreprods" aria-controls="moreprods" role="tab" data-toggle="tab">@lang('front.more_products')</a></li>
					<li role="presentation"><a href="#related" aria-controls="tab" role="tab" data-toggle="tab">@lang('front.related_services')</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="moreprods">
						<div class="well cart_more_products">
						@foreach($more_products as $product)
							<div class="form-group">
								<div class="media">
									<div class="pull-left prod_img"><a href="{{Product::url($product)}}" target="_blank">{!!Helper::image2($product->image,['class'=>'media-object img-responsive'])!!}</a></div>
									<div class="media-body"><h5 class="media-heading">{{Helper::field($product, 'name')}}</h5>
										{!!Form::open(['url'=>'cart'])!!}
					                        <input type="hidden" name="product_id" value="{{$product->id}}">
					                        <button type="submit" class="btn btn-success btn-xs add-to-cart" title="@lang('front.add_to_enq_cart')"><i class="fa fa-plus"></i></button>
					                    {!!Form::close()!!}
									</div>
								</div>
							</div>
						@endforeach
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="related">
						<div class="well">
							<div class="row">
								@foreach($services as $service)
			                		<?php $service_lang = Helper::lang_data($service, $lng_id); ?>
			                		<div class="col-md-12">
										<div class="media">
				                    		<a class="pull-right" style="padding-right:0" href="{{Service::url($service)}}">
				                    			{{Service::Image($service,['class'=>'media-object', 'style'=>'max-width:100px;'])}}
				                    		</a>
				                    		<div class="media-body">
				                    			<a href="{{service::url($service)}}"><strong class="media-heading">{{$service_lang->name}}</strong></a>
				                    		</div>
				                    	</div>
				                    </div>
				                @endforeach
				            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12" style="background: #f0f0f0; margin-top: 30px; padding:30px;">
	{{trans('front.cont_supplier_footer')}}
</div>
@endsection

@section('script')
	@include('dashboard.layouts.popup')
@endsection