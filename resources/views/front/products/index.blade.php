<?php
	use App\Category;
	use App\Product;
    use App\Helper;
    use App\User;
if (isset($lang))
    App::setLocale($lang);
?>
@extends('front.layouts.master')
@section('title') @lang('front.products') | @endsection
@section('content')
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
            <div class="col-md-2" id="sections-aside">
                <nav class="sdMnu innr-sd-mnu" >
                    <header><h2>@lang('front.categories')</h2></header>
                    <ul class="sid-mnu subcats" id="">
                        @foreach($categories->take(24) as $cat)
                            <li class="lvl-one-mnu-itm">
                                <a href="{{Product::cat_url($cat)}}" class="">{{$cat->$c_name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
			<div class="col-md-8 col-xs-12" id="toggle-responsive">
				<header><h1 class="hdr-styl-001 inr-hdr-blk">
                    @if(isset($category))
                        {{Helper::field($category,'name')}}
                    @else
                        @lang('front.products')
                    @endif
                </h1></header>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
    					<div class="breadcumb-list brcrmb-inr">
    						<ul class="">
    						  	<li><a href="{{url('home')}}">@lang('front.home')</a></li>
                                <li><a href="{{url('products')}}">@lang('front.products')</a></li>
                                @if(isset($category))
                                    <li><a href="{{Product::cat_url($category)}}">{{Helper::field($category,'name')}}</a></li>
                                @endif
    							<li>{{$products->count()}} @lang('front.products')</li>
    						</ul>
    					</div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="dataTables_length">
                            <label>@lang('front.show')
                            <select name="prods_length" aria-controls="prods" class="form-control input-sm custom-show-slct">
                                <option value="10">10</option><option value="25">25</option>
                                <option value="50">50</option><option value="100">100</option>
                            </select> @lang('front.entries')</label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                    <div class="col-xs-12 col-md-7 fltrs">
                        @lang('front.sort_by'): <label><input type="checkbox" id="verified" data-page="1">@lang('front.verified')</label>
                    </div>
                    <div class="col-xs-12 col-md-5 rt-tp-optons">
                        <div id="next-bttn"></div>
                        <div id="prv-bttn"></div>
                        <div class="pag-stt-info"></div>
                        <div class="grd-srt-btn"></div>
                        <div class="rows-srt-btn actv-grid"></div>
                        @lang('front.view_by'):
                    </div>
                </div>
                <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                    <thead><tr><th></th></tr></thead>
                    <tfoot><tr><th></th></tr></tfoot>
                    <tbody>
                    	@foreach($products as $product)
                        <?php
                        	$profile = ($product->user) ? $product->user->profile: null;
                        	if(!$profile) {
                        		continue;
                        	}
                        ?>
                        <tr>
                            <td>
                                <div class="row pro-thumb-row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                        <a class="img-hldr-product" href="{{Product::url($product)}}?lang={{$lang}}">{{Product::image($product)}}</a>
                                        <a href="{{Product::url($product)}}" class="prduct-thumb-ttl">{{Helper::field($product, 'name')}}</a>
                                        <p class="pro-thumb-sumry">{{str_limit(Helper::field($product,'description'),160)}}</p>
                                        <div class="rqst-qution-hldr">
                                            <a href="{{Product::url($product)}}" class="grn-btn">@lang('front.request_for_quote')</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  options-col">
                                        <span class="produc-by">{!!User::isVerified($product->user, false)!!}
                                        <a href="{{User::contact_info($product->user)}}">{{$profile->company_name}}</a></span>
                                        <span>{!!User::flag($product->user,[],true)!!}</span>
                                        <span class="b-frst-t-rvu"><i class="fa fa-star-o"></i> <a href="{{url('reviews/add/'.$product->id.'/product')}}" class="popup">@lang('front.befirst_to_review')</a></span>
                                        <span class="cntct-dtlls"><a href="{{User::contact_info($product->user)}}"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a></span>
                                        <div>
                                            <a ><i class="fa fa-camera"></i></a>
                                            <a href="#"><i class="fa fa-film"></i></a>
                                            <a ><i class="fa fa-info"></i></a>
                                            <a href="#"><i class="fa fa-tag"></i></a>
                                        </div>
                                    </div>
                                    <div class="rqst-qution-hldr scnd-btn">
                                        {!!Form::open(['url'=>'cart'])!!}
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <button type="submit" class="grn-btn add-to-cart"><i class="fa fa-shopping-cart"></i> @lang('front.request_for_quote')</button>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
			</div>
            <aside class="col-xs-12 col-md-2 sponsored_products">
                <header><div class="hdr-styl-asid">@lang('front.sponsored_products')</div></header>
                @foreach($advs as $adv)
                    <div class="prod_item" style="width: 100px">
                        <a href="{{$adv->link}}" target="_blank">
                            <img src="{{asset('uploads/'.$adv->image)}}" style="width: 168px">
                        </a>
                    </div>
                @endforeach
                @foreach($products_sponsored as $product)
                    <div class="prod_item">
                        <a href="{{Product::url($product)}}" class="thumbnail">
                            {{Product::image($product)}}
                            <div class="caption">{{Helper::field($product,'name')}}</div>
                        </a>
                    </div>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection

@section('script')
    @include('dashboard.layouts.popup')
    
    <script>
        $(document).ready(function(){
            $(document).on('click',"#toggle-aside",function(){
                $("#sections-aside").toggle(200);
                $("#toggle-responsive").toggleClass('col-md-10 col-md-8');
            });
        });
    </script>
@endsection