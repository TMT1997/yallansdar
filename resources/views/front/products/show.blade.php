<?php
	use App\User;
	use App\Category;
	use App\Product;
	use App\Company;
	use App\Helper;
	use App\PackageUsage;
	if (isset($lang))
    	App::setLocale($lang);
	$parents = array_reverse($category_parents);
	//$product_lang = Helper::lang_data($product, $lng_id);
	$profile = (isset($product->user->profile)) ? $product->user->profile : null;
	$reviews = $product->reviews;
	$reviews_count = $reviews->count();

	if(isset($product->more_details)) {
		$more_details = unserialize($product->more_details);
	}
	$business_types = (isset($profile) && $profile->business_type) ? explode(',', $profile->business_type):'';
	$payment_type = ($product->payment_type) ? explode(',', $product->payment_type):'';
?>
@extends('front.layouts.master')
@section('title') {{Helper::field($product,'name')}} | @endsection
@section('content')
<div class="">
	<ul class="container breadcrumb">
	  	<li><a href="{{url('home')}}">@lang('front.home')</a></li>
	  	@if(!empty($parents))
		  	@foreach($parents as $cat)
				<li><a href="{{Category::url($cat)}}">{{@$cat->$c_name}}</a></li>
			@endforeach
		@endif
		<li class="active">{{Helper::field($product,'name')}}</li>
	</ul>
</div>
<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>@lang('front.loading_images')</div>
				<div class="sp-wrap">
					@if($product->image)
                		<a  href="{{url('uploads/'.$product->image)}}">{!!Helper::image2($product->image,[])!!}</a>
                	@endif
                	@foreach($product->images as $img)
                    	<a href="{{url('uploads/'.$img->image)}}">{!!Helper::image2($img->image)!!}</a>
                    @endforeach
				</div>
			</div>

			<div class="col-xs-12 col-md-7">
				<header><h1 class="hdr-styl-001">{{Helper::field($product,'name')}}</h1></header>
				@if($reviews_count>0)
					@lang('front.reviews') ({{$reviews_count}}): ({{App\Review::rate_stars($reviews)}})
					<a href="#{{url('reviews/add/'.$product->id.'/product')}}" class="fv-link popup"><span class="fv-ico"></span> @lang('front.addreview')</a>

				@else
					<a href="{{url('reviews/add/'.$product->id.'/product')}}" class="fv-link popup"><span class="fv-ico"></span> @lang('front.befirst_to_review')</a>

				@endif

				<p class="nrml-p" style="margin-top: 10px;">
					{!!str_limit(Helper::field($product,'description'), 350)!!}
				</p>
				<div class="table-responsive">
					<table class="table table-condensed inf-tbl">
						<tbody>
							<tr>
								<td style="width:140px">@lang('front.fob_price'):</td>
								<td><a id="emailSupplink" href="#contact_supplier">@lang('front.get_latest_price')</a></td>
							</tr>
							<tr>
								<td>@lang('front.min_ord_quantity'):</td>
								<td>@if($product->min_quantity)
										{{$product->min_quantity}} {{trans('dash.units')[$product->min_quantity_unit]}}
									@endif
								</td>
							</tr>
							<tr>
								<td>@lang('front.payment_terms'):</td>
								<td>
									@if($payment_type && is_array($payment_type))
										@foreach($payment_type as $type)
											{{trans('dash.payment_methods_list')[$type]}},
										@endforeach
									@endif
								</td>
							</tr>
						</tbody>
					</table>
					<p style="text-align: center;"><a href="#find" id="get_find">@lang('front.view_more')</a></p>
				</div>

				<div class="cnct-inf-sid ad-pd">
					<h3 class="hdr-styl-004">
						@if(!isset($user_website->domain))
                            <?php $url ="#"  ?>
						@else
							<?php $url = url("store/".$user_website->domain); ?>
						@endif
						<a href="{{$url}}">{{@$profile->company_name}}</a>
						<span class="flg-ico">{!!User::flag($product->user,[],true)!!}</span>
					</h3>
					<p class="sml-p-wth-spn">
						<span>@lang('front.business_type'):</span>
						@if($business_types && is_array($business_types))
							@foreach($business_types as $type)
								{{trans('dash.business_types')[$type]}},
							@endforeach
						@endif
					</p>
					{{--<p class="sml-p-wth-spn">--}}
						{{--<span>@lang('front.mobile'):</span>--}}
						{{--@if($profile->mobile)--}}
						{{--{{$profile->mobile}}--}}
						{{--@endif--}}
					{{--</p>--}}
					{{--<p class="sml-p-wth-spn">--}}
						{{--@if($profile->primary_email)--}}
						{{--<span>@lang('front.email'):</span>--}}
						{{--{{$profile->primary_email}}--}}
						{{--@endif--}}
					{{--</p>--}}
					<p class="sml-p-wth-spn">
						@if($profile->destination)
						<span>@lang('dash.destination')</span>
						{{$profile->destination}}
						@endif
					</p>
					<div class="row">
						<div class="col-md-10 clnt-stt">
							<div>{!!User::isVerified($product->user)!!}</div>
							<div><i class="fa fa-user"></i>
								@if($profile) {{$profile->first_name}} {{$profile->last_name}} @endif
							</div>

							<div style="text-align: center">
								{{--Disable the modal which display the Company details--}}
								{!!Form::open(['url'=>'cart'])!!}
								<input type="hidden" name="product_id" value="{{$product->id}}">

								<button type="submit" class="btn bg-white"  style="background: white; font-size: 12px; padding: 3px;"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</button>
								{!!Form::close()!!}
							</div>

							@if($user_package)

							<div><i class="fa ">
									<a title="{{(($user_package->package->name_en == 'Yalla VIP')? $user_package->package->name_en :$user_package->package->name_en.trans('front.upgrade_package'))}}" href="{{url('/upgrade')}}">
										<img  src="{{url('/uploads/icons/'.$user_package->package->name_en.'.png')}}">
									</a>
								</i>

									{{$user_package->package->name_en}}

							</div>
							@endif
							{{--<a href="{{User::url_contact($product->user)}}" class="popup"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a>--}}
							{{--// make this url go to send message to supllier--}}
							{{--<a href="{{url('/cart')}}" ><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a>--}}
							@if($product->user->store)
								<?php $store = $product->user->store; ?>

								@if($store->status==2 && $store->active==1)
									<a href="{{App\UserWebsite::website_url($store)}}" class="" target="_blank"><i class="fa fa-globe"></i> @lang('front.store_website')</a>
								@endif
							@endif
						</div>
	                	<div class="col-md-2 options-col">
	                    	<div>

								<a  @if(!empty($product->image))
									href="{{url('uploads/'.$product->image)}}"
									@endif
									target= "{{(!empty($product->image) ? '_blank' : '')}}" ><i class="fa fa-camera"></i></a>

	                            <a 		@if(!empty($product->video_url))
										href="{{$product->video_url}}"
										@endif
										  target="{{(!empty($product->video_url) ?'_blank' : '')}}"><i class="fa fa-film"></i></a>
	                            {{--<a><i class="fa fa-info"></i></a>--}}
	                            {{--<a href="#"><i class="fa fa-tag"></i></a>--}}
	                        </div>
	                    </div>

	                </div>

				</div>

				<div class="shr-prnt-br02 row" id="find">
					<div class="col-xs-12 col-md-6 folow-us">
						<span>{{trans('front.share_us_on')}}:</span>
						<div style="display: inline-block;" class="addthis_inline_share_toolbox"></div>
					</div>
					<div class="col-xs-12 col-md-6 prnt-br text-right">
						<a href="{{Product::print_url($product)}}" target="_blank"><i class="fa fa-print"></i> @lang('front.print')</a>
						<a href="#"><i class="fa fa-envelope"></i> @lang('front.product_add_email')</a>
						<a href="{{url('product/addtofavorites/'.$product->id)}}" class="popup"><i class="fa fa-heart"></i> @lang('front.add_to_favorites')</a>
						{{--<a href="javascript:addToCart({{$product->id}})" data-id="{{$product->id}}"><i class="fa fa-shopping-cart"></i> @lang('front.add_to_enq_cart')</a>--}}
					</div>
				</div>
				<div class="lft-btn-hldr text-center" >
					{!!Form::open(['url'=>'cart'])!!}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit" class="btn btn-lg btn-warning add-to-cart"><i class="fa fa-shopping-cart"></i> @lang('front.request_for_quote')</button>
                    {!!Form::close()!!}
				</div>
			</div>
			<br class="clr-bth" />
			<a name="details"></a>
			<div id="tabs02" class="tabs02">
				<ul class="tabing">
				    <li><a class="tab_nav p_details" href="#tabs02-1">@lang('front.product_details')</a></li>

				    <li><a class="tab_nav" href="#tab_reviews">@lang('front.reviews') ({{$product->reviews->count()}})</a></li>
				</ul>
				<div id="tabs02-1" class="product_details">
					<div class="row">
						<div class="col-md-12" >{!!Helper::field($product,'details')!!}</div>
					</div>
				  	<div class="row" id="contact_supplier">
				  		<div class="col-md-2 opt attr" >@lang('front.product_manufactured_in'):</div>
				  		<div class="col-md-2 opt">{{App\Country::cname($product->manufactured_in)}}</div>
				  	</div>
				  	<div class="row" >
				  		<div  class="col-md-2 opt attr" >@lang('front.supply_ability'):</div>
				  		<div  class="col-md-2 opt">{{Product::capacity($product)}}</div>
				  	</div>
				  	<div class="row" >
				  		@if(is_array($more_details['attr']))
					  		@foreach($more_details['attr'] as $index=>$attr)
						  		<div class="col-md-2 opt attr">
						  			{{$attr}}:
						  		</div>
						  		<div class="col-md-2 opt">{{$more_details['value'][$index]}}
						  		</div>
						  	@endforeach
						@endif
						<hr>
						<div class="text-center">
							@if(!empty($product->video_url))
			                <div class="article_image">
			                    <iframe width="640" height="360" src="https://www.youtube.com/embed/{{Product::get_youtube_video_id($product->video_url)}}" frameborder="0" allowfullscreen></iframe>
			                </div>
			                @endif
		                </div>
				  	</div>
				</div>

				<div id="tabs02-2"  style="position: relative;">
					@if($locker && !PackageUsage::unlocked($product->user_id))
					<div class="lock_layer">
						{{--<a href="{{User::unlock_url('product', $product->id, $product->user_id)}}" class="unlock_act popup"><i class="fa fa-unlock"></i> @lang('front.unlock_contact_details')</a>--}}
						<div class="deal_locked">
							<a href="#" data-id="{{$product->id}}" data-user="{{$product->user_id}}" class="btn btn-warning btn_unlock_rfq"><i class="fa fa-unlock"></i> @lang('front.unlock_contact_details')</a>
							<div class="locked_data"></div>
						</div>
					</div>
					@endif
					@if($profile)
						@if($locker)
						<div class="row {{(PackageUsage::unlocked($product->user_id))?'':'locked'}}">
						@else
						<div class="row">
						@endif
							<div class="col-md-3">
								<div class="thumbnail text-center">
									@if(!empty($profile->co_logo))
										{!!HTML::image('uploads/'.$profile->co_logo,'',[])!!}
									@endif
									<div class="caption"><h3>{{$profile->company_name}}</h3></div>
								</div>
							    <div class="text-center">
									@foreach(App\UserProfile::$social as $field=>$label)
								        @if(!empty($profile->$field))
								            <a href="{{$profile->$field}}" target="_blank" class="btn btn-default"><i class="fa fa-{{$field}}"></i></a>
								        @endif
								    @endforeach
							    	<hr>
							    	<a href="{{User::url_contact($product->user)}}" class="btn btn-primary btn-sm popup"><i class="fa fa-paper-plane bluu"></i> {{trans('front.contact_details')}}</a>
							    	@if(!empty($user_website))
							    		<a href="{{App\UserWebsite::website_url($user_website)}}" class="btn btn-sm btn-warning">@lang('front.user_website')</a>
							    	@endif
							    </div>
							</div>

						</div>
					@endif
				</div>
				<div id="tab_reviews">
					<a name="add_review"></a>
					@if($reviews_count>0)
						@include('front.reviews.list')
					@else
						{!!trans('front.review_intro')!!}
					@endif
					<a href="{{url('reviews/add/'.$product->id.'/product')}}" class="btn btn-primary popup">@lang('front.review_now')</a>
				</div>
			</div>

			<div class="full-blk table-responsive"  id="contact_">
				<legend id = 'contact_seller'>{{trans('front.email_to_supplier')}}</legend>
				{!!Form::open(['class'=>'form-horizontal enquiry_form', 'url'=>'product/enquiry'])!!}
					{!!Form::hidden('product_id', $product->id)!!}
					@if(Auth::check())
						<div class="col-md-6">
							<div class="form-group {{$errors->has('name')?'has-error':''}}">
								{!!Form::label('name', trans('front.from'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									<a href="{{User::url_contact(Auth::user())}}" class="popup">
										{{Auth::user()->name}}
										@if(isset(Auth::user()->profile))
											<br><strong>{{Auth::user()->profile->company_name}}</strong>
										@endif
									</a>
								</div>
							</div>
						</div>
					@else
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('name',trans('front.name'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('name',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_name'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('email',trans('front.email'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('email',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_email'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('country',trans('front.country'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::select('country',$countries->lists('name','id'),$selected_country,['class'=>'form-control'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('mobile',trans('front.mobile'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_mobile'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
					@endif
					<div class="col-md-6">
						<div class="form-group">
							{!!Form::label('to',trans('front.send_to'), ['class'=>'col-md-3 control-label'])!!}
							<div class="col-md-8">
								<a href="{{User::url_contact($product->user)}}" class="popup">
									@if($product->user)
										{{$product->user->name}}<br>
										<div class="help-block">{{@$product->user->profile->company_name}}</div>
									@else
										Unknown Account!
									@endif
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-6">
						<div class="form-group">
							{!!Form::label('description',trans('front.description'), ['class'=>'col-md-3 control-label'])!!}
							<div class="col-md-8">
								{!!Form::textarea('description', null, ['class'=>'form-control', 'rows'=>'4','required'=>'required', 'placeholder'=>''])!!}
							</div>
						</div>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 0">
							<div class="col-md-offset-3 col-md-5">
								{!!Form::submit(trans('front.send_enquiry'), ['class'=>'btn btn-loading btn-warning'])!!}
							</div>
						</div>
					</div>
				{!!Form::close()!!}
			</div>
			@if(count($supplier_products))
				<div class="full-blk table-responsive">
					<legend>{{trans('front.more_prods_from_supplier')}}</legend>
					<div class="row lstng-stly-02">
						@foreach($supplier_products as $product_sup)
							<figure class="col-xs-12 col-md-6 col-md-3">

								<div>
									<span class="adToCrt"></span>
									<a href="{{Product::url($product_sup)}}" class="img-lnk-hldr">{{Product::image($product_sup)}}</a>
									<figcaption>
										<div><a href="{{Product::url($product_sup)}}">{{Helper::field($product_sup,'name')}}</a></div>
										@if($product_sup->min_quantity)
											<p class="inr-lst01"><span>@lang('front.min_ord_quantity'):</span> {{Product::MOQ($product_sup)}}</p>
										@endif
										<p class="inr-lst02"><span>@lang('front.FOB'):</span> <a href="{{Product::url($product_sup)}}#contact_supplier">@lang('front.get_latest_price')</a></p>
									</figcaption>
								</div>
							</figure>
						@endforeach
					</div>

				</div>
			@endif

			@if(count($related_products))
				<div class="full-blk table-responsive">
					<legend>{{trans('front.related_products')}}</legend>
					<div class="row lstng-stly-02">
						@foreach($related_products as $product)
							<figure class="col-xs-12 col-md-6 col-md-3">
								<div>
									<span class="adToCrt"></span>

									<a href="{{Product::url($product)}}" class="img-lnk-hldr">{{Product::image($product)}}</a>
									<figcaption>
										<div><a href="{{Product::url($product)}}">{{Helper::field($product,'name')}}</a></div>
										@if($product->min_quantity)
											<p class="inr-lst01"><span>@lang('front.min_ord_quantity'):</span> {{Product::MOQ($product)}}</p>
										@endif
										<p class="inr-lst02"><span>@lang('front.FOB'):</span> <a href="{{Product::url($product)}}#contact_supplier">@lang('front.get_latest_price')</a></p>
									</figcaption>
								</div>
							</figure>
						@endforeach
					</div>
					<div class="lft-btn">

						<a href="{{Category::url($product->category,true)}}" class="vw-mr-btn">@lang('front.view_more')</a>
					</div>
				</div>
			@endif
			<div class="addyours text-center"><a class="btn btn-success" href="{{url('dashboard/products/create')}}"><i class="fa fa-plus"></i> @lang('front.addyourproduct')</a></div>
		</div>
	</div>
</div>


@endsection
	@section('script')
		@include('dashboard.layouts.popup')
		{{--Load the print functionality--}}
		<script type="text/javascript" src="{{ URL::asset('frontend/js/app/print.js') }}"></script>
		<script type="text/javascript">

            $(document).on('click', '.btn_unlock_rfq', function(event) {
                event.preventDefault();
                let $btn = $(this);
                let $wrapper = $(this).siblings('.locked_data');
                let $id = $(this).data('id');
                let $user_id = $(this).data('user');
                $.ajax({
                    url: '{{url('package-check/unlock-product')}}',
                    type: 'POST',
                    dataType: 'html',
                    data: {product_id: $id,user_id:$user_id ,_token: '{{csrf_token()}}'},
                    beforeSend: function(){
                        $btn.val('Loading ...');
                        $btn.attr('disabled', 'disabled');
                    }
                })
                    .done(function(response) {
                        $wrapper.html(response);
                        $btn.slideUp('slow');
                    })
                    .fail(function(error) {
                        $wrapper.html('Error occured');
                        $btn.slideDwon('slow');
                        $btn.removeAttr('disabled');
                    })
                    .always(function() {
                        $btn.val('@lang('front.unlock_contact_details')');
                        $btn.removeAttr('disabled');
                    });
            });
			$(document).ready(function(){

			    $('#get_find').click(function() {
					$('.p_details').css('color','red');
					$('#contact_seller').css('color','black');
					$('#tabs02-1').css('height','100%');
					$('#contact_').css('height','');
                });
                $('#emailSupplink').click(function(e) {


                    $('.p_details').css('color','black');
                    $('#tabs02-1').css('height','');
                    $('#contact_seller').css('color','red');
                    $('#contact_').css('height','80%');

                });




            });

		</script>
@endsection