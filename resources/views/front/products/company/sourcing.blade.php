<?php
    use App\UserProfile;
    $preferred_supplier_type = (isset($user_profile) && $user_profile->preferred_supplier_type) ? explode(',', $user_profile->preferred_supplier_type):'';

    if(isset($user_profile->product_we_source) && !empty($user_profile->product_we_source)) {
        $user_profile->product_we_source = explode(',', $user_profile->product_we_source);
    }
    if(isset($user_profile->quantity) && !empty($user_profile->quantity)) {
        $user_profile->quantity = explode(',', $user_profile->quantity);
    }
    if(isset($user_profile->qtyunit) && !empty($user_profile->qtyunit)) {
        $user_profile->qtyunit = explode(',', $user_profile->qtyunit);
    }
?>

@if(!empty($user_profile->business_category))
<div class="form-group">
    {!! Form::label('catpre', trans('dash.business_category'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">{{$categories_parent[$user_profile->business_category]}}</div>
</div>
@endif

@if(!empty($user_profile->product_we_source))
<div class="form-group">
    {!! Form::label('pds', trans('dash.products_we_source'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @foreach($user_profile->product_we_source as $index=>$val)
            <div class="margbtn10">
                <strong>{{$val}}</strong>
                {{@$user_profile->quantity[$index]}} {{trans('dash.units')[@$user_profile->qtyunit[$index]]}}
                <i>({{trans('dash.yearly')}})</i>
            </div>
        @endforeach
    </div>
</div>
@endif

@if(!empty($user_profile->preferred_category))
    <div class="form-group">
        {!! Form::label('prec', trans('dash.preferred_category'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">{{@$categories_parent[$user_profile->preferred_category]}}</div>
    </div>
@endif

@if(!empty($user_profile->buying_frequency))
    <div class="form-group">
        {!! Form::label('buy', trans('dash.buying_frequency'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">{{trans('dash.buying_frequencies')[$user_profile->buying_frequency]}}</div>
    </div>
@endif

<div class="form-group">
    {!! Form::label('suplier', trans('dash.preferred_supplier_location'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if(!empty($user_profile->supplier_country_id1))
        <a href="#" class="btn btn-default">{{$countries->where('id',intval($user_profile->supplier_country_id1))->first()->name}}</a>
        @endif
        @if(!empty($user_profile->supplier_country_id2))
        <a href="#" class="btn btn-default">{{$countries->where('id',intval($user_profile->supplier_country_id2))->first()->name}}</a>
        @endif
        @if(!empty($user_profile->supplier_country_id3))
        <a href="#" class="btn btn-default">{{$countries->where('id',intval($user_profile->supplier_country_id3))->first()->name}}</a>
        @endif
    </div>
</div>

@if($preferred_supplier_type && is_array($preferred_supplier_type))
<div class="form-group">
    {!! Form::label('bty', trans('dash.preferred_supplier_type'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @foreach($preferred_supplier_type as $type)
            <a href="#" class="btn btn-default">{{trans('dash.business_types')[$type]}}</a>
        @endforeach
    </div>
</div>
@endif