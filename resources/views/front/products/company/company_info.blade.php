<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		@foreach(App\UserProfile::$front_profile_tabs as $index=>$tab)
			<li role="presentation" class="{{($index==1)?'active':''}}">
				<a href="#n{{$index}}" aria-controls="n{{$index}}" role="tab" data-toggle="tab">{{trans('dash.'.snake_case($tab[0]))}}</a>
			</li>
		@endforeach
	</ul>

	{!!Form::open(['class'=>'form-horizontal'])!!}
	<div class="tab-content co_info" style="padding-top: 20px;">
		@foreach(App\UserProfile::$front_profile_tabs as $index=>$tab)
			<div role="tabpanel" class="tab-pane {{($index==1)?'active':''}}" id="n{{$index}}">
				@include($tab[1])
			</div>
		@endforeach
	</div>
	{!!Form::close()!!}
</div>