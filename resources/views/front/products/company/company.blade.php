<?php
    use App\UserProfile;
    use App\Helper;
    $business_types = (isset($user_profile) && !empty($user_profile->business_type)) ? explode(',', $user_profile->business_type):'';
    $country = ($user_profile->co_country_id) ? App\Country::find($user_profile->co_country_id) : null;
?>

@if(isset($user_profile) && !empty($user_profile))
<div class="form-group">
    {!! Form::label('co', trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">{{$user_profile->company_name}}</div>
</div>

<div class="form-group">
    {!! Form::label('btype', trans('dash.business_type'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($business_types && is_array($business_types))
            @foreach($business_types as $type)
                {{trans('dash.business_types')[$type]}}, 
            @endforeach
        @endif
    </div>
</div>

    @if(!empty($user_profile->contact_f_name))
        <div class="form-group">
            {!! Form::label('cn', trans('dash.contact_person'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">{{$user_profile->contact_f_name}} {{$user_profile->contact_l_name}}</div>
        </div>
    @endif

    @if(!empty($user_profile->primary_email))
        <div class="form-group">
            {!! Form::label('pmail', trans('dash.primary_email'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">{{$user_profile->primary_email}}</div>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('cad', trans('dash.contact_address'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            @if(!empty($user_profile->co_address_line1) && !empty($user_profile->co_address_line2))
                {{$user_profile->co_address_line1}} {{$user_profile->co_address_line2}} <br>
            @endif
            {{$user_profile->co_city}}, {{$user_profile->co_state}},
            @if($country)
                {{$country->country_name}}
            @endif
            {{$user_profile->co_zipcode}}
        </div>
    </div>

    @if(!empty($user_profile->main_products))
        <div class="form-group">
            {!! Form::label('main_products', trans('dash.main_products'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                <?php $main_products = explode(',', $user_profile->main_products); ?>
                @foreach($main_products as $prod_item)
                    <a href="#" class="btn btn-default">{{$prod_item}}</a>
                @endforeach
            </div>
        </div>
    @endif

    @if(!empty($user_profile->other_products))
        <div class="form-group">
            {!! Form::label('other_products', trans('dash.other_products'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                <?php $user_profile->other_products = explode(',', $user_profile->other_products); ?>
                @foreach($user_profile->other_products as $prod_item)
                    <a href="#" class="btn btn-default">{{$prod_item}}</a>
                @endforeach
            </div>
        </div>
    @endif

    @if(!empty($user_profile->website))
        <div class="form-group">
            {!! Form::label('wb', trans('dash.website2'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                <a href="{{Helper::domain($user_profile->website)}}" target="_blank">{{$user_profile->website}}</a>
            </div>
        </div>
    @endif
@endif