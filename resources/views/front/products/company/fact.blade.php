<?php use App\UserProfile; ?>
@if(!empty($user_profile->established_year))
<div class="form-group">
    {!! Form::label('est', trans('dash.Year of Establishment'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">{{$user_profile->established_year}}</div>
</div>
@endif

@if(!empty($user_profile->employees_no))
<div class="form-group">
    {!! Form::label('asd', trans('dash.No. of Employees'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">{{trans('dash.employees_nums')[$user_profile->employees_no]}}</div>
</div>
@endif

@if(!empty($user_profile->turnover))
<div class="form-group">
    {!! Form::label('asd', trans('dash.Turnover'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! trans('dash.turnovers')[$user_profile->turnover] !!}
    </div>
</div>
@endif

@if(!empty($user_profile->legal_status))
<div class="form-group">
    {!! Form::label('asd', trans('dash.Legal Status'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! @trans('dash.legal_statuses')[$user_profile->legal_status] !!}
    </div>
</div>
@endif

@if(!empty($user_profile->legal_owner))
<div class="form-group">
    {!! Form::label('asd', trans('dash.Legal Owner'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">{!! $user_profile->legal_owner !!}</div>
</div>
@endif

@if(!empty($user_profile->office_size))
<div class="form-group">
    {!! Form::label('asd', trans('dash.Registered Office Size'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! trans('dash.office_sizes')[$user_profile->office_size] !!}
    </div>
</div>
@endif

@if(!empty($user_profile->certificates))
<div class="form-group {{ $errors->has('certificates') ? 'has-error' : ''}}">
    {!! Form::label('certificates', trans('dash.Certificates'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <?php $certificates = explode(',',$user_profile->certificates); ?>
        @foreach($certificates as $cert)
        	<label class="btn btn-default">{{$cert}}</label>
        @endforeach
    </div>
</div>
@endif