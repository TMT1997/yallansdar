@if($user_profile->have_brand && !empty($user_profile->brands))
    <div class="form-group">
        {!! Form::label('brands', trans('dash.Having Own Brand'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">{{$user_profile->brands}}</div>
    </div>
@endif

@if(!empty($user_profile->awards))
<div class="form-group">
    {!! Form::label('awd', trans('dash.Awards & recognition'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!$user_profile->awards!!}
        <br>
        {!!App\UserProfileDoc::generate_preview($user_profile->docs, 'awards', false)!!}
    </div>
</div>
@endif

@if(!empty($user_profile->quality_description))
<div class="form-group">
    {!! Form::label('q', trans('dash.Quality & Certification'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!$user_profile->quality_description!!}
        {!!App\UserProfileDoc::generate_preview($user_profile->docs, 'certification', false)!!}
    </div>
</div>
@endif

@if(!empty($user_profile->video))
    <div class="form-group">
        {!! Form::label('v', trans('dash.Business Videos'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8"><a href="{{$user_profile->video}}" target="_blank">{{trans('dash.preview')}}</a></div>
    </div>
@endif

@if(!empty($user_profile->co_brochure))
<div class="form-group">
    {!! Form::label('co_br', trans('dash.Company Brochures'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        <a class="pull-right btn btn-info" href="{{url('uploads/'.$user_profile->co_brochure)}}">{{trans('dash.preview')}}</a>
    </div>
</div>
@endif

@if(!empty($user_profile->overview))
    <div class="form-group">
        {!! Form::label('overview', trans('dash.overview'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">{!! $user_profile->overview !!}</div>
    </div>
@endif