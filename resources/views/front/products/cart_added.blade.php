<div class="white-popup-block">
	<legend>Result</legend>

	@if($status)
		<div class="alert alert-success">Product added successfully to favorites</div>
	@else
		@if(!Auth::check())
			<div class="alert alert-warning">Please <a href="{{url('login')}}">login</a> to be able to add to favorites</div>
		@endif
	@endif

	<div class="text-center">
		<a href="{{url('cart')}}" class="btn btn-sm btn-success">Check your Cart</a>
	</div>

</div>