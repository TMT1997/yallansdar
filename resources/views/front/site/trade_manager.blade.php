<?php
	$faq = [
		'-	من هو التريد مانجر ؟'=>'
			-	هو المسئول عن حسابك علي موقع يلا نصدر والمسئول عن منتجاتك منذ عرضها علي موقع يلا نصدر ومروراً بالبحث عن فرص التصدير وحتي اتمام عملية التصدير.
		',
		'-	ما هي مهام التريد مانجر ؟'=>'
			-	مهام التريد مانجر هي : 
			<ul>
				<li>إدراج منتجاتك علي موقع يلا نصدر والتأكد من ظهورها بشكل جيد باللغتين العربية والإنجليزية </li>
				<li>إنشاء موقع إلكتروني لعرض منتجاتك بشكل جذاب </li>
				<li>إدراج منتجاتك في كتالوج إلكتروني للتسويق عبر الإنترنت </li>
				<li>البحث عن اهم الأسواق الدولية المستهدفه لمنتجاتك</li>
				<li>تسويق منتجاتك من خلال شبكة مطوري الاعمال </li>
				<li>تسويق منتجاتك من خلال الإنترنت</li>
				<li>تسويق منتجاتك من خلال المعارض الدولية</li>
				<li>التفاوض مع المستوردين حول شروط الدفع والتسليم</li>
				<li>الإشراف علي التسعير والتعبئة والتغليف والنقل والشحن واستخراج الشهادات والأوراق المطلوبه .</li>
			</ul>
		',

		'-	ما هي شبكة مطوري الاعمال ؟'=>'
			-	هي خدمة مقدمه من يلا نصدر عن طريق المصريين المقيمين بالخارج , حيث يقوم مطور الاعمال بدراسة السوق والبحث عن أفضل الفرص والمنتجات التي يمكن تصديرها من مصر إلي الدولة التي يقيم فيها , كما يقوم بالتسويق لتلك المنتجات والتواصل مع المستوردين , كما يقوم بالإشراف من جهته علي عملية التعاقد مع المستورد , وأخيراً يقوم بالتأكد من وصول الشحنه مطابقة للمواصفات وقياس مدي رضاء المستورد عن المنتجات , ثم المتابعة مع نفس المستورد لتكرار عملية الاستيراد من مصر . 	
				<a href="'.url('site/join-us').'">المزيد ...</a>
		',

		'-	كيف يقوم التريد مانجر بالتسويق لمنتجاتي في المعارض الدولية ؟'=>'
			-	يقوم مدير التصدير  (Trade Manager  ) بتجميع منتجات أكثر من مصدر وتمثيلها في المعارض الدولية المختلفة و التي تقام علي مدار العام ويتحمل المصدر فقط تكلفه شحن المنتجات إلي بلد العرض.
		',
		'-	ليس لدي فكرة عن الأسواق المستهدفه ولا طريقة الشحن والتعبئة ولا كيفيه تحصيل ثمن الشحنه من المستورد , كيف سيساعدني التريد مانجر ؟'=>'
			-	يقوم مدير التصدير  (Trade Manager  ) بالبحث في الأسواق الخارجية عن أهم الأسواق المستهدفه وإبلاغك بها كما يقوم بعمل الدراسات التسويقية المناسبه لمنتجاتك والبحث عن أهم الفرص التسويقية كما يقوم بعقد لقاءات حواريه معك لإطلاعك علي طرق التعبئة والتغليف والنقل والشحن واستخراج الشهادات والاعتمادات المطلوبه
		',

		'-	كيف سأستلم نقودي من المستورد ؟ وكيف سيتم ضمان عملية الدفع ؟'=>'
			-	يقوم مدير التصدير  (Trade Manager  ) بالتفاوض مع المستورد حول طرق الدفع المناسبه لك وإبلاغك بطلبات المستورد لضمان الوضوح وتلافي العقبات والمخاطر التي يمكن أن تحدث أثناء عملية الشحن .
		',
		'-	كم تتكلف خدمة التريد مانجر ؟'=>'
			-	خدمة مدير التصدير  (Trade Manager) هي خدمة مدفوعه باشتراك سنوي بالإضافه إلي عموله عن كل عملية تصدير يتم الاتفاق عليها بحسب المنتج الذي يتم تصديره.
		',
		'-	كيف اشترك في  خدمة التريد مانجر ؟'=>'
			<ul>
				<li>املأ البيانات الخاصه بطلب التسجيل</li>
				<li>ادفع قيمة الاشتراك السنوي لخدمة Trade Manager </li>
				<li>تواصل مع الـ Trade Manager </li>
			</ul>
		'
	];

	$ifyou = [
		'rgba(233, 102, 86, 0.9)'=>['إذا كنت تبحث عن فرص تسويق جديدة !!', '<i class="fa fa-globe"></i>'],
		'rgba(52, 210, 147, 0.9)'=>['إذا لم يكن لديك وقت أو خبرة أو علاقات لتصدير منتجات للخارج !!', '<i class="fa fa-question-circle-o"></i>'],
		'rgba(58, 176, 226, 0.9)'=>['إذا لم يكن لديك إدارة لتسويق أو تصدير منتجاتك !!', '<i class="fa fa-comments-o"></i>'],
		'rgba(231, 192, 43, 0.9)'=>['إذا كنت تريد أن تعرف الشروط والمواصفات المطلوبه لتصبح منتجاتك جاهزة للتصدير !!', '<i class="fa fa-flag-o"></i>'],
	];

	$methods_work = [
		'blue'=>'املأ البيانات الخاصه بطلب التسجيل',
		'yellow'=>'ادفع قيمة الاشتراك السنوي لخدمة مدير التصدير (Trade Manager)',
		'orange'=>'تواصل مع الـ مدير التصدير  (Trade Manager)',
	];

	$methods_tips = [
		'blue'=>'املأ البيانات الخاصه بطلب التسجيل',
		'yellow'=>'ادفع قيمة الاشتراك السنوي لخدمة مدير التصدير (Trade Manager)',
		'orange'=>'تواصل مع الـ مدير التصدير  (Trade Manager)',
	];

	$we_provide = [
		'فرص تصدير '=>'
			يقوم مدير التصدير  (Trade Manager  ) بتوفير فرص تصدير لمنتجاتك وإبلاغك بطلبات العملاء خارج مصر	
		',
		'المعارض الدولية'=>'
			يقوم مدير التصدير  (Trade Manager  ) بتمثيل منتجاتك في المعارض الدولية الخاصة بمنتجاتك والتي تعقد علي مدار السنه في أكثر من دولة	
		',
		'شبكة مطوري الاعمال'=>'
			يقوم مدير التصدير  (Trade Manager  ) بتسويق منتجاتك من خلال شبكة مطوري الاعمال والمكونه من المصريين المقيمين بالخارج.
		',
		'التسجيل المجاني علي يلا نصدر'=>'
			يتم إدراج منتجاتك علي موقع يلا نصدر بالصور والمواصفات المطلوبه تمهيدا لتسويقها دولياً من خلال سوق يلا نصدر الإلكتروني
		',
		'متجر إلكتروني'=>'
			يتم إدراج بياناتك ومنتجاتك في موقع إلكتروني مصغر ذو مظهر جذاب , بنطاق فرعي باسم شركتك : مثال ( company.yallansadar.com ) تستطيع وضعه علي مطبوعات شركتك واستخدامه في الترويج لمنتجاتك من خلال مواقع التواصل الإجتماعي , كما يمكنك التحكم في محتواه من خلال لوحة التحكم الخاصة بك 
		',
		'التسويق الإلكتروني'=>'
			تستطيع الآن أن تصل إلي آلاف المستوردين من خلال تقنيات التسويق الإلكتروني الحديثة , حيث يقوم فريق التسويق الإلكتروني بالترويج لمنتجاتك من خلال محركات البحث , ومواقع التواصل الاجتماعي , والأدلة التجارية والصناعية , وحملات البريد الإلكتروني والمنتديات المتخصصة , والاسواق الدولية
		',
		'مشاكل التصدير'=>'
			يساعدك مدير التصدير  (Trade Manager  ) علي التغلب علي المشكلات المرتبطة بعملية التصدير مثل : التعبئة والتغليف والشحن والتخليص والجمركي وإصدار الشهادات والاوراق الرسمية والتمويل والتسعير, وفي حالة الطلبيات الكبيرة , يستطيع مدير التصدير تجميع الطلبية من أكثر من مصدر من صغار المصدرين
		',
		'القيمة المضافه'=>'
			يربط مدير التصدير  (Trade Manager  ) المصدرين ومزودي خدمات التصدير في شبكة علاقات قوية بهدف حل مشاكل التصدير وتطوير المنتج وإعطاؤه قيمة مضافة للإنتقال من مرحلة تصدير المنتج الخام إلي مرحلة تصدير المنتجات تامة التصنيع , عالية الجودة بما يتماشي مع المواصفات الدولية
		',
	];
?>
@extends('front.layouts.site')
@section('title') مدير التصدير - Trade Manager | @endsection
@section('content')
	<div id="page-banner" class="page-banner" style="position: relative;">
		{!!HTML::image('frontend/site/images/export-manag.jpg')!!}
		<div class="page-detail"></div>
		<div style="position: absolute; bottom: 0px; padding:20px 0; background: rgba(0,0,0,0.3); color: #fff; text-align: center; width: 100%; margin:0 auto; font-size: 35px;">
			<strong>مدير التصدير</strong><br>
			Trade Manager
		</div>
	</div>	
	
	<div class="container" style="direction: rtl;">
		<div class="row" style="margin-bottom: 50px;">
			<div class="entry-content">
				<span class="quote" style="margin-bottom: 10px">
					خدمة مدير التصدير هي خدمة اختيارية متاحة للشركات التي ترغب بأن يتوسط مسئول من يلا نصدر في التسويق وإبرام الصفقات التجارية والحصول علي فرص التصدير ومتابعة الإجراءات البنكية والشحن والتخليص الجمركي . 
					<br><br>
					كما يقوم مدير التصدير  (Trade Manager  ) بتكوين شبكة من مطوري الاعمال من المصريين الذين يعيشون في الخارج , وعلي إطلاع بمتطلبات السوق في البلدان التي يعيشون فيها وربط مطور الاعمال مع المصدرين المعتمدين لتوفير هذه المنتجات مع ضمان جودة المنتج وإلتزام المُصدر بمواصفات المنتج ومواعيد التسليم.
				</span>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 50px;">
			@foreach($ifyou as $index=>$item)
				<div class="col-md-3 pull-right">
					<div style="background: {{$index}}; color: #fff; padding:25px 20px 0 20px; min-height: 170px; font-size: 16px; border-radius:10px;" class="text-center">
						<div class="join-ico text-center">{!!$item[1]!!}</div>
						<strong>{!!$item[0]!!}</strong>
					</div>
				</div>
			@endforeach

			<div class="clearfix"></div><br>
			<div class="text-center">
				<a href="#devrequest" class="btn btn-default">اطلب خدمة Trade Manager</a>
			</div>
		</div>

		<div class="row" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">طريقة العمل</legend>
			<div class="wrap">
				<div class="process">
					<ul class="process__list">
						<?php $counter=1; ?>
						@foreach($methods_work as $color=>$method)
						<li style="width: 31%" class="process__item process__item_{{$color}}" data-toggle="tooltip" data-placement="bottom" title="{{$methods_tips[$color]}}">
							<div class="item__inner" style="padding: 0 52px 0 95px">
								<p class="item__content" style="font-size: 16px;">{!!$method!!}</p>
								<span class="step">{{$counter}}</span>
							</div>					
						</li>
						<?php $counter++; ?>
						@endforeach
					</ul>
				</div>
			</div>
		</div>

		<div class="row" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">نحن نوفر لك</legend>
			<ol>
			@foreach($we_provide as $item=>$details)
				<div class="col-md-3 pull-right" style="margin-bottom: 10px;">
					<div class="text-center offer_box">
						<div class="join-ico checks"><i class="fa fa-check-square-o"></i></div>
						<strong>{!!$item!!}</strong><br>
						<span>
							{!!$details!!}
						</span>
					</div>
				</div>
			@endforeach
			</ol>
		</div>

		<div class="" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">Q & A عن خدمة التريد مانجر</legend>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php $count = 1; ?>
				@foreach($faq as $q=>$answer)
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading{{$count}}">
				      <h4 class="panel-title">
				        <a class="{{($count!=1)?'collapsed':''}}" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$count}}" aria-expanded="true" aria-controls="collapse{{$count}}">
				          {!!$q!!}
				        </a>
				      </h4>
				    </div>
				    <div id="collapse{{$count}}" class="panel-collapse collapse {{($count==1)?'in':''}}" role="tabpanel" aria-labelledby="heading{{$count}}">
				      <div class="panel-body">
				        {!!$answer!!}
				      </div>
				    </div>
				  </div>
				  <?php $count++; ?>
				@endforeach
			</div>
		</div>

		<div class="well">
			<a name="devrequest"></a>
			<div class="contact-wrap row">
				<div class="col-md-8 col-md-offset-2">
					<legend class="text-primary text-center" style="background:url({{url('frontend/images/trade_manager.jpg')}}) no-repeat center bottom; height:250px; color: #fff; position: relative; border-radius:10px;">
						<div style="position: absolute; bottom: 20px; text-align: center; width: 100%; margin: 0 auto; color: #f0f0f0; font-size: 20px; font-weight: bold;">اطلب خدمة مدير التصدير </div>
					</legend>
		            <div class="status alert alert-success" style="display: none"></div>
		            <form role="form" method="POST" enctype="multipart/form-data" class="contact-form" id="businessDevForm" name="businessDevForm" action="{{url('site/trade-managers-request')}}">
		                {{ csrf_field() }}
		                <div class="form-group">
		                    <input class="form-control" name="name" required="" type="text" placeholder="الاسم"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="email" required="" type="email" placeholder="البريد الإلكتروني"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="phone" required="" type="text" placeholder="تليفون"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="city" required="" type="text" placeholder="المدينة أو المحافظة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="country" required="" type="text" placeholder="الدولة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="jobtitle" required="" type="text" placeholder="المهنة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="company" required="" type="text" placeholder="الشركة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="products" required="" type="text" placeholder="المنتجات التي ترغب في العمل بها"></input>
		                </div>
		                <div class="form-group"><label>حمل صورة بطاقة الرقم القومي </label></div>
		                <div class="form-group"><input class="form-control" type="file" name="national_id"></input></div>

		                <div class="form-group"><label>حمل صورة السجل التجاري </label></div>
		                <div class="form-group"><input class="form-control" type="file" name="commercial_record"></input></div>

		                <div class="form-group"><label>طرق الاتصال </label></div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="skype" type="text" placeholder="سكايب"></input>
		                		<span class="input-group-addon"><i class="fa fa-skype"></i></span>
		                	</div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="facebook" type="text" placeholder="فيس بوك"></input>
		                    	<span class="input-group-addon"><i class="fa fa-facebook-square"></i></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
			                    <input class="form-control" name="linkedin" type="text" placeholder="لينكد ان"></input>
			                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
			                   </div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="twitter" type="text" placeholder="تويتر"></input>
		                    	<span class="input-group-addon"><i class="fa fa-twitter"></i></span>
		                    </div>
		                </div>
		                <div class="form-group text-center">
		                    <button class="btn btn-success" type="submit">إرسال</button>
		                </div>
		            </form>
		            <div class="loading_form hidden">
		                <div class="well text-center"><h3>جاري إرسال الرسالة ...</h3> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
		            </div>
	            </div>
	        </div>
	    </div>
	</div>

	<div id="call-out-section" class="call-out-section ow-background services-call-out hidden">
		<div class="container">
			<div class="call-out-details">
				<h3>اتصل بنا الان</h3>
				<p>يسعدنا تلقي آرائكم ومقترحاتكم واستفساراتكم علي الأرقام التالية</p>	
				<p style="font-size: 25px">01111929530 - 01062166175‏</p>
			</div>
		</div>
	</div>
	<div id="partner-section" class="partner-section ow-section partners-background" style="display: none">
		<div class="container">
			<div class="section-header">
				<h3>{!!HTML::image('frontend/site/images/icon/sep-icon.png')!!}clients & partners</h3>
			</div>
			<div id="make-clean-partner" class="owl-carousel owl-theme">
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-1.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-2.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-3.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-4.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-5.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-6.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-1.png')!!}</a>
				</div>
				<div class="item">
					<a title="Partner-Logo" class="partner-logo" href="#">{!!HTML::image('frontend/site/images/partners/partners-2.png')!!}</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	{!!HTML::script('dash/packages/validator/jquery.validate.min.js')!!}
	{!!HTML::script('dash/packages/validator/additional-methods.min.js')!!}
	<script type="text/javascript">
	    $("#businessDevForm").validate({
	        rules: {
	            
	        },
	        messages: {
	            name:'ادخل اسمك الشخصي',
	            phone:'ادخل رقم تليفون أو موبايل',
	            email:'ادخل بريدك الإلكتروني بصورة صحيحة',
	            jobtitle:'ادخل المهنة أو المسمى الوظيفي الخاص بك',
	            company:'ادخل اسم الشركة التي تعمل بها',
	            products: 'ادخل المنتجات التي ترغب في العمل بها',
	        },
	        ignore: [] 
	    });
	    $(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
    </script>
@endsection