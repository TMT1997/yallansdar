@extends('front.layouts.site')
@section('content')
	<div id="page-banner" class="page-banner">
		{!!HTML::image('frontend/site/images/about-banner.jpg')!!}
		<div class="page-detail"></div>
	</div>	
	<div class="entry-content">
		<span class="quote">
			@lang('front.about_yallansadar_new')
		</span>
	</div>	
	<Section id="welcome-section" class="welcome-section ow-section" style="padding-top: 0px">
		<div class="container" style="background: #262f46;width: 100%">
			<div class="col-md-8 col-sm-7 welcome-content" style="width: 100%">
				<div class="section-header">
					<h3 style="color: #fff;text-align:center;">لماذا يلانصدر ؟؟</h3>
				</div>
				<div class="welcome-content-box row">
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/arabic.png')!!}
						@if(App::isLocale('en'))
                            <h4>Arabic marketplace</h4>
                            <p>Yalla Nsadar works with both Arabic and English language, where you can present your products in Arabic and we translate it professionally into English to be offered to the importers</p>
                        @else
                            <h4>سوق باللغة العربية</h4>
                            <p>يلا نصدر تدعم اللغتين العربية والإنجليزية , حيث تستطيع أن تعرض منتجاتك باللغة العربية , في حين أننا نقوم بالترجمة الاحترافية للغة الإنجليزية للعرض علي المستوردين</p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/united-market.png')!!}
						@if(App::isLocale('en'))
                            <h4>Easy to use</h4>
                            <p>Registration and submitting products only takes few mints, after that you are able to present your product specifications in a high professional way, you can also have the technical support during the registration process</p>
                        @else
                            <h4>سهولة في الاستخدام</h4>
                            <p>لا تستغرق عملية التسجيل وإدراج المنتجات سوي دقائق معدودة , تصبح بعدها قادر علي عرض مواصفات منتجاتك باحترافية عالية , كما يمكنك الحصول علي مساعدة الدعم الفني اثناء عملية التسجيل</p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/Foreign-exhibitions-icon.png')!!}
						@if(App::isLocale('en'))
                            <h4>Export opportunity</h4>
                            <p>You can gain an export opportunity through our baying requests that are published on Yalla Nsadar from the importers or our marketing team who works on getting export opportunities from trusted importers all over the world</p>
                        @else
                        <h4>فرص التصدير</h4>
                            <p>تستطيع أن تحصل علي فرص التصدير من خلال عروض الشراء التي يتم نشرها علي يلا نصدر بواسطة المستوردين أو بواسطة فريق التسويق الذي يعمل علي جلب فرص تصدير من مستوردين موثوقين من جميع انحاء العالم</p>
                        @endif
					</div>
				</div>
			</div>			
			<div class="col-md-8 col-sm-7 welcome-content" style="width: 100%">
				<div class="welcome-content-box row">
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/search.png')!!}
						@if(App::isLocale('en'))
                            <h4>Export manager</h4>
                            <p>We offer the export manager service, who can help you to get over problems of the export proses, such as; packing and packaging, shipping, custom clearness, issuing certificates, official documents, financing and pricing, in case of big orders export manager can work on collecting the order from several small produces</p>
                        @else
                            <h4>مدير التصدير</h4>
                            <p>نوفر خدمة مدير التصدير الذي يساعدك في التغلب علي المشكلات المرتبطة بعملية التصدير مثل : التعبئة والتغليف والشحن والتخليص والجمركي وإصدار الشهادات والاوراق الرسمية والتمويل والتسعير, وفي حالة الطلبيات الكبيرة , يستطيع مدير التصدير تجميع الطلبية من أكثر من مصدر من صغار المصدرين</p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/export.png')!!}
						@if(App::isLocale('en'))
                            <h4>Offers and deals</h4>
                            <p>If you are looking for a partner, investor, service provider or company Issuing certificates and licenses, you can search in the offers and deals presented on the platform, you can also request a service that is not offered and receive offers from the service providers to compare them, and for export service providers they can offer your services for exporters and go in commercial partnership</p>
                        @else
                            <h4>عروض وصفقات</h4>
                            <p>إذا كنت تبحث عن شريك أو ممول أو مزود خدمة أو جهه إصدار شهادات وتراخيص , فيمكنك البحث في العروض والصفقات المطروحة , كما يمكنك طلب خدمة غير موجودة وتلقي العروض من مزودي هذه الخدمة لتقارن بينها , وبالنسبة لمزودي خدمات التصدير يمكنك عرض خدماتك علي المصدرين والدخول في شراكات تجارية </p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/new-market.png')!!}
						@if(App::isLocale('en'))
                            <h4>Value added</h4>
                            <p>Yalla Nsadar links exporters and service providers in a strong network for the purpose of solving export problems and developing the product with adding value to it so that we can move from exporting row material to exporting final product with high quality that matches the international standards</p>
                        @else
                            <h4>القيمة المضافة</h4>
                            <p>يربط موقع يلا نصدر المصدرين ومزودي خدمات التصدير في شبكة علاقات قوية بهدف حل مشاكل التصدير وتطوير المنتج وإعطاؤه قيمة مضافة للإنتقال من مرحلة تصدير المنتج الخام إلي مرحلة تصدير المنتجات تامة التصنيع , عالية الجودة بما يتماشي مع المواصفات الدولية</p>
                        @endif
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-7 welcome-content" style="width: 100%">
				<div class="welcome-content-box row">
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/search.png')!!}
						@if(App::isLocale('en'))
                            <h4>Mini website</h4>
                            <p>Your data and your products will be listed in a small, professional and attractive-looking website, with a subdomain with your company name, like www.yallansadar.com/yourcompany, which you can use on your official documents and use it to promote your product through social media websites, you can also control your site components from your dashboard </p>
                        @else
                            <h4>موقع إلكتروني مصغر</h4>
                            <p>يتم إدراج بياناتك ومنتجاتك في موقع إلكتروني مصغر ذو مظهر جذاب , بنطاق فرعي باسم شركتك : مثال                                                            (www.yallansadar.com/yourcompany ) تستطيع وضعه علي مطبوعات شركتك واستخدامه في الترويج لمنتجاتك من خلال مواقع التواصل الإجتماعي , كما يمكنك التحكم في محتواه من خلال لوحة التحكم الخاصة بك </p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/export.png')!!}
						@if(App::isLocale('en'))
                            <h4>Window on the international exhibitions </h4>
                            <p>Yalla Nsadar provides you with articles, information, studies, statistics, strategies and news that will help you have knowledge about the international markets and international agreements through Yalla Nsadar blog</p>
                        @else
                            <h4>نافذة علي الاسواق الدولية</h4>
                            <p>يقوم يلا نصدر بتزويدك بالمقالات والمعلومات والدراسات والاحصاءات والاستراتيجيات والاخبار التي تساعدك في التعرف علي الاسواق الخارجية والاتفاقيات الدولية من خلال مدونة يلا نصدر </p>
                        @endif
					</div>
					<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/new-market.png')!!}
						@if(App::isLocale('en'))
                            <h4>International fairs</h4>
                            <p>Yalla Nsadar represents your product in the international fairs and open new markets to expand the export process</p>
                        @else
                            <h4>المعارض الدولية</h4>
                            <p>يقوم يلا نصدر بتمثيل منتجاتك في المعارض الخارجية وفتح اسواق خارجية جديدة بهدف توسيع نطاق عملية التصدير</p>
                        @endif
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-7 welcome-content" style="width: 100%">
				<div class="welcome-content-box row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 welcome-box" style="text-align: center;color: #ffffff">
						{!!HTML::image('frontend/site/images/welcome/new-market.png')!!}
						@if(App::isLocale('en'))
                            <h4>From domestic to international</h4>
                            <p>Thousands of byers are looking for your products, now you can reach out to them through the new marketing Techniques, add your products on Yalla Nsadar and our digital marketing team will promote your products through; search engines, social media, commercial and industrial directories, E-mail companies, Specialized forums and international markets</p>
                        @else
                            <h4>من المحلية إلي العالمية</h4>
                            <p>آلاف المشترين يبحثون عن منتجاتك , تستطيع الآن أن تصل إليهم من خلال تقنيات التسويق الإلكتروني الحديثة , اضف منتجاتك علي يلا نصدر وسوف يقوم فريق التسويق الإلكتروني بالترويج لمنتجاتك من خلال محركات البحث , ومواقع التواصل الاجتماعي , والأدلة التجارية والصناعية , وحملات البريد الإلكتروني والمنتديات المتخصصة , والاسواق الدولية</p>
                        @endif
					</div>
				</div>
			</div>
	</section>
	<div id="testimonial-section" class="testimonial-section ow-section hidden" style="padding-top: 5px;margin:0 auto;">
		<div class="container">
			<div class="welcome-content-box row">
				<div class="col-md-offset-2 col-md-4 col-sm-6 welcome-box" style="text-align: center;">
	                <h4 style="font-size: 25px;color: #ffaf03">Tarek Hosny </h4>  
					<h5 style="font-size: 17px;color: #03509d">CEO & Founder</h5>
					{!!HTML::image('frontend/site/images/partners/TAREK.png')!!}
					<p>CEO & Founder of Yalla Nsadar <br>E-marketing trainer & B2B marketing Specialist</p>
				</div>				
				<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;">
                 	<h4 style="font-size: 25px;color: #ffaf03"> Reem M. Sherif</h4>  
					<h5 style="font-size: 17px;color: #03509d">Business Development Manager</h5>
					{!!HTML::image('frontend/site/images/partners/REEM.png')!!}
					<p>6 years experience business development <br>across industries</p>
				</div>
			</div>
			<div class="welcome-content-box row">
				<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;">
               		<h4 style="font-size: 25px;color: #ffaf03"> Amir Haron</h4>  
					<h5 style="font-size: 17px;color: #03509d">Web Developer</h5>
					{!!HTML::image('frontend/site/images/partners/AMIR.png')!!}
					<p>8 years experience in Web Development,<br>Database and Server Administration</p>
				</div>
				<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;">
                	<h4 style="font-size: 25px;color: #ffaf03"> Mohamed El Ramly</h4>  
					<h5 style="font-size: 17px;color: #03509d">WEB & GRAPHIC DESIGNER</h5>
					{!!HTML::image('frontend/site/images/partners/ELRAMLY.png')!!}
					<p>5 years experience in Web Design <br> & Graphic Design</p>
				</div>
				<div class="col-md-4 col-sm-6 welcome-box" style="text-align: center;">
               		<h4 style="font-size: 25px;color: #ffaf03"> Marwan Nabil</h4>  
					<h5 style="font-size: 17px;color: #03509d">Software Engineer</h5>
					{!!HTML::image('frontend/site/images/partners/marwan.png')!!}
					<p>8 years experience in Web & Mobile Development, <br>VPS, Cloud Servers Management</p>
				</div>
			</div>
		</div>
	</div>	
	<div id="call-out-section" class="call-out-section ow-background services-call-out hidden">
		<div class="container">
			<div class="call-out-details">
				<h3>اتصل بنا الان</h3>
				<p>يسعدنا تلقي آرائكم ومقترحاتكم واستفساراتكم علي الأرقام التالية</p>
				<p style="font-size: 25px"> 01062166175‏ </p>
			</div>
		</div>
	</div>

<style type="text/css">
	body, .call-out-section h3,.welcome-content h3,.team_title,.welcome-box h4 {font-family:'Droid Arabic Kufi','Lato',sans-serif}
	.welcome-content h3 {font-size: 22px;}
	.entry-content .quote{display:inline-block;width:100%;font-style:italic;font-size:16px;margin:50px 0;padding-left:115px;padding-right:35px;position:relative;line-height:28px;color:#232323;text-transform:uppercase;text-align:right;direction:rtl}
	.entry-content-en .quote{display:inline-block;width:100%;font-style:italic;font-size:16px;margin:50px 0;padding-left:115px;padding-right:35px;position:relative;line-height:28px;color:#232323;text-transform:uppercase}
	.entry-content .quote:before{background-image:url("frontend/site/images/icon/quote-icon.png");background-repeat:no-repeat;background-position:center;content:"";height:57px;width:57px;display:inline-block;position:absolute;left:35px;border:2px solid #2196f3;border-radius:100%;top:10px}
	.entry-content-en .quote:before{background-image:url("frontend/site/images/icon/quote-icon.png");background-repeat:no-repeat;background-position:center;content:"";height:57px;width:57px;display:inline-block;position:absolute;left:35px;border:2px solid #2196f3;border-radius:100%;top:10px}
	.widget_social ul{padding-left:0;text-align:right;margin-bottom:25px}
	.widget.widget_social li{display:inline-block;width:auto;margin:0 5px}
	.widget_social li > a{border:2px solid #e1e5ea;box-shadow:0 4px 0 rgba(214,215,217,0.25);width:33px;height:33px;display:inline-block;text-align:center;line-height:30px}
	.widget.widget_social li > a > i{margin:0;color:#999}
	.widget_social li > a:hover{border:2px solid #fff;box-shadow:0 4px 0 #fff}
	.widget_social li > a:hover i{color:#fff}
	.services-call-out.call-out-section{background:#ededed}
	.call-out-details > .call-out-icon{width:62px;height:62px;border:1px solid #f3e721;border-radius:100%;margin:0 auto 20px;line-height:62px}
	.call-out-section h3{font-size:25px;color:#3290dc;margin-bottom:20px}
	.call-out-section p span{display:block;color:#000}
	.call-out-section{padding:67px 0;text-align:center}
	.call-out-section .section-header{color:#2196f3;margin-bottom:30px}
	.call-out-section p{padding:0 19%;margin-bottom:39px}
	.welcome-section .section-header h3:after{background-repeat:no-repeat;content:"";display:inline-block;height:13px;width:100%}
	.welcome-section .col-md-8 p{line-height:24px;margin-bottom:32px}
	.welcome-content{margin-top:50px}
	.welcome-section .welcome-content a,.industry-serve .row > a{border:2px solid #2196f3;box-shadow:0 4px 0 rgba(33,150,243,0.25);display:inline-block;padding:8px 20px;letter-spacing:1px;margin-right:20px;border-radius:0;margin-bottom:15px}
	.welcome-content-box{width:100%}
	.welcome-box h4{display:block;margin-top:18px;margin-bottom:12px;font-size:18px;}
	.team-section .section-header{margin-bottom:38px}
	.team-section{background-color:#f7f9fb}
	.team-section .col-md-3 > p{padding-left:40px;line-height:28px}
	.team-box{position:relative;cursor:pointer}
	.team-box-inner h4{font-weight:900}
	.team-box:hover .team-box-inner{opacity:1;height:100%}
	.team-box .team-box-inner{position:absolute;top:0;left:0;width:100%;height:0;color:#fff;opacity:0;background-color:rgba(33,150,243,0.8);transition:opacity .5s;-webkit-transition:all ease .8s;-moz-transition:all ease .5s;-o-transition:all ease .5s;-ms-transition:all ease .5s;transition:all ease .5s;padding:70px 0;border:4px solid #f3e721}
	.team-box-inner hr{width:43px;height:3px;background-color:#f3e721;margin-top:0;margin-bottom:5px}
	.team-box-inner p{font-weight:600}
	.team-section .owl-theme .owl-controls .owl-buttons{position:absolute;top:auto;left:-30.5%;right:auto;bottom:5px}
	.welcome-content-box{width:100%}
	.welcome-box h4{display:block;color:#fff}
	.welcome-box p{color:#777}
	.testimonial-section h5{margin-top:5px}
	.team_title {text-align: center; font-size: 20px; color: #666; margin: 40px auto 20px auto;}
</style>
@endsection