@extends('front.layouts.site')

@section('content')

	<div class="container" style="margin:30px auto;">
      <div class="col-md-12">
        @if(Session::has('flash_message'))
            <div class="text-center alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {!!Session::get('flash_message')!!}
            </div>
        @endif

        	<div class="text-center">
        		<a href="{{url('site')}}" class="btn btn-default">انتقل إلى الصفحة الرئيسية</a>
        	</div>
      </div>
  </div>

@endsection