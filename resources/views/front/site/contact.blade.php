@extends('front.layouts.site')
@section('title') اتصل بنا | @endsection
@section('content')
	<div id="page-banner" class="page-banner">
		{!!HTML::image('frontend/site/images/inr-banner.jpg')!!}
	</div>
	<div id="contact-detail" class="contact-detail">
		<div class="container">
			<div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box">
				{!!HTML::image('frontend/site/images/icon/address-icon.png')!!}
				<h3>يمكنكم مراسلتنا بالإيميل</h3>
				<p>
					<a title="Mail Title" href="mailto:tarek@yallansadar.com">Tarek@yallansadar.com</a><br>
					<a title="Mail Title" href="mailto:export@yallansadar.com">Export@yallansadar.com</a>
				</p>
			</div>			
			<div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box">
				{!!HTML::image('frontend/site/images/icon/customer-service-icon.png')!!}
				<h3>يمكنكم الاتصال بنا الآن</h3>
				<p style="direction:ltr">+(02) 01062166175‏</p>
				<p></p>
			</div>			
			<div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box">
				{!!HTML::image('frontend/site/images/icon/area-icon.png')!!}
				<h3>العنوان</h3>
				<p>مدينة 6 أكتوبر  - الحي السادس - مبنى هيئة تنمية الصادرات المصرية</p>
			</div>
		</div>
	</div>
	
	<div id="contact" class="contact-form-section ow-section">
		<div class="container">
			<div class="row">
				<h3>يمكنكم الاتصال بنا الان </h3>
				<p>يسعدنا تلقى أرائكم وإقتراحاتكم.</p>
				<form class="main-contact-form" method="post">					
					<div class="col-md-6 col-sm-6">
						<label>الإيميل <span>*</span></label>
						<input type="text" id="input_email" name="contact-email" required />
					</div>
					<div class="col-md-6 col-sm-6">
						<label>الاسم <span>*</span></label>
						<input type="text" id="input_name" name="contact-name" required />
					</div>
					<div class="col-md-6 col-sm-6">
						<label>التليفون <span>*</span></label>
						<input type="text" id="input_phone" name="contact-phone" required />
					</div>
					<div class="col-md-6 col-sm-6">
						<label>العنوان</label>
						<input type="text" id="input_street" name="contact-street" />
					</div>
					<div class="col-md-12 col-sm-12">
						<label>وصف قصير</label>
						<textarea id="textarea_message" name="contact-message"></textarea>
					</div>
					<div class="col-md-12 col-sm-12">
						<input type="submit" class="btn" id="btn_smt" value="ارسال" />
					</div>
					<div class="col-md-12 col-sm-12">
						<div id="alert-msg" class="alert-msg"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="map-section">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3456.5866311286018!2d30.905470334806417!3d29.96256658191216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14585604c0b941af%3A0xa8f556e28a6aa09a!2z2LPZhtiq2LEg2KfZhNmG2K7ZitmEIDLYjCDYtNin2LHYuSA02Iwg2KfZhNis2YrYstip2Iwg2YXYtdix!5e0!3m2!1sar!2sus!4v1482935253006" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
@endsection