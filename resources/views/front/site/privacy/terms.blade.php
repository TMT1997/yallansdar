@extends('front.layouts.site')
@section('title') Terms and Conditions | @endsection
@section('content')
<style type="text/css">
	.page_head {position: absolute; bottom: 0px; padding:20px 0; background: rgba(0,0,0,0.3); color: #fff; text-align: center; width: 100%; margin:0 auto; font-size: 35px;}
</style>
	<div id="page-banner" class="page-banner" style="position: relative;">
		{!!HTML::image('frontend/images/banner122.png')!!}
		<div class="page-detail"></div>
		<div class="page_head"><strong>Terms and Conditions</strong></div>
	</div>	

	<div class="container" style="direction: rtl;">
		<div class="row" style="margin-bottom: 50px; direction: ltr; padding-top:20px">
			<div class="col-md-12">

				<p><strong class="text-primary">Introduction</strong><br />
				These Website Standard Terms and Conditions written on this webpage shall manage your use of this website. These Terms will be applied fully and affect to your use of this Website. By using this Website, you agreed to accept all terms and conditions written in here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions.<br />
				Minors or people below 18 years old are not allowed to use this Website.</p>

				<p><strong class="text-primary">Intellectual Property Rights</strong><br />
				Other than the content you own, under these Terms, Yalla Nsadar and/or its licensors own all the intellectual property rights and materials contained in this Website.<br />
				You are granted limited license only for purposes of viewing the material contained on this Website.</p>

				<p><strong class="text-primary">Restrictions</strong><br />
				You are specifically restricted from all of the following<br />
				&bull;&nbsp;&nbsp; &nbsp;publishing any Website material in any other media;<br />
				&bull;&nbsp;&nbsp; &nbsp;selling, sublicensing and/or otherwise commercializing any Website material;<br />
				&bull;&nbsp;&nbsp; &nbsp;publicly performing and/or showing any Website material;<br />
				&bull;&nbsp;&nbsp; &nbsp;using this Website in any way that is or may be damaging to this Website;<br />
				&bull;&nbsp;&nbsp; &nbsp;using this Website in any way that impacts user access to this Website;<br />
				&bull;&nbsp;&nbsp; &nbsp;using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;<br />
				&bull;&nbsp;&nbsp; &nbsp;engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;<br />
				&bull;&nbsp;&nbsp; &nbsp;using this Website to engage in any advertising or marketing.<br />
				Certain areas of this Website are restricted from being access by you and Yalla Nsadar may further restrict access by you to any areas of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality as well.</p>

				<p><strong class="text-primary">Your Content</strong><br />
				In these Website Standard Terms and Conditions, &ldquo;Your Content&rdquo; shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant Yalla Nsadar a non-exclusive, worldwide irrevocable, sub licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.<br />
				Your Content must be your own and must not be invading any third-party&rsquo;s rights. Yalla Nsadar reserves the right to remove any of Your Content from this Website at any time without notice.</p>

				<p><strong class="text-primary">No warranties</strong><br />
				This Website is provided &ldquo;as is,&rdquo; with all faults, and Yalla Nsadar express no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</p>

				<p><strong class="text-primary">Limitation of liability</strong><br />
				In no event shall Yalla Nsadar, nor any of its officers, directors and employees, shall be held liable for anything arising out of or in any way connected with your use of this Website whether such liability is under contract. &nbsp;Yalla Nsadar, including its officers, directors and employees shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>

				<p><strong class="text-primary">Indemnification</strong><br />
				You hereby indemnify to the fullest extent Yalla Nsadar from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.</p>

				<p><strong class="text-primary">Severability</strong><br />
				If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</p>

				<p><strong class="text-primary">Variation of Terms</strong><br />
				Yalla Nsadar is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these Terms on a regular basis.</p>

				<p><strong class="text-primary">Assignment</strong><br />
				The Yalla Nsadar is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>

				<p><strong class="text-primary">Entire Agreement</strong><br />
				These Terms constitute the entire agreement between Yalla Nsadar and you in relation to your use of this Website, and supersede all prior agreements and understandings.</p>

				<p><strong class="text-primary">Governing Law &amp; Jurisdiction</strong><br />
				These Terms will be governed by and interpreted in accordance with the laws of the State of October City, and you submit to the non-exclusive jurisdiction of the state and federal courts located in October City for the resolution of any disputes.</p>


			</div>	
		</div>
	</div>
@endsection