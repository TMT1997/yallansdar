@extends('front.layouts.site')
@section('title') Privacy Policy | @endsection
@section('content')
<style type="text/css">
	.page_head {position: absolute; bottom: 0px; padding:20px 0; background: rgba(0,0,0,0.3); color: #fff; text-align: center; width: 100%; margin:0 auto; font-size: 35px;}
</style>
	<div id="page-banner" class="page-banner" style="position: relative;">
		{!!HTML::image('frontend/images/banner122.png')!!}
		<div class="page-detail"></div>
		<div class="page_head"><strong>Privacy Policy</strong></div>
	</div>	

	<div class="container" style="direction: rtl;">
		<div class="row" style="margin-bottom: 50px; direction: ltr;">
			<div class="col-md-12">

			<p>
				This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
			</p>

			<p><strong>What personal information do we collect from the people that visit our blog, website or app?</strong></p>

			<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, social security number or other details to help you with your experience.</p>

			<p>&nbsp;</p>

			<p><strong>When do we collect information?</strong></p>

			<p>We collect information from you when you register on our site, subscribe to a newsletter, fill out a form, Use Live Chat, Open a Support Ticket or enter information on our site.<br />
			Provide us with feedback on our products or services&nbsp;</p>

			<p>&nbsp;</p>

			<p><strong>How do we use your information?&nbsp;</strong></p>

			<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>

			<ul>
				<li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
				<li>To improve our website in order to better serve you.</li>
				<li>To allow us to better service you in responding to your customer service requests.</li>
				<li>To administer a contest, promotion, survey or other site feature.</li>
				<li>To ask for ratings and reviews of services or products</li>
				<li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
			</ul>

			<p>&nbsp;</p>

			<p><strong>How do we protect your information?</strong></p>

			<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.<br />
			We use regular Malware Scanning.<br />
			We do not use an SSL certificate<br />
			 <li> We do not need an SSL because:</p></li>

			<p>&nbsp;</p>

			<p><strong>Do we use &#39;cookies&#39;?</strong></p>

			<p>Yes. Cookies are small files that a site or its service provider transfers to your computer&#39;s hard drive through your Web browser (if you allow) that enables the site&#39;s or service provider&#39;s systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

			<p><strong>We use cookies to:</strong><br />
			 <li> Help remember and process the items in the shopping cart.<br /></li>
			 <li> Understand and save user&#39;s preferences for future visits.<br /></li>
			 <li> Keep track of advertisements.<br /></li>
			 <li> Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</li>
			 </p>

			<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser&#39;s Help Menu to learn the correct way to modify your cookies.</p>

			<p>If you turn cookies off, some features will be disabled. It won&#39;t affect the user&#39;s experience that make your site experience more efficient and may not function properly.</p>

			<p>However, you will still be able to place orders .</p>

			<p><br />
			<strong>Third-party disclosure</strong></p>

			<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it&#39;s release is appropriate to comply with the law, enforce our site policies, or protect ours or others&#39; rights, property or safety.&nbsp;</p>

			<p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.&nbsp;</p>

			<p>&nbsp;</p>

			<p><strong>Third-party Links</strong></p>

			<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>

			<p>&nbsp;</p>

			<p><strong>Google</strong></p>

			<p>Google&#39;s advertising requirements can be summed up by Google&#39;s Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en&nbsp;<br />
			We use Google AdSense Advertising on our website.</p>

			<p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google&#39;s use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>

			<p><strong>We have implemented the following:</strong></p>

			<ul>
				<li>&nbsp;Remarketing with Google AdSense</li>
				<li>&nbsp;Google Display Network Impression Reporting</li>
				<li>&nbsp;Demographics and Interests Reporting</li>
				<li>&nbsp;DoubleClick Platform Integration</li>
			</ul>

			<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.&nbsp;</p>

			<p>&nbsp;</p>

			<p><strong>Opting out:</strong><br />
			Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>

			<p>&nbsp;</p>

			<p><strong>California Online Privacy Protection Act</strong></p>

			<p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law&#39;s reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</p>

			<p>According to CalOPPA, we agree to the following:<br />
			Users can visit our site anonymously.<br />
			Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.<br />
			Our Privacy Policy link includes the word &#39;Privacy&#39; and can easily be found on the page specified above.</p>

			<p>You will be notified of any Privacy Policy changes:<br />
			 <li> On our Privacy Policy Page<br /></li>
			Can change your personal information:<br />
			 <li> By logging in to your account</p></li>

			<p>How does our site handle Do Not Track signals?<br />
			We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.&nbsp;</p>

			<p>Does our site allow third-party behavioral tracking?<br />
			It&#39;s also important to note that we allow third-party behavioral tracking</p>

			<p><strong>COPPA (Children Online Privacy Protection Act)</strong></p>

			<p>When it comes to the collection of personal information from children under the age of 13 years old, the Children&#39;s Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States&#39; consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children&#39;s privacy and safety online.<br />
			We do not specifically market to children under the age of 13 years old.</p>

			<p>&nbsp;</p>

			<p><strong>Fair Information Practices</strong></p>

			<p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.<br />
			In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:<br />
			We will notify you via email<br />
			 <li> Within 7 business days</p></li>

			<p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>

			<p>&nbsp;</p>

			<p><strong>CAN SPAM Act</strong></p>

			<p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.<br />
			We collect your email address in order to:<br />
			 <li> Send information, respond to inquiries, and/or other requests or questions<br /></li>
			 <li> Process orders and to send information and updates pertaining to orders.<br /></li>
			 <li> Send you additional information related to your product and/or service<br /></li>
			 <li> Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</p></li>

			<p>To be in accordance with CANSPAM, we agree to the following:<br />
			 <li> Not use false or misleading subjects or email addresses.<br /></li>
			 <li> Identify the message as an advertisement in some reasonable way.<br /></li>
			 <li> Include the physical address of our business or site headquarters.<br /></li>
			 <li> Monitor third-party email marketing services for compliance, if one is used.<br /></li>
			 <li> Honor opt-out/unsubscribe requests quickly.<br /></li>
			 <li> Allow users to unsubscribe by using the link at the bottom of each email.</p></li>

			<p>If at any time you would like to unsubscribe from receiving future emails, you can email us at<br />
			 <li> Follow the instructions at the bottom of each email.<br /> and we will promptly remove you from ALL correspondence.</li>
			</p>

			<p><br />
			<strong>Contacting Us</strong></p>

			<p>If there are any questions regarding this privacy policy, you may contact us using the information below.<br />
			yallansadar.com<br />
			El Nakhil Center&nbsp;<br />
			Giza, October 12544&nbsp;<br />
			Egypt<br />
			<a href="mailto:tarek@yallansadar.com">tarek@yallansadar.com</a></p>

			<p>Last Edited on 2017-01-31<br />
			&nbsp;</p>

			</div>	
		</div>
	</div>
@endsection