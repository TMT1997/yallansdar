<?php
	$faq = [
		'ماهي خدمة مطوري الاعمال ؟'=>'
			-	هي خدمة مقدمه من يلا نصدر عن طريق المصريين المقيمين بالخارج , حيث يقوم مطور الاعمال بدراسة السوق والبحث عن أفضل الفرص والمنتجات التي يمكن تصديرها من مصر إلي الدولة التي يقيم فيها , كما يقوم بالتسويق لتلك المنتجات والتواصل مع المستوردين , كما يقوم بالإشراف من جهته علي عملية التعاقد مع المستورد , وأخيراً يقوم بالتأكد من وصول الشحنه مطابقة للمواصفات وقياس مدي رضاء المستورد عن المنتجات , ثم المتابعة مع نفس المستورد لتكرار عملية الاستيراد من مصر .
		',
		'ما الفائدة التي ستعود علي مطور الأعمال ؟'=>'-	يحصل مطور الأعمال علي عمولة مقابل الخدمات التي يقدمها بعد أن تتم عملية التصدير بالجودة المطلوبة',
		'ما مقدار العمولة التي يحصل عليها مطور الأعمال ؟'=>'-	تختلف العمولة من منتج لأخر , سوف يتم تزويد مطور الأعمال بكتالوج به كل المنتجات التي يستطيع الترويج لها والعمولة المقررة عن كل منتج .',
		'كيف يستلم مطور الأعمال العمولة ؟ ومتي ؟'=>'-	يتم تحويل العمولة المقررة علي الحساب البنكي لمطور الأعمال بعد أن تتم عملية التصدير  وفق الشروط والمعايير المنصوص عليها وبعد أن يستلم المُصدر ثمن الشحنه من المستورد يقوم المُصدر بدفع العمولة المقررة لموقع يلا نصدر وفق للعقد المبرم بين المُصدر وإدارة الموقع , ثم تقوم إدارة الموقع بتحويل العمولة لمطور الاعمال .',
		'لماذا استخدم مطور الأعمال المقدمه من يلا نصدر ؟'=>'
			-	نحن نوفر لك :
			<ul>
				<li>
					<strong>مصدرين معتمدين</strong> <br>
					المصدرين الذين سيتم التعامل معهم من خلال خدمة التريد مانجر تم التأكد من هويتهم القانونيه وحجم اعمالهم وقدرتهم علي تصدير المنتجات بالمواصفات المطلوبة
				</li>
				<li>
					<strong>طريقة دفع آمنة</strong> <br>
					سوف يتم دفع عمولتك في حسابك البنكي المدرج لدينا فوراً بعد تنفيذ عملية التصدير واستلام المُصدر أمواله بأمان واستلام المستورد الشحنه مطابقة للمواصفات المنصوص عليها في العقد المبرم بين المصدر والمستورد وتحت إشرافنا
				</li>
				<li>
					<strong>منتجات متعددة</strong> <br>
					لدينا 42 تصنيف يشمل كل المنتجات التي يمكن تصديرها من مصر  إلي جميع دول العالم .
				</li>
				<li>
					<strong>آلية عمل منظمة</strong> <br>
					حتي يتم التأكد من إلتزام المُصدرين  وعدم قيامهم بالإخلال ببنود التعاقد بينهم وبين المستوردين نحن نقوم بإلزام المصدرين بإستخراج شهادات التفتيش علي المنتج في ميناء التحميل من جهه تفتيش دولية ومعتمدة.
				</li>
			</ul>
		',

		'ماهي المنتجات التي يمكن الترويج لها ؟'=>'
			يستطيع مطور الأعمال اختيار منتجات من الكتالوج الإلكتروني الذي سيقوم المسئول بإمداده به والذي يشمل منتجات المصدرين الذين تم التأكد منهم , كما يستطيع مطور الأعمال اقتراح منتجات يري أن لها رواج في السوق المستهدف , وسوف تعمل إدارة يلا نصدر علي توفير هذا المنتج في السوق المحلي .
		',
		'من هم المصدرين الذين سيتم الترويج لمنتجاتهم ؟'=>'
			المصدرين المدرجين علي يلا نصدر والمشتركين في خدمة ( التريد مانجر ) وهي خدمة مقدمة من يلا نصدر يقوم بموجبها الموقع بالتسويق والتفاوض والتعاقد نيابة عن المُصدر مقابل عموله عن كل عملية تصدير .
		',
		'هل يستطيع مطور الأعمال التسويق لأي منتج موجود علي يلا نصدر ؟'=>'
			يقوم مطور الأعمال بالتسويق فقط للمنتجات الخاصة بالمصدرين الذين تم التأكد من هويتهم القانونية وقدرتهم علي التصدير وحجم اعمالهم كما تم التوقيع علي قبول بنود وشروط خدمة يلا نصدر وكذلك عقد التسويق بالعمولة الذي ينص علي نسبة العمولة المقررة وطرق ومواعيد دفع العمولة.
		',
		'كيف يتم التأكد من جودة المنتجات ؟'=>'
			ينص التعاقد بين يلا نصدر والمُصدرين المستفيدين من خدمة التريد مانجر علي أن يقوم المُصدر بدفع تكلفة إستخراج شهادة تفتيش من جهه تفتيش معتمدة , والتي تقوم بالتفتيش علي الشحنه في ميناء التحميل للتأكد من أن الشحنه المُصدرة مطابقة لبنود التعاقد بين المصدر والمستورد .
		',
		'ما هي مهام مطور الاعمال ؟'=>'
			<ul>
				<li>التسويق للمنتجات المحلية</li>
				<li>عقد الاتفاقات مع الشركات والهيئات الاجنبية</li>
				<li>التواصل مع الشركات والهيئات للتأكد من مطابقة احتياجات العملاء مع مواصفات المنتجات المحلية</li>
				<li>الإشراف علي عملية استلام الشحنه والتأكد من مطابقتها للمواصفات</li>
				<li>التأكد من رضاء العملاء عن المنتجات التي تم شحنها</li>
				<li>متابعة العملاء لضمان الاحتفاظ بهم واستمرارية التعامل مع المصدرين المحليين</li>
			</ul>
		',
		'كيف اصبح مطور أعمال ؟'=>'
			<ol>
				<li>اطلب خدمة مطور الأعمال</li>
				<li>تواصل مع يلا نصدر </li>
				<li>اطلع علي كتالوج المنتجات </li>
				<li>اقترح منتج </li>
				<li>سوق للمنتجات التي اخترتها </li>
				<li>تعاقد علي المنتج مع المستورد </li>
				<li>استلم عمولتك بعد أن تتم صفقه التصدير  </li>

			</ol>
		'
	];

	$ifyou = [
		'rgba(233, 102, 86, 0.9)'=>['إذا كنت مصري تعيش في الخارج !!', '<i class="fa fa-globe" aria-hidden="true"></i>'],
		'rgba(52, 210, 147, 0.9)'=>['إذا كان لديك درايه بمتطلبات السوق في البلد التي تعيش فيها !!', '<i class="fa fa-question-circle-o" aria-hidden="true"></i>'],
		'rgba(58, 176, 226, 0.9)'=>['إذا كان لديك شبكة من العلاقات الجيدة أو تستطيع تكوين واحده مع المستوردين !!', '<i class="fa fa-comments-o" aria-hidden="true"></i>'],
		'rgba(231, 192, 43, 0.9)'=>['إذا كنت مهتم باستيراد منتجات من مصر !!', '<i class="fa fa-flag-o" aria-hidden="true"></i>'],
	];

	$methods_work = [
		'blue'=>'اطلب خدمة مطور الأعمال',
		'yellow'=>'تواصل مع يلا نصدر',
		'orange'=>'اطلع علي كتالوج المنتجات ',
		'pink'=>'سوق للمنتجات التي اخترتها بمساعدة المسئول',
		'green'=>'تعاقد علي المنتج مع المستورد',
		'gray'=>'استلم عمولتك بعد أن تتم صفقه التصدير',
	];

	$methods_tips = [
		'blue'=>'اطلب خدمة مطور الأعمال',
		'yellow'=>'تواصل مع يلا نصدر – سوف يقوم مسئول من يلا نصدر بالتواصل معك للتأكد من قدرتك علي تمثيل الشركة في الخارج , كما سيساعدك علي اختيار المنتجات التي ستعمل بها',
		'orange'=>'اطلع علي كتالوج المنتجات حتي تتعرف علي المنتجات الموجودة التي يمكن التسويق لها أو اقترح منتج غير موجود وسوف نسعي إلي التواصل مع المصدرين الذين يعملون في تصدير هذا المنتج',
		'pink'=>'سوق للمنتجات التي اخترتها بمساعدة المسئول',
		'green'=>'تعاقد علي المنتج مع المستورد وفق لبنود العقد الذي سيتم إرساله لك',
		'gray'=>'استلم عمولتك بعد أن تتم صفقه التصدير  بنجاح علي حسابك البنكي الموجود لدينا ',
	];

	$we_provide = [
		'مصدرين معتمدين'=>'
			المصدرين الذين سيتم التعامل معهم من خلال خدمة التريد مانجر تم التأكد من هويتهم القانونيه وحجم اعمالهم وقدرتهم علي تصدير المنتجات بالمواصفات المطلوبة',
		'طريقة دفع آمنة'=>'
			سوف يتم دفع عمولتك في حسابك البنكي المدرج لدينا فوراً بعد تنفيذ عملية التصدير واستلام المُصدر أمواله بأمان واستلام المستورد الشحنه مطابقة للمواصفات المنصوص عليها في العقد المبرم بين المصدر والمستورد وتحت إشرافنا',
		'منتجات متعددة'=>'
			لدينا 42 تصنيف يشمل كل المنتجات التي يمكن تصديرها من مصر  إلي جميع دول العالم .',
		'آلية عمل منظمة'=>'
			حتي يتم التأكد من إلتزام المُصدرين  وعدم قيامهم بالإخلال ببنود التعاقد بينهم وبين المستوردين نحن نقوم بإلزام المصدرين بإستخراج شهادات التفتيش علي المنتج في ميناء التحميل من جهه تفتيش دولية ومعتمدة.',
	];
?>
@extends('front.layouts.site')
@section('title') اعمل معنا - خدمة مطور الأعمال | @endsection
@section('content')
	<div id="page-banner" class="page-banner" style="position: relative;">
		{!!HTML::image('frontend/site/images/work-with-us.jpg')!!}
		<div class="page-detail"></div>
		<div style="position: absolute; bottom: 0px; padding:20px 0; background: rgba(0,0,0,0.3); color: #fff; text-align: center; width: 100%; margin:0 auto; font-size: 35px;">
			<strong>اعمل معنا</strong><br>
			خدمة مطور الأعمال
		</div>
	</div>	

	<div class="container" style="direction: rtl;">
		<div class="row" style="margin-bottom: 50px;">
			<div class="entry-content">
				<span class="quote" style="margin-bottom: 10px">
					نحن نقوم بتكوين شبكة من مطوري الاعمال من المصريين الذين يعيشون في الخارج , وعلي إطلاع بمتطلبات السوق في البلدان التي يعيشون فيها , ولديهم الحس الوطني الكبير والرغبه في خدمة وطنهم من مواقعهم المختلفه وفي الوقت نفسه يستطيعون تحقيق الاستفاده الماديه أثناء وجودهم في بلد الإقامة .
					<br><br>
					تقوم فكرة مطوري الأعمال علي قيام المصريين المقيمين في الخارج بالبحث في الأسواق عن أهم المنتجات التي يحتاجها السوق والمواصفات المطلوبة والاسعار المستهدفه , ونحن نقوم بربط مطور الاعمال مع المصدرين المعتمدين لتوفير هذه المنتجات مع ضمان جودة المنتج وإلتزام المُصدر بمواصفات المنتج ومواعيد التسليم , يقوم مطور الإعمال بالاتفاق مع المستورد  والإشراف علي تنفيذ عملية التصدير مقابل عمولة يتم تحصيلها من المُصدر عن كل عملية تصدير .
				</span>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 50px;">
			@foreach($ifyou as $index=>$item)
				<div class="col-md-3 pull-right">
					<div style="background: {{$index}}; color: #fff; padding:25px 20px 0 20px; min-height: 170px; font-size: 16px; border-radius:10px;" class="text-center">
						<div class="join-ico text-center">{!!$item[1]!!}</div>
						<strong>{!!$item[0]!!}</strong>
					</div>
				</div>
			@endforeach

			<div class="clearfix"></div><br>
			<div class="text-center">
				<a href="#devrequest" class="btn btn-default">اعمل معنا</a>
			</div>
		</div>

		<div class="row" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">طريقة العمل</legend>
			<div class="wrap">
				<div class="process">
					<ul class="process__list">
						<?php $counter=1; ?>
						@foreach($methods_work as $color=>$method)
						<li class="process__item process__item_{{$color}}" data-toggle="tooltip" data-placement="bottom" title="{{$methods_tips[$color]}}">
							<div class="item__inner">
								<p class="item__content">{!!$method!!}</p>
								<span class="step">{{$counter}}</span>
							</div>					
						</li>
						<?php $counter++; ?>
						@endforeach
					</ul>
				</div>
			</div>
		</div>

		<div class="row" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">نحن نوفر لك</legend>
			<ol>
			@foreach($we_provide as $item=>$details)
				<div class="col-md-3 pull-right">
					<div class="text-center offer_box">
						<div class="join-ico checks"><i class="fa fa-check-square-o"></i></div>
						<strong>{!!$item!!}</strong><br>
						<span>
							{!!$details!!}
						</span>
					</div>
				</div>
			@endforeach
			</ol>
		</div>

		<div class="" style="margin-bottom: 50px;">
			<legend class="text-primary text-center">Q & A عن خدمة مطور الاعمال</legend>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php $count = 1; ?>
				@foreach($faq as $q=>$answer)
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading{{$count}}">
				      <h4 class="panel-title">
				        <a class="{{($count!=1)?'collapsed':''}}" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$count}}" aria-expanded="true" aria-controls="collapse{{$count}}">
				          {!!$q!!}
				        </a>
				      </h4>
				    </div>
				    <div id="collapse{{$count}}" class="panel-collapse collapse {{($count==1)?'in':''}}" role="tabpanel" aria-labelledby="heading{{$count}}">
				      <div class="panel-body">
				        {!!$answer!!}
				      </div>
				    </div>
				  </div>
				  <?php $count++; ?>
				@endforeach
			</div>
		</div>

		<div class="well">
			<a name="devrequest"></a>
			<div class="contact-wrap row">
				<div class="col-md-8 col-md-offset-2">
					<legend class="text-primary text-center" style="background:url({{url('frontend/images/join.jpg')}}) no-repeat center bottom; height:250px; color: #fff; position: relative; border-radius:10px;">
						<div style="position: absolute; bottom: 20px; text-align: center; width: 100%; margin: 0 auto; color: #f0f0f0; font-size: 20px; font-weight: bold;">اطلب خدمة مطور الاعمال </div>
					</legend>
		            <div class="status alert alert-success" style="display: none"></div>
		            <form role="form" method="POST" enctype="multipart/form-data" class="contact-form" id="businessDevForm" name="businessDevForm" action="{{url('site/business-dev-request')}}">
		                {{ csrf_field() }}
		                <div class="form-group">
		                    <input class="form-control" name="name" required="" type="text" placeholder="الاسم"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="email" required="" type="email" placeholder="البريد الإلكتروني"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="phone" required="" type="text" placeholder="تليفون"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="jobtitle" required="" type="text" placeholder="المهنة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="company" required="" type="text" placeholder="الشركة"></input>
		                </div>
		                <div class="form-group">
		                    <input class="form-control" name="products" required="" type="text" placeholder="المنتجات التي ترغب في العمل بها"></input>
		                </div>
		                <div class="form-group"><label>حمل صورة جواز السفر </label></div>
		                <div class="form-group">
		                    <input class="form-control" type="file" name="passport"></input>
		                </div>

		                <div class="form-group"><label>طرق الاتصال </label></div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="skype" type="text" placeholder="سكايب"></input>
		                		<span class="input-group-addon"><i class="fa fa-skype"></i></span>
		                	</div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="facebook" type="text" placeholder="فيس بوك"></input>
		                    	<span class="input-group-addon"><i class="fa fa-facebook-square"></i></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
			                    <input class="form-control" name="linkedin" type="text" placeholder="لينكد ان"></input>
			                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
			                   </div>
		                </div>
		                <div class="form-group">
		                	<div class="input-group" style="direction: ltr">
		                    	<input class="form-control" name="twitter" type="text" placeholder="تويتر"></input>
		                    	<span class="input-group-addon"><i class="fa fa-twitter"></i></span>
		                    </div>
		                </div>
		                <div class="form-group text-center">
		                    <button class="btn btn-success" type="submit">إرسال</button>
		                </div>
		            </form>
		            <div class="loading_form hidden">
		                <div class="well text-center"><h3>جاري إرسال الرسالة ...</h3> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
		            </div>
	            </div>
	        </div>
	    </div>
	</div>

	<div id="call-out-section" class="call-out-section ow-background services-call-out hidden">
		<div class="container">
			<div class="call-out-details">
				<h3>اتصل بنا الان</h3>
				<p>يسعدنا تلقي آرائكم ومقترحاتكم واستفساراتكم علي الأرقام التالية</p>	
				<p style="font-size: 25px">0111 192 9530 - 0106 216 6175‏ </p>
			</div>
		</div>
	</div>
@endsection

@section('script')
	{!!HTML::script('dash/packages/validator/jquery.validate.min.js')!!}
	{!!HTML::script('dash/packages/validator/additional-methods.min.js')!!}
	<script type="text/javascript">
	    $("#businessDevForm").validate({
	        rules: {
	            
	        },
	        messages: {
	            name:'ادخل اسمك الشخصي',
	            phone:'ادخل رقم تليفون أو موبايل',
	            email:'ادخل بريدك الإلكتروني بصورة صحيحة',
	            jobtitle:'ادخل المهنة أو المسمى الوظيفي الخاص بك',
	            company:'ادخل اسم الشركة التي تعمل بها',
	            products: 'ادخل المنتجات التي ترغب في العمل بها',
	        },
	        ignore: [] 
	    });
	    $(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
    </script>
@endsection