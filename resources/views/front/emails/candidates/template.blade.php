<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width:70%; color: #333; background:#fff; margin:20px auto; padding:20px; border:4px solid #BB2B6F; border-radius: 10px; font-family: sans-serif, arial;">
		
		أهلا بك, <strong>{{$data['user']->name}}</strong>
		<br><br>

		<h2>{!!$data['subject']!!}</h2>
		<br>

		<p>{!!$data['body']!!}</p>
		<br><br>

		<strong>شكرا وبالتوفيق لكم </strong><br>
		فريق يلا نصدر <br>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;">

			تم إرسال هذه الرسالة إلى البريد الإلكتروني <a href="mailto:{{$data['user']->email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$data['user']->email}}</a>. 
			<br>
			إذا اردت عدم استقبال رسائل أخرى في المثتقبل من الموقع من فضلك اضغط <a href="{{url('registration/unsubscribe?email='.$data['user']->email.'&idn='.$data['indentifier'])}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">إلغاء خدمة الرسائل</a>.

			<br>

			&copy; 2017 - جميع الحقوق محفوظة لدى يلا نصدر
		</div>
	</div>
</div>