<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width:70%; color: #333; background:#fff; margin:20px auto; padding:20px; border:4px solid #BB2B6F; border-radius: 10px; font-family: sans-serif, arial;">
		
		<table class="table table-hover" cellpadding="10" border="1" style="font-size: 16px; font-weight: bold;">
			<tbody>
				<tr>
					<td>البريد اﻹلكتروني</td>
					<td>{{$contact_message->email}}</td>
				</tr>
				<tr>
					<td>الموبايل</td>
					<td>{{$contact_message->mobile}}</td>
				</tr>
				<tr>
					<td>الرسالة</td>
					<td>{!!$contact_message->message!!}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;">

			تم إرسال هذه الرسالة إلى البريد الإلكتروني <a href="mailto:{{@$user->email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{@$user->email}}</a>. 
			<br>
			إذا اردت عدم استقبال رسائل أخرى في المثتقبل من الموقع من فضلك اضغط <a href="{{url('registration/unsubscribe?email='.@$user->email.'&idn='.@$data['indentifier'])}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">إلغاء خدمة الرسائل</a>.

			<br>

			&copy; 2017 - جميع الحقوق محفوظة لدى يلا نصدر
		</div>
	</div>
</div>