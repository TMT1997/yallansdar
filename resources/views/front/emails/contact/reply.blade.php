<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width:70%; color: #333; background:#fff; margin:20px auto; padding:20px; border:4px solid #BB2B6F; border-radius: 10px; font-family: sans-serif, arial;">
		
		<div style="border:1px solid #999; background:#f0f0f0; width:100%; margin:10px auto;">
			<div style="padding:10px;">
				<strong>نص الرسالة الخاصة بك: </strong><br>
				{{strip_tags($data['message_data']->message)}}
			</div>
		</div>
		<hr>

		{!!$data['body']!!}

		<br><br>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;">

			تم إرسال هذه الرسالة إلى البريد الإلكتروني <a href="mailto:{{$data['message_data']->email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$data['message_data']->email}}</a>. 
			<br>
			<br>

			&copy; 2017 - جميع الحقوق محفوظة لدى يلا نصدر
		</div>
	</div>
</div>