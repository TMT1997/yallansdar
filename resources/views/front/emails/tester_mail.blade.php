<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">
	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width:70%; color: #333; background:#fff; margin:20px auto; padding:20px; border:4px solid #F8A605; border-radius: 10px; font-family: sans-serif, arial;">
		
		Dear, <strong>{{$name}}</strong>
		<br><br>

		{!!$body!!}
		<br><br>

		<strong>شكرا  </strong><br>
		فريق يلا نصدر <br>
		<a href="{{url('/')}}">www.yallansadar.com</a>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;">

			تم إرسال هذه الرسالة إلى البريد الإلكتروني <a href="mailto:{{$email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$email}}</a>. 
			<br>
			إذا اردت عدم استقبال رسائل أخرى في المستقبل من الموقع من فضلك اضغط <a href="{{url('registration/unsubscribe?email='.$email)}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">إلغاء خدمة الرسائل</a>.
			<br>

			&copy; {{date('Y')}} - جميع الحقوق محفوظة لدى موقع يلانصدر
		</div>
	</div>
</div>