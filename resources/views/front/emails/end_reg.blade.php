<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width:70%; color: #333; background:#fff; margin:20px auto; padding:20px; border:4px solid #F8A605; border-radius: 10px; font-family: sans-serif, arial;">
		
		@lang('front.welcometo')<strong>{{$user->name}}</strong>
		<br><br>

		@lang('front.yallansadar_thanks')
		<br><br>

		@if(isset($data['password']))
			كلمة المرور الخاصة بحسابك هي: {{$data['password']}} <br>
			<span style="font-style: italic;"></span><br>
		@endif

			@lang('dash.gotodashboard')
		<br>

		{{-- <p style="font-size:14px; color: #0069FF; text-align: center; direction: ltr;">
			<a href="{!!$data['activation_url']!!}" target="_blank">{!!$data['activation_url']!!}</a>
		</p> --}}
		<p style="direction: ltr; text-align: center;">
		<table cellspacing="0" cellpadding="0" style="margin:0 auto; "><tr> 
		  <td align="center" width="300" height="40" bgcolor="#d62828" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block;">
		    <a href="{!!$data['activation_url']!!}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
		    <span style="color: #ffffff;">@lang('dash.dashboard')</span>
		    </a>
		</td></tr></table>
		</p>

		<br><br>

		<strong>@lang('front.thanks')  </strong><br>
		  @lang('front.yallansadar_team') <br>
		<a href="{{url('/')}}">www.yallansadar.com</a>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;">

			@lang('front.send_to')<a href="mailto:{{$user->email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$user->email}}</a>. 
			<br>
			@lang('front.delete_yallansadar_emails')
			 <a href="{{url('registration/unsubscribe?email='.$user->email.'&idn='.$data['indentifier'])}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">إلغاء خدمة الرسائل</a>.

			<br>

			&copy; 2017 - @lang('front.copyrights')
		</div>
	</div>
</div>