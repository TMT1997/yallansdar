<?php
use App\Helper;
App::setLocale('en');
$quote_user = $quote->user;
$quote->subject = str_replace('اقدم لكم عرض سعر بخصوص المنتج', trans('front.mail_provide_quote'), $quote->subject);
?>
<div style="width:100%; background:#e9e9e9; padding:20px 0; direction: ltr;">
    <div style="width:70%; direction: ltr; color: #666; background:#fff; margin:20px auto; padding:20px; border:1px solid #aaa; border-radius: 10px; font-family: sans-serif, arial;">

        @lang('front.dear') {{$data->user_to->name}},
        <br><br>
        @lang('front.rfq_mail_intro')

        This is <strong>Admin From yallansadr</strong>.  <br>

        <br><br>

        <table style="border-collapse: collapse; width: 100%" cellpadding="10" cellspacing="5">
            <tbody>


            <tr style="margin-bottom: 20px; background:#f9f9f9">

                <td class="field_val" style="font-size:16px; color:#333;">
                    @lang('front.product_succ'); <br>
                    <a href="{{url('/dashboard')}}">Yallansadr</a>
                </td>
            </tr>

            </tbody>
        </table>

        <br><br>



        <br>
        <p style="font-size: 15px; color: #333">
            Regards
        </p>
        <br>


    </div>

    <div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
        <div style="margin:0 auto; width:500px;">
            This message was sent to


        </div>
    </div>
</div>