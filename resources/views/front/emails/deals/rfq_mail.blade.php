<?php
	use App\Helper;
	App::setLocale('en');
	$quote_user = $quote->user;
	$quote->subject = str_replace('اقدم لكم عرض سعر بخصوص المنتج', trans('front.mail_provide_quote'), $quote->subject);
?>
<div style="width:100%; background:#e9e9e9; padding:20px 0; direction: ltr;">

	{{-- <div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-en.png')}}" style="border:none; max-width: 100%;" alt="YallaNsadar" /></a>
	</div> --}}

	<div style="width:70%; direction: ltr; color: #666; background:#fff; margin:20px auto; padding:20px; border:1px solid #aaa; border-radius: 10px; font-family: sans-serif, arial;">

		{{-- <h1 style="text-align: center;">@lang('front.quotation')</h1> --}}
		<br>

		@lang('front.dear') {{$deal->user->name}},
		<br><br>
		@lang('front.rfq_mail_intro')

		This is <strong>{{$quote->name}}</strong>.  <br>
		We are a company located in Cairo-Egypt.  <br>
		We are pleased to offer you <strong>{{$quote->subject}}</strong>, 
		please find the offer as follows: 

		<br><br>
		<legend style="font-size: 18px; color:#f89705; margin-bottom:8px; font-weight: bold; ">@lang('front.quotation_information'):</legend>
		<table style="border-collapse: collapse; width: 100%" cellpadding="10" cellspacing="5">
			<tbody>
				<tr style="margin-bottom: 20px; background:#f9f9f9">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f; width:250px;">@lang('front.message')</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						{!!$quote->body!!}
					</td>
				</tr>
				<tr style="margin-bottom: 20px; background:#def2f5">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.quotation_product')</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						{{$deal->service_product_name}}
					</td>
				</tr>
			    <tr style="margin-bottom: 20px; background:#f9f9f9">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('products.capacity')</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
		                {{$quote->production_capacity}} <span class="unit">{{trans('dash.units')[$quote->capacity_unit]}}</span>
		                @lang('products.per') {{trans('dash.capacity_durations')[$quote->capacity_unit_per]}}
			        </td>
			    </td>
			    <tr style="margin-bottom: 20px; background:#def2f5">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.quantity_available')</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
		                {{$quote->min_quantity}} <span class="unit">{{trans('dash.units')[$quote->min_quantity_unit]}}</span>
			        </td>
			    </tr>
			    <tr style="margin-bottom: 20px; background:#f9f9f9">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#d93030;">@lang('products.fob_price')</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
			            {{trans('dash.fob_price_list')[$quote->fob_price_type]}}
			            {{$quote->fob_price}}
			            @lang('products.per') <span class="unit">{{trans('dash.units')[$quote->fob_price_unit]}}</span>
			        </td>
			    </tr>
			    <tr style="margin-bottom: 20px; background:#def2f5">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.accepted_payment_methods')</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
			        	{{$quote->payment_type}}
			        	{{$quote->other_payment_type}}
			        </td>
			    </tr>
			    <tr style="margin-bottom: 20px; background:#f9f9f9">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('products.port')</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
			            {{$quote->port}}
			        </td>
			    </tr>
			    <tr style="margin-bottom: 20px; background:#def2f5">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('products.delivery_time')}}</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
			        	{{$quote->delivery_time}} <span class="unit">{{trans('dash.delivery_duration')[$quote->delivery_duration]}}</span>
			        </td>
			    </tr>
			    <tr style="margin-bottom: 20px; background:#f9f9f9">
			        <td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('products.pack_details')}}</td>
			        <td class="field_val" style="font-size:16px; color:#333;">
			            {{$quote->packaging}}
			        </td>
			    </tr>
				<tr style="margin-bottom: 20px; background:#def2f5">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('front.attachment')}}</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						@if(!empty($quote->attachment))
							<a href="{{url('uploads/'.$quote->attachment)}}" target="_blank">@lang('front.quotation_attach_file')</a>
						@endif
					</td>
				</tr>
				<tr style="margin-bottom: 20px; background:#f9f9f9">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">Go to</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						<a href="{{url('dashboard/quotations')}}">YallaNsadr</a>
					</td>
				</tr>
			</tbody>
		</table>

		<br><br>

		<legend style="font-size: 18px; color:#f89705; margin-bottom:8px; font-weight: bold;">@lang('front.contact_information'):</legend>
		<table style="border-collapse: collapse; width: 100%" cellpadding="10" cellspacing="5">
			<tbody>
				<tr style="margin-bottom: 20px; background:#f9ecd5">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f; width:250px;">@lang('dash.name')</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						{{$quote->name}}
					</td>
				</tr>
				<tr style="margin-bottom: 20px; background:#f9f9f9">
					<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.email')</td>
					<td class="field_val" style="font-size:16px; color:#333;">
						<a href="mailto:{!!$quote->email!!}">{!!$quote->email!!}</a>
					</td>
				</tr>
				@if($quote_user)
					<tr style="margin-bottom: 20px; background:#f9ecd5">
						<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.phone')</td>
						<td class="field_val" style="font-size:16px; color:#333;">
							{!!$quote_user->mobile!!}
						</td>
					</tr>
					<tr style="margin-bottom: 20px; background:#f9f9f9">
						<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.company_name')</td>
						<td class="field_val" style="font-size:16px; color:#333;">
							{!!$quote_user->company_name!!}
						</td>
					</tr>
					@if($quote_user->address)
						<tr style="margin-bottom: 20px; background:#f9ecd5">
							<td class="td_head" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.address')</td>
							<td class="field_val" style="font-size:16px; color:#333;">
								{!!$quote_user->address!!}
							</td>
						</tr>
					@endif
				@endif
			</tbody>
		</table>

		<br>
		<p style="font-size: 15px; color: #333">
			@lang('front.quotation_mail_con_info')
		</p>
		<br>

		{{-- <div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('dash.subject')</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				{{$quote->subject}}
			</div>
		</div>
		<div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.message')</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				{!!$quote->body!!}
			</div>
		</div>
		<legend style="font-size: 18px; color:#f89705; margin-bottom:8px;">@lang('front.quotation_information'):</legend>
		<div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.quotation_product')</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				{{$deal->service_product_name}}
			</div>
		</div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('products.capacity')</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
                {{$quote->production_capacity}} <span class="unit">{{trans('dash.units')[$quote->capacity_unit]}}</span>
                @lang('products.per') {{trans('dash.capacity_durations')[$quote->capacity_unit_per]}}
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.quantity_available')</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
                {{$quote->min_quantity}} <span class="unit">{{trans('dash.units')[$quote->min_quantity_unit]}}</span>
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#d93030;">@lang('products.fob_price')</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
	            {{trans('dash.fob_price_list')[$quote->fob_price_type]}}
	            {{$quote->fob_price}}
	            @lang('products.per') <span class="unit">{{trans('dash.units')[$quote->fob_price_unit]}}</span>
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.accepted_payment_methods')</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
	        	{{$quote->payment_type}}
	        	{{$quote->other_payment_type}}
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('products.port')</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
	            {{$quote->port}}
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('products.delivery_time')}}</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
	        	{{$quote->delivery_time}} <span class="unit">{{trans('dash.delivery_duration')[$quote->delivery_duration]}}</span>
	        </div>
	    </div>
	    <div class="form-group" style="clear:both; margin-bottom: 20px;">
	        <div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('products.pack_details')}}</div>
	        <div class="field_val" style="font-size:16px; color:#333;">
	            {{$quote->packaging}}
	        </div>
	    </div>
		<div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">{{trans('front.attachment')}}</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				@if(!empty($quote->attachment))
					<a href="{{url('uploads/'.$quote->attachment)}}" target="_blank">@lang('front.quotation_attach_file')</a>
				@endif
			</div>
		</div> 
		
		<legend style="font-size: 18px; color:#f89705; margin-bottom:8px;">@lang('front.contact_information'):</legend>
		<div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.name')</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				{{$quote->name}}
			</div>
		</div>
		<div class="form-group" style="clear:both; margin-bottom: 20px;">
			<div class="field_label" style="font-weight:bold; font-size:16px; color:#00509f;">@lang('front.email')</div>
			<div class="field_val" style="font-size:16px; color:#333;">
				{{$quote->email}}
			</div>
		</div>
		--}}

		{{-- @lang('front.thanks')<br>
		@lang('front.yallansadar_team')<br> --}}
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:500px;">
			This message was sent to <a href="mailto:{{$deal->im_email}}" style="text-decoration: none; font-weight: bold; color:#487093">{{$deal->im_email}}</a>. 

			{{-- If you don't want to receive these emails from us in the future, please <a href="{{url('unsubscribe/'.$deal->im_email)}}" style="text-decoration: none; font-weight: bold; color:#487093" target="new">unsubscribe</a>.
			<br>
			2017 &copy; All rights reserved to <a href="http://yallansadar.com/home" target="_blank">YallaNsadar</a>. --}}
		</div>
	</div>
</div>