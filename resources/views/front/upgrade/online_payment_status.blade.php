@extends('front.layouts.site')
@section('title') Payment Status @endsection
@section('content')
	<div class="container text-center">
		<legend>
			@if(Session::has('flash_message'))
				<div class="alert alert-{{Session::get('alert', 'success')}}">
					{{Session::get('flash_message')}}
				</div>
			@endif

			@if($status=='success')
				<div class="alert alert-success">
					Your payment has been accepted successfully, your package has been confirmed.
				</div>
			@else
				<div class="alert alert-danger">
					Sorry, Your payment has been rejected, please try again or contact Yalla Nasadar Support.
				</div>
			@endif

			<br><br>
			<div class="text-center">
				<a href="{{url('home')}}" class="btn btn-success">Back to Homepage</a>
			</div>
		</legend>
	</div>
@endsection