<?php
  use App\Helper;
?>
@extends('front.layouts.site')

<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Lato:400,700);

body {
  background: #F2F2F2;
  padding: 0;
  maring: 0;
}

#price {
  text-align: center;
}

.plan {
  display: inline-block;
  margin: 10px 1%;
  font-family: 'Lato', Arial, sans-serif;
}

.plan-inner {
  background: #fff;
  margin: 0 auto;
  min-width: 280px;
  max-width: 100%;
  position:relative;
}

.entry-title {
  background: #53CFE9;
  height: 140px;
  position: relative;
  text-align: center;
  color: #fff;
  margin-bottom: 30px;
}

.entry-title>h3 {
  background: #20BADA;
  font-size: 20px;
  padding: 5px 0;
  text-transform: uppercase;
  font-weight: 700;
  margin: 0;
}

.entry-title .price {
  position: absolute;
  bottom: -25px;
  background: #20BADA;
  height: 95px;
  width: 95px;
  margin: 0 auto;
  left: 0;
  right: 0;
  overflow: hidden;
  border-radius: 50px;
  border: 5px solid #fff;
  line-height: 80px;
  font-size: 28px;
  font-weight: 700;
}

.price span {
  position: absolute;
  font-size: 15px;
  bottom: -19px;
  left: 30px;
  font-weight: 400;
}

.entry-content {
  color: #323232;
}

.entry-content ul {
  margin: 0;
  padding: 0;
  list-style: none;
  text-align: center;
}

.entry-content li {
  border-bottom: 1px solid #E5E5E5;
  padding: 10px 0;
}

.entry-content li:last-child {
  border: none;
}

.btn {
  padding: 3em 0;
  text-align: center;
}

.btn a {
  background: #323232;
  padding: 10px 30px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 700;
  text-decoration: none
}
.hot {
    position: absolute;
    top: -7px;
    background: #F80;
    color: #fff;
    text-transform: uppercase;
    z-index: 2;
    padding: 2px 5px;
    font-size: 9px;
    border-radius: 2px;
    right: 10px;
    font-weight: 700;
}
.basic .entry-title {
  background: #75DDD9;
}

.basic .entry-title > h3 {
  background: #44CBC6;
}

.basic .price {
  background: #44CBC6;
}

.standard .entry-title {
  background: #4484c1;
}

.standard .entry-title > h3 {
  background: #3772aa;
}

.standard .price {
  background: #3772aa;
}

.ultimite .entry-title > h3 {
  background: #DD4B5E;
}

.ultimite .entry-title {
  background: #F75C70;
}

.ultimite .price {
  background: #DD4B5E;
}
</style>
@section('content')

<div class="headtitle" style="background-color: #4484c1;color: white">
  <div class="container">
    <h1 >@lang('front.packages')</h1>
  </div>
</div>
<div id="price">
  @foreach($packages as $index=>$package)
    @if($index==0)
      <div class="plan ">
    @elseif($index == 1)
      <div class="plan basic ">
    @elseif($index == 2)
      <div class="plan standard">
    @else
      <div class="plan ultimite ">
    @endif
    <div class="plan-inner">
      <div class="entry-title">
        <h3>{{Helper::field($package,'name')}}</h3>
        <div class="price">{{round($package->price)}}<span>{{$package->currency}}</span>
        </div>
      </div>
      <div class="entry-content">
        <ul>
          @foreach($sections as $section)
            <?php
              $items_list = $items->filter(function($row) use ($section){
                return $row->section == $section;
              });
            ?>
            @if($items_list->count()!=0)
              @foreach($items_list as $item)
              <?php $data = unserialize($item->values); ?>
              <li>
                <strong>{{(App::getLocale()=='ar')?$item->name_ar:$item->name_en}}</strong>
                   @if(!empty($data[$package->id]['custom_value']))
                      {{@$data[$package->id]['custom_value']}}
                    @else
                      {{@$data[$package->id]['value']}}
                    @endif
              </li>
              @endforeach
            @endif
          @endforeach
        </ul>

      </div>
      @if($confirmed_user_package && $confirmed_user_package->package_id==$package->id)
        <i class="fa fa-2x fa-check-square text-success"></i>
      @else
        <a href="{{url('upgrade/payment/'.$package->id)}}" class="btn btn-default">@lang('front.package_select')</a>
      @endif
    </div>
  </div>
  @endforeach
</div>
@endsection