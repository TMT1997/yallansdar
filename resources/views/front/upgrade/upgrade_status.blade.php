	@extends('front.layouts.site')

	@section('content')
		<div class="container">
			<legend>
				@if(Session::has('flash_message'))
					<div class="alert alert-{{Session::get('alert', 'success')}}">
						{{Session::get('flash_message')}}
					</div>
				@endif

				<div class="text-center">
					<a href="{{url('home')}}" class="btn btn-success">Back to Homepage</a>
				</div>
			</legend>
		</div>
	@endsection