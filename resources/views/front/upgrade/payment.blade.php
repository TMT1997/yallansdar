<?php
	use App\Helper;
?>
@extends('front.layouts.site')

@section('content')

<div class="container">
	<div class="col-md-12">
		<h3>@lang('front.hi'), {{Auth::user()->name}},</h3>
		<h4>@lang('front.great_lets_go')</h4>

		@if ($errors->any())
	        <ul class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    @endif

		<br>
		{!!Form::open(['class'=>'form-horizontal', 'url'=>'upgrade/payment'])!!}
			{!!Form::hidden('package_id', $package->id)!!}
			{!!Form::hidden('amount', $package->price)!!}
			{!!Form::hidden('currency', $package->currency)!!}
			<table class="table table-bordered table-striped">
				<thead>
					<th>@lang('front.package')</th>
					<th>@lang('front.Duration')</th>
					<th>@lang('front.Billing_Amount')</th>
				</thead>
				<tbody>
					<tr>
						<td><strong>{{Helper::field($package,'name')}}</strong><br> {{$package->brief}}</td>
						<td>{{$package->period}}</td>
						<td nowrap="nowrap">{{round($package->price)}} <sup>{{$package->currency}}</sup> / @lang('front.year')</td>
					</tr>
					<tr class="total_tr">
						<td nowrap="" colspan="2" class="text-right">@lang('front.TOTAL')
							<div class="help-block">@lang('front.inclusive_all_taxes')</div>
						</td>
						<td nowrap="">{{round($package->price)}} <sup>{{$package->currency}}</sup></td>
					</tr>
				</tbody>
			</table>
			<div class="row">
				<div class="col-md-3">
					<div class="well">
						<legend><label>{{Form::radio('payment_method', 'post_transfer', null, ['class'=>' rd_pm' , 'required'])}} @lang('front.post_transfer') </label></legend>
						<strong>@lang('front.name'):</strong> طارق حسني اسماعيل عوض <br>
						<strong>@lang('front.id_no'):</strong> 27801012113417
					</div>
				</div>
				<div class="col-md-3">
					<div class="well">
						<legend><label>{{Form::radio('payment_method', 'bank_transfer', null, ['class'=>' rd_pm'])}} @lang('front.bank_transfer') </label></legend>
						<strong>@lang('front.bank'):</strong> بنك التجاري وفا - فرع سانت فاطيما<br>
						<strong>@lang('front.account_no'):</strong>370001001617546
					</div>
				</div>
				<div class="col-md-3">
					<div class="well">
						<legend><label>{{Form::radio('payment_method', 'cash', null, ['class'=>' rd_pm'])}} @lang('front.cash_at_office') </label></legend>
						<strong>@lang('front.address'):</strong> مدينه 6 أكتوبر - مكتب رقم 1, مبنى هيئة تنمية الصادرات - الحي السادس<br>
					</div>
				</div>
				<div class="col-md-3">
					<div class="well">
						<legend><label>{{Form::radio('payment_method', 'vodafone', null, ['class'=>' rd_pm'])}} @lang('front.vodafone_cash')  </label></legend>
						<strong>@lang('front.phone'):</strong> 01062166175<br>
					</div>
				</div>

				<div class="col-md-12 text-center">
					<span class="or_it">@lang('front.or')</span>

					<div class="pay_online">
						<h3>
							<label>{{Form::radio('payment_method', 'online', null, ['class'=>' rd_pm'])}} @lang('front.pay_online')  </label>
						</h3>
					</div>
				</div>
			</div>

			<div class="alert alert-info" style="margin-top:50px;">
				@lang('front.plz_send_receipt_to'): <strong>yallansadar@gmail.com </strong>
				<br>
				@lang('front.or_to_whatsapp'): <strong>01062166175</strong>
			</div>

			<div class="well">
				<legend class="text-primary">شروط عامة:</legend>
				<ul>
					<li>تدفع قيمة الباقة كاملةً قبل تفعيل الإشتراك بأحد وسائل الدفع المعلن عنها في الموقع </li>
					<li>مدة الإشتراك سنة كاملة تبدأ فور تفعيل الباقة وتنتهي بعد عام كامل ولا يحق للعميل طلب استراد قيمة الباقة خلال هذه الفترة	</li>
					<li>يحصل العميل علي تعويض جزئي أو كلي في حالة عدم حصوله علي الخدمات المنصوص عليها في الباقة التي تم الاشتراك بها , علي أن يتم التعويض بعد انقضاء فترة التعاقد </li>
					<li>تنحصر مسئولية الموقع في توفير فرص تصدير , بدون أي تدخل في عملية التصدير نفسها والتي تقع مسئوليتها كاملة علي المصدر </li>
					<li>الموقع غير مسئول عن بنود الاتفاق بين المصدر والمستورد , وما ينشأ عنها من نزاع يتم تسويته بمعرفة الطرفين وبدون تدخل من إدارة الموقع </li>
					<li>ترسل عروض الاسعار من خلال الفورم البريدي الموجود أسفل كل فرصة إلي المستورد مباشرة بنظام المناقصه , ولن يتم نشرها علانية علي الموقع </li>
				</ul>
			</div>

			<div class="text-center">
				{!!Form::submit(trans('front.pay_now'), ['class'=>'btn btn-success btn-lg'])!!}
			</div>
		{!!Form::close()!!}
		<br><hr>
		<div class="text-center">
			<a href="{{url('upgrade')}}">@lang('front.change_package')</a>
		</div>
		<br><br>
	</div>
</div>
<style type="text/css">
	.total_tr {font-size: 16px; font-weight: bold;}
	.total_tr .help-block {font-weight: normal; font-size: 12px; color: #999}
</style>
@endsection