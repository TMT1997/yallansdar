<?php
	use App\Category;
	use App\Product;
	use App\Helper;
	use App\Deals\SellOffer;
	use App\Deals\QuoteRequest;
	use App\Deals\Partnership;
	use App\Deals\Deal;
if (isset($lang))
    App::setLocale($lang);
	$parent_cats = $categories->pluck('parent');
	$parent_cats = $parent_cats->unique('id');
	$deal_users = $deals->pluck('user');
	$quote_btn = trans('front.request_product');
?>
@extends('front.layouts.master')
@section('title') @lang('front.deals') | @endsection
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-2">
			<legend>@lang('front.deals_categories')</legend>
			<ul class="list-unstyled">
				@foreach($parent_cats as $category)
					@if($category)
						<li><a href="{{($type=='requests') ? QuoteRequest::category_url($category) : Category::url_deals($category)}}"><strong>{{$category->$c_name}}</strong></a>
							<?php
								$p_id = $category->id;
								$childs = $categories->filter(function($row) use($p_id){
									return $row->parent_category_id==$p_id;
								});
							?>
							<ul>
								@foreach($childs as $child)
								<li><a href="{{($type=='requests') ? QuoteRequest::category_url($child) : Category::url_deals($child)}}">{{$child->$c_name}}</a></li>
								@endforeach
							</ul>
						</li>
					@endif
				@endforeach
			</ul>
			<hr>

			<legend>@lang('front.type_of_offers')</legend>
			<ul class="deal_types_filter">
			@foreach(Deal::$types as $key=>$data)
				<li class="{{$data['class']}}" style="margin-bottom: 10px;">
					<a href="{{url($data['url'])}}">
						<label class="abbr">{{$data['abbr']}}</label> <strong>@lang('front.'.$data['title'])</strong>
					</a>
					<?php $sub_sections = trans('dash.offer_types'); ?>
			        @if ($key=='requests')
			            <?php $sub_sections = trans('dash.rfq_request_for'); ?>
			        @elseif ($key=='partnerships')
			            <?php $sub_sections = trans('dash.partnership_types'); ?>
			        @endif

		            @if($type==$key)
			            <ul class="subitems">
			            @foreach($sub_sections as $ref=>$name)
			            	<li><a href="{{url($data['url'])}}?type={{$ref}}">{{$name}}</a></li>
			            @endforeach
			            </ul>
			        @endif
				</li>
			@endforeach
			</ul>
			<hr>

			<?php
				// Get distinct Countries from Deals
				$country_filter = [];
				foreach ($deal_users as $user)
				{
					if(isset($user->profile->country)) {
						if(!in_array($user->profile->country->id, $country_filter)) {
							$country_filter[$user->profile->country->id] = $user->profile->country->name_english;
						}
					}
				}
			?>
			<legend>{{trans('front.locations')}}</legend>
			<ul>
				<li><a href="#">{{trans('front.all_locations')}}</a></li>
				@foreach($country_filter as $id=>$country)
					@if(isset($country))
					<li><a href="#">{{$country}}</a></li>
					@endif
				@endforeach
			</ul>
		</div>
		<div class="col-md-8">
			<div class="row">
				<ol class="breadcrumb">
					<li><a href="{{url('home')}}">@lang('front.home')</a></li>
					<li><a href="{{url('deals')}}">@lang('front.deals')</a></li>
					<li class="active"><strong>{{trans('front.'.Deal::$types[$type]['title'])}}</strong> ({{$deals->count()}} @lang('front.deal'))</li>
				</ol>
			</div>
			<div id="dataGrid">
	           <div class="jplist-panel">
			      <div class="dropdown pull-left jplist-items-per-page"
			         data-control-type="boot-items-per-page-dropdown"
			         data-control-name="paging" data-control-action="paging">
			         <button  style="margin-top: 0px;"
			            class="btn btn-sm btn-primary dropdown-toggle"
			            type="button" data-toggle="dropdown" id="dropdown-menu-1" aria-expanded="true">
			            <span data-type="selected-text">{{trans('front.items_per_page')}}</span>
			            <span class="caret"></span>
			         </button>
			         <ul class="dropdown-menu" role="menu" aria-labelledby="dropdown-menu-1">
			            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="3">3 @lang('front.per_page')</a></li>
			            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="5" data-default="true">5 @lang('front.per_page')</a></li>
			            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="10">10 @lang('front.per_page')</a></li>
			            <li role="presentation" class="divider"></li>
			            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="all">@lang('front.view_all')</a></li>
			         </ul>
			      </div>
			      <ul style="margin-top: 0px;"
			          class="pagination pagination-sm pull-left jplist-pagination"
			          data-control-type="boot-pagination" data-control-name="paging" data-control-action="paging"
			          data-range="4" data-mode="google-like">
			      </ul>
	           </div>

	           <div class="list">
	              @foreach($deals as $item)
		            <div class="list-item deals_grid">
		              	<div class="row">
		                    <div class="col-md-5 pull-left deal_info">
		                    	@if($type=='offers')
			                    	<span class="label label-warning deal_appr">SO</span>
					                <a href="{{SellOffer::url($item)}}?lang={{$lang}}" class="deal_head">{{($item->product)?$item->product->name:''}}</a><br>
					                <ul class="options">
					                	<li><span>@lang('front.deal_type'):</span> {{trans('dash.offer_types')[$item->type]}}</li>
										<li><span>@lang('front.min_ord_quantity'):</span> {{$item->min_quantity}} {{trans('dash.units')[$item->min_quantity_unit]}}</li>
										<li><span>@lang('front.discount'):</span> {{$item->discount}}</li>
										<li><span>@lang('front.category'):</span>
											@if(isset($item->category))
												<a href="{{Category::url($item->category)}}">{{$item->category->$c_name}}</a>
											@endif
										</li>
					                </ul>
					                
					                @elseif($type=='requests')
					            	<?php $quote_btn=trans('front.quote_now'); ?>
					            	<span class="label label-info deal_appr">RFQ</span>
					                <a href="{{QuoteRequest::url($item)}}?lang={{$lang}}" class="deal_head">{{Helper::field($item,'name')}}</a><br>
					                <ul class="options">
					                	<li><span>@lang('front.deal_type'):</span> {{trans('dash.rfq_request_for')[$item->request_for]}}</li>
					                	@if(isset(trans('dash.units')[$item->unit]))
										<li><span>@lang('front.quantity_required'):</span> {{$item->order_quantity}} {{trans('dash.units')[$item->unit]}}</li>
										@endif
										<li><span>@lang('front.category'):</span> <a href="{{Category::url($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></li>
										<li><span>@lang('front.quotes_left'):</span> {{$item->quotes->count()}}</li>
					                </ul>
					               
								@elseif($type=='partnerships')
									<?php $quote_btn=trans('front.contact_now'); ?>
									<span class="label label-info deal_appr">PD</span>
					                <a href="{{Partnership::url($item)}}" class="deal_head">{{$item->service_product_name}}</a><br>
					                <ul class="options">
					                	<li><span>@lang('front.deal_type')}}:</span> {{trans('dash.partnership_types')[$item->type]}}</li>
										<li><span>@lang('dash.part_offer_as'):</span> {{trans('dash.partnership_offer_as')[$item->offer_as]}}</li>
										<li><span>@lang('dash.part_request'):</span> @lang('front.'.$item->request_for)</li>
										<li><span>@lang('front.category'):</span> <a href="{{Category::url($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></li>
					                </ul>
								@endif
			            	</div>
			            	<div class="col-md-7 pull-right deal_contact">
		            			<div class="row">
		            				<div class="col-md-6">
		            					<div class="deal_img_wrap">
			            					{{Helper::image_single($item->images)}}
			            				</div>
		            				</div>
		            				<div class="col-md-6">
		            					<ul>
		            						<li>
		            							@if(isset($item->im_country) && !empty($item->im_country))
													{!!App\Country::flag_by_id($item->im_country,[],true,24)!!}
												@else
													<i class="flag">{!!Deal::flag($item,16)!!}</i> <span>Egypt</span>
												@endif
		            						</li>
		            						<li><i class="fa fa-hourglass-end"></i><span>{{Deal::expire_format($item)}}</span></li>
		            						<li><i class="fa fa-calendar" title="@lang('front.added_on')"></i><span>{{Deal::date_on($item)}}</span></li>
		            					</ul>
		            					<a href="{{url('deals/enquiry/'.$item->id.'/'.$type)}}" class="grn-btn btn-sm">{{$quote_btn}}</a>
		            					<div class="help-block hidden">(Only for Premium Supplier)</div>
		            				</div>
		            			</div>
			            	</div>
			            </div>
		            </div>
	              @endforeach
	           </div>

	   			<div class="jplist-panel">
			      <ul style="margin-top: 0px;"
			          class="pagination pagination-sm pull-left jplist-pagination"
			          data-control-type="boot-pagination" data-control-name="paging" data-control-action="paging"
			          data-range="4" data-mode="google-like">
			      </ul>
	            </div>
	            <!-- no results found -->
	            <div class="jplist-no-results">
	              <p>{{trans('front.no_deals_found')}}</p>
	            </div>
		    </div>
		</div>
		<div class="col-md-2">
			<div class="well well-sm">
				<legend>@lang('front.new_deals')</legend>
				<div class="row">
				@foreach($deals->take(8) as $item)
					<div class="col-md-12">
						@if($type=='offers')
							<a href="{{SellOffer::url($item)}}"> {{SellOffer::prod_name($item)}}</a>
						@elseif($type=='requests')
							<a href="{{QuoteRequest::url($item)}}"> {{Helper::field($item,'name')}}</a>
						@elseif($type=='partnerships')
							<a href="{{Partnership::url($item)}}"> {{$item->service_product_name}}</a>
						@endif
						<hr />
					</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	#dataGrid .list-item {clear:both; border-bottom: 2px solid #e0e0e0; padding:20px 0;}
	#dataGrid .list-item img {max-width: 100%;}
</style>
@endsection
@section('script')
	{!!HTML::style('frontend/js/jplist/jplist.core.min.css')!!}
	{!!HTML::script('frontend/js/jplist/jplist.core.min.js')!!}
	{!!HTML::script('frontend/js/jplist/sorting/jplist.sort-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/pagination/jplist.pagination-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/bootstrap/jplist.bootstrap-pagination-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/filters/jplist.textbox-filter.min.js')!!}
	{!!HTML::style('frontend/js/jplist/filters/jplist.textbox-filter.min.css')!!}
	<script type="text/javascript">
	    $(document).ready(function(){
	       $('#dataGrid').jplist({
	          itemsBox: '.list',
	          itemPath: '.list-item',
	          panelPath: '.jplist-panel'
	       });
	    });
    </script>
@endsection