<?php
	use App\Helper;
	use App\User;
?>
@extends('front.layouts.master')
@section('title') {{trans('front.quote_now')}} | @endsection
@section('content')
<div class="container">
	<legend>{{trans('front.quote_now')}}</legend>

	<div class="alert alert-info">
	<strong>@lang('front.general_terms'): </strong>
	<ul>
		@lang('front.rfq_general_terms')
	</ul>
</div>
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="rfq_form" >
	@if($locker)
		<div class="text-center">
			<a href="#" class="btn btn-warning btn_unlocking" data-id="{{$deal->id}}">
				<h4><i class="fa fa-2x fa-unlock"></i> @lang('front.unlock_quote_form')</h4>
				<small>@lang('front.unlock_send_quote')</small>
			</a>
		</div>
	@else
		@include('front.deals.enquiry.rfq_form');
	@endif
	</div>
	<div class="alert alert-info" style=" margin-top: 30px; padding:20px;">
		<div class="container">
			{{trans('front.cont_supplier_footer')}}
		</div>
	</div>
</div>


@endsection

@section('script')
	@include('dashboard.layouts.popup')

	<script type="text/javascript">
		$(document).on('click', '.btn_unlocking', function(event) {
			var $form = $('.rfq_form');
			var $id = $('.btn_unlocking').data('id');
			event.preventDefault();
			$.ajax({
				url: '{{url('deals/rfq-form/41')}}',
				type: 'POST',
				dataType: 'html',
				data: {id: $id, _token: '{{csrf_token()}}'},
				beforeSend: function() {
					$form.html('@lang('front.loading')');
				}
			})
			.done(function(response) {
				$form.html(response);
			})
			.fail(function(error) {
				$form.html('<h1 class="alert alert-danger">Complete your profile first</h1>');
			})
			.always(function() {

			});
		});
	</script>
@endsection