@extends('front.layouts.contact')

@section('content')
	<a href="{{url('home')}}" class="btn btn-primary">{{trans('front.back_to_home')}}</a>
	<a href="{{url('فرص-تصدير')}}" class="btn btn-primary">{{trans('front.back_to_quotes')}}</a>

@endsection