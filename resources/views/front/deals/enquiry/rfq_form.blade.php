<style>
	.form-group.required .control-label:after {
		content:"*";
		color:red;
	}
</style>

<?php
	use App\User;
	use App\Helper;

	if(!Auth::user()->admin) {
		$item_total = $item_info['value'];
		if(!empty($item_info['custom_value'])) {
			$item_total = $item_info['custom_value'];
		}
		$user_total = $user_usage->count();

	}

?>

@if(!Auth::user()->admin)
	<h3>@lang('front.yourpackage'): {{$user_package->package->name_ar}}</h3>
	<div class="alert alert-info">
		@lang('front.you_unlocked') {{$user_total}} @lang('front.of') {{$item_total}} @lang('front.unlocks')
	</div>
	<div class="progress">

		<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{$user_total}}"
			 aria-valuemin="0" aria-valuemax="{{$item_total}}" style="width:{{ ($user_total/$item_total)*100 }}%">
			{{$user_total}}/{{$item_total}}
		</div>
	</div>
@endif

@if(!Auth::user()->admin && $item_total!='Unlimited' && $user_total>=$item_total)
	<div class="alert alert-danger">
		@lang('front.unlock_alert')	<br>
		<a href="{{url('upgrade?ref=unlock_try')}}" class="btn btn-warning">@lang('front.upgrade_now')</a>
	</div>
@else
	{{--notify that he must write in english--}}
	<div class="alert alert-warning" style="font-size: 20px">

		@lang('front.en_data_notify')
	</div>
	{!!Form::open(['url'=>'deals/rfq-quote','method'=>'POST','class'=>'form-horizontal','files'=>true])!!}
		{!!Form::hidden('uid', $deal->user_id)!!}
		{!!Form::hidden('quote_request_id', $deal->id)!!}
		@if(Auth::check())
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				{!!Form::label('name', trans('front.from'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-3">
					<a href="{{User::url_contact(Auth::user())}}" class="popup">
						{{Auth::user()->name}}
						<br>{{Auth::user()->profile->company_name}}
					</a>
				</div>
			</div>
		@else
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				{!!Form::label('name', trans('front.name'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_name')])!!}
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('email')?'has-error':''}}">
				{!!Form::label('email', trans('front.email'), ['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_email')])!!}
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		@endif
		<div class="form-group">
			{!!Form::label('to', trans('dash.to'), ['class'=>'col-md-3 control-label'])!!}
			<div class="col-md-9">
				@if($deal->im_name)
					<div class="im_name">
						<label>
							{{$deal->im_name}}
							@if($deal->im_country)
								({!!App\Country::flag_by_id($deal->im_country,[],true,16)!!})
							@endif
						</label>
					</div>
				@else
					<a href="{{User::url_contact($deal->user)}}" class="popup">
						{{$deal->user->name}}<br>
						<div class="help-block">{{$deal->user->profile->company_name}}</div>
					</a>
				@endif
				<br>

				<div>
					{{trans('front.interested_in')}}
					<?php
						$image = Helper::image_single($deal->images, ['class'=>'rounded-circle']);
						$title = ($type=='requests')?$deal->name_ar:$deal->service_product_name;
					?>
					<div class="container">
						<div ><h4>{{$title}}</h4></div>
						<a  href="#">{!!$image!!}</a>

					</div>
				</div>
			</div>
		</div>
		<div class="form-group {{$errors->has('subject')?'has-error':''}}">
			{!!Form::label('subject', trans('dash.subject'), ['class'=>'col-md-3 control-label'])!!}
			<div class="col-md-9 ">
				{!!Form::text('subject', trans('front.we_provide_quote').': '.$title,['class'=>'form-control','placeholder'=>'Your Subject Here','readonly'=>true])!!}
				{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
			</div>
		</div>

	    <div class="form-group required {{ $errors->has('production_capacity') ? 'has-error' : ''}}">
	        {!! Form::label('production_capacity', trans('products.capacity'), ['class' => 'col-md-3 control-label required']) !!}
	        <div class="col-md-9">
	            <div class="row">
	                <div class="col-md-3">
	                    {!! Form::number('production_capacity', null, ['class'=>'form-control','required'=>'required']) !!}
	                </div>
	                <div class="col-md-3 pay_type_pos">
	                    {!!Form::select('capacity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required'])!!}
	                </div>
	                <div class="col-md-4 form-inline">
	                    <label class="control-label">{{trans('products.per')}} {!! Form::select('capacity_unit_per',trans('dash.capacity_durations'),null,['class'=>'form-control','required'=>'required'])!!}
	                    </label>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="form-group required {{ $errors->has('min_quantity') ? 'has-error' : ''}}">
	        {!! Form::label('min_quantity', trans('front.quantity_available'), ['class' => 'col-md-3 control-label required']) !!}
	        <div class="col-md-9">
	            <div class="row">
	                <div class="col-md-3">
	                    {!! Form::number('min_quantity', null, ['class' => 'form-control','required'=>'required']) !!}
	                </div>
	                <div class="col-md-3">
	                    {!! Form::select('min_quantity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="form-group required {{ $errors->has('fob_price') ? 'has-error' : ''}}">
	        {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label']) !!}
	        <div class="col-md-9">
	            <div class="row">
	                <div class="col-md-3">
	                    {!! Form::select('fob_price_type', trans('dash.fob_price_list'), null,['class'=>'form-control','required'=>'required']) !!}
	                </div>
	                <div class="col-md-3">
	                    {!! Form::number('fob_price',null,['class'=>'form-control','placeholder'=>trans('dash.price'),'required'=>'required']) !!}
	                </div>
	                <div class="col-md-4 form-inline ">
	                    <label>{{trans('products.per')}} {!!Form::select('fob_price_unit',trans('dash.units'), null,['class' => 'form-control'])!!}
	                    </label>
	                </div>
	            </div>
	            <div class="help-block">{{trans('products.type_help')}}</div>
	        </div>
	    </div>
	    <div class="form-group required {{ $errors->has('fob_price') ? 'has-error' : ''}}">
	        {!! Form::label('fob_price', trans('products.payment_type'), ['class' => 'col-md-3 control-label pay_type']) !!}
	        <div class="col-md-9">
	            <div class="row">
	                @foreach(App\ProductDetails::$payment_methods as $item=>$label)
	                <div class="col-md-3">
	                    <label title="{{$item}}">{!!Form::checkbox('payment_type[]', $item, null, ['class'=>'pay_types'])!!} {{trans('dash.'.$label)}}</label>
	                    @if($label=='Other')
	                        <span id="other_payment_type">
	                            {!!Form::label('other_payment',trans('dash.enter_your_payment_type'),['class'=>''])!!}
	                            {!!Form::text('other_payment_type',null,['class'=>'form-control other_paytype'])!!}
	                        </span>
	                    @endif
	                </div>
	                @endforeach

	            </div>
	        </div>
	    </div>
	    <div class="form-group required {{ $errors->has('port') ? 'has-error' : ''}}">
	        {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label']) !!}
	        <div class="col-md-8">
	            {!! Form::select('port',trans('dash.shipping_ports'),null,['class'=>'form-control','required'=>'required']) !!}
	            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
	        </div>
	    </div>
	    <div class="form-group required {{ $errors->has('delivery_time') ? 'has-error' : ''}}">
	        {!! Form::label('delivery_time', trans('products.delivery_time'), ['class' => 'col-md-3 control-label']) !!}
	        <div class="col-md-9">
	            <div class="row">
	                <div class="col-md-3">{!! Form::number('delivery_time', null, ['class' => 'form-control']) !!}</div>
	                <div class="col-md-3">
	                    {!!Form::select('delivery_duration',trans('dash.delivery_duration'),null,['class'=>'form-control']) !!}
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="form-group brands required {{$errors->has('packaging')?'has-error':''}}">
	        {!!Form::label('packaging',trans('products.pack_details'), ['class'=>'col-md-3 control-label']) !!}
	        <div class="col-md-9">
	            {!!Form::textarea('packaging',null,['class'=>'form-control','rows'=>4,'placeholder'=>trans('products.pack_help')])!!}
	            {!! $errors->first('packaging', '<p class="help-block">:message</p>') !!}
	        </div>
	    </div>
		<div class="form-group required {{$errors->has('body')?'has-error':''}}">
			{!!Form::label('body', trans('front.message'), ['class'=>'col-md-3 control-label'])!!}
			<div class="col-md-9">
				{!!Form::textarea('body', null,['class'=>'form-control','placeholder'=>trans('front.enter_quote_message'), 'required'=>'required'])!!}
				{!! $errors->first('body', '<p class="help-block">:message</p>') !!}
			</div>
		</div>
		<div class="form-group fileAttach {{$errors->has('attachment')?'has-error':''}}">
			{!!Form::label('attachment', trans('front.attachment'), ['class'=>'col-md-3 control-label'])!!}
			<div class="col-md-9">
				{!!Form::file('attachment', ['class'=>'form-control file_upload'])!!}
				{!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
				<div class="help-block fileAttach">{{trans('dash.imgdoc')}}</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				{!!Form::submit(trans('front.send_quote'), ['class'=>'btn btn-lg btn-success btn_loading'])!!}
				<div class="help-block">{{trans('front.send_enq_recp')}}</div>
			</div>
		</div>
	{!!Form::close()!!}
@endif

<script type="text/javascript">
	$(document).ready(function() {
//	  $('.popup').magnificPopup({type:'ajax'});
        function onSubmit()
        {
            var fields = $("input[name='payment_type[]']").serializeArray();
            if (fields.length === 0)
            {
                $('.pay_type').css("color","red");
                $('html, body').animate({
                    scrollTop: ($('.pay_type_pos').offset().top)
                },500);

                // cancel submit
                return false;
            }
            else
            {

            }
        }

		// register event on form, not submit button
        $('.form-horizontal').submit(onSubmit);
		//validate file extension
        (function($) {
            $.fn.checkFileType = function(options) {
                var defaults = {
                    allowedExtensions: [],
                    success: function() {},
                    error: function() {}
                };
                options = $.extend(defaults, options);

                return this.each(function() {

                    $(this).on('change', function() {
                        var value = $(this).val(),
                            file = value.toLowerCase(),
                            extension = file.substring(file.lastIndexOf('.') + 1);

                        if ($.inArray(extension, options.allowedExtensions) == -1) {
                            options.error();
                            $(this).focus();
                        } else {
                            options.success();

                        }

                    });

                });
            };

        })(jQuery);

        $(function() {
            $('.file_upload').checkFileType({
                allowedExtensions: ['jpg', 'jpeg','png','pdf','doc','docx','pptx'],
                success: function() {

                },
                error: function() {

                    $('.file_upload').replaceWith($('.file_upload').val('').clone(true));
                    $('.file_upload').css('color','red');
                    $('.fileAttach').css('color','red');
                }
            });

        });
	});
</script>