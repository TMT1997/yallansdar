<?php
	use App\Helper;
	use App\User;
?>
@extends('front.layouts.master')

@section('content')
<div class="container">
	{!!Form::open(['class'=>'form-horizontal', 'files'=>true])!!}
		{!!Form::hidden('uid', $deal->user_id)!!}
		@if(Auth::check())
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				{!!Form::label('name', trans('front.from'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-3">
					<a href="{{User::url_contact(Auth::user())}}" class="popup">
						{{Auth::user()->name}}
						<br>{{Auth::user()->profile->company_name}}
					</a>
				</div>
			</div>
		@else
			<div class="form-group {{$errors->has('name')?'has-error':''}}">
				{!!Form::label('name', trans('front.name'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_name')])!!}
					{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
			<div class="form-group {{$errors->has('email')?'has-error':''}}">
				{!!Form::label('email', trans('front.email'), ['class'=>'col-md-2 control-label'])!!}
				<div class="col-md-9">
					{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_email')])!!}
					{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
		@endif
		<div class="form-group">
			{!!Form::label('to', trans('dash.to'), ['class'=>'col-md-2 control-label'])!!}
			<div class="col-md-9">
				<a href="{{User::url_contact($deal->user)}}" class="popup">
					{{$deal->user->name}}<br>
					<div class="help-block">{{$deal->user->profile->company_name}}</div>
				</a>
				<br>
				<div class="well well-sm">
					{{trans('front.interested_in')}} <br>
					<?php
						$image = Helper::image_single($deal->images, ['class'=>'media-object']);
						$title = ($type=='offers')?$deal->product->name:$deal->service_product_name;
					?>
					<div class="media">
						<a class="pull-left" href="#">{!!$image!!}</a>
						<div class="media-body"><h4 class="media-heading">{{$title}}</h4></div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group {{$errors->has('subject')?'has-error':''}}">
			{!!Form::label('subject', trans('dash.subject'), ['class'=>'col-md-2 control-label'])!!}
			<div class="col-md-9">
				{!!Form::text('subject', trans('front.plz_provide_quotes').':'.$title,['class'=>'form-control','placeholder'=>'Your Subject Here'])!!}
				{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
			</div>
		</div>
		<div class="form-group {{$errors->has('body')?'has-error':''}}">
			{!!Form::label('body', trans('front.message'), ['class'=>'col-md-2 control-label'])!!}
			<div class="col-md-9">
				{!!Form::textarea('body', null,['class'=>'form-control','placeholder'=>''])!!}
				{!! $errors->first('body', '<p class="help-block">:message</p>') !!}
			</div>
		</div>
		<div class="form-group {{$errors->has('attachment')?'has-error':''}}">
			{!!Form::label('attachment', trans('front.attachment'), ['class'=>'col-md-2 control-label'])!!}
			<div class="col-md-9">
				{!!Form::file('attachment', ['class'=>'form-control'])!!}
				{!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
				<div class="help-block">
				{{trans('dash.imgdoc')}}
				</div>
			</div>
		</div>
		@if(Auth::guest())
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<label>{!!Form::checkbox('createAccountOnPost',1,true)!!} {{trans('front.create_account_also')}}</label>
			</div>
		</div>
		@endif
		<div class="form-group">
			<div class="col-md-offset-2 col-md-9">
				{!!Form::submit(trans('front.send_enquiry'), ['class'=>'btn btn-lg btn-success btn_loading'])!!}
				<div class="help-block">{{trans('front.send_enq_recp')}}</div>
			</div>
		</div>
	{!!Form::close()!!}
</div>

<div class="row" style="background: #f0f0f0; margin-top: 30px; padding:30px;">
	{{trans('front.cont_supplier_footer')}}
</div>

@endsection

@section('script')
	@include('dashboard.layouts.popup')
@endsection