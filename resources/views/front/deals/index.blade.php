
<?php
	use App\Category;
	use App\Helper;
	use App\Deals\Deal;
	use App\Deals\SellOffer;
	use App\Deals\QuoteRequest;
	use App\Deals\Partnership;
?>
@extends('front.layouts.master')
@section('title') @lang('front.deals') | @endsection
@section('content')
<div class="container">
	<div class="col-md-2 cats_nav">
		<nav class="sdMnu innr-sd-mnu">
            <header><h2><a href="{{url('category')}}">@lang('front.deals') @lang('front.categories')</a></h2></header>
            <ul class="sid-mnu subcats">
            @foreach($categories as $category)
                <li class="lvl-one-mnu-itm" style="position: relative;">
                    <a href="{{Category::url_deals($category)}}" class="{{($category->childs->count()>0)?'parent':''}}">{{$category->$c_name}}</a>
                    <ul class="sub-lvl-two-itm" style="min-width:265px;height:inherit;position:absolute;top:-1px;width:auto;">
                        <li style="position:relative; z-index: 99999;"><h4>{{$category->$c_name}}</h4></li>
                        <li class="sub-sub-itms">
                            <ul>
                            	@foreach($category->childs->take(20) as $sub_cat)
                                <li style="width:100%"><a href="{{Category::url_deals($sub_cat)}}">{{$sub_cat->$c_name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>
            @endforeach
            </ul>
        </nav>
	</div>

	<div class="col-md-8">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{url('home')}}">@lang('front.home')</a></li>
				<li><a href="{{url('deals')}}">@lang('front.deals')</a></li>
			</ol>
		</div>

		<section class="">
			<header class="hdr-styl-02"><h4>@lang('front.latest_deals')</h4></header>
		    <div class="row deals-row">
		        <article class="col-xs-12 col-md-12">
		            <header class="hdr-styl-03">
		            	<a href="#" class="btn btn-warning btn-sm pull-right post_deal">@lang('front.post_sell_offer')</a>
		                <a href="{{Deal::$types['offers']['url']}}" class="tag pull-left">SO</a>
		                <h4><a href="{{Deal::$types['offers']['url']}}">@lang('front.sell_offers') ({{$deals['sell_offers']->count()}})</a></h4>
		                <p>@lang('front.sell_offer_desc')</p>
		            </header>
		            <div class="row">
			            @foreach($deals['sell_offers']->take(6) as $item)
			            	<div class="col-md-4">
					            <figure>
					                <a href="{{SellOffer::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">{!!Deal::flag($item)!!}</span>
					                    <h4><a href="{{SellOffer::url($item)}}"> {{SellOffer::prod_name($item)}}</a></h4>
					                    <p class="p-1">@lang('front.moq'): {{$item->min_quantity}} {{trans('dash.units')[$item->min_quantity_unit]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>@lang('front.category'):</span> <a href="{{Category::url_deals($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
					        </div>
			            @endforeach
			        </div>
		            <a href="{{Deal::$types['offers']['url']}}" class="vw-mr-btn">@lang('front.view_more')</a>
		        </article>

		        <div class="row"><div class="col-md-12"><hr class="bold"></div></div>

		        <article class="col-xs-12 col-md-12">
		            <header class="hdr-styl-04">
		                <a href="#d" class="btn btn-info btn-sm pull-right post_deal"> @lang('front.post_buying_req')</a>
		                <a href="{{Deal::$types['requests']['url']}}" class="tag pull-left">RFQ</a>
		                <h4><a href="{{Deal::$types['requests']['url']}}">{{trans('front.request_quotes')}} ({{$deals['quote_requests']->count()}})</a></h4>
		                <p>@lang('front.request_for_quote_desc')</p>
		            </header>
		            <div class="row">
			            @foreach($deals['quote_requests']->take(6) as $item)
			            	<div class="col-md-4">
					            <figure>
					            	<a href="{{QuoteRequest::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">
					                    	@if(isset($item->im_country) && !empty($item->im_country))
												{!!App\Country::flag_by_id($item->im_country,[],false,24)!!}
											@else
												{!!Deal::flag($item)!!}
											@endif
					                    </span>
					                    <h4><a href="{{QuoteRequest::url($item)}}">{{Helper::field($item, 'name')}}</a></h4>
					                    <p class="p-1">@lang('front.EOQ'): {{$item->order_quantity}} {{trans('dash.units')[$item->unit]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>@lang('front.category'):</span> <a href="{{QuoteRequest::category_url($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
				            </div>
			            @endforeach
			        </div>
		            <a href="{{Deal::$types['requests']['url']}}" class="vw-mr-btn">@lang('front.view_more')</a>
		        </article>

		        <div class="row"><div class="col-md-12"><hr class="bold"></div></div>

		        <article class="col-xs-12 col-md-12 latest_deals">
		            <header class="hdr-styl-05">
		            	<a href="#d" class="btn btn-success btn-sm pull-right post_deal">@lang('front.post_partnership_deal')</a>
		                <a href="{{Deal::$types['partnerships']['url']}}" class="tag pull-left">PD</a>
		                <h4><a href="{{Deal::$types['partnerships']['url']}}">@lang('front.partnership_deals')}} ({{$deals['partnerships']->count()}})</a></h4>
		                <p>@lang('front.partnership_deals_desc')}}</p>
		            </header>
		            <div class="row">
			            @foreach($deals['partnerships']->take(6) as $item)
				            <div class="col-md-4">
					            <figure>
					                <a href="{{Partnership::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">{!!Deal::flag($item)!!}</span>
					                    <h4><a href="{{Partnership::url($item)}}"> {{$item->service_product_name}}</a></h4>
					                    <p class="p-1">{{trans('dash.partnership_types')[$item->type]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>@lang('front.category'):</span> <a href="{{Category::url_deals($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
				            </div>
			            @endforeach
			        </div>
		            <a href="{{Deal::$types['partnerships']['url']}}" class="vw-mr-btn">@lang('front.view_more')</a>
		        </article>
			</div>
		</section>
	</div>

	<div class="col-md-2">
		<div class="well well-sm">
			<h4>{{trans('front.new_deals')}}</h4>
			<div class="row">
			@foreach($deals as $type=>$items)
				@foreach($items->take(4) as $item)
					<div class="col-md-12">
						@if($type=='sell_offer')
							<a href="{{SellOffer::url($item)}}" class="thumbnail">{{Helper::image_single($item->images)}} {{SellOffer::prod_name($item)}}</a>
						@elseif($type=='partnerships')
							<a href="{{Partnership::url($item)}}" class="thumbnail">{{$item->service_product_name}}</a>
						@else
							<a href="{{QuoteRequest::url($item)}}" class="thumbnail">{{Helper::field($item,'name')}}</a>
						@endif
					</div>
				@endforeach
			@endforeach
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	#dataGrid .list-item {clear:both; border-bottom: 2px solid #e0e0e0; padding:20px 0;}
	#dataGrid .list-item img {max-width: 100%;}
</style>
@endsection

@section('script')

@endsection