<?php
	use App\Category;
	use App\Product;
	use App\Helper;
	use App\Deals\Deal;
	use App\Deals\QuoteRequest;
	use App\PackageUsage;
	use App\User;

	if (isset($lang))
		App::setLocale($lang);
	$parents = ($category_parents) ? array_reverse($category_parents) : [];
	$profile = ($offer->user && isset($offer->user->profile)) ? $offer->user->profile : null;
	//$offer_lang = Helper::lang_data($offer, $lng_id);

	$offer_countries = [];
	if(!empty($offer->supplier_location)) {
		$offer_countries = explode(',', $offer->supplier_location);
	}

	$payment_terms = ($offer->payment_terms) ? explode(',', $offer->payment_terms):'';
?>
@extends('front.layouts.master')
@section('title') @lang('front.request_quotes') > {{Helper::field($offer,'name')}} | @endsection
@section('meta_desc') {!!str_limit(strip_tags(Helper::field($offer,'details')), 400)!!} @endsection
@section('fb_meta')
    <meta property="og:title" content="{{Helper::field($offer,'name')}}">
    <meta property="og:image" content="{!!url('uploads/'.$offer->image)!!}">
    <meta property="og:description" content="{!!str_limit(strip_tags(Helper::field($offer,'details')), 400)!!}">
    <meta property="og:url" content="{{QuoteRequest::url($offer)}}">
@endsection
@section('content')
<div class="breadcumb-list">
	<ul class="container">
	  	<li><a href="{{url('home')}}">@lang('front.home')</a></li>
	  	<li><a href="{{url('deals')}}">@lang('front.deals')</a></li>
	  	@if($parents)
		  	@foreach($parents as $cat)
		  		@if($cat)
					<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a></li>
				@endif
			@endforeach
		@endif
	</ul>
</div>
<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
                <div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>@lang('front.loading_images')</div>
                <div class="sp-wrap">
                	@foreach($offer->images as $item)
                    	<a href="{{url('uploads/'.$item->image)}}">{!!Helper::image2($item->image,[],false)!!}</a>
                    @endforeach
                </div>
			</div>
			<div class="col-xs-12 col-md-4">
				<header>
					<h1 class="hdr-styl-001">{{Helper::field($offer,'name')}}</h1>
				</header>
				<div class="deals-row hdr-styl-04">
		        	<a href="{{url(Deal::$types['requests']['url'])}}" class="tag pull-left">RFQ</a>
					<h4><a href="{{url(Deal::$types['requests']['url'])}}">@lang('front.request_quotes')</a></h4>
		        </div>
				<br>{{trans('dash.rfq_request_for')[$offer->request_for]}}
				<p class="nrml-p">
					{!!str_limit(Helper::field($offer, 'details'), 220)!!}
					{{--{!!Helper::field($offer, 'details')!!}--}}
				</p>
				{{--show more info--}}
				<p style="text-align: center"><a href="#more_info">@lang('front.view_more')</a></p>
				<div class="table-responsive">
					<table class="table inf-tbl">
						<tbody>
							<tr>
								<td style="width:140px">@lang('front.quantity_required'):</td>
								<td>{{$offer->order_quantity}} {{trans('dash.units')[$offer->unit]}}</td>
							</tr>
							<tr>
								<td>@lang('front.payment_terms')}}:</td>
								<td>
									@if($payment_terms && is_array($payment_terms))
										@foreach($payment_terms as $type)
											{{trans('dash.payment_methods_list')[$type]}},
										@endforeach
									@endif
								</td>
							</tr>
							<tr>
								<td>@lang('front.quotes_left'):</td>
								<td>{{$offer->quotes->count()}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				{{--Press on show more will redirect you to the full info down the page--}}

				<div class="shr-prnt-br02 row">
					<div class="col-xs-12 col-md-7 folow-us">
						<span>@lang('front.share_us_on'):</span>
						<div style="display: inline-block;" class="addthis_inline_share_toolbox"></div>
					</div>
					<div class="col-xs-12 col-md-5 prnt-br text-left">
						<a id="print" href="#"><i class="fa fa-print" aria-hidden="true"></i> @lang('front.print')</a>
						<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> @lang('front.send_to')</a>
					</div>
				</div>
			</div>

			<div class="col-md-4" >
				@include('front.deals.company_details', ['rfq'=>true])
				<div class="cnct-inf-sid ad-pd text-center">
					<div class="row">
						{{--{{url('deals/enquiry/'.$offer->id.'/requests')}}--}}
			          	<a href="#" data-id="{{Auth::id()}}" class="btn btn-xs btn-warning btn_locking" style="margin-top:10px " data-toggle="modal" data-target="#myModal">
							<div class="fa fa-2x fa-lock"> @lang('front.unlock_quote_form') </div>
						</a>
						@include('layouts.deals_modal_info')
			          </div>
				</div>
			</div>
			<br class="clr-bth" />

			<div id="tabs02" class="tabs02" style="margin:20px 0">
				<ul class="tabing">
				    <li id="more_info"><a class="tab_nav" href="#tabs02-1">@lang('front.deal_description')</a></li>
					{{-- Delete conatct info--}}
				    {{--<li><a class="tab_nav" href="#tabs02-2">@lang('front.contact_details')</a></li>--}}
				</ul>
				<div id="tabs02-1" class="offer_vars">
					<div class="row">
						<div class="col-md-6">
							{{trans('front.preferred_supplier_location')}}:
							@if(!empty($offer_countries))
								@foreach($offer_countries as $id)
									<?php $country = App\Country::get_country($id); ?>
									@if($country)
										<a href="#" class="btn btn-sm btn-default">{{$country->name}}</a>
									@endif
								@endforeach
							@else
								@lang('front.not_available')
							@endif
						</div>
					</div>
					<hr />

					<legend>@lang('front.offer_description')</legend>
					<div style="direction: initial;">
				    {!!(Helper::field($offer, 'details'))!!}
					</div>
					<div class="rfq_terms">
						@lang('dash.terms_rfq')
					</div>
				    <hr>
				    <div class="text-center">
						<a href="#" data-id="{{Auth::id()}}" class="btn btn-xs btn-warning btn_locking" style="margin-top:10px " data-toggle="modal" data-target="#myModal">
							<div class="fa fa-2x fa-lock"> @lang('front.unlock_quote_form') </div>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>

@endsection

@section('script')
	@include('dashboard.layouts.popup')
	{{--Load the print functionality--}}
	<script type="text/javascript" src="{{ URL::asset('frontend/js/app/print.js') }}"></script>
	<script type="text/javascript">

		$(document).on('click', '.btn_unlock_rfq', function(event) {
			event.preventDefault();
			let $btn = $(this);
			let $wrapper = $(this).siblings('.locked_data');
            let $id = $(this).data('id');
            $.ajax({
				url: '{{url('package-check/unlock-rfq-contact')}}',
				type: 'POST',
				dataType: 'html',
				data: {rfq_id: $id, _token: '{{csrf_token()}}'},
				beforeSend: function() {
					$btn.val('Loading ...');
					$btn.attr('disabled', 'disabled');
				}
			})
			.done(function(response) {
				$wrapper.html(response);
				$btn.slideUp('slow');
			})
			.fail(function(error) {
				$wrapper.html('Error occured');
				$btn.slideDwon('slow');
				$btn.removeAttr('disabled');
			})
			.always(function() {
				$btn.val('@lang('front.unlock_contact_details')');
				$btn.removeAttr('disabled');
			});
		});
        $(document).on('click', '.end', function(event) {
            event.preventDefault();

			$("#deals_list").empty();
        });
		// Jquery code for handling the ajax request to the controller to collect data about Package
        $(document).on('click', '.btn_locking', function(event) {
            event.preventDefault();

            let $id = $(this).data('id'); // current user_id
            $.ajax({
                url: '{{url('package-check/package-info')}}',
                type: 'POST',

                data: {user_id: $id, _token: '{{csrf_token()}}'},
                beforeSend: function() {

                }
            })
                .done(function(response) {
                    //craete the ui with the data

					if(response['not_auth'] === 'true') // not authorized
					{// go login

						window.location.replace('{{url('login')}}');
					}
					else{
                        Create_modal_UI(response);
                        show_modal();
                    }
                })
                .fail(function(error) {

                    create_err_message();
                    show_modal();

                })
                .always(function() {

                });
        });
        function show_modal(){
            var h = ($('.modal').height())/2;
            $('#myModals').css({'top':'40%','margin-top':'-'+h+'px'});
            $('#myModals').modal('show');
		}
		function create_err_message(){
            $('.conti').remove(); // remove the continue btn
		    let list = `<li class="list-group-item" style="font-size:15px ;">
                		<strong>@lang('front.err_mess') </strong><br>
                		<a href="{{url('upgrade')}}">@lang('front.upgrade_now')</a>
                		</li>`;
            $('#deals_list').append(list);
		}
        function Create_modal_UI(data){
		    //cheack if he has left some in his package
			let upgrade_acc=``;

		if(data['finish'] === 'finish'){
		    //remove the continue btn
			$('.conti').remove();
			//make upgrade link
            upgrade_acc=`<a href="{{url('upgrade')}}">@lang('front.upgrade_now')  </a> `
        }
        else{
            upgrade_acc=''
		}
            		let list =`
            			<li class="list-group-item" style="font-size:15px ;">
                		<strong>@lang('front.package') </strong> ${data['package']}
                		</li>
                		<li class="list-group-item" style="font-size:15px ;">
                		<strong>@lang('front.added_on_2') </strong> ${data['added_on_2']}
                		</li>
                		<li class="list-group-item" style="font-size:15px ;">
                		<strong>@lang('front.last_on') </strong> ${data['last_on']}
                		</li>
                		<li class="list-group-item" style="font-size:15px ;"
                		<strong>@lang("front.totalunlock") </strong> ${data['totalunlock']}
                		</li>
                		<li class="list-group-item" style="font-size:15px ;">
                		<strong>@lang('front.userunlock') </strong> ${data['userunlock']} / ${data['totalunlock']}
                		</li>
                		<li class="list-group-item" style="font-size:15px; padding:15px;">
                			`+upgrade_acc+`
                		</li>
                		` ;


				$('#deals_list').append(list);
        }
	</script>
@endsection