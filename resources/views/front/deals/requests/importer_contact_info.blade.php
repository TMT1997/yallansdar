@if(Auth::guest())
	تسجيل الدخول مطلوب
	<a href="{{url('interact/quicklogin')}}" class="popup">اضغط هنا</a>
	@include('dashboard.layouts.popup')
@else
	<div class="im_contact">
		<legend>@lang('front.contact_details')</legend>
		@if($deal)
			<ul class="ul_contact">
				<li title="@lang('front.country')">
					@if($deal->im_name && $deal->im_country)
						{!!App\Country::flag_by_id($deal->im_country,[],true,16)!!}
					@else
						{!!App\User::flag($deal->user,[],true,16)!!}
					@endif
				</li>
				<li title="@lang('front.name')">
					<i class="fa fa-user"></i>
					<strong>{{$deal->im_name}}</strong>
				</li>
				<li title="@lang('front.phone')">
					<i class="fa fa-phone"></i>
					<a href="tel:{{$deal->im_phone}}">{{$deal->im_phone}}</a>
				</li>
				<li title="@lang('front.email')">
					<i class="fa fa-envelope"></i>
					@if($deal->im_email)
						<a href="emailto:{{$deal->im_email}}">{{$deal->im_email}}</a>
					@else
						N/A
					@endif
				</li>
				<li title="@lang('front.website')">
					<i class="fa fa-globe"></i>
					@if($deal->im_website)
						<a href="{{$deal->im_website}}" target="_blank">{{$deal->im_website}}</a>
					@else
						N/A
					@endif
				</li>
			</ul>
		@else
			<div class="alert alert-danger">No data available</div>
		@endif
	</div>
@endif

<style type="text/css">
	.im_contact {background: #00509f; padding:20px 10px; border-radius: 10px; color: #fff}
	.im_contact legend {color: #ffc147; font-size: 17px; border-bottom: 1px solid #4c8bca; padding:0px 20px}
	.im_contact a {color: #fff}
	.im_contact a:hover {color: #f8a504}
	.im_contact i { width: 30px; height: 30px; background: #f8a504; border-radius: 50%; text-align: center; display: inline-block; color: #fff; font-size: 18px; padding-top: 6px;  }
	.im_contact li { padding: 5px 5px; }
</style>