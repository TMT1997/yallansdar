<?php use App\Deals\Deal; ?>
<div class="col-md-12 deal_contact">
	<ul style="font-size: 15px;">
		<li><h4>{{$offer->user->profile->company_name}}</h4></li>
		<li>
			@if(isset($rfq))
				@if($offer->im_country)
					{!!App\Country::flag_by_id($offer->im_country,[],true,16)!!}
				@else
					{!!App\User::flag($offer->user,[],true,16)!!}
				@endif
			@else
				{!!App\User::flag($offer->user,[],true,16)!!}
			@endif
		</li>
		@if(isset($unlocked) && $unlocked)
			<li>
				<i class="fa fa-user"></i>
				<span>
					@if(isset($rfq) && $offer->im_name)
						{{$offer->im_name}}
					@else
						{{$offer->user->profile->first_name}} {{$offer->user->profile->last_name}}
					@endif
				</span>
			</li>
			<li>
				<i class="fa fa-phone"></i>
				<span>
					@if(isset($rfq) && $offer->im_phone)
						{{$offer->im_phone}}
					@else
						{{Deal::get_phone($offer->user)}}
					@endif
				</span>
			</li>
			<li>
				<i class="fa fa-envelope"></i>
				<span>
					@if(isset($rfq) && $offer->im_email)
						<a href="mailto:{{$offer->im_email}}">{{$offer->im_email}}</a>
					@else
						<a href="mailto:{{$offer->im_email}}">{{$offer->user->email}}</a>
					@endif
				</span>
			</li>
		@else

		@endif
		<li title="@lang('front.added_on')"><i class="fa fa-calendar"></i><span>@lang('front.added_on'): {{Deal::date_on($offer)}}</span></li>
		<li title="@lang('front.last_on')"><i  class="fa fa-calendar"></i><span>@lang('front.last_on'): {{Deal::expired_at($offer)}}</span></li>
		<li><i class="fa fa-hourglass-end"></i><span>{{Deal::expire_format($offer)}}</span></li>
	</ul>
</div>