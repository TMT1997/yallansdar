<?php
	use App\Category;
	use App\Product;
	use App\Helper;
	use App\Deals\SellOffer;
	use App\Deals\QuoteRequest;
	use App\Deals\Partnership;
	use App\Deals\Deal;

	foreach($deals as $type=>$deal) {
		$deal_users = $deal->pluck('user');
	}
	$quote_btn = trans('front.request_product');
?>
@extends('front.layouts.master')
@section('title') {{$category->$c_name}} {{trans('front.deals')}} | @endsection
@section('content')

<div class="container">
	<div class="col-md-2">
		<legend>{{trans('front.deals_cats')}}</legend>
		<ul>
			@if(isset($category->parent))
				<li><a href="{{Category::url_deals($category->parent)}}"><strong>{{$category->parent->$c_name}}</strong></a>
					<ul>
						<li><a href="{{Category::url_deals($category)}}">{{$category->$c_name}}</a></li>
					</ul>
				</li>
			@else
				<li><a href="{{Category::url_deals($category)}}">{{$category->$c_name}}</a></li>
			@endif
		</ul>
		<hr>

		<legend>{{trans('front.type_of_offers')}}</legend>
		<ul class="deal_types_filter">
		@foreach(Deal::$types as $key=>$data)
			<li class="{{$data['class']}}" style="margin-bottom: 10px;">
				<a href="{{url($data['url'])}}">
					<label class="abbr">{{$data['abbr']}}</label> <strong>{{trans('front.'.$data['title'])}}</strong>
				</a>
				<?php $sub_sections = trans('dash.offer_types'); ?>
		        @if ($key=='requests')
		            <?php $sub_sections = trans('dash.rfq_request_for'); ?>
		        @elseif ($key=='partnerships')
		            <?php $sub_sections = trans('dash.partnership_types'); ?>
		        @endif

	            @if($type==$key)
		            <ul class="subitems">
		            @foreach($sub_sections as $ref=>$name)
		            	<li><a href="{{url($data['url'])}}?type={{$ref}}">{{$name}}</a></li>
		            @endforeach
		            </ul>
		        @endif
			</li>
		@endforeach
		</ul>
		<hr>

		<legend>{{trans('front.locations')}}</legend>
		<ul>
			<li><a href="#">{{trans('front.all_locations')}}</a></li>
			@foreach($deal_users as $user)
				@if(isset($user->profile->country))
				<li><a href="#">{{$user->profile->country->$c_nameglish}}</a></li>
				@endif
			@endforeach
		</ul>
	</div>

	<div class="col-md-8">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{url('home')}}">{{trans('front.home')}}</a></li>
				<li><a href="{{url('deals')}}">{{trans('front.deals')}}</a></li>
				<li class="active">{{trans('front.'.Deal::$types[$type]['title'])}}</li>
			</ol>
		</div>

		<div id="dataGrid">
           <div class="jplist-panel">
		      <div
		         class="dropdown pull-left jplist-items-per-page"
		         data-control-type="boot-items-per-page-dropdown"
		         data-control-name="paging"
		         data-control-action="paging">
		         <button
		            class="btn btn-primary dropdown-toggle"
		            type="button"
		            data-toggle="dropdown"
		            id="dropdown-menu-1"
		            aria-expanded="true">
		            <span data-type="selected-text">{{trans('front.items_per_page')}}</span>
		            <span class="caret"></span>
		         </button>
		         <ul class="dropdown-menu" role="menu" aria-labelledby="dropdown-menu-1">
		            <li role="presentation">
		               <a role="menuitem" tabindex="-1" href="#" data-number="3">3 {{trans('front.per_page')}}</a>
		            </li>
		            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="5" data-default="true">5 {{trans('front.per_page')}}</a></li>
		            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-number="10">10 {{trans('front.per_page')}}</a></li>
		            <li role="presentation" class="divider"></li>
		            <li role="presentation">
		               <a role="menuitem" tabindex="-1" href="#" data-number="all">{{trans('front.view_all')}}</a>
		            </li>
		         </ul>
		      </div>
		      <ul
		          class="pagination pull-left jplist-pagination"
		          data-control-type="boot-pagination" data-control-name="paging" data-control-action="paging"
		          data-range="4" data-mode="google-like">
		      </ul>
           </div>

           <div class="list">
              @foreach($deals as $type=>$deals_list)
              	@foreach($deals_list as $item)
	            <div class="list-item">
	              	<div class="row">
	                    <div class="col-md-6 pull-left deal_info">
	                    	@if($type=='offers')
		                    	<span class="label label-warning deal_appr">SO</span>
				                <a href="{{SellOffer::url($item)}}"> {{($item->product)?$item->product->name:''}}</a><br>
				                <ul class="options">
				                	<li><span>{{trans('front.deal_type')}}:</span> {{trans('dash.offer_types')[$item->type]}}</li>
									<li><span>{{trans('front.min_ord_quantity')}}:</span> {{$item->min_quantity}} {{trans('dash.units')[$item->min_quantity_unit]}}</li>
									<li><span>{{trans('front.discount')}}:</span> {{$item->discount}}</li>
									<li><span>{{trans('front.category')}}:</span>
										@if(isset($item->category))
											<a href="{{Category::url($item->category)}}">{{$item->category->$c_name}}</a>
										@endif
									</li>
				                </ul>
				            @elseif($type=='requests')
				            	<?php $quote_btn=trans('front.quote_now'); ?>
				            	<span class="label label-info deal_appr">RFQ</span>
				                <a href="{{QuoteRequest::url($item)}}"> {{Helper::field($item,'name')}}</a><br>
				                <ul class="options">
				                	<li><span>{{trans('front.deal_type')}}:</span> {{trans('dash.rfq_request_for')[$item->request_for]}}</li>
									<li><span>{{trans('front.quantity_required')}}:</span> {{$item->order_quantity}} {{trans('dash.units')[$item->unit]}}</li>
									<li><span>{{trans('front.category')}}:</span> <a href="{{Category::url($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></li>
									<li><span>{{trans('front.quotes_left')}}:</span> 0</li>
				                </ul>
							@elseif($type=='partnerships')
								<?php $quote_btn=trans('front.contact_now'); ?>
								<span class="label label-info deal_appr">PD</span>
				                <a href="{{Partnership::url($item)}}"> {{$item->service_product_name}}</a><br>
				                <ul class="options">
				                	<li><span>{{trans('front.deal_type')}}:</span> {{trans('dash.partnership_types')[$item->type]}}</li>
									<li><span>{{trans('dash.part_offer_as')}}:</span> {{trans('dash.partnership_offer_as')[$item->offer_as]}}</li>
									<li><span>{{trans('dash.part_request')}}:</span> {{trans('front.'.$item->request_for)}}</li>
									<li><span>{{trans('front.category')}}:</span> <a href="{{Category::url($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></li>
				                </ul>
							@endif
		            	</div>
		            	<div class="col-md-6 pull-right deal_contact">
	            			<div class="row">
	            				<div class="col-md-5">
	            					{{Helper::image_single($item->images)}}
	            				</div>
	            				<div class="col-md-7">
	            					<ul>
	            						<li><i class="flag">{!!Deal::flag($item,16)!!}</i> <span>Egypt</span></li>
	            						<li><i class="fa fa-hourglass-end"></i><span>{{Deal::expire_format($item)}}</span></li>
	            						<li><i class="fa fa-calendar"></i><span>{{$item->created_at}}</span></li>
	            					</ul>
	            					<a href="{{url('deals/enquiry/'.$item->id.'/'.$type)}}" class="btn btn-sm btn-success">{{$quote_btn}}</a>
	            					<div class="help-block hidden">(Only for Premium Supplier)</div>
	            				</div>
	            			</div>
		            	</div>
		            </div>
	            </div>
                @endforeach
              @endforeach
           </div>

           <div class="jplist-no-results">
              <p>{{trans('front.no_deals_found')}}</p>
           </div>
	    </div>
	</div>

	<div class="col-md-2">
		<div class="well well-sm">
			<h3>{{trans('front.new_deals')}}</h3>
			<div class="row">
			@foreach($deals as $type=>$deals_list)
				@foreach($deals_list->take(8) as $item)
					<div class="col-md-12">
						<a href="{{SellOffer::url($item)}}" class="thumbnail">
						deal title
						</a>
					</div>
				@endforeach
			@endforeach
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	#dataGrid .list-item {clear:both; border-bottom: 2px solid #e0e0e0; padding:20px 0;}
	#dataGrid .list-item img {max-width: 100%;}
</style>
@endsection

@section('script')
	{!!HTML::style('frontend/js/jplist/jplist.core.min.css')!!}
	{!!HTML::script('frontend/js/jplist/jplist.core.min.js')!!}
	{!!HTML::script('frontend/js/jplist/sorting/jplist.sort-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/pagination/jplist.pagination-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/bootstrap/jplist.bootstrap-pagination-bundle.min.js')!!}
	{!!HTML::script('frontend/js/jplist/filters/jplist.textbox-filter.min.js')!!}
	{!!HTML::style('frontend/js/jplist/filters/jplist.textbox-filter.min.css')!!}
	<script type="text/javascript">
	    $(document).ready(function(){
	       $('#dataGrid').jplist({
	          itemsBox: '.list',
	          itemPath: '.list-item',
	          panelPath: '.jplist-panel'
	       });
	    });
    </script>
@endsection