<?php
	use App\Category;
	use App\Product;
	use App\Helper;
	use App\Deals\Deal;
	use App\Deals\Partnership;
	use App\PackageUsage;
	use App\User;

	$parents = array_reverse($category_parents);
	$profile = $offer->user->profile;
	$offer_lang = Helper::lang_data($offer, $lng_id);

	$market_countries='NA';
	$offer_countries = [];
	if(!empty($offer->countries)) {
		$offer_countries = explode(',', $offer->countries);
	}
?>
@extends('front.layouts.master')
@section('title') @lang('front.partnership_deals') > {{$offer_lang->name}} | @endsection
@section('content')
<div class="breadcumb-list">
	<ul class="container">
	  	<li><a href="{{url('home')}}">@lang('front.home')</a></li>
	  	<li><a href="{{url('deals')}}">@lang('front.deals')</a></li>
	  	@foreach($parents as $cat)
	  		@if($cat)
				<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a></li>
			@endif
		@endforeach
		<li><a href="{{url(Deal::$types['partnerships']['url'])}}">@lang('front.partnership_deals')</a></li>
		<li>{{$offer_lang->name}}</li>
	</ul>
</div>
<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>@lang('front.loading_images')</div>
                <div class="sp-wrap">
                	@foreach($offer->images as $item)
                    	<a href="{{url('uploads/'.$item->image)}}">{!!Helper::image2($item->image,[],false)!!}</a>
                    @endforeach
                </div>
			</div>
			<div class="col-xs-12 col-md-5">
				<header><h1 class="hdr-styl-001">{{$offer_lang->name}}</h1></header>
				<header class="hdr-styl-05 deals-row">
	                <a href="{{url(Deal::$types['partnerships']['url'])}}" class="tag pull-left">PD</a>
	                <h4><a href="{{url(Deal::$types['partnerships']['url'])}}">@lang('front.partnership_deals')</a></h4>
	            </header>
				<p class="nrml-p">
					{!!str_limit(strip_tags($offer_lang->details), 250)!!} 
					<a href="#deal_details">@lang('dash.more')</a>
				</p>
				<div class="table-responsive">
					<table class="table inf-tbl">
						<tbody>
							<tr>
								<td style="width:140px;">@lang('dash.type_of_deal'):</td>
								<td>{{trans('dash.partnership_types')[$offer->type]}}</td>
							</tr>
							<tr>
								<td>@lang('dash.part_offer_as'):</td>
								<td>{{trans('dash.partnership_offer_as')[$offer->offer_as]}}</td>
							</tr>
							<tr>
								<td>@lang('dash.part_request'):</td>
								<td>{{trans('front.'.$offer->request_for)}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="shr-prnt-br02 row">
					<div class="col-xs-12 col-md-7 folow-us">
						<span>@lang('front.share_us_on'):</span>
						<div class="addthis_inline_share_toolbox"></div>
					</div>
					<div class="col-xs-12 col-md-5 prnt-br">
						<a href="#"><i class="fa fa-print" aria-hidden="true"></i> @lang('front.print')</a>
						<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> @lang('front.email')</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				@if($locker && !PackageUsage::unlocked($offer->user_id))
				<div class="lock_layer">
					<a href="{{User::unlock_url('deal_partnership', $offer->id, $offer->user_id)}}" class="unlock_act popup"><i class="fa fa-unlock"></i> @lang('front.unlock_contact_details')</a>
				</div>
				@endif

			@if($locker)
				<div class="{{(PackageUsage::unlocked($offer->user_id))?'':'locked'}}">
			@else
				<div>
			@endif
					@include('front.deals.company_details')
					<div class="cnct-inf-sid ad-pd text-center">
			          	<a href="{{url('deals/enquiry/'.$offer->id.'/partnerships')}}" class="grn-btn">@lang('front.contact_now')</a>
					</div>
				</div>
			</div>
			<br class="clr-bth" />

			<a name="deal_details"></a>
			<div id="tabs02" class="tabs02" style="margin:20px 0">
				<ul>
				    <li><a class="tab_nav" href="#tabs02-1">@lang('front.deal_description')</a></li>
				</ul>
				<div id="tabs02-1" class="offer_vars">
					<div class="row">
						<div class="col-md-4">
							<span>{{trans('dash.request_location')}}:</span> 
							@foreach($offer_countries as $id)
								<a href="#" class="btn btn-sm btn-default">{{App\Country::get_country($id)->name}}</a>
							@endforeach
						</div>
						<div class="col-md-4">
							<span>{{trans('dash.Field_of_Expertise')}}:</span> {{$offer_lang->expertise}}
						</div>
					</div>
					<br>
				    {!!$offer_lang->details!!}

				    <hr>
				    <div class="text-center">
				    	<a href="{{url('deals/enquiry/'.$offer->id.'/partnerships')}}" class="grn-btn">{{trans('front.contact_now')}}</a>
				    </div>
				</div>
			</div>
			<div class="addyours text-center"><a class="btn btn-success" href="{{url('dashboard/partnerships/create')}}"><i class="fa fa-plus"></i> @lang('front.addyour_partnership')</a></div>
		</div>
	</div>
</div>
@endsection

@section('script')
	@include('dashboard.layouts.popup')
@endsection