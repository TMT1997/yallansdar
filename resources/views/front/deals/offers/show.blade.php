<?php
	use App\Category;
	use App\Product;
	use App\Helper;
	use App\Deals\Deal;
	use App\Deals\SellOffer;
	use App\User;
	$parents = array_reverse($category_parents);
	$profile = ($offer->user) ? $offer->user->profile:null;
	$offer_lang = Helper::lang_data($offer, $lng_id);

	$market_countries = null;
	if(!empty($offer->market_country)) {
		$offer_countries = explode(',', $offer->market_country);
		$market_countries = '';
		foreach($offer_countries as $id) {
			$market_countries .= App\Country::get_country($id)->name.', ';
		}
	}
?>
@extends('front.layouts.master')
@section('title') {{trans('front.sell_offers')}} | @endsection
@section('content')
<div class="breadcumb-list">
	<ul class="container">
	  	<li><a href="{{url('home')}}">{{trans('front.home')}}</a></li>
	  	<li><a href="{{url('deals')}}">{{trans('front.deals')}}</a></li>
	  	@foreach($parents as $cat)
	  		@if($cat)
				<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a></li>
			@endif
		@endforeach
		<li>{{Product::name($offer->product, $lng_id)}}</li>
	</ul>
</div>
<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>@lang('front.loading_images')</div>
                <div class="sp-wrap">
                	@foreach($offer->images as $item)
                    	<a href="{{url('uploads/'.$item->image)}}">{!!Helper::image2($item->image,[],false)!!}</a>
                    @endforeach
                </div>
			</div>
			<div class="col-xs-12 col-md-5">
				<header>
					<h1 class="hdr-styl-001">{{SellOffer::prod_name($offer)}}</h1>
				</header>
				<header class="deals-row hdr-styl-03">
	                <a href="{{url(Deal::$types['offers']['url'])}}" class="tag pull-left">SO</a>
	                <h4><a href="{{url(Deal::$types['offers']['url'])}}">{{trans('front.sell_offers')}}</a></h4>
	            </header>
				<p class="nrml-p">
					{!!str_limit($offer->description, 250)!!}
				</p>
				<div class="table-responsive">
					<table class="table inf-tbl">
						<tbody>
							<tr>
								<td style="width: 120px;">{{trans('front.sell_offer_type')}}:</td>
								<td>{{trans('dash.offer_types')[$offer->type]}}</td>
							</tr>
							<tr>
								<td>{{trans('front.min_ord_quantity')}}:</td>
								<td>{{$offer->min_quantity}} {{trans('dash.units')[$offer->min_quantity_unit]}}</td>
							</tr>
							<tr>
								<td>{{trans('front.discount')}}:</td>
								<td>{{$offer->discount}}</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="shr-prnt-br02 row">
					<div class="col-xs-12 col-md-7 folow-us">
						<span>{{trans('front.share_us_on')}}:</span>
						<div style="display: inline-block;" class="addthis_inline_share_toolbox"></div>
					</div>
					<div class="col-xs-12 col-md-5 prnt-br">
						<a href="#"><i class="fa fa-print" aria-hidden="true"></i> @lang('front.print')</a>
						<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> @lang('front.email')</a>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				@include('front.deals.company_details')
				<div class="cnct-inf-sid ad-pd text-center">
		          	<a href="{{url('deals/enquiry/'.$offer->id.'/offers')}}" class="grn-btn">{{trans('front.contact_now')}}</a>
				</div>
			</div>
			<br class="clr-bth" />

			<div id="tabs02" class="tabs02" style="margin:20px 0">
				<ul>
				    <li><a class="tab_nav" href="#tabs02-1">{{trans('front.deal_description')}}</a></li>
				    <li><a class="tab_nav" href="#tabs02-2">{{trans('front.company_profile')}}</a></li>
				</ul>
				<div id="tabs02-1" class="offer_vars">
					<legend>{{trans('front.deal_description')}}</legend>
					<div class="row">
						<div class="col-md-4">
							<span>{{trans('front.fob_price')}}:</span> {{$offer->min_price}}
						</div>
						<div class="col-md-4">
							<span>{{trans('dash.discount')}}:</span> {{$offer->discount}}
						</div>
						<div class="col-md-4">
							<span>{{trans('dash.offer_valid_till')}}:</span> {{$offer->exoired_at}}
						</div>
						<div class="col-md-4">
							<span>{{trans('front.port_dispatch')}}:</span> {{$offer->port}}
						</div>
						<div class="col-md-4">
							<span>{{trans('dash.market_focus')}}:</span> {{$market_countries}}
						</div>
					</div>

					<hr>
					<legend>{{trans('front.offer_description')}}</legend>
				    {!!$offer_lang->description!!}
				</div>
				<div id="tabs02-2">
					{{$offer->user->profile->company_name}} <br>
				    <a href="{{User::url_contact($offer->user)}}" class="popup"><i class="fa fa-paper-plane bluu"></i>
				    	{{trans('front.contact_details')}}</a>
				</div>
			</div>
			<div class="addyours text-center"><a class="btn btn-success" href="{{url('dashboard/sell_offers/create')}}"><i class="fa fa-plus"></i> @lang('front.addyour_offer')</a></div>
		</div>
	</div>
</div>
@endsection

@section('script')
	@include('dashboard.layouts.popup')
@endsection