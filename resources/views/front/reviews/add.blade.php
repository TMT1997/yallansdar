<div class="white-popup-block">
<legend>{{trans('front.post_review')}}</legend>

	@if(Auth::check())
		{!!Form::open(['id'=>'rev_form', 'class'=>'form-horizontal', 'url'=>url('reviews/add')])!!}
			{!!Form::hidden('ref_id', $ref_id)!!}
			{!!Form::hidden('ref_type', $ref_type)!!}
			<div class="form-group">
				{!!Form::label('rate', trans('front.prod_review_rate'), ['class'=>'control-label col-md-3'])!!}
				<div class="col-md-9 rating">
					<div class="btn-group" data-toggle="buttons">
						@foreach(trans('front.review_rates') as $rate=>$info)
					    	<label class="btn btn-default">
					        	<input type="radio" name="rate" value="{{$rate}}" title="{{$info[0]}}" autocomplete="off">
					    		{{$info[0]}}
					    	</label>
					    @endforeach
					</div>
					<div class="error"></div>
				</div>
			</div>
			<div class="form-group">
				{!!Form::label('subject', trans('front.review_subject'), ['class'=>'control-label col-md-3'])!!}
				<div class="col-md-9">
					{!!Form::text('subject', null, ['class'=>'form-control','placeholder'=>trans('front.enter_review_subject')])!!}
					<div class="error"></div>
				</div>
			</div>
			<div class="form-group">
				{!!Form::label('body', trans('front.review_body'), ['class'=>'control-label col-md-3'])!!}
				<div class="col-md-9">
					{!!Form::textarea('body',null,['class'=>'form-control','placeholder'=>trans('front.enter_review_body'),'rows'=>4])!!}
					<div class="error"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3"></div>
				<div class="col-md-9"> 
					{!!Form::submit(trans('front.review_submit'),['class'=>'btn btn-success btn_quickAddReview'])!!}
				</div>
			</div>
		{!!Form::close()!!}
	@else
		<div class="alert alert-warning">Please <a href="{{url('login')}}">login</a> to be able to add reviews</div>
	@endif
</div>