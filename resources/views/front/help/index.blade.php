@extends('front.layouts.master')
@section('content')
	

	<div class="container">
		<legend>Yalla Nsadar Help Resources</legend>

		{!!Form::open(['url'=>'help'])!!}
			<div class="input-group">
				{!!Form::text('q', null, ['class'=>'form-control', 'placeholder'=>'Type your search keywords here'])!!}
				<span class="input-group-btn">
					<button type="button" class="btn btn-default">Search</button>					
				</span>
			</div>
		{!!Form::close()!!}

		<div class="form-group">
			<div class="alert alert-warning">No articles found.</div>
		</div>
	</div>

	
@endsection