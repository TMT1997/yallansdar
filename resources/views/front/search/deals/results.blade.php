<?php
	use App\Category;
	use App\Helper;
	use App\Deals\Deal;
	use App\Deals\SellOffer;
	use App\Deals\QuoteRequest;
	use App\Deals\Partnership;
?>
@extends('front.layouts.master')
@section('content')

<div class="container">
	<div class="col-md-2 cats_nav">
	
	</div>

	<div class="col-md-8">
		<section class="">
			<header><h1 class="hdr-styl-001 inr-hdr-blk">{{trans('front.search_results')}}: {{$keyword}}</h1></header>
		    <div class="row deals-row">
		        <article class="col-xs-12 col-md-12">
		            <header class="hdr-styl-03">
		                <a href="{{url(Deal::$types['offers']['url'])}}" class="tag pull-left">SO</a>
		                <h4><a href="{{url(Deal::$types['offers']['url'])}}">{{trans('front.sell_offers')}} ({{$deals['sell_offers']->count()}})</a></h4>
		            </header>
		            <div class="row"> 
			            @foreach($deals['sell_offers']->take(6) as $item)
			            	<div class="col-md-12">
					            <figure>
					                <a href="{{SellOffer::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">{!!Deal::flag($item)!!}</span>
					                    <h4><a href="{{SellOffer::url($item)}}"><strong>{{SellOffer::prod_name($item)}}</strong></a></h4>
					                    <p class="p-1">{{trans('front.moq')}}: {{$item->min_quantity}} {{trans('dash.units')[$item->min_quantity_unit]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>{{trans('front.category')}}:</span> <a href="{{Category::url_deals($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
					        </div>
			            @endforeach
			        </div>
		        </article>

		        <div class="row"><div class="col-md-12"><hr class="bold"></div></div>

		        <article class="col-xs-12 col-md-12">
		            <header class="hdr-styl-04">
		                <a href="{{url(Deal::$types['requests']['url'])}}" class="tag pull-left">RFQ</a>
		                <h4><a href="{{url(Deal::$types['requests']['url'])}}">{{trans('front.request_quotes')}} ({{$deals['quote_requests']->count()}})</a></h4>
		            </header>
		            <div class="row"> 
			            @foreach($deals['quote_requests']->take(6) as $item)
			            	<div class="col-md-12">
					            <figure>
					            	<a href="{{QuoteRequest::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">{!!Deal::flag($item)!!}</span>
					                    <h4><a href="{{QuoteRequest::url($item)}}"><strong>{{$item->service_product_name}}</strong></a></h4>
					                    <p class="p-1">{{trans('front.EOQ')}}: {{$item->order_quantity}} {{trans('dash.units')[$item->unit]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>{{trans('front.category')}}:</span> <a href="{{Category::url_deals($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
				            </div>
			            @endforeach
			        </div>
		        </article>

		        <div class="row"><div class="col-md-12"><hr class="bold"></div></div>

		        <article class="col-xs-12 col-md-12 latest_deals">
		            <header class="hdr-styl-05">
		                <a href="{{url(Deal::$types['partnerships']['url'])}}" class="tag pull-left">PD</a>
		                <h4><a href="{{url(Deal::$types['partnerships']['url'])}}">{{trans('front.partnership_deals')}} ({{$deals['partnerships']->count()}})</a></h4>
		            </header>
		            <div class="row"> 
			            @foreach($deals['partnerships']->take(6) as $item)
				            <div class="col-md-12">
					            <figure>
					                <a href="{{Partnership::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>
					                <figcaption>
					                    <span class="flg-ico">{!!Deal::flag($item)!!}</span>
					                    <h4><a href="{{Partnership::url($item)}}"><strong>{{$item->service_product_name}}</strong></a></h4>
					                    <p class="p-1">{{trans('dash.partnership_types')[$item->type]}}</p>
					                    <p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>
					                    <p class="p-1"><span>{{trans('front.category')}}:</span> <a href="{{Category::url_deals($item->category)}}">{{($item->category)?$item->category->$c_name:''}}</a></p>
					                </figcaption>
					            </figure>
				            </div>
			            @endforeach
			        </div>
		        </article>
			</div>
		</section>
	</div>

	<div class="col-md-2">
		<div class="well well-sm">
			<h4>{{trans('front.new_deals')}}</h4>
			<div class="row"> 
			@foreach($deals as $type=>$items)
				@foreach($items->take(4) as $item)
					<div class="col-md-12">
						@if($type=='sell_offer')
							<a href="{{SellOffer::url($item)}}" class="thumbnail">{{Helper::image_single($item->images)}} {{SellOffer::prod_name($item)}}</a>
						@elseif($type=='partnerships')
							<a href="{{Partnership::url($item)}}" class="thumbnail">{{$item->service_product_name}}</a>
						@else
							<a href="{{QuoteRequest::url($item)}}" class="thumbnail">{{$item->service_product_name}}</a>
						@endif
					</div>
				@endforeach
			@endforeach
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	#dataGrid .list-item {clear:both; border-bottom: 2px solid #e0e0e0; padding:20px 0;}
	#dataGrid .list-item img {max-width: 100%;}
</style>
@endsection

@section('script')

@endsection