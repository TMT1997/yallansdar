<?php
	use App\Category;
	use App\Product;
    use App\Helper;
    use App\User;
?>
@extends('front.layouts.master')
@section('title') @lang('front.search_results') | {{$keyword}} @endsection
@section('content')
	
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                {{-- @include('front.categories.sidebar_filter') --}}
			</div>

			<div class="col-xs-12 col-md-8">			
				<header><h1 class="hdr-styl-001 inr-hdr-blk">@lang('front.search_results'): {{$keyword}}</h1></header>
    
                <div class="col-xs-12 col-md-9">
					<div class="breadcumb-list brcrmb-inr">
						<ul class="">
						  	<li><a href="{{url('/')}}">@lang('front.home')</a></li>
                            <li><a href="{{url('/')}}">@lang('front.search_results')</a></li>
							<li>{{($products ) ? $products->count() : 0}} @lang('front.products')</li>
						</ul>
					</div>
                </div>
                @if($products && $products->count() > 0)
                    <div class="col-xs-12 col-md-3">
                        <div class="dataTables_length">
                            <label>@lang('front.show') 
                            <select name="example_length" aria-controls="example" class="form-control input-sm custom-show-slct">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> @lang('front.entries')</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                        <div class="col-xs-12 col-md-7 fltrs">
                            @lang('front.sort_by'): <label><input type="checkbox" id="verified" data-page="1">@lang('front.verified')</label>
                        </div>
                        <div class="col-xs-12 col-md-5 rt-tp-optons">
                            <div id="next-bttn"></div>
                            <div id="prv-bttn"></div>
                            <div class="pag-stt-info"></div>
                            <div class="grd-srt-btn"></div>
                            <div class="rows-srt-btn actv-grid"></div>
                            @lang('front.view_by'):
                        </div>              
                    </div>
                    <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                        <thead>
                            <tr><th></th></tr>
                        </thead>
                        <tfoot>
                            <tr><th></th></tr>
                        </tfoot>
                        <tbody>
                        	@foreach($products as $product)
                            <?php 
                                $profile = ($product->user) ? $product->user->profile : null;
                            ?>
                            <tr>
                                <td>
                                    <div class="row pro-thumb-row">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                            <a class="img-hldr-product" href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                                            <a href="{{Product::url($product)}}" class="prduct-thumb-ttl">{{Helper::field($product,'name')}}</a>
                                            <p class="pro-thumb-sumry">{{str_limit(Helper::field($product, 'description'),140)}}</p>
                                            <div class="rqst-qution-hldr">
                                                <a href="{{Product::url($product)}}" class="grn-btn">@lang('front.request_for_quote')</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  options-col">
                                            <span class="produc-by"><i class="fa fa-check-circle verfid"></i>
                                            <a href="#">{{@$profile->company_name}}</a></span>
                                            <span>{!!User::flag($product->user,[],true)!!}</span>
                                            <span class="b-frst-t-rvu"><i class="fa fa-star-o"></i> <a href="#">@lang('front.befirst_to_review')</a></span>
                                            <span class="cntct-dtlls"><a href="#"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a></span>
                                        </div>
                                        <div class="rqst-qution-hldr scnd-btn">
                                            {!!Form::open(['url'=>'cart'])!!}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <button type="submit" class="grn-btn add-to-cart"><i class="fa fa-shopping-cart"></i> @lang('front.request_for_quote')</button>
                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-warning">لا يوجد نتائج</div>
                        </div>
                    </div>
                @endif
            </div>            
            <aside class="col-xs-12 col-md-2 spnsrs-asid">
                <header><h3 class="hdr-styl-asid">@lang('front.sponsored_products')</h3></header>
                @foreach($products_sponsored as $product)
	                <div class="prod_item">
                        <a href="{{Product::url($product)}}" class="thumbnail">
                            {{Product::image($product)}}
                            <div class="caption">{{Helper::field($product,'name')}}</div>
                        </a>
                    </div>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection