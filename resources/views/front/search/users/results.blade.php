<?php
	use App\Category;
	use App\Product;
    use App\Helper;
    use App\User;
    use App\Company;
?>
@extends('front.layouts.master')
@section('title') {{trans('front.search_results')}} | {{$keyword}} @endsection
@section('content')
	
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                {{-- @include('front.categories.sidebar_filter') --}}
			</div>
			<div class="col-xs-12 col-md-8">			
				<header><h1 class="hdr-styl-001 inr-hdr-blk">{{trans('front.search_results')}}: {{$keyword}}</h1></header>
                
                <div class="col-xs-12 col-md-9">
					<div class="breadcumb-list brcrmb-inr">
						<ul class="">
						  	<li><a href="{{url('/')}}">{{trans('front.home')}}</a></li>
                            <li><a href="{{url('/')}}">{{trans('front.search_results')}}</a></li>
							<li>{{$users->count()}} {{trans('front.products')}}</li>
						</ul>
					</div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="dataTables_length">
                        <label>{{trans('front.show')}} 
                        <select name="example_length" aria-controls="example" class="form-control input-sm custom-show-slct">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select> {{trans('front.entries')}}</label>
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                    <div class="col-xs-12 col-md-7 fltrs">
                        {{trans('front.sort_by')}}: <label><input type="checkbox" id="verified" data-page="1">{{trans('front.verified')}}</label>
                    </div>
                    <div class="col-xs-12 col-md-5 rt-tp-optons">
                        <div id="next-bttn"></div>
                        <div id="prv-bttn"></div>
                        <div class="pag-stt-info"></div>
                        <div class="grd-srt-btn"></div>
                        <div class="rows-srt-btn actv-grid"></div>
                        {{trans('front.view_by')}}:
                    </div>              
                </div>
                <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr><th></th></tr>
                    </thead>
                    <tfoot>
                        <tr><th></th></tr>
                    </tfoot>
                    <tbody>
                    	@foreach($users as $user)
                        <?php 
                            $profile = $user->profile;
                        ?>
                        <tr>
                            <td>
                                <div class="row pro-thumb-row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                        <a class="img-hldr-product" href="{{User::contact_info($user)}}">
                                            @if(isset($profile->picture))
                                                {{Helper::image2($profile->picture, [])}}
                                            @endif
                                        </a>
                                        <a href="{{User::contact_info($user)}}" class="prduct-thumb-ttl">{{$user->name}}</a>
                                        <p class="pro-thumb-sumry">
                                            @if($profile)
                                                {{$profile->company_name}}
                                            @else
                                                {{$user->company_name}}
                                            @endif
                                        </p>
                                        <p>
                                            <span>{!!User::flag($user,[],true)!!}</span>
                                        </p>
                                        <div class="rqst-qution-hldr">
                                            <a href="{{User::contact_info($user)}}" class="grn-btn">{{trans('front.contact_details')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>				
			</div>
            
            <aside class="col-xs-12 col-md-2 spnsrs-asid">
                <header><h3 class="hdr-styl-asid">{{trans('front.sponsored_products')}}</h3></header>
                @foreach($products_sponsored as $user)
	                <figure class="asid-prdct-itm">
	                    <a href="{{Product::url($user)}}">{{Product::image($user)}}</a>
	                    <figcaption><h4><a href="#d">{{Product::name($user)}}</a></h4></figcaption>
	                </figure>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection