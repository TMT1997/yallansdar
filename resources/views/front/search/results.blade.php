<?php
	use App\Category;
	use App\Product;
	$parents = array_reverse($category_parents);
?>
@extends('front.layouts.master')
@section('title') {{$category->$c_name}} | @endsection
@section('content')
	
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                @include('front.categories.sidebar_filter')
			</div>

			<div class="col-xs-12 col-md-8">			
				<header><h1 class="hdr-styl-001 inr-hdr-blk">{{$category->$c_name}}</h1></header>
				
				<p class="nrml-p">
                    {!!$category->description!!}
				</p>
                
                <div class="col-xs-12 col-md-9">
					<div class="breadcumb-list brcrmb-inr">
						<ul class="">
						  	<li><a href="{{url('/')}}">Home</a></li>
						  	@foreach($parents as $cat)
								<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a>
							@endforeach
							<li>{{$products->count()}} products</li>
						</ul>
					</div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                    <div class="dataTables_length">
                        <label>Show 
                        <select name="example_length" aria-controls="example" class="form-control input-sm custom-show-slct">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select> entries</label>
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                    <div class="col-xs-12 col-md-7 fltrs">
                        Sort By: <label><input type="checkbox" id="verified" data-page="1">Verified</label>
                    </div>
                    <div class="col-xs-12 col-md-5 rt-tp-optons">
                        <div id="next-bttn"></div>
                        <div id="prv-bttn"></div>
                        <div class="pag-stt-info"></div>
                        <div class="grd-srt-btn"></div>
                        <div class="rows-srt-btn actv-grid"></div>
                        View By:
                    </div>              
                </div>
                <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr><th></th></tr>
                    </thead>
                    <tfoot>
                        <tr><th></th></tr>
                    </tfoot>
                    <tbody>
                    	@foreach($products as $product)
                        <tr>
                            <td>
                                <div class="row pro-thumb-row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                        <a class="img-hldr-product" href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                                        <a href="{{Product::url($product)}}" class="prduct-thumb-ttl">{{$product->name}}</a>
                                        <p class="pro-thumb-sumry">{{str_limit($product->details,140)}}</p>
                                        <div class="rqst-qution-hldr">
                                            <a href="{{Product::url($product)}}" class="grn-btn">Request for Quote</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  options-col">
                                        <span class="produc-by"><i class="fa fa-check-circle verfid"></i> <a href="#d">Vel Textile</a></span>
                                        <span>{!!App\User::flag($product->user)!!} Egypt</span>
                                        <span class="b-frst-t-rvu"><i class="fa fa-star-o"></i> <a href="#d"> Be the first to review</a></span>
                                        <span class="cntct-dtlls"><a href="#d"><i class="fa fa-paper-plane bluu"></i> Contact Details</a></span>
                                        <div>
                                            <a ><i class="fa fa-camera"></i></a>
                                            <a href="#d"><i class="fa fa-film"></i></a>
                                            <a ><i class="fa fa-info"></i></a>
                                            <a href="#d"><i class="fa fa-tag"></i></a>
                                        </div>
                                    </div>
                                    <div class="rqst-qution-hldr scnd-btn">
                                        <a href="#d" class="grn-btn">Request for Quote</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>				
			</div>
            
            <aside class="col-xs-12 col-md-2 spnsrs-asid">
                <header><h3 class="hdr-styl-asid">Sponsored Products</h3></header>
                @foreach($products_sponsored as $product)
	                <figure class="asid-prdct-itm">
	                    <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
	                    <figcaption><h4><a href="#d">{{Product::name($product)}}</a></h4></figcaption>
	                </figure>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection