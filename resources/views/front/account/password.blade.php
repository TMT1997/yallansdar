@extends('front.layouts.master')
@section('content')
<section class="homefront">
	<div class="container">
		<h2 class="line"><span>{!!HTML::image('images/ico.png')!!} حسابي الشخصي</span></h2>
		
		<div class="col-md-12">
	      @if(Session::has('flash_message'))
	          <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {!!Session::get('flash_message')!!}
	          </div>
	      @endif
	    </div>
	    <div class="panel panel-info">
            <div class="panel-heading">تغيير كلمة المرور</div>
            <div class="panel-body">
				<div class="col-md-12">
					{!!Form::open(['class'=>'form-horizontal', 'url'=>'account/password'])!!}
						<div class="form-group">
							{!!Form::label('old_password', 'كلمة المرور الحالية', ['class'=>'control-label'])!!}
							{!!Form::text('old_password',null, ['class'=>'form-control', 'required'=>'required'])!!}
							{!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
						</div>

						<div class="form-group">
							{!!Form::label('password', 'كلمة المرور الجديدة', ['class'=>'control-label'])!!}
							{!!Form::text('password',null, ['class'=>'form-control', 'required'=>'required'])!!}
							{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
						</div>

						<div class="form-group">
							{!!Form::label('password_confirmation', 'تأكيد كلمة المرور الجديدة', ['class'=>'control-label'])!!}
							{!!Form::text('password_confirmation',null, ['class'=>'form-control', 'required'=>'required'])!!}
							{!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
						</div>

						<div class="form-group">
							{!!Form::submit('تغيير', ['class'=>'btn btn-warning btn-lg'])!!}
						</div>
					{!!Form::close()!!}
				</div>
			</div>
		</div>
		<hr>
	    <a href="{{url('account')}}" class="btn btn-default"><i class="fa fa-forward"></i> العودة لحسابي الشخصي</a>
	    <br><br>
	</div>
</section>
@endsection