<!--add extends and add section and end section-->
<!--<section class="homefront">
	<div class="container">
		<h2 class="line"><span>حسابي الشخصي</span></h2>
		
		<div class="alert alert-success" style="margin: 30px 0; text-align: center;">
			<i class="fa fa-check fa-3x text-success"></i>
			<br>
			<strong>شكرا لقد تم تسجيلك بنجاح </strong><br>
			يرجى مراجعة صندوق البريد الخاص بك لتفعيل حسابك
			<br>

			{!!App\Settings::fetch('registered', $settings['texts'])!!}
		</div>

		<p style="font-style: italic;">
			ملحوظة: إذا لم تصلك رسالة التفعيل، يرجى مراجعة صندوق الرسائل غير المرغوب فيها (Spam/Junk Folder)
			<br>
		</p>
		<center>
	    <a href="{{url('dashboard/reg')}}">
		<div style="text-align: center;font-size: 35px;" class="btn btn-info">
			{{trans('dash.finish_reg')}}
		</div>
		</a>
		</center>
	</div>
</section>-->


         <!DOCTYPE html>
 <html lang="">
 <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Yalla Nsadar</title>
     {{HTML::style("frontend/css/bootstrap.min.css")}}
     {{HTML::style('frontend/css/font-awesome.min.css')}}
     {{HTML::style('frontend/css/custom.css')}}
     @if(App::isLocale('ar'))
         {{HTML::style('backend/dist/css/bootstrap-rtl.min.css')}}
         {{HTML::style('frontend/css/rtl.css')}}
     @endif
     <style type="text/css">
         body {background: #f5f5f5}
     </style>
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
 </head>
 <body>
 <div class="container-fluid" style="background: #79b4ea; margin-bottom: 20px;">
     <div class="row">
         <div class="col-md-12">
             <a href="{{url('/')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>

         </div>
     </div>
 </div>

 <div class="container-fluid">
     <div class="col-md-12 text-center">
         <div class="jumbotron text-xs-center">
             <h1 class="display-3">{{trans('front.thanks')}}!</h1>
             <p class="lead">{!!Session::get('redirect_message')!!}</p>
             <hr>
         </div>

     </div>
 </div>
 <section class="homefront">
	<div class="container">
		<h2 class="line"><span>{{trans('dash.my_account')}}</span></h2>
		
		<div class="alert alert-success" style="margin: 30px 0; text-align: center;">
			<i class="fa fa-check fa-3x text-success"></i>
			<br>
			<strong>{{trans('dash.registeration_thanks')}}</strong><br>
			{{trans('dash.show_your_inbox')}}
			<br>

			{!!App\Settings::fetch('registered', $settings['texts'])!!}
		</div>

		<p style="font-style: italic;">
			{{trans('dash.email_note')}}
			<br>
		</p>
		<center>
	    <a href="{{url('dashboard/reg')}}">
		<div style="text-align: center;font-size: 35px;" class="btn btn-info">
			{{trans('dash.finish_reg')}}
		</div>
		</a>
		</center>
	</div>
</section>
 {!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
 {!!HTML::script('frontend/js/bootstrap.min.js')!!}
 <script>
 </script>
 @yield('script')
 </body></html>

