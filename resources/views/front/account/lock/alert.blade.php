<?php
	
?>
<div class="white-popup-block">
<legend>@lang('front.unlock_contact_details')</legend>

	@if(Auth::check())
		@if($reason=='no_package')
			<div class="alert alert-warning">عفوا لا يوجد باقة خاصة بك حتى الأن</div>
			لاختيار باقة <a href="{{url('upgrade')}}">اضغط هنا</a>
		@endif
	@else
		<div class="alert alert-warning">@lang('front.tounlockp')
			<a href="{{url('interact/quicklogin?next=unlock&ref='.$ref.'&id='.$record_id)}}" class="popup">@lang('front.login')</a>
		</div>
	@endif
</div>

<script type="text/javascript">
	$(document).ready(function() {
	  $('.popup').magnificPopup({type:'ajax'});
	});
</script>