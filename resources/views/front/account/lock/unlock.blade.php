<?php
	$ref = Request::get('ref');
	$record_id = Request::get('id');
	$item_total = $item_info['value'];
	if(!empty($item_info['custom_value'])) {
		$item_total = $item_info['custom_value'];
	}
	$user_total = $user_usage->count();
?>
<div class="white-popup-block">
<legend>@lang('front.unlock_contact_details')</legend>

	@if(Auth::check())
		<h3>@lang('front.yourpackage'): {{$user_package->package->name}}</h3>
		<div class="alert alert-info">
			@lang('front.you_unlocked') {{$user_total}} @lang('front.of') {{$item_total}} @lang('front.unlocks')
		</div>

		@if($user_total>=$item_total)
			<div class="alert alert-danger">
				@lang('front.unlock_alert')	<br>
				<a href="{{url('upgrade?ref=unlock_try')}}" class="btn btn-warning">@lang('front.upgrade_now')</a>
			</div>
		@else
			<div id="locker_msg"></div>
			{!!Form::open(['class'=>'locker_form'])!!}
				{!!Form::hidden('ref', $ref)!!}
	            {!!Form::hidden('id', $record_id)!!}
	            {!!Form::hidden('unid', Request::get('unid'))!!}
				{!!Form::submit(trans('front.unlock'), ['class'=>'btn btn-primary locker_btn'])!!}
			{!!Form::close()!!}
		@endif
	@else
		<div class="alert alert-warning">@lang('front.tounlockp')
			<a href="{{url('interact/quicklogin?next=unlock&ref='.$ref.'&id='.$record_id)}}" class="popup">@lang('front.login')</a>
		</div>
	@endif
</div>

<script type="text/javascript">
	$(document).ready(function() {
	  $('.popup').magnificPopup({type:'ajax'});
	});
</script>