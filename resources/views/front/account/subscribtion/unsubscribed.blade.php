@extends('front.layouts.master')
@section('content')
<section class="homefront">
	<div class="container">
		<h2 class="line"><span>{!!HTML::image('images/ico.png')!!} حسابي الشخصي</span></h2>
		
		<div class="col-md-12" style="margin-bottom: 20px;">
	      @if(Session::has('flash_message'))
	          <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {!!Session::get('flash_message')!!}
	          </div>
	      @endif
	    </div>


	    <div class="text-center" style="margin: 30px 0;">
	    	<a href="{{url('account')}}" class="btn btn-primary">العودة لحسابي الشخصي</a>
	    </div>
		
	</div>
</section>
@endsection