
<!--<section class="homefront">
	<div class="container">
		<h2 class="line"><span>حسابي الشخصي</span></h2>
		
		<div class="col-md-12" style="margin-bottom: 20px;">
	      @if(Session::has('flash_message'))
	          <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {!!Session::get('flash_message')!!}
	          </div>
	      @endif
	    </div>

	    <div class="text-center">
	    	{!!Form::open(['url'=>'interact/send-verification-email'])!!}
	    		إذا لم تصلك رسالة التفعيل قم 	    		
	    		{!!Form::submit('بإعادة إرسال رسالة التفعيل', ['class'=>'btn'])!!}
	    	{!!Form::close()!!}
	    </div>
	    <br>
	    <center>
	    <a href="{{url('dashboard/reg')}}">
		<div style="text-align: center;font-size: 35px;" class="btn btn-info">
			{{trans('dash.finish_reg')}}
		</div>
		</a>
		</center>
	</div>
</section>-->

 {{--Created by PhpStorm.--}}
 {{--User: Talaat--}}
 {{--Date: 7/31/2018--}}
 {{--Time: 5:03 PM--}}

         <!DOCTYPE html>
 <html lang="">
 <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Yalla Nsadar</title>
     {{HTML::style("frontend/css/bootstrap.min.css")}}
     {{HTML::style('frontend/css/font-awesome.min.css')}}
     {{HTML::style('frontend/css/custom.css')}}
     @if(App::isLocale('ar'))
         {{HTML::style('backend/dist/css/bootstrap-rtl.min.css')}}
         {{HTML::style('frontend/css/rtl.css')}}
     @endif
     <style type="text/css">
         body {background: #f5f5f5}
     </style>
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
 </head>
 <body>
 <div class="container-fluid" style="background: #79b4ea; margin-bottom: 20px;">
     <div class="row">
         <div class="col-md-12">
             <a href="{{url('/')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>

         </div>
     </div>
 </div>

 <div class="container-fluid">
     <div class="col-md-12 text-center">
         <div class="jumbotron text-xs-center">
             <h1 class="display-3">{{trans('front.thanks')}}!</h1>
             <p class="lead">{!!Session::get('redirect_message')!!}</p>
             <hr>
         </div>

     </div>
 </div>
 <section class="homefront">
	<div class="container">
		<h2 class="line"><span>حسابي الشخصي</span></h2>
		
		<div class="col-md-12" style="margin-bottom: 20px;">
	      @if(Session::has('flash_message'))
	          <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {!!Session::get('flash_message')!!}
	          </div>
	      @endif
	    </div>

	    <div class="text-center">
	    	{!!Form::open(['url'=>'interact/send-verification-email'])!!}
	    		إذا لم تصلك رسالة التفعيل قم 	    		
	    		{!!Form::submit('بإعادة إرسال رسالة التفعيل', ['class'=>'btn'])!!}
	    	{!!Form::close()!!}
	    </div>
	    <br>
	    <center>
	    <a href="{{url('dashboard/reg')}}">
		<div style="text-align: center;font-size: 35px;" class="btn btn-info">
			{{trans('dash.finish_reg')}}
		</div>
		</a>
		</center>
	</div>
</section>
 {!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
 {!!HTML::script('frontend/js/bootstrap.min.js')!!}
 @yield('script')
 </body></html>