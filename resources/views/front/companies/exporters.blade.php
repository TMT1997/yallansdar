<?php
	use App\Category;
	use App\Product;
    use App\Company;
    use App\Helper;
    use App\User;
?>
@extends('front.layouts.master')
@section('title') @lang('front.exporters') | @endsection
@section('content')
	
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
			</div>
			<div class="col-xs-12 col-md-8">			
				<header><h1 class="hdr-styl-001 inr-hdr-blk">
                    <strong>@lang('front.exporters')</strong></h1>
                </header>
                <div class="row">
                    <div class="col-md-3">
    					<div class="breadcumb-list brcrmb-inr">
    						<ul class=""><li>@lang('front.total_exporters'): {{$exporters->total()}}</li></ul>
    					</div>
                    </div>
                    <div class="col-md-9 exporters_paginate">
                        {!!$exporters->links()!!}
                    </div>
                </div>
                
                <div class="row exporters_list"> 
                    @foreach($exporters as $supplier)
                        @if(isset($supplier->profile))
                        <figure class="col-md-12 item">
                            <div class="col-md-8">
                                <a href="{{User::contact_info($supplier)}}" class="img-lnk-hldr">{!!Helper::image2($supplier->profile->picture,[])!!}</a>
                                <figcaption>
                                    <h4>
                                        <a href="{{User::contact_info($supplier)}}">
                                        {{($supplier->profile->company_name)?$supplier->profile->company_name:$supplier->name}}</a>
                                        <span>{!!User::flag($supplier)!!}</span>
                                    </h4>
                                    @if(!empty($supplier->profile->main_products))
                                        <p><span>{{trans('front.main_products')}}:</span>
                                        {{str_replace(',',', ', str_limit($supplier->profile->main_products, 75))}}</p>
                                    @endif
                                </figcaption>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-unstyled supplier_info">
                                    <li><span>@lang('front.city'): {{$supplier->profile->city}} {{$supplier->profile->state}}</span></li>
                                    <li><a href="{{Company::products_url($supplier)}}" class="btn btn-sm btn-warning">@lang('front.co_products')</a></li>
                                    @if(!empty($supplier->profile->co_brochure))
                                    <li><a class="btn btn-info" href="{{url('uploads/'.$supplier->profile->co_brochure)}}">@lang('front.brochure')</a></li>
                                    @endif
                                    <li><a href="{{User::contact_info($supplier)}}"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a></li>
                                </ul>
                            </div>
                        </figure>
                        @endif
                    @endforeach
                </div>

                <hr>
                {!!$exporters->links()!!}
			</div>
            <aside class="col-xs-12 col-md-2 sponsored_products">
                <header><div class="hdr-styl-asid">@lang('front.latest_products')</div></header>
                @foreach($latest_products as $product)
	                <figure class="asid-prdct-itm">
	                    <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
	                    <figcaption><h4><a href="#">{{Product::name($product,$lng_id)}}</a></h4></figcaption>
	                </figure>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection