@extends('front.layouts.master')

@section('content')
	<div class="container">
		@include('front.companies.contact_details')

		<div class="text-center">
			<hr>
			<a href="{{App\Company::products_url($user)}}" class="grn-btn">{{trans('front.co_products')}}</a>
		</div>
		<br><br>
	</div>
@endsection