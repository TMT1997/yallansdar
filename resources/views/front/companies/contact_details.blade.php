<?php
	use App\Helper;
	use App\USer;

	$profile = $user->profile;
	$flag = User::flag($user,[],true);
	$details = [
		'company_name'=>$profile->company_name,
		'address'=>$profile->address_line1.' '.$profile->address_line2,
		'zipcode'=>$profile->zipcode,
		'city'=>$profile->city,
		'state'=>$profile->state,
		'country'=>$flag,
		'mobile'=>$profile->mobile,
		'phone'=>$profile->phone,
	];
?>

<div class="white-popup-block">
	<h3 class="text-warning text-center">{{trans('front.contact_details')}}</h3>

	<div class="row"> 
		<div class="col-md-12">
			<legend class="text-primary">ممثل الشركة</legend>
			<div class="media">
				<a class="pull-left" href="#">
					{!!Helper::image2($profile->picture, ['class'=>'media-object', 'style'=>'width:120px;'])!!}
				</a>
				<div class="media-body">
					<h4 class="media-heading">{{$profile->first_name}} {{$profile->last_name}}</h4>
					<p>{{$profile->destination}}</p>
					<p>
						@if(isset(trans('dash.departments_arr')[$profile->department]))
							<span>{{trans('front.department')}}</span>: {{trans('dash.departments_arr')[$profile->department]}}
						@endif
					</p>
				</div>
			</div>
		</div>
	</div>


	<div class="row"> 
		<div class="col-md-12">
			<legend class="text-primary">بيانات الشركة</legend>
			<table class="table table-condensed table-bordered table-striped table-hover">
				@foreach($details as $item=>$val)
					<tr>
						<td class="attr">{!!Form::label($item, trans('front.'.$item), ['class'=>'control-label'])!!}</td>
						<td class="val">{!!$val!!}</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>

	<div class="text-center">
		{{-- <a href="{{url('cart')}}">{{trans('front.contact_supplier')}}</a> --}}
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	  $('.popup').magnificPopup({type:'ajax'});
	});
</script>