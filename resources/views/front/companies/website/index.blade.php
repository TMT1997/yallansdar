<?php use App\Product; ?>
@extends('front.companies.website.layouts.master')

@section('content')
<html>
    <head>
        <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400" rel="stylesheet">
                {!!HTML::style('templates/css/style.css')!!}
            </link>
        </link>
    </head>
    <body>
        <div class="banner">
            <div class="topnav">
                <div class="text ttitle">
                    <a href="#">
                        {{$company->company_name}}
                    </a>
                </div>
                <div class="text nav ralign">
                    <a href="#">
                        Home
                    </a>
                    <a href="#">
                        Profile
                    </a>
                    <a href="#">
                        Products
                    </a>
                    <a href="#">
                        Contact Us
                    </a>
                </div>
            </div>
            <h1>
                Welcome to {{$company->company_name}}
            </h1>
            <p>
                {{$company->brief}}
            </p>
        </div>
        <div class="box">
            <div class="box3">
                <h1>
                    {{$company->main_products}}
                </h1>
                <p>
                    Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu.
                </p>
                <a class="More" href="#">
                    More
                </a>
            </div>
            <div class="box3 line">
                <h1>
                 	{{$company->main_products}}
                </h1>
                <p>
                    Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu.
                </p>
                <a class="More" href="#">
                    More
                </a>
            </div>
            <div class="box3 line">
                <h1>
                    {{$company->main_products}}
                </h1>
                <p>
                    Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu.
                </p>
                <a class="More" href="#">
                    More
                </a>
            </div>
        </div>
        <div class="boxp">
            <div class="title">
                <h1>
                    Products
                </h1>
                <p>
                    {{$company->main_products}}
                </p>
            </div>
            @foreach($company->user->products->take(4) as $product)
            <div class="boxphoto">
                {{Product::image($product, ['class'=>'img-responsive'])}}
                    <h3>
                        {{Product::name($product)}}
                    </h3>
                    <p>
                        {{Product::name($product)}}
                    </p>
                </img>
            </div>
            @endforeach
        </div>
        <div class="box2p">
            <div class="title">
                <h1>
                    {{$company->company_name}}
                </h1>
                <p>
                    Aliquam erat volutpat nam dui
                </p>
            </div>
            <div class="pbox">
                <div class="photo">
                    <img src="https://templated.co/items/demos/theory/images/pic01.jpg" width="100%">
                        <h3>
                            Praesent placerat magna
                        </h3>
                        <p>
                            Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor lorem ipsum.
                        </p>
                        <a class="More" href="#">
                            More
                        </a>
                    </img>
                </div>
                <div class="photo">
                    <img src="https://templated.co/items/demos/theory/images/pic02.jpg" width="100%">
                        <h3>
                            Praesent placerat magna
                        </h3>
                        <p>
                            Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna non comodo sodales tempus.
                        </p>
                        <a class="More" href="#">
                            More
                        </a>
                    </img>
                </div>
            </div>
            <hr class="line">
            </hr>
        </div>
        <div class="last">
            <div class="combo lalign">
                <p>
                    © Untitled. Design: SIIT. Images: Unsplash.
                </p>
            </div>
            <div class="combo ralign social">
                <a href="#">
                    <i aria-hidden="true" class="fa fa-facebook ">
                    </i>
                </a>
                <a href="#">
                    <i aria-hidden="true" class="fa fa-twitter ">
                    </i>
                </a>
                <a href="#">
                    <i aria-hidden="true" class="fa fa-linkedin ">
                    </i>
                </a>
                <a href="#">
                    <i aria-hidden="true" class="fa fa-pinterest-p ">
                    </i>
                </a>
                <a href="#">
                    <i aria-hidden="true" class="fa fa-vimeo ">
                    </i>
                </a>
            </div>
        </div>
    </body>
</html>
@endsection
