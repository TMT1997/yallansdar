<!DOCTYPE html>
<html>
<head>
	<title>Supplier Website</title>
	{!!HTML::style("frontend/css/bootstrap.css")!!}
    {!!HTML::style("frontend/css/font-awesome.min.css")!!}
</head>
<body>

<div class="container">
	@yield('content')
</div>

{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
{!!HTML::script('frontend/js/jquery-ui.min.js')!!}
{!!HTML::script('frontend/js/bootstrap.js')!!}
</body>
</html>