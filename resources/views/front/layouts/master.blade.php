<?php
use App\Product;
use App\Helper;
$cart = Request::session()->get('yalla_cart');
?>
        <!doctype html>
<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') @lang('front.site_title')</title>
    <meta name="description" content="@yield('meta_desc', trans('front.about_yallansadar'))" />
    <link rel="shortcut icon" href="{{url('frontend/images/logo/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{url('frontend/images/logo/favicon.ico')}}" type="image/x-icon">
    <meta name="google-site-verification" content="7I0pJuhlFFmP11bZLdw62D9DfWvluJl0saz91Cx9UqU" />
    @yield('fb_meta')
    {{HTML::style("frontend/css/bootstrap.min.css")}}
    {{HTML::style("frontend/css/pgwslider.min.css")}}
    {{HTML::style("frontend/css/fotorama.css")}}
    {{HTML::style("frontend/css/owl.carousel.css")}}
    {{HTML::style("frontend/css/owl.theme.css")}}
    {{HTML::style("frontend/css/font-awesome.min.css")}}
    {{HTML::style("frontend/css/style.css")}}
    {{HTML::style("frontend/css/custom.css")}}
    {{HTML::script('frontend/js/jquery-1.11.2.min.js')}}
    {{HTML::script('frontend/js/bootstrap.min.js')}}
    {{HTML::style('frontend/products_viewer/css/smoothproducts.css')}}

    <style>
        .btn.active.focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn:active:focus, .btn:focus {
            outline: 0;
        }
    </style>

    <script>
        $( function() {
            $("#tabs-01, #tabs-00, #tabs02").tabs();
        });
    </script>
    @if(App::isLocale('ar'))
        {!!HTML::style('backend/dist/css/bootstrap-rtl.min.css')!!}
        {!!HTML::style('frontend/css/rtl.css')!!}
    @endif
</head>
<body>
<a href="#" class="to-tp cd-top"><span class="glyphicon glyphicon-chevron-up"></span></a>
<main>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container nvFx">
            <div class="row" >
                <div class="col-md-2">

                    <div class="row" style="padding-top: 14px;">

                        <div class="col-xs-4 strips_menu_col">
                            <div style="margin-top: 10px;">
                                <button class="btn btn-default btn-sm" id="toggle-aside" style="border: none;padding: 0px 10px;font-size: 30px;line-height: 0.2;border-radius: 3px;background-color: inherit;">
                                    <i class="fa fa-bars"></i>
                                </button>
                            </div>
                        </div>

                        <div class="col-xs-8" style="padding-right: 0;">
                            <div class="navbar-header" style="width: 130%;">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#yalla_hdnav" aria-expanded="false" style="margin-top: 10px;width: 32px;height: 26px;">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="{{url('/')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    @include('front.layouts.nav_tabs')
                    <div id="tabs-00" class="tabs-00 header_drops hidden">
                        <div class="swap-hid-area">
                            <ul>
                                <li><a href="#srch-tabs-1"><span>@lang('front.products')</span></a></li>
                                <li><a href="#srch-tabs-2"><span>@lang('front.suppliers')</span></a></li>
                                <li><a href="#srch-tabs-3"><span>@lang('front.deals')</span></a></li>
                            </ul>
                            <div class="collapse navbar-collapse" id="yalla_hdnav">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>@lang('front.welcometo'),</li>
                                    @if (Auth::guest())
                                        <li class="auth_nav">
                                            <a href="{{url('register')}}">@lang('front.joinus')</a> |
                                            <a href="{{url('login')}}">@lang('front.signin')</a>
                                        </li>
                                    @else
                                        <li>{{Auth::user()->name}}</li>
                                    @endif
                                    <li class="dropdown">
                                        <a href="#" id="buy-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('front.buy') <span class="caret"></span></span></a>
                                        <ul class="dropdown-menu" aria-labelledby="buy-mnu">
                                            <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.post_buying_req')</a></li>
                                            <li><a href="{{url('dashboard/alerts/create')}}">@lang('front.create_deal_alert')</a></li>
                                            <li><a href="{{url('category')}}">@lang('front.find_products')</a></li>
                                            <li><a href="{{url('deals')}}">@lang('front.find_new_deals')</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" id="sell-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('front.sell')}} <span class="caret"></span></span></a>
                                        <ul class="dropdown-menu" aria-labelledby="sell-mnu">
                                            <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.new_rfqs')</a></li>
                                            <li><a href="{{url('dashboard/products/create')}}">@lang('front.add_products')</a></li>
                                            <li><a href="{{url('dashboard/sell_offer/create')}}">@lang('front.post_sell_offer')</a></li>
                                            <li><a href="{{url('dashboard/partnerships/create')}}">@lang('front.post_partnership_deal')</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" id="my-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('front.myyalla') <span class="caret"></span></a>
                                        <ul class="dropdown-menu" aria-labelledby="my-mnu">
                                            <li><a href="{{url('dashboard')}}">@lang('front.mydashboard')</a></li>
                                            <li><a href="{{url('dashboard/messages')}}">@lang('front.mymessages')</a></li>
                                            <li><a href="{{url('dashboard/favorites')}}">@lang('front.myfavorites')</a></li>
                                            @if(Auth::check())
                                                <li><a href="{{url('logout')}}">@lang('front.logout')</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                                        <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                                            @foreach ($languages as $lang)
                                                <?php $sel=(Session::get('yalla_locale')==$lang->iso2)?'selected="selected"':''; ?>
                                                <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="cntn-tbs-hldr">
                            <div class="crt-area">
                                <a href="#d" class="crt-btn" id="crt-btn" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> @lang('front.cart') <span class="badge">{{Product::cart_items($cart)}}</span></a>
                                <ul class="dropdown-menu">
                                    @if($cart && !empty($cart))
                                        @foreach($cart as $user_id=>$products)
                                            @foreach($products as $product)
                                                <li>
                                                    <figure class="row">
                                                        <div class="col-md-12">
                                                            <a href="{{url('cart/remove/'.$product->id)}}" class="rmov-itm-frm-crt"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                            <a href="{{Product::url($product)}}" class="img-lnk-hldr">{!!Helper::image2($product->image)!!}</a>
                                                            <figcaption>
                                                                <a href="{{Product::url($product)}}">{{Helper::field($product,'name')}}</a>
                                                            </figcaption>
                                                        </div>
                                                    </figure>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    @endif
                                    <li><a href="{{url('cart')}}" class="btn btn-success">@lang('front.contact_supplier')</a></li>
                                </ul>
                                <a href="{{url('dashboard/request_quotes/create')}}" class="pst-by-btn">@lang('front.post_buying_req')</a>
                            </div>
                            <div id="srch-tabs-1" class="srch-tbs-cntnt">
                                <div>
                                    {!!Form::open(['url'=>'search/products', 'method'=>'GET'])!!}
                                    <button type="submit" class='mn-srch-btn'><i class="fa fa-search"></i></button>
                                    {!!Form::text('q',null, ['class'=>'Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_prods')])!!}
                                    {!!Form::close()!!}
                                </div>
                            </div>
                            <div id="srch-tabs-2" class="srch-tbs-cntnt">
                                <div>
                                    {!!Form::open(['url'=>'search/suppliers', 'method'=>'GET'])!!}
                                    <button type="submit" class='mn-srch-btn'><i class="fa fa-search"></i></button>
                                    {!!Form::text('q',null, ['class'=>'Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_supp')])!!}
                                    {!!Form::close()!!}
                                </div>
                            </div>
                            <div id="srch-tabs-3" class="srch-tbs-cntnt">
                                <div>
                                    {!!Form::open(['url'=>'search/deals', 'method'=>'GET'])!!}
                                    <button type="submit" class='mn-srch-btn'><i class="fa fa-search"></i></button>
                                    {!!Form::text('q',null, ['class'=>'Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_deals')])!!}
                                    {!!Form::close()!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Menu Header under search-->
                    <div class="collapse navbar-collapse" id="yalla_hdnav">
                        @if(App::isLocale('en'))
                            <ul class="nav navbar-nav navbar-left">
                                @else
                                    <ul class="nav navbar-nav navbar-right">
                                        @endif
                                        <li class="dropdown">
                                            <a href="{{url('site')}}">@lang('front.about_us')</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" id="buy-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('dash.services') <span class="caret"></span></span></a>
                                            <ul class="dropdown-menu" aria-labelledby="buy-mnu">
                                                <li><a href="{{url('site/trade-manager')}}">@lang('front.trade_manager')</a></li>
                                                <li><a href="{{url('site/join-us')}}">@lang('dash.work_provider')</a></li>
                                                <li><a href="{{url('site/pick-describe')}}">@lang('front.pick_describe')</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="{{url('upgrade')}}">@lang('front.packages')</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="{{url('blog')}}">@lang('front.blog')</a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="{{url('site/contact')}}">@lang('front.contact_us')</a>
                                        </li>
                                    </ul>
                    </div>
                    <!--End Menu Header-->
                    <!---->

                    <!---->
                </div>
            </div>
        </div>
    </nav>
    <div class="">
        <div class="col-md-12">
            @if(Session::has('flash_message'))
                <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!!Session::get('flash_message')!!}
                </div>
            @endif
        </div>
    </div>
    <div style="padding-top: 20px;">
        @yield('content')
    </div
    <footer class="main-footer">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4 folow-us">
                        <span>@lang('front.share_us_on')</span>
                        <div style="display: inline-block;" class="addthis_inline_share_toolbox"></div>
                    </div>
                    <div class="col-xs-12 col-md-8 subscribe_form">
                        @include('front.forms.subscribe_form')
                    </div>
                </div>
            </div>
        </div>
        <div class="midl-fotr">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <article class="col-xs-6 col-md-3">
                            <header><h5>{{trans('front.get_to_know')}}</h5></header>
                            <ul>
                                <li><a href="{{url('site')}}">@lang('front.about_us')</a></li>
                                <li><a href="{{url('site/join-us')}}">@lang('front.join_us')</a></li>
                                <li><a href="{{url('site/trade-manager')}}">@lang('front.trade_manager')</a></li>
                                <li><a href="{{url('site/contact')}}">@lang('front.contact_us')</a></li>
                            </ul>
                        </article>
                        <article class="col-xs-6 col-md-3">
                            <header><h5>@lang('front.selling')</h5></header>
                            <ul>
                                <li><a href="{{url('dashboard/sell_offer/create')}}">@lang('front.post_sell_offer')</a></li>
                                <li><a href="{{url('dashboard')}}">@lang('front.create_website')</a></li>
                                <li><a href="{{url('dashboard/products/create')}}">@lang('front.add_products')</a></li>
                                <li><a href="{{url('dashboard/alerts/create')}}">@lang('front.create_deal_alert')</a></li>
                            </ul>
                        </article>
                        <article class="col-xs-6 col-md-3">
                            <header><h5>{{trans('front.buying')}}</h5></header>
                            <ul>
                                <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.post_buying_req')</a></li>
                                <li><a href="{{url('dashboard/alerts/create')}}">@lang('front.create_deal_alert')</a></li>
                                <li><a href="{{url('category')}}">@lang('front.find_products')</a></li>
                                <li><a href="{{url('deals')}}">@lang('front.find_new_deals')</a></li>
                            </ul>
                        </article>
                        <article class="col-xs-6 col-md-3">
                            <header><h5>@lang('front.lets_help')</h5></header>
                            <ul>
                                <li><a href="{{url('site/contact')}}">@lang('front.contact_us')</a></li>
                                <li><a href="#">@lang('front.adv_with_us')</a></li>
                                <li><a href="{{url('testimontials')}}">@lang('front.feedback')</a></li>
                                <li><a href="#">@lang('front.faq')</a></li>
                            </ul>
                        </article>
                        <article class="col-xs-12 col-md-12 sml-abut">
                            <p>{!!trans('front.about_yallansadar')!!}</p>
                        </article>
                    </div>
                    <article class="col-xs-12 col-md-2 footer_cats">
                        <header><h5>{{trans('front.categories')}}</h5></header>
                        <ul>
                            @foreach($categories_footer as $category)
                                <li><a href="{{App\Category::url($category)}}">{{$category->$c_name}}</a></li>
                            @endforeach
                            <li><a href="{{url('category')}}">@lang('front.more_dots')</a></li>
                        </ul>
                    </article>
                    <div class="col-xs-12 col-md-2">
                        <article>
                            <header><h5>@lang('front.follow_us')</h5></header>
                            <div class="social_follow_us">
                                @foreach(App\Settings::$social_arr as $social=>$icon)
                                    @if(isset($settings['social']) && App\Settings::fetch($social, $settings['social']))
                                        <a href="{{App\Settings::fetch($social, $settings['social'])}}" class="{{$social}}-ico" target="_blank"><i class="fa fa-{{$icon}}-square"></i></a>
                                    @endif
                                @endforeach
                            </div>

                            <header><h5>@lang('front.yallansadar_mobapp')</h5></header>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="{{url('mobileapps')}}" class="andrid-app"></a>
                                    <a href="{{url('mobileapps')}}" class="ios-app"></a>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="fotr-fotr">
            <div class="container">
                <div class="row">
                    <ul class="col-xs-12 col-md-6 trms-lst">
                        <li><a href="{{url('terms')}}">@lang('front.terms')</a></li>
                        <li><a href="{{url('privacy-policy')}}">@lang('front.privacy_policy')</a></li>
                    </ul>
                    <p class="col-xs-12 col-md-6 copyright-txt">@lang('front.copyrights')</p>
                </div>
            </div>
        </div>
    </footer>
</main>
<div class="floating-form" id="contact_form">
    <div class="contact-opener" id="contact">{{trans('front.feedback')}}</div>
    <div id="contact_results"></div>
    @include('front.forms.feedback_form')
</div>

{{HTML::style("frontend/css/dataTables.bootstrap.min.css")}}
{{HTML::script('frontend/js/jquery-ui.min.js')}}
{{HTML::script('frontend/js/owl.carousel.min.js')}}
{{HTML::script('frontend/js/fotorama.js')}}
{{HTML::script('frontend/js/back-to-top.js')}}
{{HTML::script('frontend/js/pgwslider.min.js')}}
{{HTML::script('frontend/js/jquery.dataTables.min.js')}}
{{HTML::script('frontend/js/dataTables.bootstrap.min.js')}}
{{HTML::script('frontend/products_viewer/js/smoothproducts.min.js')}}
{{HTML::script('frontend/js/iframeResizer.contentWindow.min.js')}}
<script>
    $(document).ready(function() {
            <?php $cform_class = (App::getLocale()=='ar')?'left':'right'; ?>
        var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $("#contact");
        _floatbox.css("{{$cform_class}}", "-320px"); //initial contact form position
        _floatbox_opener.click(function(){
            if (_floatbox.hasClass('visiable')){
                _floatbox.animate({"{{$cform_class}}":"-319px"}, {duration: 100}).removeClass('visiable');
            }else{
                _floatbox.animate({"{{$cform_class}}":"0px"},  {duration: 100}).addClass('visiable');
            }
        });
        $(window).scroll(function(){
            if(_scroll){
                _floatbox.animate({"top": "30px"},{duration: 100});
                _scroll = false;
            }
            if(_timer !== false){ clearTimeout(_timer); }
            _timer = setTimeout(function(){_scroll = true;
                _floatbox.animate({"top": "10px"},{easing: "linear"}, {duration: 200});}, 400);
        });


        var __scroll = true, __timer = false, __float2 = $("#claculator"), _float_opener2 = $("#open");
        __float2.css("{{$cform_class}}", "-290px"); //initial contact form position
        _float_opener2.click(function(){
            if (__float2.hasClass('visiable')){
                __float2.animate({"{{$cform_class}}":"-290px"}, {duration: 100}).removeClass('visiable');
            }else{
                __float2.animate({"{{$cform_class}}":"0px"},  {duration: 100}).addClass('visiable');
             }
        });

        $(window).scroll(function(){
            if(__scroll){
                __float2.animate({"top": "30px"},{duration: 100});
                __scroll = false;
            }
            if(__timer !== false){ clearTimeout(__timer); }
            __timer = setTimeout(function(){__scroll = true;
            __float2.animate({"top": "215px"},{easing: "linear"}, {duration: 200});}, 400);
        });

        $(".owl-itms01").owlCarousel({
            /*rtl:true,*/
            lazyLoad : true,
            nav : true,
            items : 5,
            navText: ["",""],
            dots:false,
            loop: true,
            scrollPerPage : true,
            rewindNav : true,
            responsive:{
                320: { items:1 },
                768: { items:3 },
                1024: { items:5 }
            }
        });
    });
    var $crntscrolamount =0;
    $( window ).scroll(function() {
        if ($( window ).scrollTop() > 150 ) {
            if (!$( ".tabs-00 > .swap-hid-area" ).hasClass('hdn')) {
                $( ".tabs-00 > .swap-hid-area" ).css( "display", "inline" ).fadeOut( "slow" ).addClass('hdn');
                $('main').css('padding-top', '94px');
                $(" .crt-area ul.dropdown-menu ").css("top", "49px");
            }
        }else{
            $( ".tabs-00 > .swap-hid-area" ).css( "display", "block" ).fadeIn( "slow" ).removeClass('hdn');
            $('main').css('padding-top', '94px');
            $(" .crt-area ul.dropdown-menu ").css("top", "79px");
        }
        $crntscrolamount = $( window ).scrollTop();
    });
    $(".lvl-one-mnu-itm ").hover(function() {
        $sideMnuWdth = $('.sid-mnu').outerWidth();
        $('ul.sub-lvl-two-itm').css('left', $sideMnuWdth-10);
    });
    $(".fedbk-btn").click(function(){
        $( ".fedbk-bx" ).toggleClass("shwFedBk");
    });
    $(document).mouseup(function (e){
        var container = $(" .fedbk-bx ");
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) {
            $( ".fedbk-bx" ).removeClass( "shwFedBk" );
        }
    });
    $(document).ready(function() {
        $('.pgwSlider').pgwSlider({
            listPosition: 'right',
            transitionEffect: 'fading',
            selectionMode:'mouveOver',
            displayControls:false,
            adaptiveHeight:true,
            verticalCentering:true
        });
    });
    $(document).ready(function() {
        var table = $('#data-tabl-tbl').DataTable({
            /*"pagingType": "simple",*/
            /*"language": {
                "lengthMenu": "Kisplay _MENU_ Results",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            },*/
            "oLanguage": {
                "oPaginate": {
                    "sNext": ">",
                    "sPrevious": "<",
                    "sFirst": "<<",
                    "sLast": ">>"
                }
            },
            "dom": '<"top"<"clear">>rt<"bottom"p<"grd-srt-btn"><"rows-srt-btn actv-grid">>',
            "pagingType": "full_numbers",
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "searching": false
            /*,
            "dom": '<"top"i>rt<"bottom"flp><"clear">'*/
        });
        if(null !== table) {
            var info = table.page.info();
        }
        function offFun(){
            if(null !== table) {
                $('.pag-stt-info').html(
                    //(table.page.info().page+1)+' of '+table.page.info().pages+' : Pages'
                );
            }
        };
        $('.dataTables_wrapper .bottom').click( offFun );
        offFun();

        $('.custom-show-slct, #data-tabl-tbl_length select').change(function() {
            var $showamount = $(this).find(':selected').val();
            table.page.len( $showamount ).draw();
            $(".custom-show-slct").val($showamount);
            offFun();
        });

        $('#next-bttn').on('click', function() {
            table.page('next').draw( 'page' );
            offFun();
            if( (table.page.info().page+1) == table.page.info().pages ){
                $(this).css('opacity', 0.4);
                $('#prv-bttn').css('opacity', 1);
            }else{
                $(this).css('opacity', 1);
                $('#prv-bttn').css('opacity', 1);
            }
        });
        $('#prv-bttn').on( 'click', function () {
            table.page( 'previous' ).draw( 'page' );
            offFun();
            if( (table.page.info().page+1) == 1 ){
                $(this).css('opacity', 0.4);
                $('#next-bttn').css('opacity', 1);
            }else{
                $(this).css('opacity', 1);
                $('#next-bttn').css('opacity', 1);
            }
        });
        $('.grd-srt-btn').on( 'click', function () {
            $(' .grd-srt-btn ').addClass('actv-grid');
            $(' .rows-srt-btn ').removeClass('actv-grid');
            $('.table.dataTable').addClass('gly-vuo');
        });
        $('.rows-srt-btn').on( 'click', function () {
            $(' .rows-srt-btn ').addClass('actv-grid');
            $(' .grd-srt-btn ').removeClass('actv-grid');
            $('.table.dataTable').removeClass('gly-vuo');
        });
    });
    $('.btn-loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
    function addToCart($id)
    {
        $product_id = $id;
        $.ajax({
            url: '{{url('cart')}}',
            type: 'POST',
            data: {product_id: $product_id, _token:'{{csrf_token()}}'},
            beforeSend:function() {
            }
        }).done(function(response) {
            var res = jQuery.parseJSON(response);
            if(res.status==1) {
                alert('Product added successfully');
            } else {
                alert('Product not added');
            }
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }
    $(document).ready(function() {
        $(document).on('click', '.btn_quickAddReview', function(event) {
            event.preventDefault();
            var $rate = $('#rev_form input[name="rate"]');
            var $rating = $('#rev_form .rating');
            var $subject = $('#rev_form input[name="subject"]');
            var $body = $('#rev_form textarea[name="body"]');

            if(!$rate.is(':checked')) {
                $rating.find('.error').html('من فضلك اختر مستوى التقييم');
                $rating.find('.error').addClass('text-danger');
                $rating.addClass('has-error'); return false;
            } else {
                $rating.removeClass('has-error'); $rating.find('.error').html('');
            }
            if($subject.val()=='') {
                $subject.siblings('.error').html('ادخل عنوان التقييم');
                $subject.siblings('.error').addClass('text-danger');
                $subject.parent('div').addClass('has-error'); $subject.focus(); return false;
            } else {
                $subject.removeClass('has-error'); $subject.siblings('.error').html('');
            }
            if($body.val()=='') {
                $body.siblings('.error').html('ادخل محتوى التقييم');
                $body.siblings('.error').addClass('text-danger');
                $body.parent('div').addClass('has-error'); $body.focus(); return false;
            } else {
                $body.removeClass('has-error'); $body.siblings('.error').html('');
            }
            var $btn = $(this);
            $.ajax({
                url: '{{url('reviews/add')}}',
                type: 'POST',
                data: $('#rev_form').serialize(),
                beforeSend:function(){
                    //$btn.button('loading');
                }
            })
                .done(function(response) {
                    var res = $.parseJSON(response);
                    if(res.status=='success') {
                        $('#rev_form').html('<div class="alert alert-success">{{trans('front.review_added')}}</div>');
                    } else {
                        $('#rev_form').html('<div class="alert alert-danger">Error adding Review</div>');
                    }
                }).fail(function() {
                $('#rev_form').html('<div class="alert alert-danger">Unknown Error while adding Review</div>');
            })
                .always(function() {
                    $btn.button('reset');
                });
        });
    });
    $(".dropdown").hover(
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).hide();
            $(this).toggleClass('open');
        }
    );
    $(window).load(function() {
        $('.sp-wrap').smoothproducts();
    });
    $(document).on('submit', '.quicklogin_form', function(event) {
        event.preventDefault();

        $.ajax({
            url: '{{url('interact/login')}}',
            type: 'POST',
            data: $('.quicklogin_form').serialize(),
            beforeSend: function() {
                $('.logbtn').attr({
                    disabled: 'disabled',
                });
            }
        }).done(function(response) {
            var output = jQuery.parseJSON(response);
            $('#login_msg').html(output.message);

            if(output.status=='success') {
                $('#login_msg').addClass('alert alert-success');

                //next step if exists
                if(output.next!='') {
                    if(output.next=='reload') {
                        window.location.reload();
                    }
                    if(output.next!='unlock') {
                        $.magnificPopup.open({
                            items: {
                                src: '{{url('interact/unlock')}}?ref='+output.ref+'&id='+output.id
                            },
                            type: 'ajax'
                        });
                    }
                } else {
                    setTimeout(function() {
                        $.magnificPopup.close();
                    }, 1000);
                }
            } else {
                $('#login_msg').addClass('alert alert-danger');
            }
        }).fail(function() {
            $('#login_msg').addClass('alert alert-danger');
            $('#login_msg').html('Error occured');
        }).always(function() {
            $('.logbtn').removeAttr('disabled');
        });

        return false;
    });

    $(document).on('submit', '.locker_form', function(event) {
        event.preventDefault();

        $.ajax({
            url: '{{url('interact/unlock')}}',
            type: 'POST',
            data: $('.locker_form').serialize(),
            beforeSend: function() {
                $('.locker_form button[type="submit"]').attr({
                    disabled: 'disabled',
                });
            }
        }).done(function(response) {
            var output = jQuery.parseJSON(response);
            $('#locker_msg').html(output.message);

            if(output.status=='success') {
                $('#locker_msg').removeClass('alert alert-danger');
                $('#locker_msg').addClass('alert alert-success');

                setTimeout(function() {
                    $('.locked').css('display', 'block');
                    $('.unlock_act').slideUp();
                    $.magnificPopup.close();
                }, 1000);

            } else {
                $('#locker_msg').addClass('alert alert-danger');
            }
        }).fail(function() {
            $('#locker_msg').addClass('alert alert-danger');
            $('#locker_msg').html('Error occured');
        }).always(function() {
            $('.locker_form button[type="submit"]').removeAttr('disabled');
        });

        return false;
    });
</script>
@yield('script')
@yield('script2')
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-73755643-1', 'auto');
    ga('send', 'pageview');
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58d27689eade3904"></script>
</body>
</html>
