	<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') {{trans('front.site_title')}}</title>
    {{HTML::style("frontend/css/bootstrap.min.css")}}
    {{HTML::style('frontend/css/font-awesome.min.css')}}
    {{HTML::style('frontend/css/custom.css')}}
    @if(App::isLocale('ar'))
        {{HTML::style('backend/dist/css/bootstrap-rtl.min.css')}}
        {{HTML::style('frontend/css/rtl.css')}}
    @endif
    <style type="text/css">
    	body {background: #f0f0f0}
    </style>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container">
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<a href="{{url('/')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
				</div>

				<div class="col-md-12">@yield('content')</div>
			</div>
		</div>
	</div>
	{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
	{!!HTML::script('frontend/js/bootstrap.min.js')!!}
@yield('script')
</body></html>