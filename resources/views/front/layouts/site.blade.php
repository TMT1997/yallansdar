<!doctype html>
<html class="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">	
	<title>@yield('title') يلا نصدر</title>
	<link rel="shortcut icon" href="images/favicon.png">
	<meta name="google-site-verification" content="7I0pJuhlFFmP11bZLdw62D9DfWvluJl0saz91Cx9UqU" />
	{!!HTML::style("frontend/css/bootstrap.min.css")!!}
	{!!HTML::style("frontend/css/font-awesome.min.css")!!}
	{!!HTML::style('frontend/site/css/components.css')!!}
	{!!HTML::style('frontend/site/css/style.css')!!}
	{!!HTML::style('frontend/site/css/media.css')!!}    
    @if(App::isLocale('en'))
		<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    @else
    	{!!HTML::style('backend/dist/css/bootstrap-rtl.min.css')!!}
    @endif
	<style type="text/css">
		input.error {border:1px solid red; color: red}
		.error {color: red;}
		label.error {margin-top:5px;}
		.join-ico {text-align: center; padding-bottom: 10px;}
		.join-ico i.fa {font-size: 40px; color: #f0f0f0}
		.join-ico.checks i.fa {color: #47D69D}
		.offer_box {background: #f9f9f9; border:1px solid #f0f0f0; color: #666; padding:20px 5px; min-height: 245px; font-size: 16px; border-radius:10px;}
		.offer_box:hover { background: #80aa9b; color: #fff; }
		.offer_box span {font-size: 13px; color: #999}
		.offer_box:hover span {color: #f0f0f0}
		.headtitle {background: #00509F; padding:20px 0; font-size: 30px; color: #F8AF20; margin-bottom: 10px;}
		.subheader a {font-size: 12px;}
		.subheader .navbar-nav>li>a {padding-top: 8px; padding-bottom: 8px;}
		.header-section {padding-top: 15px;}
		.header-section .navbar-nav>li {float: right;}
	</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5/html5shiv.min.js"></script>
      <script src="js/html5/respond.min.js"></script>
    <![endif]-->
</head>
<body data-offset="200" data-spy="scroll" data-target=".primary-navigation">
	<a id="top"></a>
	<div class="container-fluid subheader" style="background: #f0f0f0">
		<div class="container">
			<ul class="nav navbar-nav nav-left">
				@if(Auth::check())
					<li class="dropdown">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		                    {{Auth::user()->name}} <span class="caret"></span>
		                </a>
		                <ul class="dropdown-menu" role="menu">
		                    <li><a href="{{url('dashboard')}}"><i class="fa fa-user"></i> @lang('dash.my_profile')</a></li>
		                    <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> @lang('front.logout')</a></li>
		                </ul>
		            </li>
		        @else
		        	<li><a href="{{url('login')}}">@lang('front.login')</a></li>
		        @endif

		        <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                        @foreach ($languages as $lang)
                            <?php $sel=(Session::get('yalla_locale')==$lang->iso2)?'selected="selected"':''; ?>
                            <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
			</ul>
		</div>
	</div>
	<header id="header-section" class="header-section">
		<div class="logo-block">
			<div class="container">
				<div class="row">
					<div class="col-md-3" style="float: right;">
						<a title="Logo" href="{{url('/')}}">{{HTML::image('frontend/images/logo/logo-ar.png')}}</a>
					</div>
					<div class="col-md-9 pull-left" style="margin-top: 15px">
						<ul class="nav navbar-nav navbar-left">
							<li class="dropdown">
                                <a href="{{url('site')}}">@lang('front.about_us')</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" id="buy-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('dash.services') <span class="caret"></span></span></a>
                                <ul class="dropdown-menu" aria-labelledby="buy-mnu">
                                    <li><a href="{{url('site/trade-manager')}}">@lang('front.trade_manager')</a></li>
                                    <li><a href="{{url('site/join-us')}}">@lang('dash.work_provider')</a></li>
                                    <li><a href="{{url('site/pick-describe')}}">@lang('front.pick_describe')</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="{{url('upgrade')}}">@lang('front.packages')</a>
                            </li>
                            <li class="dropdown">
                                <a href="{{url('blog')}}">@lang('front.blog')</a>
                            </li>
                            <li class="dropdown">
                                <a href="{{url('site/contact')}}">@lang('front.contact_us')</a>
                            </li>
					   	</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	@if(Session::has('flash_message'))
		<div class="alert alert-{{Session::get('alert', 'success')}}">
			{{Session::get('flash_message')}}
		</div>
	@endif
	@yield('content')
	<footer id="footer-section" class="footer-section ow-background">
		<div class="container">
			<div class="footer-bottom" style="margin-top: 15px;padding: 10px 0">				
				<div class="col-md-6" style="float: right;text-align: right">
					<aside class="widget widget_social row">
						<p style="text-align: right;display: inline-block">@lang('front.copyrights')</p>
						<a href="{{url('terms')}}">@lang('front.terms')</a> |
                    	<a href="{{url('privacy-policy')}}">@lang('front.privacy_policy')</a>
					</aside>
				</div>				
				<div class="col-md-6">
					<aside class="widget widget_social row" style="float: left">
						<ul>
							<li><a title="Facebook" href="https://www.facebook.com/yallansadar" target="blank"><i class="fa fa-facebook"></i></a></li>
							<li><a title="Twitter" href="https://twitter.com/yallansadar" target="blank"><i class="fa fa-twitter"></i></a></li>
							<li><a title="Google Plus" href="https://plus.google.com/114404097260213152411" target="blank"><i class="fa fa-google-plus"></i></a></li>
							<li><a title="Linked In" href="https://www.linkedin.com/in/yallansadar" target="blank"><i class="fa fa-linkedin-square"></i></a></li>
						</ul>
					</aside>
				</div>
			</div>			
		</div>
	</footer>
	{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
	{!!HTML::script('frontend/js/bootstrap.min.js')!!}
	<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false'></script>
	{{-- <script src="libraries/jquery.easing.min.js"></script>
	<script src="libraries/gmap/jquery.gmap.min.js"></script>
	<script src="libraries/jquery.knob.js"></script> --}}
	@yield('script')
</body></html>