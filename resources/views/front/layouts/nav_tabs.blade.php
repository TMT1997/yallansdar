<?php
use App\Product;
use App\Helper;
?>
<div role="tabpanel tabs-00" class="tabs_new">
    <div style="float: left; ">
        <div class="collapse navbar-collapse" id="yalla_hdnav">
            <ul class="nav navbar-nav navbar-right">
                {{--<li>@lang('front.welcometo'),</li>--}}
                @if (Auth::guest())
                    <li>@lang('front.welcometo'),</li>
                    <li class="auth_nav">
                        <a href="{{url('register')}}">@lang('front.joinus')</a> |
                        <a href="{{url('login')}}">@lang('front.signin')</a>
                    </li>
                @else

                @endif
                
                <li class="dropdown">
                    <a href="#" id="buy-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('front.buy') <span class="caret"></span></span></a>
                    <ul class="dropdown-menu" aria-labelledby="buy-mnu">
                        <li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.post_buying_req')</a></li>
                        {{--<li><a href="{{url('dashboard/alerts/create')}}">@lang('front.create_deal_alert')</a></li>--}}
                        {{--<li><a href="{{url('category')}}">@lang('front.find_products')</a></li>--}}
                        {{--<li><a href="{{url('deals')}}">@lang('front.find_new_deals')</a></li>--}}
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" id="sell-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('front.sell')}} <span class="caret"></span></span></a>
                    <ul class="dropdown-menu" aria-labelledby="sell-mnu">
                        {{--<li><a href="{{url('dashboard/request_quotes/create')}}">@lang('front.new_rfqs')</a></li>--}}
                        <li><a href="{{url('product/add')}}">@lang('front.add_products')</a></li>
                        {{--<li><a href="{{url('dashboard/sell_offer/create')}}">@lang('front.post_sell_offer')</a></li>--}}
                        {{--<li><a href="{{url('dashboard/partnerships/create')}}">@lang('front.post_partnership_deal')</a></li>--}}
                    </ul>
                </li>

                {{--My Account Menu--}}
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" id="my-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('front.welcometo'), {{Auth::user()->name}} <span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="my-mnu">
                            <li><a href="{{url('dashboard')}}">@lang('front.mydashboard')</a></li>
                            <li><a href="{{url('dashboard/profiles/personal')}}">@lang('front.my_profile')</a></li>
                            <li><a href="{{url('dashboard/settings')}}">@lang('front.my_settings')</a></li>
                            <li><a href="{{url('logout')}}">@lang('front.logout')</a></li>
                        </ul>
                    </li>
                @else
                    {{--<li class="dropdown">--}}
                    {{--<a href="#" id="my-mnu" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('front.myyalla') <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu" aria-labelledby="my-mnu">--}}
                    {{--<li><a href="{{url('dashboard')}}">@lang('front.mydashboard')</a></li>--}}
                    {{--<li><a href="{{url('dashboard/messages')}}">@lang('front.mymessages')</a></li>--}}
                    {{--<li><a href="{{url('dashboard/favorites')}}">@lang('front.myfavorites')</a></li>--}}
                    {{--@if(Auth::check())--}}
                    {{--<li><a href="{{url('logout')}}">@lang('front.logout')</a></li>--}}
                    {{--@endif--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    {{--may be uncommented later--}}
                @endif
                {{--End of My Account Menu--}}

                <li class="dropdown">
                    <a href="#" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                        @foreach ($languages as $lang)
                            <?php $sel=(Session::get('yalla_locale')==$lang->iso2)?'selected="selected"':''; ?>
                            <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <ul class="nav nav-tabs tabs_search" role="tablist">
        <li role="presentation" class="active">
            <a href="#products" aria-controls="home" role="tab" data-toggle="tab">{{trans('front.products')}}</a>
        </li>
        <li role="presentation">
            <a href="#suppliers" aria-controls="tab" role="tab" data-toggle="tab">{{trans('front.suppliers')}}</a>
        </li>
        <li role="presentation">
            <a href="#deals" aria-controls="tab" role="tab" data-toggle="tab">{{trans('front.deals')}}</a>
        </li>
    </ul>
    <div style="background-color: #00509F; padding:5px 8px;">
        <div class="row">
            <div class="col-md-8">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="products">
                        {!!Form::open(['url'=>'search/products', 'method'=>'GET'])!!}
                        <div class="input-group">
                            {!!Form::text('q',null, ['class'=>'form-control Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_prods')])!!}
                            <span class="input-group-btn">
                                <button class="btn btn-warning" type="button"><i class="fa fa-search"></i></button>
                              </span>
                        </div>
                        {!!Form::close()!!}
                    </div>
                    <div role="tabpanel" class="tab-pane" id="suppliers">
                        {!!Form::open(['url'=>'search/suppliers', 'method'=>'GET'])!!}
                        <div class="input-group">
                            {!!Form::text('q',null, ['class'=>'form-control Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_supp')])!!}
                            <span class="input-group-btn">
                                <button class="btn btn-warning" type="button"><i class="fa fa-search"></i></button>
                              </span>
                        </div>
                        {!!Form::close()!!}
                    </div>
                    <div role="tabpanel" class="tab-pane" id="deals">
                        {!!Form::open(['url'=>'search/deals', 'method'=>'GET'])!!}
                        <div class="input-group">
                            {!!Form::text('q',null, ['class'=>'form-control Products pep_keyword auto_suggest_keyword ac_input','placeholder'=>trans('front.plchold_search_deals')])!!}
                            <span class="input-group-btn">
                                <button class="btn btn-warning" type="button"><i class="fa fa-search"></i></button>
                              </span>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="crt-area" style="margin: 0">
                    <a href="#d" class="crt-btn" id="crt-btn" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart"></span> {{trans('front.cart')}} <span class="badge">{{Product::cart_items($cart)}}</span></a>
                    <ul class="dropdown-menu">
                        @if($cart && !empty($cart))
                            @foreach($cart as $user_id=>$products)
                                @foreach($products as $product)
                                    <li>
                                        <figure class="row">
                                            <div class="col-md-12">
                                                <a href="{{url('cart/remove/'.$product->id)}}" class="rmov-itm-frm-crt"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                <a href="{{Product::url($product)}}" class="img-lnk-hldr">{!!App\Helper::image2($product->image)!!}</a>
                                                <figcaption>
                                                    <a href="{{Product::url($product)}}">{{Helper::field($product, 'name')}}</a>
                                                </figcaption>
                                            </div>
                                        </figure>
                                    </li>
                                @endforeach
                            @endforeach
                        @endif
                        <li><a href="{{url('cart')}}" class="btn btn-success">{{trans('front.contact_supplier')}}</a></li>
                    </ul>
                    <a href="{{url('dashboard/request_quotes/create')}}" class="pst-by-btn">{{trans('front.post_buying_req')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>