<?php use App\Helper; ?>
@extends('front.layouts.master')
@section('title') {{trans('front.what_clients_say')}} | @endsection
@section('content')
	
	<div class="container">
		<legend>{{trans('front.what_clients_say')}}</legend>
		@foreach($testimontials as $item)
			<div class="col-md-12">
				<figure class="clnst-tstmls-itm">
		            {!!Helper::image2($item->image)!!}
		            <figcaption>
		                <h5><strong>{{$item->company_name}}</strong></h5>
		                <h6>{{$item->name}} - <span>{{$item->job_title}}</span></h6>	                
		                <p><i></i> {!!$item->testimontial!!}</p>
		            </figcaption>
		        </figure>
		    </div>
		@endforeach
	</div>

	<hr>

	@include('front.testimontials.add')
@endsection