<div class="container clearfix">
	<div class="col-md-offset-3 col-md-6">
		<div class="well">
			<legend class="text-center">{{trans('front.add_testimontial')}}</legend>
			<div style="padding:0 20px;">
				{!!Form::open(['class'=>'form-horizontal', 'files'=>true, 'url'=>'testimontials/add'])!!}
					<div class="form-group {{$errors->has('name')?'has-error':''}}">
						{!!Form::label('name', trans('front.name'), ['class'=>'control-label'])!!}
						{!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>trans('front.ent_ur_name')])!!}
						{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
					</div>
					<div class="form-group {{$errors->has('job_title')?'has-error':''}}">
						{!!Form::label('job_title', trans('front.job'), ['class'=>'control-label'])!!}
						{!!Form::text('job_title', null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_job')])!!}
						{!! $errors->first('job_title', '<p class="help-block">:message</p>') !!}						
					</div>
					<div class="form-group {{$errors->has('company_name')?'has-error':''}}">
						{!!Form::label('company_name', trans('front.company_name'), ['class'=>'control-label'])!!}
						{!!Form::text('company_name',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_company_name')])!!}
						{!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}						
					</div>
					<div class="form-group {{$errors->has('testimontial')?'has-error':''}}">
						{!!Form::label('testimontial', trans('front.message'), ['class'=>'control-label'])!!}
						{!!Form::textarea('testimontial', null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_testimonial')])!!}
						{!! $errors->first('testimontial', '<p class="help-block">:message</p>') !!}						
					</div>
					<div class="form-group">
						<div class="text-center">
							{!!Form::submit('إرسال', ['class'=>'btn btn-primary'])!!}
						</div>
					</div>
				{!!Form::close()!!}
			</div>			
		</div>
	</div>
</div>
<div class="clearfix"></div>