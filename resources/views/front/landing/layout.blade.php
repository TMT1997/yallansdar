<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yalla Nsadar">
    <meta name="author" content="Yalla Nsadar">
    <link rel="shortcut icon" href="{{url('frontend/images/logo/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{url('frontend/images/logo/favicon.ico')}}" type="image/x-icon">
    <title>يلا نصدر</title>
    {{HTML::style("frontend/css/bootstrap.min.css")}}
    {{HTML::style("frontend/css/font-awesome.min.css")}}
	{{HTML::style('backend/dist/css/magnific-popup.css')}}
    {{HTML::style("frontend/landing/css/creative.css")}}
    {{HTML::style("frontend/landing/js/animated-headline/css/style.css")}}
    {{HTML::script("frontend/landing/js/animated-headline/js/modernizr.js")}}
    {{HTML::style("frontend/landing/css/custom.css")}}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top">
    @yield('content')

    {{HTML::script('frontend/landing/vendor/jquery/jquery.min.js')}}
    {{HTML::script('frontend/landing/js/animated-headline/js/main.js')}}
    {{HTML::script('frontend/js/bootstrap.min.js')}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    {{HTML::script('frontend/landing/vendor/scrollreveal/scrollreveal.min.js')}}
    {!!HTML::script('backend/dist/js/jquery.magnific-popup.min.js')!!}
    {!!HTML::script('frontend/landing/js/creative.min.js')!!}
    <script type="text/javascript">
        $('.carousel').carousel({
            interval:false,
        })
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-73755643-1', 'auto');
      ga('send', 'pageview');
    </script>
</body></html>