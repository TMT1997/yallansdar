@extends('front.landing.layout')
@section('content')
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header navbar-right">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll logo" href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
            <a class="navbar-brand page-scroll logo-wh" href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo_wh'),'Yalla Nsadar')}}</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li><a class="page-scroll" href="#home">الرئيسية</a></li>
                <li><a class="page-scroll" href="#about">عن يلا نصدر</a></li>
                <li><a class="page-scroll" href="#services">دُلني علي السوق</a></li>
                <li><a class="page-scroll" href="#products">المنتجات</a></li>
                <li><a class="page-scroll" href="#trade_manager">مدير التصدير</a></li>
                <li><a class="page-scroll" href="#contact">تواصل معنا</a></li>
                @if(Auth::guest())
                    <li><a class="page-scroll" href="{{url('login')}}">تسجيل الدخول</a></li>
                @else
                    <li><a class="page-scroll" href="{{url('dashboard')}}">حسابي</a></li>
                @endif
                @if(App::getLocale()=='ar')
                    <li><a class="nav_lang" href="{{App\Language::switch_url('en')}}">English</a></li>
                @else
                    <li><a class="nav_lang" href="{{App\Language::switch_url('ar')}}">العربية</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<header id="home">
    <div class="header-content">
        <div class="header-content-inner">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators" style="direction: rtl;">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active">أنا مصدر</li>
                    <li data-target="#carousel-example-generic" data-slide-to="1">انا مستورد</li>
                    <li data-target="#carousel-example-generic" data-slide-to="2">أنا مزود خدمة</li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <h1 id="homeHeading">أنا مُصدر</h1>
                        <div class="cd-intro">
                            <h1 class="cd-headline clip is-full-width">
                                <span>إذا كنت </span>
                                <span class="cd-words-wrapper">
                                    <b class="is-visible">تبحث عن فرص تصدير لمنتجاتك </b>
                                    <b>تريد الدخول إلي سوق التصدير</b>
                                    <b>لا تعرف أساليب التعبئة والتغليف والشحن</b>
                                    <b>لا تعرف كيف تستلم أموالك من المستورد بأمان</b>
                                    <b>لا تمتلك اوراق رسمية لإجراء عملية التصدير</b>
                                    <b>لا تعرف الشروط والمواصفات المطلوبة لكل بلد</b>
                                </span>
                            </h1>
                        </div>
                        <hr><p>سوف نساعدك في التغلب علي معوقات التصدير</p>
                        @if(Auth::guest())
                            <a href="{{url('register')}}" class="btn btn-primary btn-xl page-scroll">ادخل وسجل الآن</a>
                        @endif
                        <a href="{{url('dashboard/products/create')}}" class="btn btn-primary btn-xl">اضف منتجاتك الأن</a>
                    </div>
                    <div class="item">
                        <h1 id="homeHeading">أنا مُستورد</h1>
                        <div class="cd-intro">
                            <h1 class="cd-headline clip is-full-width">
                                <span>إذا كنت تبحث عن </span>
                                <span class="cd-words-wrapper">
                                    <b class="is-visible">أفضل المنتجات</b>
                                    <b>افضل الاسعار</b>
                                    <b>مصدرين يتمتعون بسمعه طيبة ومصداقية عالية</b>
                                    <b>تأمين عملية الدفع واستلام المنتجات المطابقة للمواصفات المطلوبة</b>
                                    <b>منتجات متنوعة , وخامات عالية الجودة</b>
                                    <b>مصنعين علي درجة عالية من الكفاءة والاحترافية</b>
                                </span>
                            </h1>
                        </div>
                        <hr><p>سوف نساعدك في التغلب علي معوقات التصدير</p>
                        @if(Auth::guest())
                            <a href="{{url('register')}}" class="btn btn-primary btn-xl">ادخل وسجل الآن</a>
                        @else
                            <a href="{{url('home')}}" class="btn btn-primary btn-xl">تصفح المنتجات</a>
                        @endif
                    </div>
                    <div class="item">
                        <h1 id="homeHeading">أنا مُزود خدمة</h1>
                        <div class="cd-intro">
                            <h1 class="cd-headline clip is-full-width">
                                <span>إذا كنت </span>
                                <span class="cd-words-wrapper">
                                    <b class="is-visible">شركة شحن</b>
                                    <b>مخلص جمركي</b>
                                    <b>مقاول نقل</b>
                                    <b>محطة فرز</b>
                                    <b>شركة تعبئة وتغليف</b>
                                    <b>شركة تأمين</b>
                                    <b>مركز تدريب واستشارات</b>
                                    <b>مصدر تمويل</b>
                                    <b>مستثمر</b>
                                </span>
                            </h1>
                        </div>
                        <hr><p>لديك الان فرصة ذهبية للتواصل مع المصدرين وعرض خدماتك عليهم</p>
                        @if(Auth::guest())
                            <a href="{{url('register')}}" class="btn btn-primary btn-xl">ادخل وسجل الآن</a>
                        @else
                            <a href="{{url('home')}}" class="btn btn-primary btn-xl">تصفح المنتجات</a>
                        @endif
                    </div>
                </div>
                <div class="slider_btns">
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">عن يلا نصدر</h2>
                <hr class="light">
                <p class="text-faded">
                    يلا نصدر هو سوق إلكتروني لمنتجات الشركات الصغيرة والمتوسطة يهدف إلي تسهيل التواصل بين المصدرين وتجار الجملة والمنتجين والمصنعين المصريين من ناحية و المستوردين في جميع انحاء العالم من ناحية أخري , بمساعدة مدير التصدير الذي يعمل علي تسهيل عملية التواصل بين المصدرين والمستوردين
                    كما يعمل يلا نصدر كشبكة ربط بين المصدرين ومزودي خدمات التصدير مثل شركات الشحن والنقل والتخليص الجمركي ومحطات الفرز وشركات التعبئة والتغليف وشركات اصدار الاعتمادات والتراخيص والشهادات ومراكز التدريب والاستشارات ومصادر التمويل
                </p>
                <a href="{{url('site')}}" class="page-scroll btn btn-default btn-xl sr-button">المزيد</a>
            </div>
        </div>
    </div>
</section>
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">دُلني علي السوق</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div class="container services">
        <div class="row">
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-comments text-primary sr-icons"></i></a>
                    <h3>تواصل مع مستوردين</h3>
                    <p class="text-muted">نحن لا نقوم بالتصدير نيابة عنك ,نحن نساعدك فقط في التواصل مع المستوردين</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-briefcase text-primary sr-icons"></i></a>
                    <h3>تريد مانجر</h3>
                    <p class="text-muted">نوفر لك خدمة التريد مانجر الذي يساعدك في التغلب علي مشاكل التصدير</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i></a>
                    <h3>فرص تصدير</h3>
                    <p class="text-muted">نوفر لك فرص تصدير</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i></a>
                    <h3>متجرك الإلكتروني</h3>
                    <p class="text-muted">نحن نساعدك في عرض منتجاتك بطريقة احترافية من خلال متجر إلكتروني خاص بك</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-globe text-primary sr-icons"></i></a>
                    <h3>تسويق المنتجات</h3>
                    <p class="text-muted">نقوم بالتسويق لكل المنتجات المعروضه علي يلا نصدر من خلال الانترنت ومن خلال المعارض الدولية</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 service_sgl text-center">
                <div class="service-box">
                    <a href="{{url('home')}}"><i class="fa fa-4x fa-line-chart text-primary sr-icons"></i></a>
                    <h3>الاحصاءات والدراسات</h3>
                    <p class="text-muted">نتيح لك المعلومات والاحصاءات والدراسات التي تجعلك علي اطلاع دائم بأهم المستجدات في السوق العالمية</p>
                </div>
            </div>
            <div class="row text-center clearfix">
                <div class="col-md-12">
                    <a href="{{url('dashboard/products/create')}}" class="btn btn-primary btn-xl">اضف منتجاتك الأن</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="no-padding" id="products">
    <div class="container-fluid">
        <div class="row no-gutter ">
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/1/agriculture')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/1.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded"> زراعة </div>
                            <div class="project-name">
                                معدات زراعية | مخلفات زراعية | كيماويات ومبيدات | منتجات حيوانية | أسمدة | فاكهة طازجة | خضراوات طازجة  | حبوب | بقوليات | مشروم | المنتجات العضوية | نباتات الزينة | زيوت نباتية وحيوانية
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/15/food-beverage')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/2.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">أغذية ومشروبات</div>
                            <div class="project-name">
                                غذاء أطفال | مخبوزات | حلويات | منتجات البقوليات | منتجات الفاكهة | منتجات الحبوب | منتجات الخضراوات | منتجات البيض
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/2/apparel')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/3.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">ملابس</div>
                            <div class="project-name">
                                ملابس أطفالي | ملابس أولادي | ملابس بناتي | ملابس رجالي | ملابس حريمي | ملابس قطنية | ملابس رياضية | يونيفورم | ملابس مستعملة 
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/14/fashion-accessories')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/4.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">موضة و إكسسوارات</div>
                            <div class="project-name">
                                إكسسوارات أحزمة | أحزمة | قفازات | إكسسوارات الرأس | إكسسوارات الرقبة | أطقم قفازات وقبعات ووشاح | خدمات تصميم الإكسسوارات | خدمات تجهيز الإكسسوارات | مخزون الإكسسوارات
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/4/business-services')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/5.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">خدمات الأعمال</div>
                            <div class="project-name">
                                إعلانات | وكالات | خدمات التأمين | مزادات | خدمات الوساطة | خدمات سفر الاعمال | التفتيش واستخراج الشهادات | الكمبيوتر وخدمات تكنولوجيا الأعمال | خدمات البناء
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a href="{{url('category/5/chemicals')}}" class="portfolio-box">
                    {{HTML::image('frontend/landing/img/portfolio/thumbnails/6.jpg', '', ['class'=>'img-responsive'])}}
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">كيماويات</div>
                            <div class="project-name">
                                مواد لاصقة ومانعة للتسرب | خليط وإضافات | كيماويات زراعية ومبيدات | كيماويات عضوية أساسية | المنتجات البيولوجية الكيميائية | محفزات وعوامل مساعدة كيميائية | منتجات الكشف الكيميائي
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<aside class="bg-dark" id="trade_manager">
    <div class="container text-center">
        <div class="call-to-action">
            <h2>مدير التصدير</h2>
            <p>
                خدمة مدير التصدير هي خدمة اختيارية متاحة للشركات التي ترغب بأن يكون لديها خبير في التسويق وإبرام الصفقات التجارية والحصول علي فرص التصدير ومتابعة الإجراءات البنكية والشحن والتخليص الجمركي. 
                <br>
                سواء أكان لديك خبرة في التصدير وليس لديك الوقت لمتابعة عملية التسويق والتفاوض , أو لم يكن لديك خبرة وترغب في أن يكون لديك مدير تصدير من إدارة الموقع يقوم بكل العمليات بالنيابة عنك , فهذه الخدمة ستكون مثالية بالنسبة لك.
                <br>
                <strong>مدير التصدير هو المسئول عن :</strong>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-globe text-primary sr-icons"></i>
                        <p class="text-muted">تسويق منتجاتك في الأسواق الخارجية </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-fax text-primary sr-icons"></i>
                        <p class="text-muted">الاتصال بالعملاء والتعريف بمنتجاتك </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-binoculars text-primary sr-icons"></i>
                        <p class="text-muted">التفاوض حول الصفقه </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-balance-scale text-primary sr-icons"></i>
                        <p class="text-muted">إبلاغك بطلبات العملاء </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-truck text-primary sr-icons"></i>
                        <p class="text-muted">التأكد من مواصفات المنتجات ومواعيد الشحن والتسليم </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-check-square-o text-primary sr-icons"></i>
                        <p class="text-muted">التأكد من معرفه العملاء بكل بنود الإتفاق بينك وبينهم </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-users text-primary sr-icons"></i>
                        <p class="text-muted">متابعة إبرام العقود وفتح الاعتمادات </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-ship text-primary sr-icons"></i>
                        <p class="text-muted">متابعة عملية الشحن واستلام الشحنه من قبل المستورد </p>
                    </div>
                </div>
            </p>
            <a href="{{url('site/trade-manager')}}" class="btn btn-default btn-xl sr-button page-scroll">اطلب خدمة مدير التصدير</a>
        </div>
    </div>
</aside>
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading text-default">تواصل معنا</h2>
                <hr class="primary"><br>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-map fa-3x sr-contact"></i>
                <h3>العنوان</h3>
                <p>
                    مدينة 6 أكتوبر  - الحي السادس - مبنى هيئة تنمية الصادرات المصرية
                </p>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-phone fa-3x sr-contact"></i>
                <h3>يمكنكم الاتصال بنا الآن</h3>
                <p>
                    +(02) 010 6216 6175
                    <br>
                </p>
            </div>
            <div class="col-lg-4 text-center">
                <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                <h3>يمكنكم مراسلتنا بالإيميل</h3>
                <p>
                    <a href="mailto:tarek@yallansadar.com">Tarek@yallansadar.com</a>
                    <br>
                    <a href="mailto:export@yallansadar.com">Export@yallansadar.com</a>
                </p>
            </div>
        </div>
    </div>
</section>
<footer id="footer-section" class="footer-section ow-background">
    <div class="container">
        <div class="footer-bottom" style="margin-top: 15px;padding: 10px 0">
            <div class="col-md-6" style="float: right;text-align: right">
                <div class="widget widget_social row">
                    <p style="text-align: right;display: inline-block">جميع الحقوق محفوظة © لشركة  يللّا نصدّر  2017</p>    
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget widget_social row" style="float: left">
                    <ul>
                        <li><a title="Facebook" href="https://www.facebook.com/yallansadar" target="blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a title="Twitter" href="https://twitter.com/yallansadar" target="blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a title="Google Plus" href="https://plus.google.com/114404097260213152411" target="blank"><i class="fa fa-google-plus"></i></a></li>
                        <li><a title="Linked In" href="https://www.linkedin.com/in/yallansadar" target="blank"><i class="fa fa-linkedin-square"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
@endsection