@extends('front.landing.layout')
@section('content')
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,800" rel="stylesheet">
<style type="text/css">
body {font-family: 'Raleway', sans-serif;}
    .navbar-nav>li {float: left;}
    .nav_lang {border-left: 1px dotted #666;}
    .cd-words-wrapper { text-align: left; }
    .cd-headline.clip .cd-words-wrapper:after {right:0 !important; left:initial !important;}
    .call-to-action {direction: ltr;}
    .guide_me {direction: ltr; }
    .guide_me li {text-align: left}
    .cd-intro {direction: ltr}
</style>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll logo" href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
                <a class="navbar-brand page-scroll logo-wh" href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo_wh'),'Yalla Nsadar')}}</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="page-scroll" href="#home">Home</a></li>
                    <li><a class="page-scroll" href="#about">About Us</a></li>
                    <li><a class="page-scroll" href="#services">What we do</a></li>
                    <li><a class="page-scroll" href="#products">Products</a></li>
                    <li><a class="page-scroll" href="#trade_manager">Trade Manager</a></li>
                    <li><a class="page-scroll" href="#contact">Contact Us</a></li>
                    @if(Auth::guest())
                        <li><a class="page-scroll" href="{{url('login')}}">Login</a></li>
                    @else
                        <li><a class="page-scroll" href="{{url('dashboard')}}">My Account</a></li>
                    @endif
                    <li><a class="nav_lang" href="{{App\Language::switch_url('ar')}}">العربية</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <header id="home">
        <div class="header-content">
            <div class="header-content-inner">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active">I’m an Exporter</li>
                        <li data-target="#carousel-example-generic" data-slide-to="1">I’m an Importer</li>
                        <li data-target="#carousel-example-generic" data-slide-to="2">I’m a Service Provider</li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <h1 id="homeHeading">I’m an Exporter</h1>
                            <div class="cd-intro">
                                <h1 class="cd-headline clip is-full-width">
                                    <span>If you </span>
                                    <span class="cd-words-wrapper">
                                        <b class="is-visible">Looking for chances to export your products </b>
                                        <b>Want exposure on  international markets</b>
                                        <b>do not know about the process of packing, transporting and shipping</b>
                                        <b>want to secure your money from importers</b>
                                        <b>do not have a registered file for exporting</b>
                                        <b>do not know the policies and regulation of international markets</b>
                                    </span>
                                </h1>
                            </div>
                            <hr><p>We will help you to overcome all the obstacles so you can export</p>
                            @if(Auth::guest())
                                <a href="{{url('register')}}" class="btn btn-primary btn-xl page-scroll">Sign Up Now</a>
                            @endif
                            <a href="{{url('dashboard/products/create')}}" class="btn btn-primary btn-xl">Add your products</a>
                        </div>
                        <div class="item">
                            <h1 id="homeHeading">I’m An Importer</h1>
                            <div class="cd-intro">
                                <h1 class="cd-headline clip is-full-width">
                                    <span>If you’re looking for </span>
                                    <span class="cd-words-wrapper">
                                        <b class="is-visible">High quality products</b>
                                        <b>Competitive prices</b>
                                        <b>Qualified exporters high on quality, transparency and professionalism.</b>
                                        <b>Insurance on your payments, products and delivery</b>
                                        <b>Diversified industries and products.</b>
                                        <b>Manufacturers of certified quality assurance. </b>
                                    </span>
                                </h1>
                            </div>
                            <hr><p>We would love to welcome you on board. </p>
                            @if(Auth::guest())
                                <a href="{{url('register')}}" class="btn btn-primary btn-xl page-scroll">Sign Up Here</a>
                            @else
                                <a href="{{url('home')}}" class="btn btn-primary btn-xl page-scroll">Browse Now</a>
                            @endif
                        </div>
                        <div class="item">
                            <h1 id="homeHeading">I’m a service Provider</h1>
                            <div class="cd-intro">
                                <h1 class="cd-headline clip is-full-width">
                                    <span>If you are </span>
                                    <span class="cd-words-wrapper">
                                        <b class="is-visible">A Shipping company</b>
                                        <b>Customs clearance</b>
                                        <b>Transportation agency</b>
                                        <b>Sort station</b>
                                        <b>Packing and packaging company</b>
                                        <b>Insurance company</b>
                                        <b>Training /consultancy company</b>
                                        <b>Investor</b>
                                        <b>Broker</b>
                                    </span>
                                </h1>
                            </div>
                            <hr><p>Now you have the opportunity to connect your services with importers and exporters </p>
                            @if(Auth::guest())
                                <a href="{{url('register')}}" class="btn btn-primary btn-xl page-scroll">Sign Up Now</a>
                            @else
                                <a href="{{url('home')}}" class="btn btn-primary btn-xl page-scroll">Browse Now</a>
                            @endif
                        </div>
                    </div>
                    <div class="slider_btns">
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">About Yalla Nsadar</h2>
                    <hr class="light">
                    <p class="text-faded">
                        Yalla Nsadar, is an online b2b market place for individual, small and medium exporters exposing them to international importers from all around the globe. With the help of trade managers who are delegated to ease the process between both exporter and importer. 
                        <br><br>
                        Yalla Nsadar marketplace is also a connected network to build a linkage between exporters and service providers in their country such as shipping, clearance, packing and packaging, certifications, legal and financial consultation, and funding. 

                    </p>
                    <a href="{{url('site')}}" class="page-scroll btn btn-default btn-xl sr-button">Read more...</a>
                </div>
            </div>
        </div>
    </section>
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">What we do</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container services">
            <div class="row">
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-diamond text-primary sr-icons"></i></a>
                        <h3>international importers</h3>
                        <p class="text-muted">We do not export on your behalf, but we go half way through with international importers</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i></a>
                        <h3>Your Website</h3>
                        <p class="text-muted">We enhance your product’s marketing reach through providing your own mini-website.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i></a>
                        <h3>Exporting chances</h3>
                        <p class="text-muted">We use our expertise to find importers interested in buying Egyptian products
</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i></a>
                        <h3>Trade Manager</h3>
                        <p class="text-muted">We support with our trade manager service to build up your exporting process.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-heart text-primary sr-icons"></i></a>
                        <h3>Products Marketing</h3>
                        <p class="text-muted">We market all products through our platform and international trade fairs. </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 service_sgl text-center">
                    <div class="service-box">
                        <a href="{{url('home')}}"><i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i></a>
                        <h3>Market research </h3>
                        <p class="text-muted">We generate market research to give you a scope on international market needs and wants.</p>
                    </div>
                </div>
                <div class="row text-center clearfix">
                    <div class="col-md-12">
                        <a href="{{url('dashboard/products/create')}}" class="btn btn-primary btn-xl">Add your products</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="no-padding" id="products">
        <div class="container-fluid">
            <div class="row no-gutter ">
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/1/agriculture')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/1.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Agriculture
                                </div>
                                <div class="project-name">
                                    Agricultural Equipment | Agricultural Waste | Agrochemicals & Pesticides | Animal Products | Feed | Fertilizer | Fresh Fruit | Fresh Vegetables | Nuts & Kernels | Grain | Beans | Mushrooms & Truffles | Organic Produce | Ornamental Plants |
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/15/food-beverage')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/2.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Food & Beverage
                                </div>
                                <div class="project-name">
                                    Baby Food | Baked Goods | Confectionery | Bean Products | Fruit Products | Grain Products | Vegetable Products | Egg Products | Honey Products | Meat & Poultry | Seafood | Canned Food | Instant Food | Snack Food | Beverage | Dairy | Food Ingredients 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/2/apparel')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/3.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Apparel 
                                </div>
                                <div class="project-name">
                                    Baby's Clothing | Boy’s Clothing | Girls’ Clothing | Men’s Clothing | Women’s Clothing | Cotton Clothing | Sportswear | Uniforms | Used Clothes | Stage & Dance Wear | Garment Accessories | Sewing Supplies | Apparel Design Services | Apparel Processing Services | Apparel Stock | Other Apparel
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/14/fashion-accessories')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/4.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Fashion Accessories
                                </div>
                                <div class="project-name">
                                    Belt Accessories | Belts | Gloves & Mittens | Headwear | Neckwear | Scarf, Hat & Glove Sets | Fashion Accessories Design Services | Fashion Accessories Processing Services | Fashion Accessories Stock | Other Fashion Accessories 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/4/business-services')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/5.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Business Services 
                                </div>
                                <div class="project-name">
                                    Advertising | Agency Services | Assurance Services | Auction | Brokerage, Intermediary Service | Business Travel Services | Certification, Inspection & Credit Management | Computer & Information Technology Services | Construction Services | Consulting 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="{{url('category/5/chemicals')}}" class="portfolio-box">
                        {{HTML::image('frontend/landing/img/portfolio/thumbnails/6.jpg', '', ['class'=>'img-responsive'])}}
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chemicals
                                </div>
                                <div class="project-name">
                                    Adhesives & Sealants | Admixture & Additives | Agrochemicals & Pesticides | Basic Organic Chemicals | Biological Chemical Products | Catalysts & Chemical Auxilliary Agents | Chemical Reagent Products | Coating & Painting | Daily Chemicals | Flavour & Fragrance | Inorganic Chemicals
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <aside class="bg-dark" id="trade_manager">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Trade Manager</h2>
                <p>
                    The “Trade Manager” service is an optional service provided by our team to easily fulfill the need of having an export manager for your products. In this service you can easily connect, communicate, market, and get your deal personally handled to importers you wish you collaborate with.

                    <br>
                    <strong>Trade Manager is responsible for:</strong>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-balance-scale text-primary sr-icons"></i>
                            <p class="text-muted">Marketing your products globally. </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-binoculars text-primary sr-icons"></i>
                            <p class="text-muted">Contacting importers to introduce your products to.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-fax text-primary sr-icons"></i>
                            <p class="text-muted">Negotiations on deals</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-globe text-primary sr-icons"></i>
                            <p class="text-muted">Order on inquiries. </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-money text-primary sr-icons"></i>
                            <p class="text-muted">Assurance on quality, and delivery time </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-shopping-cart text-primary sr-icons"></i>
                            <p class="text-muted">Assurance that all terms and conditions are met by both parties. </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-users text-primary sr-icons"></i>
                            <p class="text-muted">Assurance on legal and financial regulations and policies. </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
                            <p class="text-muted">Follow up on importers when receiving the shipment and feedback. </p>
                        </div>
                    </div>
                </p>
                <br>
                <div class="col-md-12 request_tm">
                    <a href="{{url('site/trade-manager')}}" class="btn btn-default btn-xl sr-button page-scroll">Request Trade Manager Service</a>
                </div>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading text-default">Contact Us</h2>
                    <hr class="primary"><br>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-map fa-3x sr-contact"></i>
                    <h3>Address</h3>
                    <p>
                        St. 4 - El Nakhil Center <br>
                        11th District, 6<sup>th</sup> of October City
                    </p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <h3>Get in touch</h3>
                    <p>
                        +(02) 01062166175
                        <br>
                        +(02) 01111929530
                    </p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <h3>Contact us via Email</h3>
                    <p>
                        <a href="mailto:tarek@yallansadar.com">Tarek@yallansadar.com</a>
                        <br>
                        <a href="mailto:reem@yallansadar.com">Reem@yallansadar.com</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <footer id="footer-section" class="footer-section ow-background">
        <div class="container">
            <div class="footer-bottom" style="margin-top: 15px;padding: 10px 0">                
                <div class="col-md-6" style="float: right;text-align: right">
                    <div class="widget widget_social row">
                        <p style="text-align: right;display: inline-block">All copyrights reserved for Yalla Nsadar 2017 &copy;</p>    
                    </div>
                </div>              
                <div class="col-md-6">
                    <div class="widget widget_social row" style="float: left">
                        <ul>
                            <li><a title="Facebook" href="https://www.facebook.com/yallansadar" target="blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a title="Twitter" href="https://twitter.com/yallansadar" target="blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a title="Google Plus" href="https://plus.google.com/114404097260213152411" target="blank"><i class="fa fa-google-plus"></i></a></li>
                            <li><a title="Linked In" href="https://www.linkedin.com/in/yallansadar" target="blank"><i class="fa fa-linkedin-square"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>          
        </div>
    </footer>
@endsection