<?php
	use App\Category;
	use App\Service;
    use App\Helper;
    use App\User;
?>
@extends('front.layouts.master')
@section('title') {{trans('dash.services')}} | @endsection
@section('content')
	
<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                
			</div>
			<div class="col-xs-12 col-md-8">			
				<header><h1 class="hdr-styl-001 inr-hdr-blk">{{trans('dash.services')}}</h1></header>
                
                <div class="row">                    
                    <div class="col-xs-12 col-md-3">
                        <div class="dataTables_length">
                            <label>{{trans('front.show')}}  
                            <select name="example_length" aria-controls="example" class="form-control input-sm custom-show-slct">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> {{trans('front.entries')}}</label>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                    <div class="col-xs-12 col-md-7 fltrs">
                        {{trans('front.sort_by')}}: <label><input type="checkbox" id="verified" data-page="1">{{trans('front.verified')}}</label>
                    </div>
                    <div class="col-xs-12 col-md-5 rt-tp-optons">
                        <div id="next-bttn"></div>
                        <div id="prv-bttn"></div>
                        <div class="pag-stt-info"></div>
                        <div class="grd-srt-btn"></div>
                        <div class="rows-srt-btn actv-grid"></div>
                        {{trans('front.view_by')}}:
                    </div>              
                </div>
                <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr><th></th></tr>
                    </thead>
                    <tfoot>
                        <tr><th></th></tr>
                    </tfoot>
                    <tbody>
                    	@foreach($services as $service)
                        <?php $profile = $service->user->profile; 
                            $service_lang = Helper::lang_data($service, $lng_id);
                        ?>
                        <tr>
                            <td>
                                <div class="row pro-thumb-row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                        <div class="media">
                                            <a class="pull-right" style="padding-right:0" href="{{service::url($service)}}">
                                                {{Service::Image($service,['class'=>'media-object', 'style'=>'max-width:100px;'])}}
                                            </a>
                                            <div class="media-body">
                                                <a href="{{service::url($service)}}"><h4 class="media-heading">{{$service_lang->name}}</h4></a>
                                                <p>{{str_limit($service_lang->description, 120)}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 options-col">
                                        <span class="produc-by">
                                            {!!User::isVerified($service->user,false)!!}
                                            <a href="#">{{$profile->company_name}}</a>
                                        </span>
                                        <span>{!!User::flag($service->user,[],true)!!}</span>
                                        <span class="cntct-dtlls"><a href="{{User::url_contact($service->user)}}" class="popup"><i class="fa fa-paper-plane bluu"></i> {{trans('front.contact_details')}}</a></span>
                                    </div>
                                    <div class="rqst-qution-hldr scnd-btn">
                                        {!!Form::open(['url'=>'cart'])!!}
                                            <input type="hidden" name="service_id" value="{{$service->id}}">
                                            <button type="submit" class="grn-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{trans('front.request_for_quote')}}</button>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>				
			</div>
            
            <aside class="col-xs-12 col-md-2 sponsored_services">
                <header><div class="hdr-styl-asid">{{trans('front.sponsored_services')}}</div></header>
                @foreach($services as $service)
	                <figure class="asid-prdct-itm">
	                    <a href="{{Service::url($service)}}">{{Service::Image($service)}}</a>
	                    <figcaption><h4><a href="#d">{{Service::name($service,$lng_id)}}</a></h4></figcaption>
	                </figure>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection

@section('script')
    @include('dashboard.layouts.popup')
@endsection