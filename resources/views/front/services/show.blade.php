<?php
	use App\User;
	use App\Category;
	use App\Service;
	use App\Company;
	use App\Helper;
	$parents = array_reverse($category_parents);
	$service_lang = Helper::lang_data($service, $lng_id);
	$profile = $service->user->profile;
	if(!$service_lang) {
		return redirect()->back();
	}

	$business_types = ($profile->business_type) ? explode(',', $profile->business_type):'';
	$service_locations = (!empty($service->locations)) ? explode(',',$service->locations): [];
?>
@extends('front.layouts.master')
@section('title') {{$service_lang->name}} | @endsection
@section('content')
<div class="">
	<ul class="container breadcrumb">
	  	<li><a href="{{url('/')}}">{{trans('front.home')}}</a></li>
	  	@if(!empty($parents))
		  	@foreach($parents as $cat)
				<li><a href="{{Category::url($cat)}}">{{@$cat->$c_name}}</a></li>
			@endforeach
		@endif
		<li class="active">{{$service_lang->name}}</li>
	</ul>
</div>

<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
                <div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>{{trans('front.loading_images')}}</div>
				<div class="sp-wrap">
                	@if($service->image)
                		<a href="{{url('uploads/'.$service->image)}}">{!!Helper::image2($service->image, [], false)!!}</a>
                	@endif
                	@foreach($service->images as $img)
                    	<a href="{{url('uploads/'.$img->image)}}">{!!Helper::image2($img->image, [], false)!!}</a>
                    @endforeach
                </div>
			</div>
			<div class="col-xs-12 col-md-8">
				<header><h1 class="hdr-styl-001" style="margin-top: 0">{{$service_lang->name}}</h1></header>
				<p class="nrml-p" style="margin-top: 10px;">
					{!!str_limit($service_lang->description, 500)!!}
				</p>
				<div class="table-responsive">
					<table class="table table-condensed inf-tbl">
						<tbody>
							<tr>
								<td style="width:140px;">{{trans('dash.price_range')}}:</td>
								<td>
									@if($service->price_min && $service->price_max)
										{{trans('front.from')}} {{$service->price_min}} {{trans('front.to')}} {{$service->price_max}}</a>
									@else
										{{trans('front.not_available')}}
									@endif
								</td>
							</tr>
							<tr>
								<td>{{trans('dash.expiry_date')}}:</td>
								<td>{{$service->expired_at}}</td>
							</tr>
							<tr>
								<td>{{trans('dash.request_location')}}:</td>
								<td>
									@if(count($service_locations)>0)
										@foreach($service_locations as $id)
											<a href="#" class="btn btn-xs btn-default">{{App\Country::get_country($id)->name}}</a>
										@endforeach
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="cnct-inf-sid ad-pd">
					<h3 class="hdr-styl-004"><a href="#d">{{$profile->company_name}}</a> <span class="flg-ico">
					{!!User::flag($service->user,[],true)!!}</span></h3>

					<p class="sml-p-wth-spn">
						<span>{{trans('front.business_type')}}:</span>
						@if($business_types && is_array($business_types))
							@foreach($business_types as $type)
								{{trans('dash.business_types')[$type]}}, 
							@endforeach
						@endif
					</p>

					<div class="clnt-stt">
						<div><a href="javascript:void(0)"><i class="fa fa-info-circle nt-vrfid"></i></a> {{trans('front.not_verified')}}</div>
						<div><i class="fa fa-user"></i> {{$profile->first_name}} {{$profile->last_name}}</div>
						<a href="{{User::url_contact($service->user)}}" class="popup"><i class="fa fa-paper-plane bluu"></i> {{trans('front.contact_details')}}</a>
					</div>
                	<div class="options-col">
                    	<div>
                            <a><i class="fa fa-camera"></i></a>
                            <a href="#d"><i class="fa fa-film"></i></a>
                            <a href="{{User::url_contact($service->user)}}" class="popup"><i class="fa fa-info"></i></a>
                            <a href="#"><i class="fa fa-tag"></i></a>
                        </div>
                    </div>
				</div>

				<div class="shr-prnt-br02 row">
					<div class="col-xs-12 col-md-6 folow-us">
						<span>{{trans('front.share_us_on')}}:</span>
						<div style="display: inline-block;" class="addthis_inline_share_toolbox"></div>
					</div>
					<div class="col-xs-12 col-md-6 prnt-br text-right">
						<a href="{{Service::print_url($service)}}" target="_blank"><i class="fa fa-print"></i> {{trans('front.print')}}</a>
						<a href="#d"><i class="fa fa-envelope"></i> {{trans('front.product_add_email')}}</a>
						<a href="{{url('product/addtofavorites/'.$service->id.'/service')}}" class="popup"><i class="fa fa-heart"></i> {{trans('front.add_to_favorites')}}</a>
					</div>
				</div>
				
				<div class="lft-btn-hldr hidden">
					{!!Form::open(['url'=>'cart'])!!}
                        <input type="hidden" name="product_id" value="{{$service->id}}">
                        <button type="submit" class="grn-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{trans('front.request_for_quote')}}</button>
                    {!!Form::close()!!}
				</div>
			</div>
			<br class="clr-bth" />
			<a name="details"></a>
			<div id="tabs02" class="tabs02">
				<ul>
				    <li><a class="tab_nav" href="#tabs02-1">@lang('front.service_details')</a></li>
				    <li><a class="tab_nav" href="#tabs02-2">@lang('front.company_profile')</a></li>
				</ul>
				<div id="tabs02-1" class="product_details">
					<div class="row">
						<div class="col-md-12">
							{!!$service_lang->details!!}
						</div>
					</div>
				</div>
				<div id="tabs02-2">
				    {{$profile->company_name}}
				    <br>
				    <div class="lock_layer">
						<a href="{{url('interact/unlock?ref=contact_details&id=')}}" class="unlock_act popup"><i class="fa fa-unlock"></i> @lang('front.unlock_contact_details')</a>
					</div>
				    <div class="locked">
				    	<a href="{{User::url_contact($service->user)}}" class="popup btn btn-primary"><i class="fa fa-paper-plane"></i> {{trans('front.contact_details')}}</a>
				    </div>
				</div>
			</div>

			<a name="contact_supplier"></a>
			<div class="full-blk table-responsive">
				<legend>{{trans('front.contact_service_provider')}}</legend>
				{!!Form::open(['class'=>'form-horizontal', 'url'=>'services/enquiry'])!!}
					{!!Form::hidden('service_id', $service->id)!!}
					@if(Auth::check())
						<div class="col-md-6">
							<div class="form-group {{$errors->has('name')?'has-error':''}}">
								{!!Form::label('name', trans('front.from'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									<a href="{{User::url_contact(Auth::user())}}" class="popup">
										{{Auth::user()->name}} 
										@if(Auth::user()->profile)
											<br>{{Auth::user()->profile->company_name}}	
										@endif
									</a>
								</div>
							</div>
						</div>
					@else
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('name',trans('front.name'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('name',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_name'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('email',trans('front.email'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('email',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_email'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('country',trans('front.country'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::select('country',$countries->lists('name','id'),$selected_country,['class'=>'form-control'])!!}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!!Form::label('mobile',trans('front.mobile'), ['class'=>'col-md-3 control-label'])!!}
								<div class="col-md-8">
									{!!Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_mobile'),'required'=>'required'])!!}
								</div>
							</div>
						</div>
					@endif
					<div class="col-md-6">
						<div class="form-group">
							{!!Form::label('to',trans('front.send_to'), ['class'=>'col-md-3 control-label'])!!}
							<div class="col-md-8">
								<a href="{{User::url_contact($service->user)}}" class="popup">
									{{$service->user->name}}<br>
									<div class="help-block">{{$service->user->profile->company_name}}</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-6">
						<div class="form-group">
							{!!Form::label('description',trans('front.description'), ['class'=>'col-md-3 control-label'])!!}
							<div class="col-md-8">
								{!!Form::textarea('description', null, ['class'=>'form-control', 'rows'=>'3'])!!}
							</div>
						</div>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-5">
								{!!Form::submit(trans('front.send_enquiry'), ['class'=>'btn btn-loading btn-warning'])!!}
							</div>
						</div>
					</div>
				{!!Form::close()!!}
			</div>

			<div class="full-blk table-responsive">
				<legend>{{trans('front.other_services')}}</legend>
				<div class="row lstng-stly-02">
					@foreach($other_services as $i_service)
						<?php $service_lang = Helper::lang_data($i_service, $lng_id); ?>
				        <figure class="col-xs-12 col-sm-6 col-md-3">
				            <div>
				                <a href="{{Service::url($i_service)}}" class="img-lnk-hldr">{{Service::Image($i_service)}}</a>
				                <figcaption>
				                    <div><a href="{{Service::url($i_service)}}">{{$service_lang->name}}</a></div>
				                </figcaption>
				            </div>
				        </figure>
				    @endforeach
			    </div>
			    <div class="lft-btn">
			    	<a href="{{url('services')}}" class="vw-mr-btn">{{trans('front.view_more')}}</a>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
	@include('dashboard.layouts.popup')
@endsection