{!!Form::open(['url'=>'interact/comments', 'class'=>'form-horizontal', 'id'=>'contact-form'])!!}
{!!Form::hidden('record_id', $item->id)!!}
{!!Form::hidden('model', $model)!!}
<div class="comments_form">
  @if(isset($item->comments) && $item->comments->count()>0)
  <div class="well">
    <h3 class="head">@lang('front.comments_o')</h3>
    <ul class="list-group comments_list">
    @foreach($item->comments as $cm)
      <li class="list-group-item">
        <div class="media">
          <a class="pull-right avatar" href="#"><i class="fa fa-user fa-2x" class="media-object"></i></a>
          <div class="media-body">
            <p>{{$cm->comment}} <br>
            <span class="light text-sm post_date">{{$cm->name}} - {{Date::parse($item->updated_at)->diffForHumans()}}</span>
            </p>
          </div>
        </div>
      </li>
    @endforeach
    </ul>
  </div>
  @endif

<a name="addcomment"></a>
<div class="ib2-style2 ib2-text-color-light-theme ib2-custom" style="background-color:#fff;">
  <div class="ib2-inner">
    <h3 class="ib2-content--title">@lang('front.leave_comments')</h3>
       <div class="form-group">
          <div class="col-md-6">
            <label for="name" class="required control-label">@lang('front.name') <span class="required">*</span></label>
            {!!Form::text('name', null, ['id'=>'cmnt_name','required'=>'required', 'class'=>'form-control'])!!}
          </div>
          <div class="col-md-6">
            <label for="email" class="required control-label">@lang('front.email') <span class="required">*</span></label>
            {!!Form::email('email', null, ['id'=>'cmnt_email','required'=>'required', 'class'=>'form-control'])!!}
          </div>
       </div>
       <div class="form-group">
          <div class="col-md-12">
            <label for="comment" class="required control-label">@lang('front.comment') <span class="required">*</span></label>
            {!!Form::textarea('comment', null, ['required'=>'required','rows'=>5, 'class'=>'form-control'])!!}
          </div>
       </div>
       <div class="form-group" style="margin-bottom: 0;">
          <div class="col-md-12">
            {!!Form::submit(trans('front.addcomment'), ['class'=>'btn btn-warning', 'id'=>'mybtn'])!!}
            <span class="note">@lang('front.fields') <span class="required">*</span>@lang('front.required').</span>
          </div>
       </div>
    </div>
  </div>
</div>
{!!Form::close()!!}