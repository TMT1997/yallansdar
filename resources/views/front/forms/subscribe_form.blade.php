{!!Form::open(['url'=>'interact/subscribe', 'method'=>'post','class'=>'form-inline'])!!}
    <div class="form-group">
        <label class="control-label">{{trans('front.keepme_informed')}}</label>
    </div>
    <div class="form-group">
        {{Form::text('name',null, ['placeholder'=>trans('front.yourname'),'class'=>'form-control input-sm','required'=>'required'])}}
    </div>
    <div class="form-group">
        {{Form::text('email',null,['placeholder'=>trans('front.youremail'),'class'=>'form-control input-sm','required'=>'required'])}}
    </div>
    <div class="form-group">{{Form::submit(trans('front.subscribe'), ['class'=>'btn btn-sm btn_subscribe'])}}</div>
{!!Form::close()!!}