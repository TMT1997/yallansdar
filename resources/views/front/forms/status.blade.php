@extends('front.layouts.master')

@section('content')
	<ul class="breadcrumb">
		<li><a href="{{url('/')}}" class="home">@lang('front.home')</a></li>
		<li>@lang('front.status')</li>
	</ul>
	
	<h1 class="heading_primary">@lang('front.status')</h1>
	
	<section class="content-area">
		<div class="row">
			<div class="site-main col-sm-12 alignleft">
				<div class="alert alert-{{$status}}">
					{!!$message!!}
				</div>
			</div>
		</div>
	</section>
@endsection