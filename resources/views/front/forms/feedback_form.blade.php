{!!Form::open(['url'=>'interact/feedback', 'method'=>'post','class'=>'pull-right'])!!}
	{!!Form::hidden('type', 'inquiry')!!}
	{!!Form::hidden('tag', 'Side Feedback')!!}
	<div class="form-group">
        <label class="control-label">@lang('front.name') <span class="required">*</span>
        {{Form::text('name',null, ['placeholder'=>trans('front.ent_ur_name'),'class'=>'form-control','required'=>'required'])}}
        </label>
    </div>
    <div class="form-group">
        <label class="control-label">@lang('front.email') <span class="required">*</span>
        {{Form::email('email',null, ['placeholder'=>trans('front.ent_ur_email'),'class'=>'form-control','required'=>'required'])}}
        </label>
    </div>
    <div class="form-group">
        <label class="control-label">@lang('front.phone') <span class="required">*</span>
        {{Form::text('phone',null, ['placeholder'=>trans('front.ent_ur_mobile'),'class'=>'form-control','required'=>'required'])}}
        </label>
    </div>
    <div class="form-group">
        <label class="control-label">@lang('front.message') <span class="required">*</span>
        {{Form::textarea('message',null, ['placeholder'=>trans('front.ent_ur_message'),'class'=>'form-control','required'=>'required', 'rows'=>4])}}
        </label>
    </div>
    <div class="form-group">
        {{Form::submit(trans('front.send'),['class'=>'btn btn-primary'])}}
    </div>
{!!Form::close()!!}