@extends('front.layouts.master')

@section('content')
	
	<div class="container">
		<div class="well">
			<div class="alert alert-{{($status)?$status:'success'}} text-center">
				{!!$message!!}
			</div>
		</div>
	</div>

@endsection