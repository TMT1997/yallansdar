<?php
    $prod_with = [
        'image'=>['Image', 'fa fa-camera'],
        'video'=>['Video', 'fa fa-film'],
        'deal'=>['Deal', 'fa fa-tag'],
        'rating'=>['Rating', 'fa fa-star'],
    ];
?>
<aside class="sid-filtr-hldr">
	<h1 class="grn-mid-hdr">{{trans('front.categories')}}</h1>
    <ul class="nrml-lst">
        <h5>{{$category->$c_name}}</h5>
        @foreach($categories_tree as $cat)
            <li><a href="{{App\Category::url($cat)}}">{{$cat->$c_name}}</a>
        @endforeach
    </ul>

    <h5 class="grn-mid-hdr">{{trans('front.supplier_locations')}}</h5>
    <ul>
    	<li>
        	<input type="checkbox" id="cunt-all" value="All Locations" />
            <label for="cunt-all">{{trans('front.all_locations')}}</a>
        </li>
        <li>
        	<input type="checkbox" id="cunt-egypt" value="Egypt" />
            <label for="cunt-egypt">Egypt</a>
        </li>
    </ul>

    <h5 class="grn-mid-hdr">{{trans('front.show_prods_with')}}</h5>
    <ul>
        @foreach($prod_with as $key=>$info)
    	<li>
            <label for="with-{{$key}}">
        	   <input type="checkbox" id="with-{{$key}}" value="{{$key}}" />
                <i class="fa fa-{{$info[1]}}"></i> {{trans('front.'.$key)}}
            </label>
        </li>
        @endforeach
    </ul>

    <h5 class="grn-mid-hdr">{{trans('front.supplier_businesstype')}}</h5>
    <ul>
    	<li>
        	<input type="checkbox" id="manf-all" value="All Manufacture" />
            <label for="manf-all">{{trans('front.all')}}</a>
        </li>
        {{-- <li>
        	<input type="checkbox" id="manfecturs-num" value="Manufacture nummber" />
            <label for="manfecturs-num">Manufacture (8)</a>
        </li> --}}
    </ul>
</aside>