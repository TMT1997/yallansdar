<?php
	use App\Category;
	use App\Product;
    use App\Helper;
    use App\User;
	$parents = array_reverse($category_parents);
?>
@extends('front.layouts.master')
@section('title') {{$category->$c_name}} | @endsection
@section('content')

<div class="inner-pg-cntnr wht-bg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                @include('front.categories.sidebar_filter')
			</div>
			<div class="col-xs-12 col-md-8">
				<header><h1 class="hdr-styl-001 inr-hdr-blk">{{$category->$c_name}}</h1></header>
				<p class="nrml-p">
                    {!!$category->description!!}
				</p>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
    					<div class="breadcumb-list brcrmb-inr">
    						<ul class="">
    						  	<li><a href="{{url('home')}}">@lang('front.home')</a></li>
                                @if($parents)
        						  	@foreach($parents as $cat)
                                        @if($cat)
        								    <li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a></li>
                                        @endif
        							@endforeach
                                @endif
    							<li>{{$products->count()}} {{trans('front.products')}}</li>
    						</ul>
    					</div>
                    </div>

                    <div class="col-xs-12 col-md-3">
                        <div class="dataTables_length">
                            <label>{{trans('front.show')}}
                            <select name="example_length" aria-controls="example" class="form-control input-sm custom-show-slct">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> {{trans('front.entries')}}</label>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-12 top-tabl-op-bar">
                    <div class="col-xs-12 col-md-7 fltrs">
                        {{trans('front.sort_by')}}: <label><input type="checkbox" id="verified" data-page="1">{{trans('front.verified')}}</label>
                    </div>
                    <div class="col-xs-12 col-md-5 rt-tp-optons">
                        <div id="next-bttn"></div>
                        <div id="prv-bttn"></div>
                        <div class="pag-stt-info"></div>
                        <div class="grd-srt-btn"></div>
                        <div class="rows-srt-btn actv-grid"></div>
                        {{trans('front.view_by')}}:
                    </div>
                </div>
                <table id="data-tabl-tbl" class="table" cellspacing="0" width="100%">
                    <thead><tr><th></th></tr></thead>
                    <tfoot><tr><th></th></tr></tfoot>
                    <tbody>
                    	@foreach($products as $product)
                        <?php
                            $profile = (isset($product->user->profile)) ? $product->user->profile:null;
                        ?>
                        <tr>
                            <td>
                                <div class="row pro-thumb-row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 img-ttl-col">
                                        <a class="img-hldr-product" href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                                        <a href="{{Product::url($product)}}" class="prduct-thumb-ttl">{{Helper::field($product,'name')}}</a>
                                        <p class="pro-thumb-sumry">{{str_limit(Helper::field($product,'description'),160)}}</p>
                                        <div class="rqst-qution-hldr">
                                            <a href="{{Product::url($product)}}" class="grn-btn">{{trans('front.request_for_quote')}}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  options-col">
                                        <span class="produc-by">{!!User::isVerified($product->user, false)!!}
                                        <a href="{{App\Company::products_url($product->user)}}">{{@$profile->company_name}}</a></span>
                                        <span>{!!User::flag($product->user,[],true)!!}</span>
                                        {{--<span class="b-frst-t-rvu"><i class="fa fa-star-o"></i> <a href="{{Product::url($product)}}#details">@lang('front.befirst_to_review')</a></span>--}}
                                        <span class="b-frst-t-rvu"><i class="fa fa-star-o popup"></i> <a class="popup" href="{{url('reviews/add/'.$product->id.'/product')}}">@lang('front.befirst_to_review')</a></span>

                                        <span class="cntct-dtlls">
                                            {{--<a href="{{User::contact_info($product->user)}}"><i class="fa fa-paper-plane bluu"></i> @lang('front.contact_details')</a>--}}
                                            {!!Form::open(['url'=>'cart'])!!}
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <button type="submit" class="btn bg-white"  style="background: white; font-size: 12px; padding: 3px;"><i class="fa fa-paper-plane bluu"></i> @lang('front.request_for_quote')</button>
                                            {!!Form::close()!!}
                                        </span>

                                        <div>
                                            <a ><i class="fa fa-camera"></i></a>
                                            <a href="#d"><i class="fa fa-film"></i></a>

                                        </div>
                                    </div>
                                    <div class="rqst-qution-hldr scnd-btn">
                                        {!!Form::open(['url'=>'cart'])!!}
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <button type="submit" class="grn-btn add-to-cart"><i class="fa fa-shopping-cart"></i> {{trans('front.request_for_quote')}}</button>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
			</div>

            <aside class="col-xs-12 col-md-2 sponsored_products">
                <header><div class="hdr-styl-asid">{{trans('front.sponsored_products')}}</div></header>
                @foreach($products_sponsored as $product)
	                <figure class="asid-prdct-itm">
	                    <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
	                    <figcaption><h4><a href="#">{{Helper::field($product,'name')}}</a></h4></figcaption>
	                </figure>
                @endforeach
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection

@section('script')
    @include('dashboard.layouts.popup')
@endsection