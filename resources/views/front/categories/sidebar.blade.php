<?php use App\Category; ?>
<nav class="sdMnu innr-sd-mnu">
    <header><h2>{{trans('front.categories')}}</h2></header>
    <ul class="sid-mnu subcats">
        @foreach($categories_tree->take(36) as $category)
            <li class="lvl-one-mnu-itm" style="position: relative;">
                <a href="{{Category::url($category)}}" class="{{($category->childs->count()>0)?'parent':''}}">{{$category->$c_name}} 
                @if($category->childs->count()>0)
                    </a>
                    <ul class="sub-lvl-two-itm" style="min-width:265px;height:inherit;position:absolute;top:-1px;width:auto;">
                        <li><h4>{{$category->$c_name}}</h4></li>
                        <li class="sub-sub-itms">
                            <ul>
                            @foreach($category->childs as $sub_cat)
                                <li style="width:100%"><a href="{{Category::url($sub_cat)}}">{{$sub_cat->$c_name}}</a></li>
                            @endforeach
                            </ul>
                        </li>
                    </ul>
                @else
                    </a>
                @endif
            </li>
        @endforeach
    </ul>
</nav>