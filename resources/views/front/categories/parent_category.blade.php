<?php
	use App\Category;
    use App\Product;
    use App\Company;
    use App\Helper;
    use App\User;
    use App\Service;
?>
@extends('front.layouts.master')
@section('title') {{$category->$c_name}} | @endsection
@section('content')
<div class="container">
    <div class="row">
    	<ul class="breadcrumb">
    	  <li><a href="{{url('home')}}">@lang('front.home')</a></li>
          <li><a href="{{url('category')}}">@lang('front.categories')</a></li>
    	  <li>{{$category->$c_name}}</li>
    	</ul>
    </div>
</div>
<div class="inner-pg-cntnr">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-2">
                @include('front.categories.sidebar')
			</div>
			<div class="col-xs-12 col-md-8">				
				<header><h1 class="hdr-styl-001 inr-hdr-blk">{{$category->$c_name}}</h1></header>
				<p class="nrml-p">
                    {!!$category->description!!}
				</p>
                @if(!empty($category->banner))
                    {!!Helper::image2($category->banner, ['class'=>'ful-wdth-img'], false)!!}
                @endif

                <div class="innr-ftshrd-sup">
                    <header class="hdr-styl-02">
                        <a href="{{Product::cat_url($category)}}"><h4>@lang('front.hot_products')</h4></a>
                    </header>
                    <div class="row lstng-stly-02">
                        @foreach($products->take(12) as $product)
                            <figure class="col-xs-12 col-sm-6 col-md-2 asid-prdct-itm">
                                <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                                <figcaption>
                                    <h4><a href="{{Product::url($product)}}">{{Helper::field($product,'name')}}</a></h4>
                                    <p><span>@lang('front.moq'): </span>{{$product->min_quantity}} {{trans('dash.units')[$product->min_quantity_unit]}}</p>
                                </figcaption>
                            </figure>
                        @endforeach
                        @if($products->count()==0)
                            <div class="col-md-12"><div class="alert alert-info">@lang('front.no_products')</div></div>
                        @endif

                        <div class="col-md-12">
                            <div style="margin-bottom: 10px;" class="text-left">
                            <a href="{{Product::cat_url($category)}}" class="btn btn-primary btn-sm">@lang('front.more')</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="innr-ftshrd-sup">
                    <div class="featured-tg"></div>
                    <header class="hdr-styl-02"><h4>@lang('front.featured_suppliers')</h4></header>
                    <div class="row lstng-stly-02">
                        @foreach($suppliers as $supplier)
                            @if(!$supplier->profile)
                            <?php continue; ?>
                            @endif
                            <figure class="col-xs-12 col-sm-6 col-md-4">
                                <div>
                                    <a href="{{User::website($supplier)}}" class="img-lnk-hldr">{{Company::logo($supplier)}}</a>
                                    <figcaption>
                                        <h3><span>{!!User::flag($supplier)!!}</span></h3> <a href="{{User::website($supplier)}}">{{@$supplier->profile->company_name}}</a>
                                        <p><span>@lang('front.main_products'):</span> {{str_limit(str_replace(',',', ',$supplier->profile->main_products),45)}}</p>
                                    </figcaption>
                                </div>
                            </figure>
                        @endforeach
                    </div>
                    @if($suppliers->count()==0)
                        <div class="alert alert-info">@lang('front.no_products')</div>
                    @endif
                </div>

                <div class="innr-ftshrd-sup" style="margin-bottom: 10px;">
                    <div class="featured-tg"></div>
                    <header class="hdr-styl-02">
                        <a href="{{Product::cat_url($category)}}"><h4>@lang('front.featured_products')</h4></a>
                    </header>
                    @foreach($products->take(12) as $product)
                        <figure class="col-xs-12 col-sm-6 col-md-2 asid-prdct-itm">
                            <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                            <figcaption>
                                <h4><a href="{{Product::url($product)}}">{{Product::name($product)}}</a></h4>
                                <p><span>@lang('front.moq'): </span>{{Product::MOQ($product)}}</p>
                            </figcaption>
                        </figure>
                    @endforeach
                    @if($products->count()==0)
                        <div class="alert alert-info">@lang('front.no_products')</div>
                    @endif
                    <br class="clr-bth" />
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-bottom: 10px;" class="text-left">
                            <a href="{{Product::cat_url($category)}}" class="btn btn-primary btn-sm">@lang('front.more')</a>
                            </div>
                        </div>
                    </div>
                </div>

                @if($services->count()>0)
                    <div class="innr-ftshrd-sup" style="margin-bottom: 10px;">
                        <header class="hdr-styl-02"><h4>@lang('front.related_services')</h4></header>
                        <div class="row">
                            @foreach($services as $service)
                            <?php $service_lang = Helper::lang_data($service, $lng_id); ?>
                            <div class="col-md-6">
                                <div class="media">
                                    <a class="pull-right" style="padding-right:0" href="{{service::url($service)}}">
                                        {{Service::Image($service,['class'=>'media-object', 'style'=>'max-width:100px;'])}}
                                    </a>
                                    <div class="media-body">
                                        <a href="{{service::url($service)}}"><strong class="media-heading">{{$service_lang->name}}</strong></a>
                                        <p style="font-size: 13px;">{{str_limit($service_lang->description, 90)}}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                <br class="clr-bth" />
			</div>

            <aside class="col-xs-12 col-md-2 sponsored_products">
                <header><p class="hdr-styl-asid">@lang('front.sponsored_products')</p></header>
                @foreach($products->take(10) as $product)
                    <figure class="asid-prdct-itm">
                        <a href="{{Product::url($product)}}">{{Product::image($product)}}</a>
                        <figcaption><h4><a href="{{Product::url($product)}}">{{Product::name($product)}}</a></h4></figcaption>
                    </figure>
                @endforeach
                @if($products->count()==0)
                    <div class="alert alert-info">@lang('front.no_products')</div>
                @endif
            </aside>
			<br class="clr-bth" />
		</div>
	</div>
</div>
@endsection