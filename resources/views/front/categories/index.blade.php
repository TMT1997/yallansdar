<?php
	use App\Category;
?>
@extends('front.layouts.master')
@section('title') @lang('front.categories') | @endsection
@section('content')
<div class="container">
	<div class="row cat_directory">
		@foreach($categories as $category)
			<div class="col-md-4" style="min-height: 250px;">
				<legend><a href="{{Category::url($category)}}">{{$category->$c_name}}</a>
					<span style="font-size: 12px; color: #999">({{$category->childs->count()}})</span>
				</legend>

				<div class="listing">
					@foreach($category->childs->take(17) as $sub_cat)
						<a href="{{Category::url($sub_cat)}}">{{$sub_cat->$c_name}}</a>
						<span class="sept">|</span>
					@endforeach
				</div>

				<p style="text-align: right;"><a href="{{Category::url($category)}}">@lang('front.see_all')</a></p>
			</div>
		@endforeach
	</div>
</div>
@endsection