@extends('front.layouts.master')
@section('title') {{$page->title}} | @endsection
@section('content')
	
	<div class="container">
		<legend>{{$page->title}}</legend>
		<h4>{{$page->subtitle}}</h4>
		
		<div class="text-center">
			{!!App\Helper::image2($page->image)!!}
		</div>

		<div class="text">
			{!!$page->details!!}
		</div>

	</div>

@endsection