<?php
	use App\Helper;
	use App\Blog;
	use Jenssegers\Date\Date;
?>
@extends('front.layouts.master')
@section('title') @lang('front.blog') | {{Helper::field($item,'title')}} @endsection

@section('fb_meta')
    <meta property="og:title" content="{{Helper::field($item,'title')}}">
    <meta property="og:image" content="{!!url('uploads/'.$item->image)!!}">
    <meta property="og:description" content="{!!str_limit(strip_tags(Helper::field($item,'details')), 400)!!}">
    <meta property="og:url" content="{{Blog::url($item)}}">
@endsection

@section('content')
<div class="page-subheader">
	<div class="container">
		<ul class="breadcrumb fixclear">
			<li><a href="{{url('/')}}">@lang('front.home')</a></li>
			<li><a href="{{url('blog')}}">@lang('front.blog')</a></li>
			<li>{{Helper::field($item,'title')}}</li>
		</ul>
	</div>
</div>

<section class="hg_section ptop-50">          
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div id="th-content-post">
					<h1 class="page-title black">{{Helper::field($item,'title')}}</h1>
					<div class="itemView clearfix eBlog">
						<div class="itemHeader">
							<div class="post_details">
								<span class="itemAuthor">
									@lang('front.by') <a href="#" rel="author"><strong>{{Helper::field($item->author,'name')}}</strong></a>
								</span>
								<span class="infSep"> / </span>
								<span class="itemDateCreated"><span class="glyphicon glyphicon-calendar"></span> {{Date::parse($item->created_at)->format('l j F Y')}}</span>
								<span class="infSep"> / </span>
								<span class="itemCommentsBlock"></span>
								<span class="itemCategory">
									<span class="glyphicon glyphicon-folder-close"></span>
									@lang('front.published_in')
								</span>
								<a href="{{Blog::cat_url($item->category)}}"><strong>{{Helper::field($item->category,'title')}}</strong></a>
							</div>
							<hr>
							<div class="addthis_inline_share_toolbox"></div>
							<hr>

						</div>

						<div class="itemBody" style="line-height: 30px;">
							<a data-lightbox="image" href="images/ipad-605439_640.jpg" class="hoverBorder pull-left" style="margin-right: 20px;margin-bottom:4px;">
								<span class="hoverBorderWrapper">
									{!!Helper::image2($item->image,['style'=>'width:400px'],false)!!}
									<span class="theHoverBorder"></span>
								</span>
							</a>
							{!!Helper::field($item, 'details')!!}
						</div>
						<div class="clear"></div>
						<div class="addthis_inline_share_toolbox"></div>

						<div class="itemTagsBlock">
							@if(!empty($item->tags))
								<hr>
								<span class="text-primary">@lang('front.tags'):</span>
								<?php $tags_arr = explode(',', $item->tags); ?>
								<div class="itemTagsBlock">
									@foreach($tags_arr as $tag)
										<a href="{{Blog::tag_url($tag)}}" class="btn btn-xs btn-default">{{$tag}}</a>,
									@endforeach
								</div>
							@endif
						</div>
						<hr>

						@if($read_also->count() > 0)
							<div class="related-articles">
								<h3 class="rta-title">@lang('front.read_also')</h3>
								<div class="row">
									@foreach($read_also->take(4) as $post)
									<div class="col-sm-3">
										<div class="rta-post">
											<a href="{{Blog::url($post)}}">{!!Helper::image2($post->image,['class'=>'img-responsive','style'=>'width:370px;height:240'],false)!!}</a>
											<h5><a href="{{Blog::url($post)}}">{{Helper::field($post,'title')}}</a></h5>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						@endif
					</div>
					<hr>
					<div class="comment-form-wrapper">
						@include('front.forms.commentform', ['id'=>$item->id, 'model'=>'Blog'])
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				@include('front.blog.sidebar')
			</div>
		</div>
	</div>
</section>
@endsection