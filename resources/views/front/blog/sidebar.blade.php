<?php
	use App\Blog;
	use App\Helper;
	use Jenssegers\Date\Date;
?>
<div id="sidebar-widget" class="sidebar">
	<div class="search gensearch__wrapper">
		<form id="searchform" class="gensearch__form" action="{{url('search')}}" method="get" onSubmit="Gsitesearch(this)" target="_blank">
			<div class="input-group">
				<input name="q" maxlength="20" class="form-control" type="text" placeholder="@lang('front.search.')">
				<span class="input-group-btn">
					<button type="submit" value="go" class="btn btn-primary gensearch__submit glyphicon glyphicon-search"></button>
				</span>
			</div>
		</form>
	</div>
	<hr>

	@if($featured_posts->count() > 0)
		<div class="widget widget_recent_entries">
			<div class=" latest_posts style3">
				<h3 class="widgettitle title">@lang('front.featured_posts')</h3>
				<div class="row">
					@foreach($featured_posts as $fpost)
					<div class="col-md-12" style="margin-bottom: 10px;">
						<a href="{{Blog::url($fpost)}}" class="hoverBorder pull-left">
							{!!Helper::image2($fpost->image,['style'=>'width:50px'],false)!!}
						</a>
						<div class="title"><a href="{{Blog::url($fpost)}}">{{Helper::field($fpost,'title')}}</a></div>
						<small class="light">0 @lang('front.comments')</small>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif

	<hr>
	<div class="widget widget_categories">
		<h3 class="widgettitle title">@lang('front.categories')</h3>
		<ul class="menu">
			@foreach($blog_cats as $cat)
			<li class="cat-item"><a href="{{App\BlogCategory::url($cat)}}">{{Helper::field($cat,'title')}}</a></li>
			@endforeach
		</ul>
	</div>
	<hr>
	<div class="widget widget_meta">
		<h3 class="widgettitle title">@lang('front.tags')</h3>
		<?php $ctags = 0; ?>
		@foreach($blog_tags as $row)
			@if($ctags==8) <?php break; ?> @endif
			@if(!empty($row->tags))
				<?php $tags_arr = explode(',', $row->tags); ?>
				@foreach($tags_arr as $tag)
					<a href="{{Blog::tag_url($tag)}}" class="btn btn-default btn=sm">{{$tag}}</a>
				@endforeach
				<?php $ctags++; ?>
			@endif
		@endforeach
	</div>
</div>