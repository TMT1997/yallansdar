<?php
	use App\Helper;
	use App\Blog;
	use Jenssegers\Date\Date;
?>
@extends('front.layouts.master')
@section('title') @lang('front.blog') @endsection
@section('content')
<div id="page_header" class="page-subheader site-subheader-cst blog_pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="subheader-titles">
					<h2 class="text-warning">@lang('front.blog')</h2>
					<h4 class="subheader-subtitle">@lang('front.blog_hint')</h4>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="hg_section ptop-50">          
	<div class="container">
		<div class="row">					
			<div class="col-md-9 col-sm-9">
				@if($blog && $blog->count() > 0)
				<div class="itemListView clearfix eBlog">
					<div class="itemList">
						@foreach($blog as $post)
							<div class="itemHeader">
								<h3 class="itemTitle"><a href="{{Blog::url($post)}}">{{Helper::field($post,'title')}}</a></h3>
								<div class="post_details">
									<span class="catItemDateCreated">
									{{Date::parse($post->created_at)->format('l j F Y')}} </span> |
									<span class="catItemAuthor">@lang('front.by') <a href="#">{{Helper::field($post->author,'name')}}</a></span> |
									<span>@lang('front.published_in')</span> <a href="{{Blog::cat_url($post)}}">{{Helper::field($post->category,'title')}}</a>
								</div>
							</div>
							<div class="media">
								<a href="{{Blog::url($post)}}" class="pull-right">
									{!!Helper::image2($post->image,['class'=>'media-object','style'=>'width:110px'])!!}
								</a>
								<div class="media-body">
									<p>{!!str_limit(strip_tags(Helper::field($post, 'details')), 350)!!}</p>
									
									<div class="itemBottom clearfix">
										@if(!empty($post->tags))
										<?php $tags_arr = explode(',', $post->tags); ?>
										<div class="itemTagsBlock">
											@foreach($tags_arr as $tag)
											<a href="{{Blog::tag_url($tag)}}" class="btn btn-default btn-xs">{{$tag}}</a>
											@endforeach
											<div class="clear"></div>
										</div>
										@endif
									</div>
								</div>
							</div>
							<hr />
						@endforeach
					</div>

					<div class="paging">
					{!!$blog->links()!!}
					</div>
				</div>
				@else
					<div class="alert alert-warning">@lang('front.no_articles_found')</div>
				@endif
			</div>
			<div class="col-md-3 col-sm-3">
				@include('front.blog.sidebar')
			</div>	
		</div>
	</div>
</section>
@endsection