<?php
use App\User;
use App\Category;
use App\Product;
use App\Service;
use App\Company;
use App\Helper;
use App\Deals\Deal;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use App\Deals\Partnership;
use App\Blog;
use Jenssegers\Date\Date;
?>
@extends('front.layouts.master')

@section('fb_meta')
<style type="text/css">
	@media only screen and (max-width: 768px) {
	    .sid-mnu{
	        margin: 60px;
	    }
	}
</style>
@endsection

@section('content')
	<div class="container" >
		<div class="row">
			<div class="col-xs-12 col-md-9 hp_bodywrap">
				<nav class="navbar navbar-default mobile_res" role="navigation">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-cats">
								<span class="sr-only">Toggle navigation</span>
								<span class="cats_toggle"><i class="fa fa-bars"></i> @lang('front.categories')</span>
							</button>
						</div>
						<div class="collapse navbar-collapse navbar-cats">
							<ul class="nav navbar-nav">
								@foreach($categories->slice(0,13) as $category)
									<li><a href="{{Category::url($category)}}">{{$category->$c_name}}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</nav>
				<nav class="col-xs-12 col-md-3 sdMnu cats_pc_menu" id="sections-aside">
					<header><h2><a href="{{url('category')}}">@lang('front.categories')</a></h2></header>
					<ul class="sid-mnu">
						@foreach($categories->slice(0,13) as $category)
                            <?php $menuimage=(App::getLocale('yalla_locale')=='ar')?$category->menuimage_ar:$category->menuimage; ?>
							<li class="lvl-one-mnu-itm">
								<a href="{{Category::url($category)}}" class="parent">{{$category->$c_name}}</a>
								<ul class="sub-lvl-two-itm" style="min-width: 550px;height: 492px">
									<li style="position:relative; z-index: 99999;"><h4>{{$category->$c_name}}</h4></li>
									<li class="sub-sub-itms" style="position:relative; z-index: 99999">
										<ul class="ul_submenu">
											@foreach($category->childs->slice(0,12) as $sub_cat)
												<li><a href="{{Category::url($sub_cat)}}">{{$sub_cat->$c_name}}</a></li>
											@endforeach
											<li style="width:100%;clear:both;"><a href="{{Category::url($category)}}"><strong>@lang('front.more_cats')</strong></a></li>
										</ul>
										<ul class="ul_submenu">
											@foreach($category->childs->slice(12,6) as $sub_cat)
												<li><a href="{{Category::url($sub_cat)}}">{{$sub_cat->$c_name}}</a></li>
											@endforeach
										</ul>
									</li>
									<li class="cat-img">{!!Helper::image2($menuimage,['class'=>'lazy','data-original'=>''],false)!!}</li>
								</ul>
							</li>
						@endforeach
						<li class="lvl-one-mnu-itm">
							<a href="{{url('category')}}" class="parent"><strong>@lang('front.more_cats')</strong></a>
							<ul class="sub-lvl-two-itm" style="min-width: 550px;height: 492px">
								<li style="position:relative; z-index: 99999;"><h4>@lang('front.more_cats')</h4></li>
								<li class="sub-sub-itms" style="position:relative; z-index: 99999">
									<ul>
										@foreach($categories->slice(13) as $cat)
											<li><a href="{{Category::url($cat)}}">{{$cat->$c_name}}</a></li>
										@endforeach
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
				<div class="col-xs-12 col-md-9 grbd-itms" id="toggle-col">
					{{-- start of Main Slider that should be removed--}}
						<div class="fotorama" data-width="100%" data-click="false" data-ratio="751/232" data-autoplay="true">
						<?php  $counter = 0;?>
						@foreach($slider as $item)
							<?php $image=(App::getLocale('yalla_locale')=='ar')?$item->image_ar:$item->image;
								if(empty($image)) {
								$image = $item->image;
								}
							?>
							@if($counter == 0)
								<div data-img="{{url('uploads/'.$image)}}"><a href="http://www.cairofashiontex.com/" target="http://www.cairofashiontex.com/"></a></div>
							@elseif($counter == 1)
								<div data-img="{{url('uploads/'.$image)}}"><a href="http://www.cairokidsmother.com/" target="http://www.cairokidsmother.com/"></a></div>
							@else
								<div data-img="{{url('uploads/'.$image)}}"><a href="#"></a></div>
							@endif

							<?php $counter++?>
						@endforeach
						</div>
					{{-- The End of Main Slider that should be removed--}}

					<section class="top_categories">
						<header>
							<a href="{{url('فرص-تصدير')}}"><h2 class="hdr-styl-01">@lang('front.top_export_chances')</h2></a>
						</header>
						<div class="row">
							<div class="col-md-12" >
								@foreach($export_categories as $ex_category)
                                    <?php
                                    $category = $ex_category->parent_category;
                                    if(!$category) {
                                        continue;
                                    }
                                    ?>
									<div class="col-xs-6 col-sm-4 col-md-3 item">
										<a href="{{QuoteRequest::category_url($category)}}" class="thumbnail">
											{!!Helper::image2($category->image, [])!!}
											<div class="content">
												<strong>{{$category->$c_name}}</strong> <br>
												<small>({{$ex_category->total_rfq}} @lang('front.export_oppt'))</small>
											</div>
										</a>
									</div>
								@endforeach
							</div>
						</div>
					</section>
				</div>
				
					
						<section class="hot_products">
						<header><a href="{{url('products')}}"><h4>@lang('front.hot_products')</h4></a></header>
						<div class="owl-itms01 owl-carousel owl-theme owl-btn-brdr">
							@foreach($hot_products->slice(0,10) as $product)
								<div class="item">
									<a href="{{Product::url($product)}}" >
										<span class="img-hldr">{{Product::image($product)}}</span>
										<span>{{Helper::field($product, 'name')}}</span>
									</a>
								</div>
							@endforeach
						</div>

						<div class="owl-itms01 owl-carousel owl-theme owl-btn-brdr">
							@foreach($hot_products->slice(10,10) as $product)
								<div class="item">
									<a href="{{Product::url($product)}}">
										<span class="img-hldr">{{Product::image($product)}}</span>
										<span>{{Helper::field($product, 'name')}}</span>
									</a>
								</div>
							@endforeach
						</div>
					</section>
					
				
				<!-- Services -->
				@if($services->count()>0)
				
					<section class="hot_services">
						<header><h4>@lang('dash.services')</h4></header>
						<div class="row">
							@foreach($services as $service)
                                <?php $service_lang = Helper::lang_data($service, $lng_id); ?>
								<div class="col-md-4">
									<div class="media">
										<a class="pull-right" style="padding-right:0" href="{{service::url($service)}}">
											{{Service::Image($service,['class'=>'media-object', 'style'=>'max-width:100px;'])}}
										</a>
										<div class="media-body">
											<a href="{{service::url($service)}}"><strong class="media-heading">{{$service_lang->name}}</strong></a>
											<p style="font-size: 13px;">{{str_limit($service_lang->description, 90)}}</p>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</section>
				
					
				@endif

				<!--News * events-->
				
					<section class="featured_suppliers" id="news">
						<header class="hdr-styl-02">
							<a><h4>@lang('front.news')</h4></a>
						</header>
						<div class="row lstng-stly-02">
									<figure class="col-xs-12 col-sm-6 col-md-4">
										<div>
											<a style="width:150px ; height: 80px" href="http://www.cairofashiontex.com/" target="http://www.cairofashiontex.com/" class="img-lnk-hldr">
											<img src="uploads/cairo_fashion.png"></a>
											<figcaption>
												<h3>
													<a href="http://www.cairofashiontex.com" target="http://www.cairofashiontex.com">@lang('front.cairo_fashion')
														</a>
													<span></span>
												</h3>

													<p><span>@lang('front.cairo_fashion_date') </span><br>
														مركز مصر للمعارض الدولية –محور المشير / التجمع الخامس
														</p>

												<a href="http://www.cairofashiontex.com" target="http://www.cairofashiontex.com" class="vw-mr-btn">المعرض الدولى للملابس الجاهزة و مستلزمات الأنتاج </a>
											</figcaption>
										</div>
									</figure>
									<figure class="col-xs-12 col-sm-6 col-md-4" >
										<div>
											<a style="width:150px ; height: 80px" href="http://www.cairokidsmother.com/" target="http://www.cairokidsmother.com/" class="img-lnk-hldr">
											<img src="uploads/cairo_mother_kids.png"></a>
											<figcaption>
												<h3>
													<a href="http://www.cairokidsmother.com" target="http://www.cairokidsmother.com">
														@lang('front.cairo_mother')
														</a>
													<span></span>
												</h3>

													<p><span>@lang('front.cairo_mother_date')
													</span><br>
														مركز مصر للمعارض الدولية –محور المشير / التجمع الخامس</p>

												<a href="http://www.cairokidsmother.com" target="http://www.cairokidsmother.com" class="vw-mr-btn">المعرض الدولى لملابس الطفل و الأم و مستلزماتهم</a>
											</figcaption>
										</div>
									</figure>
	                                <figure class="col-xs-12 col-sm-6 col-md-4">
	                                    <div>
	                                        <a style="width:150px ; height: 80px" href="http://www.cairokidsmother.com/" target="http://www.cairokidsmother.com/" class="img-lnk-hldr">
	                                            <img src="uploads/cairo_mother.png"></a>
	                                        <figcaption>
	                                            <h3>
	                                                <a href="http://www.cairokidsmother.com" target="http://www.cairokidsmother.com">
	                                                    @lang('front.cairo_mother_only')
	                                                </a>
	                                                <span></span>
	                                            </h3>

	                                            <p><span>@lang('front.cairo_mother_date')
	                                                        </span><br>
	                                                مركز مصر للمعارض الدولية –محور المشير / التجمع الخامس</p>

	                                            <a href="http://www.cairokidsmother.com" target="http://www.cairokidsmother.com" class="vw-mr-btn">معرض ملابس البيت الحريمى و اللانجيرى ( هوم وير )</a>
	                                        </figcaption>
	                                    </div>
	                                </figure>
						</div>
					</section>
			

				<!--end news & events section-->
				
					<section class="featured_suppliers">
						<header class="hdr-styl-02">
							<a href="{{url('exporters')}}"><h4>@lang('front.featured_suppliers')</h4></a>
						</header>
						<div class="row lstng-stly-02">
							@foreach($suppliers as $supplier)
								@if(isset($supplier->profile))
									<figure class="col-xs-12 col-sm-6 col-md-4">
										<div>
											<a href="{{User::contact_info($supplier)}}" class="img-lnk-hldr">{!!Helper::image2($supplier->profile->picture,[])!!}</a>
											<figcaption>
												<h3>
													<a href="{{User::contact_info($supplier)}}">
														{{($supplier->profile->company_name)?$supplier->profile->company_name:$supplier->name}}</a>
													<span>{!!User::flag($supplier)!!}</span>
												</h3>
												@if(!empty($supplier->profile->main_products))
													<p><span>@lang('front.main_products'):</span>
														{{str_replace(',',', ', str_limit($supplier->profile->main_products, 75))}}</p>
												@endif
												<a href="{{Company::products_url($supplier)}}" class="vw-mr-btn">@lang('front.products')</a>
											</figcaption>
										</div>
									</figure>
								@endif
							@endforeach
						</div>
						<a href="{{url('exporters')}}" class="vw-mr-btn pull-left">@lang('front.exporters_list')</a>
					</section>
				
				<section class="latest_deals">
					{{--<header class="hdr-styl-02"><h4>@lang('front.latest_deals')</h4></header>--}}
					{{--<div class="row deals-row">--}}
						{{--<article class="col-xs-12 col-sm-6 col-md-4">--}}
							{{--<header class="hdr-styl-03">--}}
								{{--<a href="{{Deal::$types['offers']['url']}}" class="tag">SO</a>--}}
								{{--<h4><a href="{{Deal::$types['offers']['url']}}">@lang('front.sell_offers')</a></h4>--}}
								{{--<p>@lang('front.sell_offer_desc')</p>--}}
							{{--</header>--}}
							{{--@foreach($deals['sell_offers'] as $item)--}}
								{{--<figure>--}}
									{{--<a href="{{SellOffer::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>--}}
									{{--<figcaption>--}}
										{{--<span class="flg-ico">{!!User::flag($item->user)!!}</span>--}}
										{{--<h4><a href="{{SellOffer::url($item)}}"> {{($item->product)? Helper::field($item->product, 'name'):''}}</a></h4>--}}
										{{--<p class="p-1">{{trans('front.'.$item->type)}}</p>--}}
										{{--<p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>--}}
									{{--</figcaption>--}}
								{{--</figure>--}}
							{{--@endforeach--}}
							{{--<a href="{{Deal::$types['offers']['url']}}" class="vw-mr-btn">@lang('front.more')</a>--}}
							{{--<a href="{{url('dashboard/sell_offers/create')}}" class="pst-btn">@lang('front.post_sell_offer')</a>--}}
						{{--</article>--}}

						{{--<article class="col-xs-12 col-sm-6 col-md-4">--}}
							{{--<header class="hdr-styl-04">--}}
								{{--<a href="{{Deal::$types['requests']['url']}}" class="tag">RFQ</a>--}}
								{{--<h4><a href="{{Deal::$types['requests']['url']}}">@lang('front.request_quotes')</a></h4>--}}
								{{--<p>@lang('front.request_for_quote_desc')</p>--}}
							{{--</header>--}}
							{{--@foreach($deals['quote_requests'] as $item)--}}
								{{--<figure>--}}
									{{--<a href="{{QuoteRequest::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>--}}
									{{--<figcaption>--}}
				                    {{--<span class="flg-ico">--}}
										{{--@if($item->im_country)--}}
											{{--{!!App\Country::flag_by_id($item->im_country,[],false,24)!!}--}}
										{{--@else--}}
											{{--{!!User::flag($item->user)!!}--}}
										{{--@endif--}}
				                    {{--</span>--}}
										{{--<h4><a href="{{QuoteRequest::url($item)}}">{{Helper::field($item,'name')}}</a></h4>--}}
										{{--<p class="p-1">{{trans('front.'.QuoteRequest::$request_for[$item->request_for])}}</p>--}}
										{{--<p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>--}}
									{{--</figcaption>--}}
								{{--</figure>--}}
							{{--@endforeach--}}
							{{--<a href="{{Deal::$types['requests']['url']}}" class="vw-mr-btn">@lang('front.more')</a>--}}
							{{--<a href="{{url('dashboard/request_quotes/create')}}" class="pst-btn">@lang('front.post_buying_req')</a>--}}
						{{--</article>--}}

						{{--<article class="col-xs-12 col-sm-6 col-md-4">--}}
							{{--<header class="hdr-styl-05">--}}
								{{--<a href="{{Deal::$types['partnerships']['url']}}" class="tag">PD</a>--}}
								{{--<h4><a href="{{Deal::$types['partnerships']['url']}}">@lang('front.partnership_deals')</a></h4>--}}
								{{--<p>@lang('front.partnership_deals_desc')</p>--}}
							{{--</header>--}}
							{{--@foreach($deals['partnerships'] as $item)--}}
                                {{--< ?php $partner_lang = Helper::lang_data($item, $lng_id); ?>--}}
								{{--<figure>--}}
									{{--<a href="{{Partnership::url($item)}}" class="img-lnk-hldr">{{Helper::image_single($item->images)}}</a>--}}
									{{--<figcaption>--}}
										{{--<span class="flg-ico">{!!User::flag($item->user)!!}</span>--}}
										{{--<h4><a href="{{Partnership::url($item)}}"> {{@$partner_lang->name}}</a></h4>--}}
										{{--<p class="p-1">{{trans('dash.partnership_types')[$item->type]}}</p>--}}
										{{--<p class="p-2"><span class="alrm-ico">{{Deal::expire_format($item)}}</span></p>--}}
									{{--</figcaption>--}}
								{{--</figure>--}}
							{{--@endforeach--}}
							{{--<a href="{{Deal::$types['partnerships']['url']}}" class="vw-mr-btn">@lang('front.more')</a>--}}
							{{--<a href="{{url('dashboard/partnerships/create')}}" class="pst-btn">@lang('front.post_partnership_deal')</a>--}}
						{{--</article>--}}
					{{--</div>--}}
				</section>

				<section class="latest_deals">
					{{--<header class="hdr-styl-02"><a href="{{url('blog')}}"><h4>@lang('front.latest_blog_posts')</h4></a></header>--}}
					{{--<div class="row">--}}
						{{--@foreach($blog as $article)--}}
							{{--<div class="col-md-4 blog_post">--}}
								{{--<a href="{{Blog::url($article)}}">--}}
									{{--<div class="blog_img">--}}
										{{--{{Helper::image2($article->image, [])}}--}}
									{{--</div>--}}
									{{--<div class="blog_strip">--}}
										{{--<h4>{{Helper::field($article,'title')}}</h4>--}}
										{{--<p>{!!str_limit(strip_tags(Helper::field($article, 'details')), 110)!!}</p>--}}
										{{--<span class="date">{{Date::parse($article->updated_at)->format('j F Y')}}</span>--}}
									{{--</div>--}}
								{{--</a>--}}
							{{--</div>--}}
						{{--@endforeach--}}
					{{--</div>--}}
				</section>
			</div>

			<!-- Aside Area  -->
			<aside class="col-xs-12 col-md-3 main-asid">
				@foreach($advs as $adv)
					<div class="banner_unit">
						<a href="{{$adv->link}}" target="_blank">{!!HTML::image('uploads/'.$adv->image,'')!!}</a>
					</div>
				@endforeach
				{{-- <div class="banner_unit"> 
					<a href="{{url('dashboard/products/create')}}">{!!HTML::image('frontend/images/ads/side1.png','',['class'=>'img-responsive'])!!}</a>
					<div class="text-center"><a href="{{url('dashboard/products/create')}}" class="grn-btn btn-sm form-control">سجل الآن</a></div>
				</div> --}}
				<div class="banner_unit">
					@if(App::getLocale()=='ar')
						<a href="https://chat.whatsapp.com/GUn1KUMZGhk9J7jWfBrdDx" target="_blank">{!!HTML::image('frontend/images/whatsapp_join.jpg','')!!}</a>
					@else
						<a href="https://chat.whatsapp.com/GUn1KUMZGhk9J7jWfBrdDx" target="_blank">{!!HTML::image('frontend/images/whatsapp_join_en.jpg','')!!}</a>
					@endif
				</div>
				<div class="banner_unit">
					<a href="{{url('site/trade-manager')}}" target="_blank">{!!HTML::image('frontend/images/ads/trade_manager.jpg','')!!}</a>
				</div>

				<div class="banner_unit">
                    <?php $up_banner = (App::getLocale()=='ar')?'upgrade_banner':'upgrade_banner_en'; ?>
					<a href="{{url('upgrade')}}" target="_blank">{!!HTML::image('frontend/images/ads/'.$up_banner.'.jpg')!!}</a>
				</div>
			</aside>
		</div>
	</div>
	<section class="bnr-728-ful-wdth">
	</section>
	<style type="text/css">
		/* in homepage only */
		@if(App::getLocale()=='ar')
		.navbar-brand {padding-right: 0}
		@endif
	</style>
@endsection

@section('script2')
<script>
	$(document).ready(function(){
		$(document).on('click',"#toggle-aside",function(){
			$("#sections-aside").toggle(500);
			$("#toggle-col").toggleClass('col-md-9');
		});
	});
</script>
@endsection