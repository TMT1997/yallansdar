<?php
    use App\Service;
    use App\Helper;
    use App\Product;
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">الخدمات</div>
        <div class="panel-body">
            <div class="well well-sm">
                {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                    <div class="form-group">
                        {!!Form::select('status', Product::$product_status, Request::get('status'),['class'=>'form-control'])!!}
                    </div>
                {!!Form::close()!!}
            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>الحساب</th>
                        <th>إظهار</th>
                        <th>الحالة</th>
                        <th>الاسم</th>
                        <th>الصورة</th>
                        <th>منذ</th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($services as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{!!App\User::user($item)!!}</td>
                        <td>{!!Helper::sw($item,'active','Service')!!}</td>
                        <td>{!!Helper::statusCtrl(Product::$product_status,$item,'status','Service')!!}</td>
                        <td>{{ $item->name }}</td>
                        <td>{!!Helper::image($item->image, ['width'=>'80'])!!}</td>
                        <td>{!! Helper::since($item) !!}</td>
                        <td>
                            <a href="{{url('admin/services/'.$item->id)}}" class="btn btn-success btn-xs popup" title="View"><span class="fa fa-eye" /></a>
                            <a href="{{ url('admin/services/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/services', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection