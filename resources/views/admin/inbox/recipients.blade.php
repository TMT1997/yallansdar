<div class="" style="width: 500px; margin: 0 auto; padding: 20px; background: #fff">
	<div class="row">

		<div class="col-md-12">
			<legend>المستلمون للرسالة: (العدد الكلي: {{count($recipients)}})</legend>
			<ul class="list-group">
				@foreach($recipients as $recp)
					<?php $user = App\User::findOrFail($recp['id']); ?>
					<li class="list-group-item">
						<strong>{{$user->name}}</strong> <br>
						@if($user->company_name)
						Company:{{$user->company_name}} <br>
						@endif
					 	<a href="mailto:{{$user->email}}">{{$user->email}}</a>
					 </li>
				@endforeach
			</ul>
		</div>
	</div>
</div>