<?php use App\Deals\Deal;
use App\Helper;
?>
@extends('admin.layouts.master')

@section('content')
	<div class="panel-heading">
		<legend>العروض والفرص والطلبات</legend>
    </div>
    <div class="panel-body">
    	<div class="well well-sm">
            {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                <div class="form-group">
                    {!!Form::select('status', Deal::$statues, Request::get('status'), ['class'=>'form-control'])!!}
                </div>
                <div class="input-group">
                        {!! Form::text('from_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.from')]) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="input-group">
                    {!! Form::text('end_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.to')]) !!}
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            {!!Form::close()!!}
        </div>
        <div role="tabpanel">
            <ul class="nav nav-tabs product_status" role="tablist">
                @foreach(Deal::$sections as $section=>$label)
                	@if($section == "quote_request")
                    <li role="presentation active"><a href="#f{{$section}}" data-sid="{{$section}}" aria-controls="{{$section}}" role="tab" data-toggle="tab" aria-expanded="true">{{$label}}</a></li>
                    @endif
                @endforeach
            </ul>

            <div class="tab-content">
            	@foreach(Deal::$sections as $section=>$label)
            		@if($section == "quote_request")
            		<div role="tabpanel" class="tab-pane active" id="f{{$section}}">
	            		<?php $deals_section = $deals->filter(function($row) use ($section) {
	            			return ($row->section==$section);
	            		}); ?>
	            		<div class="row">
				            <div class="col-md-4 col-md-offset-4 report_box_all_members count_table">
				            عدد النتائج: <p class="count_number">{{$deals_section->count()}}</p>
				            </div>
				        </div>
						<div class="label label-primary pull-left">No. of Deals {{$deals_section->count()}}</div>
						<table class="table table-bordered table-striped table-hover dataTable">
						    <thead>
						        <tr>
						            <th>{{trans('products.id')}}</th>
						            <th>User</th>
						            <th>Section</th>
						            <th>Type</th>
						            <th>Status</th>
						            <th>Valid Till</th>
						            <th>Last update</th>
						            <th>{{trans('products.actions')}}</th>
						        </tr>
						    </thead>
						    <tbody>
						    @foreach($deals_section as $item)
						        <tr id="tr_{{$item->id}}">
						            <td>{{ $item->id }}</td>
						            <td>{!!App\User::user($item)!!}</td>
						            <td>{{ Deal::$sections[$item->section]}}</td>
						            <td>{{ $item->type }}</td>
						            <td>{!!Helper::statusCtrl(Deal::$statues,$item,'status','Deals\Deal')!!}</td>
						            <td>{{ Carbon\Carbon::parse($item->expired_at)->format('M. d, Y') }}</td>
						            <td>{!! Helper::since($item) !!}</td>
						            <td>
						            	<a href="{{url('admin/deals/'.$item->id)}}" class="btn btn-success btn-sm popup" title="View"><span class="fa fa-eye" aria-hidden="true"/></a>

						                <a href="{{route('dashboard.deals.edit',$item->id)}}" class="btn btn-primary btn-sm" title="{{trans('deals.edit_prod')}}"><span class="glyphicon glyphicon-pencil"/></a>

					                    {!! Form::open([
					                        'method'=>'DELETE','url'=>['admin/deals',$item->id],'style'=>'display:inline'
					                    ]) !!}
				                        {!! Form::button('<i class="fa fa-trash" title="'.trans("products.del_prod").'"></i>', array('type' => 'submit',
				                        	'class'=>'btn btn-danger btn-sm',
				                        	'title'=>'Delete Deal Permenantly',
				                        	'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
				                        )) !!}
					                    {!! Form::close() !!}
						            </td>
						        </tr>
						    @endforeach
						    </tbody>
						</table>
					</div>
					@endif
				@endforeach
            </div>
        </div>
    </div>
@endsection

@section('script')
	<script type="text/javascript">
    	$(function(){
		  var hash = window.location.hash;
		  hash && $('ul.nav a[href="' + hash + '"]').tab('show');
		});
    </script>
@endsection