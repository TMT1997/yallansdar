<?php
    use App\Deals\SellOffer; 
    use App\Helper;
?>

<div class="white-popup-block" style="min-width: 900px;">
{!! Form::model($deal, ['method' => 'PATCH', 'class' => 'form-horizontal', 'id'=>'service_form']) !!}
<div class="well well-sm">{{trans('dash.offer_information')}}</div>
    <div class="form-group">
        {!! Form::label('type', trans('dash.type_of_offer'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!!Form::select('type', trans('dash.offer_types'), null, ['class'=>'form-control offer_type'])!!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('offer_for', trans('dash.offer_for'), ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-8">
            @foreach(trans('dash.selloffer_for') as $val=>$item)
                <label>{!!Form::radio('offer_for', $val, null, ['class'=>'offer_for'])!!} {{$item}}</label>
            @endforeach
        </div>
    </div>
    <div class="form-group selectproduct">
        {!!Form::label('product_id', trans('dash.Service_Product_Name'), ['class'=>'col-md-3 control-label'])!!}
        <div class="col-md-8 form-inline">
            {{@$deal->product->name}}
        </div>
    </div>
    <div class="form-group service_product_name {{ $errors->has('service_product_name') ? 'has-error' : ''}}">
        {!! Form::label('service_product_name', trans('dash.service_name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('service_product_name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group available_units {{ $errors->has('available_units') ? 'has-error' : ''}}">
        {!! Form::label('available_units', trans('dash.stock_available_unit'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('available_units', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('description', trans('dash.offer_details'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        {!! Form::textarea('lang['.$lang->id.'][description]', null, ['class'=>'form-control','rows'=>5, 'placeholder'=>Lang::get('deals.description',[],$lang->iso2)]) !!}
                        </div></div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('add_images') ? 'has-error' : ''}}">
        {!! Form::label('add_images', trans('dash.related_images'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 additional_imgs">
            @if(isset($sell_offer))
                <div class="row">
                    @foreach($sell_offer->images as $item)
                    <div class="col-xs-6 col-sm-4 col-md-3 infile">
                        <div class="thumbnail">
                            {!!App\Helper::image($item->image,['style'=>'max-height:60px;'])!!}
                            <div class="caption">
                                <p>{{$item->name}}</p>
                                {!!Form::button('<i class="fa fa-trash" title="Delete"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\Deals\SellOfferImage").'")'])!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endif
            <div class="media">
                <a class="pull-left" href="#">{!!HTML::Image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfields'])!!}</a>
                <div class="media-body">{!!Form::file('add_images[]', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}</div>
            </div>
            <div class="input_imgfiles_wrap"></div>
        </div>
    </div>

    <div class="form-group fob_price {{ $errors->has('fob_price') ? 'has-error' : ''}}">
        {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    {!! Form::select('currency', trans('dash.fob_price_list'), null,['class'=>'form-control','required'=>'required']) !!}
                </div>
                <div class="col-md-2">                    
                    {!! Form::text('min_price', null, ['class'=>'form-control', 'placeholder'=>trans('dash.min_price'),'required'=>'required']) !!}
                </div>
                <div class="col-md-2">                    
                    {!! Form::text('max_price', null, ['class'=>'form-control', 'placeholder'=>trans('dash.max_price'),'required'=>'required']) !!}
                </div>
                <div class="col-md-4 form-inline">
                    <label>{{trans('products.per')}} {!! Form::select('unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required']) !!}
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('discount') ? 'has-error' : ''}}">
        {!! Form::label('discount', trans('dash.discount'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::text('discount', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group min_quantity {{ $errors->has('min_quantity') ? 'has-error' : ''}}">
        {!! Form::label('min_quantity', trans('dash.min_order_quantity'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!!Form::text('min_quantity', null, ['class'=>'form-control','required'=>'required']) !!}
            {!!Form::select('min_quantity_unit',trans('dash.units'),null,['class'=>'form-control','required'=>'required'])!!}
        </div>
    </div>

    <div class="form-group brands">
        {!! Form::label('brands', trans('dash.brands'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('brands', null, ['class'=>'form-control tags','placeholder'=>trans('dash.brands')]) !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.offer_valid_till'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class'=>'form-control expired_datepicker','required'=>'required']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>

<div class="well well-sm">{{trans('dash.trade_information')}}</div>
    {!!Form::hidden('market_country', '')!!}
    <div class="form-group">
        {!! Form::label('market_country', trans('dash.market_focus'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            @if(isset($deal->market_country) && count($deal->market_country)>0)
                @foreach($deal->market_country as $id)
                    <a href="#" class="btn btn-sm btn-default">{{@App\Country::get_country($id)->name}}</a>
                @endforeach
            @endif
        </div>
    </div>
    <div class="form-group payment_types">
        {!!Form::hidden('payment_type', '')!!}
        {!! Form::label('payment_type', trans('products.payment_type'), ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div class="row">
                @foreach(trans('dash.payment_methods_list') as $item=>$label)
                <div class="col-md-3">
                    <label title="{{$item}}">{!!Form::checkbox('payment_type[]', $item, null, ['class'=>'pay_types'])!!} {{$label}}</label>
                    @if($label=='Other')
                        <span id="other_payment_type">
                            {!!Form::label('other_payment', 'Enter your payment type',['class'=>'control-label'])!!}
                            {!!Form::text('other_payment_type', null,['class'=>'form-control other_paytype'])!!}
                        </span>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group port">
        {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            {!! Form::text('port', null, ['class' => 'form-control','required'=>'required']) !!}
        </div>
    </div>
    <div class="form-group shipping_terms">
        {!! Form::label('shipping_terms', trans('deals.shipping_terms'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec3{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec3{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                            {!! Form::textarea('lang['.$lang->id.'][shipping_terms]', null, ['class'=>'form-control','rows'=>3, 'placeholder'=>Lang::get('deals.shipping_terms_help',[],$lang->iso2)]) !!}
                        </div></div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
@endsection