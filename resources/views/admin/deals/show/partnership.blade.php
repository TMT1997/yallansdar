<?php
    use App\Deals\Partnership;
    use App\Helper;
?>
<div class="white-popup-block" style="min-width: 900px;">
{!! Form::model($deal, ['method' => 'PATCH', 'class' => 'form-horizontal', 'id'=>'service_form']) !!}

    <div class="well well-sm">{{trans('dash.Partnership_Information')}}</div>
    <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
        {!! Form::label('type', trans('dash.type_of_deal'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-5">
            {!! Form::select('type', trans('dash.partnership_types'), null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('offer_as') ? 'has-error' : ''}}">
        {!! Form::label('offer_as', trans('dash.part_offer_as'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-5">
            {!!Form::select('offer_as',trans('dash.partnership_offer_as'),null,['class'=>'form-control','required'=>'required'])!!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('request_for') ? 'has-error' : ''}}">
        {!! Form::label('request_for', trans('dash.part_request'), ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            @foreach(Partnership::$request_for as $val=>$item)
                <label>{!!Form::radio('request_for', $val, null, ['class'=>'request_for','required'=>'required'])!!} {{trans('dash.'.$item)}}</label>
            @endforeach
        </div>
    </div>

    <div class="form-group service_product_name {{ $errors->has('service_product_name') ? 'has-error' : ''}}">
        {!! Form::label('service_product_name', trans('dash.Service_Product_Name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div class="col-md-9">
                <div role="tabpanel">
                    <?php $count=0; $count2=0; ?>
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach($languages as $lang)
                            <?php $active = ($count==0) ? 'class="active"':''; ?>
                            <li {!!$active!!}><a href="#nsec{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                            <?php $count++ ?>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($languages as $lang)
                            <?php $active = ($count2==0) ? 'active':''; ?>
                            <div class="tab-pane fade in {!!$active!!}" id="nsec{{$lang->id}}">
                                <div class="panel panel-default" style="border-top: none;">
                                    <div class="panel-body">
                                    {!! Form::text('lang['.$lang->id.'][name]', null, ['class'=>'form-control','placeholder'=>Lang::get('dash.Service_Product_Name',[],$lang->iso2)]) !!}
                                    </div>
                                </div>
                            </div>
                            <?php $count2++ ?>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('products.prod_cat'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {{@$deal->category->name}}
        </div>
    </div>

    {!!Form::hidden('countries', '')!!}
    <div class="form-group {{ $errors->has('countries') ? 'has-error' : ''}}">
        {!! Form::label('countries', trans('dash.request_location'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('countries[]',$countries,null,['class'=>'form-control select2c','multiple'=>'multiple']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('details', trans('dash.details'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;">
                            <div class="panel-body">
                            {!! Form::textarea('lang['.$lang->id.'][details]', null, ['class'=>'form-control','id'=>'editor'.$lang->id, 'placeholder'=>Lang::get('deals.details',[],$lang->iso2)]) !!}
                            </div>
                        </div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('add_images') ? 'has-error' : ''}}">
        {!! Form::label('add_images', trans('dash.desc_image'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 additional_imgs">
            @if(isset($partnership))
                <div class="row">
                    @foreach($partnership->images as $item)
                    <div class="col-xs-6 col-sm-4 col-md-3 infile">
                        <div class="thumbnail">
                            {!!App\Helper::image($item->image, ['style'=>'max-height:60px;'])!!}
                            <div class="caption">
                                <p>{{$item->name}}</p>
                                {!!Form::button('<i class="fa fa-trash" title="Delete"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\Deals\PartnershipImage").'")'])!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endif
            <div class="media">
                <a class="pull-left" href="#">{!!HTML::Image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfields'])!!}</a>
                <a class="pull-right btn btn-success add_imgfile_button">+</a>
                <div class="media-body">{!!Form::file('add_images[]', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}</div>
            </div>
            <div class="input_imgfiles_wrap"></div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('expertise', trans('dash.Field_of_Expertise'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec2{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec2{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        {!! Form::text('lang['.$lang->id.'][expertise]', null, ['class'=>'form-control']) !!}
                        </div></div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.part_expiry'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class' => 'form-control expired_datepicker','required'=>'required']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>

<div class="well well-sm">{{trans('dash.contact_details')}}</div>
    <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
        {!! Form::label('contact_name', trans('dash.contact_name'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::text('contact_name', null, ['class' => 'form-control','placeholder'=>trans('dash.contact_name'),'required'=>'required']) !!}
        </div>
    </div>
    <div class="form-group email {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email', trans('dash.email'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            {!! Form::email('email',null,['class'=>'form-control','placeholder'=>trans('dash.email'),'required'=>'required'])!!}
        </div>
    </div>
    <div class="form-group mobile {{ $errors->has('mobile') ? 'has-error' : ''}}">
        {!! Form::label('mobile', trans('dash.mobile'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::text('code', null, ['class' => 'form-control short','placeholder'=>trans('dash.code')]) !!}
            {!! Form::text('mobile', null, ['class' => 'form-control','placeholder'=>trans('dash.mobile'),'required'=>'required']) !!}
        </div>
    </div>
    <div class="form-group telephone {{ $errors->has('telephone') ? 'has-error' : ''}}">
        {!! Form::label('telephone', trans('dash.telephone'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::text('code_tel', null, ['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!}
            {!! Form::text('telephone', null, ['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!}
        </div>
    </div>
</div>
@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
@endsection