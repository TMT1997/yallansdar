
<?php
use App\User;
use App\Helper;

?>
<div class="white-popup-block" style="min-width: 900px;">
    <style>
        .form-group.required .control-label:after {
            content:"*";
            color:red;
        }
    </style>
        
        {!!Form::open(['url'=>'deals/rfq-quote','method'=>'POST','class'=>'form-horizontal','files'=>true])!!}
        <div class="form-group">
            {!!Form::label('subject', trans('dash.subject'), ['class'=>'col-md-3 control-label'])!!}
            <div class="col-md-9 ">
                {!!Form::text('subject', trans('front.we_provide_quote').': '.$quote->request->name_ar,['class'=>'form-control','placeholder'=>'Your Subject Here','readonly'=>true])!!}
            </div>
        </div>

        <div class="form-group required">
            {!! Form::label('production_capacity', trans('products.capacity'), ['class' => 'col-md-3 control-label required','readonly'=>true]) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::number('production_capacity',$quote->production_capacity, ['class'=>'form-control','required'=>'required','readonly'=>true]) !!}
                    </div>
                    <div class="col-md-3 pay_type_pos">
                        {!!Form::select('capacity_unit',trans('dash.units'),$quote->capacity_unit,['class'=>'form-control','required'=>'required','readonly'=>true])!!}
                    </div>
                    <div class="col-md-4 form-inline">
                        <label class="control-label">{{trans('products.per')}} {!! Form::select('capacity_unit_per',trans('dash.capacity_durations'),$quote->capacity_unit_per,['class'=>'form-control','required'=>'required','readonly'=>true])!!}
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group required">
            {!! Form::label('min_quantity', trans('front.quantity_available'), ['class' => 'col-md-3 control-label required','readonly'=>true]) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::number('min_quantity', $quote->min_quantity, ['class' => 'form-control','required'=>'required','readonly'=>true]) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::select('min_quantity_unit',trans('dash.units'),$quote->min_quantity_unit,['class'=>'form-control','required'=>'required','readonly'=>true]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group required">
            {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::select('fob_price_type', trans('dash.fob_price_list'), $quote->fob_price_type,['class'=>'form-control','required'=>'required','readonly'=>true]) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::number('fob_price',$quote->fob_price,['class'=>'form-control','placeholder'=>trans('dash.price'),'required'=>'required','readonly'=>true]) !!}
                    </div>
                    <div class="col-md-4 form-inline ">
                        <label>{{trans('products.per')}} {!!Form::select('fob_price_unit',trans('dash.units'), $quote->fob_price_unit,['class' => 'form-control','readonly'=>true])!!}
                        </label>
                    </div>
                </div>
                <div class="help-block">{{trans('products.type_help')}}</div>
            </div>
        </div>
        <div class="form-group required ">
            {!! Form::label('fob_price', trans('products.payment_type'), ['class' => 'col-md-3 control-label pay_type']) !!}
            <div class="col-md-9">
                <div class="row">
                    @foreach(App\ProductDetails::$payment_methods as $item=>$label)
                        <div class="col-md-3">
                            <label title="{{$item}}">{!!Form::checkbox('payment_type[]', $item, null, ['checked'=>(in_array($item,$quote->payment_type)?true:false),'class'=>'pay_types','disabled'=>true])!!} {{trans('dash.'.$label)}}</label>
                            @if($label=='Other')
                                <span id="other_payment_type">
	                            {!!Form::label('other_payment',trans('dash.enter_your_payment_type'),['class'=>''])!!}
                                    {!!Form::text('other_payment_type',$quote->other_payment_type,['class'=>'form-control other_paytype','readonly'=>true])!!}
	                        </span>
                            @endif
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="form-group required ">
            {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-8">
                {!! Form::select('port',trans('dash.shipping_ports'),$quote->port,['class'=>'form-control','required'=>'required','readonly'=>true]) !!}

            </div>
        </div>
        <div class="form-group required ">
            {!! Form::label('delivery_time', trans('products.delivery_time'), ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3">{!! Form::number('delivery_time', $quote->delivery_time, ['class' => 'form-control','readonly'=>true]) !!}</div>
                    <div class="col-md-3">
                        {!!Form::select('delivery_duration',trans('dash.delivery_duration'),$quote->delivery_duration,['class'=>'form-control','readonly'=>true]) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group brands required">
            {!!Form::label('packaging',trans('products.pack_details'), ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!!Form::textarea('packaging',$quote->packaging,['class'=>'form-control','readonly'=>true,'rows'=>4,'placeholder'=>trans('products.pack_help')])!!}

            </div>
        </div>
        <div class="form-group required ">
            {!!Form::label('body', trans('front.message'), ['class'=>'col-md-3 control-label'])!!}
            <div class="col-md-9">
                {!!Form::textarea('body', $quote->body,['class'=>'form-control','placeholder'=>trans('front.enter_quote_message'), 'required'=>'required','readonly'=>true])!!}

            </div>
        </div>

        {!!Form::close()!!}

</div>
@section('script')
    {!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
    {!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
    {!!HTML::script('frontend/js/app/products.js')!!}
    {!!HTML::script('frontend/js/app/deals.js')!!}
@endsection