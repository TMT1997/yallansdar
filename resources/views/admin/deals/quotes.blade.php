<?php
use App\Service;
use App\Helper;
use App\Product;
use App\Deals\Quote;
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">طلب عروض الاسعار</div>
        <div class="panel-body">
            <div id="succ" style="text-align: center">

            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Exporter</th>
                    <th>Importer</th>
                    <th>Subject</th>
                    <th>Body</th>
                    <th>Status</th>
                    <th>Attachment</th>
                    <th>Send in</th>
                    <th>{{trans('products.actions')}}</th>

                </thead>
                <tbody>
                @foreach($quotes as $quote)
                    <tr>
                        <td>{{ $quote->id }}</td>
                        <td>{!!App\User::user($quote->user_from,$quote->user_from->name,'id')!!}</td>
                        <td>{!!App\User::user($quote->user,$quote->user->name,'id')!!}</td>
                        <td width="200px">{{$quote->subject}}</td>
                        <td width="200px">{{$quote->body}}</td>
                        <td>{!!Helper::statusCtrl(Quote::$status,$quote,'status','Deals\Quote')!!}</td>
                        <td>{!!Helper::image($quote->attachment, ['width'=>'80'])!!}</td>
                        <td>{{$quote->created_at}}</td>
                        <td style="display: inline">
                            <a href="{{url('admin/dealsDesc/'.$quote->id)}}" class="btn btn-success btn-sm popup" title="View"><span class="fa fa-eye" aria-hidden="true"/></a>

                            {!! Form::open([
                                'method'=>'post','url'=>['admin/Destroyquote'],'style'=>'display:inline'
                            ]) !!}
                            {!!Form::hidden('qid', $quote->id)!!}
                            {!! Form::button('<i class="fa fa-trash" title="'.trans("products.del_prod").'"></i>', array('type' => 'submit',
                                'class'=>'btn btn-danger btn-sm',
                                'title'=>'Delete Deal Permenantly',
                                'onclick'=>'return confirm("'.trans("products.confirm_delete").'")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>


                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function(){
            //  check if the status changed
            $('.selector').on('change',function (e) {
                var status = ($(this).find("option:selected").val());
                var quote_id = $(this).data('id'); // get the deal id
                var test = $('#succ');
                $.ajax({
                    url:'{{url('admin/sendquotes')}}',
                    type: 'POST',
                    data : {id : quote_id , status : status , _token:'{{csrf_token()}}' },
                        beforeSend: function () {
                            test.html('Loading.....')
                        }
                }).done(function (response) {
                    test.addClass('alert alert-success');
                    test.html(response);
                })
            })

        });
    </script>
@endsection