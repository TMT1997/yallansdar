<?php
    use App\Review;
    use App\User;
    use App\Helper;
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">رسائل التواصل بين العملاء بخصوص المنتجات</div>
        <div class="panel-body">
            <br/>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>المنتج</th>
                        <th>من</th>
                        <th>إلى</th>
                        <th>الرسالة</th>
                        <th>تم قرأتها</th>
                        <th>الحالة </th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>


                @foreach($messages as $item)
                     @if($item->from_user)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-4" style="max-height:130px; overflow: hidden;">{!!Helper::image($item->product->image,['class'=>'img-responsive'])!!}</div>
                                <div class="col-md-8">{{$item->product->name_ar}}</div>
                            </div>
                        </td>
                        <td>{!!User::user($item, $item->from_user->name, 'from_user_id')!!}</td>
                        <td>{!!User::user($item, $item->to_user->name, 'to_user_id')!!}</td>
                        <td>{!! $item->message !!}</td>
                        <td>{!!Helper::sw($item,'read','ProductEnquiry')!!}</td>
                        <td>{!!Helper::statusCtrl_product_enquiry(\App\Messages\Message::$status,$item,'status','Messages\Message')!!}</td>
                        <td>
                            {{-- <a href="{{ url('/admin/reviews/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> --}}
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/messages', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o"></i>', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection