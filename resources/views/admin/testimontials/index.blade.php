@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>أراء الزوار</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/testimontials')}}">أراء الزوار</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{ url('/admin/testimontials/create?tag='.Request::get('tag')) }}" class="btn btn-primary btn-sm" title="Add New Testimontial"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة جديد</a></h3>
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>مفعل</th>
                            <th>مضاف للقائمة</th>
                            <th>الصورة </th>
                            <th>اسم الحساب </th>
                            <th>الرأي </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($testimontials as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!App\Helper::sw($item,'active','Testimontial')!!}</td>
                            <td>{!!App\Helper::sw($item,'homepage','Testimontial')!!}</td>
                            <td>{!!App\Helper::image($item->image, ['width'=>50])!!}</td>
                            <td><a href="{{route('admin.testimontials.edit', $item->id)}}">{{ $item->company_name }}</a></td>
                            <td>{{ str_limit(strip_tags($item->testimontial),150) }}</td>
                            <td nowrap="nowrap">
                                <a href="{{ url('/admin/testimontials/' . $item->id) }}" class="btn btn-success btn-sm" title="View Testimontial"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('/admin/testimontials/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="Edit Testimontial"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/testimontials', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Testimontial" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete Testimontial',
                                        'onclick'=>'return confirm("حذف الصفحة: هل أنت متأكد؟")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>        
      </div>
    </section>
@endsection