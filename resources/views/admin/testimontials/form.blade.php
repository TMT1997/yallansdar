<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'اسم الشخص', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
    {!! Form::label('company_name', 'اسم الشركة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('company_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('job_title') ? 'has-error' : ''}}">
    {!! Form::label('job_title', 'الوظيفة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('job_title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('job_title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'الصورة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!!App\Helper::image(@$testimontial->image)!!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('testimontial') ? 'has-error' : ''}}">
    {!! Form::label('testimontial', 'التفاصيل', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('testimontial', null, ['class' => 'form-control','id'=>'editor']) !!}
        {!! $errors->first('testimontial', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'تفعيل الظهور', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
    <div class="checkbox">
        <label>{!! Form::radio('active', '1', null) !!} نعم</label>
        <label>{!! Form::radio('active', '0', null) !!} ﻻ</label>
    </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit('إضافة', ['class' => 'btn btn-primary']) !!}
        <a href="{!!url('admin/testimontials')!!}" class="btn btn-default">إلغاء</a>
    </div>
</div>