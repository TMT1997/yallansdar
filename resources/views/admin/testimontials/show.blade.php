@extends('admin.layouts.master')

@section('content')

    <section class="content-header">
      <h1>أراء الزوار</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/testimontials')}}">أراء الزوار</a></li>
        <li><a href="{{url('admin/testimontials/show')}}">Show</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ $page->id }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <a href="{{ url('admin/testimontials/' . $page->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Page"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/testimontials', $page->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Page',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $page->id }}</td>
                        </tr>
                        <tr><th> Title </th><td> {{ $page->title }} </td></tr><tr><th> Slug </th><td> {{ $page->slug }} </td></tr><tr><th> Image </th><td> {{ $page->image }} </td></tr><tr><th> Details </th><td> {{ $page->details }} </td></tr><tr><th> Active </th><td> {{ $page->active }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            <a href="{!!url('admin/testimontials')!!}" class="btn btn-default">Go Back</a>
        </div>
    </div>
@endsection