@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Create New Admin User</div>
                <div class="panel-body">

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => 'admin/admins', 'class' => 'form-horizontal']) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'الاسم', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('name', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                            {!! Form::label('email', 'البريد الإلكتروني', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('email', null, ['class' => 'form-control','style'=>'direction:ltr','autocomplete'=>'off']) !!}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                            {!! Form::label('password', 'كلمة السر', ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password', ['class' => 'form-control','autocomplete'=>'off']) !!}
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('super_admin', 'Admin Level', ['class'=>'col-md-3 control-label required']) !!}
                            <div class="col-md-9">
                                <label>{{Form::radio('super_admin', 0, null, ['class'=>'rd_type'])}} Admin </label>
                                <label>{{Form::radio('super_admin', 1, null, ['class'=>'rd_type'])}} Super Admin </label>
                            </div>
                        </div>

                        <div class="type_admin">
                            <div class="form-group">
                                {{ Form::label('password', 'الصفحات المسموح بها', ['class'=>'col-md-3 control-label']) }}
                                <div class="col-md-6">
                                    <select class="form-control" name="access_pages[]" multiple="" size="8">
                                        @foreach(App\Menu::$admin_access_menu as $item=>$sept)
                                            <optgroup label="{{trans($sept['label'])}}">
                                            @foreach($sept['menu'] as $url=>$page)
                                                <?php $sel = ''; ?>
                                                @if(isset($user->access_pages) && !empty($user->access_pages))
                                                    <?php
                                                        $acc_page = $user->access_pages->where('page', $url);
                                                        $sel = ($acc_page && !$acc_page->isEmpty()) ? 'selected="selected"':'';
                                                    ?>
                                                @endif

                                                <option {{$sel}} value="{{$url}}">--- {{trans($page['label'])}}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    {!! $errors->first('parent', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="type_superadmin"></div>

                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6">
                                {!! Form::submit('اضافة', ['class' => 'btn btn-primary']) !!}
                                <a href="{!!url('admin/users')!!}" class="btn btn-default">إلغاء</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.rd_type', function(event) {
            //event.preventDefault();
            $val = $(this).val();
            if($val==1) {
              $('.type_admin').slideUp('fast');
              $('.type_superadmin').slideDown('fast');
            } else {
              $('.type_superadmin').slideUp('fast');
              $('.type_admin').slideDown('fast');
            }
        });
        $(document).ready(function() {
            $('.type_admin').slideUp('fast');
            $('.type_superadmin').slideUp('fast');
            $('.rd_type:checked').trigger('click');
        });
    </script>
@endsection
