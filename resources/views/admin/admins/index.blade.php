@extends('admin.layouts.master')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Admin Users</div>
    <div class="panel-body">
        <a href="{{ url('admin/admins/create') }}" class="btn btn-primary pull-right btn-sm">Add New Admin</a>
        <br/>
        <br/>

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email Verified</th>
                        <th>Is Admin</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><a href="{{ url('admin/admins', $item->id) }}">{{ $item->name }}</a></td>
                        <td>{{ $item->email }}</td>
                        <td>{!!App\Helper::sw($item,'active', 'User')!!}</td>
                        <td>{!!App\Helper::sw($item,'admin', 'User')!!}</td>
                        <td>
                            <a href="{{ url('admin/profiles/login/'.$item->id) }}" class="btn btn-warning btn-sm">Login</a>
                            <a href="{{ url('admin/admins/'.$item->id.'/edit') }}" class="btn btn-primary btn-sm">Edit</a>
                            {!! Form::open([
                                'method'=>'DELETE', 'url' => ['/admin/admins', $item->id], 'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" title="Delete"></i> Delete', array(
                                    'type' => 'submit', 'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete', 'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination"> {!! $users->render() !!} </div>
        </div>
    </div>
</div>
@endsection
