<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {{ Form::label('password', 'كلمة المرور', ['class'=>'col-md-4 control-label']) }}
    <div class="col-md-8">
        <a href="#" class="btn btn-warning btn_changepass" data-id="{{$user->id}}" data-toggle="modal" data-target="#ChangePassModal">تغيير كلمة المرور</a>
    </div>
</div>

<div class="form-group">
    {!! Form::label('super_admin', 'Admin Level', ['class'=>'col-md-4 control-label required']) !!}
    <div class="col-md-8">
        <label>{{Form::radio('super_admin', 0, null, ['class'=>'rd_type'])}} Admin </label>
        <label>{{Form::radio('super_admin', 1, null, ['class'=>'rd_type'])}} Super Admin </label>
    </div>
</div>
<div class="type_admin">
    <div class="form-group">
        {{ Form::label('password', 'الصفحات المسموح بها', ['class'=>'col-md-4 control-label']) }}
        <div class="col-md-8">
            <select class="form-control" name="access_pages[]" multiple="" size="8">
                @foreach(App\Menu::$admin_access_menu as $item=>$sept)
                    <optgroup label="{{trans($sept['label'])}}">
                    @foreach($sept['menu'] as $url=>$page)
                        <?php $sel = ''; ?>
                        @if(isset($user->access_pages) && !empty($user->access_pages))
                            <?php
                                $acc_page = $user->access_pages->where('page', $url);
                                $sel = ($acc_page && !$acc_page->isEmpty()) ? 'selected="selected"':'';
                            ?>
                        @endif

                        <option {{$sel}} value="{{$url}}">--- {{trans($page['label'])}}</option>
                    @endforeach
                @endforeach
            </select>
            {!! $errors->first('parent', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="type_superadmin"></div>
<hr><br>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        <a href="{{route('admin.admins.index')}}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.rd_type', function(event) {
            //event.preventDefault();
            $val = $(this).val();
            if($val==1) {
              $('.type_admin').slideUp('fast');
              $('.type_superadmin').slideDown('fast');
            } else {
              $('.type_superadmin').slideUp('fast');
              $('.type_admin').slideDown('fast');
            }
        });
        $(document).ready(function() {
          $('.rd_type:checked').trigger('click');
        });
    </script>
@endsection