@extends('admin.layouts.master')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Create New PackageItem</div>
            <div class="panel-body">

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['url' => '/admin/package-items', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('admin.package-items.form')

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection