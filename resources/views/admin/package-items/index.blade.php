<?php
    use App\Helper;
    use App\Packages\Package;
    $sections = array_prepend(Package::$sections, 'All', '');
?>
@extends('admin.layouts.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Package items
        <a href="{{ url('/admin/package-items/create') }}" class="btn btn-primary btn-sm pull-left" title="Add New PackageItem"><i class="fa fa-plus"></i> Add New PackageItem</a>
    </div>
    <div class="panel-body">
        <div class="box-header with-border">
            {!!Form::open(['method'=>'GET','class'=>'form-inline form-filter'])!!}
                <div class="form-group">
                    Filter by section: {!!Form::select('section', $sections, null, ['class'=>'form-control'])!!}
                </div>
            {!!Form::close()!!}
        </div>

        <br/>
        <table class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Section </th>
                    <th>Name </th>
                    <th>Description</th>
                    <th>Code</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($packageitems as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->section }}</td>
                    <td><a href="{{route('admin.package-items.edit',$item->id)}}">{{Helper::field($item,'name')}}</a></td>
                    <td>{{ Helper::field($item, 'description') }}</td>
                    <td>{{ $item->code }}</td>
                    <td nowrap="">
                        <a href="{{ url('admin/package-items/' . $item->id) }}" class="btn btn-success btn-sm" title="View"><span class="fa fa-eye"/></a>
                        <a href="{{ url('admin/package-items/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="Edit"><span class="fa fa-pencil"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/package-items', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="fa fa-trash-o" title="Delete" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection