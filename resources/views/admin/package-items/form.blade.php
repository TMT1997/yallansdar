<?php
    use App\Packages\Package;
    use App\Packages\PackageItem; 
?>
<div class="form-group {{ $errors->has('section') ? 'has-error' : ''}}">
    {!! Form::label('section', 'Section', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select('section', Package::$sections, null, ['class' => 'form-control']) !!}
        {!! $errors->first('section', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
    {!! Form::label('name_ar', 'Name', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-6">
                <label>Arabic </label>
                {!! Form::text('name_ar', null, ['class' => 'form-control', 'placeholder'=>'Name عربي']) !!}
            </div>
            <div class="col-md-6">
                <label>English </label>
                {!! Form::text('name_en', null, ['class' => 'form-control', 'placeholder'=>'Name English']) !!}
            </div>
        </div>
        {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description_ar') ? 'has-error' : ''}}">
    {!! Form::label('description_ar', 'Description', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-6">
                <label>Arabic </label>
                {!! Form::textarea('description_ar', null, ['class' => 'form-control', 'rows'=>3, 'placeholder'=>'الوصف عربي']) !!}
            </div>
            <div class="col-md-6">
                <label>English </label>
                {!! Form::textarea('description_en', null, ['class' => 'form-control', 'rows'=>3, 'placeholder'=>'Description English']) !!}
            </div>
        </div>
        {!! $errors->first('description_ar', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('code', 'Developer Code', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-4"> 
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
        <div class="help-block">Please don't change this value</div>
    </div>
</div>
<div class="row">
    @foreach($packages as $package)
        <div class="col-md-3">
            <legend>{{$package->name_ar}}</legend>
            <div class="form-inline">
                {!! Form::label('value', 'Value', ['class' => 'control-label']) !!}<br>
                {!! Form::select('package[values]['.$package->id.'][value]', PackageItem::$values, null, ['class'=>'form-control sm']) !!} OR 
                {!! Form::text('package[values]['.$package->id.'][custom_value]', null, ['class'=>'form-control','size'=>'8','placeholder'=>'Custom Value']) !!} 
            </div>
            <div class="">
                {!! Form::label('value_hint', 'Value Hint', ['class' => 'control-label']) !!}
                {!! Form::text('package[values]['.$package->id.'][value_hint]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endforeach
</div>

<div class="row">
    <div class="col-md-6">
        <br>
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<style type="text/css">
    select.sm {font-size:12px;padding-left:2px;padding-right:2px}
</style>