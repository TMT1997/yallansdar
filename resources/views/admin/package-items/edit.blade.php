@extends('admin.layouts.master')

@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">Edit Package Item {{ $packageitem->title }}</div>
        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($packageitem, [
                'method' => 'PATCH',
                'url' => ['/admin/package-items', $packageitem->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include ('admin.package-items.form', ['submitButtonText' => 'Update'])
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection