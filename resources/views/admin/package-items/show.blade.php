@extends('admin.layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">PackageItem {{ $packageitem->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('admin/package-items/' . $packageitem->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit PackageItem"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/packageitems', $packageitem->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete PackageItem',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $packageitem->id }}</td>
                                    </tr>
                                    <tr><th> Package Id </th><td> {{ $packageitem->package_id }} </td></tr><tr><th> Section </th><td> {{ $packageitem->section }} </td></tr><tr><th> Name </th><td> {{ $packageitem->name }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection