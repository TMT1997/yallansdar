@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
        <h1>اعلانات عامة </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ url('/admin/adv_general/create') }}" class="btn btn-primary btn-sm" title="أضافة سليدر جديد"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة عنصر جديد</a></h3>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="table">
                    <table class="table table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>رقم</th>
                            <th> الصورة </th>
                            <th> لينك</th>
                            <th> النوع</th>
                            <th> مفعل </th>
                            <th>مهام</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($advs as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{!!App\Helper::image($item->image)!!}</td>
                                <td><a href="{{$item->link}}" target="_blank">{{ $item->link}}</a></td>
                                <td>{{ $item->type}}</td>
                                <td>{!!App\Helper::sw($item,'active','Advertisement')!!}</td>
                                <td>
                                    <a href="{{ url('admin/adv_general/' . $item->id) }}" class="btn btn-success btn-sm" title="View Slider"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('admin/adv_general/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="تعديل الاسليدر"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/adv_general', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Advertisement" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete Advertisement',
                                            'onclick'=>'return confirm("حذف الأعلان: هل أنت متأكد؟")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection