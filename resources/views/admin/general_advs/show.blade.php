@extends('admin.layouts.master')

@section('content')

    <section class="content-header">
        <h1>Advertisement</h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Advertisement {{ $adv->id }}</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="إخفاء">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="{{ url('admin/adv_general/' . $adv->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Slider"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/adv_general', $adv->id],
                    'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Advertisement',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                        <tr><th>ID</th><td>{{ $adv->id }}</td></tr>
                        <tr><th> Image </th>
                        <td>{!!App\Helper::image($adv->image)!!}</td></tr>
                        {{--<tr><th> Content </th><td>{{ $adv->content }}</td></tr>--}}
                        <tr><th> Link </th><td>{{ $adv->link }}</td></tr>
                        <tr><th> Type</th><td>{{$adv->type}}</td></tr>
                        <tr><th> Active </th><td>{{($adv->active==1)? "Yes":"No"}}</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <a href="{!!url('admin/adv_general')!!}" class="btn btn-default">Go Back</a>
            </div>
        </div>
@endsection