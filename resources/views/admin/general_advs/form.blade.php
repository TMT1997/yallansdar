@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
        <h1> اعلانات عامة</h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">اضافة عنصر جديد</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['url' => '/admin/adv_general', 'class' => 'form-horizontal', 'files' => true]) !!}

                <div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
                    {!! Form::label('link', 'اضافة لينك للأعلان', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('link', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('is_link', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                    {!! Form::label('type', 'مكان النشر', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('type',
                        ['الصفحة الرئيسية' => 'الصفحة الرئيسية',
                         'صفحة المنتجات' => 'صفحة المنتجات']); !!}
                        {!! $errors->first('is_link', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>


                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image', 'صورة الأعلان', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::file('image', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        {!!App\Helper::size(1915,1230)!!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                    {!! Form::label('active', 'تفعيل الظهور', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label>{!! Form::radio('active', '1' , true) !!} نعم</label>
                            <label>{!! Form::radio('active', '0') !!} ﻻ</label>
                        </div>
                        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-4">
                        {!! Form::submit('إضافة', ['class' => 'btn btn-primary']) !!}
                        <a href="{!!url('admin/adv_general')!!}" class="btn btn-default">إلغاء</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection