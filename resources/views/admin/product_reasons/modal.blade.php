<div class="col-md-12">
	{{Form::open(['class'=>'form-horizontal reason_form'])}}
		{{Form::hidden('product_id', $product_id)}}
		<div class="form-group">
			<div><a href="{{'product_reasons'}}" class="btn btn-sm btn-info pull-left" target="_blank">Add New Reason</a></div>
			<label>Select Reason</label>
			{{Form::select('product_reason_id', $reasons->lists('title_'.App::getLocale(), 'id'), null, ['class'=>'form-control'])}}
		</div>

		<div class="form-group">
			<a href="javascript:void(0)" class="btn btn-primary btn_set_reason">Submit</a>
		</div>
	{{Form::close()}}
</div>