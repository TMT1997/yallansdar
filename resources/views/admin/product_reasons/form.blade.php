<div class="form-group">
@foreach(App\Language::$langs as $iso=>$lang)
    <?php $lang_name = $lang['name']; ?>
    <div class="col-md-6">
        <div class="{{$errors->has('title_'.$iso)?'has-error':''}}">
            {!! Form::label('title_'.$iso, 'العنوان ('.$lang_name.')', ['class'=>'control-label required']) !!}
            <div>
                {!! Form::text('title_'.$iso, null,['class'=>'form-control']) !!}
                {!! $errors->first('title_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="{{$errors->has('details_'.$iso)?'has-error':''}}">
            {!! Form::label('details_'.$iso, 'تفاصيل السبب ('.$lang_name.')', ['class'=>'control-label required']) !!}
            <div>
                {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso])!!}
                {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
@endforeach
</div>

<div class="form-group">
    <div class="col-md-4">
        {!! Form::submit('حفظ', ['class' => 'btn btn-primary btn_loading']) !!}
        <a href="{!!url('admin/product_reasons')!!}" class="btn btn-default">إلغاء</a>
    </div>
</div>