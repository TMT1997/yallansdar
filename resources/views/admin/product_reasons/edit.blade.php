@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>أسباب رفض المنتجات</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/product_reasons')}}">أسباب رفض المنتجات</a></li>
        <li><a href="{{url('admin/product_reasons/edit')}}">تعديل</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">تعديل دولة: {{ $reason->title_ar }}</h3>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($reason, [
                'method' => 'PATCH',
                'url' => ['/admin/product_reasons', $reason->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include('admin.product_reasons.form')            
            {!! Form::close() !!}
        </div>
    </div>
@endsection