<?php
    use App\Helper;
?>
@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>أسباب رفض المنتجات</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/product_reasons')}}">أسباب رفض المنتجات</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{ url('admin/product_reasons/create?tag='.Request::get('tag')) }}" class="btn btn-primary btn-sm" title="Add New ProductReason"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة جديد</a></h3>
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>مفعل</th>
                            <th>العنوان </th>
                            <th>المحتوى </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($product_reasons as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!App\Helper::sw($item,'active','ProductReason')!!}</td>
                            <td><a href="{{route('admin.product_reasons.edit',$item->id)}}">{{Helper::field($item,'title')}}</a></td>
                            <td>{{ strip_tags(Helper::field($item,'details')) }}</td>
                            <td nowrap="nowrap">
                                <a href="{{ url('/admin/product_reasons/' . $item->id) }}" class="btn btn-success btn-xs" title="View ProductReason"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('/admin/product_reasons/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit ProductReason"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/product_reasons', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete ProductReason" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete ProductReason',
                                        'onclick'=>'return confirm("حذف أسباب رفض المنتجاتة: هل أنت متأكد؟")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>        
      </div>
    </section>
@endsection