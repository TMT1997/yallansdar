<?php
    use App\UserProfile;
    use App\Helper;
    use App\User;
?>
@extends('admin.layouts.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">البروفايل</div>
    <div class="panel-body">
        <div class="well well-sm">
            {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                <div class="form-group">
                    {!!Form::select('status', UserProfile::$status, Request::get('status'), ['class'=>'form-control'])!!}
                </div>
            {!!Form::close()!!}
        </div>
        <table class="table table-bordered table-striped table-hover dataTable">
            <thead>
                <tr>
                    <th>Login</th>
                    <th>الحالة</th>
                    <th>الصورة</th>
                    <th>الاسم</th>
                    <th>الشركة</th>
                    <th>الإيميل</th>
                    <th>الاتصال</th>
                    <th>مهام</th>
                </tr>
            </thead>
            <tbody>
            @foreach($profiles as $item)
                <tr>
                    <td><a href="{{url('admin/profiles/login/'.$item->user_id)}}" class="btn btn-warning btn-xs" title="قم بالتسجيل بهذا المستخدم">تسجيل الدخول</a></td>
                    <td>{!!Helper::statusCtrl(UserProfile::$status,$item,'status','UserProfile')!!}</td>
                    <td>{!!Helper::image($item->picture,['style'=>'max-height:80px;'])!!}</td>
                    <td>
                        {!!User::user($item, $item->first_name.' '.$item->last_name)!!}
                        @if($item->user)
                            <div class="help-block">{{Carbon\Carbon::parse($item->user->created_at)->toDayDateTimeString()}}</div>
                        @endif
                    </td>
                    <td>{{$item->company_name}}</td>
                    <td>
                        <a href="mailto:{{$item->primary_email}}">{{$item->primary_email}}</a> <br>
                        <a href="mailto:{{$item->alt_email}}">{{$item->alt_email}}</a>
                    </td>
                    <td>تليفون.: ({{$item->tel_code}}) {{$item->telephone}} <br>
                        موبايل: ({{$item->mob_code}}) {{$item->mobile}} <br></td>
                    <td>
                        <a href="{{ url('admin/profiles/profile/'.$item->user_id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-eye"/></a>
                        <a href="{{ url('admin/profiles/'.$item->id.'/edit') }}" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection