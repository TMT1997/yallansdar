<?php 
	use App\Helper;
	use App\Settings; 
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
?>
<div class="white-popup-block" style="min-width: 1100px;">	
	<legend>{{trans('dash.products')}}</legend>
	<div role="tabpanel">
		<!--No. Added Product-->
		<fieldset class="the-fieldset">
			<legend class="the-legend">{{trans('dash.no_products')}} 
				<span class="count_item">{{$added_products->count()}}</span></legend>
			@foreach($added_products as $product)
			<li>
			<a href="{{url('admin/products/'.$product->id)}}" class="popup product_name">{{Helper::field($product,'name')}}</a></li>
			@endforeach
		</fieldset>
		<!--No. Opportunity Seen-->
		<fieldset class="the-fieldset">
			<legend class="the-legend">{{trans('dash.no_opportunity_seen')}} 
				<span class="count_item">{{$opportunity_seen->count()}}</span></legend>
			@foreach($opportunity_seen as $product)
			<li>
			<a href="{{url('admin/deals/'.$product->id)}}" class="popup product_name">{{Helper::field($product,'name')}}</a></li>
			@endforeach
		</fieldset>
		<!--No. Offers Sent-->
		<fieldset class="the-fieldset">
			<legend class="the-legend">{{trans('dash.no_opportunity_made')}} 
				<span class="count_item">{{$offers_sent->count()}}</span></legend>
			@foreach($offers_sent as $product)
			<li>
			<a href="{{url('admin/dealsDesc/'.$product->id)}}" class="popup product_name">
				{{$product->subject}}</a></li>
			@endforeach
		</fieldset>
	</div>
	

	<script>
		$(document).ready(function() {
		  $('.popup').magnificPopup({type:'ajax'});
		});
	</script>
</div>

<style type="text/css">
	.f_label 	{margin-top: 7px;}
	.form-group {border-bottom: 1px solid #e0e0e0; padding:6px 5px; margin-bottom: 0;}
	 .the-legend {
        border-style: none;
        border-width: 0;
        font-size: 14px;
        line-height: 20px;
        margin-bottom: 0;
        width: auto;
        padding: 0 10px;
    }
    .the-fieldset {
        border: 1px solid #e0e0e0;
        padding: 10px;
        margin-bottom: 20px;
    }
    .product_name
    {
    	font-size: 20px;
    	font-weight: bold;
    }
    .count_item
    {
    	font-weight: bold;
    	font-size :20px;
    	color:#f79434;
    }
</style>
@section('script')
<script type="text/javascript">
</script>
@endsection