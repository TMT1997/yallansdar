<?php use App\UserProfile; 
    $category = \App\Category::whereId($user_profile->other_products)->first();
?>

<?php $field = 'company_name'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'business_type'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.business_type'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="row"> 
            @if($user_profile->business_type)
            @foreach($user_profile->business_type as $type)
            	<div class="col-md-4">{{trans('dash.business_types')[$type]}}</div>
            @endforeach
            @endif
        </div>
    </div>
</div>

<?php $field = 'co_copy_info'; ?>
{{-- <div class="form-group">
    {!! Form::label($field, trans('dash.company_contact_info'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div> --}}

<?php $field = 'contact_f_name'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.contact_person'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        <div class="f_label">{{$user_profile->$field}} {{$user_profile->contact_l_name}}</div>
    </div>
</div>

<?php $field = 'primary_email'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.primary_email'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'co_address_line1'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.contact_address'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'co_address_line2'; ?>
<div class="form-group">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'co_country_id'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.co_country'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{@$countries[$user_profile->$field]}}</div>
    </div>
</div>

<?php $field = 'co_state'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.co_state'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'co_city'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.co_city'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'co_zipcode'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'main_products'; ?>
<div class="form-group {{ $errors->has('main_products') ? 'has-error' : ''}}">
    {!! Form::label('main_products', trans('dash.main_products'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        <div class="f_label">
        @if(!empty($user_profile->main_products))
            @foreach($user_profile->main_products as $index=>$name)
                <li>{{$name}}</li>
            </div>
            @endforeach
        @endif
        </div>
    </div>
</div>

<?php $field = 'other_products'; ?>
<div class="form-group {{ $errors->has('other_products') ? 'has-error' : ''}}">
    {!! Form::label('other_products', trans('dash.section_prod'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if($category)
            <li>{{$category->name_ar}}</li>
        @endif
    </div>
</div>

<?php $field = 'website'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.website2'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->website))
            @foreach($user_profile->website as $index=>$name)
                <li>{{$name}}</li>
            @endforeach
        @endif
    </div>
</div>

<?php $field = 'div_offices'; ?>
{{-- <div class="form-group">
    {!! Form::label($field, trans('dash.divisional_office'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
    	<div class="help-block">{{trans('dash.division_desc')}}</div>
        <div class="div_office_template hidden">
            @include('dashboard.profile.forms.division_office')
        </div>

        @if($user_profile->offices->count()>0)
            @foreach($user_profile->offices as $office)
                @include('admin.profiles.tabs.division_office_row')
            @endforeach
        @endif
    </div>
</div> --}}

<legend>{{trans('dash.logos_certificates')}}</legend>
<?php $field = 'co_logo'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.company_logo'), ['class'=>'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $filepath = (isset($user_profile))? 'uploads/'.$user_profile->co_logo:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield2'])!!}</a>
        </div>
    </div>
</div>

<?php $field = 'identity_proof'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.upload_identity_proof'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {{-- {!!Form::select('identity_proof_opt', trans('dash.identity_docs'), null, ['class'=>'form-control'])!!} --}}

        @if(isset($user_profile) && !empty($user_profile->identity_proof))
            <a class="pull-right preview_doc btn btn-info" href="{{url('uploads/'.$user_profile->identity_proof)}}">{{trans('dash.perview')}}</a>
        @endif
    </div>
</div>

<?php $field = 'identity_proof2'; ?>
<div class="form-group">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        {{-- {!!Form::select('identity_proof2_opt', trans('dash.identity_docs'), null, ['class'=>'form-control'])!!} --}}
        @if(isset($user_profile) && !empty($user_profile->identity_proof2))
            <a class="pull-right preview_doc btn btn-info" href="{{url('uploads/'.$user_profile->identity_proof2)}}">{{trans('dash.perview')}}</a>
        @endif
        {{-- <div class="help-block">{{trans('dash.imgdoc')}}</div> --}}
    </div>
</div>