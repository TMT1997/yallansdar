<?php
    use App\UserProfile;
    $countries = array_prepend($countries->toArray(), trans('dash.please_select'));
?>

<?php $field = 'business_category'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.business_category'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($user_profile->$field)
        {{$categories->where('id', $user_profile->$field)->$c_name}}
        @endif
    </div>
</div>

<?php $field = 'product_we_source'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.products_we_source'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if(!empty($user_profile->product_we_source))
            @foreach($user_profile->product_we_source as $index=>$val)
                <table>
                    <tr>
                        <td>{{$val}}</td>
                        <td>{{@$user_profile->quantity[$index]}}</td>
                        <td>{{@trans('dash.units')[$user_profile->qtyunit[$index]]}}</td>
                    </tr>
                </table>
            </div>
            @endforeach
        @endif
    </div>
</div>

<?php $field = 'preferred_category'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.preferred_category'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8 form-inline">
        @if($user_profile->$field)
        {{$categories->where('id', $user_profile->$field)->$c_name}}
        @endif
    </div>
</div>

<?php $field = 'buying_frequency'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.buying_frequency'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{trans('dash.buying_frequencies')[$user_profile->$field]}}</div>
    </div>
</div>

<?php $field = 'supplier_country_id1'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.preferred_supplier_location'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($user_profile->$field)
            <div class="f_label">{{$countries[$user_profile->$field]}}</div>
        @endif
    </div>
</div>

<?php $field = 'supplier_country_id2'; ?>
<div class="form-group">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($user_profile->$field)
            <div class="f_label">{{$countries[$user_profile->$field]}}</div>
        @endif
    </div>
</div>

<?php $field = 'supplier_country_id3'; ?>
<div class="form-group">
    {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($user_profile->$field)
            <div class="f_label">{{$countries[$user_profile->$field]}}</div>
        @endif
    </div>
</div>

<?php $field = 'preferred_supplier_type'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.preferred_supplier_type'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if(!empty($user_profile->$field))
            @foreach($user_profile->$field as $type)
                <a href="#" class="btn btn-default">{{trans('dash.business_types')[$type]}}</a>
            @endforeach
        @endif
    </div>
</div>