<?php use App\UserProfile; ?>

<?php $field = 'established_year'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.Year of Establishment'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'employees_no'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.No. of Employees'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{@trans('dash.employees_nums')[$user_profile->$field]}}</div>
    </div>
</div>

<?php $field = 'turnover'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.Turnover'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{@trans('dash.turnovers')[$user_profile->$field]}}</div>
    </div>
</div>

<?php $field = 'legal_status'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.Legal Status'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{@trans('dash.legal_statuses')[$user_profile->$field]}}</div>
    </div>
</div>

<?php $field = 'legal_owner'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.Legal Owner'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{$user_profile->$field}}</div>
    </div>
</div>

<?php $field = 'office_size'; ?>
<div class="form-group">
    {!! Form::label($field, trans('dash.Registered Office Size'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="f_label">{{@trans('dash.office_sizes')[$user_profile->$field]}}</div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('certificates', trans('dash.Certificates'), ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        @if($user_profile->certificates)
            @foreach($user_profile->certificates as $type)
            	<a href="#" class="btn btn-default">{{$type}}</a>
            @endforeach
        @endif
    </div>
</div>