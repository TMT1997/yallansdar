<div class="well well-sm">
	{!!Form::hidden('divoffice[id][]', null)!!}
	<legend>{{trans('dash.divisional_office_details')}}</legend>
	<?php $field = 'division'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select('divoffice['.$field.'][]', trans('dash.divisions_type'), null, ['class'=>'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'contact_person'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'address_line1'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, 'Contact Address', ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'address_line2'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'country_id'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::select('divoffice['.$field.'][]', $countries, null, ['class' => 'form-control']) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'state'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'city'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!! Form::text('divoffice['.$field.'][]', null, ['class' => 'form-control', 'placeholder'=>trans('dash.'.$field)]) !!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'zipcode'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::text('divoffice['.$field.'][]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'email'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, null, ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8">
	        {!!Form::text('divoffice['.$field.'][]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'telephone'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, 'Phone', ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[tel_code][]', null, ['class'=>'form-control short', 'placeholder'=>'Code'])!!}
	        {!!Form::text('divoffice['.$field.'][]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'mobile'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, 'Mobile', ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[mob_code][]', null, ['class'=>'form-control short', 'placeholder'=>'Code'])!!}
	        {!!Form::text('divoffice['.$field.'][]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
	<?php $field = 'fax'; ?>
	<div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
	    {!! Form::label($field, 'Fax', ['class' => 'col-md-3 control-label']) !!}
	    <div class="col-md-8 form-inline">
	        {!!Form::text('divoffice[fax_code][]', null, ['class'=>'form-control short', 'placeholder'=>'Code'])!!}
	        {!!Form::text('divoffice['.$field.'][]', null, ['class'=>'form-control', 'placeholder'=>trans('dash.'.$field)])!!}
	        {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
	    </div>
	</div>
</div>