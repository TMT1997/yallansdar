<?php
    use App\UserProfile;
    $countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
@if($user_profile)
    <?php $field = 'user_type'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.user_type'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            @if($user_profile->user_type)
                @foreach($user_profile->user_type as $type)
                    <label class="control-label">{{trans('front.'.$type)}}</label>
                @endforeach
            @endif
        </div>
    </div>

    <?php $field = 'first_name'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.first_name'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'last_name'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.last_name'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'gender'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.gender'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'picture'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.profile_picture'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {{App\Helper::image($user_profile->picture, ['style'=>'width:100px;'])}}
        </div>
    </div>

    <?php $field = 'represent_company'; ?>
    {{-- <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.do_u_represent_company'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div> --}}

    <?php $field = 'company_name'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.company_name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'destination'; ?>
    {{-- <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.destination'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div> --}}

    <?php $field = 'department'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.department'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            @if($user_profile->$field == "")
            <div class="f_label"></div>
            @else
            <div class="f_label">{{trans('dash.departments_arr')[$user_profile->$field]}}</div>
            @endif
        </div>
    </div>

    <?php $field = 'primary_email'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.primary_email'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'alt_email'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.alternative_email'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'address_line1'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.address'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'address_line2'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, ' ', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'country_id'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.country'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$countries[$user_profile->$field]}}</div>
        </div>
    </div>

    <?php $field = 'state'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.state'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'city'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.city'), ['class' => 'col-md-3 control-label ']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'zipcode'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.zipcode'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="f_label">{{$user_profile->$field}}</div>
        </div>
    </div>

    <?php $field = 'telephone'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.telephone'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            @if(!empty($user_profile->telephone))
                @foreach($user_profile->telephone as $index=>$val)
                    {{@$user_profile->tel_code[$index]}} {{$val}}
                @endforeach
            @endif
        </div>
    </div>

    <?php $field = 'mobile'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.mobile'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            @if(!empty($user_profile->mobile) && !empty($user_profile->mob_code) && is_array($user_profile->mobile))
                @foreach($user_profile->mobile as $index=>$val)
                {{@$user_profile->mob_code[$index]}} {{$val}}
                @endforeach
            @endif
        </div>
    </div>

    <?php $field = 'fax'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.fax'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            <div class="f_label">
                {{$user_profile->fax_code}} {{$user_profile->$field}}
            </div>
        </div>
    </div>
@endif