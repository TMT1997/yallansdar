<?php use App\Settings; 
	function lab($name) {
		return ucwords(str_replace('_', ' ', $name));
	}
?>
<div class="white-popup-block" style="min-width: 1100px;">	
	<legend>{{trans('dash.profile')}}</legend>
	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">
			@foreach(App\UserProfile::$profile_tabs as $index=>$tab)
				<li role="presentation" class="{{($index==1)?'active':''}}">
					<a href="#n{{$index}}" aria-controls="n{{$index}}" role="tab" data-toggle="tab">{{trans('dash.'.snake_case($tab[0]))}}</a>
				</li>
			@endforeach
		</ul>
		@if(!empty($user_profile))
			{!!Form::model($user_profile, [
				'class'=>'form-horizontal','url'=>'admin/profile','method' => 'POST','files'=>true,'id'=>'profileForm'
			])!!}
			<div class="tab-content" style="padding-top: 20px;">
					@foreach(App\UserProfile::$admin_profile_tabs as $index=>$tab)
						<div role="tabpanel" class="tab-pane {{($index==1)?'active':''}}" id="n{{$index}}">
								@include($tab[1], ['user_profile'=>$user_profile])
						</div>
					@endforeach
			</div>
		@else
			<div class="alert alert-warning">Profile not found</div>
		@endif
		{!!Form::close()!!}	
	</div>
</div>
<style type="text/css">
	.f_label {margin-top: 7px;}
	.form-group {border-bottom: 1px solid #e0e0e0; padding:6px 5px; margin-bottom: 0;}
</style>

@section('script')
<script type="text/javascript">
	function getTemplate($sec)
	{
		$output = '';
		if($sec=='products') {
			$output='<div class="frec">'+
	    	'{!!Form::text('main_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='oproducts') {
			$output='<div class="frec">'+
	    	'{!!Form::text('other_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='website') {
			$output='<div class="frec">'+
	    	'{!!Form::text('website[]',null,['class'=>'form-control','placeholder'=>trans('dash.website')])!!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='product_we_source') {
	    	$output='<div class="frec">'+
	    	'{!!Form::text('product_we_source[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!} '+
	    	'{!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!} '+
	    	'{!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!} <span>({{trans("dash.yearly")}})</span>'+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
			if($sec=='telephone') {
	    	$output='<div class="frec">'+
	    	'{!!Form::text('tel_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::text('telephone[]', null,['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		if($sec=='mobile') {
	    	$output='<div class="frec">'+
	    	'{!!Form::text('mob_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
	    	'{!!Form::text('mobile[]', null,['class'=>'form-control','placeholder'=>trans('dash.mobile')]) !!} '+
	    	'<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
		}
		return $output;
	}

	$(document).on('click', '.add_new_field', function(e){
	    e.preventDefault();
	    var $ft = $(this).data('ft');
	    var max_fields = $(this).data('max');
	    var count_childs = $(this).siblings('.more_fields').children('.frec').length;
	    count_childs = count_childs+1;
	    if(count_childs < max_fields){
	        $wrapper = $(this).siblings('.more_fields');
	        $wrapper.append(getTemplate($ft));
	    } else {
	        alert('Sorry, you cannot add more than '+max_fields+' inputs');
	    }
	});
	$(document).on("click",".remove_field", function(e){
	    e.preventDefault(); $(this).parent('div').remove();
	});

	$(document).on('click', '.add_div_office', function(event) {
		event.preventDefault();
		$('.div_offices').append($('.div_office_template').html());
		$('html, body').animate({
		    scrollTop: $(".div_offices").offset().top
		}, 1000);
	});

	$(document).on('click', '.sameContactInfo', function(event) {
		if($(this).prop('checked')) {
			$('input[name="contact_f_name"]').val($('input[name="first_name"]').val());
			$('input[name="contact_l_name"]').val($('input[name="last_name"]').val());
			$('input[name="co_address_line1"]').val($('input[name="address_line1"]').val());
			$('input[name="co_address_line2"]').val($('input[name="address_line2"]').val());
			$('input[name="co_state"]').val($('input[name="state"]').val());
			$('input[name="co_city"]').val($('input[name="city"]').val());
			$('input[name="co_zipcode"]').val($('input[name="zipcode"]').val());
		} else  {
			//do nothing
		}
	});
	$('#profileForm').validate({
		rules: {
            "user_type":"required",
        },
        messages: {
            "user_type":"يرجى اختيار على اقل خيار واحد ليعبر عن طبيعة الحساب الخاص بك",
        }
	});
	$(document).on('change', '.co_nameper', function(event) {
		event.preventDefault();
		$('.co_nameco').val($(this).val());
	});

	$(document).on('change', '.co_nameco', function(event) {
		event.preventDefault();
		$('.co_nameper').val($(this).val());
	});
</script>
@endsection