@extends('admin.layouts.master')
<style type="text/css">
  .content-wrapper
  {
    min-height: 1200px;
  }
</style>
@section('report_admin')
  <!--collect data blady :) -->
  <div id="users_per_day">
    @foreach($users_daily as $user_per_day)
      <usersday id="{{$user_per_day->day_date}}" style="visibility: hidden">{{$user_per_day->data}}</usersday>
    @endforeach
  </div>
  <div id="users_per_month">
    @foreach($users_monthly as $user_per_month)
      <usersmonth id="{{$user_per_month->month}}" style="visibility: hidden">{{$user_per_month->data}}</usersmonth>
    @endforeach
  </div>
  <div id="users_per_year">
    @foreach($users_yearly as $user_per_year)
      <usersyear id="{{$user_per_year->year}}" style="visibility: hidden">{{$user_per_year->data}}</usersyear>
    @endforeach
  </div>
  <div id="sector_users">
    @foreach($sector_users as $sector_user)
      <usersector id="{{$sector_user->name_ar}}" style="visibility: hidden">{{$sector_user->data}}</usersector>
    @endforeach
  </div>
  <!--end collect data-->
  <!--start charts-->
  <div class="row" style="padding: 15px">
      <div class="col-md-12">
          @if(session()->has('success'))
              <div class="alert alert-success">
                  {{session()->get('success')}}
              </div>
          @elseif(session()->has('empty'))
              <div class="alert alert-danger">
                  {{session()->get('empty')}}
              </div>
          @endif
      </div>
  </div>
  <div class="row" style="padding: 15px">
    {{--no users daily--}}
    <div class="col-md-6">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title" style="text-align: center;display: block;">الاعضاء يوميا</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="users_daily"  width="510" height="265"></canvas>
        </div>
      </div>
    </div>
  </div>
    {{--end no users daily--}}
    {{--no users monthly--}}
    <div class="col-md-6">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title" style="text-align: center;display: block;">الاعضاء شهريا</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="users_months"  width="510" height="265"></canvas>
        </div>
      </div>
    </div>
  </div>
    {{--end no users monthly--}}
    <div class="row" style="padding: 15px">
    {{--no users yearly--}}
    <div class="col-md-3">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title" style="text-align: center;display: block;">الاعضاء سنويا</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="users_years"  width="510" height="265"></canvas>
        </div>
      </div>
    </div>
  </div>
    {{--end no users yearly--}}
     {{--rfq and products and services--}}
    <div class="col-md-3">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center;display: block;">المنتجات والخدمات وفرص التصدير</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <canvas id="pieChart"  width="530" height="265"></canvas>
        </div>
      </div>
    </div>
    {{--end rfq and products and services--}}
     {{--importers and exporters and service providers--}}
    <div class="col-md-3">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center;display: block;">المستوردين و المصدرين و مزودي الخدمة</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <canvas id="im_ex_sp"  width="530" height="265"></canvas>
        </div>
      </div>
    </div>
    {{--end importers and exporters and service providers--}}
    {{--RFQ VS OFFERS--}}
    <div class="col-md-3">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h4 class="box-title" style="text-align: center;display: block;">عروض الاسعار وفرص التصدير التي شاهدت</h4>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <canvas id="rfq_vs_offers"width="530" height="265"></canvas>
        </div>
      </div>
    </div>
    {{--end RFQ VS OFFERS--}}
    {{--user_sectors--}}
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h4 class="box-title" style="text-align: center;display: block;">الاعضاء في كل قطاع </h4>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <canvas id="users_for_each_sector" width="530" height="265"></canvas>
        </div>
      </div>
    </div>
    {{--end user_sectors--}}
  </div>
  </div>
  {{--end charts--}}
  <div class="row" style="padding: 15px">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <a href="{{url('admin/users')}}">
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="fa fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">@lang('front.all_users')</span>
            <span class="info-box-number">{{$user_counts['all']}}</span>

            <div class="progress">
              <div class="progress-bar" style="width: {{($user_counts['all']/$user_counts['all'])*100}}%;"></div>
            </div>
                <span class="progress-description">
                 {{$user_counts['all']}}/{{$user_counts['all']}}
                </span>
          </div>
        </div>
      </a>
    </div>
    @foreach(App\User::$types as $type=>$label)
    <div class="col-md-3">
        <a href="{{url('admin/users?type='.$type)}}">
        <div class="info-box bg-green">
          <span class="info-box-icon"><i class="fa fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">@lang('front.'.$label)</span>
            <span class="info-box-number">{{$user_counts[$type]}}</span>

            <div class="progress">
              <div class="progress-bar" style="width: {{($user_counts[$type]/$user_counts['all'])*100}}%;"></div>
            </div>
                <span class="progress-description">
                 {{$user_counts[$type]}}/{{$user_counts['all']}}
                </span>
          </div>
        </div>
      </a>
  </div>
    @endforeach
  </div>
  <!--end users report-->
  <!--start report products and services and deals and quotes-->
  <div class="row" style="padding: 15px">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$user_counts['all_product']}}</h3>

          <p>@lang('front.products')</p>
        </div>
        <div class="icon">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <a href="{{url('admin/products')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
     <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$user_counts['quote_request']}}</h3>

          <p>@lang('dash.request_for_quote')</p>
        </div>
        <div class="icon">
          <i class="fa fa-truck-moving"></i>
        </div>
        <a href="{{url('admin/deals')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$user_counts['service']}}</h3>

          <p>@lang('front.service')</p>
        </div>
        <div class="icon">
          <i class="fa fa-cog"></i>
        </div>
        <a href="{{url('admin/services')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$user_counts['deal_quote']}}</h3>

          <p>@lang('dash.sell_offers')</p>
        </div>
        <div class="icon">
          <i class="fa fa-ad"></i>
        </div>
        <a href="{{url('admin/deals')}}" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
  </div>
  <!--eno no_product / quote / service / deals-->
@endsection

@section('content')

    @foreach($dash_items as $item=>$data)
        <div class="col-md-4 col-xs-6">
          <div class="small-box bg-{{($pending[$item]>0)?'yellow':'aqua'}}">
            <div class="inner">
              <h3>{{$pending[$item]}}</h3>
              <p>Pending <strong>{{$data['label']}}</strong></p>
            </div>
            <div class="icon"><i class="fa fa-chart"></i></div>
            <a href="{{url('admin/'.$data['url'])}}" class="small-box-footer">التفاصيل <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    @endforeach
@endsection
  @section('script')
<script type="text/javascript">
  $(function () {
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : {{$user_counts['all_product']}},
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'المنتجات'
      },
      {
        value    : {{$user_counts['service']}},
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'الخدمات'
      },
      {
        value    : {{$user_counts['quote_request']}},
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : 'فرص التصدير'
      },
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)

    //-------------
    //- PIE CHART FOR IMPORTERS AND EXPORTERS AND SERVICE PROVIDERS-
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#im_ex_sp').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : {{$user_counts['importer']}},
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'المسنوردين'
      },
      {
        value    : {{$user_counts['exporter']}},
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'المصدرين'
      },
      {
        value    : {{$user_counts['service_provider']}},
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : 'مزودي الخدمة'
      },
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
    //-------------
    //- PIE CHART FOR RFQ SEEN AND OFFERS SENT-
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#rfq_vs_offers').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : {{$no_rfq_seen}},
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'فرص التصدير التي شاهدت'
      },
      {
        value    : {{$no_offers_sent}},
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'عروض الاسعار التي ارسلت'
      },
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)
    //------------------------------------------------------------------------------
    //-------------
    //- BAR CHART FOR USERS PER MONTHS-
    //-------------
      var div = document.getElementById('users_per_month');
      var divs = div.getElementsByTagName('usersmonth');
      var months_values = [];
      for (var i = 0; i < divs.length; i += 1) {
        months_values.push(divs[i].innerHTML);
      }
     var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
      datasets: [
        {
          label               : 'Months',
          fillColor           : '#00c0ef',
          strokeColor         : '#00c0ef',
          pointColor          : '#00c0ef',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : months_values
        },
      ]
    }
    var barChartCanvas                   = $('#users_months').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
    //------------------------------------------------------------------------------
    //-------------
    //- BAR CHART FOR USERS PER YEARS-
    //-------------
      var div = document.getElementById('users_per_year');
      var divs = div.getElementsByTagName('usersyear');
      var year_values = [];
      var years = [];
      for (var i = 0; i < divs.length; i += 1) {
        year_values.push(divs[i].innerHTML);
        years.push(divs[i].id);
      }
     var areaChartData = {
      labels  : years,
      datasets: [
        {
          label               : 'Years',
          fillColor           : '#f56954',
          strokeColor         : '#f56954',
          pointColor          : '#f56954',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : year_values
        },
      ]
    }
    var barChartCanvas                   = $('#users_years').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
    //------------------------------------------------------------------------------
    //-------------
    //- BAR CHART FOR USERS PER DAY-
    //-------------
      var div = document.getElementById('users_per_day');
      var divs = div.getElementsByTagName('usersday');
      var day_values = [];
      var days = [];
      for (var i = 0; i < divs.length; i += 1) {
        day_values.push(divs[i].innerHTML);
        days.push(divs[i].id);
      }
     var areaChartData = {
      labels  : days,
      datasets: [
        {
          label               : 'Day',
          fillColor           : '#00a65a',
          strokeColor         : '#00a65a',
          pointColor          : '#00a65a',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : day_values
        },
      ]
    }
    var barChartCanvas                   = $('#users_daily').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
    //-------------
    //- Bar CHART FOR Users for each sector-
    //-------------
    var div = document.getElementById('sector_users');
    var divs = div.getElementsByTagName('usersector');
    var secor_names = [];
    var sector_values = [];
    for (var i = 0; i < divs.length; i += 1) {
      secor_names.push(divs[i].id);
      sector_values.push(divs[i].innerHTML)
    }
    var areaChartData = {
      labels  : secor_names,
      datasets: [
        {
          label               : 'Day',
          fillColor           : '#00a65a',
          strokeColor         : '#00a65a',
          pointColor          : '#00a65a',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : sector_values
        },
      ]
    }
    var barChartCanvas                   = $('#users_for_each_sector').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })
</script>
@endsection