<?php
    use App\Helper;
?>

<div class="white-popup-block">
	<legend>@lang('front.business_developers'): {{ $item->name }}</legend>
	<table class="table table-bordered table-striped table-hover">
		
	    <tr><td width="220">رقم</td><td>{{ $item->id }}</td></tr>
	    <tr><td>الاسم</td><td>{{ $item->name }}</td></tr>
	    <tr><td>المهنة</td><td>{{ $item->jobtitle }}</td></tr>
	    <tr><td>الشركة</td><td>{{ $item->company }}</td></tr>
	    <tr><td>التليفون</td><td>{{ $item->phone }}</td></tr>
	    <tr><td>الإيميل</td><td><a href="mailto:{{$item->email}}">{{$item->email}}</a></td></tr>
	    <tr><td>المنتجات التي يرغب العمل بها</td><td>{{ $item->products }}</td></tr>
	    <tr><td>جواز السفر</td><td>{!!Helper::image($item->passport,['style'=>'width:200px'])!!}</td></tr>

	    <tr><td>Skype</td><td><a href="{{ $item->skype }}" target="_blank">{{ $item->skype }}</a></td></tr>
	    <tr><td>Facebook</td><td><a href="{{ $item->facebook }}" target="_blank">{{ $item->facebook }}</a></td></tr>
	    <tr><td>Twitter</td><td><a href="{{ $item->twitter }}" target="_blank">{{ $item->twitter }}</a></td></tr>
	    <tr><td>Linked In</td><td><a href="{{ $item->linkedin }}" target="_blank">{{ $item->linkedin }}</a></td></tr>

	    <tr><td>featured</td><td>{!!Helper::sw($item,'featured','BusinessDeveloper')!!}</td></tr>
		
	</table>
</div>