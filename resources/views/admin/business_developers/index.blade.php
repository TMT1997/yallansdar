@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>@lang('front.business_developers')</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/business-developers')}}">@lang('front.business_developers')</a></li>
      </ol>
    </section>
    <section class="content">        
        <div class="well well-sm">
            {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                {!!Form::text('q', Request::get('q'), ['placeholder'=>'Search...', 'class'=>'form-control'])!!}
                {!!Form::select('no_results',[5=>5,7=>7,1=>10,20=>20,50=>50,100=>100],Request::get('no_results'),['class'=>'form-control'])!!}
            {!! Form::close() !!}
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>الاسم</th>
                            <th>المهنة</th>
                            <th>الشركة</th>
                            <th>التليفون</th>
                            <th>الإيميل</th>
                            <th>Featured </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($bs_developers as $item)
                        <tr id="tr_{{$item->id}}">
                            <td>{{ $item->id }}</td>
                            <td><a href="{{url('admin/business-developers/'.$item->id)}}" class="btn btn-default btn-sm popup"><strong>{{ $item->name }}</strong></a></td>
                            <td>{{ $item->jobtitle }}</td>
                            <td>{{ $item->company }}</td>
                            <td>{{ $item->phone }}</td>
                            <td><a href="mailto:{{$item->email}}">{{$item->email}}</a></td>
                            <td>{!!App\Helper::sw($item,'featured','BusinessDeveloper')!!}</td>
                            <td>
                                <a href="{{url('admin/business-developers/'.$item->id)}}" class="btn btn-info btn-sm popup">View</a>
                                {!! Form::open([
                                    'method'=>'DELETE','url'=>['/admin/business-developers',$item->id],'style'=>'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete" />', array(
                                        'type' => 'submit','class' => 'btn btn-danger btn-sm',
                                        'onclick'=>'return confirm("حذف: هل أنت متأكد؟")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            
            {{ $bs_developers->appends([
                'no_results'=>Request::get('no_results',7),
                'q'=>Request::get('q')])->links()
             }}
        </div>
   </div>
</section>
@endsection