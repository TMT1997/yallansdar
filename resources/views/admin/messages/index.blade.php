@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>الرسائل</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/messages')}}">الرسائل</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="إخفاء">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover table-bordered dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>اﻻسم</th>
                            <th>الموبايل </th>
                            <th>اﻻيميل </th>
                            <th>الرسالة </th>
                            <th>تم الرد </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $item)
                        <tr class="{{($item->read==0)?'unread':''}}">
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->mobile }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ str_limit($item->message,120) }}</td>
                            <td>{!!App\Helper::sw($item,'replied','Message')!!}</td>
                            <td>
                                <a href="{{ url('/admin/messages/' . $item->id) }}" class="btn btn-success btn-sm" title="عرض الرسالة"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/messages', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Message" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'حذف الرسالة',
                                            'onclick'=>'return confirm("حذف الرسالة: هل أنت متأكد؟")'
                                    )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>        
      </div>
    </section>
    <style type="text/css">
        .unread td {font-weight: bold;}
    </style>
@endsection