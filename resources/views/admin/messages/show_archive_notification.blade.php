<div class="panel panel-default">
    <div class="panel-heading" style="text-align: center;">ارشيف العضو</div>
    <div class="panel-body">
        <br/>
        <div id="succ">

        </div>
        <table class="table table-bordered table-striped table-hover dataTable">
            <thead>
            <tr>
                <th>تسلسلي</th>
                <th>رقم</th>
                <th>العنوان</th>
                <th>الرسالة</th>
                <th>التاريخ</th>
                <th>الحدث</th>
            </tr>
            </thead>
            <tbody>
            @foreach($notifications as $index=>$item)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{ $item->id }}</td>
                    {{--<td>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-4" style="max-height:130px; overflow: hidden;">{!!Helper::image($item->attachments->file,['class'=>'img-responsive'])!!}</div>--}}
                    {{--{{$item->attachments}}--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    <td style="text-align: center;width: 150px">
                        @if(App::isLocale('ar'))
                            {{\App\Notification::$NOTIFICATION_EVENTS[$item->section]['value_ar']}}
                          @else
                            {{\App\Notification::$NOTIFICATION_EVENTS[$item->section]['value_en']}}
                          @endif
                    </td>
                    <td style="text-align: center;">
                        {{$item->message}}
                    </td>
                    <td style="width:100px">{!! $item->created_at !!}</td>
                    <td style="text-align: center;">
                        {{-- <a href="{{ url('/admin/reviews/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> --}}
                        @if(\App\Notification::$NOTIFICATION_EVENTS[$item->section]['route'])
                            <a href="{{url(\App\Notification::$NOTIFICATION_EVENTS[$item->section]['route'].$item->section_id)}}" style="white-space: unset" class="popup">
                                <li class="fa fa-eye"></li>
                            </a>
                        @else
                            <a style="white-space: unset"></a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        $(document).ready(function() {
          $('.popup').magnificPopup({type:'ajax'});
        });
    </script>
</div>