

  {{--Created by TMT.--}}
  {{--User: Talaat--}}
  {{--Date: 7/26/2018--}}
  {{--Time: 10:47 AM--}}
 {{----}}

<?php
use App\Review;
use App\User;
use App\Helper;
$action_filter_en = [
        ''=>'كل ',
        'Add_Product'=>'Add_Product',
        'Add_User'=>'Add_User',
        'Profile_Completed'=>'Profile_Completed',
        'Select_Package'=>'Select_Package',
        'Rfq_Seen'=>'Rfq_Seen',
        'Member_Message'=>'Member_Message',
    ];
$action_filter_ar = [
    ''=>'كل ',
    'Add_Product'=>'اضافة المنتجات',
    'Add_User'=>'اضافة عضو',
    'Profile_Completed'=>'انتهاء البروفايل',
    'Select_Package'=>'اختيار باقة',
    'Rfq_Seen'=>'مشاهدة فرصة',
    'Member_Message'=>'تواصل بين عضوين',
];
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">@lang('dash.notification_settings')</div>
        <div class="panel-body">
            <div class="row">
                {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                @if (App::isLocale('en'))
                    {!!Form::select('action_filter',$action_filter_en, Request::get('action_filter'),['class'=>'form-control input-sm'])!!}
                @else
                    {!!Form::select('action_filter',$action_filter_ar, Request::get('action_filter'),['class'=>'form-control input-sm'])!!}
                @endif
                {!! Form::close() !!}
            </div>
            <br/>
            <div id="succ">

            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                <tr>
                    <th>تسلسلي</th>
                    <th> العضو</th>>
                    {{-- <th style="visibility: hidden;">رقم</th> --}}
                    <th>العنوان</th>
                    <th>الرسالة</th>
                    <th>التاريخ</th>
                    <th>الحدث</th>
                    <th>الارشيفٍٍ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($notifications as $index=>$item)
                    <tr>
                        <td>{{$index+1}}</td>
                       {{--  <td style="visibility: hidden;">{{ $item->id }}</td> --}}
                       <td><a href="{{url('admin/profiles/profile/'.$item->user_id)}}" class="btn btn-default btn-sm popup">Member</a></td>
                        <td style="text-align: center;width: 150px">
                            @if(App::isLocale('ar'))
                                {{\App\Notification::$NOTIFICATION_EVENTS[$item->section]['value_ar']}}
                              @else
                                {{\App\Notification::$NOTIFICATION_EVENTS[$item->section]['value_en']}}
                              @endif
                        </td>
                        <td style="text-align: center;">
                            {!!$item->message!!}
                        </td>
                        <td style="width:100px">{!! $item->created_at !!}</td>
                        <td style="text-align: center;">
                            {{-- <a href="{{ url('/admin/reviews/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> --}}
                            @if(\App\Notification::$NOTIFICATION_EVENTS[$item->section]['route'])
                                <a href="{{url(\App\Notification::$NOTIFICATION_EVENTS[$item->section]['route'].$item->section_id)}}" style="white-space: unset" class="popup">
                                    <li class="fa fa-eye"></li>
                                </a>
                            @else
                                <a style="white-space: unset"></a>
                            @endif
                        </td>
                        <td style="text-align: center;">
                            <a href="{{ url('admin/notification/archive/'.$item->user_id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-history"/>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            //  check if the status changed
            var test = $('#succ');
            $('.selector').on('change',function (e) {
                test.addClass('alert alert-success');
                test.html('تمت العملية بنجاح');
            });
        });
    </script>
@endsection