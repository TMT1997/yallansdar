<div class="form-group {{ $errors->has('tag') ? 'has-error' : ''}}">
    {!! Form::label('tag', 'القسم الخاص بالصفحة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select('tag', App\Page::$tags,Request::get('tag'),['class' => 'form-control', 'required'=>'required']) !!}
        {!! $errors->first('tag', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'العنوان', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('subtitle') ? 'has-error' : ''}}">
    {!! Form::label('subtitle', 'العنوان الفرعي', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
        {!! $errors->first('subtitle', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'الصورة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!!App\Helper::image(@$page->image)!!}
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('banner') ? 'has-error' : ''}}">
    {!! Form::label('banner', 'الصورة العريضة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!!App\Helper::image(@$page->image)!!}
        {!! Form::file('banner', null, ['class' => 'form-control']) !!}
        {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
    {!! Form::label('details', 'التفاصيل', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('details', null, ['class' => 'form-control','id'=>'editor']) !!}
        {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'تفعيل الظهور', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
    <div class="checkbox">
        <label>{!! Form::radio('active', '1') !!} نعم</label>
        <label>{!! Form::radio('active', '0', true) !!} ﻻ</label>
    </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit('إضافة', ['class' => 'btn btn-primary']) !!}
        <a href="{!!url('admin/pages?tag='.Request::get('tag'))!!}" class="btn btn-default">إلغاء</a>
    </div>
</div>