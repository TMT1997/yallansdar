@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>الصفحات</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/pages')}}">الصفحات</a></li>
        <li><a href="{{url('admin/pages/create')}}">إضافة صفحة جديدة</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">إضافة صفحة جديدة</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => '/admin/pages', 'class' => 'form-horizontal', 'files' => true]) !!}
            @include('admin.pages.form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection