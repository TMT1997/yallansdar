@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>
        @if(Request::has('tag'))
            {{App\Page::$tags[Request::get('tag')]}}
        @else
            الصفحات
        @endif
        </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/pages')}}">الصفحات</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{ url('/admin/pages/create?tag='.Request::get('tag')) }}" class="btn btn-primary btn-sm" title="Add New Page"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة صفحة جديدة</a></h3>
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>مفعل</th>
                            <th>مضاف للقائمة</th>
                            <th>القسم</th>
                            <th>العنوان </th>
                            <th>الصورة </th>
                            <th>التفاصيل </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!App\Helper::sw($item,'active','Page')!!}</td>
                            <td>{!!App\Helper::sw($item,'addtomenu','Page')!!}</td>
                            <td>{{@App\Page::$tags[$item->tag]}}</td>
                            <td><a href="{{route('admin.pages.edit', $item->id)}}">{{ $item->title }}</a></td>
                            <td>{!!App\Helper::image($item->image)!!}</td>
                            <td>{{ str_limit(strip_tags($item->details),150) }}</td>
                            <td nowrap="nowrap">
                                <a href="{{ url('/admin/pages/' . $item->id) }}" class="btn btn-success btn-sm" title="View Page"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="Edit Page"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/pages', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Page" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete Page',
                                        'onclick'=>'return confirm("حذف الصفحة: هل أنت متأكد؟")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>        
      </div>
    </section>
@endsection