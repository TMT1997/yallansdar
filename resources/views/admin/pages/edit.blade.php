@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>الصفحات</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/pages')}}">الصفحات</a></li>
        <li><a href="{{url('admin/pages/edit')}}">تعديل صفحة</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">تعديل الصفحة: {{ $page->title }}</h3>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($page, [
                'method' => 'PATCH',
                'url' => ['/admin/pages', $page->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include('admin.pages.form')            
            {!! Form::close() !!}
        </div>
    </div>
@endsection