<div class="white-popup-block">
	<legend>Add New Category</legend>

	{!!Form::open(['class'=>'form-horizontal quickAddCatForm'])!!}
		{!!Form::hidden('parent_category_id', $parent_cat_id)!!}
		<div class="form-group">
			<label class="control-label">
				Category English
				{!!Form::text('name_en', null,['class'=>'form-control'])!!}
			</label>
		</div>

		<div class="form-group">
			<label class="control-label">
				Category العربي
				{!!Form::text('name_ar', null,['class'=>'form-control'])!!}
			</label>
		</div>

		<div class="form-group">
			{!!Form::submit('Add', ['class'=>'btn btn-success quickAddCat', 'data-parent'=>$parent_cat_id])!!}
		</div>

	{!!Form::close()!!}
</div>