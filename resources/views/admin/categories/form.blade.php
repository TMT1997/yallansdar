<?php use App\Category; ?>
@if(in_array($category->parent_category_id, $parents->lists('id')->toArray()))
    <div class="form-group {{ $errors->has('parent_category_id') ? 'has-error' : ''}}">
        {!! Form::label('parent_category_id', 'Parent Category', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!!Form::select('parent_category_id', $parents->lists('name_ar', 'id'), null, ['class'=>'form-control select2'])!!}
            {!! $errors->first('parent_category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif

@if($is_subsub)    
    <div class="form-group {{ $errors->has('parent_category_id') ? 'has-error' : ''}}">
        {!! Form::label('parent_category_id', 'Level 1', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 level1Wrap">
            {!!Form::select('parent_category_id', $parents->lists('name_ar', 'id'), $tree['parentIDOflevel2'], ['class'=>'form-control level1Cats select2'])!!}
            {!! $errors->first('parent_category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('parent_category_id') ? 'has-error' : ''}}">
        {!! Form::label('parent_category_id', 'Level 2', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 level2Wrap">
            {!!Form::select('parent_category_id',$tree['parent2_childs']->lists('name_ar','id'), $category->parent_category_id,['class'=>'form-control level2Cats select2'])!!}
            {!! $errors->first('parent_category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif

<div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
    {!! Form::label('name_ar', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::text('name_ar', null, ['class'=>'form-control', 'placeholder'=>'Name'])!!}
        {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('name_en') ? 'has-error' : ''}}">
    {!! Form::label('name_en', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::text('name_en', null, ['class'=>'form-control', 'placeholder'=>'Last Name'])!!}
        {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!!Form::textarea('description', null, ['class'=>'form-control', 'placeholder'=>'description', 'id'=>'editor'])!!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $imagepath = (isset($category))? 'uploads/'.$category->image:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($imagepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>            
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('image',['class'=>'form-control img_browse','data-fid'=>'1'])!!}</h4>
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('banner') ? 'has-error' : ''}}">
    {!! Form::label('banner', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $imagepath = (isset($category))? 'uploads/'.$category->banner:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($imagepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>            
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('banner',['class'=>'form-control img_browse','data-fid'=>'2'])!!}</h4>
            </div>
        </div>
        {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('menuimage') ? 'has-error' : ''}}">
    {!! Form::label('menuimage', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $imagepath = (isset($category))? 'uploads/'.$category->menuimage:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($imagepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>            
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('menuimage',['class'=>'form-control img_browse','data-fid'=>'3'])!!}</h4>
            </div>
        </div>
        {!! $errors->first('menuimage', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('menuimage_ar') ? 'has-error' : ''}}">
    {!! Form::label('menuimage_ar', null, ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        <div class="media">
            <?php $imagepath = (isset($category))? 'uploads/'.$category->menuimage_ar:'frontend/images/no_thumb.jpg';?>
            <a class="pull-left" href="#">{!!HTML::image($imagepath,'',['class'=>'media-object img_preview imgfield1','style'=>'max-width:120px;'])!!}</a>            
            <div class="media-body">
                <h4 class="media-heading">{!!Form::file('menuimage_ar',['class'=>'form-control img_browse','data-fid'=>'3'])!!}</h4>
            </div>
        </div>
        {!! $errors->first('menuimage_ar', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-4">
        {!!Form::submit(isset($submitButtonText) ? $submitButtonText : 'Add Category', ['class' => 'btn btn-loading btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        $('body').on('change', '.level1Cats', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $('.level2Wrap').html('');
            $.ajax({
                url: '{{url('dashboard/products/subcats')}}/'+ $cat_id,
                type: 'GET',
                beforeSend:function() {
                    $('.level2Wrap').html('Loading sub categories...');
                }
            }).done(function(response) {
                if(response!=='') {
                    $('.level2Wrap').html('<select name="parent_category_id" size="8" class="form-control level2Cats select2 sub_cat_selector" style="display:inline-block;">'+response+'</select>');
                }
            }).fail(function() {
                alert('Error while process sub categories');
            }).always(function() {
                $('.select2').select2({});
            });
        });
    </script>
@endsection
