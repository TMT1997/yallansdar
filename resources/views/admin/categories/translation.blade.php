@extends('admin.layouts.master')
@section('content')
	
	<div class="well well-sm">
        <div class="row">
            <div class="form-group cat_cord"> 
                <div class="col-md-3 parent"></div>
                <div class="col-md-5 sub" data-id=""></div>
                <div class="col-md-4 subsub" data-id=""></div>
            </div>
        </div>
	</div>

    <div id="selected_category_id"></div>
    <div class="selected_cat"></div>

    <style type="text/css">
        .cat_fields, .cat_selector {max-height: 400px; overflow: auto;}
        .catHighlight {font-weight: bold;}
        .catHighlight input {background: #8e4b9a; color: #ffff00; border:2px solid #ec86ac;}
        .selected_cat {text-align: center; font-size: 20px;}
        .btn_savecats {margin:10px 0;}
        .cat_cord .col-md-3, .cat_cord .col-md-5, .cat_cord .col-md-4 {padding-right:4px; padding-left: 4px;}
        .catTransForm input[type="text"] {padding:7px 2px;}
        .catTransForm .table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th {padding:5px 2px;}
        @media (min-width: 768px) {
            .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .right-side, .sidebar-mini.sidebar-collapse .main-footer {
                margin-right:51px!important;
            }    
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript">
        var csrf_token = '{{csrf_token()}}';
        function getParentCats()
        {
            $cat_id = 0;
            $('.sub').html(''); $('.subsub').html('');
            $.ajax({
                url: '{{url('admin/categories/sub')}}/0',
                type: 'GET',
            }).done(function(response) {
                $('.selected_cat').html(getSelectedText('.cat_selector', $cat_id));
                if(response!=='') {
                    $('.parent').html(response);
                } else {
                    $('.selected_cat').append(' ... DONE');
                    $("#selected_category_id").val($cat_id);
                }
            }).fail(function() {
                alert('Error while get parent categories');
            }).always(function() {
            });
        }
        function deleteCtrl($id) {
            $('.delTrs').hide('fast', function() {
                $('.delTr_'+$id).slideDown('fast', function() {
                });
            });
        }
        $(document).ready(function() {
            getParentCats();
            $(document).on('click', '.delcat', function(event) {
                $btn = $(this);
                if(window.confirm('Trash Category: Are you sure?'))
                {
                    event.preventDefault();
                    $id = $(this).data('id');
                    $.ajax({
                        url: '{{url('admin/categories/trash')}}',
                        type: 'POST',
                        data: {_token: csrf_token, id: $id},
                        beforeSend: function() {
                            $btn.button('loading');
                        }
                    }).done(function() {
                        $('.trcat_'+$id).slideUp('fast', function(){
                            $btn.val('deleted');
                        })
                    }).fail(function() {
                    }).always(function() {
                        $('.delTr_'+$id).slideUp();
                    });
                }
            });
            $(document).on('click', '.parent tr.tr_cat_row', function(event) {
                event.preventDefault();
                $(this).siblings('.tr_cat_row').removeClass('catHighlight');
                $(this).addClass('catHighlight');
                $cat_id = $(this).data('catid');
                deleteCtrl($cat_id);
                $parent_id = $('.cat_selector').val();
                $text = $(this).find('input[type=text].in_en').val();

                $('.subsub').html('');$('.sub').html('');
                $.ajax({
                    url: '{{url('admin/categories/sub')}}/'+ $cat_id,
                    type: 'GET',
                }).done(function(response) {
                    $('.selected_cat').html($text);
                    if(response!=='') {
                        $('.sub').html(response);
                    } else {
                        $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                        $("#selected_category_id").val($cat_id);
                    }
                }).fail(function() {
                    alert('Error while process sub of sub categories');
                }).always(function() {
                    //check if subsub has data-id, this is used in the Edit Product
                    if($('.subsub').data('id')!='') {
                        $('.subsub_cat_selector').val($('.subsub').data('id'));
                    }
                });
            });
            $(document).on('click', '.sub tr.tr_cat_row', function(event) {
                event.preventDefault();
                $(this).siblings('.tr_cat_row').removeClass('catHighlight');
                $(this).addClass('catHighlight');
                $cat_id = $(this).data('catid');
                $parent = $('.parent').find('tr.catHighlight').find('input.in_en').val();
                $text = $(this).find('input[type=text].in_en').val();
                deleteCtrl($cat_id);
                $('.subsub').html('');
                $.ajax({
                    url: '{{url('admin/categories/sub')}}/'+ $cat_id,
                    type: 'GET',
                }).done(function(response) {
                    $('.selected_cat').html($parent + ' >> ' + $text);
                    if(response!=='') {
                        $('.subsub').html(response);
                    } else {
                        $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                        $("#selected_category_id").val($cat_id);
                    }
                }).fail(function() {
                    alert('Error while process sub of sub categories');
                }).always(function() {
                    //check if subsub has data-id, this is used in the Edit Product
                    if($('.subsub').data('id')!='') {
                        $('.subsub_cat_selector').val($('.subsub').data('id'));
                    }
                });
            });
            $(document).on('click', '.subsub tr.tr_cat_row', function(event) {
                $('.subsub tr.tr_cat_row').removeClass('catHighlight');
                $(this).addClass('catHighlight');
                $cat_id = $(this).data('catid');
                deleteCtrl($cat_id);
            });
            $(document).on('change', '.subsub_cat_selector', function(event) {
                event.preventDefault();
                $cat_id = $(this).val();
                $parent_cat_id = $('.cat_selector').val();
                $sub_cat_id = $('.sub_cat_selector').val();

                $('.selected_cat').html(getSelectedText('.cat_selector', $parent_cat_id));
                $('.selected_cat').append(' >> ' + getSelectedText('.sub_cat_selector', $sub_cat_id));
                $('.selected_cat').append(' >> ' + getSelectedText('.subsub_cat_selector', $cat_id));
                $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');

                $("#selected_category_id").val($cat_id);
            });

            $(".cat_select").on('change', function(event) {
                event.preventDefault();
                $(".selectcatForm").submit();
            });
        });
        $('body').on('click', '.btn_cat_search', function(event) {
            event.preventDefault();
            $('.cat_set_fsearch').trigger('blur');
        });
        function getSelectedText($obj, $val) {
            return $($obj + " option[value='"+$val+"']").text();
        }

        $(document).on('click', '.btn_savecats', function(event) {
            event.preventDefault();
            var $btn = $(this);
            var $form = $(this).parent('.catTransForm');
            var $state = $btn.siblings('.save_state');

            $.ajax({
                url: '{{url('admin/categories/save')}}',
                type:'POST',
                data: $form.serialize(),
                beforeSend: function() {
                    $btn.button('loading');
                }
            }).done(function(response) {
                var output = jQuery.parseJSON(response);
                if(output.status=='success') {
                    $state.html('<div class="alert alert-success">Saved Successfully</div>');
                } else {
                    $state.html('<div class="alert alert-danger">Error while saving data</div>');
                }
            }).fail(function() {
                $state.html('<div class="alert alert-danger">Exception Error</div>');
            }).always(function() {
                $btn.button('reset');
            });
        });

        $(document).on('click', '.quickAddCat', function(event) {
            event.preventDefault();
            $parent_id = $(this).data('parent');
            quickAdd('{{url('admin/categories/quickadd')}}', '.quickAddCat', $parent_id);
        });

        function quickAdd(URL, btn, parent_id)
        {
            var $table = $('.table_'+parent_id);
            var $state = $table.parent('.cat_fields').siblings('.save_state');
            $btn = $(btn);
            $.ajax({
                url: URL,
                data: $(".quickAddCatForm").serializeArray(),
                type:'POST',
                dataType:'json',
                beforeSend:function() {
                    $btn.button('loading');
                }
            }).done(function(output) {
                if(output.status=='success') {
                    var $id = output.data.id;
                    var $name_en = output.data.name_en;
                    var $name_ar = output.data.name_ar;

                    $table.append('<tr data-catid="'+$id+'" class="tr_cat_row trcat_'+$id+'">'+
                        '<td><input class="form-control in_en input-sm" name="cats['+$id+'][en]" type="text" value="'+$name_en+'"></td>'+
                        '<td><input class="form-control in_ar input-sm" name="cats['+$id+'][ar]" type="text" value="'+$name_ar+'"></td></tr>'+
                        '<tr class="delTrs delTr_'+$id+'" style="display: none;">'+
                        '<td colspan="2"><a href="javascript:void(0)" data-id="'+$id+'" class="delcat btn btn-xs btn-danger">Trash</a> '+
                        '<a href="{{url('admin/categories/edit')}}/'+$id+'" class="btn btn-xs btn-info">Edit</a></td></tr>');

                    $state.html('<div class="alert alert-success">Saved Successfully</div>');
                } else {
                    $state.html('<div class="alert alert-danger">Error while saving data</div>');
                }
            }).fail(function() {
                notify('Error occured, please try again', 'error'); return false;
            }).always(function() {
                $btn.button('reset');
                $.magnificPopup.close();
            });
        }
    </script>
@endsection