@if($childs->count())
	{!!Form::open(['url'=>'admin/categories/save', 'class'=>'form-horizontal catTransForm'])!!}

		<div class="cat_fields">
			<table class="table table-condensed table-striped table-hover table_{{$parent_cat_id}}">
				<thead>
					<tr>
						<th>English</th>
						<th>عربي</th>
					</th>
				</thead>
				<tbody>
				@foreach($childs as $cat)
					<tr data-catid="{{$cat->id}}" class="tr_cat_row trcat_{{$cat->id}}">
						<td>
							{!!Form::text('cats['.$cat->id.'][en]', $cat->name_en, ['class'=>'form-control in_en input-sm'])!!}
						</td>
						<td>
							{!!Form::text('cats['.$cat->id.'][ar]', $cat->name_ar, ['class'=>'form-control in_ar input-sm'])!!}
						</td>
					</tr>
					<tr class="delTrs delTr_{{$cat->id}}" style="display: none;">
						<td colspan="2">
							<a href="javascript:void(0)" data-id="{{$cat->id}}" class="delcat btn btn-xs btn-danger">Trash</a>
							<a href="{{url('admin/categories/edit/'.$cat->id)}}" class="btn btn-xs btn-info">Edit</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>

		{!!Form::submit('Save', ['class'=>'btn btn-primary btn_savecats'])!!}
		<div class="save_state"></div>
	{!!Form::close()!!}
@else
	<div class="alert alert-warning">No child categories here</div>
@endif
<a href="{{url('admin/categories/quickadd/'.$parent_cat_id)}}" class="popup btn btn-xs btn-success">+</a>

<script type="text/javascript">
	$(document).ready(function() {
	  $('.popup').magnificPopup({type:'ajax'});
	});
</script>