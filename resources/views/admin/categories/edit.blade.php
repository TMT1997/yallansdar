@extends('admin.layouts.master')
@section('content')
	
	<legend>Edit Category</legend>
	@if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! Form::model($category, [
        'method' => 'POST',
        'url' => ['admin/categories/update', $category->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

    <div class="well"> 
    	@include ('admin.categories.form', ['submitButtonText' => 'Save Changes'])
    </div>

	{!!Form::close()!!}
@endsection