@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>التصنيفات</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/categories')}}">التصنيفات</a></li>
      </ol>
    </section>
    <section class="content">        
        <div class="well well-sm">
            {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                {!!App\Helper::select('category_id',$parents->lists('name_ar','id'),[''=>'القسم الأب'],Request::get('category_id'))!!}
                {!!Form::text('q', Request::get('q'), ['placeholder'=>'Search Categories', 'class'=>'form-control'])!!}
                {!!Form::select('trashed',[0=>'Not Trashed', 1=>'Trashed'], Request::get('trashed'),['class'=>'form-control'])!!}

                {!!Form::select('no_results',[5=>5,7=>7,1=>10,20=>20,50=>50,100=>100],Request::get('no_results'),['class'=>'form-control'])!!}
                <a href="{{url('admin/categories/translation')}}" class="btn btn-sm btn-primary">Translation</a>
            {!! Form::close() !!}
        </div>

        {!!Form::open(['url'=>'admin/settings/sortitems'])!!}
        {!! Form::hidden('model', '\App\Category') !!}
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Sort</th>
                            <th>الاسم عرربي</th>
                            <th>الاسم English</th>
                            <th>بالقائمة الرئيسية </th>
                            <th>Home Top Cats</th>
                            <th>مفعل </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $item)
                        <tr id="tr_{{$item->id}}" {{($item->trashed)?'bg_trashed':''}}>
                            <td>{{ $item->id }}</td>
                            <td>{!!Form::text('sort['.$item->id.']', $item->sort, ['size'=>2])!!}</td>
                            <td>{{ $item->name_ar }}</td>
                            <td>{{ $item->name_en }}</td>
                            <td>{!!App\Helper::sw($item,'main_nav','Category')!!}</td>
                            <td>{!!App\Helper::sw($item,'hp_top','Category')!!}</td>
                            <td>{!!App\Helper::sw($item,'active','Category')!!}</td>
                            <td>
                                <a href="{{url('admin/categories/edit/'.$item->id)}}" class="btn btn-info btn-sm">Edit</a>
                                @if($item->trashed)
                                    {!!App\Helper::trash_btn('restore',$item,'trashed','Category')!!}

                                    {!! Form::open([
                                        'method'=>'DELETE','url'=>['/admin/categories',$item->id],'style'=>'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Category" />', array(
                                            'type' => 'submit','class' => 'btn btn-danger btn-sm',
                                            'onclick'=>'return confirm("حذف العنصر نهائيا: هل أنت متأكد؟")'
                                    )) !!}
                                    {!! Form::close() !!}
                                @else
                                    {!!App\Helper::trash_btn('trash',$item,'trashed','Category')!!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!!Form::submit('Re-Sort', ['class'=>'btn btn-warning btn-sm'])!!}
                <div class="pull-right"><a href="{{url('admin/settings/re-numbering-sort?where=parent_category_id:0&model=\App\Category')}}" class="btn btn-sm btn-default">Sort Re-numbering</a></div>
            </div>
            

            {{ $categories->appends([
                 'category_id'=>Request::get('category_id'),
                 'no_results'=>Request::get('no_results',7),
                 'trashed'=>Request::get('trashed'),
                'q'=>Request::get('q')])->links()
             }}
        </div>
        {!!Form::close()!!}   
   </div>
</section>
@endsection