@foreach(App\Language::$langs as $iso=>$lang)
    <div class="form-group {{$errors->has('title_'.$iso)?'has-error':''}}">
        {!! Form::label('title_'.$iso, 'اسم القسم ('.$lang['name'].')', ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::text('title_'.$iso, null, ['class'=>'form-control']) !!}
            {!! $errors->first('title_'.$iso, '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endforeach

<div class="form-group {{$errors->has('active')?'has-error':''}}">
    {!! Form::label('active', 'إظهار', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1', null) !!} نعم</label>
            <label>{!! Form::radio('active', '0', null) !!} لا</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'اضافة', ['class' => 'btn btn-primary btn_loading']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">العودة</a>
    </div>
</div>