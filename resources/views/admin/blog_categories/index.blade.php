<?php
    use App\Helper;
    use App\News;
?>
@extends('admin.layouts.master')
@section('title') أقسام المدونة | @endsection
@section('content')
    <section class="content-header">
        <h1>أقسام المدونة
        <a href="{{ url('admin/blog_categories/create') }}" class="btn btn-success pull-left">+ اضف قسم جدبد</a>
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>إظهار</th>
                            <th>اسم القسم</th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($blog_categories as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!Helper::sw($item,'active','BlogCategory')!!}</td>
                            <td><a href="{{url('admin/blog_categories/'.$item->id.'/edit')}}"><strong>{{Helper::field($item,'title')}}</strong></a></td>
                            <td nowrap="">
                                <a href="{{ url('admin/blog_categories/'.$item->id) }}" class="btn btn-success btn-xs" title="View"><span class="fa fa-eye"/></a>
                                <a href="{{url('admin/blog_categories/' .$item->id.'/edit')}}" class="btn btn-primary btn-xs" title="Edit"><span class="fa fa-pencil" /></a>
                                {!!Form::open(['method'=>'DELETE','url'=>['admin/blog_categories',$item->id],'style'=>'display:inline'])!!}
                                {!!Form::button('<span class="fa fa-trash-o" title="Delete record" />', array(
                                    'type' => 'submit', 'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete', 'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection