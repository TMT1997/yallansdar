@extends('admin.layouts.master')

@section('content')
    
    <div class="panel panel-default">
        <div class="panel-heading">Create New Package</div>
        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => '/admin/packages', 'class' => 'form-horizontal', 'files' => true]) !!}
            @include ('admin.packages.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection