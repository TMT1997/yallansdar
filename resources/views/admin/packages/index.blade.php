<?php
    use App\Helper;
?>
@extends('admin.layouts.master')
@section('title') Packages @endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">الباقات</div>
    <div class="panel-body">

        <a href="{{ url('admin/packages/create') }}" class="btn btn-primary btn-sm" title="Add New Package"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
        <a href="{{ url('admin/package_upgrades') }}" class="btn btn-success btn-sm">باقات العملاء</a>

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>اسم الباقة </th>
                        <th> نبذة </th>
                        <th> السعر </th>
                        <th> التجديد </th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($packages as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><strong>{{ Helper::field($item, 'name') }}</strong></td>
                        <td>{{ Helper::field($item, 'brief') }}</td>
                        <td>{{ $item->price }} {{ $item->currency }}</td>
                        <td>{{ $item->period }}</td>
                        <td>
                            <a href="{{ url('/admin/packages/' . $item->id) }}" class="btn btn-success btn-xs" title="View Package"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/packages/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Package"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/packages', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Package" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Package',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper">
                
            </div>
        </div>
    </div>
</div>
@endsection