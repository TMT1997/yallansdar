@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Edit Package {{ $package->id }}</div>
        <div class="panel-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($package, [
                'method' => 'PATCH',
                'url' => ['admin/packages', $package->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include ('admin.packages.form', ['submitButtonText' => 'Update'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection