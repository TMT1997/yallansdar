<?php use App\Packages\Package; ?>
<div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
    {!! Form::label('name_ar', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <label>Arabic </label>
                {!! Form::text('name_ar', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'العربي']) !!}
            </div>
            <div class="col-md-6">
                <label>English </label>
                {!! Form::text('name_en', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'English']) !!}
            </div>
        </div>
        {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('brief_ar') ? 'has-error' : ''}}">
    {!! Form::label('brief_ar', 'Brief', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <label>Arabic </label>
                {!! Form::text('brief_ar', null, ['class'=>'form-control', 'required'=>'required', 'placeholder'=>'العربي']) !!}
            </div>
            <div class="col-md-6">
                <label>English </label>
                {!! Form::text('brief_en', null, ['class'=>'form-control', 'required'=>'required', 'placeholder'=>'English']) !!}
            </div>
        </div>
        {!! $errors->first('brief', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
    {!! Form::label('currency', 'Currency', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('currency', null, ['class' => 'form-control']) !!}
        {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('period') ? 'has-error' : ''}}">
    {!! Form::label('period', 'Period', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('period', Package::$periods, null, ['class' => 'form-control']) !!}
        {!! $errors->first('period', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('recommended') ? 'has-error' : ''}}">
    {!! Form::label('recommended', 'Recommended', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('recommended', '1') !!} Yes</label>
            <label>{!! Form::radio('recommended', '0', true) !!} No</label>
        </div>
        {!! $errors->first('recommended', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1') !!} Yes</label>
            <label>{!! Form::radio('active', '0', true) !!} No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('free') ? 'has-error' : ''}}">
    {!! Form::label('free', 'Is Free?', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('free', '1', null) !!} Yes</label>
            <label>{!! Form::radio('free', '0', null) !!} No</label>
        </div>
        {!! $errors->first('free', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('sort_order') ? 'has-error' : ''}}">
    {!! Form::label('sort_order', 'Sort Order', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">العودة</a>
    </div>
</div>