@extends('admin.layouts.master')

@section('content')

    <section class="content-header">
      <h1>Slider</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/sliders')}}">Sliders</a></li>
        <li><a href="{{url('admin/sliders/show')}}">Show Slider</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Slider {{ $slider->id }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="إغلاق">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="إخفاء">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <a href="{{ url('admin/sliders/' . $slider->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Slider"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/sliders', $slider->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Slider',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $slider->id }}</td>
                        </tr>
                        <tr><th> Title </th><td> {{ $slider->title }} </td></tr><tr><th> Image </th>
                        <td>{!!App\Helper::image($slider->image)!!}</td></tr>
                        <tr><th> Content </th><td> {{ $slider->content }} </td></tr>
                        <tr><th> Link </th><td> {{ $projects[$slider->link] }} </td></tr><tr><th> Type </th><td> {{ $slider->type }} </td></tr><tr><th> Active </th><td> {{ $slider->active }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            <a href="{!!url('admin/sliders')!!}" class="btn btn-default">Go Back</a>
        </div>
    </div>
@endsection