@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>السليدر المتحرك</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/sliders')}}">السليدر المتحرك</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{ url('/admin/sliders/create') }}" class="btn btn-primary btn-sm" title="أضافة سليدر جديد"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة عنصر جديد</a></h3>
          <div class="box-tools pull-right">
            
          </div>
        </div>
        <div class="box-body">
            <div class="table">
                {!! Form::open(['url' => 'admin/settings/slidersort', 'class'=>'form-inline']) !!}
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th> الصورة </th>
                            <th> العنوان </th>
                            <th>العنوان الفرعي</th>
                            <th> مفعل </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($sliders as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!App\Helper::image($item->image)!!}</td>
                            <td><a href="{{route('admin.sliders.edit',$item->id)}}">{{ $item->title}}</a></td>
                            <td>{{ $item->subtitle }}</td>
                            <td>{!!App\Helper::sw($item,'active','Slider')!!}</td>
                            <td>
                                <a href="{{ url('admin/sliders/' . $item->id) }}" class="btn btn-success btn-sm" title="View Slider"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('admin/sliders/' . $item->id . '/edit') }}" class="btn btn-primary btn-sm" title="تعديل الاسليدر"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/sliders', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Slider" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete Slider',
                                            'onclick'=>'return confirm("حذف السليدر: هل أنت متأكد؟")'
                                    )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!!Form::close()!!}
            </div>
        </div>        
      </div>
    </section>
@endsection