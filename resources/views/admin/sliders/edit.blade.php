@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>سليدر الرئيسية</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/sliders')}}">سليدر الرئيسية</a></li>
        <li><a href="{{url('admin/sliders/create')}}">تعديل سليدر</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">تعديل سليدر {{ $slider->id }}</h3>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            {!! Form::model($slider, [
                'method' => 'PATCH',
                'url' => ['/admin/sliders', $slider->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'العنوان', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('subtitle') ? 'has-error' : ''}}">
                {!! Form::label('subtitle', 'نص اسفل العنوان', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('subtitle', null, ['class' => 'form-control', 'rows'=>3]) !!}
                    {!! $errors->first('subtitle', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'الصورة EN', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    {!!App\Helper::size(1915,1230)!!}
                </div>
                <div class="col-md-2">{!!App\Helper::image($slider->image)!!}</div>
            </div>
            <div class="form-group {{ $errors->has('image_ar') ? 'has-error' : ''}}">
                {!! Form::label('image_ar', 'الصورة AR', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-4">
                    {!! Form::file('image_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image_ar', '<p class="help-block">:message</p>') !!}
                    {!!App\Helper::size(1915,1230)!!}
                </div>
                <div class="col-md-2">{!!App\Helper::image($slider->image_ar)!!}</div>
            </div>
            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'تفعيل الظهور', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <div class="checkbox">
                        <label>{!! Form::radio('active', '1', null) !!} نعم</label>
                        <label>{!! Form::radio('active', '0', null) !!} لا</label>
                    </div>
                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    {!! Form::submit('حفظ التعديلات', ['class' => 'btn btn-primary']) !!}
                    <a href="{!!url('admin/sliders')!!}" class="btn btn-default">إلغاء</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection