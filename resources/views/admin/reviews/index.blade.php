<?php
    use App\Review;
    use App\Helper;
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">التقييمات</div>
        <div class="panel-body">
            <br/>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>تقييم منتج خاص بالحساب</th>
                        <th>الحالة</th>
                        <th>عنوان التقييم</th>
                        <th>التقييم</th>
                        <th>منذ</th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reviews as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{!!App\User::user($item)!!}</td>
                        <td>{!!Helper::statusCtrl(Review::$status,$item,'active','Review')!!}</td>
                        <td>{{ $item->subject }}</td>
                        <td>{{ $item->body }}</td>
                        <td>{!! Helper::since($item) !!}</td>
                        <td>
                            <a href="{{ url('admin/reviews/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('admin/reviews/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/reviews', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o"></i>', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection