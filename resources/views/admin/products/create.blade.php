@extends('admin.layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Create New Product</div>
                <div class="panel-body">
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::open(['url' => '/dashboard/products', 'class' => 'form-horizontal', 'files' => true]) !!}
                    @include ('dashboard.products.form')
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection