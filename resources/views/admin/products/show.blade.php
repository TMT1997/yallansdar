<?php
    use App\Product;
    use App\Helper;
    use App\User;
    use App\Category;
    use App\Company;
    use App\Country;
    $country = Country::whereId($product->manufactured_in)->first();
    if(isset($product->more_details)) {
        $product->more_details = unserialize($product->more_details);
    }
    $parents = array_reverse($category_parents);
    $profile = (isset($product->user->profile)) ? $product->user->profile : null;
    $payment_types = ($product->payment_type) ? explode(',', $product->payment_type):'';

?>
<div class="white-popup-block" style="min-width: 1000px;">
    <div class="row">
    <div class="col-md-4"><a href="{{Product::url($product)}}" class="btn btn-sm btn-info pull-left" target="_blank">View on Website</a></div>

    <div class="col-md-4">
        <a>
        {!!Helper::statusCtrl(Product::$product_status,$product,'approved','Product')!!}
        </a>
    </div>
    <div class="col-md-4">
        @if($profile)
            <a href="{{url('admin/profiles/profile/'.$profile->user_id)}}" class="btn btn-sm btn-info pull-left popup" >
                    {{$profile->first_name}}
            </a>
        @endif
        <script>
            $(document).ready(function() {
              $('.popup').magnificPopup({type:'ajax'});
            });
        </script>
    </div>
    </div>
    <ul class="breadcrumb">
        @if(!empty($parents))
            @foreach($parents as $cat)
                <li><a href="{{Category::url($cat)}}">{{@$cat->$c_name}}</a></li>
            @endforeach
        @endif
    </ul>

    <legend class="text-danger">@lang('products.basic_prd_info')</legend>
    <div class="well well-sm">
        <div class="row"> 
        @foreach(App\Language::$langs as $iso=>$lang)
            <?php $lang_name = $lang['name']; ?>
            <div class="col-md-6">
                <div class="well well-sm">
                    {!! Form::label('name_'.$iso,trans('products.prod_name').' ('.$lang_name.')', ['class'=>'control-label text-primary']) !!}
                    <div>
                        <?php $field = 'name_'.$iso; ?>
                        {!!$product->$field!!}
                    </div>
                </div>
                <div class="well well-sm">
                    {!! Form::label('description_'.$iso, trans('products.prod_desc').' ('.$lang_name.')', ['class'=>'control-label text-primary']) !!}
                    <div>
                        <?php $field = 'description_'.$iso; ?>
                        {!!$product->$field!!}
                    </div>
                </div>
                <div class="well well-sm">
                    {!! Form::label('details_'.$iso, trans('products.detailed_desc').' ('.$lang_name.')', ['class'=>'control-label text-primary']) !!}
                    <div>
                        <?php $field = 'details_'.$iso; ?>
                        {!!$product->$field!!}
                    </div>
                </div>
                <div class="well well-sm">
                    {!! Form::label('keywords_'.$iso, trans('products.prod_keywords').' ('.$lang_name.')', ['class'=>'control-label text-primary']) !!}
                    <div>
                        <?php $field = 'keywords_'.$iso; ?>
                        {!!$product->$field!!}
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            {!! Form::label('image', trans('products.prod_img'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                {!!Helper::image($product->image)!!}
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('group_id', trans('products.prod_group'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8 form-inline">
                @if(isset($product->group) && !empty($product->group))
                    {{$product->group->name}}
                @endif
            </div>
        </div>
    </div>

    <legend class="text-danger">@lang('products.prod_specifications')</legend>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('manufactured_in', trans('products.prod_manuf_in'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                @if($country)
                    {{$country->country_name}}
                @endif
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('reference_code', trans('products.ref_code'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                {{$product->reference_code}}
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('more_details', trans('products.more_details'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9 form-inline">
                @if($product->more_details)
                    @foreach($product->more_details['attr'] as $index=>$attr)
                    <div class="margbtn10">
                        {!!$attr!!}
                        {!!$product->more_details['value'][$index]!!}
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            {!! Form::label('add_images',trans('products.additional_images'),['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8 additional_imgs">
                @foreach($product->images as $pimg)
                    <div class="col-md-3">
                        {!!Helper::image($pimg->image, ['style'=>'height:120px;', 'class'=>'thumbnail'])!!}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('brochure', trans('products.brochure'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                @if($product->brochure)
                    <a href="{{url('uploads/'.$product->brochure)}}" target="_blank">Brochure File</a>
                @endif
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('video_url', trans('products.prod_video'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                @if($product->video_url)
                    <a href="{{$product->video_url}}" target="_blank">Open Video ({{$product->video_url}})</a>
                @endif
            </div>
        </div>
    </div>

    <legend class="text-danger">@lang('products.prod_trade_details')</legend>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('production_capacity', trans('products.capacity'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                {{$product->production_capacity}}
                {{trans('dash.units')[$product->capacity_unit]}}
                @lang('products.per') {{trans('dash.capacity_durations')[$product->capacity_unit_per]}}
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <div class="row">
            {!! Form::label('min_quantity', trans('products.min_quantity'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                {{$product->min_quantity}} {{trans('dash.units')[$product->min_quantity_unit]}}    
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('fob_price', trans('products.fob_price'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                {{$product->fob_price_type}}
                @lang('dash.from')
                {{$product->fob_price_from}}
                @lang('dash.to')
                {{$product->fob_price}}
                {{trans('products.per')}}
                @if(in_array($product->fob_price_unit , trans('dash.units')))
                    {{trans('dash.units')[$product->fob_price_unit]}}
                @else
                <p></p>
                @endif
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('fob_price', trans('products.payment_type'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                @if(!empty($payment_types))
                    @foreach($payment_types as $type)
                        @if(isset(App\ProductDetails::$payment_methods[$type]))
                            <a class="btn btn-default">{{App\ProductDetails::$payment_methods[$type]}}</a>
                        @else
                            {{$type}}
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('port', trans('products.port'), ['class' => 'col-md-3 control-label text-primary']) !!}
            <div class="col-md-8">
                @if(isset(trans('dash.shipping_ports')[$product->port]))
                    {{trans('dash.shipping_ports')[$product->port]}}
                @else
                    {{$product->port}}
                @endif
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row">
            {!! Form::label('delivery_time', trans('products.delivery_time'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                {{$product->delivery_time}} {{trans('dash.delivery_duration')[$product->delivery_duration]}}
            </div>
        </div>
    </div>
    <div class="well well-sm">
        <div class="row"> 
            {!!Form::label('packaging',trans('products.pack_details'), ['class'=>'col-md-3 control-label text-primary']) !!}
            <div class="col-md-9">
                <div class="row"> 
                @foreach(App\Language::$langs as $iso=>$lang)
                    <div class="col-md-6">
                        {!! Form::label('packaging_'.$iso, '('.$lang['name'].')', ['class'=>'control-label text-primary']) !!}
                        <div>
                            <?php $field = 'packaging_'.$iso; ?>
                            {!!$product->$field!!}
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>