<?php
    use App\Product;
    use App\Helper;
    use App\Category;
    $parents = Category::whereParent_category_id(0)->orderBy('name_ar')->get();
    $parents['']=''
?>
@extends('admin.layouts.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">المنتجات</div>
    <div class="panel-body">
        <div class="well well-sm">
            {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                <div class="form-group">
                    {!!Form::select('status', Product::$product_status, Request::get('status'), ['class'=>'form-control'])!!}
                </div>
                <div class="input-group">
                        {!! Form::text('from_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.from')]) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="input-group">
                    {!! Form::text('end_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.to')]) !!}
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            {!!Form::close()!!}
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 report_box_all_members count_table">
            عدد النتائج: <p class="count_number">{{$products->total()}}</p>
            </div>
        </div>
        <br />
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>رقم</th>
                    <th>الحساب</th>
                    <th>إظهار</th>
                    <th>الحالة</th>
                    <th>الاسم</th>
                    <th>الصورة</th>
                    <th>تاريخ الإضافة</th>
                    <th>مهام</th>
                </tr>
            </thead>
            <tbody>
            @foreach($products as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{!!App\User::user($item)!!}</td>
                    <td>{!!Helper::sw($item,'active','Product')!!}</td>
                    <td>{!!Helper::statusCtrl(Product::$product_status,$item,'approved','Product')!!}</td>
                    <td><a href="{{url('admin/products/'.$item->id)}}" class="popup">{{Helper::field($item,'name')}}</a></td>
                    <td>{!!Helper::image($item->image,['style'=>'width:70px'])!!}</td>
                    <td>{!!Helper::since($item)!!}</td>
                    <td nowrap="">
                        <a href="{{url('admin/products/'.$item->id)}}" class="btn btn-success btn-sm popup"><i class="fa fa-eye"></i> View</a>
                        {!! Form::open([
                            'method'=>'DELETE', 'url'=>['admin/products', $item->id], 'style'=>'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="fa fa-trash-o" title="Delete" />', array(
                            'type' => 'submit', 'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete', 'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="paging">
            {!!$products->links()!!}
        </div>

    </div>
</div>
@endsection