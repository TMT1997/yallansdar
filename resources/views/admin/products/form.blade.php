<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'اسم المنتج', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{!!App\Helper::image_field('photo', @$product)!!}
<div class="form-group {{ $errors->has('brief') ? 'has-error' : ''}}">
    {!! Form::label('brief', 'التفاصيل', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::textarea('brief', null, ['class' => 'form-control', 'id'=>'editor']) !!}
        {!! $errors->first('brief', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'إظهار', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox"><label>{!! Form::radio('active', '1') !!} Yes</label></div>
        <div class="checkbox"><label>{!! Form::radio('active', '0', true) !!} No</label></div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'إضافة', ['class' => 'btn btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">العودة</a>
    </div>
</div>