<div class="row">
@foreach(App\Language::$langs as $iso=>$lang)
    <div class="col-md-6">
        <div class="form-group {{$errors->has('name_'.$iso)?'has-error':''}}">
            {!! Form::label('name_'.$iso, 'الاسم ('.$lang['name'].')', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('name_'.$iso, null, ['class'=>'form-control']) !!}
                {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{$errors->has('details_'.$iso)?'has-error':''}}">
            {!! Form::label('details_'.$iso, 'نبذة ('.$lang['name'].')', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso]) !!}
                {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
@endforeach
</div>

<div class="image">
    {!!App\Helper::image_field('image', @$article)!!}
</div>

<div class="form-group {{$errors->has('active')?'has-error':''}}">
    {!! Form::label('active', 'إظهار', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1', null) !!} نعم</label>
            <label>{!! Form::radio('active', '0', null) !!} لا</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'اضافة', ['class' => 'btn btn-primary']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">العودة</a>
    </div>
</div>