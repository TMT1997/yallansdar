@extends('admin.layouts.master')
@section('title') المدونة | الكتاب | @endsection
@section('content')
    <section class="content-header">
      <h1>تعديل كاتب: <strong>{{ $article->title_ar }}</strong></h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
            <li><a href="{{url('admin/blog')}}">المدونة</a></li>
            <li><a href="{{url('admin/blog_authors')}}">كتاب المقالات</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border"></div>
            <div class="box-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($article, [
                    'method' => 'PATCH',
                    'url' => ['admin/blog_authors', $article->id],
                    'class' => 'form-horizontal article_form',
                    'files' => true
                ]) !!}
                    @include ('admin.blog_authors.form', ['submitButtonText' => 'حفظ التعديلات'])
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
    </script>
@endsection