<?php
    use App\Helper;
?>
@extends('admin.layouts.master')
@section('title') المدونة | الكتاب | @endsection
@section('content')
    <section class="content-header">
        <h1>كتاب المقالات بالمدونة
        <a href="{{ url('admin/blog_authors/create') }}" class="btn btn-success pull-left">+ اضف جديد</a>
        </h1>
    </section>

    <section class="content">
        <div class="box">                
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>إظهار</th>
                            <th>الصورة</th>
                            <th>الاسم</th>
                            <th>نبذة</th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($blog_authors as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!Helper::sw($item,'active','BlogAuthor')!!}</td>
                            <td>{!!Helper::image($item->image,['width'=>100])!!}</td>
                            <td><a href="{{url('admin/blog_authors/'.$item->id.'/edit')}}"><strong>{{Helper::field($item,'name')}}</strong></a></td>
                            <td>{{ str_limit(strip_tags(Helper::field($item,'details')), 150) }}</td>
                            <td nowrap="">
                                <a href="{{ url('admin/blog_authors/'.$item->id) }}" class="btn btn-success btn-xs" title="View"><span class="fa fa-eye"/></a>
                                <a href="{{url('admin/blog_authors/' .$item->id.'/edit')}}" class="btn btn-primary btn-xs" title="Edit"><span class="fa fa-pencil" /></a>
                                {!! Form::open([
                                    'method'=>'DELETE', 'url'=>['admin/blog_authors', $item->id], 'style'=>'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="fa fa-trash-o" title="Delete record" />', array(
                                    'type' => 'submit', 'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete', 'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection