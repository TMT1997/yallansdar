<?php
    use App\Helper; 
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">تصميمات المتاجر</div>
        <div class="panel-body">
            <a href="{{ url('admin/templates/create') }}" class="btn btn-success btn-sm" title="Add New Template">
                <i class="fa fa-plus" aria-hidden="true"></i> اضافة قالب جديد
            </a>

            {!! Form::open(['method' => 'GET', 'url' => 'admin/templates', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            {!! Form::close() !!}

            <br/>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Active</th>
                        <th>Preview</th>
                        <th>Skin</th>
                        <th>Is Free</th>
                        <th>Type</th>
                        <th>Category Id</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($templates as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{!!Helper::sw($item,'active','Template')!!}</td>
                        <td>{!!App\Helper::image($item->image,['style'=>'width:100px'])!!}</td>
                        <td>{{@App\Template::$skins[$item->skin]}}</td>
                        <td>{!!Helper::sw($item,'is_free','Template')!!}</td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->category_id }}</td>
                        <td>
                            <a href="{{ url('admin/templates/' . $item->id) }}" title="View Template"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('admin/templates/' . $item->id . '/edit') }}" title="Edit Template"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/templates', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Template',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper">
                {!! $templates->appends(['search' => Request::get('search')])->render() !!}
            </div>
        </div>
    </div>
@endsection
