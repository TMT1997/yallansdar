@extends('admin.layouts.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Edit Template #{{ $template->id }}</div>
    <div class="panel-body">
        <a href="{{ url('/admin/templates') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <br />
        <br />

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::model($template, [
            'method' => 'PATCH',
            'url' => ['/admin/templates', $template->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
        @include ('admin.templates.form', ['submitButtonText' => 'حفظ'])
        {!! Form::close() !!}

    </div>
</div>
@endsection
