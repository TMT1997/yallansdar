<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'اسم المجلد', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('name', App\Template::$folders, null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('skin') ? 'has-error' : ''}}">
    {!! Form::label('skin', 'اختر التصميم', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('skin', App\Template::$skins, null, ['class' => 'form-control']) !!}
        {!! $errors->first('skin', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'الصورة', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-4">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-2">
        @if(isset($template))
            {!!App\Helper::image($template->image, ['class'=>'img-responsive'])!!}
        @endif</div>
</div>
<div class="form-group {{ $errors->has('is_free') ? 'has-error' : ''}}">
    {!! Form::label('is_free', 'قالب مجاني', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('is_free', '1') !!} نعم</label>
            <label>{!! Form::radio('is_free', '0', true) !!} لا</label>
        </div>
        {!! $errors->first('is_free', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'المجال', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('type', null, ['class' => 'form-control']) !!}
        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
    {!! Form::label('category_id', 'التصنيف', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('color') ? 'has-error' : ''}}">
    {!! Form::label('color', 'اللون', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('color', null, ['class' => 'form-control']) !!}
        {!! $errors->first('color', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'اضف القالب', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
