<?php use App\Helper; 
$my_website = \App\UserWebsite::whereUser_id(\Auth::id())->first();

?>
@extends('admin.layouts.master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">عرض بيانات العضو: {{ $user->name }}</div>
    <div class="panel-body">
        <div class="well">
            <legend>البيانات الشخصية</legend>
            <table class="table table-bordered table-hover table-striped">
                <tbody>
                    <tr><td width="200">ID.</td><td>{{ $user->id }}</td></tr>
                    <tr><td>Name</td><td>{{ $user->name }}</td></tr>
                    <tr><td>Email</td><td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td></tr>
                    <tr><td>Mobile</td><td>{{ $user->mobile }}</td></tr>
                    <tr><td>Address</td><td>{{ $user->address }}</td></tr>
                    <tr><td>Company Name</td><td>{{ $user->company_name }}</td></tr>
                    @if($my_website)
                        @if($my_website->domain)
                        <tr><td>Store</td><td><a class="btn btn-info" href="{{url('store/'.$my_website->domain)}}" target="_blank">{{ $my_website->domain }}</a></td></tr>
                        @endif
                    @endif
                    <tr><td>Active</td><td>{!!Helper::sw($user,'active', 'User')!!}</td></tr>
                    <tr><td>Email Verified</td><td>{!!Helper::sw($user,'verified', 'User')!!}</td></tr>
                    <tr><td>Is Admin</td><td>{!!Helper::sw($user,'admin', 'User')!!}</td></tr>
                </tbody>
            </table>
        </div>

        <div class="well">
            <legend> معلومات اكثر</legend>
        <a style="text-align: center;" href="{{ url('admin/profiles/reportproduct/'.$user->id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-shopping-cart"/> More Data</a>
        </div>

        <div class="well">
            <legend>بيانات الباقة</legend>
            <table class="table table-bordered table-hover table-striped">
                <tbody>
                    <tr><td width="200">الباقة الحالية</td><td>{!!$user->package!!}</td></tr>
                </tbody>
            </table>
        </div>
        
        <div class="well">
            <legend>منتجات العضو</legend>
            <div class="alert alert-warning">القسم تحت الإنشاء</div>
        </div>

        <div class="well">
            <legend>صفقات العضو</legend>
            <div class="alert alert-warning">القسم تحت الإنشاء</div>
        </div>
    </div>
</div>
@endsection