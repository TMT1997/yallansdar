<div class="white-popup-block">
	@if($user)
		<legend>{{$user->name}}</legend>
		<table class="table table-striped table-hover">
			<tr>
				<td>Email</td>
				<td>{{$user->email}}</td>
			</tr>
			<tr>
				<td>Primary Email</td>
				<td>{{$user->profile->primary_email}}</td>
			</tr>
			<tr>
				<td>Alternative Email</td>
				<td>{{$user->profile->alt_email}}</td>
			</tr>
			<tr>
				<td>Company Name</td>
				<td>{{$user->profile->company_name}}</td>
			</tr>
			<tr>
				<td>Business Type</td>
				<td>{{$user->profile->business_type}}</td>
			</tr>
			<tr>
				<td>Main Products</td>
				<td>{{$user->profile->main_products}}</td>
			</tr>
			<tr>
				<td>Website</td>
				<td>{{$user->profile->website}}</td>
			</tr>
		</table>

		<div class="row">
			@foreach(App\UserProfile::$social as $field=>$label)
				<div class="col-md-2">
					<i class="fa fa-{{$field}}"></i>
					<a href="{{$user->profile->$field}}" target="_blank">{{$label}}</a>
				</div>
			@endforeach
		</div>	

		<div class="row">
			<div class="col-md-12 text-center">
			<a href="{{url('admin/profiles/login/'.$user->id)}}" class="btn btn-warning btn-xs" title="قم بالتسجيل بهذا المستخدم">تسجيل الدخول</a>
			</div>
		</div>
	@else
		<div class="alert alert-danger">User Account not found</div>
	@endif
</div>