<?php
    use App\Helper;
    use App\User;
    use App\Category;
    $usertypes = array_prepend(User::$types, 'All Types', '');
    $packages = array_prepend($packages->lists('name_'.App::getLocale(), 'id')->toArray(), 'كل الباقات', '');
    $reg_period = [
        ''=>'كل الأوقات',
        'today'=>'اليوم فقط',
        'yesterday'=>'منذ أمس حتى الأن',
        '1week'=>'منذ أسبوع حتى الأن',
        '1month'=>'منذ شهر حتى الأن',
    ];
    $email_verify_arr = [
        ''=>'Verified & Not Verified',
        0=>'Not Email Verified',
        1=>'Email Verified'
    ];
    $pages = [5=>5,7=>7,10=>10,20=>20,50=>50,100=>100];
    $parents = Category::whereParent_category_id(0)->orderBy('name_ar')->get();
    $parents['']='';
?>
@extends('admin.layouts.master')
@section('title') الأعضاء @endsection
@section('content')
<div class="panel panel-default">
    @if(Request::get('type'))
    <div class="panel-heading">الأعضاء ({{str_plural(Request::get('type'))}})</div>
    @endif
    <div class="panel-body">
        <div class="well well-sm">
            <div class="row">
                {!! Form::open(['method'=>'get','class'=>'form-inline form-filter']) !!}
                    {!!Form::hidden('type', Request::get('type'))!!}
                    <div class="col-md-8">
                    {!!Form::select('package_id', $packages, Request::get('package_id'),['class'=>'form-control input-sm'])!!}
                    {!!Form::select('period',$reg_period, Request::get('period'),['class'=>'form-control input-sm'])!!}
                    <div class="input-group">
                        {!! Form::text('from_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.from')]) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <div class="input-group">
                        {!! Form::text('end_date',null,['class'=>'form-control datepicker input-sm','placeholder'=>trans('dash.to')]) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    {!! Form::select('sector',$parents->lists('name_ar','id'),'',['class'=>'form-control input-sm','required'=>'required']) !!}
                    {!!Form::select('type',$usertypes, Request::get('type'),['class'=>'form-control input-sm'])!!}
                    {!!Form::select('email_verified', $email_verify_arr, Request::get('email_verified'),['class'=>'form-control input-sm'])!!}
                    </div>
                    <div class="col-md-1">
                        {!!Form::select('no_results', $pages, Request::get('no_results'),['class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-3">
                        {!!Form::text('q', Request::get('q'), ['placeholder'=>'Search ...','class'=>'form-control', 'style'=>'max-width:100%'])!!}
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 report_box_all_members count_table">
                عدد النتائج: <p class="count_number">{{$users->total()}}</p>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email Verified</th>
                        <th>Active</th>
                        <th>Featured</th>
                        <th>Verified</th>
                        <th>Registered</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $item)
                    <tr>
                        @if($item->user_id)
                            <td><a href="{{ url('admin/profiles/login/'.$item->user_id) }}" class="btn btn-warning btn-xs" title="قم بالتسجيل بهذا المستخدم">تسجيل الدخول</a></td>
                        @else
                            <td><a href="{{ url('admin/profiles/login/'.$item->id) }}" class="btn btn-warning btn-xs" title="قم بالتسجيل بهذا المستخدم">تسجيل الدخول</a></td>
                        @endif
                        @if($item->user_id)
                            <td><a href="{{ url('admin/users', $item->user_id) }}">{{ $item->name }}</a></td>
                        @else
                            <td><a href="{{ url('admin/users', $item->id) }}">{{ $item->name }}</a></td>
                        @endif
                        <td>{{ $item->email }}</td>
                        <td>{!!Helper::sw($item,'email_verified', 'User')!!}</td>
                        <td>{!!Helper::sw($item,'active', 'User')!!}</td>
                        <td>{!!Helper::sw($item,'featured', 'User')!!}</td>
                        <td>{!!Helper::sw($item,'verified', 'User')!!}</td>
                        <td>{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            @if($item->user_id)
                                <a href="{{ url('admin/profiles/profile/'.$item->user_id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-eye"/></a>
                                <a href="{{ url('admin/profiles/reportproduct/'.$item->user_id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-shopping-cart"/></a>
                            @else
                                <a href="{{ url('admin/profiles/profile/'.$item->id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-eye"/></a>
                                <a href="{{ url('admin/profiles/reportproduct/'.$item->id)}}" class="btn btn-success btn-xs popup"><span class="fa fa-shopping-cart"/></a>
                            @endif
                            @if($item->user_id)
                                {!! Form::open([
                                    'method'=>'DELETE', 'url'=>['admin/users', $item->user_id], 'style'=>'display:inline'
                                ]) !!}
                            @else
                                {!! Form::open([
                                    'method'=>'DELETE', 'url'=>['admin/users', $item->id], 'style'=>'display:inline'
                                ]) !!}
                            @endif
                            {!! Form::button('<span class="fa fa-trash" title="Delete" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete User',
                                    'onclick'=>'return confirm("حذف: هل أنت متأكد؟")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-10">
                <div class="pagination">
                    {!! $users->appends([
                        'type'=>Request::get('type'),
                        'q'=>Request::get('q'),
                        'no_results'=>Request::get('no_results',7),
                        'email_verified'=>Request::get('email_verified'),
                    ])->links() !!}
                </div>
            </div>
            <div class="col-md-2">
                عدد النتائج: {{$users->total()}}
            </div>
        </div>
    </div>
</div>
@endsection
