<?php
    use App\Helper;
?>

<div class="white-popup-block">
	<legend>@lang('front.trade_managers'): {{ $item->name }}</legend>
	<table class="table table-bordered table-striped table-hover">
		
	    <tr><td width="220">رقم</td><td>{{ $item->id }}</td></tr>
	    <tr><td>الاسم</td><td>{{ $item->name }}</td></tr>
	    <tr><td>المهنة</td><td>{{ $item->jobtitle }}</td></tr>
	    <tr><td>الشركة</td><td>{{ $item->company }}</td></tr>
	    <tr><td>التليفون</td><td>{{ $item->phone }}</td></tr>
	    <tr><td>الإيميل</td><td><a href="mailto:{{$item->email}}">{{$item->email}}</a></td></tr>
	    <tr><td>المحافظة</td><td>{{ $item->city }}</td></tr>
	    <tr><td>الدولة</td><td>{{ $item->country }}</td></tr>
	    <tr><td>المنتجات التي يرغب العمل بها</td><td>{{ $item->products }}</td></tr>
	    

	    <!-- <tr><td>featured</td><td>{!!Helper::sw($item,'featured','TradeManager')!!}</td></tr> -->
		
	</table>
</div>