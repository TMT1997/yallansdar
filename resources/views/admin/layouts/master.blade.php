<?php
        $unread_notification = \App\Notification::OrderBy('id','DESC')->limit(100)->get();
        $unread_count = \App\Notification::whereRead(0)->count();

        $unread_call_notification = \App\Messages\Message::whereSubject("من فضلك كلمني")->OrderBy('id','DESC')->limit(100)->get();
        $unread_call_count = \App\Messages\Message::whereSubject("من فضلك كلمني")->whereRead(0)->count();
        $count_new_members = \App\Notification::whereSection("Add_User")->whereRead(0)->count();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title') لوحة التحكم</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {!!HTML::style('backend/bootstrap/css/bootstrap.min.css')!!}
  {{HTML::style('frontend/css/jquery-ui.min.css')}}
  {!!HTML::style('frontend/css/font-awesome.min.css')!!}
  {!!HTML::style('backend/plugins/datatables/dataTables.bootstrap.css')!!}
  {!!HTML::style('backend/dist/css/AdminLTE.min.css')!!}
  {!!HTML::style('backend/dist/css/skins/_all-skins.min.css')!!}
  {!!HTML::style('backend/plugins/select2/select2.min.css')!!}
  {!!HTML::style('backend/dist/css/custom.css')!!}
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  @if(App::isLocale('ar'))
      {!!HTML::style('backend/dist/css/bootstrap-rtl.min.css')!!}
  @endif
  @if(App::isLocale('en'))
    <style type="text/css">
      .content-header>.breadcrumb {right:10px;left: initial;}
      .main-sidebar, .left-side {left:0; right: initial;}
      .content-wrapper, .right-side, .main-footer {margin-left: 230px; margin-right: 0}
      .main-header .logo {float: left;}
      .main-header>.navbar {margin-right: 0; margin-left: 230px}
      .main-header .sidebar-toggle {float: left;}
      .main-header .navbar-custom-menu, .main-header .navbar-right {float: right;}
    </style>
  @endif
  <style type="text/css">
    select.form-control {padding: 3px 10px;}
    .report_box_all_members
    {
      border: 1px solid #e7e7e7;
      padding: 15px;
      margin-left:2px;
      margin-top:2px;
      margin-bottom:2px;
      border-top: 2px solid #f79434;
      background-color: white;
      min-width: 200px;
    }
    .report_box
    {
      border: 1px solid #e7e7e7;
      padding: 15px;
      margin-left:2px;
      margin-top:2px;
      margin-bottom:2px;
      border-top: 2px solid #00c0ef;
      background-color: white;
      min-width: 200px;
    }
    .report_box_image_all_members
    {
      float: left;
      font-size: 50px;
      color: #f79434;
    }
    .report_box_image
    {
      float: left;
      font-size: 50px;
      color: #00c0ef;
    }
    .report_text_count
    {
      font-size: 30px;
      font-weight: bold;
    }
    .report_text_label
    {
      font-size: 15px;
      font-weight: bold;
    }
    .count_table
    {
      text-align: center;
      font-size: 20px;
      padding: 0px;margin-bottom:0px;
    }
    .count_number
    {
      color:#f79434;
      font-weight: bold;
    }
    @if(App::isLocale('en'))
    .navbar-custom-menu>.navbar-nav>li>.dropdown-menu { left: auto; right: 0; }
    .navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a>h4>small { left: auto; right:0; }
    .navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a>p  { margin: 0 45px 0 0; }
    @endif
  </style>
  <script type="text/javascript">
    var csrf_token = '{{csrf_token()}}';
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="{{url('admin')}}" class="logo"><span class="logo-mini"><b>Y</b>N</span> <span class="logo-lg">{{trans('admin.adminpanel')}}</span></a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="{{url('/')}}" target="_blank">{{trans('admin.goto_website')}}</a></li>
          <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                  @foreach ($languages as $lang)
                      <?php $sel = (Session::get('yalla_locale')==$lang->iso2) ? 'selected="selected"':''; ?>
                      <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                  @endforeach
              </ul>
          </li>
          {{--NOTIFICATIONS--}}
          <li class="dropdown messages-menu notfication_menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bell"></i>
              <span class="label label-success">{{$unread_count}}</span>
            </a>
            <ul class="dropdown-menu" >
              <li class="header" >لديك {{$unread_count}} تنبيهات</li>
              <li>
                <ul class="menu" >
                  @foreach($unread_notification as $msg)
                  <li>
                      @if(\App\Notification::$NOTIFICATION_EVENTS[$msg->section]['route'])
                        <a href="{{url(\App\Notification::$NOTIFICATION_EVENTS[$msg->section]['route'].$msg->section_id)}}" style="white-space: unset" class="popup">
                      @else
                        <a style="white-space: unset">
                      @endif
                      @if(App::isLocale('ar'))
                      <h4 style="margin:unset;color:unset;">{{\App\Notification::$NOTIFICATION_EVENTS[$msg->section]['value_ar']}}
                        <small><i class="fa fa-clock-o"></i>{{Carbon\Carbon::parse($msg->created_at)->diffForHumans()}}</small>
                      </h4>
                      @else
                      <h4 style="margin:unset;color:unset;">{{\App\Notification::$NOTIFICATION_EVENTS[$msg->section]['value_en']}}
                        <small><i class="fa fa-clock-o"></i>{{Carbon\Carbon::parse($msg->created_at)->diffForHumans()}}</small>
                      </h4>
                      @endif
                      <p>{{$msg->message}}</p>
                    </a>
                  </li>
                  @endforeach
                </ul>
              </li>
              <li class="footer"><a href="{{url('admin/notification/all')}}">عرض كل التنبيهات</a></li>
            </ul>
          </li>
          {{--END NOTIFICATIONS--}}
          {{--Call Notification--}}
          <li class="dropdown messages-menu notfication_call_menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-phone"></i>
              <span class="label label-success">{{$unread_call_count}}</span>
            </a>
            <ul class="dropdown-menu" >
              <li class="header" >لديك {{$unread_call_count}} تنبيهات</li>
              <li>
                <ul class="menu" >
                  @foreach($unread_call_notification as $msg)
                  <li>
                    <a>
                      @if(App::isLocale('ar'))
                      <h4 style="margin:unset;color:unset;">{{$msg->subject}}
                        <small><i class="fa fa-clock-o"></i>{{Carbon\Carbon::parse($msg->created_at)->diffForHumans()}}</small>
                      </h4>
                      @else
                      <h4 style="margin:unset;color:unset;">{{$msg->subject}}
                        <small><i class="fa fa-clock-o"></i>{{Carbon\Carbon::parse($msg->created_at)->diffForHumans()}}</small>
                      </h4>
                      @endif
                      <p>{{$msg->body}}</p>
                    </a>
                  </li>
                  @endforeach
                </ul>
              </li>
              <li class="footer"><a></a></li>
            </ul>
          </li>
          {{--END Call Notification--}}
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope"></i>
              {{-- <span class="label label-success">{{@$common['messages']->count()}}</span> --}}
            </a>
            <ul class="dropdown-menu">
              {{-- <li class="header">لديك {{@$common['messages']->count()}} رسالة</li> --}}
              <li>
                <ul class="menu">
                {{-- @foreach(@$common['messages'] as $msg)
                  <li>
                    <a href="{{route('admin.messages.show', $msg->id)}}">
                      <h4>{{$msg->email}}
                        <small><i class="fa fa-clock-o"></i>{{Carbon\Carbon::parse($msg->created_at)->diffForHumans()}}</small>
                      </h4>
                      <p>{{str_limit($msg->message, 30)}}</p>
                    </a>
                  </li>
                  @endforeach --}}
                </ul>
              </li>
              <li class="footer"><a href="{{route('admin.messages.index')}}">عرض كل الرسائل</a></li>
            </ul>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {!!HTML::Image('backend/dist/img/user2-160x160.jpg','',['class'=>'user-image'])!!}
              <span class="hidden-xs">{{Auth::user()->name}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                {!!HTML::Image('backend/dist/img/user2-160x160.jpg','',['class'=>'img-circle'])!!}
                <p>{{Auth::user()->name}} <small>Member since {{Auth::user()->created_at}}</small></p>
              </li>
              <li class="user-footer">
                <div class="pull-left"><a href="{{route('admin.users.show', Auth::id())}}" class="btn btn-default btn-flat">البروفايل</a></div>
                <div class="pull-right"><a href="{{url('logout')}}" class="btn btn-default btn-flat">تسجيل خروج</a></div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel"></div>
      <ul class="sidebar-menu">
        
        @foreach(App\Menu::$admin_access_menu as $item=>$sept)
            @if(App\User::hasAcessSection($sept['menu']))
              <li class="treeview">
                @if($item == 2)
                  <a href="#"><span>{{trans($sept['label'])}}
                    @if($count_new_members >0)
                    <span class="label label-danger">{{$count_new_members}}</span>
                    @endif
                  </span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                  </a>
                @else
                  <a href="#"><span>{{trans($sept['label'])}}</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                  </a>
                @endif
              <ul class="treeview-menu">
            @endif

            @foreach($sept['menu'] as $url=>$page)
              @if(App\User::hasAccess($url))
                <li><a href="{{url($url)}}"><i class="fa fa-{{$page['icon']}}"></i> <span>{{trans($page['label'])}}</span> 
                  {{ (isset($page['user_counts'])) ? '('.$user_counts[$page['user_counts']].')':'' }}
                </a></li>
              @endif
            @endforeach

            @if(App\User::hasAcessSection($sept['menu']))
              </ul></li>
            @endif
        @endforeach

        {{-- <li class="treeview">
            <a href="#"><span>@lang('admin.services')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/business-developers')}}"><i class="fa fa-eye"></i> <span>{{trans('front.business_developers')}}</span> ({{$user_counts['business_developers']}})</a></li>
              <li><a href="{{url('admin/trade-managers')}}"><i class="fa fa-user"></i> <span>{{trans('front.trade_managers')}}</span> ({{$user_counts['trade_managers']}})</a></li>
              <li><a href="{{url('admin/pick-describes')}}"><i class="fa fa-user"></i> <span>{{trans('front.pick_describe')}}</span> ({{$user_counts['pick_describes']}})</a></li>
            </ul>
        </li>
        
         <li class="treeview">
            <a href="#"><span>@lang('admin.members')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/users')}}"><i class="fa fa-user"></i> <span>@lang('front.all_users')</span> ({{$user_counts['all']}})</a></li>
              @foreach(App\User::$types as $type=>$label)
                <li><a href="{{url('admin/users?type='.$type)}}"><i class="fa fa-file"></i> <span>@lang('front.'.$label)</span> ({{$user_counts[$type]}})</a></li>
              @endforeach
            </ul>
          </li>
        
        <li class="treeview">
            <a href="#"><span>@lang('admin.messages')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/messages')}}"><i class="fa fa-inbox"></i> <span>@lang('dash.inbox')</span></a></li>
              <li><a href="{{url('admin/compose')}}"><i class="fa fa-share-square"></i> <span>@lang('dash.sent')</span></a></li>
              <li><a href="{{url('admin/compose/bulk')}}"><i class="fa fa-share-square"></i> <span>@lang('dash.send_message')</span></a></li>
            </ul>
          </li>
        
        <li class="treeview">
            <a href="#"><span>@lang('admin.content')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/blog')}}"><i class="fa fa-bold"></i> <span>@lang('admin.blog')</span></a></li>
              <li><a href="{{url('admin/pages')}}"><i class="fa fa-file"></i> <span>@lang('admin.pages')</span></a></li>
              <li><a href="{{url('admin/packages')}}"><i class="fa fa-file"></i> <span>@lang('admin.packages')</span></a></li>
              <li><a href="{{url('admin/countries')}}"><i class="fa fa-flag-o"></i> <span>@lang('admin.countries')</span></a></li>
              <li><a href="{{url('admin/categories')}}"><i class="fa fa-file"></i> <span>@lang('admin.categories')</span></a></li>
              <li><a href="{{url('admin/blog_categories')}}"><i class="fa fa-bold"></i> <span>@lang('admin.blog_categories')</span></a></li>
              <li><a href="{{url('admin/advertisement')}}"><i class="fa fa-file"></i> <span>@lang('admin.my_advs')</span></a></li>
              <li><a href="{{url('admin/templates')}}"><i class="fa fa-tv"></i> <span>@lang('admin.templates')</span></a></li>
              <li><a href="{{url('admin/sliders')}}"><i class="fa fa-file"></i> <span>@lang('admin.sliders')</span></a></li>
            </ul>
          </li>
        
        <li class="treeview">
            <a href="#"><span>@lang('admin.activity_member')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/products')}}"><i class="fa fa-file"></i> <span>@lang('admin.products')</span></a></li>
              <li><a href="{{url('admin/deals')}}"><i class="fa fa-file"></i> <span>@lang('admin.deals')</span></a></li>
              <li><a href="{{url('admin/services')}}"><i class="fa fa-file"></i> <span>@lang('admin.services')</span></a></li>
              <li><a href="{{url('admin/userwebsites')}}"><i class="fa fa-globe"></i> <span>@lang('admin.userwebsites')</span></a></li>
              <li><a href="{{url('admin/package_upgrades')}}"><i class="fa fa-smile-o"></i> <span>@lang('dash.upgrade')</span></a></li>
              <li><a href="{{url('admin/reviews')}}"><i class="fa fa-smile-o"></i> <span>@lang('admin.reviews')</span></a></li>
              <li><a href="{{url('admin/testimontials')}}"><i class="fa fa-comment-o"></i> <span>@lang('admin.testimontials')</span></a></li>
              <li><a href="{{url('admin/profiles')}}"><i class="fa fa-user"></i> <span>@lang('dash.profile')</span></a></li>
            </ul>
          </li>
        
        <li class="treeview">
            <a href="#"><span>@lang('admin.settings')</span>
              <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('admin/admins')}}"><i class="fa fa-user"></i> <span>@lang('admin.admins')</span></a></li>
              <li><a href="{{url('admin/settings')}}"><i class="fa fa-cog"></i> <span>@lang('admin.settings')</span></a></li>
            </ul>
          </li> --}}
        
      </ul>
    </section>
  </aside>
  <div class="content-wrapper">
    <div class="col-md-12">
      @if(Session::has('flash_message'))
          <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!!Session::get('flash_message')!!}
          </div>
      @endif
    </div>
    <div style="clear:both">@yield('report_admin')</div>
    <div style="clear:both">@yield('content')</div>
  </div>
  <footer class="main-footer"><div class="pull-right hidden-xs"><b>YallaNsadar Admin - Version</b> 1.0.0</div></footer>
  <aside class="control-sidebar control-sidebar-dark">
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs"></ul>
    <div class="tab-content">
      <div class="tab-pane" id="control-sidebar-home-tab"></div>
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
    </div>
  </aside>
  <div class="control-sidebar-bg"></div>
</div>
<div class="modal fade" id="modal_showApplication" role="dialog" aria-labelledby="showApplication" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header modal-header-danger">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">عرض تفاصيل اﻻستمارة</h3>
        </div>
        <div class="modal-body app_content"></div>
        <div class="modal-footer"></div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_statusReason" role="dialog" aria-labelledby="statusReason" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header modal-header-danger">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title">سبب رفض المنتج</h3>
        </div>
        <div class="modal-body model_reasons_content"></div>
        <div class="modal-footer"></div>
    </div>
  </div>
</div>
<div class="modal fade" id="ChangePassModal" tabindex="-1" role="dialog" aria-labelledby="myPSModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">تغيير كلمة المرور</h4>
      </div>
        {!! Form::open(['method'=>'POST','url'=>['admin/settings/changepassword'],'class'=>'form-horizontal']) !!}
        {!!Form::hidden('user_id',null,['id'=>'ch_user_id'])!!}
          <div class="modal-body">
            <div class="form-group">
                {!! Form::label('password', 'كلمة المرور الجديدة: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('password', null, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('password_confirm', 'تأكيد كلمة المرور الجديدة: ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('password_confirm', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
            <button type="submit" class="btn btn-primary">تغيير كلمة المرور</button>
          </div>
        {!! Form::close() !!}
    </div>
  </div>
</div>
{!!HTML::script('backend/plugins/jQuery/jquery-2.2.3.min.js')!!}
{!!HTML::script('frontend/js/jquery-ui.min.js')!!}
{!!HTML::script('backend/dist/js/chart.js')!!}
{!!HTML::script('backend/bootstrap/js/bootstrap.min.js')!!}
{!!HTML::script('backend/plugins/datatables/jquery.dataTables.min.js')!!}
{!!HTML::script('backend/plugins/datatables/dataTables.bootstrap.min.js')!!}
{!!HTML::script('backend/plugins/slimScroll/jquery.slimscroll.min.js')!!}
{!!HTML::script('backend/plugins/fastclick/fastclick.js')!!}
{!!HTML::script('backend/ckeditor/ckeditor.js')!!}
{!!HTML::script('backend/plugins/select2/select2.min.js')!!}
{!!HTML::script('backend/dist/js/app.min.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
@include('admin.layouts.popup')
@yield('script')
<script>
    $(document).ready(function() {
      $('.popup').magnificPopup({type:'ajax'});
    });
  </script>
<script type="text/javascript">
  $(function() {
        $(".datepicker").datepicker();
    });
  $(document).ready(function() {
    $('.select2').select2({});
    $('.btn-loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
    $('.type').on('change', function(event) {
        event.preventDefault();
        $type = $(this).val();
        if($type=='video') {
            $("#video").removeClass('hidden');
            $("#video").show();
            $("#image").hide();
        } else {
            $("#video").addClass('hidden');
            $("#video").hide();
            $("#image").show();
        }
    });
  });
  $('.page_form .url').on('keyup', function(event) {
      event.preventDefault();
      if($(this).val()!='') {
        $('.page_form .body').slideUp();
        $('.page_form .image').slideUp();
      } else {
        $('.page_form .body').slideDown();
        $('.page_form .image').slideDown();
      }
  });
  (function ($) {
    "use restrict";
        $(".btn_status").on('click', function(event) {
            var $record_id = $(this).data('id');
            var $model = $(this).data('model');
            var btnThis = $(this);
            if(window.confirm("هل تريد بالفعل تغيير حالة تفعيل هذا العنصر؟"))
            {
              event.preventDefault();
              $.ajax({
                  url: '{{url("admin/settings/change-status")}}/'+$record_id+'/'+$model,
                  type: 'GET',
                  beforeSend:function() {
                      btnThis.button('loading');
                  }
              })
              .done(function(data) {
                  result = $.parseJSON(data);
                  if(result.output=='no'){
                      btnThis.removeClass('btn-success');
                      btnThis.addClass('btn-warning');
                      btnThis.html('Archived');
                  } else {
                      btnThis.removeClass('btn-warning');
                      btnThis.addClass('btn-success');
                      btnThis.html('Published');
                  }
              })
              .fail(function() {
                  alert('عفوا حدث خطأ اثناء تغيير حالة العنصر');
              })
              .always(function() {
                  btnThis.button('reset');
              });
            }
        });

        $('.btn_changepass').on('click', function(event) {
            event.preventDefault();
            $("#ChangePassModal #ch_user_id").val($(this).data('id'));
        });
    })(jQuery);

    $('body').on('click', '.notfication_menu', function(event) {
      $.ajax({
          url: '{{url('admin/notification_messages')}}',
          type: 'GET',
        }).done(function(response) {
          
        }).fail(function() {
          
        }).always(function() {
        });  
    });
    $('body').on('click', '.notfication_call_menu', function(event) {
      $.ajax({
          url: '{{url('admin/notification_call_messages')}}',
          type: 'GET',
        }).done(function(response) {
          
        }).fail(function() {
          
        }).always(function() {
        });  
    });

    $('body').on('click', '.btn_boolean', function(event) {
        event.preventDefault();
        if(window.confirm('هل تريد بالفعل تغيير حالة تفعيل هذا العنصر؟'))
        {
          $id = $(this).data('id');
          $field = $(this).data('field');
          $model = $(this).data('model');
          $element = $('.elem'+$field+$id);
          $.ajax({
              url: '{{url('admin/settings/switch-boolean')}}',
              type: 'GET',
              data: {id: $id, model: $model, field: $field},
              beforeSend:function() {
                  $element.html('loading');
              }
          }).done(function(response) {
              $res = $.parseJSON(response);
              $elem_wrapper = $element.parent('td');
              $element.remove();
              if($res.status==1) {
                  if($res.newval==1) {
                      $color = 'success';
                      $icon = 'check-circle';
                  } else {
                      $color = 'danger';
                      $icon = 'minus-circle';
                  }

                  if($field=='trashed') {
                    $('#tr_'+$id).hide('slow');
                  }
                  $elem_wrapper.html('<a href="#" class="text-'+$color+' btn_boolean elem'+$field+$id+'" data-id="'+$id+'" data-model="'+$model+'" data-field="'+$field+'"><i class="fa fa-2x fa-'+$icon+'"></i></a>');
                  
              } else {
                  alert('خطا حدث في تغيير التفعيل');
              }
          }).fail(function() {
              console.log("خطا حدث في تغيير التفعيل");
          }).always(function() {
          });
        }
    });
    $(document).on('change', '.statusCtrl', function(event) {
        event.preventDefault();
        $id = $(this).data('id');
        $field = $(this).data('field');
        $model = $(this).data('model');
        $valToSet = $(this).val();
        $element = $('.stat_'+$field+$id);
        $result_elem = $(this).siblings('.statusResult');
        $.ajax({
          url: '{{url('admin/settings/status-change')}}',
          type: 'GET',
          data: {id: $id, model: $model, field: $field, value: $valToSet},
          beforeSend:function() {
              $result_elem.html('loading..');
          }
        }).done(function(response){
          $res = $.parseJSON(response);
          
          if($res.status==1) {
              $result_elem.html('<div class="label label-success">Done</div>');

              if($res.newval==3 || $res.newval==4)
              {
                $('#modal_statusReason').modal('show');
                $.ajax({
                  url: '{{url('admin/settings/product-reasons')}}',
                  type: 'GET',
                  dataType: 'html',
                  data: {id: $id},
                  beforeSend: function() {
                    $('.model_reasons_content').html('Loading ...');
                  }
                }).done(function(response) {
                  $('.model_reasons_content').html(response);
                }).fail(function() {
                  $('.model_reasons_content').html('Error occured');
                }).always(function() {
                });                
              }

          } else {
              $result_elem.html('<div class="label label-danger">Error</div>');
          }
        }).fail(function() {
          $result_elem.html('<div class="label label-danger">Exception Error</div>');
        }).always(function() {
        });
    });
    $(document).on('click', '.btn_set_reason', function(event) {
      event.preventDefault();
      var $btn = $(this);
      $.ajax({
        url: '{{url('admin/settings/user-product-reason')}}',
        type: 'POST',
        dataType: 'json',
        data: $('.reason_form').serialize(),
        beforeSend: function() {
          $btn.button('loading');
        }
      }).done(function(response) {
        if(response.status==1) {
          $('.model_reasons_content').append('<div class="col-md-12"><div class="alert alert-success">Done</div></div>');
        } else {
          $('.model_reasons_content').append('<div class="col-md-12"><div class="alert alert-danger">Error</div></div>');
        }
      }).fail(function(err) {
        $('.model_reasons_content').append('<div class="col-md-12"><div class="alert alert-danger">Error Occured</div></div>');
      }).always(function() {
        $btn.button('reset');
        setTimeout(function() {
          $('#modal_statusReason').modal('hide');
        }, 2000);
      });
    });
    $('.btn_loading').on('click', function () {
        var $btn = $(this).button('loading');
    });
    $('body').on('change', '.form-filter select', function(event) {
      event.preventDefault();
      $('.form-filter').submit();
    });
    $('body').on('change', '.form-filter input', function(event) {
      event.preventDefault();
      $('.form-filter').submit();
    });
    $(function () {
      $(".dataTable").DataTable({
        "language": {
          "sProcessing":   "جارٍ التحميل...",
          "sLengthMenu":   "أظهر _MENU_ مدخلات",
          "sZeroRecords":  "لم يعثر على أية سجلات",
          "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
          "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
          "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
          "sInfoPostFix":  "",
          "sSearch":       "ابحث:",
          "sUrl":          "",
          "oPaginate": {
              "sFirst":    "الأول",
              "sPrevious": "السابق",
              "sNext":     "التالي",
              "sLast":     "الأخير"
          }
        },
        "aaSorting": []
      });
      $('.dataTable2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "aaSorting": []
      });
    });
    $(function () {
      if($("#editor").length>0) {
          CKEDITOR.replace('editor',{
            toolbar : 'Full',
            filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
      if($("#editor2").length>0) {
        CKEDITOR.replace('editor2',{
          toolbar : 'Full',
          filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
      if($('#editor_ar').length) {
        CKEDITOR.replace('editor_ar',{
          toolbar : 'Full',
          filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
      if($('#editor_en').length) {
        CKEDITOR.replace('editor_en',{
          toolbar : 'Full',
          filebrowserUploadUrl: "{{url('admin/settings/upload')}}"
        });
      }
    });

    $(document).on('click', '.show_appModal', function (event) {
        event.preventDefault();
        var $id = $(this).data('id');
        var $type = $(this).data('type');
        //$('#modal_showApplication').modal('hide');
        var url = '{{url('admin/applications/show')}}/' + $id + '/'+$type;
        $.ajax({
            url: url,
            beforeSend: function () {
                $('.app_content').html('تحميل يرجى اﻻنتظار ...');
            }
        }).done(function (data) {
            $(".app_content").html(data);
            $('#modal_showApplication').modal('show');
        }).fail(function (err) {
            alert(err);
        }).always(function () {
        });
    });
</script>
  <script type="text/javascript">
      $(document).ready(function() {
          $(document).on('submit','.search_form', function(e) {
              e.preventDefault();
              var results = $('#results');
              var article_id = $('.search_form #article_id').val();
              $.ajax({
                  url: '{{url('admin/settings/search-article')}}',
                  type: 'POST',
                  data: $(this).serialize(),
              })
              .done(function(response) {
                  data = response;
                  $.each(data, function(index, val) {
                     $('.searchresults_table').append('<tr id="row_'+val.id+'"><td>'+val.id+'</td><td>'+val.title+'</td><td><a href="#" class="add_related" data-related_id="'+val.id+'" data-article_id="'+article_id+'" data-title="'+val.title+'">اضف</a></td></tr>');
                  });
                  results.html(response);
              })
              .fail(function() {
                  results.html('Error');
              })
              .always(function() {
                  console.log("complete");
              });
              
          });
      });
      $(document).on('click', '.add_related', function(event) {
        event.preventDefault();
        var article_id = $(this).data('article_id');
        var related_id = $(this).data('related_id');
        var title = $(this).data('title');
        $.ajax({
          url: '{{url('admin/settings/add-related-article')}}/'+article_id+'/'+related_id,
          type: 'GET',
        })
        .done(function(response) {
          var row_id = response.id;
          $('.relatedTable').append('<tr id="row_'+row_id+'"><td>'+related_id+'</td><td>'+title+'</td><td><a href="#" class="remove_related" data-relatedId="'+related_id+'" data-articleId="'+article_id+'">إزالة</a></td></tr>');
          $('.searchresults_table tr#row_'+related_id).remove();
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
        });
      });

      $(document).on('click', '.remove_related', function(event) {
        event.preventDefault();
        var id = $(this).data('id');
        if(window.confirm('حذف هذه المقالة من المواضيغ ذات الصلة؟'))
        {
          $.ajax({
            url: '{{url('admin/settings/remove-related-article')}}/'+id,
            type: 'GET',
          })
          .done(function(response) {
            alert(response);
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            $('.relatedTable tr#row_'+id).remove();
          });
        }        
      });

      $(document).on('click', '.ad_type_radio', function(event) {
        //event.preventDefault();
        $('#type_code').hide();
        $('#type_uploads').hide();
        if($(this).val()=='uploads') {
          $('#type_uploads').slideDown('400');
        } else {
          $('#type_code').slideDown('400');
        }
      });
      $(document).ready(function() {
        $('.ad_type_radio').trigger('click');
      });
  </script>
 
</body></html>