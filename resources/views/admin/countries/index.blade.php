@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>الدول</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/countries')}}">الدول</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><a href="{{ url('/admin/countries/create?tag='.Request::get('tag')) }}" class="btn btn-primary btn-sm" title="Add New Country"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> اضافة جديد</a></h3>
        </div>
        <div class="box-body">
            <div class="table">
                <table class="table table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>مفعل</th>
                            <th>العلم</th>
                            <th>الاسم بالعربية </th>
                            <th>الاسم English </th>
                            <th>الكود ISO </th>
                            <th>كود الاتصال </th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($countries as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!App\Helper::sw($item,'active','Country')!!}</td>
                            <td>{!!App\Country::flag($item)!!} </td>
                            <td><a href="{{route('admin.countries.edit',$item->id)}}">{{$item->country_name}}</a></td>
                            <td><a href="{{route('admin.countries.edit',$item->id)}}">{{$item->name_english}}</a></td>
                            <td>{{ $item->iso2 }}</td>
                            <td>{{ $item->calling_code }}</td>
                            <td nowrap="nowrap">
                                <a href="{{ url('/admin/countries/' . $item->id) }}" class="btn btn-success btn-xs" title="View Country"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('/admin/countries/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Country"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/countries', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Country" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Country',
                                        'onclick'=>'return confirm("حذف الدولة: هل أنت متأكد؟")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>        
      </div>
    </section>
@endsection