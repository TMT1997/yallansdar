@extends('admin.layouts.master')

@section('content')
    <section class="content-header">
      <h1>الدول</h1>
      <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
        <li><a href="{{url('admin/countries')}}">الدول</a></li>
        <li><a href="{{url('admin/countries/edit')}}">تعديل</a></li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">تعديل دولة: {{ $country->ountry_name }}</h3>
        </div>
        <div class="box-body">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($country, [
                'method' => 'PATCH',
                'url' => ['/admin/countries', $country->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}
            @include('admin.countries.form')            
            {!! Form::close() !!}
        </div>
    </div>
@endsection