<div class="form-group {{ $errors->has('country_name') ? 'has-error' : ''}}">
    {!! Form::label('country_name', 'الاسم بالعربية', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('country_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('country_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name_english') ? 'has-error' : ''}}">
    {!! Form::label('name_english', 'English Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('name_english', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name_english', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('calling_code') ? 'has-error' : ''}}">
    {!! Form::label('calling_code', 'كود الاتصال', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('calling_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('calling_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit('حفظ', ['class' => 'btn btn-primary']) !!}
        <a href="{!!url('admin/countries')!!}" class="btn btn-default">إلغاء</a>
    </div>
</div>