<?php
    use App\UserWebsite;
    use App\Helper;
    $status_form = array_prepend(UserWebsite::$status, 'All', 0);
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">متاجر الأعضاء</div>
        <div class="panel-body">
            <div class="well well-sm">
                {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                    <div class="form-group">
                        {!!Form::select('status', $status_form, Request::get('status'),['class'=>'form-control'])!!}
                    </div>
                {!!Form::close()!!}
            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>الحساب</th>
                        <th>اسم المتجر</th>
                        <th>القالب</th>
                        <th>الحالة</th>
                        <th>نشر</th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($userwebsites as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{!!App\User::user($item)!!}</td>
                        <td>{{ $item->domain }}</td>
                        <td>{{@$item->template->name}}</td>
                        <td>{!!Helper::statusCtrl(UserWebsite::$status,$item,'status','UserWebsite')!!}</td>
                        <td>{!!Helper::sw($item,'active','UserWebsite')!!}</td>
                        <td>
                            @if($item->domain)
                            <a href="{{UserWebsite::url(null,$item->domain)}}?preview=1" class="btn btn-success btn-xs" target="_blank">عرض المتجر</a>
                            @endif
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/userwebsites', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="fa fa-trash" aria-hidden="true" title="حذف" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'حذف',
                                    'onclick'=>'return confirm("Delete record: Are you sure?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection