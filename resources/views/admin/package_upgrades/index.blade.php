<?php
    use App\Packages\UserPackage;
    use App\Helper;
    $status_form = array_prepend(UserPackage::$status, 'كل الحالات', 'all');
    $packages = array_prepend($packages->toArray(), 'كل الباقات', 'all');

    $paid_arr = [
        'all'=>'Paid & Not Paid',
        0=>'Not Paid',
        1=>'Paid'
    ];
?>
@extends('admin.layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">باقات العملاء ({{$package_upgrades->count()}})</div>
        <div class="panel-body">
            <div class="well well-sm">
                {!!Form::open(['method'=>'GET', 'class'=>'form-inline form-filter'])!!}
                    <div class="form-group">
                        {!!Form::select('confirmed', $status_form, Request::get('confirmed'),['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::select('paid', $paid_arr, Request::get('paid'),['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::select('package_id', $packages, Request::get('package_id'),['class'=>'form-control'])!!}
                    </div>
                {!!Form::close()!!}
            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                    <tr>
                        <th>رقم</th>
                        <th>الحساب</th>
                        <th>اسم الباقة</th>
                        <th>السعر</th>
                        <th>الحالة</th>
                        <th>إتمام الدفع</th>
                        <th>التاريخ</th>
                        <th>التاريخ</th>
                        <th>مهام</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($package_upgrades as $index=>$item)
                    <tr>
                        <td>{{ $index+1 }}</td>
                        <td>{!!App\User::user($item)!!}</td>
                        <td>{{ Helper::field($item->package,'name') }}</td>
                        <td>{{ round($item->amount) }} {{ $item->currency }}</td>
                        <td>{!!Helper::statusCtrl(UserPackage::$status,$item,'confirmed','Packages\UserPackage')!!}</td>
                        <td>{!!Helper::sw($item,'paid','Packages\UserPackage')!!}</td>
                        <td>{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            @if($item->domain)
                            <a href="{{UserPackage::url(null,$item->domain)}}" class="btn btn-success btn-xs" target="_blank">عرض المتجر</a>
                            @endif
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/package_upgrades', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="fa fa-trash" aria-hidden="true" title="حذف" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'حذف',
                                    'onclick'=>'return confirm("Delete record: Are you sure?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection