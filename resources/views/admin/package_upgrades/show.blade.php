<?php
    use App\Category;
    use App\Helper;
    $parents = array_reverse($category_parents);
    $service_locations = (!empty($service->locations)) ? explode(',',$service->locations): [];
?>
<div class="white-popup-block" style="min-width: 900px;">
{!! Form::model($service, ['method' => 'PATCH', 'class' => 'form-horizontal', 'id'=>'service_form']) !!}
    <ul class="breadcrumb">
        <li><a href="{{url('/')}}">{{trans('front.home')}}</a></li>
        @if(!empty($parents))
            @foreach($parents as $cat)
                <li><a href="{{Category::url($cat)}}">{{@$cat->$c_name}}</a></li>
            @endforeach
        @endif
    </ul>
    <div class="form-group name {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', trans('dash.service_name'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('description', trans('deals.description'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=0; ?>
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#sec{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==0) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="sec{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        {!! Form::textarea('lang['.$lang->id.'][description]',null,['class'=>'form-control','rows'=>5]) !!}
                        </div></div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <?php $field = 'image'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.image'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <div class="media">
                <?php $filepath=(isset($service))? 'uploads/'.$service->$field:'frontend/images/no_thumb.jpg';?>
                <a class="pull-left" href="#">{!!HTML::image($filepath,'',['class'=>'media-object img_preview imgfield1'])!!}</a>
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('details', trans('deals.details'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            <div role="tabpanel">
                <?php $count=0; $count2=1; ?>
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $lang)
                        <?php $active = ($count==0) ? 'class="active"':''; ?>
                        <li {!!$active!!}><a href="#det{{$lang->id}}" data-toggle="tab">{{$lang->name}}</a></li>
                        <?php $count++ ?>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $lang)
                    <?php $active = ($count2==1) ? 'active':''; ?>
                    <div class="tab-pane fade in {!!$active!!}" id="det{{$lang->id}}">
                        <div class="panel panel-default" style="border-top: none;"><div class="panel-body">
                        {!! Form::textarea('lang['.$lang->id.'][details]', null, ['class'=>'form-control','rows'=>3]) !!}
                        </div></div>
                    </div>
                    <?php $count2++ ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('price_min') ? 'has-error' : ''}}">
        {!! Form::label('price_min', trans('dash.price_range'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::text('price_min', null, ['class' => 'form-control', 'placeholder'=>trans('dash.min')]) !!}
            {!! Form::text('price_max', null, ['class' => 'form-control', 'placeholder'=>trans('dash.max')]) !!}
        </div>
    </div>
    <?php $field = 'document'; ?>
    <div class="form-group {{ $errors->has($field) ? 'has-error' : ''}}">
        {!! Form::label($field, trans('dash.document_if_any'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! $errors->first($field, '<p class="help-block">:message</p>') !!}
            @if(isset($service) && !empty($service->document))
                <a class="pull-right btn btn-info" href="{{url('uploads/'.$service->document)}}">{{trans('dash.preview')}}</a>
            @endif
        </div>
    </div>
    {!!Form::hidden('locations', '')!!}
    <div class="form-group {{ $errors->has('locations') ? 'has-error' : ''}}">
        {!! Form::label('locations', trans('dash.service_offered_location'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            @if(count($service_locations)>0)
                @foreach($service_locations as $id)
                    <a href="#" class="btn btn-sm btn-default">{{App\Country::get_country($id)->name}}</a>
                @endforeach
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.valid_till'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class' => 'form-control expired_datepicker','required'=>'required']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    {!!Form::close()!!}
</div>
{!!HTML::script('frontend/js/app/products.js')!!}
{!!HTML::script('frontend/js/app/deals.js')!!}
<script type="text/javascript">
    @if(isset($category_tree))
        $(document).ready(function() {
            $(".select_category").show();
            $('.cat_selector').val('{{$category_tree[0]->id}}');
            $('.cat_selector').trigger('change');
            $('.sub').attr('data-id', {{$category_tree[1]->id}});
            @if(!empty($category_tree[2]->id))
                $('.subsub').attr('data-id', {{$category_tree[2]->id}});
            @endif
            $('#selected_category_id').val('{{$service->category_id}}');
        });
    @endif
</script>