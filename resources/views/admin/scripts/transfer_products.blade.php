@extends('admin.layouts.master')

@section('content')
    <div class="container">
    	<legend>Transfer Products</legend>
    	Transfer products from old Website to This Platform
    	{!!Form::open(['url'=>'admin/transfer/transfer-products'])!!}
    		{!!Form::submit('Start Transfer', ['class'=>'btn-loading btn btn-primary'])!!}
    	{!!Form::close()!!}
    </div>
@endsection