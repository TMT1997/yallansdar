@extends('admin.layouts.master')

@section('content')
    <div class="container">
    	<legend>Transfer Users</legend>
    	Transfer User Accounts from current Website to This Platform
    	{!!Form::open(['url'=>'admin/transfer/transfer-users'])!!}
    		{!!Form::submit('Start Transfer', ['class'=>'btn-loading btn btn-primary'])!!}
    	{!!Form::close()!!}
    </div>
@endsection