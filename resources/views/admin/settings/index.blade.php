<?php use App\Settings; ?>
@extends("admin.layouts.master")

@section("content")

    <div class="panel-heading">
        <legend>اﻹعدادت</legend>
    </div>

    <div class="panel-body">
        {!!Form::open(['class'=>'form-horizontal','url'=>'admin/settings/save','files'=>true])!!}

        <div class="well well-sm">
            <h4 class="text-primary">إعدادات الموقع</h4>
            <div class="form-group">
                {{ Form::label('website_title', 'عنوان الموقع', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[site][website_title]',Settings::fetch('website_title',$site),['class'=>'form-control','placeholder'=>'عنوان الموقع']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('copyrights', 'نص الحقوق محفوظة', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[site][copyrights]',Settings::fetch('copyrights',$site),['class'=>'form-control','placeholder'=>'جميع الحقوق محفوظة']) }}
                </div>
            </div>
        </div>

        <div class="well well-sm">
            <h4 class="text-primary">إعدادات البريد اﻹلكتروني</h4>
            <div class="form-group">
                {{ Form::label('email_from', 'الإيميل المرسل', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[mail][email_from]',Settings::fetch('email_from',$mail),['class'=>'form-control','placeholder'=>'اﻹيميل الذي سيقوم بإرسال الرسائل']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('email_to', 'أرسل إلى إيميل', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[mail][email_to]',Settings::fetch('email_to',$mail),['class'=>'form-control','placeholder'=>'اﻹيميل المرسل إليه']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('email_name', 'اسم المرسل إليه', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[mail][email_name]',Settings::fetch('email_name',$mail),['class'=>'form-control','placeholder'=>'اسم المرسل إليه']) }}
                </div>
            </div>
        </div>

        <div class="well well-sm">
            <h4 class="text-primary">إعدادات العرض</h4>
            <div class="form-group">
                {{ Form::label('enable_registration', 'التسجيل بالموقع', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {!!Settings::option_box('enable_registration', $awards, 'فتح باب التسجيل', 'awards')!!}
                </div>
            </div>
        </div>

        <div class="well well-sm">
            <h4 class="text-primary">صفحات التواصل</h4>
            @foreach(Settings::$social_arr as $key=>$val)
                <div class="form-group">
                    {{ Form::label($key, $val, ['class'=>'col-md-3 control-label']) }}
                    <div class="col-md-9">
                        {{ Form::text('settings[social]['.$key.']',Settings::fetch($key,$social) ,['class'=>'form-control ltr','placeholder'=>$val]) }}
                    </div>
                </div>
            @endforeach
        </div>

        <div class="well well-sm">
            <h4 class="text-primary">معلومات اﻻتصال</h4>
            <div class="form-group">
                {{ Form::label('Address', 'العنوان', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Address]',Settings::fetch('Address',$contact) ,['class'=>'form-control','placeholder'=>'العنوان']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Phone', 'التليفون', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Phone]',Settings::fetch('Phone',$contact) ,['class'=>'form-control','placeholder'=>'التليفون']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Mobile', 'الموبايل', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Mobile]',Settings::fetch('Mobile',$contact) ,['class'=>'form-control','placeholder'=>'رقم الموبايل']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Fax', 'الفاكس', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Fax]',Settings::fetch('Fax',$contact) ,['class'=>'form-control','placeholder'=>'فاكس']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Email', 'البريد اﻹلكتروني', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Email]',Settings::fetch('Email',$contact) ,['class'=>'form-control','placeholder'=>'البريد اﻹلكتروني']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Skype', 'حساب السكاي بي', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][Skype]',Settings::fetch('Skype',$contact) ,['class'=>'form-control','placeholder'=>'حساب السكاي بي']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('WhatsApp', 'حساب الواتس اب', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::text('settings[contact][WhatsApp]',Settings::fetch('WhatsApp',$contact) ,['class'=>'form-control','placeholder'=>'حساب الواتس اب']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('Map', 'الخريطة', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    <div class="">
                        {!! Form::text('settings[contact][Map]', Settings::fetch('Map',$contact), ['class' => 'placepicker form-control','data-map-container-id'=>'collapseOne']) !!}
                    </div>
                    <div id="collapseOne" class=""><div class="placepicker-map thumbnail"></div></div>
                    {!! $errors->first('map', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>

        <div class="well well-sm">
            <h4 class="text-primary">إعدادات محرك البحث</h4>            
            <div class="well well-sm">
                <legend>إعدادات الصفحة الرئيسية</legend>
                <div class="form-group">
                    {{ Form::label('description_home', 'الوصف', ['class'=>'col-md-3 control-label']) }}
                    <div class="col-md-9">
                        {{ Form::textarea('settings[meta][description_home]',Settings::fetch('description_home',$meta) ,['class'=>'form-control','placeholder'=>'الوصف للصفحة الرئيسية','rows'=>3]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('keywords_home', 'كلمات البحث', ['class'=>'col-md-3 control-label']) }}
                    <div class="col-md-9">
                        {{ Form::text('settings[meta][keywords_home]',Settings::fetch('keywords_home',$meta) ,['class'=>'form-control tags','placeholder'=>'الكلمات المفتاحية للرئيسية']) }}
                    </div>
                </div>
            </div>

            <div class="well well-sm">
                <legend>إعدادات صفحة اتصل بنا</legend>
                <div class="form-group">
                    {{ Form::label('description_contact', 'الوصف', ['class'=>'col-md-3 control-label']) }}
                    <div class="col-md-9">
                        {{ Form::textarea('settings[meta][description_contact]',Settings::fetch('description_contact',$meta) ,['class'=>'form-control','placeholder'=>'الوصف لصفحة اتصل بنا','rows'=>3]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('keywords_contact', 'كلمات البحث', ['class'=>'col-md-3 control-label']) }}
                    <div class="col-md-9">
                        {{ Form::text('settings[meta][keywords_contact]',Settings::fetch('keywords_contact',$meta) ,['class'=>'form-control tags','placeholder'=>'الكلمات المفتاحية لصفحة اتصل بنا']) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">{!!Form::submit('حفظ اﻹعدادت', ['class'=>'btn btn-success btn-submit'])!!}</div>
        </div>
        {!!Form::close()!!}
    </div>
@endsection

@section('script')
    {!!HTML::style('backend/plugins/tags/jquery.tagsinput.min.css')!!}
    {!!HTML::script('backend/plugins/tags/jquery.tagsinput.min.js')!!}
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBbQjR-we6l9PFW_2kMXzpVwKYcGr_tQhU&sensor=true&libraries=places"></script>
    {!!HTML::script('backend/plugins/locationpicker/jquery.placepicker.min.js')!!}
    
    <script type="text/javascript">
        $('.tags').tagsInput({
            'width':'100%',
            'height':'80px',
            'defaultText':'اكتب ثم اضغط Enter',
        });
        var mapPlacepicker = $(".placepicker").placepicker();
    </script>
@endsection