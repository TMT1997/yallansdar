<?php use App\Settings; ?>
@extends('admin.layouts.master')

@section('content')

    <div class="panel-heading">
        <legend>اعدادت النصوص</legend>
    </div>

    <div class="panel-body">
        {!!Form::open(['class'=>'form-horizontal','url'=>'admin/settings/save','files'=>true])!!}

        <div class="well well-sm">
            <h4 class="text-primary">إعدادات الموقع</h4>
            <div class="form-group">
                {{ Form::label('register', 'نص التقدم  للجائزة', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::textarea('settings[texts][register]',Settings::fetch('register',$texts),['class'=>'form-control','id'=>'editr1']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('registered', 'نص الرسالة بعد التسجيل مباشرة', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::textarea('settings[texts][registered]',Settings::fetch('registered',$texts),['class'=>'form-control','id'=>'editr2']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('account_page', 'نص في صفحة الحساب الشخصي', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::textarea('settings[texts][account_page]',Settings::fetch('account_page',$texts),['class'=>'form-control','id'=>'editr3']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('application_notes', 'نص التنبيه في صفحة إرسال استماره', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::textarea('settings[texts][application_notes]',Settings::fetch('application_notes',$texts),['class'=>'form-control','id'=>'editr4']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('application_submitted', 'نص الرسالة بعد إرسال المترشح استمارة', ['class'=>'col-md-3 control-label']) }}
                <div class="col-md-9">
                    {{ Form::textarea('settings[texts][application_submitted]',Settings::fetch('application_submitted',$texts),['class'=>'form-control','id'=>'editr5']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">{!!Form::submit('حفظ اﻹعدادت', ['class'=>'btn btn-success btn-loading btn-submit'])!!}</div>
        </div>
        {!!Form::close()!!}
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var configShared = {
            toolbar : 'Basic',
        };
        
        CKEDITOR.replace('editr1', configShared);
        CKEDITOR.replace('editr2', configShared);
        CKEDITOR.replace('editr3', configShared);
        CKEDITOR.replace('editr4', configShared);
        CKEDITOR.replace('editr5', configShared);
        
    </script>
@endsection