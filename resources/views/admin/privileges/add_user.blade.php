@extends('admin.layouts.master')

@section('content')
    <div class="container" style="padding-top: 20px;">
        <div class="row">
            @if ($errors->any())

                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" style="border-radius: 25px">
                            {{$error}}
                        </div>
                    @endforeach
                </ul>

            @endif
            <div class="registration_block">
                <div class="panel section_wrap">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/privilege/add-user') }}">
                            {{ csrf_field() }}
                            <div class="terms_check" style="width: 0; height:0; position: absolute; bottom: 0; left:0; ">{!!Form::checkbox('terms_checker', 1, false, ['id'=>'check_terms', 'style'=>'width:0;height:0'])!!}</div>

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="name" class="control-label">@lang('dash.first_name')</label>
                                    <input id="name" type="text" class="form-control check_english" name="name" value="{{ old('name') }}" placeholder="@lang('dash.first_name')">
                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="last_name" class="control-label">@lang('dash.last_name')</label>
                                    <input id="last_name" type="text" class="form-control check_english"name="last_name" value="{{ old('last_name') }}" placeholder="@lang('dash.last_name')">
                                    @if ($errors->has('last_name'))

                                        <span class="help-block"><strong>{{ $errors->first('last_name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="email" class="control-label">@lang('front.email')</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('front.email')">

                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="mobile" class="control-label">@lang('front.mobile')</label>
                                    <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="@lang('front.mobile')" size="30">

                                    @if ($errors->has('mobile'))
                                        <span class="help-block"><strong>{{ $errors->first('mobile') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="password" class="control-label">@lang('front.password')</label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="@lang('front.password')">

                                    @if ($errors->has('password'))
                                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="col-md-10 col-md-offset-1">
                                    <label for="password-confirm" class="control-label">@lang('front.confirm_password')</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <button type="submit" style="height: unset" class="btn btn-primary btn-lg btn-loading form-control">
                                        <i class="fa fa-btn fa-user"></i> @lang('front.joinus')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

    </script>
@endsection