@extends('admin.layouts.master')

@section('content')
<?php
$countries = array_prepend($countries->toArray(), trans('dash.select_country'), 0);
?>
{!!Form::open(['class'=>'form-horizontal','id'=>'deals_form','files'=>true,'novalidate'=>'']) !!}
<div class="well well-sm">{{trans('dash.request_information')}}</div>
    <div class="form-group {{ $errors->has('request_for') ? 'has-error' : ''}}">
        <div class="col-md-12">
            @foreach(trans('dash.rfq_request_for') as $val=>$item)
                @if($val=="product")
                <label>{!!Form::radio('request_for', $val, 1, ['class'=>'request_for required'])!!} {{$item}}</label>
                @else
                @endif
            @endforeach
        </div>
    </div>
     <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
        {!! Form::label('category', trans('products.prod_cat'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            <a href="javascript:void(0);" class="toggle_selectcat btn">{{trans('products.select_cat')}}</a>
        </div>
    </div> 
    @include('dashboard.categories.select_category', ['selected_category_id'=>@$quote_request->category_id, 'keep_parent_id'=>true])

    <div class="form-group">
    @foreach(\App\Language::$langs as $iso=>$lang)
        <div class="col-md-6">
            <div class="{{$errors->has('name_'.$iso)?'has-error':''}}">
                {!! Form::label('name_'.$iso, trans('dash.Service_Product_Name').' ('.$lang['name'].')', ['class'=>'control-label ']) !!}
                <div>
                    @if($iso == "en")
                    {!! Form::text('name_'.$iso, null,['class'=>'form-control prod_cat_search check_english','required'=>'required']) !!}
                    {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!}
                    @else
                    {!! Form::text('name_'.$iso, "",['class'=>'form-control prod_cat_search']) !!}
                    {!! $errors->first('name_'.$iso, '<p class="help-block">:message</p>') !!} 
                    @endif
                </div>
            </div>
            <div class="{{$errors->has('details_'.$iso)?'has-error':''}}">

                {!! Form::label('details_'.$iso, trans('dash.req_in_details').' ('.$lang['name'].')', ['class'=>'control-label']) !!}

                <div>
                    {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso]) !!}
                    {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    @endforeach
    </div>

    <div class="form-group {{ $errors->has('add_images') ? 'has-error' : ''}}">
        {!! Form::label('add_images', trans('dash.desc_image'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 additional_imgs">
            @if(isset($quote_request))
                <div class="row">
                    @foreach($quote_request->images as $item)
                    <div class="col-xs-6 col-sm-4 col-md-3 infile">
                        <div class="thumbnail">
                            {!!App\Helper::image($item->image,['style'=>'max-height:60px;'])!!}
                            <div class="caption">
                                <p>{{$item->name}}</p>
                                {!!Form::button('<i class="fa fa-trash" title="Delete"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\Deals\QuoteRequestImage").'")'])!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endif
            <div class="media">
                <a class="pull-left" href="#">{!!HTML::Image('frontend/images/no_thumb.jpg','',['class'=>'media-object img_preview imgfields'])!!}</a>
                <a class="pull-right btn btn-success add_imgfile_button">+</a>
                <div class="media-body">{!!Form::file('add_images[]', ['class'=>'form-control img_browse','data-fid'=>'s'])!!}</div>
            </div>
            <div class="input_imgfiles_wrap"></div>
            <p><div class="help-block">{{trans('products.img_info')}}</div></p>
            {!! $errors->first('add_images', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group order_quantity {{ $errors->has('order_quantity') ? 'has-error' : ''}}">
        {!! Form::label('order_quantity', trans('dash.est_order_quantity'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('order_quantity', null,['class'=>'form-control','required'=>'required']) !!}
            <label>{!!Form::select('unit',trans('dash.units'),null,['class'=>'form-control select2','required'=>'required']) !!}</label>
        </div>
    </div>

    <div class="form-group durations {{ $errors->has('duration_from') ? 'has-error' : ''}}">
        {!! Form::label('duration_from', trans('dash.service_req_duration'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        {!! Form::text('duration_from',null,['class'=>'form-control expired_datepicker','placeholder'=>trans('dash.from'),'required'=>'required']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        {!! Form::text('duration_to', null, ['class'=>'form-control expired_datepicker','placeholder'=>trans('dash.to'),'required'=>'required']) !!}
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            {!! $errors->first('duration_from', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('expired_at') ? 'has-error' : ''}}">
        {!! Form::label('expired_at', trans('dash.rfq_expiry_date'), ['class' => 'col-md-3 control-label required']) !!}
        <div class="col-md-8 form-inline">
            <div class="input-group">
                {!! Form::text('expired_at', null, ['class' => 'form-control expired_datepicker','required'=>'required']) !!}
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            {!! $errors->first('expired_at', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

<div class="well well-sm">{{trans('dash.trade_information')}}</div>
    {!!Form::hidden('supplier_location', '')!!}
    <div class="form-group {{ $errors->has('supplier_location') ? 'has-error' : ''}}">
        {!! Form::label('supplier_location',trans('dash.Preferred_Supplier_Location'),['class'=>'col-md-3 control-label'])!!}
        <div class="col-md-8 form-inline">
            {!! Form::select('supplier_location[]',$countries,null,['class'=>'form-control select2','multiple'=>'multiple'])!!}
            {!! $errors->first('supplier_location', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('frequency') ? 'has-error' : ''}}">
        {!! Form::label('frequency', trans('dash.Sourcing_frequency'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8 form-inline">
            {!! Form::select('frequency',trans('dash.source_frequencies'),null,['class'=>'form-control']) !!}
            {!! $errors->first('frequency', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group annual_purchase {{ $errors->has('annual_purchase') ? 'has-error' : ''}}">
        {!! Form::label('annual_purchase', trans('dash.Annual_Purchase_Volume'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('annual_purchase', null,['class'=>'form-control']) !!}
            <label>{!!Form::select('purchase_unit',trans('dash.units'),null,['class'=>'form-control select2']) !!}</label>
        </div>
    </div>

    {{-- <div class="form-group brand {{ $errors->has('brand') ? 'has-error' : ''}}">
        {!! Form::label('brand', trans('dash.Preferred_Brand_Name'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::text('brand', null,['class'=>'form-control']) !!}
        </div>
    </div> --}}

    <div class="form-group unit_price {{ $errors->has('unit_price') ? 'has-error' : ''}}">
        {!! Form::label('unit_price', trans('dash.Preferred_Unit_Price'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 form-inline">
            {!! Form::number('unit_price', null,['class'=>'form-control','placeholder'=>trans('dash.unit_price')]) !!}
            <label>{!!Form::select('currency',trans('dash.fob_price_list'),null,['class'=>'form-control']) !!}</label>
        </div>
    </div>

    <div class="form-group payment_terms {{ $errors->has('payment_terms') ? 'has-error' : ''}}">
        {!!Form::hidden('payment_terms', '')!!}
        {!! Form::label('payment_terms', trans('dash.payment_terms'), ['class'=>'col-md-3 control-label required']) !!}
        <div class="col-md-9">
            <div class="row">
                @foreach(trans('dash.payment_methods_list') as $item=>$label)
                <div class="col-md-3">
                    <label title="{{$item}}">{!!Form::checkbox('payment_terms[]',$item,null,['class'=>'pay_types','required'=>'required'])!!} {{$label}}</label>
                    @if($label=='Other')
                        <span id="other_payment_type">
                            {!!Form::label('other_payment', 'Enter your payment type',['class'=>'control-label'])!!}
                            {!!Form::text('other_payment_type', null,['class'=>'form-control other_paytype'])!!}
                        </span>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="form-group port {{ $errors->has('port') ? 'has-error' : ''}}">
        {!! Form::label('port', trans('dash.destination_port'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('port', null, ['class' => 'form-control check_english','placeholder'=>trans('dash.destination_port')]) !!}
            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
<div class="form-group">
    <div class="col-md-offset-3 col-md-5">
        <input type="submit" name="add_pd3" class="submit_form btn btn-info" value="{{trans('dash.save')}}"/>
        {!! Form::close() !!}
    </div>
</div>
@section('script')
{!!HTML::style('backend/dist/css/jquery.tagsinput.min.css')!!}
{!!HTML::script('backend/dist/js/jquery.tagsinput.min.js')!!}
{!!HTML::script('frontend/js/app/products.js')!!}
@include('dashboard.layouts.popup');
<script type="text/javascript">
    var catSearchURL = '{{url('dashboard/settings/category-search')}}';
    var $no_thumb_url = '{{url('frontend/images/no_thumb.jpg')}}';
    var $addGroupRoute = '{{route('dashboard.groups.store') }}';
    var $switch_bool_url = '{{url('admin/settings/switch-boolean')}}';
    //add fields
    function getTemplate($sec)
    {
        $output = '';
        if($sec=='products') {
            $output='<div class="frec">'+
            '{!!Form::text('main_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name'), 'required'=>'required'])!!} '+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
        if($sec=='oproducts') {
            $output='<div class="frec">'+
            '{!!Form::text('other_products[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')])!!} '+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
        if($sec=='website') {
            $output='<div class="frec">'+
            '{!!Form::text('website[]',null,['class'=>'form-control','placeholder'=>trans('dash.website')])!!} '+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
        if($sec=='product_we_source') {
            $output='<div class="frec">'+
            '{!!Form::text('product_we_source[]',null,['class'=>'form-control','placeholder'=>trans('dash.product_name')]) !!} '+
            '{!!Form::text('quantity[]', null,['class'=>'form-control short','placeholder'=>trans('dash.qty')]) !!} '+
            '{!!Form::select('qtyunit[]', trans('dash.units'),null, ['class'=>'form-control']) !!} <span>({{trans("dash.yearly")}})</span>'+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
            if($sec=='telephone') {
            $output='<div class="frec">'+
            '{!!Form::number('tel_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
            '{!!Form::number('telephone[]', null,['class'=>'form-control','placeholder'=>trans('dash.telephone')]) !!} '+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
        if($sec=='mobile') {
            $output='<div class="frec">'+
            '{!!Form::number('mob_code[]', null,['class'=>'form-control short','placeholder'=>trans('dash.code')]) !!} '+
            '{!!Form::number('mobile[]', null,['class'=>'form-control','placeholder'=>trans('dash.mobile')]) !!} '+
            '<a href="javascript;" class="btn btn-danger btn-sm remove_field">-</a></div>';
        }
        return $output;
    }


    $(document).on('click', '.add_new_field', function(e){
        e.preventDefault();
        var $ft = $(this).data('ft');
        var max_fields = $(this).data('max');
        var count_childs = $(this).siblings('.more_fields').children('.frec').length;
        count_childs = count_childs+1;
        if(count_childs < max_fields){
            $wrapper = $(this).siblings('.more_fields');
            $wrapper.append(getTemplate($ft));
        } else {
            alert('Sorry, you cannot add more than '+max_fields+' inputs');
        }
    });
    $(document).on("click",".remove_field", function(e){
        e.preventDefault(); $(this).parent('div').remove();
    });
    $(document).ready(function() {            
        $(".select_category").hide();
        $(document).on('click', '.toggle_selectcat', function(event) {
            event.preventDefault();
            $(".select_category").slideToggle('400');
        });
        $(document).on('click', '.selectcat_cancel', function(event) {
            event.preventDefault();
            $('.select_category').slideUp('fast', function() {
            });
        });
        $(document).on('click', '.selectcat_ok', function(event) {
            event.preventDefault();
            $('.selected_cat_path').html($('.selected_cat').html());
            $('.select_category').slideUp('fast', function() {
            });
        });
        $(document).on('click', '.btn_cat_browse', function(event) {
            event.preventDefault();
            $('.cat_search_results').html('');
            $('.cat_cord').show();
            $('.toggle_selectcat').trigger('click');
            $('.select_category').show('fast', function() {
            });
        });
        $(document).on('click', '.toggle_selectcat ', function(event) {
            event.preventDefault();
            if($('#selected_category_id').val()!=0) {
                return false; //if already any category selected then don't reload categories
            }
            $('.sub').html(''); $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/0',
                type: 'GET',
                beforeSend:function(){
                    $('.cat_select_plugin .parent').html('{{trans('dash.loading_categories')}}');
                }
            }).done(function(response) {
                //$('.selected_cat').html(getSelectedText('.cat_selector', $cat_id));
                if(response!=='') {
                    $('.parent').html('<select required="required" name="category_id" class="form-control cat_selector" data-parent="1" size="7">'+response+'</select>');
                } else {
                    $('.parent').html('nothing to preview');
                }
            }).fail(function() {
                alert('Error while process categories');
            }).always(function() {
            });             
        });
        $(document).on('change', '.cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $('.sub').html(''); $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/'+ $cat_id,
                type: 'GET',
            }).done(function(response) {
                $('.selected_cat').html(getSelectedText('.cat_selector', $cat_id));

                if(response!=='') {
                    $('.sub').append('<select name="category_id" size="7" class="form-control sub_cat_selector" style="display:inline-block;">'+response+'</select>');
                } else {
                    $('.selected_cat').append(' ... DONE');
                    $("#selected_category_id").val($cat_id);
                }
            }).fail(function() {
                alert('Error while process sub categories');
            }).always(function() {
                //check if sub has data-id, this is used in the Edit Product
                if($('.sub').data('id')!='') {
                    $('.sub_cat_selector').val($('.sub').data('id'));
                    $('.sub_cat_selector').trigger('change');
                }
            });
        });
        $(document).on('change', '.sub_cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $parent_id = $('.cat_selector').val();

            $('.subsub').html('');
            $.ajax({
                url: '{{url('category/sub')}}/'+ $cat_id,
                type: 'GET',
            }).done(function(response) {

                $('.selected_cat').html(getSelectedText('.cat_selector', $parent_id));
                $('.selected_cat').append(' >> ' + getSelectedText('.sub_cat_selector', $cat_id));

                if(response!=='') {
                    $('.subsub').append('<select name="category_id" size="7" class="form-control subsub_cat_selector" style="display:inline-block;">'+response+'</select>');
                } else {
                    $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                    $("#selected_category_id").val($cat_id);
                }
            }).fail(function() {
                alert('Error while process sub of sub categories');
            }).always(function() {
                //check if subsub has data-id, this is used in the Edit Product
                if($('.subsub').data('id')!='') {
                    $('.subsub_cat_selector').val($('.subsub').data('id'));

                    $('.selected_cat').append(' >> ' + getSelectedText('.subsub_cat_selector', $('.subsub').data('id')));
                    $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');
                }
            });
        });
        $(document).on('change', '.subsub_cat_selector', function(event) {
            event.preventDefault();
            $cat_id = $(this).val();
            $parent_cat_id = $('.cat_selector').val();
            $sub_cat_id = $('.sub_cat_selector').val();

            $('.selected_cat').html(getSelectedText('.cat_selector', $parent_cat_id));
            $('.selected_cat').append(' >> ' + getSelectedText('.sub_cat_selector', $sub_cat_id));
            $('.selected_cat').append(' >> ' + getSelectedText('.subsub_cat_selector', $cat_id));
            $('.selected_cat').append(' <i class="fa fa-check-circle text-success"></i>');

            $("#selected_category_id").val($cat_id);
        });

        $(".cat_select").on('change', function(event) {
            event.preventDefault();
            $(".selectcatForm").submit();
        });
    });
    $(document).on('click', '.btn_cat_search', function(event) {
        event.preventDefault();
        $('.cat_set_fsearch').trigger('blur');
    });
    $(document).on('click', '.cat_set_fsearch', function(event) {
        event.preventDefault();
        var $cat_id = $(this).data('id');
        $("#selected_category_id").val($cat_id);
        $('.selected_cat_path').addClass('well well-sm');
    });
    $(document).on("keypress", '.cat_q', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            $('.btn_cat_search').trigger('click');
            return false;
        }
    });
    function getSelectedText($obj, $val) {
        return $($obj + " option[value='"+$val+"']").text();
    }

    $(document).ready(function() {
        $(".select_category").show();
        @if(isset($category_tree) && !empty($category_tree))
            $('.cat_selector').val('{{@$category_tree[0]->id}}');
            $('.cat_selector').trigger('change');
            @if(isset($category_tree[1]->id))
                $('.sub').attr('data-id', {{$category_tree[1]->id}});
            @endif
            @if(isset($category_tree[2]) && !empty($category_tree[2]->id))
                $('.subsub').attr('data-id', {{@$category_tree[2]->id}});
                $('.subsub_cat_selector').trigger('change');
            @endif
            $('#selected_category_id').val('{{@$selected_category_id}}');
        @endif 
    });

    //Search in categories
    $(document).on('click', '.btn_cat_search', function(event) {
        event.preventDefault();
        var $q = $('.cat_q ').val();
        $.ajax({
            url: catSearchURL,
            type: 'GET',
            data:{q:$q},
            beforeSend:function() {
                var $loader_new = '<div class="text-center"><i class="fa fa-spinner fa-spin fa-fw"></i><span class="">@lang('front.loading')</span></div>';
                $('.cat_search_results').html($loader_new);
            }
        }).done(function(results) {
            if(results=='') {
                $('.cat_search_results').html('لا يوجد نتائج لبحثك');
            } else {
                $('.cat_search_results').html(results);                
            }
        }).fail(function(error) {
            alert('Error while searching');
        }).always(function() {
            $('.cat_cord').hide();
            $('.select_category').slideDown('slow');
        });
    });
    $("#deals_form").submit(function(e){
       $('#deals_form').validate({
        rules: {
            "name_ar": "required",
            "name_en": "required",
            "details_ar": "required",
            "details_en": "required",
        },
        messages: {
            'service_product_name':'من فضلك ادخل اسم المنتج / الخدمة',
            "details_ar": "من فضلك ادخل تفاصيل عرض السعر",
            "details_en": "من فضلك ادخل تفاصيل عرض السعر",
        }
    });
    var form_data = new FormData($("#deals_form")[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{url('admin/privilege/add-rfq')}}',
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: form_data,

    }).done(function(response) {
      alert("done");
    }).fail(function(response) {
      alert("error");
    }).always(function() {
    });
     e.preventDefault();
});
</script>
@endsection
@endsection