@extends('admin.layouts.master')
@section('title') المدونة | @endsection
@section('content')
    <section class="content-header">
      <h1>اضف مقال جديد</h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
            <li><a href="{{url('admin/blog')}}">المدونة</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border"></div>
            <div class="box-body">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::open(['url'=>'admin/blog', 'class'=>'form-horizontal page_form', 'files'=>true]) !!}
                @include ('admin.blog.form')
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
