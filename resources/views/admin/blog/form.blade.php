<div class="form-group {{ $errors->has('category_id')?'has-error':''}}">
    {!! Form::label('category_id', 'القسم', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-5 form-inline">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-2">
        <a href="{{url('admin/quickadd/blog/category')}}" class="popup btn btn-default">+</a>
    </div>
</div>

<div class="form-group {{ $errors->has('author_id')?'has-error':''}}">
    {!! Form::label('author_id', 'الكاتب', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-5 form-inline">
        {!! Form::select('author_id', $authors, null, ['class' => 'form-control']) !!}
        {!! $errors->first('author_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-2">
        <a href="{{url('admin/quickadd/blog/author')}}" class="popup btn btn-default">+</a>
    </div>
</div>

<div class="form-group">
@foreach(App\Language::$langs as $iso=>$lang)
    <div class="col-md-6">
        <div class="{{$errors->has('title_'.$iso)?'has-error':''}}" style="margin-bottom: 15px;">
            {!! Form::label('title_'.$iso, 'عنوان المقال ('.$lang['name'].')', ['class'=>'control-label']) !!}
            <div>
                {!! Form::text('title_'.$iso, null, ['class'=>'form-control']) !!}
                {!! $errors->first('title_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="{{$errors->has('details_'.$iso)?'has-error':''}}" style="margin-bottom: 15px;">
            {!! Form::label('details_'.$iso, 'المحتوى ('.$lang['name'].')', ['class'=>'control-label']) !!}
            <div>
                {!! Form::textarea('details_'.$iso, null,['class'=>'form-control','id'=>'editor_'.$iso]) !!}
                {!! $errors->first('details_'.$iso, '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
@endforeach
</div>

<div class="image">
    {!!App\Helper::image_field('image', @$article)!!}
</div>

<div class="form-group {{ $errors->has('tags')?'has-error':''}}">
    {!! Form::label('tags', 'الوسوم', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 form-inline">
        {!! Form::text('tags', null, ['class' => 'form-control tags']) !!}
        {!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{$errors->has('active')?'has-error':''}}">
    {!! Form::label('active', 'إظهار', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1', null) !!} نعم</label>
            <label>{!! Form::radio('active', '0', null) !!} لا</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-3 col-md-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'اضافة', ['class' => 'btn btn-primary btn_loading']) !!}
        <a href="javascript:history.back(1);" class="btn btn-default">العودة</a>
    </div>
</div>


@section('script')
    @include('admin.layouts.popup');
    {!!HTML::style('backend/plugins/tags/jquery.tagsinput.min.css')!!}
    {!!HTML::script('backend/plugins/tags/jquery.tagsinput.min.js')!!}    
    <script type="text/javascript">
        $('.tags').tagsInput({
            'width':'100%',
            'height':'80px',
            'defaultText':'اكتب ثم اضغط Enter',
        });

        $(document).on('click','.btn_quickAddAuthor', function(event) {
            event.preventDefault();
            var $btn  = $(this);
            $.ajax({
                url: '{{route('admin.blog_authors.store') }}',
                data: $("#popup_form").serializeArray(),
                type:'POST',
                dataType:'json',
                beforeSend: function() {
                    $btn.button('loading');
                }
            }).done(function(output) {
                if (output.status == 'success') {
                    var indata = output.data;
                    $("#author_id").append('<option value="'+indata.id+'" selected="selected">'+indata.name_ar+'</option>');
                    $.magnificPopup.close();
                } else {
                    alert('Error occured');
                }
            }).fail(function() {
                alert('Error');
            }).always(function() {
                $btn.button('reset');
            });
        });
        $(document).on('click','.btn_quickAddCat', function(event) {
            event.preventDefault();
            var $btn  = $(this);
            $.ajax({
                url: '{{route('admin.blog_categories.store') }}',
                data: $("#popup_form").serializeArray(),
                type:'POST',
                dataType:'json',
                beforeSend: function() {
                    $btn.button('loading');
                }
            }).done(function(output) {
                if (output.status == 'success') {
                    var indata = output.data;
                    $("#category_id").append('<option value="'+indata.id+'" selected="selected">'+indata.title_ar+'</option>');
                    $.magnificPopup.close();
                } else {
                    alert('Error occured');
                }
            }).fail(function() {
                alert('Error');
            }).always(function() {
                $btn.button('reset');
            });
        });
    </script>
@endsection