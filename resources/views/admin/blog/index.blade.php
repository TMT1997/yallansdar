<?php
    use App\Helper;
    use App\Blog;
    $categories = array_prepend($categories->toArray(), 'كل الأقسام','');
    $authors = array_prepend($authors->toArray(), 'كل الكتاب','');
?>
@extends('admin.layouts.master')
@section('title') المدونة | @endsection
@section('content')
    <section class="content-header">
        <h1 class="text-primary">المدونة
            <small>(العدد الكلي: {{$blog->count()}})</small>
            <a href="{{ url('admin/blog/create') }}" class="btn btn-success pull-left">+ اضف مقال جدبد</a>
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                {!!Form::open(['method'=>'GET','class'=>'form-inline form-filter'])!!}
                    <div class="form-group">
                        {!!Form::select('category_id', $categories, null, ['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::select('author_id', $authors, null, ['class'=>'form-control'])!!}
                    </div>
                {!!Form::close()!!}
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>رقم</th>
                            <th>إظهار</th>
                            <th>موضوع مختار</th>
                            <th>الصورة</th>
                            <th>القسم</th>
                            <th>عنوان المقال</th>
                            <th>المحتوى</th>
                            <th>مهام</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($blog as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{!!Helper::sw($item,'active','Blog')!!}</td>
                            <td>{!!Helper::sw($item,'featured','Blog')!!}</td>
                            <td>{!!Helper::image($item->image,['width'=>100])!!}</td>
                            <td>{{ $item->category->title_ar or '-' }}</td>
                            <td><a href="{{url('admin/blog/'.$item->id.'/edit')}}"><strong>{{Helper::field($item,'title')}}</strong></a></td>
                            <td>{{ str_limit(strip_tags(Helper::field($item,'details')), 100) }}</td>
                            <td nowrap="">
                                <a href="{{ url('admin/blog/'.$item->id) }}" class="btn btn-success btn-xs" title="View"><span class="fa fa-eye"/></a>
                                <a href="{{url('admin/blog/' .$item->id.'/edit')}}" class="btn btn-primary btn-xs" title="Edit"><span class="fa fa-pencil" /></a>
                                {!!Form::open(['method'=>'DELETE','url'=>['admin/blog',$item->id],'style'=>'display:inline'])!!}
                                {!!Form::button('<span class="fa fa-trash-o" title="Delete record" />', array(
                                    'type' => 'submit', 'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete', 'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection