<?php
use App\Review;
use App\User;
use App\Helper;
?>
@extends('admin.layouts.master')
{{--<style>--}}
    {{--.content-loader tr td {--}}
        {{--white-space: nowrap;--}}
    {{--}--}}
{{--</style>--}}
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">رسائل التواصل  بخصوص المنتجات</div>
        <div style="overflow: auto" class="panel-body">
            <br/>
            <div id="succ">

            </div>
            <table class="table table-bordered table-striped table-hover dataTable">
                <thead>
                <tr>
                    <th>رقم</th>

                    <th>من</th>
                    <th>إلى</th>
                    <th>الموضوع</th>
                    <th>الرسالة</th>
                    <th>الحالة </th>
                    {{--<th>المرفق </th>--}}
                    <th>التاريخ</th>
                    <th>مهام</th>
                </tr>
                </thead>
                <tbody>


                @foreach($messages as $item)
                    <tr>
                        <td>{{ $item->id }}</td>

                        <td>
                            @if($item->user_from)
                                {!!User::user($item, $item->user_from->name, 'from_user_id')!!}
                            @endif
                        </td>
                        <td>
                            @if($item->user_to)
                                {!!User::user($item, $item->user_to->name, 'to_user_id')!!}
                            @endif
                        </td>
                        <td>{!! $item->subject !!}</td>
                        <td >{!! $item->body !!}</td>
                        <td>{!!Helper::statusCtrl(\App\Messages\Message::$status,$item,'status','Messages\Message')!!}</td>
                        {{--<td>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-4" style="max-height:130px; overflow: hidden;">{!!Helper::image($item->attachments->file,['class'=>'img-responsive'])!!}</div>--}}
                        {{--{{$item->attachments}}--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        <td style="min-width: 100px">{{$item->created_at}}</td>
                        <td>
                            {{-- <a href="{{ url('/admin/reviews/' . $item->id) }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> --}}
                            {!! Form::open([
                                'method'=>'GET',
                                'url' => ['/admin/delete_message', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o"></i>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            //  check if the status changed
            $('.selector').on('change',function (e) {
                var status = ($(this).find("option:selected").val());
                var quote_id = $(this).data('id'); // get the deal id
                var test = $('#succ');
                $.ajax({
                    url:'{{url('admin/SendProducts_mail')}}',
                    type: 'POST',
                    data : {id : quote_id , status : status , _token:'{{csrf_token()}}' },
                    beforeSend: function () {
                        test.html('Loading.....')
                    }
                }).done(function (response) {
                    test.addClass('alert alert-success');
                    test.html(response);
                })
            })

        });
    </script>
@endsection