<yalla class="yalla_head">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar_yallaheader">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'))}}</a>
            </div>
            <div class="collapse navbar-collapse navbar_yallaheader">
                <p class="navbar-text navbar-left">
                    {{trans('front.welcometo')}}, 
                    @if (Auth::guest())
                        <a href="{{url('register')}}">{{trans('front.joinus')}}</a> |
                        <a href="{{url('login')}}">{{trans('front.signin')}}</a>
                    @else
                        {{Auth::user()->name}}, <a href="{{url('logout')}}">{{trans('front.logout')}}</a>
                    @endif
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$current_language->name}} <span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="buy-drp-mnu">
                            @foreach ($languages as $lang)
                                <?php $sel=(Session::get('yalla_locale')==$lang->iso2)?'selected="selected"':''; ?>
                                <li><a href="{{App\Language::switch_url($lang->iso2)}}" {{$sel}}>{{$lang->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                <a href="{{url('cart')}}" class="btn btn-default btn-xs navbar-right navbar-btn"><span class="fa fa-shopping-cart"></span> {{trans('front.cart')}} <span class="badge">{{App\Product::cart_items($cart)}}</span></a>
                <form action="{{url('search/products')}}" class="navbar-form navbar-right form-inline" role="search">
                    <div class="form-group">
                        <div class="input-group search_ctrl">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-search"></i></button>
                            </span>
                            <input type="text" name="q" class="form-control input-xs" placeholder="@lang('front.search')"></div>
                        </div>
                </form>
                <a href="{{url('dashboard/request_quotes/create')}}" class="btn btn-default btn-xs navbar-right navbar-btn">{{trans('front.post_buying_req')}}</a>
            </div>
        </div>
    </nav>
</yalla>