<yalla class="yalla_footer">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar_tempfooter">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar_tempfooter">
                <ul class="nav navbar-nav">
                    <li><a href="{{url('site')}}">{{trans('front.about_us')}}</a></li>
                    <li><a href="{{url('site/join-us')}}">{{trans('front.join_us')}}</a></li>
                    <li><a href="{{url('site/trade-manager')}}">{{trans('front.trade_manager')}}</a></li>
                    <li><a href="{{url('site/contact')}}">{{trans('front.contact_us')}}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right nav_terms">
                    <li><a href="#">{{trans('front.terms')}}</a></li>
                    <li><a href="#">{{trans('front.privacy_policy')}}</a></li>
                </ul>
                <p class="navbar-text navbar-right copyrights">{{trans('front.copyrights')}}</p>
            </div>
        </div>
    </nav>
</yalla>