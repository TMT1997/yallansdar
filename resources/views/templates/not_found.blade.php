@extends('front.layouts.master')

@section('content')
	
	<div class="alert alert-warning text-center">
		<h3>
			No website found, or it's not available now <br>
			عفوا لا يوجد متجر بهذه البيانات, <br>
			أو المتجر غير متاح حاليا
		</h3>
	</div>
@endsection