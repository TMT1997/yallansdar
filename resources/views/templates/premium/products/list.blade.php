<?php
    use App\Deals\Deal;
    use App\Helper;
    use App\Product;
    use App\Template;
    use App\UserWebsite;
    $cur = 'products';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') @lang('front.product_showcase') | @endsection
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{UserWebsite::url('/',$domain)}}">{{trans('front.home')}}</a></li>
        <li class="active">@lang('front.product_showcase')</li>
    </ol>

    <div class="col-md-3 pull-left">
        <div class="row">
            @include('templates.'.$template.'.range_search')
            @include('templates.'.$template.'.factsheet')
        </div>
    </div>
    <div class="col-md-9 pull-right">
        <div class="row">
            <div class="page_content">
                <div class="showcase">
                    <legend>@lang('front.product_showcase')</legend>
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-xs-12 col-sm-6 col-md-3 prod_item">
                                <div class="thumbnail">
                                    <a href="{{Template::product_url($product, $domain)}}">
                                        <div class="img_prod">{{Product::image($product)}}</div>
                                        <div class="caption">
                                            <span>{{Helper::field($product,'name')}}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">{!!$products->links()!!}</div>
                </div>
            </div>
            <div style="margin-left: 10px; ">
                @include('templates.'.$template.'.email_form')
            </div>
        </div>
    </div>
@endsection