<?php
	use App\User;
	use App\Category;
	use App\Product;
	use App\Company;
	use App\Helper;
	use App\UserWebsite;

	$cur = 'products';
	$parents = array_reverse($category_parents);
	$profile = (isset($product->user->profile)) ? $product->user->profile : null;
	$reviews = $product->reviews;
	$reviews_count = $reviews->count();

	if(isset($product->more_details)) {
		$more_details = unserialize($product->more_details);
	}
	$business_types = (isset($profile) && $profile->business_type) ? explode(',', $profile->business_type):'';
	$payment_type = ($product->payment_type) ? explode(',', $product->payment_type):'';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') {{Helper::field($product,'name')}} | @endsection
@section('content')
{{HTML::style('frontend/products_viewer/css/smoothproducts.css')}}
<ul class="breadcrumb">
  	<li><a href="{{UserWebsite::url('/',$domain)}}">{{trans('front.home')}}</a></li>
  	@if(!empty($parents))
	  	@foreach($parents as $cat)
			<li><a href="{{Category::url($cat)}}">{{@$cat->$c_name}}</a></li>
		@endforeach
	@endif
	<li class="active">{{Helper::field($product,'name')}}</li>
</ul>
<div class="col-md-3 pull-left">
    <div class="row">
        @include('templates.'.$template.'.range_search')
        @include('templates.'.$template.'.factsheet')
    </div>
</div>
<div class="col-md-9 pull-right">
	<div class="row">
		<div style="margin-left: 10px; background: #fff; padding:15px; margin-bottom: 20px;">
			<div class="row">
				<div class="col-xs-12 col-md-5">
					<div class="sp-loading">{{HTML::image('frontend/images/sp-loading.gif')}}<br>{{trans('front.loading_images')}}</div>
					<div class="sp-wrap">
						@if($product->image)
		            		<a href="{{url('uploads/'.$product->image)}}">{!!Helper::image2($product->image,[])!!}</a>
		            	@endif
		            	@foreach($product->images as $img)
		                	<a href="{{url('uploads/'.$img->image)}}">{!!Helper::image2($img->image)!!}</a>
		                @endforeach
					</div>
				</div>
				<div class="col-xs-12 col-md-7">
					<header><h1>{{Helper::field($product,'name')}}</h1></header>
					@if($reviews_count>0)
						{{trans('front.reviews')}} ({{$reviews_count}}): ({{App\Review::rate_stars($reviews)}}) 
						<a href="#details">{{trans('front.addreview')}}</a>
					@else
						<a href="#details">{{trans('front.befirst_to_review')}}</a>
					@endif

					<p class="nrml-p" style="margin-top: 10px;">
						{!!str_limit(Helper::field($product, 'description'), 350)!!}
					</p>
					<table class="table table-condensed">
						<tbody>
							<tr>
								<td style="width:140px">{{trans('front.fob_price')}}:</td>
								<td><a id="emailSupplink" href="#contact_supplier">{{trans('front.get_latest_price')}}</a></td>
							</tr>
							<tr>
								<td>{{trans('front.min_ord_quantity')}}:</td>
								<td>@if($product->min_quantity)
										{{$product->min_quantity}} {{trans('dash.units')[$product->min_quantity_unit]}}
									@endif
								</td>
							</tr>
							<tr>
								<td>{{trans('front.payment_terms')}}:</td>
								<td>
									@if($payment_type && is_array($payment_type))
										@foreach($payment_type as $type)
											{{trans('dash.payment_methods_list')[$type]}}, 
										@endforeach
									@endif
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="row">
						<div class="col-md-6">
							<span>{{trans('front.share_us_on')}}:</span>
							<div class="addthis_inline_share_toolbox"></div>
						</div>
						<div class="col-md-6 text-right reqBtn">
							<br>
							{!!Form::open(['url'=>'cart'])!!}
		                    	{!!Form::hidden('product_id', $product->id)!!}
		                    	{!!Form::submit(trans('front.request_for_quote'), ['class'=>'btn btn-success'])!!}
		                	{!!Form::close()!!}
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-12">
							<a href="{{url('product/addtofavorites/'.$product->id)}}" class="popup btn btn-sm"><i class="fa fa-heart"></i> {{trans('front.add_to_favorites')}}</a>
							|
							<a href="javascript:addToCart({{$product->id}})" data-id="{{$product->id}}" class="btn btn-sm"><i class="fa fa-shopping-cart"></i> {{trans('front.add_to_enq_cart')}}</a>
							|
							<a href="{{Product::print_url($product)}}" class="btn btn-sm" target="_blank"><i class="fa fa-print"></i> {{trans('front.print')}}</a>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<a name="details"></a>
					<div role="tabpanel">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#home" aria-controls="home" role="tab" data-toggle="tab">{{trans('front.product_details')}}</a>
							</li>
							<li role="presentation">
								<a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">{{trans('front.reviews')}} ({{$product->reviews->count()}})</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="home">
								<div class="product_details" style="padding:15px;">
									<div class="row">
										<div class="col-md-12">{!!Helper::field($product,'details')!!}</div>
									</div>
								  	<div class="row">
								  		<div class="col-md-2 opt attr">{{trans('front.product_manufactured_in')}}:</div>
								  		<div class="col-md-2 opt">{{App\Country::cname($product->manufactured_in)}}</div>
								  	</div>
								  	<div class="row">
								  		<div class="col-md-2 opt attr">{{trans('front.supply_ability')}}:</div>
								  		<div class="col-md-2 opt">{{Product::capacity($product)}}</div>
								  	</div>
								  	<div class="row">
								  		@if(is_array($more_details['attr']))
									  		@foreach($more_details['attr'] as $index=>$attr)
										  		<div class="col-md-2 opt attr">{{$attr}}:</div>
										  		<div class="col-md-2 opt">{{$more_details['value'][$index]}}</div>
										  	@endforeach
										@endif
								  	</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="reviews">
								<div class="product_reviews" style="padding:15px;">
									<a name="add_review"></a>
									@if($reviews_count>0)
										@include('front.reviews.list')
									@else
										{!!trans('front.review_intro')!!}
									@endif
									<a href="{{url('reviews/add/'.$product->id.'/product')}}" class="btn btn-primary popup">{{trans('front.review_now')}}</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a name="contact_supplier"></a>
				<div class="clearfix"></div>
				<hr>
				<div class="col-md-12">
					<legend>{{trans('front.more_prods_from_supplier')}}</legend>
					<div class="products_slider">
						@foreach($supplier_products as $product)
					        <div class="item more_prod_item">
				                <div class="thumbnail">
			                        <a href="{{App\Template::product_url($product, $domain)}}">
			                            {{Product::image($product)}}
			                            <div class="caption">
			                                <span>{{Helper::field($product,'name')}}</span>
			                            </div>
			                        </a>
			                    </div>
					        </div>
					    @endforeach
				    </div>
				</div>
			</div>
		</div>
		<div style="margin-top: 20px; margin-left: 10px; ">
            @include('templates.'.$template.'.email_form')
        </div>
	</div>
</div>
@endsection

@section('script')
	{{HTML::script('frontend/products_viewer/js/smoothproducts.min.js')}}
	<script type="text/javascript">
		$(window).load(function() {
	        $('.sp-wrap').smoothproducts();
	    });
	    $('.products_slider').owlCarousel({     
	         /*rtl:true,*/
	         lazyLoad : true,
	         nav : true,
	         items : 5,
	         navText: ["",""],
	         dots:true,
	         loop: true,
	         scrollPerPage : true,
	         rewindNav : true,
	         responsive:{
	            320: { items:1 },
	            768: { items:3 },
	            1024: { items:5 }
	         }
	      });
	</script>
	@include('dashboard.layouts.popup')
@endsection