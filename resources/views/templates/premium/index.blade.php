<?php
    use App\Helper;
    use App\Product;
    use App\Template;
    $home=true;
    $cur = '/';
?>
@extends('templates.'.$template.'.layouts.master')

@section('content')
    <div class="panel panel-primary hp_showcase">
        <div class="panel-heading">
            <h3 class="panel-title">@lang('front.product_showcase')</h3>
        </div>
        <div class="panel-body">
            @foreach($products as $product)
                <div class="col-xs-12 col-sm-6 col-md-3 prod_item">
                    <div class="thumbnail">
                        <a href="{{Template::product_url($product, $domain)}}">
                            <div class="img_prod">{{Product::image($product)}}</div>
                            <div class="caption">
                                <span>{{Helper::field($product,'name')}}</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="{{App\UserWebsite::url('products',$domain)}}">@lang('front.view_all')</a>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-primary hp_newproducts">
        <div class="panel-heading">
            <h3 class="panel-title">@lang('front.new_products')</h3>
        </div>
        <div class="panel-body">
            @foreach($products->take(4) as $product)
                <div class="col-xs-12 col-sm-6 col-md-3 prod_item">
                    <div class="thumbnail">
                        <a href="{{Template::product_url($product, $domain)}}">
                            <div class="img_prod">{{Product::image($product)}}</div>
                            <div class="caption">
                                <span>{{Helper::field($product,'name')}}</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script')

@endsection