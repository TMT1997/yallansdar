<?php use App\User; ?>
<div class="panel panel-primary email_supplier_wrap">
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-envelope"></i> {{trans('front.email_to_supplier')}}</h3>
	</div>
	<div class="panel-body">
		{!!Form::open(['class'=>'form-horizontal', 'method'=>'POST', 'url'=>'store/'.$domain.'/contact'])!!}
			<div class="form-group">
				<div class="col-md-6">
					{!!Form::label('name',trans('front.name'), ['class'=>'control-label'])!!}
					{!!Form::text('name',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_name'),'required'=>'required'])!!}
				</div>
				<div class="col-md-6">
					{!!Form::label('email',trans('front.email'), ['class'=>'control-label'])!!}
					{!!Form::text('email',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_email'),'required'=>'required'])!!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6">
					{!!Form::label('country',trans('front.country'), ['class'=>'control-label'])!!}
					{!!Form::select('country',$countries->lists('name','id'),$selected_country,['class'=>'form-control'])!!}
				</div>
				<div class="col-md-6">
					{!!Form::label('mobile',trans('front.mobile'), ['class'=>'control-label'])!!}
					{!!Form::text('mobile',null,['class'=>'form-control','placeholder'=>trans('front.ent_ur_mobile'),'required'=>'required'])!!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					{!!Form::label('to',trans('front.send_to'), ['class'=>'control-label'])!!}
					<a href="{{User::url_contact($user)}}" class="popup">
						{{$user->name}}	<strong>({{$user->profile->company_name}})</strong>
					</a>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					{!!Form::label('description',trans('front.description'), ['class'=>'control-label'])!!}
					{!!Form::textarea('description',null,['class'=>'form-control','rows'=>'4','required'=>'required','placeholder'=>''])!!}
				</div>
			</div>
			<div class="form-group submit_group">
				<div class="col-md-6">
					{!!Form::submit(trans('front.send_enquiry'), ['class'=>'btn btn-loading btn-primary'])!!}
				</div>
			</div>
		{!!Form::close()!!}
	</div>
</div>