<?php
    use App\Deals\Deal;
    use App\Helper; 
    use App\UserWebsite;
    $cur = 'contact';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') @lang('front.contact_us') | @endsection
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{UserWebsite::url('/',$domain)}}">@lang('front.home')</a></li>
        <li class="active">@lang('front.contact_us')</li>
    </ol>

    <div class="col-md-3 pull-left">
        <div class="row">
        @include('templates.'.$template.'.factsheet')
        </div>
    </div>

    <div class="col-md-9 pull-right">
        <div class="row">
            <div class="page_content">
                <legend>@lang('front.contact_us')</legend>

                <div class="section">
                    <h3>@lang('front.contact_information')</h3>
                    <div class="media">
                        <a class="pull-left" href="#">
                            {!!Helper::image2($user_profile->picture, ['class'=>'media-object', 'style'=>'width:120px;'])!!}
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{$user_profile->first_name}} {{$user_profile->last_name}}</h4>
                            <p>{{$user_profile->destination}}</p>
                            <p>
                                @if(isset(trans('dash.departments_arr')[$user_profile->department]))
                                    <span>{{trans('front.department')}}</span>: {{trans('dash.departments_arr')[$user_profile->department]}}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>

                <div class="section company_info">
                    <h3>@lang('front.company_information')</h3>
                    <div class="row">
                        <div class="col-md-4">@lang('front.company_name'):</div>
                        <div class="col-md-7">{{$user_profile->company_name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@lang('front.address'):</div>
                        <div class="col-md-7">{{$user_profile->co_address_line1}} {{$user_profile->co_address_line2}}</div>
                    </div>
                    @if(!empty($user_profile->co_zipcode))
                    <div class="row">
                        <div class="col-md-4">@lang('front.zipcode')::</div>
                        <div class="col-md-7">{{$user_profile->co_zipcode}}</div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-4">@lang('front.city'):</div>
                        <div class="col-md-7">{{$user_profile->co_city}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@lang('front.state'):</div>
                        <div class="col-md-7">{{$user_profile->co_state}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@lang('front.country'):</div>
                        <div class="col-md-7">{{$user_profile->country->country_name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@lang('front.mobile'):</div>
                        <div class="col-md-7">{{Deal::get_phone($user, 'mobile', 'mod_code')}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">@lang('front.phone'):</div>
                        <div class="col-md-7">{{Deal::get_phone($user)}}</div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 20px; margin-left: 10px; ">
                @include('templates.'.$template.'.email_form')
            </div>
        </div>
    </div>
</div>
@endsection