<?php
    use App\UserWebsite;
    use App\Settings;
    use App\UserTradeInfo;                    
    $cur = 'profile';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') @lang('front.company_info') | @endsection
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{UserWebsite::url('/',$domain)}}">@lang('front.home')</a></li>
        <li class="active">@lang('front.company_info')</li>
    </ol>

    <div class="col-md-3 pull-left">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('front.company_overview')</h3>
                </div>
                <div class="panel-body">
                    <div class="list-group">
                        @foreach(trans('front.profile_nav') as $url=>$nav)
                            <a href="{{App\UserWebsite::url($url,$domain)}}" class="list-group-item">{{$nav}}</a>
                        @endforeach
                    </div>
                </div>
            </div>

            @include('templates.'.$template.'.factsheet')
        </div>
    </div>
    <div class="col-md-9 pull-right">
        <div class="row">
            <div class="page_content">
                <legend>@lang('dash.trading_info')</legend>
                @if($page)
                    <legend>{{$page->title}}</legend>
                    {!!$page->details!!}
                @endif
                <br>
            @if($trade)
                {!!Form::model($trade, ['class'=>'form-horizontal','url'=>'/','method'=>'POST','id'=>'tradeInfoForm'])!!}
                <?php $field = 'export_percentage'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8">
                        {{trans('dash.export_percentages')[$trade->$field]}}
                    </div>
                </div>

                <?php $field = 'market_region'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.main_markets'), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8">
                        @if($trade->market_region=='Region')
                            {{$trade->main_markets}}
                        @elseif($trade->market_region=='Country')
                            @foreach($trade->region_countries as $c_id)
                                {{@$countries->where('id',$c_id)->first()->name}}
                            @endforeach
                        @endif
                    </div>
                </div>

                <?php $field = 'nearest_port'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8">
                        @foreach($trade->nearest_port as $port)
                            <a href="javascript:void(0)" class="btn btn-sm btn-default">{{$port}}</a>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">{{trans('dash.avg_lead_time')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <?php $field = 'avg_lead_time'; ?>
                            <div class="col-md-6">
                                {!!Form::label($field, trans('dash.sample_lead_time'),['class'=>'control-label']) !!}
                                <br>
                                {{$trade->$field}}
                                {{@trans('dash.avg_lead_duration')[$trade->avg_lead_time_dur]}}
                            </div>
                            <?php $field = 'ord_delivery_time'; ?>
                            <div class="col-md-6">
                                {!! Form::label($field, trans('dash.'.$field), ['class' => 'control-label']) !!}
                                <br>
                                {{$trade->$field}}
                                {{@trans('dash.avg_lead_duration')[$trade->ord_delivery_time_dur]}}
                            </div>
                        </div>
                    </div>
                </div>

                @if($trade->overseas_office==1)
                <div class="form-group">
                    <label class="col-md-4 control-label">{{trans('dash.overseas_office_addr')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            @for($i=1;$i<=2;$i++)
                                <div class="col-md-6">
                                    <?php $field = 'office_name'.$i; ?>
                                    <div class="form-group">
                                        {!! Form::label($field, trans('dash.office_name'), ['class' => 'col-md-4 control-label']) !!}
                                        <div class="col-md-8 form-inline">
                                            {{$trade->$field}}
                                        </div>
                                    </div>
                                    <?php $field = 'office_country'.$i; ?>
                                    <div class="form-group">
                                        {!! Form::label($field, trans('dash.country'), ['class'=>'col-md-4 control-label']) !!}
                                        <div class="col-md-8 form-inline">
                                            {{@$countries->where('id',$trade->$field)->first()->name}}
                                        </div>
                                    </div>
                                    <?php $field = 'office_city'.$i; ?>
                                    <div class="form-group">
                                        {!! Form::label($field, trans('dash.city2'), ['class' => 'col-md-4 control-label']) !!}
                                        <div class="col-md-8 form-inline">{{$trade->$field}}</div>
                                    </div>
                                    <?php $field = 'office_zipcode'.$i; ?>
                                    <div class="form-group">
                                        {!!Form::label($field,trans('dash.zipcode'),['class'=>'col-md-4 control-label'])!!}
                                        <div class="col-md-8 form-inline">{{$trade->$field}}</div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
                @endif

                <?php $field = 'no_trade_staff'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        {{trans('dash.no_trade_staffs')[$trade->$field]}}
                    </div>
                </div>

                <?php $field = 'minimum_order_value'; ?>
                @if(!empty($trade->$field))
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        {{@trans('dash.minimum_order_values')[$trade->$field]}}
                    </div>
                </div>
                @endif

                <?php $field = 'shipment_mode'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        <div class="row"> 
                        @foreach($trade->shipment_mode as $val)
                            <a href="javascript:void(0)" class="btn btn-sm btn-default">{{$val}}</a>
                        @endforeach
                        </div>
                    </div>
                </div>

                <?php $field = 'accepted_delivery_terms'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        <div class="row"> 
                            @foreach($trade->accepted_delivery_terms as $val)
                                <a href="javascript:void(0)" class="btn btn-sm btn-default">{{$val}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <?php $field = 'accepted_payment_currency'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        <div class="row"> 
                        @foreach($trade->accepted_payment_currency as $val)
                            <a href="javascript:void(0)" class="btn btn-sm btn-default">{{$val}}</a>
                        @endforeach
                        </div>
                    </div>
                </div>

                <?php $field = 'accepted_payment_type'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.payment_terms'), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        <div class="row"> 
                        @foreach($trade->accepted_payment_type as $val)
                            <a href="javascript:void(0)" class="btn btn-sm btn-default">{{trans('dash.payment_methods_list')[$val]}}</a>
                        @endforeach
                        </div>
                    </div>
                </div>

                <?php $field = 'payment_term'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        {{$trade->$field}}
                    </div>
                </div>

                <?php $field = 'language_spoken'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8 form-inline">
                        <div class="row"> 
                        @foreach($trade->language_spoken as $val)
                            <a href="javascript:void(0)" class="btn btn-sm btn-default">{{trans('dash.languages_spoken')[$val]}}</a>
                        @endforeach
                        </div>
                    </div>
                </div>

                <?php $field = 'trade_memberships'; ?>
                <div class="form-group">
                    {!! Form::label($field, trans('dash.'.$field), ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-8">
                        {!!$trade->$field!!}
                    </div>
                </div>
            @endif
            </div>
            <div style="margin-top: 20px; margin-left: 10px;">
                @include('templates.'.$template.'.email_form')
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection