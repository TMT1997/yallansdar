<?php
    use App\UserWebsite;
    $cur = 'profile';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') @lang('front.company_info') | @endsection
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{UserWebsite::url('/',$domain)}}">@lang('front.home')</a></li>
        <li class="active">@lang('front.company_info')</li>
    </ol>

    <div class="col-md-3 pull-left">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('front.company_overview')</h3>
                </div>
                <div class="panel-body">
                    <div class="list-group">
                        @foreach(trans('front.profile_nav') as $url=>$nav)
                            <a href="{{App\UserWebsite::url($url,$domain)}}" class="list-group-item">{{$nav}}</a>
                        @endforeach
                    </div>
                </div>
            </div>

            @include('templates.'.$template.'.factsheet')
        </div>
    </div>
    <div class="col-md-9 pull-right">
        <div class="row">
            <div class="page_content">
                @if($page)
                    <legend>{{$page->title}}</legend>
                    {!!$page->details!!}
                @else
                    No contents found
                @endif
                <br>
            </div>

            <div style="margin-top: 20px; margin-left: 10px;">
                @include('templates.'.$template.'.email_form')
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection