<?php
    use App\UserWebsite;
    $cur = 'contact';
?>
@extends('templates.'.$template.'.layouts.master')
@section('title') @lang('front.contact_us') | @endsection
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{UserWebsite::url('/',$domain)}}">@lang('front.home')</a></li>
        <li class="active">@lang('front.contact_us')</li>
    </ol>

    @include('templates.'.$template.'.email_form')
@endsection
