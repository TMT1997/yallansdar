<?php
    use App\UserWebsite;
    use App\Helper;
    $page_banner = $banners->filter(function($row) use ($cur) {
        return $row->title==$cur;
    })->first();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title') {{$user_profile->company_name}}</title>
    {{HTML::style("frontend/css/bootstrap.min.css")}}
    @if(App::isLocale('ar'))
        {{HTML::style('backend/dist/css/bootstrap-rtl.min.css')}}
    @endif
	{{HTML::style("frontend/css/font-awesome.min.css")}}
    {{HTML::style("frontend/templates/".$templatecss."/style2.css")}}
    {{HTML::style("frontend/templates/skins/".$skin.".css")}}
    {{HTML::style("frontend/css/owl.carousel.css")}}
    {{HTML::style("frontend/css/owl.theme.css")}}

    @if(App::isLocale('ar'))
        {{HTML::style("frontend/templates/".$templatecss."/rtl.css")}}
    @endif
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<a href="#" class="to-tp cd-top"><span class="fa fa-chevron-up"></span></a>
@include('templates.common.yalla_header')

@if(Request::has('preview') && Request::get('preview')==1)
    <div class="alert alert-warning text-center">Preview Mode</div>
@endif

<div class="_container" style="width:980px; margin:10px auto">
    <header class="temphead">
        <i class="ribbon {{($website->template->is_free)?'free':'premium'}}_ribbon"></i>
        <div class="">
    	    <nav class="navbar mainnav" role="navigation" style="margin-bottom: 0;">
                <div class="navbar-header hidden">
	               <div class="_container">
    	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_main">
    	                    <span class="sr-only">Toggle navigation</span>
    	                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
    	                </button>
    	                <a class="navbar-brand" href="{{UserWebsite::url('/',$domain)}}">{{$user_profile->company_name}}</a>
                    </div>
	            </div>
                <div class="collapse navbar-collapse navbar_items" id="navbar_main">
                   <div class="_container">
    	                <ul class="nav navbar-nav">
    	                	@foreach(trans('front.site_mainmenu') as $url=>$label)
                                <?php $active = (isset($cur) && $cur==$url)?'active':''; ?>
    	                    	<li class="{{$active}}"><a href="{{UserWebsite::url($url,$domain)}}">{{$label}}</a></li>
    	                    @endforeach
    	                </ul>
    	            </div>
                </div>
    	    </nav>

            @if(isset($home))
                <div class="row carousel-holder" style="margin-bottom: 0">
                    <div class="col-md-12">
                        <div id="header_slider" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php $count=0; ?>
                                @foreach($slider as $slide)
                                    <li data-target="#header_slider" data-slide-to="{{$count}}" class="{{($count==0)?'active':''}}"></li>
                                    <?php $count++; ?>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                <?php $count=0; ?>
                                @foreach($slider as $slide)
                                    <div class="item {{($count==0)?'active':''}}">
                                        @if(($slide->default))
                                            <img class="slide-image" src="{{url('frontend/templates/images/slider/'.$slide->image)}}">
                                        @else
                                            {!! Helper::image2($slide->image, ['class'=>'slide-image']) !!}
                                        @endif
                                    </div>
                                    <?php $count++; ?>
                                @endforeach
                            </div>
                            <a class="left carousel-control" href="#header_slider" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#header_slider" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            @else
                <div class="head_banner">
                    @if($page_banner)
                        @if(($page_banner->default))
                            <img class="slide-image" src="{{url('frontend/templates/images/slider/'.$page_banner->image)}}">
                        @else
                            {!! Helper::image2($page_banner->image, ['class'=>'slide-image']) !!}
                        @endif
                    @else
                        <img class="slide-image" src="http://placehold.it/800x150" alt="">
                    @endif                        
                </div>
            @endif
            <div class="brand">
                <div class="logo">
                    <div class="logo_img">
                        @if(!empty($user_profile->co_logo))
                            {!!HTML::image('uploads/'.$user_profile->co_logo,'',[])!!}
                        @elseif(!empty($user_profile->picture))
                            {!!HTML::image('uploads/'.$user_profile->picture,'',[])!!}
                        @endif
                    </div>
                    <div class="logo_company">
                        <a class="" href="{{UserWebsite::url('/',$domain)}}">
                            <h1>{{$user_profile->company_name}}</h1>
                        </a>
                    </div>
                </div>

                @if(!isset($home))
                <div class="social_nav">
                    @foreach(App\UserProfile::$social as $field=>$label)
                        @if(!empty($user_profile->$field))
                            <a href="{{$user_profile->$field}}" target="_blank" class="btn btn-default i_{{$field}}"><i class="fa fa-{{$field}}"></i></a>
                        @endif
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </header>
</div>

<div class="container-fluid">
    <div class="_container" style="width: 980px;position: relative;margin: 0 auto; padding: 10px 0;">
        <div class="col-md-12">
          @if(Session::has('flash_message'))
              <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!!Session::get('flash_message')!!}
              </div>
          @endif
        </div>

        @if(isset($home) && $home==true)
            @include('templates.'.$template.'.layouts.home_content')
        @else
            @yield('content')
        @endif
    </div>
</div>

@include('templates.common.yalla_footer')

{{HTML::script('frontend/js/jquery-1.11.2.min.js')}}
{{HTML::script('frontend/js/bootstrap.min.js')}}
{{HTML::script('frontend/js/owl.carousel.min.js')}}
{{HTML::script('frontend/js/back-to-top.js')}}
@yield('script')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58d27689eade3904"></script> 
</body></html>