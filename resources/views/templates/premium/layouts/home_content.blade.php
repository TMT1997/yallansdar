<div class="col-md-4 sidebar">
    <div class="hp_sidebar">
        @include('templates.'.$template.'.factsheet')
        @include('templates.'.$template.'.range_search')
        @include('templates.'.$template.'.includes.side_banners')
    </div>
</div>
<div class="col-md-8 main_contents">
    <div class="row">
        @yield('content')
        @include('templates.'.$template.'.email_form')
    </div>
</div>