<?php
	$profile = $user->profile;
	$business_types = (isset($profile) && $profile->business_type) ? explode(',', $profile->business_type):'';	
?>
<div class="sheet_paper">
	<div class="factsheet">
		<legend>@lang('front.factsheet')</legend>
		<div class="verifiedStatus">
			{!!App\User::isVerified($user, true)!!}
		</div>
		<div class="block">
			<strong>@lang('dash.location'):</strong>
			<div class="help-block">, {{$profile->state}}, {{$profile->city}}</div>
		</div>
		<div class="block">
			<strong>@lang('dash.business_type'):</strong>
			<div class="help-block">
				@if($business_types && is_array($business_types))
					@foreach($business_types as $type)
						{{trans('dash.business_types')[$type]}}, 
					@endforeach
				@endif
			</div>
		</div>
		<div class="block">
			<strong>@lang('dash.Turnover'):</strong>
			<div class="help-block">
				{{trans('dash.turnovers')[$profile->turnover]}}
			</div>		
		</div>
		<div class="block">
			<strong>@lang('dash.main_products'):</strong>
			<div class="help-block">{{$profile->main_products}}</div>
		</div>
		<div class="block">
			<strong>@lang('dash.reviews'):</strong>
			<div class="help-block">@lang('front.befirst_to_review')</div>
		</div>
	</div>

	<div class="contact_suplier">
		<a href="{{App\UserWebsite::url('contact-supplier',$domain)}}" class="btn btn-primary">
			<i class="fa fa-envelope"></i> @lang('front.contact_supplier')</a>
	</div>
</div>