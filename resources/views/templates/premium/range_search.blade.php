<div class="search">
    {!!Form::open(['method'=>'GET', 'url'=>App\UserWebsite::url('search',$domain)])!!}
    <div class="input-group">
        <input type="text" name="q" class="form-control" id="search_q" placeholder="@lang('front.search')">
        <span class="input-group-btn">
            <button type="button" class="btn btn-primary btn_search"><i class="fa fa-search"></i></button>
        </span>
    </div>
    {!!Form::close()!!}
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">@lang('front.product_range')</h3>
    </div>
    <div class="panel-body">
        <div class="list-group">
            @foreach($top_categories as $cat)
                <a href="#" class="list-group-item">{{$cat->name_en}}</a>
            @endforeach
        </div>
    </div>
</div>