<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Yalla Nsadar</title>
	{!!HTML::style("frontend/css/bootstrap.min.css")!!}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		.navbar-brand img {max-height: 70px;}
		.navbar-nav>li>a{line-height: 60px;}
	</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<a class="navbar-brand" href="{{url('/')}}">{!!HTML::image('frontend/images/logo/logo-en.png')!!}</a>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('/')}}">@lang('front.home')</a></li>
				<li><a href="{{url('site')}}">@lang('front.about_us')</a></li>
				<li><a href="{{url('site/contact')}}">@lang('front.contact_us')</a></li>
				@if(Auth::check())
					<li class="dropdown">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		                    {{Auth::user()->name}} <span class="caret"></span>
		                </a>
		                <ul class="dropdown-menu" role="menu">
		                    <li><a href="{{url('dashboard/profile')}}"><i class="fa fa-user"></i> @lang('dash.my_profile')</a></li>
		                    <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> @lang('front.logout')</a></li>
		                </ul>
		            </li>
		        @else
		        	<li><a href="{{url('login')}}">@lang('front.login')</a></li>
		        @endif
			</ul>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="container">
			<div class="row">
		      <div class="col-md-12">
		        @if(Session::has('flash_message'))
		            <div class="alert alert-{{Session::get('alert','success')}}" style="margin-bottom: 0">
		              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		              {!!Session::get('flash_message')!!}
		            </div>
		        @endif
		      </div>
		    </div>

			@yield('content')
		</div>
	</div>
{!!HTML::script('frontend/js/jquery-1.11.2.min.js')!!}
{!!HTML::script('frontend/js/bootstrap.min.js')!!}
</body></html>