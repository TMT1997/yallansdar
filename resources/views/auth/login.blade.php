@extends('layouts.app')
@section('title') @lang('front.login') | @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="login_block">
            <div class="panel section_wrap">
                <div class="panel-heading text-center">
                    <a href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
                </div>
                <div class="panel-body text-center">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('front.email')">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="@lang('front.password')">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{$errors->first('password')}}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> @lang('front.remember_me')</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <button type="submit" class="btn btn-success btn-lg btn-loading form-control">
                                     @lang('front.login')
                                </button>
                                <br>
                                {{--disable facebook btn --}}
                                {{--<div class="text-center fb_login">--}}
                                    {{--<p>@lang('front.or')</p>--}}
                                    {{--<a href="{{url('redirect')}}" class="btn btn-primary btn-fb form-control">@lang('front.facebook_login') <i class="fa fa-facebook-square fa-2x"></i></a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </form>
                    <div class="row text-center">
                        <div class="col-md-12">
                            @lang('front.not_amember') <a href="{{url('register')}}">@lang('front.register_now')</a>
                            <br>
                            <a class="btn btn-link" href="{{ url('/password/reset') }}">@lang('front.forget_ur_pass')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection