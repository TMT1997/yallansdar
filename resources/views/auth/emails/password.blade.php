
<div style="width:100%; background:#e9e9e9; padding:15px 0; direction: rtl; font-size: 16px;">

	<div style="width:70%; margin:20px auto; text-align: center;">
		<a href="{{url('/')}}" target="_blank"><img src="{{url('frontend/images/logo/logo-ar.png')}}" style="border:none; max-width: 100%;" /></a>
	</div>

	<div style="width: 70%;
    color: white;
    background: #00509f;
    margin: 20px auto;
    padding: 20px;
    text-align: center;
    border: 10px solid white;
    border-radius: 10px;
    font-family: sans-serif, arial;
    font-size: 20px;">
		
		<br><br>
		{{trans('front.click_here_to_reset_your_password')}}:
		<br>

		<p style="font-size:20px; color: #0069FF; text-align: center; direction: ltr;background-color:#F8A605;">
		<a style="text-decoration: none;" 
    	href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> Update My Password</a>
		</p>

		<br><br>
		<strong>شكرا  </strong><br>
		فريق يلا نصدر <br>
		<a href="{{url('/')}}" style="color:#F8A605;">www.yallansadar.com</a>
	</div>

	<div style="margin-top: 20px; text-align: center; font-family: sans-serif; color: #999; font-size: 13px;">
		<div style="margin:0 auto; width:70%;font-size: 20px;color: #4b4a4a;">
			&copy; 2017 - جميع الحقوق محفوظة لدى موقع يلانصدر
		</div>
	</div>
</div>
