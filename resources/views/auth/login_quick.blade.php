<div class="white-popup-block">
    <div class="row">
        <div class="login_block">
            <div class="panel section_wrap">
                <div class="panel-heading text-center">
                    <a href="{{url('home')}}">{{HTML::image('frontend/images/logo/'.trans('front.logo'),'Yalla Nsadar')}}</a>
                </div>
                <div class="panel-body text-center">
                    <div id="login_msg"></div>
                    {{Form::open(['class'=>'form-horizontal quicklogin_form', 'method'=>'POST', 'url'=>'/login'])}}
                        {!!Form::hidden('ref', Request::get('ref'))!!}
                        {!!Form::hidden('next', Request::get('next'))!!}
                        {!!Form::hidden('id', Request::get('id'))!!}
                        {!!Form::hidden('unid', Request::get('unid'))!!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-10 col-md-offset-1"">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o"></i></span>
                                    <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" placeholder="@lang('front.email')">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password')?' has-error':'' }}">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
                                    <input type="password" id="password" class="form-control" name="password" placeholder="@lang('front.password')">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{$errors->first('password')}}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <button type="submit" class="btn btn-success logbtn form-control">@lang('front.login')</button>
                                {{--<br>--}}
                                {{--<div class="text-center fb_login">--}}
                                    {{--<p>{{trans('front.or')}}</p>--}}
                                    {{--<a href="{{url('redirect')}}" class="btn btn-primary btn-fb form-control">@lang('front.facebook_login') <i class="fa fa-facebook-square"></i></a>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>