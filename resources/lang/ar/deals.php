<?php

return array(
	'brands'=>'المركات',
	'description'=>'الوصف',
	'shipping_terms'=>'شروط الشحن',
	'brands_ifany'=>'Brand (If any)',
	'short_desc'=>'Short Description about your service (maximum of 250 characters )',
	'shipping_terms_help'=>'Please update the Shipping terms & conditions',
	'type_offer'=>'Type of Offer',
	'related_images'=>'Related Images',
	'edit_offer'=>'Edit Offer',
	'edit_partnership'=>'Edit Partnership Deal',
	'edit_quote_request'=>'Edit Quote Request',
	'deal_alerts'=>'تنبيهات الصفقات',
	'deal_alert'=>'تنبيه',
	'details'=>'تفاصيل',
);