<?php

return array(
	'brands'=>'Brands',
	'brands_ifany'=>'Brand (If any)',
	'description'=>'Offer Description',
	'short_desc'=>'Short Description about your service (maximum of 250 characters )',
	'shipping_terms'=>'Shipping Terms',
	'shipping_terms_help'=>'Please update the Shipping terms & conditions',
	'type_offer'=>'Type of Offer',
	'related_images'=>'Related Images',
	'edit_offer'=>'Edit Offer',
	'edit_partnership'=>'Edit Partnership Deal',
	'edit_quote_request'=>'Edit Quote Request',
	'deal_alerts'=>'Deal Alerts',
	'deal_alert'=>'Deal Alert',
	'details'=>'Details',
);