<?php

Route::get('pack_items',function (){
    return \App\Packages\Package::packages_with_items();
});
//for test the api
Route::get('test',function (){
    return view('dashboard.website.steps.upgrade');
});
Route::auth();
Route::get('clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'cache cleared';
});
Route::get('country_collection', function() {
    $countries = Cache::get('countries');
    $cid = 60;
    return $countries->where('id',$cid)->first()->name;
});
Route::get('mail_tester', function() {

    $send_to = 'marwan.nabil.mohamed@gmail.com';
    $data = array('name'=>"Yalla Tester", "body" => "This is a Test mail", 'email'=>$send_to);

    Mail::send('front.emails.tester_mail', $data, function($message) use ($send_to) {
        $message->to($send_to, 'Tester mail')
            ->subject('Yalla Testing Mail');
        $message->from('yallansadar@gmail.com','Yallansadar');
    });

    return 'Mail should be sent to; ' .$send_to;
});
Route::get('slugging', function() {
    //Make slug strings for Request for Quotes
    $rfq = \App\Deals\QuoteRequest::all();
    foreach ($rfq as $row)
    {
        $row->slug = \App\Helper::generate_slug('\App\Deals\QuoteRequest', $row->service_product_name, $row->id);
        if($row->save()) {
            print($row->service_product_name.': <strong>('.$row->slug.')</strong> Done <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('rfq_lang', function() {
    //Make slug strings for Request for Quotes
    $rfq = \App\Deals\QuoteRequest::with('lang')->get();
    foreach ($rfq as $row)
    {
        $row->details_en = $row->lang->where('language_id', 1)->first()->details;
        $row->details_ar = $row->lang->where('language_id', 2)->first()->details;
        if($row->save()) {
            print($row->service_product_name.' lang updated <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('rfq_names_lang', function() {
    //Make slug strings for Request for Quotes
    $rfq = \App\Deals\QuoteRequest::all();
    foreach ($rfq as $row)
    {
        $row->name_ar = $row->service_product_name;
        $row->name_en = $row->service_product_name;
        if($row->save()) {
            print($row->service_product_name.' lang updated <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('set_parent_cats_rfq', function() {
    //Get Parent Category and save for RFQ
    $rfq = \App\Deals\QuoteRequest::all();
    foreach ($rfq as $row)
    {
        $cats = App\Category::get_parents($row->category_id);
        if($cats && count($cats) > 0){
            foreach($cats as $cat) {
                if(!$cat){
                    continue;
                }
                if($cat->parent_category_id==0) {
                    $row->parent_cat_id = $cat->id;
                    break;
                }
            }

            if($row->save()) {
                print($row->service_product_name.': updated with cat '.$row->parent_cat_id.'<br>');
            }
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('transfer_product_lang', function() {
    $products = \App\Product::with('lang')->get();
    foreach ($products as $row)
    {
        $row->name_en = @$row->lang->where('language_id', 1)->first()->name;
        $row->name_ar = @$row->lang->where('language_id', 2)->first()->name;
        $row->description_en = @$row->lang->where('language_id', 1)->first()->description;
        $row->description_ar = @$row->lang->where('language_id', 2)->first()->description;
        $row->details_en = @$row->lang->where('language_id', 1)->first()->details;
        $row->details_ar = @$row->lang->where('language_id', 2)->first()->details;
        $row->keywords_en = @$row->lang->where('language_id', 1)->first()->keywords;
        $row->keywords_ar = @$row->lang->where('language_id', 2)->first()->keywords;
        $row->packaging_en = @$row->lang->where('language_id', 1)->first()->packaging;
        $row->packaging_ar = @$row->lang->where('language_id', 2)->first()->packaging;

        if($row->save()) {
            print($row->name_ar.': updated <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('slugging_products', function()
{
    //Make slug strings for Products
    $products = \App\Product::all();
    foreach ($products as $row)
    {
        $row->slug = \App\Helper::generate_slug('\App\Product', $row->name, $row->id);
        if($row->save()) {
            print($row->name.': <strong>('.$row->slug.')</strong> Done <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('slugging_users', function()
{
    //Make slug strings for Users
    $users = \App\User::all();
    foreach ($users as $row)
    {
        if(!empty($row->company_name)) {
            $row->slug = \App\Helper::generate_slug('\App\User', $row->company_name, $row->id);
        } else {
            $user_profile = $row->profile;
            if($user_profile && !empty($user_profile->company_name)) {
                $row->slug = \App\Helper::generate_slug('\App\User', $user_profile->company_name, $row->id);
            } else {
                $row->slug = \App\Helper::generate_slug('\App\User', $row->name, $row->id);
            }
        }
        if($row->save()) {
            print('<strong>'.$row->slug.'</strong> <br>');
        }
    }
    return ' ********************* End of script ********************';
});
Route::get('see_mail', function() {
    $quote = \App\Deals\Quote::find(96);
    $deal = \App\Deals\QuoteRequest::find(41);
    $data = $deal->toArray();
    return view('front.emails.deals.rfq_mail', ['quote'=>$quote,'deal'=>$deal,'data'=>$data]);
});

Route::get('set_users_has_products', function() {
    $products = \App\Product::where('active',1)->where('approved',2)->with('user')->get();
    foreach($products as $product) {
        $product->user->has_products = 1;
        if($product->user->save()) {
            return $product->user->name.' set to has products';
        }
    }
    return ' ********************* End of script ********************';
});

Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);

Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

// routes special for api request
Route::group(['prefix' => 'api' , 'namespace' => 'api'], function (){
    Route::post('login','UserController@login');
    Route::post('register','UserController@register');
    Route::get('/password/reset' , 'UserController@Reset');
    
    //now the route that need authentication first
    Route::group(['middleware' => 'api_auth'] , function (){
        Route::get('packages' , 'PackagesController@GetPackageAllDetails');
        Route::get('en/GetAllPorts' , function (){
           return \App\Helper::api_response_format(true , [
                'Alexandria', 'Suze', 'Abu Qir', 'Abu Zenima', 'Adabiya', 'Ain Sukhna', 'Damietta', 'El Dekheila', 'Kosseir', 'Maadiya Port', 'Nuweibah', 'Port Said', 'Ras Sudr', 'Za\'farana',
            ]);
        });
        Route::get('ar/GetAllPorts' , function (){
            return \App\Helper::api_response_format(true , [
                'الاسكندرية', 'السويس', 'ابو قير', 'ابو زنيمة', 'الادبية', 'العين السخنة', 'دمياط', 'الدقهلية', 'القصير', 'المعدية', 'نويبع', 'بور سعيد', 'رأس سدر', 'الزعفرانة',
            ]);
        });

        Route::post('upload/file' , 'FileCOntroller@uploadSingleFile');
        Route::post('upload/multifile' , 'FileCOntroller@uploadMulipleFile');
        Route::get('GetAllThemes' , 'ThemesController@GetAllThemese');
        Route::post('SelectTheme' , 'ThemesController@SelectTheme');
        Route::get('ar/paymentTerms',function (){
            return Response::json([
                'data' => ['T/T','L/C','D/P D/A','ماني جرام','كارت ائتمان','باي بال','ويسترن يونيون','كاش','شيك','Demand Draft','أخرى']
            ], 200);

        });

        Route::get('en/paymentTerms',function (){
            return Response::json([
                'data' => ['T/T' , 'L/C','D/P D/A','MoneyGram','Credit Card','PayPal','Western Union','Cash','Cheque','Demand Draft','Other']
            ], 200);
        });
        //to list all countries by limit
        Route::get('countries/{limit?}' , function ($limit = null){
            return \App\Country::limit($limit)->get();    
        });
        
        Route::get('product/categories' , function (\Illuminate\Http\Request $request){
            $categories = \App\Category::whereParent_category_id(0);
            $categories = \App\Helper::check_collection_limit($categories , $request->limit , $request->offset)->get(['name_ar' , 'id' , 'name_en']);
            foreach ($categories as $category){
                $category->childs = $category->childs_tree;
            }
            return $categories;
        });
        Route::post('setUserRole' , 'UserController@setUserRole');
        Route::post('countries/{iso}' , function ($iso){
            return \App\Country::whereIso2($iso)->get();    
        });
        //to activate accounts
        Route::get('activate' , 'UserController@send_activation');
        //get specific user
        Route::post('users/{id}','UserController@get_user');
        //update specific user
        Route::PUT('users/{id}','UserController@update_user');
//*****************************Profile*******************************U**********************
        // the user profiles specified with that user
        Route::get('user-profile/{id}','UserProfileController@get_profile');
          //create userProfile
        Route::post('user-profile','UserProfileController@create_profile');
          //update profile
        Route::put('user-profile','UserProfileController@update_profile');
          //delete profile
        Route::delete('user-profile/{id}','UserProfileController@destroy_profile');
//***********************************Products******************************************************
        //create product
        Route::post('user-product','ProductController@create_product');
        // the products specified with that user remote
        Route::get('user-product/{limit?}','ProductController@get_product');
        //update on specific product
        Route::PUT('user-product/{id}','ProductController@update_product');
        //DELETE Product
        Route::delete('user-product/{id}','ProductController@delete_product');
        //restore product from trash
        Route::post('user-product/{id}/restore','ProductController@restore_product');
        //get all allowed importer products
        Route::get('importer-products' , 'ProductController@GetAllProductsForImporter');

        Route::get('GetOtheProducts' , 'ProductController@GetOtheProducts');

        Route::get('GetImporterAnotherCats' , 'ProductController@GetImporterAnotherCats');

        Route::get('GetExporterProducts' , 'ProductController@ExporterProducts');

        Route::get('GetExporterOtherProducts' , 'ProductController@ExporterOtherProducts');
        Route::post('rfq_delete_img' , 'ExportOpportunitiesController@rfq_delete_img');
//***********************************User Dashboard**************************************************************
        // get data
        Route::get('user-dashboard/{id}','UserDashboardController@get_dashboard');
//**************************************Upgrade Account**********************************************************
        // get data
        Route::get('upgrade-account/{id}','UpgradeAccountController@get_upgrade_account');
        //create
        Route::post('upgrade-account','UpgradeAccountController@create_upgrade_account');
        //update
        Route::put('upgrade-account/{id}','UpgradeAccountController@update_upgrade_account');
        //delete
        Route::delete('upgrade-account/{id}','UpgradeAccountController@delete_upgrade_account');

        Route::post('selectPackage' , 'PackagesController@SelectPackage');

        Route::get('GetUserPackageDetails' , 'PackagesController@GetPackageDetails');
//**************************************News & Events************************************************************
        Route::get('news-events/{limit?}','NewsEventsController@get_news_events');
//**************************************User Messages************************************************************
        Route::get('user-messages/{id}','UserMessageController@get_inbox');
        //create
        Route::get('user-messages/sent-messages/{id}','UserMessageController@get_sent_messages');
        //create
        Route::post('user-messages/{id}/{to_id}','UserMessageController@send_message');
        //delete
        Route::delete('user-messages/{id}','UserMessageController@delete_message');
        //make the message read
        Route::post('TurnOld' , 'UserMessageController@turnToOld');
        //thread
        Route::get('user-thread' , 'UserMessageController@getUserThread');
        //search
        Route::get('messages-search' , 'UserMessageController@searchMessage');
//**************************************Export opportunities **************************************************
        // get specific export opportunities
        Route::get('export-opportunities/{id}','ExportOpportunitiesController@get_export_opportunities');
        // get the whole export opportunities
        Route::get('export-opportunities-all/{limit?}','ExportOpportunitiesController@get_all_export_opportunities');
        //get the export opp associated with the importer api_token
        Route::get('my-export-opportunities/{limit?}','ExportOpportunitiesController@get_my_export_opportunities');
        //create
        Route::post('export-opportunities','ExportOpportunitiesController@create_export_opportunities');
        //update
        Route::put('export-opportunities/{id}','ExportOpportunitiesController@update_export_opportunities');
        //delete mean put it into the trash
        Route::delete('export-opportunities/{id}','ExportOpportunitiesController@delete_export_opportunities');
        //restore the deleted one
        Route::post('export-opportunities/{id}/restore','ExportOpportunitiesController@restore_export_opportunities');

        //importer other rfq
        Route::get('importer-others-products' , 'ExportOpportunitiesController@AvailableRFQForImpoerter');

        //get not open RFQ
        Route::get('GetNotOpenRFQ' , 'ExportOpportunitiesController@GetNotOpenRequests');
        //get latest rfq
        Route::get('GetNewRFQ' , 'ExportOpportunitiesController@GetNewRFQs');
        //open Rfq and get it's details
        Route::post('open-rfq/{id}','ExportOpportunitiesController@open_Rfq');
//*****************************************Quotations provided*************************************************
        // get quotes this user provide
        Route::get('quote-provided/{id}','QuotationsProvidedController@get_quote_provided');
        //create
        Route::post('quote-provided','QuotationsProvidedController@create_quote_provided');
        //delete mean put it into the trash
        Route::delete('quote-provided/{id}','QuotationsProvidedController@delete_quote_provided');
        //restore the deleted one
        Route::post('quote-provided/{id}/restore','QuotationsProvidedController@restore_quote_provided');
        //**************************************Service Provider *****************************************************
        //create service
        Route::post('service' , 'ServiceController@AddService');
        //get all the services
        Route::get('service/{limit?}','ServiceController@get_services');
        //get service by status
        Route::get('get-service-by-status' , 'ServiceController@get_services_by_status');
        //get other services
        Route::get('get-other-service' , 'ServiceController@get_other_services');
        //get my services
        Route::get('get-my-service' , 'ServiceController@get_my_services');
        //update service provider
        Route::put('update-service','ServiceController@update_service');
////**************************************Upgrade Account *****************************************************
//        // get quotes this user provide
//        Route::post('upgrade-account/{id}','UpgradeAccountController@get_upgrade_account');
//        //create
//        Route::post('upgrade-account','UpgradeAccountController@create_upgrade_account');
//        //update package
//        Route::put('upgrade-account/{id}','UpgradeAccountController@update_upgrade_account');
//        //delete mean put it into the trash
//        Route::delete('upgrade-account/{id}','UpgradeAccountController@delete_upgrade_account');
    });

});

Route::group(['prefix'=>'admin', 'namespace'=>'Admin','middleware'=>['auth', 'admin']], function() {
    Route::get('give-role-permissions', 'AdminController@getGiveRolePermissions');
    Route::post('give-role-permissions', 'AdminController@postGiveRolePermissions');
    Route::get('profiles/login/{id}', 'ProfilesController@loginById');
    Route::get('delete_message/{id}', 'MessagesController@delete_message');
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('users', 'UsersController');
    Route::resource('admins', 'AdminsController');
    Route::resource('adv_general', 'AdvGeneralController');
    Route::resource('messages', 'MessagesController');
    Route::get('notification/all', 'MessagesController@get_all_notification');
    Route::get('notification/archive/{id}', 'MessagesController@get_archive_notification');
    Route::resource('products', 'ProductsController');
    Route::get('pending_messages', 'MessagesController@get_messages'); // get the whole messages in the system
    Route::get('notification_messages', 'MessagesController@set_read_notification');
    Route::get('notification_call_messages', 'MessagesController@set_read_call_notification');
    Route::get('products_messages', 'ProductsController@product_message'); // get all the message send to exporter
    Route::post('SendProducts_mail', 'ProductsController@send_products_mail'); // send the mail to both exporter and importer
    Route::resource('services', 'ServicesController');
    Route::resource('deals', 'DealsController');
    Route::get('getquotes', 'DealsController@get_deals_quotes'); // redirect to Controller deals function get_deals_quotes
    Route::post('sendquotes', 'DealsController@send_quotes'); // redirect to Controller deals function get_deals_quotes
    Route::get('dealsDesc/{id}', 'DealsController@quote_details'); // redirect to Controller deals function quote_details
    Route::post('Destroyquote', 'DealsController@destroy_quote'); // redirect to Controller deals to delte quote
    Route::resource('userwebsites', 'UserWebsitesController');
    Route::resource('packages', 'PackagesController');
    Route::resource('package-items', 'PackageItemsController');
    Route::resource('sliders', 'SlidersController');
    Route::resource('pages', 'PagesController');
    Route::resource('testimontials', 'TestimontialsController');
    Route::resource('reviews', 'ReviewsController');
    Route::resource('countries', 'CountriesController');
    Route::resource('business-developers', 'BusinessDevelopersController');
    Route::resource('trade-managers', 'TradeManagersController');
    Route::resource('pick-describes', 'PickDescribeController');
    Route::resource('templates', 'TemplatesController');
    Route::resource('package_upgrades', 'UserPackagesController');
    Route::resource('product_reasons', 'ProductReasonsController');
    Route::resource('blog', 'BlogController');
    Route::resource('blog_categories', 'BlogCategoriesController');
    Route::resource('blog_authors', 'BlogAuthorsController');
    Route::resource('advertisement', 'AdvController');
    Route::controller('profiles', 'ProfilesController');
    Route::controller('categories', 'CategoriesController');
    Route::controller('settings', 'SettingsController');
    Route::controller('transfer', 'TransferController');
    Route::controller('compose', 'ComposeController');
    Route::controller('privilege','AdminPrivilegesController');
    Route::controller('/', 'DashboardController');
    Route::get('/home', 'AdminController@index');
    Route::post('/home', 'AdminController@index');


});

Route::group(['prefix'=>'dashboard', 'namespace'=>'Dashboard','middleware'=>['auth', 'active']], function() {
    Route::controller('reg', 'WebsiteController');
    Route::controller('website', 'WebsiteController');
    //---------------------------------------------
    Route::post('profile_form_done', 'WebsiteController@postProfile');
    Route::post('proudct_form_done', 'ProductsController@store');
    Route::post('launch_form_done', 'WebsiteController@postLaunch');
    Route::post('service_form_done', 'ServicesController@store');
    Route::post('request_quotes_done', 'Deals\RequestQuotesController@store');
    //----------------------------------------------
    Route::resource('products', 'ProductsController');
    Route::post('countryget','DashboardController@country_code');
    Route::get('productss/grid', 'ProductsController@grid');
    Route::get('quotess/grid', 'Deals\QuotesController@gride');
});

Route::group(['prefix'=>'dashboard', 'namespace'=>'Dashboard','middleware'=>['auth', 'active','reg_steps']], function(){
    Route::get('products/subcats/{category_id}', 'ProductsController@get_sub');

    Route::get('services/grid', 'ServicesController@grid');
    Route::get('media/grid', 'MediaController@grid');
    Route::get('reviews/grid', 'ReviewsController@grid');
    Route::get('switch_lang/{locale}', 'DashboardController@getSwitch_lang');
    Route::post('products/trash', 'ProductsController@trash');
    Route::post('products/restore', 'ProductsController@restore');
    Route::post('deals/trash', 'Deals\DealsController@trash');
    Route::post('deals/restore', 'Deals\DealsController@restore');
    Route::get('products/readreason', 'ProductsController@readReason');
    Route::resource('services', 'ServicesController');
    Route::resource('groups', 'GroupsController');
    Route::resource('media', 'MediaController');
    Route::resource('albums', 'MediaAlbumsController');
    Route::resource('offices', 'OfficesController');

    Route::get('deals/grid', 'Deals\DealsController@grid');
    Route::resource('deals', 'Deals\DealsController');
    Route::resource('sell_offers', 'Deals\SellOffersController');
    Route::resource('request_quotes', 'Deals\RequestQuotesController');
    Route::resource('partnerships', 'Deals\PartnershipsController');
    Route::resource('alerts', 'Deals\AlertsController');
    Route::controller('quotations', 'Deals\QuotesController');
    Route::get('quotations-send', [ 'as'=>'getveiw','uses'=>'Deals\QuotesController@get_sent_request']);
    Route::controller('messages', 'Messages\MessagesController');
    Route::resource('contacts', 'Messages\ContactsController');
    Route::resource('folders', 'Messages\FoldersController');

    Route::resource('websitepages', 'WebsitePagesController');
    Route::controller('quickadd', 'QuickAddController');
    Route::controller('uploader', 'UploadController');
    Route::controller('settings', 'SettingsController');
    Route::controller('reviews', 'ReviewsController');
    Route::controller('profile', 'ProfileController');

    Route::get('profiles/{id}', 'ProfileController@getIndex');
    Route::controller('favorites', 'FavoritesController');
    Route::controller('analytics', 'AnalyticsController');
    Route::controller('promots', 'PromoteController');
    Route::controller('ppp', 'PppController');
    Route::controller('support', 'SupportController');
    Route::controller('listings', 'ListingsController');
    Route::controller('activities', 'ActivitiesController');
    Route::controller('/', 'DashboardController');

});

Route::group(['prefix'=>'store/{domain?}', 'namespace'=>'Front'], function() {
    //Route::get('pr{product_id}/{slug?}', 'TemplatesController@getProduct');
    Route::controller('/', 'TemplatesController');
});

Route::group(['prefix'=>'/', 'namespace'=>'Front'], function(){

    
    Route::get('{id}/فرص-تصدير', 'DealsController@getExportChances');
    Route::get('فرص-تصدير/{slug}/{id?}/', 'DealsController@getRfq');
    Route::get('فرص-تصدير', 'DealsController@getBrowse');
    Route::get('products/{slug}', 'ProductsController@getShow');
    Route::get('contact-info/{slug}/{id?}', 'CompaniesController@getContactInfo');

    Route::get('blog/{slug}', 'BlogController@getShow');
    Route::get('blog/category/{slug}', 'BlogController@getCategory');
    Route::get('blog/tag/{tag}', 'BlogController@getTag');
    Route::controller('blog', 'BlogController');

    Route::get('category/sub/{category_id}', 'CategoryController@getSub');
    Route::get('category/{category_id}/{slug?}', 'CategoryController@getShow');
    Route::get('product/i/{product_id}/{slug?}', 'ProductsController@getShow');
    Route::get('products', 'ProductsController@getIndex');
    Route::get('y/{page_id}/{slug?}', 'PagesController@getShow');
//	Route::get('home', 'HomeController@getHome');
    //The above Route is for the Home Page, Front.index is the main view
//	Route::get('/', 'HomeController@getLanding');

    Route::get('/', 'HomeController@getHome'); //replaced task (1) view(front.index)
    Route::get('/redirect', 'SocialAuthController@redirect');
    Route::get('/callback', 'SocialAuthController@callback');

    Route::get('en', function() { return redirect()->to('lang/switch/en?back=no'); });

    Route::controller('registration', 'RegistrationController');
    Route::controller('category', 'CategoryController');
    Route::controller('product', 'ProductsController');
    Route::controller('services', 'ServicesController');
    Route::controller('deals', 'DealsController');
    Route::controller('company', 'CompaniesController');
    Route::controller('upgrade', 'UpgradeController');
    Route::controller('reviews', 'ReviewsController');
    Route::controller('cart', 'CartController');
    Route::controller('pages', 'PagesController');
    Route::controller('search', 'SearchController');
    Route::controller('interact', 'FormsController');
    Route::controller('lang', 'LanguagesController');
    Route::controller('site', 'SiteController');
    Route::controller('testimontials', 'TestimontialsController');
    Route::controller('package-check', 'PackageCheckController');

    Route::controller('templates', 'TemplatesController');
    Route::controller('/', 'HomeController');



});
