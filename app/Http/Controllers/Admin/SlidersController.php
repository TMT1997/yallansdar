<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Slider;
use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;
use View;

class SlidersController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $sliders = Slider::all();
        return view('admin.sliders.index', compact('sliders'));
    }

    public function create()
    {
        return view('admin.sliders.create');
    }

    public function store(Request $request)
    {
        $requestData = $request->all();

        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        if($request->hasFile('image_ar')) {
            $requestData['image_ar'] = Helper::upload($request, 'image_ar');
        }
        $model = Slider::create($requestData);

        Session::flash('flash_message', 'تم اضافة عنصر جديد بالسليدر بنجاح');
        return redirect('admin/sliders');
    }

    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.show', compact('slider'));
    }

    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.edit', compact('slider'));
    }

    public function update($id, Request $request)
    {        
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        if($request->hasFile('image_ar')) {
            $requestData['image_ar'] = Helper::upload($request, 'image_ar');
        }
        $slider = Slider::findOrFail($id);
        $slider->update($requestData);

        Session::flash('flash_message', 'تم التعديل بنجاح');
        return redirect('admin/sliders');
    }

    public function destroy($id)
    {
        Slider::destroy($id);
        Session::flash('flash_message', 'تم حذف عنصر من السليدر بنجاح');
        return redirect('admin/sliders');
    }
}