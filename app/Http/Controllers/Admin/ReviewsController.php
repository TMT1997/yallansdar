<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;

class ReviewsController extends InitController
{
    public function __construct() {
    	parent::__construct();
    }

    public function index()
    {
        $reviews = Review::with('user', 'from_user')->paginate(25);
        return view('admin.reviews.index', compact('reviews'));
    }

    public function create()
    {
        return view('admin.reviews.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        Review::create($requestData);

        Session::flash('flash_message', 'Review added!');
        return redirect('admin/reviews');
    }

    public function show($id)
    {
        $review = Review::findOrFail($id);
        return view('admin.reviews.show', compact('review'));
    }

    public function edit($id)
    {
        $review = Review::findOrFail($id);
        return view('admin.reviews.edit', compact('review'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $review = Review::findOrFail($id);
        $review->update($requestData);

        Session::flash('flash_message', 'Review updated!');
        return redirect('admin/reviews');
    }

    public function destroy($id)
    {
        Review::destroy($id);
        Session::flash('flash_message', 'Review deleted!');
        return redirect('admin/reviews');
    }
}