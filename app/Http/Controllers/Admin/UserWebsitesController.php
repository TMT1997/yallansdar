<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserWebsite;
use App\Category;
use Session;

class UserWebsitesController extends InitController
{
    public function __construct() {
    	parent::__construct();
    }

    public function index(Request $request)
    {
        $userwebsites = new UserWebsite;

        if($request->has('status') && $request->get('status')!=0) {
            $userwebsites = $userwebsites->whereStatus($request->get('status'));
        }

        $userwebsites = $userwebsites->orderBy('id', 'DESC')->with('template')->paginate(10);
        return view('admin.userwebsites.index', compact('userwebsites'));
    }

    public function create()
    {
        return view('admin.userwebsites.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        UserWebsite::create($requestData);

        Session::flash('flash_message', 'UserWebsite added!');
        return redirect('admin/userwebsites');
    }

    public function show($id)
    {
        $userwebsite = UserWebsite::whereId($id)->with('lang','category','images','user')->first();
        $category_parents = [];
        if($userwebsite->category) {
            $category_parents = Category::get_parents($userwebsite->category->id);
        }
        return view('admin.userwebsites.show', compact('userwebsite', 'category_parents'));
    }

    public function edit($id)
    {
        $userwebsite = UserWebsite::findOrFail($id);
        return view('admin.userwebsites.edit', compact('userwebsite'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $userwebsite = UserWebsite::findOrFail($id);
        $userwebsite->update($requestData);

        Session::flash('flash_message', 'UserWebsite updated!');
        return redirect('admin/userwebsites');
    }

    public function destroy($id)
    {
        UserWebsite::destroy($id);
        Session::flash('flash_message', 'User Website deleted!');
        return redirect('admin/userwebsites');
    }
}
