<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ProductReason;
use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;

class ProductReasonsController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
    	$product_reasons = ProductReason::orderBy('id', 'DESC')->get();
    	return view('admin.product_reasons.index', compact('product_reasons'));
    }

    public function create()
    {
        return view('admin.product_reasons.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title_ar'=>'min:2|required',
            'title_en'=>'min:2|required',
        ]);
        $requestData = $request->all();
        $model = ProductReason::create($requestData);

        Session::flash('flash_message', 'تم اضافة الدولة بنجاح');
        return redirect('admin/product_reasons');
    }

    public function show($id)
    {
        $reason = ProductReason::findOrFail($id);
        return view('admin.product_reasons.show', compact('reason'));
    }

    public function edit($id)
    {
        $reason = ProductReason::findOrFail($id);
        return view('admin.product_reasons.edit', compact('reason'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title_ar'=>'min:2|required',
            'title_en'=>'min:2|required',
        ]);
        $requestData = $request->all();
        $reason = ProductReason::findOrFail($id);
        $reason->update($requestData);

        Session::flash('flash_message', 'تم تعديل الدولة بنجاح');
        return redirect('admin/product_reasons');
    }

    public function destroy($id)
    {
        $reason = ProductReason::findOrFail($id);
        ProductReason::destroy($id);
        Session::flash('flash_message', 'تم حذف الدولة بنجاح');
        return redirect('admin/product_reasons');
    }
}
