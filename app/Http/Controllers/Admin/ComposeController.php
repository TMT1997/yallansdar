<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Hash;
use Auth;
use Mail;

use App\User;
use App\Email;
use App\EmailBulk;

class ComposeController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        $emails = Email::all();
        $emailsbulk = EmailBulk::all();

        return view('admin.inbox.index', compact('emails', 'emailsbulk'));
    }

    public function getNew($candidate_id)
    {
    	$candidate = User::whereId($candidate_id)->with('emails')->first();
    	return view('admin.inbox.new', compact('candidate'));
    }

    public function postNew(Request $request)
    {
        $goto_inbox = $request->get('goto_inbox');
        $goto_email = $request->get('goto_email');
        $message = new Email;
        $flash_message = "";

        if(isset($goto_inbox) && $goto_inbox==1)
        {
        	$flash_message .= 'تم إرسال الرسالة بنجاح إلى صندوق رسائل المشترك';
        }

        if(isset($goto_email) && $goto_email==1)
        {
            //send verification mail to user
            $data['subject'] = $request->get('subject');
            $data['body'] = nl2br($request->get('body'));
            $data['user'] = User::findOrFail($request->get('user_id'));
            $data['indentifier'] = Hash::make($data['user']->id);

            if($data['user']->unsubscribed==1) {
                $flash_message .= '<br> بناءا على طلب المشترك ﻻ يمكن إرسال رسائل له حيث أنه قام بإلغاء اشتراكه في خدمة رسائل الموقع';
            } else {
                Mail::send('front.emails.candidates.template',['data'=>$data], function($message) use($data)
                {
                    $message->from('mail@yallansadar.com', "YallaNsadar يلانصدر");
                    $message->subject($data['subject']);
                    $message->to($data['user']->email);
                });

                $flash_message .= '<br> تم إرسال الرسالة بريدية إلى المشترك';
            }
        }

        $requestData = $request->all();
        $requestData['admin_id'] = Auth::id();
        if($message->create($requestData)) {
            //Nothing to do now
        }
        Session::flash('flash_message', $flash_message);
        return redirect('admin/candidates/show/'.$request->get('user_id'));
    }

    public function postReply(Request $request)
    {
        $message_id = $request->get('message_id');
        $message_data = \App\Message::findOrFail($message_id);
        $message_reply = new \App\MessageReply;

        $goto_inbox = $request->get('goto_inbox');
        $goto_email = $request->get('goto_email');

        $flash_message = "";

        $data['subject'] = $request->get('subject');
        $data['body'] = $request->get('body');
        $data['message_data'] = $message_data;

        $user = \App\User::whereEmail($message_data->email)->first();

        if(isset($goto_inbox) && $goto_inbox==1 && $user)
        {
            $userInbox = new Email;
            $requestData = $request->all();
            $requestData['admin_id'] = Auth::id();

            if($userInbox->create($requestData)) {
                $flash_message .= 'تم إرسال الرسالة بنجاح إلى صندوق رسائل المشترك: '.$user->name.' <br>';                
            }
        }
        if(isset($goto_email) && $goto_email==1)
        {
            Mail::send('front.emails.contact.reply',['data'=>$data], function($message) use($data)
            {
                $message->from('mail@yallansadar.com', "YallaNsadar يلانصدر");
                $message->subject($data['subject']);
                $message->to($data['message_data']->email);
            });
        }

        $flash_message .= '<br> تم إرسال الرد الخاص بك إلى صاحب الرسالة';

        $requestData = $request->all();
        $requestData['admin_id'] = Auth::id();
        if($message_reply->create($requestData)) {
            //Nothing to do now
        }
        Session::flash('flash_message', $flash_message);
        return redirect()->back();
    }

    public function getShow($email_id)
    {
        $email = Email::whereId($email_id)->first();
        $data['subject'] = $email->subject;
        $data['body'] = $email->body;
        $data['user'] = User::findOrFail($email->user_id);
        $data['indentifier'] = Hash::make($data['user']->id);

        return view('admin.inbox.show', compact('data'));
    }

    ////////////////

    public function getBulk()
    {
        $users = User::orderBy('name', 'ASC')->get();
        return view('admin.inbox.bulk', compact('users'));
    }

    //Save the bulk email statistics in the model EmailBulk
    public function postBulk(Request $request)
    {
        $sending_option = $request->get('sending_option');
        $emailbulk = new EmailBulk;
        $candidates_received = [];

        $goto_inbox = $request->get('goto_inbox');
        $goto_email = $request->get('goto_email');
        $subject = $request->get('subject');
        $body = $request->get('body');

        $user_active = $request->get('user_active');
        $has_products = $request->get('has_products');

        $candidates = new User;

        if($sending_option==1) //Send using select filters (active, branch)
        {
            if($user_active=='1') {
                $candidates = $candidates->whereEmail_verified(1);
            } else if($user_active=='0') {
                $candidates = $candidates->whereEmail_verified(0);
            }

            if($has_products=='1') {
                $candidates = $candidates->join('products', 'users.id', '=', 'products.user_id');
            } else if($has_products=='0') { //users that has no products
                $candidates = $candidates->leftJoin('products', 'users.id', '=', 'products.user_id')->where('products.user_id','=',NULL);
            }
        } else { //send to selected candidates
            $candidates = $candidates->whereIn('id', $request->get('custom_users'));
        }
        $candidates = $candidates->whereAdmin(0)->whereUnsubscribed(0)->get();

        $message = new Email;
        $flash_message = "";

        foreach($candidates as $candidate)
        {
            if(isset($goto_inbox) && $goto_inbox==1)
            {
                $flash_message .= 'تم إرسال الرسالة بنجاح إلى صندوق رسائل المشترك: '.$candidate->name.' <br>';
            }
            if(isset($goto_email) && $goto_email==1)
            {
                //send verification mail to user
                $data['subject'] = $subject;
                $data['body'] = $body;
                $data['user'] = $candidate;
                $data['indentifier'] = Hash::make($data['user']->id);

                if($data['user']->unsubscribed==1) {
                    $flash_message .= '<br> بناءا على طلب المشترك ﻻ يمكن إرسال رسائل له حيث أنه قام بإلغاء اشتراكه في خدمة رسائل الموقع';
                } else {
                    Mail::send('front.emails.candidates.template',['data'=>$data], function($message) use($data)
                    {
                        $message->from('mail@yallansadar.com', "YallaNsadar يلانصدر");
                        $message->subject($data['subject']);
                        $message->to($data['user']->email);
                    });
                    $flash_message .= 'تم إرسال الرسالة بريدية إلى المشترك '.$candidate->email.'<br>';
                }
            }

            $requestData = $request->all();
            $requestData['admin_id'] = Auth::id();
            if($message->create($requestData))
            {
                $candidates_received[] = ['id'=>$candidate->id, 'name'=>$candidate->name];
            }
        }

        //Save the statistics
        $emailbulk->goto_inbox = $goto_inbox;
        $emailbulk->goto_email = $goto_email;
        $emailbulk->active_status = $user_active;
        $emailbulk->has_products = $has_products;
        $emailbulk->subject = $subject;
        $emailbulk->body = $body;
        $emailbulk->candidates_received = serialize($candidates_received);
        if($emailbulk->save()) {
            $flash_message .= '<br><i>Bulk Email statistics saved</i>';
        }

        Session::flash('flash_message', $flash_message);
        return redirect('admin/compose/bulk');
    }

    public function getBulkshow($email_id)
    {
        $email = EmailBulk::whereId($email_id)->first();
        $data['subject'] = $email->subject;
        $data['body'] = $email->body;

        $candidates = unserialize($email->candidates_received);
        $candidate = $candidates[0];
        if(!$candidate) {
            $data['user'] = User::first();
        } else {
            $data['user'] = User::findOrFail($candidate['id']);
        }
        $data['indentifier'] = Hash::make($data['user']->id);
        return view('admin.inbox.show', compact('data'));
    }

    public function getBulkRecipients($email_id)
    {
        $email = EmailBulk::whereId($email_id)->first();
        $recipients = unserialize($email->candidates_received);

        return view('admin.inbox.recipients', compact('recipients'));
    }
}