<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Role;
use App\User;
use App\Packages\Package;
use Carbon\Carbon;
use Session;
use View;

class UsersController extends InitController
{
    public function __construct() {
        $packages = Package::orderBy('sort_order', 'ASC')->get();
        View::share('packages', $packages);

        parent::__construct();
    }

    public function index(Request $request)
    {
        $no_results = 10;
        $users = new User;
        if($request->has('type')) {
            $users = $users->where($request->get('type'), 1);
        }
        if($request->has('no_results')) {
            $no_results = $request->get('no_results');
        }
        if($request->has('q')) {
            $q = trim($request->get('q'));
            $users = $users->where('users.name','like','%'.$q.'%')
                ->orWhere('email','like','%'.$q.'%')
                ->orWhere('mobile','like','%'.$q.'%')
                ->orWhere('company_name','like','%'.$q.'%');
        }
        if($request->has('email_verified') && $request->get('email_verified')==1) {
            $users = $users->whereEmail_verified(1);
        } else if($request->has('email_verified') && $request->get('email_verified')==0) {
            $users = $users->whereEmail_verified(0);
        }

        if($request->has('period')) {
            $period = $request->get('period');
            if($period=='today') {
                $users = $users->where('users.created_at', '>=', Carbon::today()->toDateString());
            } else if($period=='yesterday') {
                $users = $users->where('users.created_at', '>=', Carbon::yesterday()->toDateString());
            } else if($period=='1week') {
                $users = $users->where('users.created_at', '>=', Carbon::now()->subDays(7)->toDateString());
            } else if($period=='1month') {
                $users = $users->where('users.created_at', '>=', Carbon::now()->subMonth()->toDateString());
            }
        }
        //----------------------------------
        if($request->has('sector')) {
            if($request->get('sector')=="all")
            {
                $users = $users->join('user_profiles', 'users.id', '=', 'user_profiles.user_id');
            }
            else
            {
                $sector = $request->get('sector');
                $users = $users->join('user_profiles', 'users.id', '=', 'user_profiles.user_id');
                $users->where('user_profiles.other_products', $sector);
            }
        }
        //----------------------------------
        if($request->has('end_date'))
        {
            $end_date   = date('Y-m-d', strtotime($request->get('end_date')));
            $start_date = date('Y-m-d', strtotime($request->get('from_date')));
            if($request->get('end_date')=="all")
            {

            }
            else
            {
                $users = $users->whereDate('users.created_at','<=',$end_date)
                        ->whereDate('users.created_at','>=',$start_date);
            }
        }
        //----------------------------------
        if($request->has('package_id')) {
            $users = $users->join('user_packages', 'users.id', '=', 'user_packages.user_id');
            $users->where('user_packages.package_id', $request->get('package_id'));
            $users->where('user_packages.confirmed', 1);
            $users->where('user_packages.paid', 1);
        }
        // $users->select('users.*','user_packages.user_id','user_packages.package_id');
        $users = $users->orderBy('users.id', 'DESC')->paginate($no_results);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|email',
            'password' => 'required'
        ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->get('password'));
        $user = User::create($data);

        if($request->get('roles')) {
            foreach ($request->roles as $role) {
                $user->assignRole($role);
            }            
        }

        Session::flash('flash_message', 'User added!');
        return redirect('admin/users');
    }

    public function show($id, Request $request)
    {
        $user = User::find($id);
        if($request->ajax()) {
            return view('admin.users.perview', compact('user'));    
        }
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $roles = Role::select('id', 'name', 'label')->get();

        $user = User::with('roles')->select('id', 'name', 'email')->findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }

        return view('admin.users.edit', compact('user', 'roles', 'user_roles'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required', 'roles' => 'required']);

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);

        $user->roles()->detach();
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        Session::flash('flash_message', 'User updated!');
        return redirect('admin/users');
    }

    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('flash_message', 'User deleted!');
        return redirect('admin/users');
    }
}