<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserProfile;
use App\quote_request_seen;
use App\Product;
use App\Deals\QuoteRequest;
use App\Deals\Quote;
use Auth;
use View;

class ProfilesController extends InitController
{
    public function __construct() {
        parent::__construct();

        $categories = \App\Category::whereParent_category_id(0)->orderBy('name_ar', 'ASC')->get();
        View::share('categories', $categories);
    }

    public function getIndex(Request $request)
    {
        $profiles = new UserProfile;
        if($request->has('status') && $request->get('status')!='') {
            $profiles = $profiles->whereStatus($request->get('status'));
        }
        $profiles = $profiles->whereProfile('temporary')->with('user')->orderBy('id','DESC')->get();
        return view('admin.profiles.index', compact('profiles'));
    }

    public function getProfile($user_id)
    {
        $user_profile = UserProfile::whereUser_id($user_id)->whereProfile('temporary')->with('docs','offices')->first();
        foreach(UserProfile::$fields_arr as $field=>$action) {
            if(isset($user_profile->$field) && !empty($user_profile->$field)) {
                if($action=='serialize')
                    $user_profile->$field = unserialize($user_profile->$field);
                else
                    $user_profile->$field = explode(',', $user_profile->$field);
            }
        }
        
        return view('admin.profiles.profile.profile', compact('user_profile'));
    }
    public function getReportproduct($user_id)
    {
        $added_products     = Product::whereUser_id($user_id)->get();
        $deal_quote_req_ids = quote_request_seen::whereUser_id($user_id)->get(['quoute_req_id']);
        $opportunity_seen   = QuoteRequest::whereIn('id' , $deal_quote_req_ids)->get();
        $offers_sent        = Quote::whereFrom_user_id($user_id)->get();
        return view('admin.profiles.product_report', compact
            ('added_products','opportunity_seen','offers_sent'));
    }
    public function loginById($id){
        Auth::loginUsingId($id);
        return redirect('home');
    }
}