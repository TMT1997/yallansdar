<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Session;
use App\User;
use App\Helper;
use App\Media;
use App\ProductImage;
use App\Deals\Deal;
use App\Deals\QuoteRequest;
use App\Deals\QuoteRequestImage;
use Auth;
use App\Service;
use App\ServiceLang;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
class AdminPrivilegesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAddUser()
    {
        return view('admin.privileges.add_user');
    }
    public function getAddProduct()
    {
        return view('admin.privileges.add_product');
    }
    public function getAddRfq()
    {
        return view('admin.privileges.add_rfq');
    }
    public function getAddService()
    {
        return view('admin.privileges.add_service');
    }
    public function postAddUser(Request $request)
    {
        $this->validate($request , [
            'name'  => 'required|max:60',
            'last_name'  => 'required|max:60',
            'email' => 'required|email|max:80|unique:users',
            'mobile'=>'required|numeric',
            'password' => 'required|min:6|confirmed',
        ]);

        $newUSer = User::create([
            'name'      =>$request->name,
            'last_name' => $request->last_name,
            'email'     => $request->email,
            'mobile'    => $request->mobile,
            'password'  => bcrypt($request->password),
            'active'    => 1,
        ]);

        if ($newUSer)
        {
            session()->flash('success',"The user was added successfully");
            return redirect('/admin');
        }
        else
        {
            session()->flash('error',"Oops .. ! Some thing is wrong.");
            return back();
        }

    }
    //------------------------------------------------------------------
    public function postAddProduct(Request $request)
    {
        $this->validate($request, Product::$rules);
        //=========================
        $data = $request->except('lang', 'add_images','add_pd');
        $data['user_id'] = Auth::id();
        $data['approved'] =2;//approve by default
        $data['slug'] = Helper::generate_slug('\App\Product', $data['name_ar']);
        if($request->hasFile('image')) {
            $data['image'] = Helper::upload($request, 'image');
        }
        $data['category_id'] = $request->get('selected_category_id');
        if(isset($data['payment_type'])) {
            $data['payment_type'] = implode(',', $data['payment_type']);
        }
        
        unset($data['selected_category_id']);
        $product = Product::create($data);

        if($product)
        {
            $media = Media::create([
                'user_id'=>Auth::id(),
                'type'=>'images',
                'media_album_id'=>1,
                'file'=>$data['image'],
                'name'=>'no_name'
            ]);
        }
        $images = ProductImage::upload_multi_images($request->file('add_images'), $product->id);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', trans('dash.prod_added',['prod'=>'<a href="'.route('dashboard.products.edit',$product->id).'" class="btn btn-default">'.$product->name_ar.'</a>']));
        return redirect('admin/products');
    }//end post product
    //------------------------------------------------------------------
    public function postAddRfq(Request $request)
    {
        $this->validate($request, Deal::$rules);
        $request_data = $request->except('add_images','add_pd3');
        $request_data['user_id'] = Auth::id();
        $request_data['status'] = 2 ;
        if($request_data['supplier_location']) {
            $request_data['supplier_location'] = implode(',', $request_data['supplier_location']);
        }
        if($request_data['payment_terms']) {
            $request_data['payment_terms'] = implode(',', $request_data['payment_terms']);            
        }
        // $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        
        $request_data['status'] = 2; //pending
        $request_data['slug'] = str_slug($request_data['name_ar']);
        // $request_data['name_ar']= $request_data['name_ar'];
        // $request_data['details_ar']= $request_data['details_en'];
        $quote_request = QuoteRequest::create($request_data);

        if($quote_request)
        {
            //Upload & Save Additional Images
            $images = QuoteRequestImage::upload_multi_images($request->file('add_images'), $quote_request->id);

            //Save As Common Deal 
            $deal = Deal::create([
                'user_id'=>Auth::id(),
                'section'=>'quote_request',
                'record_id'=>$quote_request->id,
                'type'=>@$request_data['request_for'],
                'status'=>$quote_request->status,
                'expired_at'=>$request_data['expired_at']
            ]);          
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$quote_request]);
        }
        Session::flash('flash_message', 'Request for Quote Deal added!');
        return redirect('admin/deals');
    }//end post rfq
    //------------------------------------------------------------------
    public function postAddService(Request $request)
    {
        $this->validate($request, Service::$rules);
        $data = $request->except('add_pd2');

        $data['user_id'] = Auth::id();
        $data['status'] = 2;
        $data['image'] = Helper::upload($request, 'image');
        $data['document'] = Helper::upload($request, 'document', false);
        // $data['category_id'] = $request->get('selected_category_id');
        $data['name'] = $request->get('name');
        //$data['english_name'] = $request->get('english_name');
        $data['locations'] = implode(',', $request->get('locations'));
        $data['price_min'] = $request->get('price_min');
        $data['price_max'] = $request->get('price_max');
        $data['expired_at'] = $request->get('expired_at');
        $langData = $request->get('lang');

        unset($data['lang']);
        unset($data['selected_category_id']);
        $service = Service::create($data);

        if($service)
        {
            //Save Service Language
            foreach ($langData as $lang_id=>$lndata) {
                $row = new ServiceLang;
                $row->service_id = $service->id;
                $row->language_id = $lang_id;
                $row->name = $data['name'];
                $row->english_name = $data['english_name'];
                $row->description = $lndata['description'];
                $row->details = $lndata['details'];
                $row->save();
            }
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$service]);
        }

        Session::flash('flash_message', 'Service added!');
        return redirect('admin/services');
    }//end post service
    //------------------------------------------------------------------
}
