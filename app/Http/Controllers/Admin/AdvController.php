<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Advs;
use App\Helper;
use Session;
use Image;
use File;
use View;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdvController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $advs = Advs::all();
        return view('admin.advertisement.index')->with('advs',$advs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }

        $model = Advs::create($requestData);

        Session::flash('flash_message', 'تم اضافة أعلان جديد
         بنجاح');
        return redirect('admin/advertisement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adv = Advs::findOrFail($id);
        return view('admin.advertisement.show')->with('adv',$adv);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adv = Advs::findOrFail($id);
        return view('admin.advertisement.edit')->with('adv',$adv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
//        if($request->hasFile('image_ar')) {
//            $requestData['image_ar'] = Helper::upload($request, 'image_ar');
//        }
        $adv = Advs::findOrFail($id);
        $adv->update($requestData);

        Session::flash('flash_message', 'تم التعديل بنجاح');
        return redirect('admin/advertisement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advs::destroy($id);
        Session::flash('flash_message', 'تم حذف عنصر من السليدر بنجاح');
        return redirect('admin/advertisement');
    }
}
