<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Session;
use View;
use Image;
use File;
use App\BlogCategory;
use App\Helper;

class BlogCategoriesController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $req)
    {
        $blog_categories = new BlogCategory;
        $blog_categories = $blog_categories->orderBy('id', 'DESC')->get();
        return view('admin.blog_categories.index', compact('blog_categories'));
    }

    public function create() {
        return view('admin.blog_categories.create');
    }

    public function store(Request $request) {
        $this->validate($request, BlogCategory::$rules);

        $data = $request->all();            
        $data['slug'] = str_slug($data['title_ar']);

        $cat = BlogCategory::create($data);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$cat]);
        }
        Session::flash('flash_message', 'تم اضافة القسم بنجاح');
        return redirect('admin/blog_categories');
    }

    public function show($id) {
        $row = BlogCategory::findOrFail($id);
        return view('admin.blog_categories.show', compact('row'));
    }

    public function edit($id) {
        $article = BlogCategory::findOrFail($id);
        return view('admin.blog_categories.edit', compact('article'));
    }

    public function update($id, Request $request) {
        $this->validate($request, BlogCategory::$rules);
        $requestData = $request->all();
        $requestData['slug'] = str_slug($request->get('title_ar'));
        $article = BlogCategory::findOrFail($id);
        $article->update($requestData);

        Session::flash('flash_message', 'تم تعديل القسم بنجاح');
        return redirect('admin/blog_categories');
    }

    public function destroy($id) {
        $article = BlogCategory::findOrFail($id);
        BlogCategory::destroy($id);

        Session::flash('flash_message', 'تم حذف القسم بنجاح');
        return redirect('admin/blog_categories');
    }
}