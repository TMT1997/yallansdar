<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\User;
use App\UserProfile;
use App\Language;
use App\Country;
use View;
use App;
use Session;
use Cache;

class InitController extends Controller
{
	public $current_lang_id;

    public function __construct()
    {
    	if(Session::has('yalla_locale')) {
    		App::setLocale(Session::get('yalla_locale'));
    	}
    	$languages = Language::whereActive(1)->get();
        $current_language = $languages->filter(function($lang){
            if(!empty(App::getLocale())){
                return $lang->iso2==App::getLocale();
            }
            return $lang->iso2=='ar';
        })->first();
        $this->current_lang_id = $current_language->id;
        $c_name = (App::getLocale()=='ar')?'name_ar':'name_en';
        \Carbon\Carbon::setLocale($current_language->iso2);

        $users = Cache::remember('users_count', 10, function() { return User::all(); });
        $user_counts['all'] = $users->count();
        $user_counts['importer'] = $users->filter(function($row){return ($row->importer==1);})->count();
        $user_counts['exporter'] = $users->filter(function($row){return ($row->exporter==1);})->count();
        $user_counts['service_provider'] = $users->filter(function($row){return ($row->service_provider==1);})->count();
        $user_counts['business_developers'] = Cache::remember('business_developers', 10, function() {
            return \App\BusinessDeveloper::count();
        });
        $user_counts['pick_describes'] = Cache::remember('pick_describes', 10, function() {
            return \App\PickDescribe::count();
        });
        $user_counts['trade_managers'] = Cache::remember('trade_managers', 10, function() {
            return \App\TradeManager::count();
        });
        $user_counts['products'] = Cache::remember('products_approved', 10, function() {
            return \App\Product::whereApproved(2)->count();
        });
        $user_counts['all_product'] = App\Product::count();
        $user_counts['quote_request'] = App\Deals\QuoteRequest::count();
        $user_counts['deal_quote'] = App\Deals\Quote::count();
        $user_counts['service'] = App\Service::count();
        //report for users for each month
        $now = Carbon::now();
        $users_monthly = User::select(\DB::raw('count(id) as `data`'),\DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
               ->groupby('year','month')
               ->having('YEAR','=',$now->year)
               ->get();
        //end report for users monthly
        //report for users for each year
       $users_yearly = User::select(\DB::raw('count(id) as `data`'),\DB::raw('YEAR(created_at) year'))
       ->groupby('year')
       ->get();
        //end report for users yearly
       //report for users for each day in last week
       $users_daily = User::select(\DB::raw('count(id) as `data` , DATE(created_at) as day_date'),\DB::raw('DAY(created_at) day' ))
       ->where('created_at', '>=', $now->subDays(7)->toDateString())
       ->groupby('day')
       ->get();
        //end report for users last week
       //report for users for each sector
       $all_parent_ids = \App\Category::whereParent_category_id(0)->get(['id']);
       $sector_users = UserProfile::select(\DB::raw('count(user_profiles.id) as `data` , other_products as sector , categories.name_ar'))
       ->join('categories', 'categories.id', '=', 'user_profiles.other_products')
       ->whereIn('other_products',$all_parent_ids)
       ->groupby('sector')
       ->get();
        //end report for users in sector
       //get count of rfq seen
       $no_rfq_seen = \App\quote_request_seen::distinct('quoute_req_id')->count('quoute_req_id');
       //end get data of rfq seen
       //get offers sent 
       $no_offers_sent = \App\Deals\Quote::count();
       //end get offers sent
        $countries = new Country;
        $countries = $countries->whereActive(1);
        if(App::getLocale()=='en') {
            $countries = $countries->orderBy('name_english', 'ASC')->select('id','name_english AS name')->lists('name', 'id');
        } else {
            $countries = $countries->orderBy('country_name', 'ASC')->select('id','country_name AS name')->lists('name', 'id');
        }
		View::share('languages', $languages);
        View::share('current_language', $current_language);
        View::share('lng_id', $current_language->id);
        View::share('no_rfq_seen', $no_rfq_seen);
        View::share('no_offers_sent', $no_offers_sent);
		View::share('user_counts', $user_counts);
        View::share('users_daily', $users_daily);
        View::share('users_monthly', $users_monthly);
        View::share('users_yearly', $users_yearly);
        View::share('sector_users', $sector_users);
        View::share('c_name', $c_name);
        View::share('countries', $countries);
    }
}
