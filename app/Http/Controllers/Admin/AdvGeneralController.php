<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Adv;
use App\Helper;
use Illuminate\Http\Request;
use Session;

class AdvGeneralController extends InitController
{
    public function index()
    {
        $advs = Adv::all();
       return view('admin.general_advs.index')->with('advs',$advs);
    }//end index

    public function create()
    {
        return view('admin.general_advs.form');
    }//end create

    public function store(Request $request)
    {

        $this->validate($request, ['type'=>'required' ,'image'=>'required']);
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }

        $model = Adv::create($requestData);

        Session::flash('flash_message', 'تم اضافة أعلان جديد
         بنجاح');
        return redirect('admin/adv_general');
    }//end store

    public function show($id, Request $request)
    {
       $adv = Adv::findOrFail($id);
       return view('admin.general_advs.show')->with('adv',$adv);
    }//end show

    public function edit($id)
    {
        $adv = Adv::findOrFail($id);
        return view('admin.general_advs.edit')->with('adv',$adv);
    }//end edit

    public function update($id, Request $request)
    {
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        $adv = Adv::findOrFail($id);
        $adv->update($requestData);

        Session::flash('flash_message', 'تم التعديل بنجاح');
        return redirect('admin/adv_general');
    }//end update

    public function destroy($id)
    {
       Adv::destroy($id);
       Session::flash('flash_message', 'تم حذف عنصر من السليدر بنجاح');
        return redirect('admin/adv_general');
    }//end destroy
}