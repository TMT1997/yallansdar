<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Helper;
use Session;

class CategoriesController extends InitController
{
    public function __construct() {
        parent::__construct();
        $parents = Category::whereParent_category_id(0)->orderBy('name_ar')->get();
        \View::share('parents', $parents);
    }

    public function getIndex(Request $request)
    {
        $no_results = 7;
        $categories = new Category;

        if($request->has('category_id')) {
            $categories = $categories->whereParent_category_id($request->get('category_id'));
        } else {
            $categories = $categories->whereParent_category_id(0);
        }
        if($request->has('no_results')) {
            $no_results = $request->get('no_results');
        }
        if($request->has('q')) {
            $q = trim($request->get('q'));
            $categories = $categories->where('name_ar','like','%'.$q.'%')
                ->orWhere('name_en','like','%'.$q.'%')
                ->orWhere('description','like','%'.$q.'%');
        }
        if($request->has('trashed') && $request->get('trashed')==1) {
            $categories = $categories->whereTrashed(1);
        } else {
            $categories = $categories->whereTrashed(0);
        }

        if(!$request->has('order_by')) {
            $categories = $categories->orderBy('sort', 'ASC');
        }

        $categories = $categories->paginate($no_results);
        return view('admin.categories.index', compact('categories'));
    }

    public function getTranslation(){
    	return view('admin.categories.translation');
    }

    public function getSub($category_id)
    {
        $parent_cat_id = $category_id;
        $childs = Category::whereParent_category_id($category_id)->whereTrashed(0)->get();
        
        return view('admin.categories.trans_form', compact('childs', 'parent_cat_id'));
    }

    public function postSave(Request $request)
    {
    	$data = $request->all();

    	$ids = array_keys($data['cats']);
    	$categories = Category::whereIn('id', $ids)->get();

    	foreach($categories as $category) {
    		$category->name_ar = $data['cats'][$category->id]['ar'];
    		$category->name_en = $data['cats'][$category->id]['en'];
    		$category->save();
    	}
    	return json_encode(['status'=>'success']);
    }

    public function postTrash(Request $request)
    {
        $category = Category::findOrFail($request->get('id'));
        $category->trashed=1;
        if($category->save()){
            return json_encode(['status'=>'success']);
        }
        return json_encode(['status'=>'error']);
    }

    public function getEdit($id){
        $category = Category::findOrFail($id);
        $tree['category'] = $category;
        $is_subsub = false;
        if($category->parent_category_id!=0) {
            $parent1 = Category::find($category->parent_category_id);
            if($parent1->parent_category_id!=0) {
                $parent2 = Category::find($parent1->parent_category_id);
                $parent2_childs = Category::whereParent_category_id($parent2->id)->select('id','parent_category_id','name_ar','name_en')->get();
                $tree['parent2_childs'] = $parent2_childs;
                $is_subsub = true;
                $tree['parentIDOflevel2'] = $parent2->id;
            }
        } 
        
        return view('admin.categories.edit', compact('category', 'is_subsub', 'tree'));
    }

    public function postUpdate(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $this->validate($request, Category::$adminRules);

        $requestData = $request->all();
        $media  = array('image','banner','menuimage','menuimage_ar');
        foreach($media as $file) {
            if($request->hasFile($file)) {
                $requestData[$file] = Helper::upload($request, $file);
            }
        }
        $result = $category->update($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Category Updated!');
        return redirect('admin/categories');
    }

    public function getQuickadd($parent_cat_id)
    {
        return view('admin.categories.quick_add', compact('parent_cat_id'));
    }

    public function postQuickadd(Request $request)
    {
        $category = new Category;
        $this->validate($request, Category::$adminRules);

        $requestData = $request->all();
        $result = $category->create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Category Updated!');
        return redirect('admin/categories');
    }
}
