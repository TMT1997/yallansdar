<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PickDescribe;
use Session;

class PickDescribeController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
    	$no_results = 7;
        $bs_developers = new PickDescribe;

        if($request->has('no_results')) {
            $no_results = $request->get('no_results');
        }
        if($request->has('q')) {
            $q = trim($request->get('q'));
            $bs_developers = $bs_developers->where('company','like','%'.$q.'%')
                ->orWhere('name','like','%'.$q.'%')
                ->orWhere('email','like','%'.$q.'%')
                ->orWhere('phone','like','%'.$q.'%')
                ->orWhere('jobtitle','like','%'.$q.'%');
        }

        $bs_developers = $bs_developers->orderBy('id', 'DESC')->paginate($no_results);
    	return view('admin.pick_describes.index', compact('bs_developers'));
    }

    public function show($id)
    {
    	$item = PickDescribe::findOrFail($id);
    	return view('admin.pick_describes.show', compact('item'));
    }

    public function destroy($id)
    {
        PickDescribe::destroy($id);
        Session::flash('flash_message', 'تم الحذف بنجاح');
        return redirect('admin/pick-describes');
    }
}
