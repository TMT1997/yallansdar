<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Country;
use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;

class CountriesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
    	$countries = Country::orderBy('id', 'DESC')->get();
    	return view('admin.countries.index', compact('countries'));
    }

    public function create()
    {
        return view('admin.countries.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name_english'=>'min:2|required','country_name'=>'min:2|required']);
        $requestData = $request->all();
        $model = Country::create($requestData);

        Session::flash('flash_message', 'تم اضافة الدولة بنجاح');
        return redirect('admin/countries');
    }

    public function show($id)
    {
        $country = Country::findOrFail($id);
        return view('admin.countries.show', compact('country'));
    }

    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('admin.countries.edit', compact('country'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name_english'=>'min:2|required','country_name'=>'min:2|required']);
        $requestData = $request->all();
        $country = Country::findOrFail($id);
        $country->update($requestData);

        Session::flash('flash_message', 'تم تعديل الدولة بنجاح');
        return redirect('admin/countries');
    }

    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        Country::destroy($id);
        Session::flash('flash_message', 'تم حذف الدولة بنجاح');
        return redirect('admin/countries');
    }
}
