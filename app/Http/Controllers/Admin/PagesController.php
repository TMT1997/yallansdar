<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;
use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;

class PagesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $req)
    {
        $pages = new Page;
        if($req->has('tag')) {
            $pages = $pages->whereTag($req->get('tag'));
        }        
        $pages = $pages->get();
        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {
        return view('admin.pages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'min:5|required']);
        $requestData = $request->all();
        
        if ($request->hasFile('image')) {
            $fileName = rand(111111111, 9999999999) . '.' . $request->file('image')->getClientOriginalExtension();

            $request->file('image')->move($this->upload_path, $fileName);
            $requestData['image'] = $fileName;
            Helper::setThumbnail($requestData['image'], $this->upload_path);
        }

        if ($request->hasFile('banner')) {
            $fileName = rand(111111111, 9999999999) . '.' . $request->file('banner')->getClientOriginalExtension();

            $request->file('banner')->move($this->upload_path, $fileName);
            $requestData['banner'] = $fileName;
            Helper::setThumbnail($requestData['banner'], $this->upload_path);
        }

        foreach(Page::$page_docs as $file) {
            if($request->hasFile($file)) {
                $requestData[$file] = Helper::upload($request, $file);
            }
        }

        $requestData['slug'] = str_slug($request->get('title'));
        $model = Page::create($requestData);

        Session::flash('flash_message', 'تم اضافة الصفحة بنجاح');
        return redirect('admin/pages');
    }

    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.pages.show', compact('page'));
    }

    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.pages.edit', compact('page'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['title' => 'min:5|required']);
        $requestData = $request->all();        

        if ($request->hasFile('image')) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = rand(111111111, 9999999999999999) . '.' . $extension;
            $request->file('image')->move($this->upload_path, $fileName);
            $requestData['image'] = $fileName;
            Helper::setThumbnail($requestData['image'], $this->upload_path);
        }

        if ($request->hasFile('banner')) {
            $fileName = rand(111111111, 9999999999999999) . '.' . $request->file('banner')->getClientOriginalExtension();
            $request->file('banner')->move($this->upload_path, $fileName);
            $requestData['banner'] = $fileName;
            Helper::setThumbnail($requestData['banner'], $this->upload_path);
        }

        if ($request->hasFile('pdf_file')) {
            $fileName = rand(1111111111111, 9999999999999999).'.'.$request->file('pdf_file')->getClientOriginalExtension();
            $request->file('pdf_file')->move($this->upload_path, $fileName);
            $requestData['pdf_file'] = $fileName;
        }

        $requestData['slug'] = str_slug($request->get('title'));
        $page = Page::findOrFail($id);
        $page->update($requestData);

        Session::flash('flash_message', 'تم تعديل الصفحة بنجاح');
        return redirect('admin/pages');
    }

    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $tag = $page->tag;
        Page::destroy($id);
        Session::flash('flash_message', 'تم حذف الصفحة بنجاح');
        return redirect('admin/pages');
    }
}