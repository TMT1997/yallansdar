<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersOld;
use App\User;
use App\UserProfile;
use Hash;
use Auth;
use Session;

class TransferController extends InitController
{
	public function __construct()
    {
        parent::__construct();
    }

	public function getTransferUsers()
	{
		return view('admin.scripts.transfer_users');
	}

    public function postTransferUsers()
    {
    	$count_added = 0;
    	$password = 123456;
		$users_old = UsersOld::all();
		foreach($users_old as $old)
		{
			$user = new User;
			$data = [
				'name'=>$old->full_name,
				'package'=>'Free',
				'company_name'=>$old->company_name,
				'exporter'=>$old->exporter_importer,
				'email'=>$old->email,
				'mobile'=>$old->phone_number,
				'address'=>$old->address,
				'email_verified'=>1,
				'active'=>1,
				'password'=>Hash::make($password),
				'created_at'=>$old->register_date,
			];
			$user_inserted = $user->create($data);
			if($user_inserted)
			{
				$count_added++;
				echo 'User '.$user_inserted->name.' added successfully <br>';

				$user_profile = UserProfile::init($user_inserted);
				$profile = [
					'user_id'=>$user_inserted->id,
					'first_name'=>$old->full_name,
					'company_name'=>$old->company_name,
					'primary_email'=>$old->email,
					'mobile'=>$old->phone_number,
					'picture'=>$old->picture,
					'destination'=>$old->brief,
					'address_line1'=>$old->address,
					'country_id'=>$old->country,
					'website'=>$old->website,
					'co_logo'=>$old->company_logo,
					'co_address_line1'=>$old->company_address,
					'facebook'=>$old->facebook,
					'twitter'=>$old->twitter,
				];
				if($user_profile->update($profile))
				{
					echo 'Profile '.$user_inserted->name.' added successfully <br>';
				}
			}
		}

		echo'<br>******* DONE ****************** Added: ' .$count_added;
    }

    public function getTransferProducts()
	{
		return view('admin.scripts.transfer_products');
	}

	public function postTransferProducts()
	{
		return 'start';
	}

}
