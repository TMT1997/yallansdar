<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use App\Category;
use Session;

class ServicesController extends InitController
{
    public function __construct() {
    	parent::__construct();
    }

    public function index(Request $request)
    {
        $services = new Service;

        if($request->has('status') && $request->get('status')!=0) {
            $services = $services->whereStatus($request->get('status'));
        }

        $services = $services->orderBy('id', 'DESC')->paginate(10);
        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        Service::create($requestData);

        Session::flash('flash_message', 'Service added!');
        return redirect('admin/services');
    }

    public function show($id)
    {
        $service = Service::whereId($id)->with('lang','category','images','user')->first();
        $category_parents = [];
        if($service->category) {
            $category_parents = Category::get_parents($service->category->id);
        }
        return view('admin.services.show', compact('service', 'category_parents'));
    }

    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.services.edit', compact('service'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $service = Service::findOrFail($id);
        $service->update($requestData);

        Session::flash('flash_message', 'Service updated!');
        return redirect('admin/services');
    }

    public function destroy($id)
    {
        Service::destroy($id);
        Session::flash('flash_message', 'Service deleted!');
        return redirect('admin/services');
    }
}
