<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Packages\PackageItem;
use App\Packages\Package;
use Session;
use View;

class PackageItemsController extends InitController
{
    public function __construct() {
        parent::__construct();

        $packages = Package::orderBy('sort_order', 'ASC')->get();
        View::share('packages', $packages);
    }

    public function index()
    {
        $packageitems = new PackageItem;

        if(request()->has('section') && request()->get('section')!='') {
            $packageitems = $packageitems->whereSection(request()->get('section'));
        }

        $packageitems = $packageitems->orderBy('id', 'DESC')->get();
        return view('admin.package-items.index', compact('packageitems'));
    }

    public function create()
    {
        return view('admin.package-items.create');
    }

    public function store(Request $request)
    {   
        $requestData = $request->all();
        foreach($requestData['package'] as $values)
        {
            $data = [];
            $data['name_ar']=$requestData['name_ar'];
            $data['name_en']=$requestData['name_en'];
            $data['description_ar']=$requestData['description_ar'];
            $data['description_en']=$requestData['description_en'];
            $data['section']=$requestData['section'];
            $data['code']=$requestData['code'];
            $data['values'] = serialize($values);

            PackageItem::create($data);
        }

        Session::flash('flash_message', 'PackageItem added!');
        return redirect('admin/package-items');
    }

    public function show($id)
    {
        $packageitem = PackageItem::findOrFail($id);
        return view('admin.package-items.show', compact('packageitem'));
    }

    public function edit($id)
    {
        $packageitem = PackageItem::findOrFail($id);
        $packageitem->values = unserialize($packageitem->values);
        
        $package['values'] = $packageitem->values;
        $packageitem->package = $package;

        return view('admin.package-items.edit', compact('packageitem', 'package'));
    }

    public function update($id, Request $request)
    {
        $requestData = $request->all();
        $packageitem = PackageItem::findOrFail($id);

        foreach($requestData['package'] as $values)
        {
            $data = [];
            $data['name_ar']=$requestData['name_ar'];
            $data['name_en']=$requestData['name_en'];
            $data['description_ar']=$requestData['description_ar'];
            $data['description_en']=$requestData['description_en'];
            $data['section']=$requestData['section'];
            $data['code']=$requestData['code'];
            $data['values'] = serialize($values);

            $packageitem->update($data);
        }

        Session::flash('flash_message', 'PackageItem updated!');
        return redirect('admin/package-items');
    }

    public function destroy($id)
    {
        PackageItem::destroy($id);

        Session::flash('flash_message', 'PackageItem deleted!');

        return redirect('admin/package-items');
    }
}