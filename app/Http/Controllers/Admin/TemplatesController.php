<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Template;
use App\Helper;
use Session;

class TemplatesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $templates = Template::where('is_free', 'LIKE', "%$keyword%")
				->orWhere('type', 'LIKE', "%$keyword%")
				->orWhere('category_id', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('color', 'LIKE', "%$keyword%")
				
                ->paginate($perPage);
        } else {
            $templates = Template::paginate($perPage);
        }
        return view('admin.templates.index', compact('templates'));
    }

    public function create()
    {
        return view('admin.templates.create');
    }

    public function store(Request $request)
    {        
        $requestData = $request->all();
        
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }

        Template::create($requestData);

        Session::flash('flash_message', 'Template added!');
        return redirect('admin/templates');
    }

    public function show($id)
    {
        $template = Template::findOrFail($id);
        return view('admin.templates.show', compact('template'));
    }

    public function edit($id)
    {
        $template = Template::findOrFail($id);

        return view('admin.templates.edit', compact('template'));
    }

    public function update($id, Request $request)
    {        
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }

        $template = Template::findOrFail($id);
        $template->update($requestData);

        Session::flash('flash_message', 'Template updated!');
        return redirect('admin/templates');
    }

    public function destroy($id)
    {
        Template::destroy($id);

        Session::flash('flash_message', 'Template deleted!');
        return redirect('admin/templates');
    }
}