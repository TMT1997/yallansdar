<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Testimontial;
use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;

class TestimontialsController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
    	$testimontials = Testimontial::orderBy('id', 'DESC')->get();
    	return view('admin.testimontials.index', compact('testimontials'));
    }

    public function create()
    {
        return view('admin.testimontials.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'min:3|required']);
        $requestData = $request->all();
        
        if ($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        $model = Testimontial::create($requestData);

        Session::flash('flash_message', 'تم اضافة الصفحة بنجاح');
        return redirect('admin/testimontials');
    }

    public function show($id)
    {
        $testimontial = Testimontial::findOrFail($id);
        return view('admin.testimontials.show', compact('testimontial'));
    }

    public function edit($id)
    {
        $testimontial = Testimontial::findOrFail($id);
        return view('admin.testimontials.edit', compact('testimontial'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'min:3|required']);
        $requestData = $request->all();        

        if ($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }

        $requestData['slug'] = str_slug($request->get('title'));
        $testimontial = Testimontial::findOrFail($id);
        $testimontial->update($requestData);

        Session::flash('flash_message', 'تم تعديل الصفحة بنجاح');
        return redirect('admin/testimontials');
    }

    public function destroy($id)
    {
        $testimontial = Testimontial::findOrFail($id);
        Testimontial::destroy($id);
        Session::flash('flash_message', 'تم حذف الصفحة بنجاح');
        return redirect('admin/testimontials');
    }
}
