<?php

namespace App\Http\Controllers\Admin;

use App\Messages\Message;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductEnquiry;

class MessagesController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
    	$messages = ProductEnquiry::join('messages', 'messages.id', '=', 'product_enquiries.message_id')
   ->orderBy('id', 'DESC')->with('product','from_user','to_user')->get(['product_enquiries.*','messages.status']);;

    	return view('admin.product_enquiries.index', compact('messages'));
    }

    public function destroy($id)
    {
        ProductEnquiry::destroy($id);
        Session::flash('flash_message', 'Product Enquiry Message has been deleted!');
        return redirect('admin/messages');
    }
    /*
     * function to delete message from the db
     * input: message id
     * output : none
     */
    public function delete_message($id){
        Message::destroy($id);
//        Session::flash('flash_message', 'Message has been deleted!');
        return redirect('admin/products_messages');
    }

    /*
     * function to display the whole messagess that send from/to importer/exporter
     * input: none
     * output : a view with the whole pending messages
     */
    public function get_messages(){
        //get all the messages with status 0 which is pending for approved by the user
       $messages = Message::whereFrom_admin(0)->whereStatus(0)->whereIsProduct(0)->orderBy('created_at','DESC')->with('user_from','user_to' , 'attachments')->get();

       return view('admin.messages.show_all_messages',compact('messages'));
    }
     /*
     * function to make notification read
     * input: none
     * output : set all notification read
     */
    public function set_read_notification(Request $req)
    {
        \DB::table('notifications')
            ->where('read', 0)
            ->update(['read' => 1]);
        return "done";
    }
     /*
     * function to make notification call read
     * input: none
     * output : set all notification read only call
     */
    public function set_read_call_notification(Request $req)
    {
        \DB::table('messages')
            ->where('read', 0)
            ->where('subject','من فضلك كلمني')
            ->update(['read' => 1]);
        return "done";
    }
    /*
     * function to get all notification
     * input: none
     * output : get all notification
     */
    public function get_all_notification(Request $req)
    {
        if($req->has('action_filter')) {
            $filter = $req->get('action_filter');
            $notifications = \App\Notification::whereSection($filter)->OrderBy('id','DESC')->get();
        }
        else
            $notifications = \App\Notification::OrderBy('id','DESC')->get();
        return view('admin.messages.show_all_notification',compact('notifications'));
    }
    /*
     * function to get archive notification
     * input: none
     * output : get all archive for specific user notification
     */
    public function get_archive_notification($user_id)
    {
        $notifications = \App\Notification::whereUser_id($user_id)->OrderBy('id','DESC')->get();
        return view('admin.messages.show_archive_notification',compact('notifications'));
    }
}
