<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Packages\UserPackage;
use App\Packages\Package;
use Session;
use View;

class UserPackagesController extends InitController
{
    public function __construct()
    {
    	parent::__construct();

        $packages = Package::lists('name_ar', 'id');
        View::share('packages', $packages);
    }

    public function index(Request $request)
    {
        $package_upgrades = new UserPackage;

        if($request->has('confirmed') && $request->get('confirmed')!='all') {
            $package_upgrades = $package_upgrades->whereConfirmed($request->get('confirmed'));
        }

        if($request->has('paid') && $request->get('paid')!='all') {
            $package_upgrades = $package_upgrades->wherePaid($request->get('paid'));
        }

        if($request->has('package_id') && $request->get('package_id')!='all') {
            $package_upgrades = $package_upgrades->wherePackage_id($request->get('package_id'));
        }

        $package_upgrades = $package_upgrades->orderBy('id', 'DESC')->with('user', 'package');

        $package_upgrades = $package_upgrades->get();

        return view('admin.package_upgrades.index', compact('package_upgrades'));
    }

    public function create() {
    }

    public function store(Request $request) {
    }

    public function show($id) {
        $user_package = UserPackage::whereId($id)->with('user')->first();
        return view('admin.package_upgrades.show', compact('user_package'));
    }

    public function edit($id) {
        $user_package = UserPackage::findOrFail($id);
        return view('admin.package_upgrades.edit', compact('user_package'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'package_id' => 'required',
			'user_id' => 'required',
		]);
        $requestData = $request->all();
        $user_package = UserPackage::findOrFail($id);
        $user_package->update($requestData);

        Session::flash('flash_message', 'UserPackage updated!');
        return redirect('admin/package_upgrades');
    }

    public function destroy($id)
    {
        UserPackage::destroy($id);
        Session::flash('flash_message', 'User Package deleted!');
        return redirect('admin/package_upgrades');
    }
}