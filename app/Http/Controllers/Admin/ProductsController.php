<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Messages\Message;
use Illuminate\Http\Request;

use App\Product;
use App\Category;
use Session;

class ProductsController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $products = new Product;

        if($request->has('status') && $request->get('status')!=0) {
            $products = $products->whereApproved($request->get('status'));
        }
        if($request->has('end_date'))
        {
            $end_date   = date('Y-m-d', strtotime($request->get('end_date')));
            $start_date = date('Y-m-d', strtotime($request->get('from_date')));
            if($request->get('end_date')=="all")
            {

            }
            else
            {
                $products = $products->whereDate('products.created_at','<=',$end_date)
                        ->whereDate('products.created_at','>=',$start_date);
            }
        }
        $products = $products->orderBy('id', 'DESC');
        $products = $products->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();
        Product::create($requestData);

        Session::flash('flash_message', 'Product added!');
        return redirect('admin/products');
    }

    public function show($id)
    {
        $product = Product::whereId($id)->with('lang','category','images','user','reviews', 'group')->first();

        $category_parents = [];
        if($product->category) {
            $category_parents = Category::get_parents($product->category->id);
        }

        return view('admin.products.show', compact('product', 'category_parents'));
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.edit', compact('product'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'category_id' => 'required',
			'user_id' => 'required',
			'name' => 'required'
		]);
        $requestData = $request->all();

        $product = Product::findOrFail($id);
        $product->update($requestData);

        Session::flash('flash_message', 'Product updated!');
        return redirect('admin/products');
    }

    public function destroy($id)
    {
        Product::destroy($id);
        Session::flash('flash_message', 'Product deleted!');
        return redirect('admin/products');
    }

    public function product_message(){
        //select the messages that send from exporter to importer
        $messages = Message::whereStatus(0)->whereIsProduct(1)->with('user_from','user_to' , 'attachments')->orderBy('id','desc')->get();

        return view('admin.products_messages.index' ,compact('messages'));
    }
    /*
     * send the mails to both importer and exporter
     * input :
     * output :
     */
    public function send_products_mail(Request $request)
    {
        $status = $request->get('status');

        if ($status != '1') return 'تمت العملية بنجاح';    // if it's not approved then we mustn't do anything

        $mess_id = $request->get('id');
        //get the message
        $data = Message::whereId($mess_id)->with('user_to', 'user_from')->first();

        $msgData = [
            'to_user_id'=>$data->user_from->id,
            'status'=>'1', // it doesn't need approval from admin
            'from_user_id'=>$data->user_to->id,
            'from_email'=>$data->user_to->email,
            'from_name'=>$data->user_to->name,
            'subject'=>'تم قبول عرض السعر من الادمن',
            'body'=>trans('front.quote_succ'),
        ];

        //send the message to the exporter
        $message = \App\Messages\Message::create($msgData);


        //Send mail message


        $view = 'front.emails.Products.exporter_rec';
        $mail['mail_to'] = $data->user_to->email; // the mail of the exporter
        $mail['subject'] = str_replace('لقد تلقيت رسالة عن منتجك الذي عرضته', 'You received a message about your product', $data['subject']);
        $mail['mail_from'] = $data->user_from->email;
        $mail['name_from'] = $data->user_from->name;

        \Mail::send($view, ['quote' => $data, 'deal' => $data, 'data' => $data], function ($message) use ($mail) {
            $message->from($mail['mail_from'], $mail['name_from']);
            $message->to($mail['mail_to']);
            $message->replyTo($mail['mail_from'], $mail['name_from']);
            $message->subject($mail['subject']);
        });
        //send to the importer that the message successfuly send
        $view = 'front.emails.Products.importer_rec';
        $mail['mail_to'] = $data->user_from->email; // the exporter mail
        $mail['subject'] = str_replace('لقد تم توصيل الرسالة للأدمن بنجاح', 'The message has been successfully delivered to the exporter', $data['subject']);
        $mail['mail_from'] = 'admin@yallansadar.com';
        $mail['name_from'] = 'YallaNsadr';

        \Mail::send($view, ['quote' => $data, 'deal' => $data, 'data' => $data], function ($message) use ($mail) {
            $message->from($mail['mail_from'], $mail['name_from']);
            $message->to($mail['mail_to']);
            $message->replyTo($mail['mail_from'], $mail['name_from']);
            $message->subject($mail['subject']);
        });
        return trans('admin.confirm_send_quote');
    }
}



