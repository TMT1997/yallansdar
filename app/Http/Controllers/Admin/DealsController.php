<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Deals\Deal;
use App\Deals\Partnership;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use Session;
use App\Deals\Quote;

class DealsController extends InitController
{
    public function __construct(){
    	parent::__construct();
    }

    public function index(Request $request){
    	$deals = new Deal;

    	if($request->has('status') && $request->get('status')!=0) {
            $deals = $deals->whereStatus($request->get('status'));
        }
        //----------------------------------
        if($request->has('end_date'))
        {
            $end_date   = date('Y-m-d', strtotime($request->get('end_date')));
            $start_date = date('Y-m-d', strtotime($request->get('from_date')));
            if($request->get('end_date')=="all")
            {

            }
            else
            {
                $deals = $deals->whereDate('deals.created_at','<=',$end_date)
                        ->whereDate('deals.created_at','>=',$start_date);
            }
        }
        //----------------------------------
        $deals = $deals->orderBy('id', 'DESC');
        $deals = $deals->get();

        return view('admin.deals.index', compact('deals'));
    }
    /*
     * Get Pending Deal quotes
     * Input : None
     * Output : A view with the data in it
     */
    public function get_deals_quotes(){
        $quotes = new Quote;
        $quotes = $quotes->whereStatus('0')->with('user_from');
        $quotes = $quotes->orderBy('id', 'DESC');
        $quotes = $quotes->get();

        return view('admin.deals.quotes', compact('quotes'));
    }
    /*
     * Sending the quotes to the importer and sending the email for both of them
     * Input : quote ID , Status of the quote
     * Output : None
     */
    public function send_quotes(Request $request){

        $status = $request->get('status');

        if($status != '1') return 'تمت العملية بنجاح';    // if it's not approved then we mustn't do anything

        $quote_id = $request->get('id');

        //get the quote
        $data = Quote::whereId($quote_id)->first();

        //create message to send to the importer
        $msgData = [
            'to_user_id'=>$data['user_id'],
            'status'=>'1', // it doesn't need approval from admin
            'from_user_id'=>$data['from_user_id'],
            'from_email'=>$data['email'],
            'from_name'=>$data['name'],
            'subject'=>$data['subject'],
            'body'=>$data['body'],
        ];


        $message = \App\Messages\Message::create($msgData);
        //return $message;

        if($message)
        {
            if(!empty($data['attachment'])){
                $attachmentData = [
                    'message_id'=>$message->id,
                    'file'=>$data['attachment'],
                ];
                \App\Messages\Attachment::create($attachmentData);
            }

            //Send mail message
            //If importer data inserted then send to importer, otherwise send to user account added this RFQ deal
            $deal = QuoteRequest::whereId($data['quote_request_id'])->first();

            $view = 'front.emails.deals.rfq_mail';
            $mail['mail_to'] = (!empty($deal->im_email)) ? $deal->im_email : $deal->user->email;
            $mail['subject'] = str_replace('اقدم لكم عرض سعر بخصوص المنتج','We provide you a quotation for product',$data['subject']);
            $mail['mail_from'] = $data['email'];
            $mail['name_from'] = $data['name'];

            \Mail::send($view,['quote'=>$data,'deal'=>$deal,'data'=>$data],function($message) use($mail)
            {
                $message->from($mail['mail_from'], $mail['name_from']);
                $message->to($mail['mail_to']);
                $message->replyTo($mail['mail_from'], $mail['name_from']);
                $message->subject($mail['subject']);
            });
            //send to the exporter that the quotes successfuly send
            $view = 'front.emails.deals.rfq_mail_exporter';
            $mail['mail_to'] = $data['email']; // the exporter mail
            $mail['subject'] = str_replace('لقد تم ارسال عرض السعر بنجاح','The bid was successfully sent',$data['subject']);
            $mail['mail_from'] = 'admin@yallansadar.com';
            $mail['name_from'] = 'Yalla Nsadar';

            \Mail::send($view,['quote'=>$data,'deal'=>$deal,'data'=>$data],function($message) use($mail)
            {
                $message->from($mail['mail_from'], $mail['name_from']);
                $message->to($mail['mail_to']);
                $message->replyTo($mail['mail_from'], $mail['name_from']);
                $message->subject($mail['subject']);
            });
//******************************************************************************************************************
        }
        return trans('admin.confirm_send_quote');
    }
    public function show($id)
    {
        $deal_row = Deal::findOrFail($id);
        $view = '';
        if($deal_row->section=='sell_offer')
        {
            $view = 'offer';
            $deal = SellOffer::whereId($deal_row->record_id)->with('lang', 'product')->first();
            if($deal) {
                $deal->market_country = explode(',', $deal->market_country);
                $deal->payment_type = explode(',', $deal->payment_type);
            }
        }
        else if($deal_row->section=='quote_request')
        {
            $view = 'request_quote';
            $deal = QuoteRequest::whereId($deal_row->record_id)->with('lang', 'images')->first();
            $deal->supplier_location = explode(',', $deal->supplier_location);
            $deal->payment_terms = explode(',', $deal->payment_terms);
        }
        else if($deal_row->section=='partnership')
        {
            $view = 'partnership';
            $deal = Partnership::whereId($deal_row->record_id)->with('lang', 'images', 'category')->first();
            if($deal){
                $deal->countries = explode(',', $deal->countries);
            }
        }


        if(isset($deal) && isset($deal->lang)) {
            $lang=[];
            foreach($deal->lang as $item) {
                $lang[$item->language_id] = $item;
            }
            $deal->lang = $lang;
        }

        return view('admin.deals.show.'.$view, compact('deal'));
    }

    public function destroy($id)
    {
        $deal = Deal::find($id);
        if(!$deal) {
            return redirect()->to('admin/deals');
        }

        if($deal->section=='sell_offer') {
            SellOffer::destroy($deal->record_id);
        } else if($deal->section=='quote_request') {
            QuoteRequest::destroy($deal->record_id);
        } else if($deal->section=='partnership') {
            Partnership::destroy($deal->record_id);
        }

        Deal::destroy($id);

        Session::flash('flash_message', 'Deal deleted!');
        return redirect('admin/deals');
    }
    /*
     * get all the description of deals quotes and put it into view
     * input : quote id
     * output : view with all the information attached to that quote
     */
    public function quote_details($id){
        $quote = new Quote;
        $quote = $quote->whereId($id)->with('request')->first();
        $quote->payment_type = explode(',', $quote->payment_type);

        return view('admin.deals.show.quote' , compact('quote'));
    }
    /*
     * delete specific quote
     * input : quote id
     * output : none
     */
    public function destroy_quote(Request $request){
        $id = $request->get('qid');
        $quote = Quote::find($id);
        if(!$quote) {
            return redirect()->to('admin/getquotes');
        }



        $quote::destroy($id);

        Session::flash('flash_message', 'Quote deleted!');
        return redirect('admin/getquotes');
    }
}