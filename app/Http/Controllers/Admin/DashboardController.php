<?php

namespace App\Http\Controllers\Admin;

use App\Deals\Quote;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Service;
use App\User;
use App\UserProfile;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use App\Deals\Partnership;
use App\UserWebsite;
use App\Messages\Message;
use App\Notification;
class DashboardController extends InitController
{
    public function __construct() {
    	parent::__construct();
    }

    public function getIndex()
    {
    	$pending['products'] = Product::whereApproved(1)->count();
    	$pending['services'] = Service::whereStatus(1)->count();
    	$pending['profiles'] = UserProfile::whereProfile('temporary')->whereStatus(0)->count();
    	$pending['sell_offers'] = SellOffer::whereStatus(1)->count();
    	$pending['quote_requests'] = QuoteRequest::whereStatus(1)->count();
    	//pending box for deals_quotes
        $pending['deals_quotes'] = Quote::whereStatus(0)->count();
    	$pending['partnerships'] = Partnership::whereStatus(1)->count();
        $pending['userwebsites'] = UserWebsite::whereStatus(1)->count();
        $pending['product_enquiries'] = \App\ProductEnquiry::whereRead(0)->count();
        $pending['package_upgrades'] = \App\Packages\UserPackage::whereConfirmed(0)->count();
        $pending['products_message'] = Message::whereStatus(0)->whereIsProduct(1)->count(); // pending box for products messages
        $pending['messages'] = Message::whereStatus(0)->whereIsProduct(0)->count(); // pending box the whole messages expect the product messages
        $dash_items = [
            'products'=>['label'=>'Products', 'url'=>'products?status=1'],
            'services'=>['label'=>'Services', 'url'=>'services?status=1'],
            'profiles'=>['label'=>'User Profiles', 'url'=>'profiles?status=0'],
            'sell_offers'=>['label'=>'Sell Offers', 'url'=>'deals?status=1#fsell_offer'],
            'quote_requests'=>['label'=>'Request for Quotes', 'url'=>'deals?status=1#fquote_request'],
            'partnerships'=>['label'=>'Partnerships', 'url'=>'deals?status=1#fpartnership'],
            'userwebsites'=>['label'=>'User Websites', 'url'=>'userwebsites?status=1'],
            'product_enquiries'=>['label'=>'Product Enquiries', 'url'=>'messages'],
            'package_upgrades'=>['label'=>'Package Upgrades', 'url'=>'package_upgrades'],
            'deals_quotes'=>['label'=>'Deals Quotes', 'url'=>'getquotes'],
            'products_message'=>['label'=>'Products Messages', 'url'=>'products_messages'],
            'messages'=>['label'=>'Messages', 'url'=>'pending_messages'],
        ];
    	return view('admin.dashboard', compact('pending', 'dash_items'));
    }
}
