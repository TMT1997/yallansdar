<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;
use Session;

class PermissionsController extends Controller
{

    public function index()
    {
        $permissions = Permission::paginate(15);

        return view('admin.permissions.index', compact('permissions'));
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);
        Permission::create($request->all());

        Session::flash('flash_message', 'Permission added!');
        return redirect('admin/permissions');
    }

    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.show', compact('permission'));
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.edit', compact('permission'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        Session::flash('flash_message', 'Permission updated!');

        return redirect('admin/permissions');
    }

    public function destroy($id)
    {
        Permission::destroy($id);
        Session::flash('flash_message', 'Permission deleted!');
        return redirect('admin/permissions');
    }
}