<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use App\Packages\Package;

class PackagesController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $packages = new Package;
        $packages = $packages->orderBy('sort', 'ASC');
        $packages = $packages->paginate(25);
        return view('admin.packages.index', compact('packages'));
    }

    public function create() {
        return view('admin.packages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'name_ar' => 'required',
            'name_en' => 'required'
		]);
        $requestData = $request->all();
        Package::create($requestData);

        Session::flash('flash_message', 'Package added!');
        return redirect('admin/packages');
    }

    public function show($id) {
        $package = Package::findOrFail($id);
        return view('admin.packages.show', compact('package'));
    }

    public function edit($id) {
        $package = Package::findOrFail($id);
        return view('admin.packages.edit', compact('package'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'name_ar' => 'required',
            'name_en' => 'required'
		]);
        $requestData = $request->all();
        
        $package = Package::findOrFail($id);
        $package->update($requestData);

        Session::flash('flash_message', 'Package updated!');
        return redirect('admin/packages');
    }

    public function destroy($id) {
        Package::destroy($id);
        Session::flash('flash_message', 'Package deleted!');
        return redirect('admin/packages');
    }
}
