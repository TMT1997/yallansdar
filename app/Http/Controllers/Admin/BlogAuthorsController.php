<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Session;
use View;
use Image;
use File;
use App\BlogAuthor;
use App\Helper;

class BlogAuthorsController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $req)
    {
        $blog_authors = new BlogAuthor;
        $blog_authors = $blog_authors->orderBy('id', 'DESC')->get();
        return view('admin.blog_authors.index', compact('blog_authors'));
    }

    public function create() {
        return view('admin.blog_authors.create');
    }

    public function store(Request $request) {
        $this->validate($request, BlogAuthor::$rules);

        $data = $request->all();            
        if ($request->hasFile('image')) {
            $data['image'] = Helper::upload($request, 'image');
        }
        $data['slug'] = str_slug($data['name_ar']);

        $author = BlogAuthor::create($data);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$author]);
        }

        Session::flash('flash_message', 'تم اضافة الخبر بنجاح');
        return redirect('admin/blog_authors');
    }

    public function show($id) {
        $row = BlogAuthor::findOrFail($id);
        return view('admin.blog_authors.show', compact('row'));
    }

    public function edit($id) {
        $article = BlogAuthor::findOrFail($id);
        return view('admin.blog_authors.edit', compact('article'));
    }

    public function update($id, Request $request) {
        $this->validate($request, BlogAuthor::$rules);
        $requestData = $request->all();

        if ($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        $requestData['slug'] = str_slug($request->get('name_ar'));
        $article = BlogAuthor::findOrFail($id);
        $article->update($requestData);

        Session::flash('flash_message', 'تم تعديل الخبر بنجاح');
        return redirect('admin/blog_authors');
    }

    public function destroy($id) {
        $article = BlogAuthor::findOrFail($id);
        BlogAuthor::destroy($id);

        Session::flash('flash_message', 'تم حذف الخبر بنجاح');
        return redirect('admin/blog_authors');
    }
}