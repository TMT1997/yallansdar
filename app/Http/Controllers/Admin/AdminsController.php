<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Session;

class AdminsController extends InitController
{
    public function index()
    {
        $users = User::whereAdmin(1)->paginate(8);
        return view('admin.admins.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        return view('admin.admins.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required',
        ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->get('password'));

        unset($data['access_pages']);
        $user = User::create($data);

        if($request->get('super_admin')==0 && $request->has('access_pages')) {
            if(!empty($request->get('access_pages'))) {
                foreach ($request->get('access_pages') as $acc_page) {
                    $new_page = \App\UserAccessPage::create([
                        'page'=>$acc_page,
                        'user_id'=>$user->id,
                        'by_user_id'=>auth()->id(),
                    ]);
                }
            }
        }

        $user->admin = 1;
        $user->active = 1;
        if(!$user->save()) {
            return 'Error setting Admin User';
        }

        Session::flash('flash_message', 'Admin User added!');
        return redirect('admin/admins');
    }

    public function show($id, Request $request)
    {
        $user = User::find($id);
        if($request->ajax()) {
            return view('admin.admins.perview', compact('user'));    
        }
        return view('admin.admins.show', compact('user'));
    }

    public function edit($id)
    {
        $roles = Role::select('id', 'name', 'label')->get();

        $user = User::with('roles')->with('access_pages')->select('id', 'name', 'email', 'super_admin')->findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }

        return view('admin.admins.edit', compact('user', 'roles', 'user_roles'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $requestData = $request->all();
        $user = User::findOrFail($id);

        \App\UserAccessPage::whereUser_id($id)->delete();
        if($request->has('access_pages')) {
            if(!empty($request->get('access_pages'))) {
                foreach ($request->get('access_pages') as $acc_page) {
                    $new_page = \App\UserAccessPage::create([
                        'page'=>$acc_page,
                        'user_id'=>$id,
                        'by_user_id'=>auth()->id(),
                    ]);
                }
            }
            unset($requestData['access_pages']);
        }

        $user->update($requestData);

        $user->roles()->detach();
        if($request->get('roles')) {
            foreach ($request->get('roles') as $role) {
                $user->assignRole($role);
            }
        }

        Session::flash('flash_message', 'Admin User updated!');
        return redirect('admin/admins');
    }

    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('flash_message', 'Admin User deleted!');
        return redirect('admin/admins');
    }
}