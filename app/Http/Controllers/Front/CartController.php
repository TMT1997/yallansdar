<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\YallaCart;
use App\Helper;
use App\Service;
use App\User;
use Auth;
use Session;

class CartController extends InitController
{
    protected static $cart_name;

    public function __construct() {
    	parent::__construct();
        self::$cart_name = 'yalla_cart';
    }

    //View Cart with enquiry form
    public function getIndex(Request $request, $product_id=null)
    {
        $cart = $request->session()->get(self::$cart_name);
        $more_products = Product::whereActive(1)->whereApproved(2)->orderBy('id','DESC')->take(12)->get();
        $services = Service::whereActive(1)->whereStatus(2)->orderBy('id','DESC')->with('lang')->take(6)->get();

        return view('front.products.enquiry.post', compact('cart', 'more_products', 'services'));
    }

    //Add to Cart
    public function postIndex(Request $request)
    {
        $product_id = $request->get('product_id');
        $product = Product::find($product_id);
        $user_id = $product->user_id;

        if($request->session()->has(self::$cart_name))
        {
            $current_items = $request->session()->pull(self::$cart_name);

            if($request->session()->has('yalla_cart.'.$user_id)) {
                $current_items[$user_id][] = $product;
                $request->session()->put(self::$cart_name, $current_items);
            } else {
                $current_items[$user_id][] = $product;
                $request->session()->put(self::$cart_name, $current_items);
            }
        } else {
            $cart_item[$user_id][] = $product;
            $request->session()->put(self::$cart_name, $cart_item);
        }

        if($request->has('quick_add') || $request->ajax()) {
            return json_encode(['status'=>1]);
        }
        return redirect('cart');
    }

    public function getRemove(Request $request, $product_id)
    {
        $cart = $request->session()->get(self::$cart_name);
        $new_cart = [];
        if($cart)
        {
            foreach($cart as $user_id=>$items)
            {
                foreach($items as $item) {
                    if($item['id']==$product_id) {
                        continue;
                    }
                    $new_cart[$user_id][] = $item;
                }
            }
            $request->session()->put(self::$cart_name, $new_cart);
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success']);
        }

        return redirect()->back();
    }

    public function getFlush(Request $request) {
        $request->session()->flush(self::$cart_name);
        return redirect()->back();
    }

    public function getEnquiry() {
        return redirect()->to('cart');
    }

    public static function generateRandomPassword() {
        return rand(10000, 99999);
    }

    public function postEnquiry(Request $request)
    {
        $flash_message = '';
        $data = $request->all();
        $validation_rules = [
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'body'=>'required',
            'attachment'=>'file|max:3072|mimes:jpg,jpeg,bmp,png,gif,doc,docs,xls,xlsx,pdf,txt',
        ];
        if(Auth::check()) {
            unset($validation_rules['name']);
            unset($validation_rules['email']);
            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['from_user_id'] = Auth::id();
        }

        $this->validate($request, $validation_rules);

        foreach($data['user_ids'] as $user_id)
        {
            $from_user_id = (isset($data['from_user_id'])) ? $data['from_user_id']:0;
            $msgData = [
                'to_user_id'=>$user_id,
                'from_user_id'=>$from_user_id,
                'is_product'=>'1',
                'from_email'=>$data['email'],
                'from_name'=>$data['name'],
                'subject'=>$data['subject'],
                'body'=>$data['body'],
            ];

            $message = \App\Messages\Message::create($msgData);
            if($message)
            {
                if($request->hasFile('attachment')) {
                    $attachment = Helper::upload($request, 'attachment', false);
                    $attachmentData = [
                        'message_id'=>$message->id,
                        'file'=>$attachment,
                    ];
                    \App\Messages\Attachment::create($attachmentData);
                }
                $flash_message = trans('front.enquiry_sent');
            } else {
                Session::flash('alert', 'danger');
                $flash_message = trans('front.error_occured');
            }            
        }

        if($request->has('createAccountOnPost')) {
            $user_data = [
                'email'=>$data['email'],
                'name'=>$data['name'],
                'password'=>self::generateRandomPassword()
            ];
            User::createVisitorAccount($user_data);
            $flash_message .= ', <br>'.trans('front.account_created_also');
            
        }
        Session::flash('flash_message', $flash_message);
        return view('front.products.enquiry.status');
    }

    public function getStatus() {
        return view('front.products.enquiry.status');
    }
}
