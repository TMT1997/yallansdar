<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\User;

class CompaniesController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getSite($company_id, $slug)
    {
    	$company = Company::whereId($company_id)->with('user_products')->first();
    	return view('front.companies.website.index', compact('company'));
    }

    public function getContact($user_id)
    {
        $user = User::whereId($user_id)->with('profile')->first();
        return view('front.companies.contact_details', compact('user'));
    }

    public function getContactInfo($slug, $id=null)
    {
        $user = new User;
        if(is_numeric($slug)) {
            $user = $user->whereId($slug);
        } else {
            $user = $user->whereSlug($slug);
        }
    	$user = $user->with('profile')->first();
    	return view('front.companies.contact_page', compact('user'));
    }
}