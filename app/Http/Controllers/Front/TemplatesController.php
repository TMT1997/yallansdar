<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Product;
use App\Slider;
use App\Company;
use App\User;
use App\Service;
use App\Template;
use App\UserWebsite;
use App\UserWebsitePage;
use App\UserTradeInfo;

use Auth;
use View;
use Session;

class TemplatesController extends InitController
{
	public $user_id;
	public $cssdir;
    public $tempdir;
    public $skin;
    public $domain;

    public function __construct(Request $request)
    {
    	parent::__construct();
//        $this->middleware(['website_launch']);

        $domain = $request->segment(2);

        $website = UserWebsite::whereDomain($domain)->with('template', 'user')->first();
        if(!$website->template) return;
//        dd($website->template);
        if($website)
        {
            $this->tempdir = 'premium';
            $this->cssdir = 'default';
            $this->skin = $website->template->skin;
            $this->domain = $website->domain;
            $this->user_id = $website->user->id;
            $user = $website->user;

            $cart = $request->session()->get('yalla_cart');
            $top_categories = Category::whereHp_top(1)->take(8)->get();
            $banners = UserWebsitePage::whereUser_id($this->user_id)->whereActive(1)->whereType('banner')->get();
            $user_profile = $user->profile;

            View::share('user', $user);
            View::share('user_profile', $user_profile);
            View::share('website', $website);
            View::share('cart', $cart);
            View::share('template', $this->tempdir);
            View::share('templatecss', $this->cssdir);
            View::share('skin', $this->skin);
            View::share('domain', $this->domain);
            View::share('top_categories', $top_categories);
            View::share('banners', $banners);
        }
    }

    public function getIndex(Request $request)
    {

        if(!$this->domain) {
            return 'Website Store not found';
        }
        if($this->domain=='master') {
            $slider = UserWebsitePage::whereType('Slider')->whereActive(1)->whereInslider(1)->orderBy('id', 'DESC')->get();
            $menu = UserWebsitePage::whereActive(1)->whereInmenu(1)->orderBy('id', 'DESC')->get();
            $products = Product::whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->with('lang','category')->take(16)->get();
            $services = Service::whereActive(1)->whereStatus(2)->orderBy('id','DESC')->with('lang')->take(6)->get();
        } else {
            $slider = UserWebsitePage::whereUser_id($this->user_id)->whereType('Slider')->whereActive(1)->whereInslider(1)->orderBy('id', 'DESC')->get();
            $menu = UserWebsitePage::whereUser_id($this->user_id)->whereActive(1)->whereInmenu(1)->orderBy('id', 'DESC')->get();
            $products = Product::whereUser_id($this->user_id)->whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->with('lang', 'category')->take(16)->get();
            $services = Service::whereUser_id($this->user_id)->whereActive(1)->whereStatus(2)->orderBy('id','DESC')->with('lang')->take(6)->get();
        }

    	return view('templates.'.$this->tempdir.'.index', compact('slider','menu', 'products', 'services'));
    }

    public function getProducts()
    {

        $products = new Product;
        $products = $products->whereUser_id($this->user_id)->whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->with('lang', 'category');

        $services = Service::whereUser_id($this->user_id)->whereActive(1)->whereStatus(2)->orderBy('id','DESC')->with('lang')->paginate(9);

        $product_cats=[];
        foreach($products->get() as $product):
        	if($product->category)
        		$product_cats[] = $product->category;
        endforeach;

        $products = $products->paginate(9);
    	return view('templates.'.$this->tempdir.'.products.list', compact('products', 'services'));
    }

    public function getProduct(Request $req)
    {

        $product_id = $req->get('id');
        $product = Product::whereId($product_id)->whereActive(1)->whereApproved(2)->with('lang','category','images','user','reviews')->first();
        if(!$product) {
            Session::flash('alert', 'warning');
            Session::flash('flash_message', trans('front.product_not_available'));
            return redirect('products');
        }
        $supplier_products = Product::whereActive(1)->whereUser_id($product->user_id)->orderBy('id', 'DESC')->take(8)->with('lang')->get();

        $category_ids = [];
        $related_products = [];
        $category_parents = [];
        if($product->category) {
            $category_ids = Category::whereParent_category_id($product->category->parent_category_id)->lists('id');
            $related_products = Product::whereIn('category_id',$category_ids)->whereActive(1)->orderBy('id','DESC')->take(8)->with('lang')->get();
            $category_parents = Category::get_parents($product->category->id);
        }

        return view('templates.'.$this->tempdir.'.products.show', compact('product','supplier_products','related_products','category_parents'));
    }

    public function getProfile() {
        $tag = 'profile';
        $page = UserWebsitePage::whereUser_id($this->user_id)->whereType('Page')->whereActive(1)->whereTag($tag)->first();
        $profile = \App\UserProfile::whereUser_id($this->user_id)->first();
        $categories_parent = Category::whereParent_category_id(0)->lists($this->c_name, 'id');

        return view('templates.'.$this->tempdir.'.profile.profile', compact('page', 'profile', 'categories_parent'));
    }

    public function getProduction() {
        $tag = 'production';
        $page = UserWebsitePage::whereUser_id($this->user_id)->whereType('Page')->whereActive(1)->whereTag($tag)->first();
        return view('templates.'.$this->tempdir.'.profile.production', compact('page'));
    }

    public function getTrade() {

        $tag = 'trade';
        $page = UserWebsitePage::whereUser_id($this->user_id)->whereType('Page')->whereActive(1)->whereTag($tag)->first();
        $trade = UserTradeInfo::whereUser_id($this->user_id)->whereProfile('temporary')->first();
        /*if(!$trade) {
            $trade = UserTradeInfo::init(Auth::user());
        }*/
        foreach(UserTradeInfo::$array_fields as $field) {
            if(!empty($trade->$field)) {
                if($field == 'nearest_port') {
                    $trade->$field = unserialize($trade->$field);
                } else {
                    $trade->$field = explode(',', $trade->$field);
                }
            }
        }

        return view('templates.'.$this->tempdir.'.profile.trade', compact('page', 'trade'));
    }

    public function getFaq() {
        $tag = 'faq';
    	return view('templates.'.$this->tempdir.'.profile.faq', compact('tag'));
    }

    public function getContact() {
        return view('templates.'.$this->tempdir.'.contact');
    }

    public function getContactSupplier() {
    	return view('templates.'.$this->tempdir.'.contact.supplier');
    }

    public function postContact(Request $request) {

        $req = $request->all();
        $product_id = null;
        $subject = 'A Message from your website @YallaNsadar';

        if($request->has('product_id')) {
            $product_id = $req['product_id'];
            $product = Product::whereId($product_id)->first();
            $subject = 'Enquiry about your product: '.$product->name;
        }

        $body = $req['description'];

        if(Auth::check()) {
            $req['name'] = Auth::user()->name;
            $req['email'] = Auth::user()->email;
            $req['mobile'] = Auth::user()->mobile;
            $req['country'] = Auth::user()->profile->country_id;
            $req['from_user_id'] = Auth::id();
        }
        $user_data['from_user_id'] = $req['from_user_id'];
        $user_data['name']=$req['name'];
        $user_data['email']=$req['email'];
        $user_data['mobile']=$req['mobile'];
        $user_data['country']=$req['country'];

        $message = \App\Messages\Message::sendEnquiry($this->user_id, $user_data, $subject, $body);
        if($message) {

            $user_data['to_user_id'] = $this->user_id;
            $user_data['message_id'] = $message->id;
            $user_data['message'] = $body;

            if($request->has('product_id')) {
                $user_data['product_id'] = $product->id;
            }
            $prod_enquriy = \App\ProductEnquiry::create($user_data);

            Session::flash('flash_message', trans('front.enquiry_sent'));
            return redirect()->back();
        }
    }

    public function getSearch(Request $req)
    {
        $keyword = $req->get('q');
        $products = new Product;
        $products = $products->whereUser_id($this->user_id)
        ->where('name','like', '%'.$keyword.'%')
        ->whereActive(1)
        ->whereApproved(2)
        ->with('lang','category')
        ->paginate(10);

        return view('templates.'.$this->tempdir.'.search.products', compact('products'));
    }

    public function getNotFound()
    {
        return view('templates.'.$this->tempdir.'.404');
    }
}