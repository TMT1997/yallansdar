<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserStep;
use Auth;
use Session;
use Hash;

class RegistrationController extends InitController
{
	public function __construct()
    {
        parent::__construct();

    }

    public function getRegistered()
    {
    	if(Session::has('new_account') && Session::get('new_account')==true)  {
	    	Session::flash('alert', 'success');
	        Session::flash('status', 'new_registration');

            //Set User Steps
            UserStep::init(Auth::user());
	        return view('front.account.registered');
    	}

        if(UserStep::isCompleted(Auth::user())) {
    	   return redirect('dashboard');
        }

        return redirect('dashboard/welcome');
    }

    public function getActive()
    {
        if(Auth::user()->active==0){
            Session::flash('alert', 'danger');
            Session::flash('flash_message', trans('front.activate_account_gotomail'));
        } else {
            return redirect('dashboard');
        }
        return view('front.account.active_alert');
    }

    public function getEmail()
    {
    	$user = Auth::user();
    	$data['activation_url'] = url('registration/activate?activation='.Hash::make($user->email).'&email='.$user->email);
    	$data['indentifier'] = Hash::make($user->id);
    	return view('front.emails.activate_account', compact('user', 'data'));
    }

    public function getActivate(Request $request)
    {
    	$activation_code = $request->get('activation');
    	$email = $request->get('email');
    	$user = null;

    	if(Auth::check()) {
    		$user = Auth::user();
    	} else {
    		$user = User::whereEmail($email)->first();
    	}
        if(!$user) {
            return 'Unknown account';
        }

        $from_app = ($request->has('mobapp') && $request->get('mobapp')==1) ? 1 : 0;

        if($user->active==1) {
            Auth::loginUsingId($user->id);
            Session::flash('alert', 'warning');
            Session::flash('flash_message', trans('front.already_activated'));
           return view('redirect.redirect_reg', compact('from_app'));
    	}

    	if(Hash::check($user->email, $activation_code))
    	{
            $user->email_verified = 1;
            $user->active = 1;
    		if($user->save()) {
                Auth::loginUsingId($user->id);
    			Session::flash('alert', 'success');
                Session::flash('flash_message', trans('front.account_hasbeen_activated'));

                return view('redirect.redirect_reg', compact('from_app'));
    		}
    	}

    	return 'Unknown Request';
    }

    public function getUnsubscribe(Request $request)
    {
        $email = $request->get('email');
        $idn = $request->get('idn');
        $user = User::whereEmail($email)->first();

        if($user)
        {
            if(Hash::check($user->id, $idn))
            {
                $user->unsubscribed = 1;
                if($user->save()) {
                    Session::flash('alert', 'success');
                    Session::flash('flash_message', '<strong>'.trans('front.youhave_unsubscribed').'</strong>');
                    return redirect('registration/unsubscribed');
                }
            }
        }

        Session::flash('alert', 'danger');
        Session::flash('flash_message', '<strong>عفوا لم يتم التعرف على الحساب، يرجى المحاولة لاحقا</strong>');
        return redirect('registration/unsubscribed');
    }

    public function getUnsubscribed(Request $request)
    {
        return view('front.account.subscribtion.unsubscribed');
    }
}
