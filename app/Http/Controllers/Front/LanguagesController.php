<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Language;
use Cache;

class LanguagesController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getSwitch($locale='en')
    {
    	if(Language::switch_language($locale)) {
            Cache::forget('countries');

            if(request()->has('back') && request()->get('back')=='no') {
                return redirect()->to('/');
            }
    		return redirect()->back();
    	}
    	return 'Cannot change system language';
    }
}