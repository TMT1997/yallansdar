<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Company;
use App\User;

class CategoryController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getIndex()
    {
    	$categories = Category::get_tree(0,0,false);
    	return view('front.categories.index', compact('categories'));
    }

    public function getAll($category_id, $slug='')
    {
    	$categories = Category::get_tree($category_id);
    	return view('front.categories.all_childs', compact('categories'));
    }

    public function getShow($category_id, $slug='')
    {
    	$category = Category::whereId($category_id)->first();

    	if($category->parent_category_id==0)
    	{
    		$categories_tree = Category::get_category_childs($category->id);
            $ids = [];
            $products = null;
            if($categories_tree->count()>0) {
                foreach($categories_tree as $cat) {
                    $ids[] = $cat->id;
                }
                $products = Product::where('products.active', 1)->where('products.approved', 2)
                    ->join('categories', 'products.category_id', '=', 'categories.id')
                    ->whereIn('categories.parent_category_id', $ids)
                    ->orWhereIn('products.category_id', $ids)
                    ->select('products.*')
                    ->orderBy('products.id', 'DESC')
                    ->with('details')
                    ->get();

                $services = \App\Service::where('services.active', 1)->where('services.status',2)
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->whereIn('categories.parent_category_id', $ids)
                ->orderBy('services.id','DESC')
                ->select('services.*')
                ->with('lang')
                ->take(6)->get();

                $user_ids = $products->pluck('user_id');
                $suppliers = User::whereIn('id', $user_ids)->with('profile')->get();
            }
    		return view('front.categories.parent_category', compact('category','categories_tree', 'products', 'suppliers','services'));
    	}

        //Check if this category has childs then get all products under all childs
        $categories_tree = Category::get_category_childs($category->id);
        $category_parents = Category::get_parents($category->id);
        if($categories_tree->count()>0)
        {
            $products = Product::where('products.active', 1)->where('products.approved', 2)
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('categories.parent_category_id', $category->id)
                ->select('products.*')
                ->orderBy('products.id', 'DESC')
                ->with('primary_image', 'details')
                ->get();
        } else {
            $products = Product::whereActive(1)->whereCategory_id($category->id)->orderBy('id', 'DESC')->get();
        }
        $user_ids = $products->pluck('user_id');
        $suppliers = User::whereIn('id', $user_ids)->with('profile')->get();
        $products_sponsored = Product::whereActive(1)->orderBy('id', 'DESC')->take(8)->get();

    	return view('front.categories.category', compact('category', 'categories_tree', 'category_parents', 'products', 'products_sponsored', 'suppliers'));
    }

    public function getSub($category_id)
    {
        $parent_cat_id = $category_id;
        $childs = Category::whereParent_category_id($category_id)->orderBy('sort', 'ASC')->get();
        $output = '';
        $c_name = $this->c_name;
        foreach($childs as $child) {
            $output .='<option value="'.$child->id.'">'.$child->$c_name.'</option>';
        }
        return $output;
    }
}
