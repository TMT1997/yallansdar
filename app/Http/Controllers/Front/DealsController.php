<?php

namespace App\Http\Controllers\Front;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Deals\Deal;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use App\Deals\Partnership;
use App\Packages\UserPackage;
use App\UserProfile;
use App\Packages\PackageItem;
use App\PackageUsage;
use App\quote_request_seen;
use App\Helper;
use Illuminate\Support\Facades\App;
use Session;
use Auth;
use Lang;
class DealsController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function getIndex()
    {
        $categories = Category::get_tree(0, 26, false);
        $deals['sell_offers'] = SellOffer::whereActive(1)->whereStatus(2)->with('images','product')->orderBy('id','DESC')->get();
        $deals['quote_requests'] = QuoteRequest::whereActive(1)->whereStatus(2)->with('images', 'quotes')->orderBy('id','DESC')->get();
        $deals['partnerships'] = Partnership::whereActive(1)->whereStatus(2)->with('images')->orderBy('id','DESC')->get();

        return view('front.deals.index', compact('deals', 'categories'));
    }
    /*
     * get the current date
     * input: none
     * output: current date in format Y-m-d
     */
    public function getCurrentDate()
    {
        return Carbon::now()->format('Y-m-d');
    }
    public function getBrowse($type='requests')
    {
        //get the current date to compare it with the expiration date
        $current_date = self::getCurrentDate();

        $deals = null;
        if($type=='offers') {
            $deals = SellOffer::whereActive(1)->whereStatus(2)->with('images','product','category','lang')->orderBy('id','DESC')->get();
        } else if ($type=='requests') {
            $deals = QuoteRequest::whereActive(1)
                ->whereStatus(2)->with('images','category','lang', 'quotes')
                ->orderBy('id','DESC')->get();
        } else if ($type=='partnerships') {
            $deals = Partnership::whereActive(1)->whereStatus(2)->with('images','category')->orderBy('id','DESC')->get();
        } else {
            
            return redirect('deals');
        }
        
        $category_ids = [];
        foreach($deals as $deal) {
            $category_ids[] = $deal->category_id;
        }
        $categories = Category::whereIn('id', $category_ids)->with('parent')->get();
        $lang = null ;
        if (isset(request()->lang)){
            if (request()->lang == 'ar')
                $lang = 'ar';
            if (request()->lang == 'en')
                $lang = 'en';
        }
        return view('front.deals.lists', compact('type', 'deals', 'categories' , 'lang'));
    }

    public function getCategory($category_id, $slug)
    {
        $category = Category::whereId($category_id)->with('parent')->first();

        if(!$category) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'عفوا لا يوجد صفقات');
            return redirect('deals');
        }

        $cats = Category::get_category_childs($category_id);
        $childs_arr = [];
        if($category->parent_category_id==0) {
            $childs = $cats->pluck('childs');
            $childs_arr = $childs->flatten()->lists('id');
        } else {
            if($cats->count() > 0) {
                $childs_arr = $cats->lists('id');
            }
        }

        $sell_offers = new SellOffer;
        $quote_requests = new QuoteRequest;
        $partnerships = new Partnership;

        if(!empty($childs_arr)) {
            $sell_offers = $sell_offers->whereIn('category_id', $childs_arr);
            $quote_requests = $quote_requests->whereIn('category_id', $childs_arr);
            $partnerships = $partnerships->whereIn('category_id', $childs_arr);
        } else {
            $sell_offers = $sell_offers->where('category_id', $category->id);
            $quote_requests = $quote_requests->where('category_id', $category->id);
            $partnerships = $partnerships->where('category_id', $category->id);
        }

        $deals['sell_offers'] = $sell_offers->whereActive(1)->whereStatus(2)->with('images','product')->orderBy('id','DESC')->get();
        $deals['quote_requests'] = $quote_requests->whereActive(1)->whereStatus(2)->with('images')->orderBy('id','DESC')->get();
        $deals['partnerships'] = $partnerships->whereActive(1)->whereStatus(2)->with('images')->orderBy('id','DESC')->get();

        return view('front.deals.category', compact('type', 'deals', 'category'));
    }

    //Export Chances is Request for Quotes
    public function getExportChances($category_id, $slug=null)
    {

        $category = Category::whereId($category_id)->with('parent')->first();
        $quote_requests = new QuoteRequest;

        if(!$category) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'عفوا لا يوجد صفقات');
            return redirect('deals');
        }

        $cats = Category::get_category_childs($category_id);
        $childs_arr = [];
        if($category->parent_category_id==0) {
            $childs = $cats->pluck('childs');
            $childs_arr = $childs->flatten()->lists('id');
        } else {
            if($cats->count() > 0) {
                $childs_arr = $cats->lists('id');
            }
        }

        if($category->parent_category_id==0) {
            $quote_requests = $quote_requests->whereParent_cat_id($category->id);
        } else {
            if(!empty($childs_arr)) {
                $quote_requests = $quote_requests->whereIn('category_id', $childs_arr);
            } else {
                $quote_requests = $quote_requests->where('category_id', $category->id);
            }
        }

        $quote_requests = $quote_requests->whereActive(1)->whereStatus(2)->with('images')->orderBy('id','DESC')->get();

        return view('front.deals.requests.index', compact('type', 'quote_requests', 'category', 'cats'));
    }

    public function getOffer($id, $slug=null)
    {
        $offer = SellOffer::whereId($id)->whereActive(1)->whereStatus(2)->with('images','product','lang', 'user')->first();
        $category_parents = Category::get_parents($offer->product->category_id);

        return view('front.deals.offers.show', compact('offer', 'category_parents'));
    }
    //select specific deal
    public function getRfq($slug, $id=null)
    {
        //get the current date to compare it with the expiration date
        $current_date = self::getCurrentDate();

        $offer = new QuoteRequest;
        if(is_numeric($slug)) {
            $offer = $offer->whereId($slug);
        } else {
            $offer = $offer->whereSlug($slug);

        }
        $offer = $offer->whereActive(1)->whereStatus(2)->with('images', 'quotes')->first();

        if(!$offer) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'Deal not found');
            return redirect()->to('deals/browse/requests');
        }
        $category_parents = [];
        if($offer) {
            $category_parents = ($offer->category_id) ? Category::get_parents($offer->category_id) : '';
            //get main category id and save seen for this product if this your category
            $cat_id = 0;
            if($category_parents) {
                $counter = count($category_parents) - 1; // used to get the last index in the array
                if($category_parents[$counter]){ // if the $category u selected is not = null
                    $cat_id = $category_parents[$counter]->id;
                }
                else{//take the the second one from down
                    $cat_id = $category_parents[$counter - 1]->id;
                }

            }
            else
                $cat_id = $offer->parent_cat_id;
            $user_profile = UserProfile::whereUser_id(\Auth::id())->orderBy('id', 'asc')->first();
            if($user_profile)
            {
                if($user_profile->other_products == $cat_id)
                {
                    $check_seen_before = quote_request_seen::whereUser_id(Auth::id())->whereQuoute_req_id($offer->id)->get();
                    if($check_seen_before->count() > 0);//nothing
                    else
                        $quote = quote_request_seen::create(['user_id' => Auth::id() , 'quoute_req_id' => $offer->id]);
                }//if this your category
            }//end check if has profile
        }
        $lang = null ;
        if (isset(request()->lang)){
            if (request()->lang == 'ar')
                $lang = 'ar';
            if (request()->lang == 'en')
                $lang = 'en';
        }



        return view('front.deals.requests.show', compact('offer', 'category_parents' , 'lang'));
    }//end fucntion

    public function getPartnership($id, $slug=null)
    {
        $offer = Partnership::whereId($id)->whereActive(1)->whereStatus(2)->with('images','lang')->first();
        $category_parents = Category::get_parents($offer->category_id);
        return view('front.deals.partnerships.show', compact('offer', 'category_parents'));
    }

    public function getEnquiryStatus($ref='status') {
        return view('front.deals.enquiry.status');
    }

    public function getEnquiry($id, $type='offers')
    {
        $deal = null;
        if($type=='offers') {
            $deal = SellOffer::whereId($id)->with('user','images','product','lang')->first();
        } else if ($type=='requests') {
            $deal = QuoteRequest::whereId($id)->with('user','images','category','lang')->first();
        } else if ($type=='partnerships') {
            $deal = Partnership::whereId($id)->with('user','category')->first();
        } else {
            return redirect('deals');
        }
        if(!$deal) {
            Session::flash('flash_message', 'عفوا الصفقة غير متاحة الأن');
            return redirect('deals');
        }

        if($type=='requests') {
            return view('front.deals.enquiry.rfq', compact('type', 'deal'));
        }
        return view('front.deals.enquiry.post', compact('type', 'deal'));
    }

    public function getRfqForm($id=null) {

        return redirect('deals');
    }
    public function postRfqForm(Request $request)
    {
        $id = $request->get('id');
        $type='requests';

        if(Auth::guest()) {
            return redirect()->to('interact/quicklogin?next=reload&ref=rfq_unlock');
        }

        //Check if super admin, then disable Package Usage Check
        if(!Auth::user()->admin)
        {
            $user_package = UserPackage::whereUser_id(Auth::id())->orderby('id','desc')->with('package')->first();
            $package_items = PackageItem::whereActive(1)->get();
            if(!$user_package) {
                return 'You don\'t have a package <a href="'.url('upgrade').'">Select a Package</a>';
            }

            //Get item info
            $unlock_item = $package_items->where('code', 'unlock_contact')->first();
            $item_info = PackageUsage::get_item($user_package->package, $unlock_item);

            //Get user usage
            $user_usage = PackageUsage::get_usage('unlock_contact');

            //Check if user already unlocked it
            if(!PackageUsage::unlocked_deal($id)) {
                //Store package usage record
                $pusage = PackageUsage::create([
                    'user_id'=>Auth::id(),
                    'package_id'=>$user_package->package_id,
                    'deal_id'=>$id,
                    'item'=>'unlock_contact',
                    'value'=>1,
                    'year'=>date('Y'),
                    'month'=>date('m')
                ]);
            }
        }

        $deal = QuoteRequest::whereId($id)->with('user','images','category','lang')->first();
        if(!$deal) {
            Session::flash('flash_message', 'عفوا الصفقة غير متاحة الأن');
            return redirect('deals');
        }
        return view('front.deals.enquiry.rfq_form', compact('type', 'deal', 'user_package', 'item_info', 'user_usage'));

    }

    public function postEnquiry(Request $request)
    {
        $validation_rules = [
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'body'=>'required',
            'attachment'=>'file|max:3072|mimes:jpg,jpeg,bmp,png,gif,doc,docs,xls,xlsx,pdf,txt',
        ];
        if(Auth::check()) {
            $validation_rules = [
                'subject'=>'required',
                'body'=>'required',
                'attachment'=>'file|max:3072|mimes:jpg,jpeg,bmp,png,gif,doc,docs,xls,xlsx,pdf,txt',
            ];
        }
        $this->validate($request, $validation_rules);

        $data['from_user_id'] = 0;
        if(Auth::check()) {
            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['from_user_id'] = Auth::id();
        }
        $msgData = [
            'to_user_id'=>$data['uid'],
            'from_user_id'=>$data['from_user_id'],
            'from_email'=>$data['email'],
            'from_name'=>$data['name'],
            'subject'=>$data['subject'],
            'body'=>$data['body'],
        ];

        $message = \App\Messages\Message::create($msgData);
        if($message)
        {
            if($request->hasFile('attachment')) {
                $attachment = Helper::upload($request, 'attachment', false);
                $attachmentData = [
                    'message_id'=>$message->id,
                    'file'=>$attachment,
                ];
                \App\Messages\Attachment::create($attachmentData);
            }
            Session::flash('alert', 'success');
            Session::flash('flash_message', trans('front.enquiry_sent'));
        } else {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', trans('front.error_occured'));
        }
        return redirect('deals/enquiry-status');
    }

    public function postRfqQuote(Request $request)
    {
        $validation_rules = [
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
            'production_capacity' => 'required|numeric',
            'capacity_unit' => 'required|numeric',
            'capacity_unit_per' => 'required|numeric',
            'delivery_time' => 'required|numeric',
            'payment_type' => 'required',
            'min_quantity' => 'required',
            'port' => 'required',
            'body'=>'required',
            'attachment'=>'file|max:3072|mimes:jpg,jpeg,bmp,png,gif,doc,docs,xls,xlsx,pdf,txt',
        ];
        if(Auth::check()) {
            $validation_rules = [
                'subject'=>'required',
                'payment_type' => 'required',
                'body'=>'required',

            ];
        }

        $this->validate($request, $validation_rules,['payment_type.required'=>'Please fill all the data']);

        $data = $request->all();
        $data['user_id'] = $data['uid']; //the request owner
        $data['payment_type'] = implode(',', $data['payment_type']);
        unset($data['uid']);

        $data['attachment'] = '';
        if($request->hasFile('attachment')) {
            $data['attachment'] = Helper::upload($request, 'attachment', false);
        }

        $data['from_user_id'] = 0;
        if(Auth::check()) {
            $data['name'] = Auth::user()->name;
            $data['email'] = Auth::user()->email;
            $data['from_user_id'] = Auth::id();
        }
        //Insert the quotation data
        $quote = \App\Deals\Quote::create($data);
//*********************************************************************************************
            Session::flash('alert', 'success');
            Session::flash('flash_message', trans('front.quotation_sent'));

        return view('front.deals.enquiry.status');
    }
}
