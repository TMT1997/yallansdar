<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Language;
use App\Country;
use App\Settings;
use View;
use Session;
use App;
use Cache;

class InitController extends Controller
{
    public $current_lang_id;
    public $countries;
    public $settings;
    public $c_name;
    public $locker;

    public function __construct()
    {
        //if true, contact details will be locked till user upgrade his package
        $this->locker = true;

        //Get Settings from Cache or set if not exist
        $this->settings = Cache::remember('settings', 10, function() {
            $settings = Settings::all();
            return [
                'social'=>Settings::fetch_section('social', $settings),
                'site'=>Settings::fetch_section('site', $settings),
                'general'=>Settings::fetch_section('general', $settings),
                'contact'=>Settings::fetch_section('contact', $settings),
                'meta'=>Settings::fetch_section('meta', $settings),
                'texts'=>Settings::fetch_section('texts', $settings),
            ];
        });

		$categories = \App\Category::whereActive(1)->take(7)->get();

		if(Session::has('yalla_locale')) {
    		App::setLocale(Session::get('yalla_locale'));
    	}
    	$languages = Language::whereActive(1)->get();
        $current_language = $languages->filter(function($lang){ return $lang->iso2==App::getLocale(); })->first();
        $this->current_lang_id = $current_language->id;
        \Carbon\Carbon::setLocale($current_language->iso2);

        $c_name = 'name_ar';
        if($current_language->iso2=='en') {
            $c_name = 'name_en';
        }
        $this->c_name = $c_name;
        $selected_country = 66;

        if(!Cache::has('countries')) {
            $countries = new Country;
            if($current_language->iso2=='en') {
                $countries = $countries->orderBy('name_english','ASC')->select('name_english AS name','id','iso2','active');
            } else {
                $countries = $countries->orderBy('country_name','ASC')->select('country_name AS name','id','iso2','active');
            }
            $countries = $countries->whereActive(1)->get();
            Cache::put('countries', $countries, 300);
        }
        $this->countries = Cache::get('countries');

		View::share('categories_footer', $categories);
		View::share('languages', $languages);
        View::share('current_language', $current_language);
        View::share('lng_id', $current_language->id);
        View::share('c_name', $c_name);
        View::share('countries', $this->countries);
        View::share('selected_country', $selected_country);
        View::share('settings', $this->settings);
        View::share('locker', $this->locker);
    }
}