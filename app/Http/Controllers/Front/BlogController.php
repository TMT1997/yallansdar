<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogCategory;
use Session;
use View;
use Jenssegers\Date\Date;

class BlogController extends InitController
{
	public function __construct() {
    	parent::__construct();

        $blog_cats = BlogCategory::whereActive(1)->get();
        $blog_tags = Blog::whereActive(1)->where('tags','<>','')->take(10)->select('id','tags')->get();
        $featured_posts = Blog::whereActive(1)->whereFeatured(1)->take(5)->orderBy('id','DESC')->get();

        View::share('blog_cats', $blog_cats);
        View::share('blog_tags', $blog_tags);
        View::share('featured_posts', $featured_posts);
    }

	public function getIndex()
    {
		$page_size = 10;

		$blog = new Blog;
        $blog = $blog->whereActive(1);
        $blog = $blog->orderBy('id','DESC');
        $blog = $blog->with('category','author');
        $blog = $blog->paginate($page_size);

    	return view('front.blog.index', compact('blog'));
    }

    public function getCategory($slug)
    {
        $category = BlogCategory::whereSlug($slug)->first();
        if(!$category) {
            Session::flash('flash_message', 'القسم غير موجود');
            return redirect()->to('blog');
        }
        $blog = new Blog;
        $blog = $blog->whereActive(1)->whereCategory_id($category->id)->orderBy('id','DESC');
        $blog = $blog->with('category','author');
        $blog = $blog->paginate(10);

        return view('front.blog.category', compact('blog', 'category'));
    }

    public function getTag($tag)
    {
    	$blog = Blog::whereActive(1)->where('tags','LIKE','%'.$tag.'%')->orderBy('id','DESC')->paginate(10);
    	return view('front.blog.tag', compact('blog', 'tag'));
    }

    public function getShow($slug) {
    	if(!$slug) {
    		return redirect('blog');
    	}
        $item = Blog::whereActive(1)->whereSlug($slug)->with('comments','category','author')->first();
        if(!$item) {
        	return redirect('blog');
        }
        $item->views = $item->views + 1;
        $item->save();

        $read_also = Blog::where('id','<>',$item->id)->whereActive(1)->orderBy('id','DESC')->take(4)->get();
        return view('front.blog.show', compact('item', 'read_also'));
    }
}