<?php

namespace App\Http\Controllers\Front;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Deals\QuoteRequest;
use App\Packages\UserPackage;
use App\Packages\PackageItem;
use App\PackageUsage;

class PackageCheckController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }
    /*
     * Function for gathering info about the package
     * Input: User ID
     * output : how many add left and open contact info left
     */
    public function postPackageInfo(Request $request){
        $user_id = $request->user_id;    //get user id
        $key = 'unlock_contact';         //item code is unlock contact
        $user_package_info = null;              // object to hold all the info we need to return to the ajax
        if(Auth::check()) {

                //get the newest updated package
                $user_package = UserPackage::where([["user_id","=",Auth::id()]
                ,["paid","=","1"],["confirmed","=","1"]])->orderby('created_at','desc')->with('package')->first(); //get the package the user assigned to
                //get the whole package
                $package_date = UserPackage::where("user_id","=",$user_id)->pluck('updated_at'); //get the package the user assigned to
                //get the package start date
//                $package_date=$package->updated_at;
                //add it to our object
                $user_package_info['package']=$user_package['package']['name_en']; // get the package from the user package then the package name

                //add the start date to our object
                $user_package_info['added_on_2'] = $package_date[0]; // the date was in array so we need [0] to get it
                //calculate the year from the start date
                $user_package_info['last_on'] = PackageUsage::getAge($package_date[0]);
                //Get item info
                $unlock_item = PackageItem::whereActive(1)->where('code', $key)->first(); // get the package item with code $key ( add product or ..)
                $date_package =$user_package->created_at; // the time the package created in

                $item_info = PackageUsage::get_item($user_package->package, $unlock_item); //

                //Get user usage
                $user_usage = PackageUsage::get_usage($key,null,null,null,$date_package); // get the usage of specifc user

                $item_total = $item_info['value'];

                $user_total = $user_usage->count();

                if(!empty($item_info['custom_value'])) {
                    $item_total = $item_info['custom_value'];
                }
                $user_package_info['totalunlock'] = $item_total; // add the number of unlocks to the item
                $user_package_info['userunlock'] = $user_total;  // the number of unlocks the user did
                //check if he has left some in his package
                if($item_total =='Unlimited') {
                    $user_package_info['finish'] = 'not';
                }
                else if($user_total >= $item_total){
                    $user_package_info['finish'] = 'finish';
                }
                else  $user_package_info['finish'] = 'not';
            }

         else {
//            return redirect()->to('interact/quicklogin?next=reload');
             //if not authorized
             $user_package_info['not_auth'] = 'true';
              return response()->json($user_package_info);
        }
        $user_package_info['not_auth'] = 'false'; // authorized person
        // return the information u gathered
        return response()->json($user_package_info);
    }
    public function postUnlockRfqContact(Request $request)
    {
    	$rfq_id = $request->get('rfq_id');
    	$deal = QuoteRequest::whereId($rfq_id)->whereActive(1)->whereStatus(2)->first();

		$user_package = null;
		$key = 'unlock_contact';

		if(Auth::check()) {
                $user_package = UserPackage::where([["user_id","=",Auth::id()]
                ,["paid","=","1"],["confirmed","=","1"]])->orderby('created_at','desc')->with('package')->first(); //get the package the user assigned to

				if(!$user_package) {
					$reason = 'no_package';
					return view('front.account.lock.alert', compact('reason'));
				}
				//Get item info
				$unlock_item = PackageItem::whereActive(1)->where('code', $key)->first(); // get the package item with code $key ( add product or ..)
				$item_info = PackageUsage::get_item($user_package->package, $unlock_item); //
                $date_package =$user_package->created_at; // the time the package created in
				//Get user usage
				$user_usage = PackageUsage::get_usage($key,null,null,null,$date_package); // get the usage of specifc user

				$item_total = $item_info['value'];

				$user_total = $user_usage->count();

				if(!empty($item_info['custom_value'])) {
					$item_total = $item_info['custom_value'];
				}

				if($item_total!='Unlimited' && $user_total >= $item_total) {
					return view('front.account.lock.unlock', compact('user_package', 'item_info', 'user_usage'));	
				} else {
					PackageUsage::create([
						'user_id'=>Auth::id(),
						'package_id'=>$user_package->package_id,
						'unlocked_user_id'=>Auth::id(),
						'deal_id'=>$rfq_id,
						'item'=>$key,
						'value'=>1,
						'year'=>date('Y'),
						'month'=>date('m')
					]);
				}
			}

		 else {
			return redirect()->to('interact/quicklogin?next=reload');
		}

        return view('front.deals.requests.importer_contact_info', compact('deal'));
    }
    public function postUnlockProduct(Request $request)
    {
        $product_id = $request->get('product_id');
        $user_id = $request->get('user_id');

        $deal = User::whereId($user_id)->first();
//        return $deal;
        $user_package = null;
        $key = 'unlock_contact';

        if(Auth::check()){
            $user_package = UserPackage::where([["user_id","=",Auth::id()]
                ,["paid","=","1"],["confirmed","=","1"]])->orderby('created_at','desc')->with('package')->first(); //get the package the user assigned to and accepted by the admin

            if(!$user_package) {
                $reason = 'no_package';
                return view('front.account.lock.alert', compact('reason'));
            }
            //Get item info
            $unlock_item = PackageItem::whereActive(1)->where('code', $key)->first(); // get the package item with code $key ( add product or ..)
            $item_info = PackageUsage::get_item($user_package->package, $unlock_item); //

            $date_package =$user_package->created_at; // the time the package created in
            //Get user usage
            $user_usage = PackageUsage::get_usage($key,null,null,null,$date_package); // get the usage of specifc user

            $item_total = $item_info['value'];

            $user_total = $user_usage->count();

            if(!empty($item_info['custom_value'])) {
                $item_total = $item_info['custom_value'];
            }

            if($item_total!='Unlimited' && $user_total >= $item_total) {
                return view('front.account.lock.unlock', compact('user_package', 'item_info', 'user_usage'));
            } else {
                PackageUsage::create([
                    'user_id'=>Auth::id(),
                    'package_id'=>$user_package->package_id,
                    'unlocked_user_id'=>Auth::id(),
                    'product_id'=>$product_id,
                    'item'=>$key,
                    'value'=>1,
                    'year'=>date('Y'),
                    'month'=>date('m')
                ]);
            }
        }

        else {
            return redirect()->to('interact/quicklogin?next=reload');
        }

        return view('front.products.product_contact_info', compact('deal'));
    }
}
