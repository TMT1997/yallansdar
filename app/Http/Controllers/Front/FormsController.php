<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subscriber;
use App\Feedback;
use App\User;
use App\Comment;
use Auth;
use Session;
use App\Packages\UserPackage;
use App\Packages\PackageItem;
use App\PackageUsage;

class FormsController extends InitController
{
    public function __construct() {
		parent::__construct();
	}

	public function postSubscribe(Request $request)
	{
		$this->validate($request, ['name'=>'required', 'email'=>'required']);
		$subscriber = new Subscriber;
		if($subscriber->create($request->all()))
		{
			$message = 'Thanks, you have subscribed successfully in Newsletter <a href="'.url('/').'">Back to website</a>';
			$status = 'success';
		} else {
			$status = 'danger';
			$message = 'Error in your subscribtion, please review your form';
		}
		return view('front.forms.form_submitted', compact('message','status'));
	}

	public function postFeedback(Request $request)
	{
		$message = new Feedback;
		if($message->create($request->all()))
		{
			$message =  'Thanks, your message has been sent successfully, we may reply to you if needed <a href="'.url('/').'">Back to website</a>';
			$status = 'success';
		} else {
			$status = 'danger';
			$message = 'Error in your message, please review your form';
		}

		return view('front.forms.form_submitted', compact('message','status'));
	}

	public function postSendVerificationEmail()
	{
		if(Auth::check())
		{
			$user = Auth::user();
			if(User::sendVerificationEmail($user)) {
				Session::flash('alert', 'success');
                Session::flash('flash_message', '<strong>تم إرسال رسالة التفعيل بنجاح</strong>');
             	return redirect('interact/send-status');
	        }
		}
	}


	public function postComments(Request $request)
	{
		$this->validate($request, ['email'=>'required', 'comment'=>'required']);
		$comment = new Comment;
		if($comment->create($request->all()))
		{
			$message = 'تم اضافة التعليق بنجاح';
			$status = 'success';
		} else {
			$status = 'danger';
			$message = 'عفوا هناك خطأ اثناء اضافة التعليق';
		}
		return view('front.forms.status', compact('message','status'));
	}

	public function getSendStatus() {
		return view('front.account.active_alert');
	}

	public function getQuicklogin() {
		return view('auth.login_quick');
	}

	public function postLogin(Request $request)
	{
		$email = $request->get('email');
		$password = $request->get('password');
		$next = $request->get('next');
		$ref = $request->get('ref');
		$id = $request->get('id');

		if(empty($email) || empty($password)) {
			return json_encode(['status'=>'failed', 'message'=>'Both Email and password fields are required']);
		}
		if(Auth::attempt(['email'=>$email, 'password'=>$password]))
		{
			Session::flash('flash_message', trans('front.login_quick_thanks'));
			Session::flash('ref', $ref);
			return json_encode(['status'=>'success', 'message'=>'Great, you have logged in', 'next'=>$next, 'ref'=>$ref, 'id'=>$id]);
		} else {
			return json_encode(['status'=>'failed', 'message'=>'Invalid email or password']);
		}

		return json_encode(['status'=>'failed', 'message'=>'Unknown Error']);
	}

	public function getUnlock(Request $request)
	{
		$user_package = null;
		$key = 'acc_buyer';

		if(Auth::check()) {
			$user_package = UserPackage::whereUser_id(Auth::id())->with('package')->first();
			$package_items = PackageItem::whereActive(1)->get();

			if(!$user_package) {
				$reason = 'no_package';
				return view('front.account.lock.alert', compact('reason'));
			}
			//Get item info
			$unlock_item = $package_items->where('code', $key)->first();
			$item_info = PackageUsage::get_item($user_package->package, $unlock_item);

			//Get user usage
			$user_usage = PackageUsage::get_usage($key);

			return view('front.account.lock.unlock', compact('user_package', 'item_info', 'user_usage'));
		} else {
			return $this->getQuicklogin();
		}
	}

	public function postUnlock(Request $request)
	{
		$key = 'acc_buyer';

		if(Auth::guest()) {
			return json_encode(['status'=>'failed', 'message'=>'You should login first']);	
		}

		$user_package = null;
		if(Auth::check()) {
			$user_package = UserPackage::whereUser_id(Auth::id())->with('package')->first();
		}

		PackageUsage::create([
			'user_id'=>Auth::id(),
			'package_id'=>$user_package->package_id,
			'unlocked_user_id'=>$request->get('unid'),
			'item'=>$key,
			'value'=>1,
			'year'=>date('Y'),
			'month'=>date('m')
		]);
		return json_encode(['status'=>'success', 'message'=>'Unlocked Successfully']);
	}
}