<?php

namespace App\Http\Controllers\Front;

use App\Packages\Package;
use App\Packages\UserPackage;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Adv;
use App\Category;
use App\Product;
use App\Company;
use App\Country;
use App\Favorite;
use App\User;
use App\Helper;
use App\ProductLang;
use Session;
use Cart;
use Auth;

class ProductsController extends InitController
{
    public function __construct() {
    	parent::__construct();
        $categories_parent = Category::whereParent_category_id(0)->lists($this->c_name, 'id');
        \View::share('categories_parent', $categories_parent);
    }

    public function getIndex()
    {
        $advs = Adv::whereType('صفحة المنتجات')->whereActive(1)->get();
        $categories = Category::whereActive(1)->whereParent_category_id(0)->get();
        $products = Product::whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->take(100)->get();
        $user_ids = $products->pluck('user_id');
        $products_sponsored = Product::whereActive(1)->orderBy('id', 'DESC')->take(8)->get();
        $lang = null ;
        if (isset(request()->lang)){
            if (request()->lang == 'ar')
                $lang = 'ar';
            if (request()->lang == 'en')
                $lang = 'en';
        }
        return view('front.products.index', compact('categories','products','products_sponsored' , 'lang','advs'));
    }

    public function getCategory($slug)
    {
        $category = Category::whereId($slug)->whereActive(1)->first();
        $categories = Category::whereActive(1)->whereParent_category_id(0)->get();
        $categories_tree = Category::get_category_childs($category->id);
        
        $ids = [];
        $products = null;
        if($categories_tree->count()>0) {
            foreach($categories_tree as $cat) {
                $ids[] = $cat->id;
            }
            $products = Product::where('products.active', 1)
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->whereIn('categories.parent_category_id', $ids)
                ->orWhereIn('products.category_id', $ids)
                ->select('products.*')
                ->orderBy('products.id', 'DESC')
                ->get();
        }

        $products_sponsored = Product::whereActive(1)->orderBy('id', 'DESC')->take(8)->get();

    	return view('front.products.index', compact('category','categories','products','products_sponsored'));
    }

    public function getShow($slug)
    {
        $product = Product::whereSlug($slug)->whereActive(1)->whereApproved(2)->with('category','images','user','reviews')->first();
//        return $product;
        if(!$product) {
            Session::flash('alert', 'warning');
            Session::flash('flash_message', trans('front.product_not_available'));
            return redirect('products');
        }
        //get the package of the user added this product
        $user_package = UserPackage::whereUser_id($product->user_id)->orderby('created_at','desc')->with('package')->first();

        $supplier_products = Product::whereActive(1)->whereUser_id($product->user_id)->where('id',"!=",$product->id)->orderBy('id', 'DESC')->take(8)->get();
//        return $supplier_products;
        $category_ids = [];
        $related_products = [];
        $category_parents = [];
        if($product->category) {
            //check if the category id is a parent category
            if($product->category->parent_category_id == 0)
                $category_ids = Category::whereId($product->category->id)->lists('id');
            else//it's a child category id then get it's parents
                $category_ids       = Category::whereParent_category_id($product->category->parent_category_id)->lists('id');
//            return $product->category->parent_category_id;
            $related_products   = Product::whereIn('category_id',$category_ids)->where('id','!=',$product->id)->whereActive(1)->orderBy('id','DESC')->take(8)->get();
            $category_parents   = Category::get_parents($product->category->id);
        }
        //Check Website exists
        $user_website = \App\UserWebsite::whereUser_id($product->user_id)->whereStatus(1)->whereActive(1)->first();
        $lang = null ;
        if (isset(request()->lang)){
            if (request()->lang == 'ar')
                $lang = 'ar';
            if (request()->lang == 'en')
                $lang = 'en';
        }
        return view('front.products.show', compact('product','supplier_products','related_products','category_parents', 'user_website','user_package' , 'lang'));
    }//end get show

    public function getPrint($product_id, $slug='')
    {
    	$product = Product::whereId($product_id)->whereActive(1)->with('category','images','user','reviews')->first();
        $category_ids = null;
        $category_parents = null;
        if($product->category) {
           $category_ids = Category::whereParent_category_id($product->category->parent_category_id)->lists('id');            
    	   $category_parents = Category::get_parents($product->category->id);
        }

    	return view('front.products.print', compact('product','category_parents'));
    }

    public function postEnquiry(Request $request) {

        $req = $request->all();
        $product_id = $req['product_id'];

        $product = Product::whereId($product_id)->first();
        $product_name = (!empty($product->name_ar) ? $product->name_ar : $product->name_en);
        $subject = 'Enquiry about your product: '.$product_name;
        $body = $req['description'];

        if(Auth::check()) {
            $req['name'] = Auth::user()->name;
            $req['email'] = Auth::user()->email;
            $req['mobile'] = Auth::user()->mobile;
            $req['country'] = Auth::user()->profile->country_id;
            $req['from_user_id'] = Auth::id();
        }
        $user_data['from_user_id'] = $req['from_user_id'];
        $user_data['name']=$req['name'];
        $user_data['email']=$req['email'];
        $user_data['mobile']=$req['mobile'];
        $user_data['country']=$req['country'];

        $message = \App\Messages\Message::sendEnquiry($product->user_id, $user_data, $subject, $body);
        if($message) {

            $user_data['to_user_id'] = $product->user_id;
            $user_data['product_id'] = $product->id;
            $user_data['message_id'] = $message->id;
            $user_data['message'] = $body;
            $prod_enquriy = \App\ProductEnquiry::create($user_data);
            Session::flash('redirect_message', trans('front.redirect')); // for the redirect page
            Session::flash('flash_message', trans('front.enquiry_sent')); // for the main page
            return view('redirect.index');
        }
    }

    public function getAddtofavorites($product_id, $type='products')
    {
        $status = false;
        $logged_in = true;
        $already_added = false;
        if(Auth::check())
        {
            //Check if already added
            $check = Favorite::whereType('Products')->whereRecord_id($product_id)->whereUser_id(Auth::id())->first();

            if(!$check) {
                $favorite = new Favorite;
                $favorite->type = $type;
                $favorite->record_id = $product_id;
                $favorite->user_id = Auth::id();
                if($favorite->save()) {
                    $status = true;
                }
            } else {
                $already_added = true;
            }
        } else {
            $logged_in = false;
        }
        return view('front.products.addToFavorite', compact('status', 'logged_in', 'already_added'));
    }

    public function getSupplier($id, $slug=null)
    {


        $user = new User;
        if(is_numeric($id)){
            $user = $user->whereId($id);
        } else {
            $user = $user->whereSlug($id);
        }
        $user = $user->with('profile')->first();

        if(!$user) {
            Session::flash('flash_message', 'Account not found');
            return redirect('/home');
        }
        if($slug) {
            //if it pass slug here as product id then don't select that id with u
            $products = Product::whereActive(1)->whereUser_id($user->id)->where('id','!=',$slug)->orderBy('id', 'DESC')->get();

        }
        else{
            $products = Product::whereActive(1)->whereUser_id($user->id)->orderBy('id', 'DESC')->get();
        }
        $products_sponsored = Product::whereActive(1)->orderBy('id', 'DESC')->take(6)->get();

        return view('front.products.supplier_products', compact('user', 'products', 'products_sponsored'));
    }

    public function getAdd()
    {
        return view('front.products.quick.add');
    }

    public function postAdd(Request $request)
    {
        $this->validate($request, [
            'prod_name'=>'required',
            'description'=>'required',
            'details'=>'required',
            'name'=>'required',
            'email'=>'required|email',
        ]);

        $data = $request->all();

        //Create new user or Get User
        if(Auth::guest()) {
            $user_data = [
                'email'=>$data['email'],
                'name'=>$data['name'],
                'password'=>User::generateRandomPassword()
            ];
            $user = User::createVisitorAccount($user_data);
            $data['user_id'] = $user->id;
        } else {
            $data['user_id'] = Auth::id();
        }

        $product = Product::create([
            'user_id'=>$data['user_id'],
            'name_ar'=>$data['prod_name'],
            'description_ar'=>$data['description'],
            'image'=>Helper::upload($request,'image'),
        ]);

        $product_lang = ProductLang::create([
            'language_id'=>1,
            'product_id'=>$product->id,
            'name'=>$data['prod_name'],
            'description'=>$data['description'],
            'details'=>$data['details'],
        ]);

        if(Auth::guest()) {
            Session::flash('flash_message', trans('front.quick_prod_added_withaccount'));
        } else {
            Session::flash('flash_message', trans('front.quick_prod_added'));
        }
        return redirect()->back();
    }
}
