<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;
use App\Product;
use Auth;

class ReviewsController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getAdd($ref_id, $ref_type='product')
    {
    	return view('front.reviews.add', compact('ref_id', 'ref_type'));
    }

    public function postAdd(Request $request)
    {
    	if(!Auth::check()) {
    		$output = ['status'=>'error', 'message'=>'Please login first'];
    	} else {
	    	$data = $request->all();

	    	if($data['ref_type']=='product') {
	    		$product = Product::find($data['ref_id']);
	    		$data['user_id'] = $product->user_id;
	    		$data['product_id'] = $product->id;
	    	}

	    	if($data['ref_type']=='company') {
	    		$user = User::whereEmail($data['ref_id'])->first();
	    		$data['user_id'] = $user->user_id;
	    	}

	    	$data['active']=0;
	    	$data['from_user_id'] = Auth::id();
	    	unset($data['ref_id']);
	    	unset($data['ref_type']);

	    	$output = [];
	    	$review = Review::create($data);
	    	if($review) {
	    		$output = ['status'=>'success'];
	    	} else {
	    		$output = ['status'=>'error'];
	    	}
    	}

    	return json_encode($output);
    }

}
