<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Company;
use App\Testimontial;
use App\Slider;
use App\User;
use App\Service;
use App\Adv;
class HomeController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getHome()
    {
    	$categories = Category::get_tree(0, 30);
        $top_categories = Category::whereHp_top(1)->take(12)->get();
    	$testimontials = Testimontial::whereActive(1)->whereHomepage(1)->take(4)->get();
    	$slider = Slider::whereActive(1)->orderBy('id', 'DESC')->get();
        $hot_products = Product::whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->with('lang')->take(20)->get();
        $suppliers = User::whereActive(1)->whereFeatured(1)->orderBy('id', 'DESC')->take(6)->with('profile')->get();
        $services = Service::whereActive(1)->whereStatus(2)->orderBy('id','DESC')->with('lang')->take(6)->get();

        $export_categories = new \App\Deals\QuoteRequest;
        $export_categories = $export_categories->whereActive(1)->whereStatus(2)->groupBy('parent_cat_id');
        $export_categories = $export_categories->select(\DB::raw('count(*) as total_rfq, parent_cat_id'));
        $export_categories = $export_categories->with('parent_category');
        $export_categories = $export_categories->orderBy('total_rfq', 'DESC');
        $export_categories = $export_categories->take(9);
        $export_categories = $export_categories->get();

        $deals['sell_offers'] = \App\Deals\SellOffer::whereActive(1)->whereStatus(2)->with('user','images','product')->orderBy('id','DESC')->take(4)->get();
        $deals['quote_requests'] = \App\Deals\QuoteRequest::whereActive(1)->whereStatus(2)->with('user','images')->orderBy('id','DESC')->take(4)->get();
        $deals['partnerships'] = \App\Deals\Partnership::whereActive(1)->whereStatus(2)->with('user','images')->orderBy('id','DESC')->take(4)->get();

        $blog = \App\Blog::whereActive(1)->orderBy('updated_at', 'DESC')->take(3)->get();
        $advs = Adv::whereType('الصفحة الرئيسية')->whereActive(1)->get();
    	return view('front.index', compact('categories','top_categories', 'export_categories', 'testimontials', 'slider', 'hot_products', 'suppliers','deals', 'services', 'blog','advs'));
    }

    public function getMobileapps() {
        return view('front.mobileapp.soon');
    }

    public function getHelp() {
        return view('front.help.index');
    }

    public function getPrivacyPolicy() {
        return view('front.site.privacy.policy');
    }

    public function getTerms() {
        return view('front.site.privacy.terms');
    }

    public function getLanding() {
        if(\App::getLocale()=='ar'){
            return view('front.landing.page');
        } else {
            return view('front.landing.page_en');
        }        
    }

    public function getExporters() {
        $exporters = new User;
        $exporters = $exporters->whereActive(1)->whereExporter(1);
        $exporters = $exporters->groupBy('name');
        $exporters = $exporters->orderBy('name', 'ASC')->with('profile')->paginate(8);

        $latest_products = Product::whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->take(10)->get();        
        return view('front.companies.exporters', compact('exporters', 'latest_products'));
    }
   
}