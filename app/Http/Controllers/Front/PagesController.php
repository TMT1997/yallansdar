<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getShow($page_id, $slug=null)
    {
    	$page = Page::find($page_id);
    	return view('front.pages.page', compact('page'));
    }
}
