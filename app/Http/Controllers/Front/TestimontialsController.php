<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Testimontial;

class TestimontialsController extends InitController
{
    public function __construct() {
    	parent::__construct();
    }

    public function getIndex() {
        $testimontials = \App\Testimontial::whereActive(1)->orderBy('id', 'DESC')->get();
        return view('front.testimontials.index', compact('testimontials'));
    }

    public function getAdd() {
    	return redirect()->to('testimontials#add');
    }
    public function postAdd(Request $request) {
    	$this->validate($request, [
    		'name'=>'required|max:50',
    		'job_title'=>'required',
    		'company_name'=>'required',
    		'testimontial'=>'required',
    	]);

        $data = $request->all();
        $data['active']=0;
        $data['by_visitors']=1;
        if(Testimontial::create($data)) {
        	Session::flash('flash_message', 'Thanks, Testimontail added successfully');
        }
        return redirect()->to('testimontials');
    }
}