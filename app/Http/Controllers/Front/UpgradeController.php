<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Packages\UserPackage;
use Auth;
use Session;
use Hash;

class UpgradeController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getIndex()
    {
        $packages = Package::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $sections = Package::$sections;

        $confirmed_user_package = (Auth::check()) ? UserPackage::whereUser_id(Auth::id())->whereConfirmed(1)->first() : null;

        return view('front.upgrade.package_select', compact('packages', 'items', 'sections', 'confirmed_user_package'));
    }

    public function getQuick($template_id=null)
    {
    	$packages = Package::whereActive(1)->orderBy('sort_order', 'ASC')->get();
    	$items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
    	$sections = Package::$sections;
    
    	return view('dashboard.packages.select_package', compact('packages', 'items', 'sections'));
    }

    public function getPayment($package_id)
    {
    	if(!Auth::check()) {
    		return redirect('login');
    	}

        $package = Package::whereActive(1)->whereId($package_id)->first();
        if(!$package) {
            Session::flash('flash_message', 'Unknown package');
            return redirect('upgrade');
        }
    	return view('front.upgrade.payment', compact('package'));	
    }

    public function postPayment(Request $request)
    {
        $this->validate($request, [
            'payment_method'=>'required',
        ]);
    	$data['package_id'] = $request->get('package_id');
    	$data['user_id'] = Auth::id();
    	$data['amount'] = $request->get('amount');
    	$data['currency'] = $request->get('currency');
        $data['payment_method'] = $request->get('payment_method');
    	$data['confirmed']= ($request->get('amount')==0)? 1 : 0;

    	$package = Package::whereActive(1)->whereId($data['package_id'])->first();
    	if(!$package || ($data['amount'] != $package->price)) {
    		return 'Unknown package';
    	}

    	$user_package = UserPackage::create($data);

        if($request->get('payment_method')=='online') {

            $salt = auth()->user()->email.auth()->id();
            $secret = Hash::make($salt);

            $site_id = "5d26025e-309f-4689-a2fb-61d3a18ecc2e";
            $amount = \App\Helper::toUSD($data['amount']);
            
            $success_url = "http://yallansadar.com/upgrade/online-status/".$secret."/success";
            $fail_url = "http://yallansadar.com/upgrade/online-status/".$secret."/fail";

            /*$success_url = "http://yallansdar/public/upgrade/online-status/".$secret."/success";
            $fail_url = "http://yallansdar/public/upgrade/online-status/".$secret."/fail";*/
            

            return redirect()->to("https://app.vapulus.com/website3ds/?".
                "siteId=$site_id".
                "&amount=$amount".
                "&link=http://storage.googleapis.com&pageTitle=Yalla-Nsadar".
                "&onaccept=$success_url".
                "&onfail=$fail_url");

        }
    	Session::flash('flash_message', 'Congrats, your request for package upgrade has been sent successfully to Admins, we will contact you back to complete upgrade process');
    	return redirect()->to('upgrade/status');
    }

    public function getStatus()
    {
        return view('front.upgrade.upgrade_status');
    }

    public function getOnlineStatus($secret, $status)
    {
        if(auth()->check())
        {
            $salt = auth()->user()->email.auth()->id();
            if(Hash::check($salt, $secret)) {
                // Confirm the user package
                if($status=='success') {
                    $user_package = UserPackage::whereUser_id(auth()->id())->first();
                    $user_package->confirmed = 1;
                    $user_package->save();
                }
            } else {
                Session::flash('alert', 'danger');
                Session::flash('flash_message', 'Invalid Transaction');
                return redirect('upgrade');
            }
        } else {
            redirect()->to('login');
        }

        return view('front.upgrade.online_payment_status', compact('status'));
    }

    public function getHash()
    {
        $key = auth()->user()->email.auth()->id();
        $secret = Hash::make($key);

        if(Hash::check($key, $secret)) {
            return 'ok';
        }
        return 'no';
    }

}