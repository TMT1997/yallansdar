<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\Service;
use App\Company;
use App\Country;
use App\Favorite;
use Session;
use Cart;
use Auth;

class ServicesController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getIndex()
    {
    	$services = Service::whereActive(1)->whereStatus(2)->orderBy('id', 'DESC')->take(100)->get();
    	return view('front.services.index', compact('services'));
    }

    public function getShow($service_id, $slug='')
    {
        $service = Service::whereId($service_id)->whereActive(1)->with('lang','category','images','user')->first();
        $other_services = Service::whereActive(1)->whereStatus(2)->orderBy('id','DESC')->take(8)->with('lang')->get();

        $category_ids = [];
        $related_services = [];
        $category_parents = [];
        if($service->category) {
            $category_ids = Category::whereParent_category_id($service->category->parent_category_id)->lists('id');            
            $related_services = Service::whereIn('category_id',$category_ids)->whereActive(1)->orderBy('id','DESC')->take(8)->with('lang')->get();
            $category_parents = Category::get_parents($service->category->id);
        }

        return view('front.services.show', compact('service','other_services','category_parents'));
    }

    public function postEnquiry(Request $request) {
        $req = $request->all();
        $service_id = $req['service_id'];

        $service = Service::whereId($service_id)->first();
        $subject = 'Enquiry about your service: '.$service->name;
        $body = $req['description'];

        if(Auth::check()) {
            $req['name'] = Auth::user()->name;
            $req['email'] = Auth::user()->email;
            $req['mobile'] = Auth::user()->mobile;
            $req['country'] = Auth::user()->profile->country_id;
            $req['from_user_id'] = Auth::id();
        }
        $user_data['from_user_id'] = $req['from_user_id'];
        $user_data['name']=$req['name'];
        $user_data['email']=$req['email'];
        $user_data['mobile']=$req['mobile'];
        $user_data['country']=$req['country'];

        if(\App\Messages\Message::sendEnquiry($service->user_id, $user_data, $subject, $body)) {
            Session::flash('flash_message', trans('front.enquiry_sent'));
            return redirect()->back();
        }
    }
}