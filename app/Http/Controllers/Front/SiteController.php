<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BusinessDeveloper;
use App\TradeManager;
use App\PickDescribe;
use App\Helper;
use Auth;
use Session;

class SiteController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
    	return view('front.site.index');
    }

    public function getJoinUs()
    {
    	return view('front.site.join');
    }

    public function getTradeManager()
    {
    	return view('front.site.trade_manager');
    }
    public function getPickDescribe()
    {
        return view('front.site.pick_describe');
    }
    public function getContact()
    {
        return view('front.site.contact');
    }

    public function getStatus(Request $req)
    {
    	return view('front.site.form_status');
    }

    public function postBusinessDevRequest(Request $request) {
        $this->validate($request, [
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required|email',
            'passport'=>'image',
        ]);

        $data = $request->all();
        $data['passport'] = Helper::upload($request, 'passport');
        $account = BusinessDeveloper::create($data);
        if($account) {
            Session::flash('flash_message', trans('front.request_sent_msg'));
            return redirect()->to('site/status?dev_request=true');
        }
    }

    public function postTradeManagersRequest(Request $request) {
        $this->validate($request, [
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required|email',
            'national_id'=>'image',
            'commercial_record'=>'image',
        ]);

        $data = $request->all();
        $data['national_id'] = Helper::upload($request, 'national_id');
        $data['commercial_record'] = Helper::upload($request, 'commercial_record');
        $account = TradeManager::create($data);
        if($account) {
            Session::flash('flash_message', trans('front.request_sent_msg'));
            return redirect()->to('site/status?dev_request=true');
        }
    }

    public function postPickDescribeRequest(Request $request) {
         $this->validate($request, [
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required|email',
            'national_id'=>'image',
            'commercial_record'=>'image',
        ]);

        $data = $request->all();
        $data['national_id'] = Helper::upload($request, 'national_id');
        $data['commercial_record'] = Helper::upload($request, 'commercial_record');
        $account = PickDescribe::create($data);
        if($account) {
            Session::flash('flash_message', trans('front.request_sent_msg'));
            return redirect()->to('site/status?dev_request=true');
        }

    }
}
