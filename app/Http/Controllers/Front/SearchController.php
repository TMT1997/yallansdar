<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\User;
use App\Deals\Deal;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use App\Deals\Partnership;

class SearchController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getProducts(Request $req)
    {
        $keyword = $req->get('q');
        $products = null;

        if(!empty($keyword) && strlen($keyword)>2) {

            $products = new Product;
            $products = $products->where('active',1)->where('approved',2);
            $products = $products->where(function($query) use ($keyword) {
                $query->where('name_ar', 'LIKE', '%'.$keyword.'%')
                ->orWhere('name_en', 'LIKE', '%'.$keyword.'%')
                ->orWhere('description_ar', 'LIKE', '%'.$keyword.'%')
                ->orWhere('description_en', 'LIKE', '%'.$keyword.'%')
                ->orWhere('keywords_ar', 'LIKE', '%'.$keyword.'%')
                ->orWhere('keywords_en', 'LIKE', '%'.$keyword.'%')
                ->orWhere('details_ar', 'LIKE', '%'.$keyword.'%')
                ->orWhere('details_en', 'LIKE', '%'.$keyword.'%');
            });
            $products = $products->get();
        }
        $products_sponsored = Product::whereActive(1)->whereApproved(2)->orderBy('id', 'DESC')->take(8)->get();
        return view('front.search.products.results', compact('products','keyword','products_sponsored'));
    }

    public function getSuppliers(Request $req)
    {
        $keyword = $req->get('q');
        $users = new User;
        $users = $users
            ->where('name','like', '%'.$keyword.'%')
            ->orWhere('email','like', '%'.$keyword.'%')
            ->orWhere('mobile','like', '%'.$keyword.'%')
            ->orWhere('company_name','like', '%'.$keyword.'%');
        $users = $users->whereActive(1)->with('profile')->get();
        
        $products_sponsored = Product::whereActive(1)->orderBy('id', 'DESC')->take(8)->get();
        
        return view('front.search.users.results', compact('users','keyword','products_sponsored'));
    }

    public function getDeals(Request $req)
    {
    	$keyword = $req->get('q');

        $deals['sell_offers'] = SellOffer::where('deals_sell_offers.active','=',1)
        ->where('deals_sell_offers.status','=',2)
        ->join('deals_sell_offers_lang', 'deals_sell_offers.id','=', 'deals_sell_offers_lang.sell_offer_id')
        ->join('products', 'deals_sell_offers.product_id','=', 'products.id')
        ->where('deals_sell_offers_lang.description','like', '%'.$keyword.'%')
        ->orWhere('deals_sell_offers_lang.brands','like', '%'.$keyword.'%')
        ->orWhere('products.name_ar','like', '%'.$keyword.'%')
        ->orWhere('products.name_en','like', '%'.$keyword.'%')
        ->orWhere('products.keywords_ar','like', '%'.$keyword.'%')
        ->orWhere('products.keywords_en','like', '%'.$keyword.'%')
        ->orWhere('products.description_ar','like', '%'.$keyword.'%')
        ->orWhere('products.description_en','like', '%'.$keyword.'%')
        ->orWhere('products.port','like', '%'.$keyword.'%')
        ->select('deals_sell_offers.*','deals_sell_offers_lang.sell_offer_id','deals_sell_offers_lang.description')
        ->with('images','product')
        ->get();

        /*$deals['quote_requests'] = QuoteRequest::join('deals_quote_requests_lang', 'deals_quote_requests.id','=', 'deals_quote_requests_lang.quote_request_id')
        ->where('deals_quote_requests.active','=',1)
        ->where('deals_quote_requests.status','=',2)
        ->where('deals_quote_requests.service_product_name','like', '%'.$keyword.'%')
        ->select('deals_quote_requests.*','deals_quote_requests_lang.quote_request_id','deals_quote_requests_lang.details')
        ->with('images')
        ->get();*/
        $quote_requests = new QuoteRequest;
        $quote_requests = $quote_requests->where('active','=',1);
        $quote_requests = $quote_requests->where('status','=',2);
        $quote_requests = $quote_requests->where('service_product_name','LIKE', '%'.$keyword.'%');
        $quote_requests = $quote_requests->with('images');
        $quote_requests = $quote_requests->orderBy('id', 'DESC');
        $quote_requests = $quote_requests->take(40)->get();
        $deals['quote_requests'] = $quote_requests;

        $deals['partnerships'] = Partnership::join('deals_partnerships_lang', 'deals_partnerships.id','=', 'deals_partnerships_lang.partnership_id')
        ->where('deals_partnerships.active','=',1)
        ->where('deals_partnerships.status','=',2)
        ->where('deals_partnerships.service_product_name','like', '%'.$keyword.'%')
        ->with('images')
        ->get();

    	return view('front.search.deals.results', compact('deals', 'keyword'));
    }
}
