<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller ;
use App\Advs;
use App\Helper;
class NewsEventsController extends Controller
{
    public function get_news_events(Request $request)
    {
        $limit = $request->limit;
        if($limit == 0)
            $dash_adv = Advs::whereActive(1)->whereType('أخبار و فعاليات')->get();
        else
            $dash_adv = Advs::whereActive(1)->whereType('أخبار و فعاليات')->limit($limit)->get();
        return Helper::api_response_format(true , $dash_adv);
    }//end get news and events
}
