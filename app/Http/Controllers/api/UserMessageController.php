<?php

namespace App\Http\Controllers\api;

use App\UserProfile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Messages\Message;
use App\Helper;
use App\Messages\Contact;
use App\User;

class UserMessageController extends Controller
{
    public function get_inbox(Request $request , $id)
    {
        $limit = 0 ; $offset = 0 ;
        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;

        $user = User::whereId($id)->first();
        if($user)
        {
            $messages = Message::whereTo_user_id($id)->whereDraft(0)->whereTrashed(0)->whereStatus(1)->whereSpammed(0)->orderBy('created_at','DESC')->with('attachments');
            $messages = Helper::check_collection_limit($messages ,$limit,$offset)->get();
            foreach ($messages as $message){
	    	    $img = 'frontend/images/no_thumb.jpg';
	    	    if (isset(UserProfile::whereUser_id($message->from_user_id)->first()->picture) && UserProfile::whereUser_id($message->from_user_id)->first()->picture != null)
	    	        $img = UserProfile::whereUser_id($message->from_user_id)->first()->picture;

                $message->form_user = User::find($message->from_user_id)->name;
                $message->date = date('Y.j.n' , strtotime($message->created_at));
                $message->time = date('H:i:s' , strtotime($message->created_at));
                $message->sender_image = url('public/uploads/' . $img);
            }
	    	return Helper::api_response_format(true , $messages);
    	}//if user found
    	else
    	{
    		return Helper::api_response_format(false,'','User Not Found');
    	}// if user not found
    }//end get inbox

    public function getUserThread(Request $request){
        $user = User::whereId($request->user_id)->first();
        if (! isset($user->id) ){
            return Helper::api_response_format(false , 'In valid user id');
        }
        $user_id = Helper::api_current_userID($request->api_token);
        $messages = Message::whereTo_user_id($user_id)->whereFrom_user_id($request->user_id)->whereDraft(0)->whereTrashed(0)->whereStatus(1)->whereSpammed(0)->orderBy('created_at','DESC')->with('attachments');
        $messages = Helper::check_collection_limit($messages , $request->limit , $request->offset)->get();
        foreach ($messages as $message){
            $img = 'frontend/images/no_thumb.jpg';
            if (isset(UserProfile::whereUser_id($message->from_user_id)->first()->picture) && UserProfile::whereUser_id($message->from_user_id)->first()->picture != null)
                $img = UserProfile::whereUser_id($message->from_user_id)->first()->picture;

            $message->form_user = User::find($message->from_user_id)->name;
            $message->date = date('Y.j.n' , strtotime($message->created_at));
            $message->time = date('H:i:s' , strtotime($message->created_at));
            $message->sender_image = url('public/uploads/' . $img);
        }
        return Helper::api_response_format(true , $messages);
    }
    //**********************************************************************************
    public function get_sent_messages($id)
    {
    	if(User::find($id))
    	{
	    	$messages = Message::whereFrom_user_id($id)->whereTrashed(0)->orderBy('id','DESC')->with('contact')->get();
            foreach ($messages as $message){
                $img = 'frontend/images/no_thumb.jpg';
                if (isset(UserProfile::whereUser_id($message->from_user_id)->first()->picture) && UserProfile::whereUser_id($message->from_user_id)->first()->picture != null)
                    $img = UserProfile::whereUser_id($message->from_user_id)->first()->picture;

                $message->form_user = User::find($message->from_user_id)->name;
                $message->date = date('Y.j.n' , strtotime($message->created_at));
                $message->time = date('H:i:s' , strtotime($message->created_at));
                $message->sender_image = url('public/uploads/' . $img);
            }
	    	return Helper::api_response_format(true , $messages);
    	}//if user found
    	else
    	{
    		return Helper::api_response_format(false,'','User Not Found');
    	}// if user not found
    }//end get sent_messages
    //**********************************************************************************
    public function send_message (Request $request , $id , $to_id)
    {
    	if(User::find($id) && User::find($to_id))
    	{
	        $data = $request->all();
	        $message_data = [
	            'from_user_id'=>$id,
	            'to_user_id'=>$to_id,
	            'section'=>'inbox',
	            'subject'=>$data['subject'],
	            'body'=>$data['body'],
	        ];
	        $message = Message::create($message_data);
		    return Helper::api_response_format(true , $message);
	    }//if user found
    	else
    	{
    		return Helper::api_response_format(false,'','Users Not Found');
    	}// if user not found
    }
    //**********************************************************************************
    public function delete_message($id)
    {
    	$message = Message::find($id);
    	return Helper::api_delete($message , 'message not found');
    }
    //**********************************************************************************
    public function turnToOld(Request $request){
        $msg = Message::whereId($request->message_id)->first();
        if ($msg){
            $msg->read = 1 ;
            $msg->save();
            return Helper::api_response_format(true , 'true');
        }
        return Helper::api_response_format(false , '', 'you have not any messages yet');
    }
    //**********************************************************************************
    public function searchMessage(Request $request){
        if (! isset($request->word))
            return Helper::api_response_format(false , 'word field is required');

        $user_id = Helper::api_current_userID($request->api_token);
        $word = $request->word;
        $messages = Message::whereTo_user_id($user_id)->whereDraft(0)->whereTrashed(0)->whereStatus(1)->whereSpammed(0)->where('body' , 'like' , '%' . $word . '%')->orWhere('subject' , 'like' , '%' . $word . '%')->orderBy('created_at','DESC')->with('attachments')->get();

        foreach ($messages as $message){
            $img = 'frontend/images/no_thumb.jpg';
            if (isset(UserProfile::whereUser_id($message->from_user_id)->first()->picture) && UserProfile::whereUser_id($message->from_user_id)->first()->picture != null)
                $img = UserProfile::whereUser_id($message->from_user_id)->first()->picture;
            $message->form_user = User::find($message->from_user_id);
            //check if the user is = null doesn't exist rare case
            $message->from_user ? $message->from_user = $message->from_user->name : $message->from_user;
            $message->date = date('Y.j.n' , strtotime($message->created_at));
            $message->time = date('H:i:s' , strtotime($message->created_at));
            $message->sender_image = url('public/uploads/' . $img);

        }
        return $messages;
    }
}
