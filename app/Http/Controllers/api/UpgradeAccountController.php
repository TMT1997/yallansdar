<?php

namespace App\Http\Controllers\api;

use App\Helper;
use App\Packages\Package;
use App\Packages\UserPackage;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UpgradeAccountController extends Controller
{
    /* get the package the user has now
     * input : user id
     * output: the package in json file
     */
    public function get_upgrade_account($id){
        $user_package = UserPackage::whereUser_id($id)->with('package')->orderby('created_at','desc')->with('package')->first(); //get the package the user assigned to
        if(!$user_package) return Helper::api_response_format(false,'','no package for this user id');
        return Helper::api_response_format(true,$user_package);
    }
    /* upgrade the user account
     * input :
     * output: the package in json file
     */
    public function create_upgrade_account(Request $request)
    {

        $validation_rules = [
            'payment_method'=>'required',
            'package_id'=>'required',
            'amount'=>'required',
            'currency'=>'required',
        ];

        //make validate
        $validator = Validator::make($request->all(),$validation_rules);

        //if the validation fails
        if($validator->fails()){
            //make response with the validations fails message and succ false
            return Helper::api_response_format(false,'',$validator->messages());
        }//end if

        $data['package_id'] = $request->get('package_id');
        $data['user_id'] = Helper::api_current_userID($request->api_token);
        $data['amount'] = $request->get('amount');
        $data['currency'] = $request->get('currency');
        $data['payment_method'] = $request->get('payment_method');
        $data['confirmed']= ($request->get('amount')==0)? 1 : 0;

        $package = Package::whereActive(1)->whereId($data['package_id'])->first();
        
        if(!isset($package) || $data['amount'] != $package->price) {
            return Helper::api_response_format(false,'','package info didn\'t match any package info');
        }
 
        $user_package = UserPackage::create($data);
        
        
        if($user_package){
            return Helper::api_response_format(true,$user_package);
        }

    }


}