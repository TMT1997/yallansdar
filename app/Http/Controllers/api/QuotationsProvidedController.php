<?php

namespace App\Http\Controllers\api;

use App\Deals\Deal;
use App\Deals\Partnership;
use App\Deals\Quote;
use App\Deals\QuoteRequest;
use App\Deals\SellOffer;
use App\Helper;
use App\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
/*
 * this class has no update
 */
class QuotationsProvidedController extends Controller
{
    /*
     * get the quote provided by the user
     * input : user id
     * output : get the quote provided
     */
    public function get_quote_provided($id){
        $quotations = Quote::whereFrom_user_id($id)->with('request')->get();
        //no quote found
        if(!count($quotations)) return Helper::api_response_format(false,'','no quotes for this user id');

        return Helper::api_response_format(true,$quotations);
    }
    /*
     * create quote to provide it to user
     * input : none
     * output: created quote
     */
    public function create_quote_provided(Request $request)
    {
        $validation_rules = [
            'subject'=>'required',
            'production_capacity' => 'required|numeric',
            'capacity_unit' => 'required|numeric',
            'capacity_unit_per' => 'required|numeric',
            'delivery_time' => 'required|numeric',
            'payment_type' => 'required',
            'min_quantity' => 'required',
            'user_id' => 'required',
            'port' => 'required',
            'body'=>'required',
            'quote_request_id'=>'required',
            'attachment'=>'file|max:3072|mimes:jpg,jpeg,bmp,png,gif,doc,docs,xls,xlsx,pdf,txt',
        ];

        //make validate
        $validator = Validator::make($request->all(),$validation_rules);

        //if the validation fails
        if($validator->fails()){
            //make response with the validations fails message and succ false
            return Helper::api_response_format(false,'',$validator->messages());
        }//end if

        $data = $request->except('api_token');

        //if the payment type not array
        if(!is_array(($data['payment_type']))) return Helper::api_response_format(false,'','payment type must be array');
        $data['payment_type'] = implode(',', $data['payment_type']);

        $data['attachment'] = '';
        if($request->hasFile('attachment')) {
            $data['attachment'] = Helper::upload($request, 'attachment', false);
        }
        //get the logged user data
        $user = User::whereApi_token($request->api_token)->first();

        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['from_user_id'] = $user->id;

        //Insert the quotation data
        $quote = \App\Deals\Quote::create($data);
        if($quote)
            return Helper::api_response_format(true,$quote);

        //error happen
        return Helper::api_response_format(false,'','Some error happened');
    }
    /*
     * put the quote in the trash
     * input  : deal id
     * output : deleted row
     */
    public function delete_quote_provided($id){
        $deal = Deal::whereRecord_id($id)->first();
        
        if(!$deal) return Helper::api_response_format(false,'','deal not found');
        $model = null;
        if($deal->section=='sell_offer') {
            $model = SellOffer::whereId($deal->record_id)->first();
        }
        else if($deal->section=='quote_request') {
            $model = QuoteRequest::whereId($deal->record_id)->first();
        }
        else if($deal->section=='partnership') {
            $model = Partnership::whereId($deal->record_id)->first();
        }
        if($deal->status == 5) return Helper::api_response_format(false,'','deal already in trash');
        if($model) {
            $model->restored_as = $deal->status; //If restored set its status as this
            $model->save();
        }
        $deal->status = 5; //5: trash

        if($deal->save()) {
            return Helper::api_response_format(true,$model);
        }
        return Helper::api_response_format(false,'','Some error happened');

    }
    /*
     * restore the trashed quote
     * input: deal id
     * output : the restored row
     */
    public function restore_quote_provided($id){
       $deal = Deal::whereRecord_id($id)->first();
        
        if(!$deal) return Helper::api_response_format(false,'','deal not found');
        $model = null;
        if($deal->section=='sell_offer') {
            $model = SellOffer::whereId($deal->record_id)->first();
        }
        else if($deal->section=='quote_request') {
            $model = QuoteRequest::whereId($deal->record_id)->first();
        }
        else if($deal->section=='partnership') {
            $model = Partnership::whereId($deal->record_id)->first();
        }
        if($deal->status == 1) return Helper::api_response_format(false,'','deal not in trashed already');
        if($model) {
            $deal->status = $model->restored_as;
        } else {
            $deal->status = 1;
        }

        if($deal->save()) {
            return Helper::api_response_format(true,$model);
        }

    }
}














