<?php

namespace App\Http\Controllers\api;

use App\Country;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helper;
use App\Product;
use Validator;
use App\ProductImage;
use App\Media;
use App\Category;

class ProductController extends Controller
{
    /*
     * get all the products with user id and the request contain status determine the kind of products he want to get
     * input: limit
     * response witht he whole data inside the product table
     */

    public function get_product(Request $request, $limit = 0, $forUser = true)
    {
        //create the offset first
        $offset = $request->offset;
        $limit = $request->limit;
        if (!isset($request->limit)) $limit = 0;
        if (!isset($offset)) $offset = 0;

        $status = ['approved' => 2, 'pending' => 1, 'disapproved' => 3];
        $products = null;
        if ($forUser)//for the user owned this products
        {
            $id = Helper::api_current_userID($request->api_token);
            $products = Product::whereUser_id($id);
        } else {
            $products = new Product;
        }
        //$products = $products->whereUser_id($id);
        if (!count($products->get())) //user has product
            return Helper::api_response_format(false, '', 'User doesn\'t has any products yet');

        $status_as_num = $request->get('status');
        if (isset($status_as_num))//does exist in the request
        {
            $status_as_num = $status[$status_as_num];

            $products = $products->whereApproved($status_as_num);
        }//end status if

        if (isset($request->country)) {
            $products = $products->whereManufactured_in($request->country);
        }
        if (isset($request->sub_category)) {
            $products = $products->whereCategory_id($request->sub_category);
        }
        if (isset($request->parent_category)) {
            if ($request->parent_category <= 42) // then it's parent
            {
                $category = Category::whereParent_category_id($request->parent_category); // get all the sub category for this parent
                $category = $category->lists('id')->toArray();
                array_push($category, $request->parent_category);
                $products = $products->whereIn('category_id', $category);
            } else // then it's sub
            {
                $products = $products->whereCategory_id($request->parent_category);
            }
        } //search tech
        if (isset($request->search)) {
            $search_word = urldecode($request->search);
            $products->where(function ($q) use ($search_word) {
                $q->where('name_ar', 'LIKE', '%' . $search_word . '%')
                    ->orWhere('name_en', 'LIKE', '%' . $search_word . '%')
                    ->orWhere('id', 'LIKE', '%' . $search_word . '%')
                    ->orWhere('details_en', 'LIKE', '%' . $search_word . '%');
            });
        }
        $products = $products->orderBy('id', 'DESC')->with('images', 'status_reason');
        $products = self::HandleProduct($request, $products, $limit, $offset);
        return Helper::api_response_format(true, $products);
    }

    public function HandleProduct(Request $request, $products, $limit, $offset)
    {
        if ($limit) {
            $products = $products->skip($offset)->take($limit);
        }

        $products = $products->orderBy('id', 'DESC');

        if ($request->has('country_id') && !empty($request->get('country_id'))) {
            $products = $products->where('manufactured_in', $request->get('country_id'));
        }

        if ($request->has('sub_category') && !empty($request->get('sub_category'))) {
            $products = $products->where('category_id', $request->get('sub_category'));
        }

        if ($request->has('search') && !empty($request->get('search')))
        {
            $search = urldecode($request->search);
            $search = '%' . $search. '%';

            $products = $products->where(function ($q) use ($search){
              $q->where('name_ar', 'LIKE', $search)
              ->orWhere('name_en', 'LIKE', $search)
              ->orWhere('keywords_ar', 'LIKE', $search)
              ->orWhere('keywords_en', 'LIKE', $search)
              ->orWhere('description_ar', 'LIKE', $search)
              ->orWhere('description_en', 'LIKE', $search)
              ->orWhere('details_en', 'LIKE', $search)
              ->orWhere('details_ar', 'LIKE', $search);
            });
        }

        $products = $products->get();

        foreach ($products as $product)
        {
            foreach ($product->images as $image) {
                $image->image = url('public/uploads/' . $image->image);
            }

            $product->payment_type = explode(',', $product->payment_type);
            $product->image = url('public/uploads/' . $product->image);
            $country = Country::whereId($product->manufactured_in)->first();
            $product->manufactured_in = $country;

            if (isset($country)) {
                $product->manufactured_in->country_icon = url('/flags/24/' . $country->iso2 . '.png');
            }
            $product->category = Category::find($product->category_id);
        }
        return $products;
    }

    /*
     * get the collection and return the values withing the limit and offset specified
     */
    public function check_products_limit($collection, $limit, $offset)
    {
        if ($limit)
            return $collection->skip($offset)->take($limit)->get();
        else
            return $collection->get();
    }

    /*
     * update product
     * input : id of the product , request has colums with the edit data
     * return response
     */
    public function update_product(Request $request, $id)
    {
        $product = Product::whereId($id)->with('images')->first();
        if (!count($product)) {// if it's empty
            return Helper::api_response_format(false, '', 'Product not found');
        }
        $product_data = $request->except('lang', 'add_images', '_method', 'api_token');

        $product_data['category_id'] = $request->get('category_id');
        $products_data['slug'] = Helper::generate_slug('\App\Product', $product_data['name_ar'], $id);
        if ($request->has('image')) {
            $product_data['image'] = $request->image;
        }
        if (isset($product_data['more_details'])) {
            $product_data['more_details'] = serialize($product_data['more_details']);
        }
        if (isset($product_data['payment_type'])) {
            $product_data['payment_type'] = implode(',', $product_data['payment_type']);
        }

        $product->update($product_data);

        if ($product) {
            //Upload & Save Additional Images

            if ($request->has('add_images'))

                $images = self::UpdateProductMedia($request->add_images, $product->id);
            //return data with status true
            $product->images;
            return Helper::api_response_format(true, $product);

        } else {
            return Helper::api_response_format(false, '', 'some error happened');
        }

    }

    /*
     * IT'S make the product in the trash mean approved take value 4
     * input : id of the product
     * return response
     */
    public function delete_product($id)
    {
        // get the product
        $product = Product::find($id);
        if ($product) {//exist

            $product->restored_as = $product->approved; //If restored set its status as this
            $product->approved = 4; //4: trash
            if ($product->save()) {
                return Helper::api_response_format(true, $product);
            } else {//some error happened
                return Helper::api_response_format(false, '', 'Some Error happened');
            }
        } else {
            return Helper::api_response_format(false, '', 'Product not found');
        }


    }

    /*
     * IT'S restore the product from the trash
     * input : id of the product
     * return response
     */
    public function restore_product($id)
    {
        // get the product
        $product = Product::find($id);
        if ($product) {//exist
            $product->approved = $product->restored_as; //The status was before trash
            if ($product->save()) {
                return Helper::api_response_format(true, $product);
            } else {//some error happened
                return Helper::api_response_format(false, '', 'Some Error happened');
            }
        } else {
            return Helper::api_response_format(false, '', 'Product not found');
        }
    }

    /*
     * create product
     * input : $request has the product data
     * return response
     */
    public function create_product(Request $request)
    {
        //current user
        $user = User::whereApi_token($request->api_token)->first();
        //current user id
        $user_id = $user->id;
        //make validate
        $validator = Validator::make($request->all(), Product::$rules);

        //if the validation fails
        if ($validator->fails()) {
            //make response with the validations fails message and succ false
            return Helper::api_response_format(false, '', $validator->messages());
        }//end if
        else {//if the validation holds
            $data = $request->except('lang', 'add_images', 'api_token');
            $data['user_id'] = $user_id;
            $data['slug'] = Helper::generate_slug('\App\Product', $data['name_ar']);
            if ($request->hasFile('image')) {
                $data['image'] = Helper::upload($request, 'image');
            }

            $data['category_id'] = $request->get('category_id');
            if (isset($data['payment_type'])) {
                $data['payment_type'] = implode(',', $data['payment_type']);
            }
            unset($data['selected_category_id']);
            $product = Product::create($data);
            if ($product) {
                $media = Media::create([
                    'user_id' => $user_id,
                    'type' => 'images',
                    'media_album_id' => 1,
                    'file' => $data['image'],
                    'name' => 'no_name'
                ]);

                \App\TemplateSteps::update_step('add_products', $user);

            }//edn if product
            else {//product didn't create successfully
                return Helper::api_response_format(false, '', 'Product didn\'t created successfully');
            }
            if ($request->has('add_images'))
                $images = self::storeProductMedia($request->add_images, $product->id);

            // everything is ok
            $product->images;
            $product->payment_type = explode(',', $product->payment_type);
            return Helper::api_response_format(true, $product);
        }//else validator

    }

    private function storeProductMedia($files, $product_id)
    {
        foreach ($files as $file) {
            $s = ProductImage::create(['product_id' => $product_id, 'image' => $file]);
            //Save uploaded image to media
            $media = Media::create([
                'user_id' => Helper::api_current_userID(\request()->api_token),
                'type' => 'images',
                'media_album_id' => 1,
                'file' => $file,
                'name' => 'no_name'
            ]);
        }
    }

    private function UpdateProductMedia($files, $product_id)
    {
        $media = ProductImage::whereProduct_id($product_id)->delete();

        self::storeProductMedia($files, $product_id);
    }

    public function GetOtheProducts(Request $request)
    {
        $product = Product::whereCategory_id($request->category_id)->get();
        if ($product) {
            if ($product->category) {
                $category_ids = Category::whereParent_category_id($product->category->parent_category_id)->lists('id');
                $related_products = Product::whereIn('category_id', $category_ids)->where('id', '!=', $product->id)->whereActive(1)->orderBy('id', 'DESC')->take(8);
                return Helper::api_response_format(true, self::HandleProduct($request, $related_products, 0, 0));
            }
        }
        return Helper::api_response_format(false, '', 'No Products yet');
    }

    public function GetAllProductsForImporter(Request $request)
    {
        $profile = UserProfile::whereUser_id(Helper::api_current_userID($request->api_token))->first();
        $products = new Product;
        $products = $products->orderBy('created_at', 'DESC')->whereCategory_id($profile->other_products);
        $offset = 0;
        $limit = 0;
        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;

        $products = self::HandleProduct($request, $products, $limit, $offset);
        return Helper::api_response_format(true, $products);
    }

    public function GetImporterAnotherCats(Request $request)
    {
        $profile = UserProfile::whereUser_id(Helper::api_current_userID($request->api_token))->first();
        $products = new Product;
        $products = $products->orderBy('created_at', 'DESC')->where('category_id', '!=', $profile->other_products);
        $offset = 0;
        $limit = 0;

        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;

        $products = self::HandleProduct($request, $products, $limit, $offset);
        return Helper::api_response_format(true, $products);
    }

    public function ExporterProducts(Request $request)
    {
//        return self::get_product($request , 0 , true);
        $limit = 0;
        $offset = 0;
        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;

        $products = Product::whereUser_id(Helper::api_current_userID($request->api_token))->with('images', 'status_reason');
        if (isset($request->status)) {
            $status = ['approved' => 2, 'pending' => 1, 'disapproved' => 3];
            if (!in_array($request->status, array_keys($status)))
                return Helper::api_response_format(false, '', 'Invalid status');
            $products->whereApproved($status[$request->status]);
        }
        return Helper::api_response_format(true, self::HandleProduct($request, $products, $limit, $offset));
    }

    public function ExporterOtherProducts(Request $request)
    {
        $limit = 0;
        $offset = 0;

        if (isset($request->offset)) {
            $offset = $request->offset;
        }
        if (isset($request->limit)) {
            $limit = $request->limit;
        }

        $products = new Product;
        $products = $products->where('active', 1)->where('approved', 2);
        $products = $products->where('user_id', '!=', Helper::api_current_userID($request->api_token));

        if ($request->has('search') && !empty($request->get('search'))) {
            $q = $request->get('search');
            $keyword = '%' . $q . '%';

            $products = $products->where(function ($query) use ($keyword) {
                $query->where('name_ar', 'LIKE', $keyword)
                    ->orWhere('name_en', 'LIKE', $keyword)
                    ->orWhere('description_ar', 'LIKE', $keyword)
                    ->orWhere('description_en', 'LIKE', $keyword)
                    ->orWhere('keywords_ar', 'LIKE', $keyword)
                    ->orWhere('keywords_en', 'LIKE', $keyword)
                    ->orWhere('details_ar', 'LIKE', $keyword)
                    ->orWhere('details_en', 'LIKE', $keyword);
            });
        }

        if ($request->has('sub_category') && !empty($request->get('sub_category'))) {
            $products = $products->where('category_id', $request->get('sub_category'));
        }

        if ($request->has('country_id') && !empty($request->get('country_id'))) {
            $products = $products->whereManufactured_in($request->get('country_id'));
        }

        $products = $products->with('images');
        return Helper::api_response_format(true, self::HandleProduct($request, $products, $limit, $offset));
    }
}