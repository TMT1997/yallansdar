<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Country;
use App\Helper;
use App\Service;
use App\ServiceLang;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function AddService(Request $request){
        $user = User::whereApi_token($request->api_token)->first();
        if($user->service_provider == 0){
            return Helper::api_response_format(false , '' , 'you are not allowed to add a service you are not a service provider');
        }
        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'english_name' => 'required',
            'image' => 'required',
            'document' => 'required',
            'price_min' => 'required',
            'price_max' => 'required',
            'locations' => 'required',
            'expire_at' => 'required',
            'arabic_description' => 'required',
            'english_description' => 'required',
            'arabic_details' => 'required',
            'english_details' => 'required',
        ]);
        if ($validator->fails()){
            return Helper::api_response_format(false , '' , $validator->errors());
        }
        $service = Service::create([
            'user_id' => Helper::api_current_userID($request->api_token),
            'name' => $request->name ,
            'english_name' => $request->english_name,
            'image' => $request->image,
            'document' => $request->document,
            'price_min' => $request->price_min,
            'price_max' => $request->price_max,
            'locations' => implode(',' , $request->locations),
            'expired_at' => $request->expire_at,
            'arabic_description' => $request->arabic_description,
            'english_description' => $request->english_description,
            'arabic_details' => $request->arabic_details,
            'english_details' => $request->english_details,
        ]);
        $service_lang = ServiceLang::create([
            'language_id' => 1,
            'service_id' => $service->id,
            'english_name' => $request->english_name,
            'name' =>  $request->english_name,
            'description' => $request->english_description,
            'details' => $request->english_details
        ]);
        $service_lang = ServiceLang::create([
            'language_id' => 2,
            'service_id' => $service->id,
            'name' => $request->name ,
            'details' => $request->arabic_details ,
            'description' => $request->english_description
        ]);
        return Helper::api_response_format(true , $service);
    }
    /*
     * get all the services provided
     * input: api_token
     * output: the whole services provided
     */
    public function get_services(Request $request,$limit = 0 , $is_mine = false)
    {
        $limit = 0;
        if (isset($request->limit))
            $limit = $request->limit;

        $returned_service = [];
        $index = 0;

        $offset = 0;
        $service = Service::whereStatus(2)->whereActive(1)->with('lang');
        if($is_mine)
        {
            $service = Service::whereUser_id(Helper::api_current_userID($request->api_token));
        }
        if(isset($request->offset))
            $offset = $request->offset;
        //now deal with filter
        //search
        //search tech
        if (isset($request->search))
        {
            $search_word  = $request->search;
            $service->where(function ($q) use ($search_word)
            {
                $q->where('name','LIKE','%'.$search_word.'%')
                    ->orWhere('english_name','LIKE','%'.$search_word.'%');
            });
        }
        //filter by category
        if (isset($request->sub_category))
        {
            $service = $service->whereCategory_id($request->sub_category);
        }
        if (isset($request->parent_category))
        {
            if($request->parent_category <= 42) // then it's parent
            {
                $category = Category::whereParent_category_id($request->parent_category); // get all the sub category for this parent
                $category= $category->lists('id')->toArray();
                array_push($category,$request->parent_category);
                $service->whereIn('category_id',$category);
            }
            else // then it's sub
            {
                $service->whereCategory_id($request->parent_category);
            }
        }
        if (isset($request->country_id)) {
            $service->where('Locations' , 'like' , '%,'.$request->country_id.',%')->orWhere('Locations' , 'like' , $request->country_id.',%')->orWhere('Locations' , 'like' , '%,'.$request->country_id);
        }
        $service = $service->orderBy('id','desc');
        $service = Helper::check_collection_limit($service , $limit, $offset)->get();

        foreach ($service as $ser)
        {
            $countries_ids = explode(',',$ser->locations);

            if($request->has('country_id') && !empty($request->get('country_id'))) {
                if(!in_array($request->get('country_id'), $countries_ids)) {
                    continue;
                }
            }

            $returned_service[$index]['id']=$ser->id;
            $returned_service[$index]['status']=$ser->status;
            $returned_service[$index]['user_id']=$ser->user_id;
            $returned_service[$index]['name']=$ser->name;
            $returned_service[$index]['en_name']=$ser->english_name;
            $returned_service[$index]['ar_name']=$ser->arabic_name;
            $countries = Country::whereIn('id',$countries_ids)->get();

            $returned_service[$index]['min_price']=$ser->price_min;
            $returned_service[$index]['max_price']=$ser->price_max;
            $returned_service[$index]['expiration_date']=$ser->expired_at;

            $returned_service[$index]['image']=url('public/uploads/' . $ser->image);
            $returned_service[$index]['brochure']=url('public/uploads/' . $ser->document);

            foreach ($countries as $country)
            {
                $country->country_icon = url('flags/24/'.$country->iso2.'.png');
            }

            $returned_service[$index]['description_ar'] = $ser->arabic_description;
            $returned_service[$index]['description_en'] = $ser->english_description;
            $returned_service[$index]['more_details_ar'] = $ser->arabic_details;
            $returned_service[$index]['more_details_en'] = $ser->english_details;
            $returned_service[$index]['country']=$countries;
            $index++;
        }
        return Helper::api_response_format(true , $returned_service);
    }

    public function get_services_by_status(Request $request){
        $status = [ 'pending' => 1 , 'approved' => 2, 'disapproved' => 3, 'expired' => 4, 'trashed' => 5, 'active' => 6 ];
        if (!isset($request->status))
            return Helper::api_response_format(false , '' , 'You must provide status');

        if (in_array($request->status , array_keys($status))){
            $offset = 0;
            $limit = 0;
            if (isset($request->offset))
                $offset = $request->offset;
            if (isset($request->limit))
                $limit = $request->limit;
            $services = Service::whereStatus($status[$request->status])->whereUser_id(Helper::api_current_userID($request->api_token))->with('lang');
            return Helper::api_response_format(true , self::ServiceHandler($services , $limit , $offset));
        }
        return Helper::api_response_format(false , '' , 'Invalid status');
    }

    public function get_other_services(Request $request){
        //return self::get_services($request , 0 , false);
        $offset = 0;
        $limit = 0;
        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;

        $services = Service::where('user_id' , '!=' , Helper::api_current_userID($request->api_token))->whereStatus(2)->whereActive(1);
        return Helper::api_response_format(true , self::ServiceHandler($services , $limit , $offset));
    }

    public function get_my_services(Request $request){
        //return self::get_services($request , 0 , true);
        $status = ['pending' => 1, 'approved' => 2, 'disapproved' => 3, 'expired' => 4, 'trashed' => 5, 'active' => 6];
        $offset = 0;
        $limit = 0;
        if (isset($request->offset))
            $offset = $request->offset;
        if (isset($request->limit))
            $limit = $request->limit;
        $services = Service::where('user_id' , '=' , Helper::api_current_userID($request->api_token));
        if (isset($request->status)){
            if (!in_array($request->status, array_keys($status)))
                return Helper::api_response_format(false, '', 'Invalid status');
            $services->whereStatus($status[$request->status]);
        }
        return Helper::api_response_format(true , self::ServiceHandler($services , $limit , $offset));
    }

    private function ServiceHandler($services , $limit , $offset){
        $request = \request();
        if (isset($request->search))
        {
            $search_word  = $request->search;
            $services->where(function ($q) use ($search_word)
            {
                $q->where('name','LIKE','%'.$search_word.'%')
                    ->orWhere('english_name','LIKE','%'.$search_word.'%');
            });
        }
        //filter by category
        if (isset($request->sub_category))
        {
            $services->whereCategory_id($request->sub_category);
        }
        if (isset($request->parent_category))
        {
            if($request->parent_category <= 42) // then it's parent
            {
                $category = Category::whereParent_category_id($request->parent_category); // get all the sub category for this parent
                $category= $category->lists('id')->toArray();
                array_push($category,$request->parent_category);
                $services->whereIn('category_id',$category);
            }
            else // then it's sub
            {
                $services->whereCategory_id($request->parent_category);
            }
        }
        if (isset($request->country_id)) {
            $country = $request->country_id;
            $services->where(function ($q) use ($country)
            {
                $q->where('Locations' , 'like' , '%,'.$country.',%')
                    ->orWhere('Locations' , 'like' , $country.',%')
                    ->orWhere('Locations' , 'like' , '%,'.$country)
                    ->orWhere('Locations' , 'like' , $country);
            });

        }
        $index = 0 ;
        if ($limit)
            $services = $services->skip($offset)->take($limit)->get();
        else
            $services = $services->get();
        $returned_service = [];
        foreach ($services as $service){
            $countries_ids = explode(',',$service->locations);
            $service->image = url('public/uploads/' . $service->image);
            $service->document = url('public/uploads/' . $service->document);
            $service->locations = Country::whereIn('id',$countries_ids)->get();

            $returned_service[$index]['id']=$service->id;
            $returned_service[$index]['status']=$service->status;
            $returned_service[$index]['user_id']=$service->user_id;
            $returned_service[$index]['name']=$service->name;
            $returned_service[$index]['en_name']=$service->english_name;
            $returned_service[$index]['ar_name']=$service->name;
            $countries = Country::whereIn('id',$countries_ids)->get();

            $returned_service[$index]['min_price']=$service->price_min;
            $returned_service[$index]['max_price']=$service->price_max;
            $returned_service[$index]['expiration_date']=$service->expired_at;

            $returned_service[$index]['image']=$service->image;
            $returned_service[$index]['brochure']=$service->document;

            foreach ($countries as $country)
            {
                $country->country_icon = url('flags/24/'.$country->iso2.'.png');
            }
            $returned_service[$index]['description_ar'] = $service->arabic_description;
            $returned_service[$index]['description_en'] = $service->english_description;
            $returned_service[$index]['more_details_ar'] = $service->arabic_details;
            $returned_service[$index]['more_details_en'] = $service->english_details;
            $returned_service[$index]['country']=$countries;
            $index++;
            foreach ($service->locations as $country)
            {
                $country->country_icon = url('flags/24/'.$country->iso2.'.png');
            }
        }
        return $returned_service;
    }

    public function update_service(Request $request)
    {

        $service = Service::whereId($request->service_id)->whereUser_id(Helper::api_current_userID($request->api_token))->first();
        if (!$service)
            return Helper::api_response_format(false , '' , 'Service Not Found');
        $user = User::whereApi_token($request->api_token)->first();
        if($user->service_provider == 0){
            return Helper::api_response_format(false , '' , 'you are not allowed to update a service you are not a service provider');
        }
        $service->name = $request->name;
        $service->english_name = $request->english_name;
        $service->document = $request->document;
        $service->price_min = $request->price_min;
        $service->price_max = $request->price_max;
        $service->locations = implode(',', $request->locations);
        $service->expired_at = $request->expire_at;
        $service->arabic_description = $request->arabic_description;
        $service->english_description = $request->english_description;
        $service->arabic_details = $request->arabic_details;
        $service->english_details = $request->english_details;
        if (isset($request->image))
            $service->image =  $request->image;
        $service->save();
        return Helper::api_response_format(true, $service);
    }
}