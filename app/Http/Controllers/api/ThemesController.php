<?php

namespace App\Http\Controllers\api;

use App\Helper;
use App\Template;
use App\TemplateSteps;
use Illuminate\Http\Request;
use App\UserWebsite;
use Auth;
use App\User;
use App\Packages\UserPackage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ThemesController extends Controller
{
    public function GetAllThemese(Request $request){
        $theme = Template::whereIs_free(1)->get();
        foreach ($theme as $value){
            $value->image = url('public/uploads/' . $value->image);
        }
        return Helper::api_response_format(true , $theme);
    }

    public function SelectTheme(Request $request){
        $validator = Validator::make($request->all() , [
            'theme_id' => 'required',
            'store_name' => 'required|unique:'
        ]);
        if ($validator->fails()){
            return Helper::api_response_format(false , '' , $validator->errors());
        }
        $temp = TemplateSteps::create([
            'user_id' => Helper::api_current_userID($request->api_token),
            'select_template' => $request->theme_id
        ]);
        $domain = str_slug($request->store_name);

        $check = UserWebsite::whereDomain($domain)->where('user_id', '<>',  Helper::api_current_userID($request->api_token))->count();
        if($check>0) {
            return Helper::api_response_format(false , '' , 'This name is already taken');
        }
        $user_website = UserWebsite::whereUser_id(Auth::id())->first();
        if(!$user_website) {
            $user_website = UserWebsite::create([
                'user_id' => Helper::api_current_userID($request->api_token),
                'template_id' => $request->theme_id,
                'domain' => $domain,
                'status' => 0,
                'active' => 0
            ]);
        }
        $user_website->domain = $domain;
        $user_website->status = 1;
        if($user_website->save()) {
            UserWebsite::setDefaultData($user_website);
            Helper::api_response_format(true, 'لقد تم إرسال طلب نشر متجرك بنجاح إلى إدارة الموقع');
        }
        $steps = $temp;
        $steps->completed=1;
        $steps->save();
        //=================================select free package
        $check_package = UserPackage::whereUser_id(Auth::id())->first();
        if(!$check_package)
        {
            $data['package_id'] = 4;
            $data['user_id'] =  Helper::api_current_userID($request->api_token);
            $data['amount'] = 0;
            $data['currency'] = '';
            $data['payment_method'] = 'bank_transfer';
            $data['confirmed']= 1;
            $user_package = UserPackage::create($data);
        }

        $user = User::whereApi_token($request->api_token)->first();
        if(User::sendwelcomeEmail($user));

        return Helper::api_response_format(true , 'Theme selected successfully');
    }
}
