<?php

namespace App\Http\Controllers\api;

use App\Helper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FileCOntroller extends Controller
{
    public static $max_upload_doc_size = 3145728;
    public static $max_files_per_application = 5;
    public static $allowed_types = [
        'jpg','jpeg','png','gif',
    ];

    public function uploadSingleFile(Request $request)
    {
        $make_thumb = true;
        $request = request();
        if ($request->hasFile('file')) {
            //file ext
            $ext =  $request->file('file')->getClientOriginalExtension();
            if(!in_array($ext , FileCOntroller::$allowed_types))
            {
                $make_thumb = false;
            }
            return \App\Helper::api_response_format(true, \App\Helper::upload($request, 'file' , $make_thumb));
        }
        return \App\Helper::api_response_format(false, '', 'No image found in your request');
    }

    public function uploadMulipleFile(Request $request)
    {
        if ($request->hasFile('files')){
            $imgs = [];
            $year = date('Y');
            $month = date('m');
            $Images = $request->file('files');
            foreach ($Images as $image) {

                $profileImageSaveAsName = rand(10000000, 99999999) ."." . $image->getClientOriginalExtension();
                $upload_path = Helper::upload_path();
                $profile_image_url = $upload_path . $profileImageSaveAsName;
                $success = $image->move($upload_path, $profileImageSaveAsName);
                array_push($imgs ,$year.'/'.$month.'/' . $profileImageSaveAsName );
            }
            return Helper::api_response_format(true , $imgs);
        }
        return Helper::api_response_format(false , '' , 'You must provide files');
    }
}