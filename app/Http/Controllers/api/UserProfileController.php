<?php

namespace App\Http\Controllers\api;

use App\UserFactoryInfo;
use App\UserOffice;
use App\UserProfile;
use App\UserTradeInfo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\Helper;
use App\UserWebsite;
class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *input L user id
     * @return \Illuminate\Http\Response
     */
    public function get_profile(Request $request , $id)
    {
        $user_id = Helper::api_current_userID($request->api_token);
        //get the userID associated with that token and get the profiles associated with that user
        $user = UserProfile::whereUser_id($user_id)->with('country')
            ->with('co_country')->
            with("business_category")->
            whereProfile('temporary')->first();
        //َ
        $user_factory = UserFactoryInfo::whereUser_id($user_id)->whereProfile('temporary')->first();
        $user_trade_info = UserTradeInfo::whereUser_id($user_id)->whereProfile('temporary')->first();
        if ($user_factory) {
            $user->annual_output_value = $user_factory->annual_output_value;
            $user->contract_manufacturings = $user_factory->contract_manufacturings;
        }
        if($user_trade_info)
        {
            $user->export_percentage = $user_trade_info->export_percentage;
        }
        if ($user) {
            $store = UserWebsite::whereUser_id($user->id)->first();
            $user->store = null;
            if ($store != null){
                $user->store = url('store/' . $store->domain);
            }
            //now let's  attach the full url to the images
            if($user->co_logo)
                $user->co_logo = url('uploads/'.$user->co_logo);
                $user->identity_proof = url('uploads/'.$user->identity_proof);
                $user->identity_proof2 = url('uploads/'.$user->identity_proof2);
                $user->picture = url('uploads/'.$user->picture);
            //return the user profiles
            return Helper::api_response_format(true, $user);
        }//end use if
        else {//user not found
            return Helper::api_response_format(false, '', 'User Doesn\'t  Has Profile');
        }
    }
            /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function create_profile(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'country_id' => 'required',
            'mob_code' => 'required',
            'mobile' => 'required'
        ]);
        if ($validator->fails()) {
            return Helper::api_response_format(false, '', $validator->errors());
        }
        //get object from userprofile
        $Profile = new UserProfile();
        //create profile
        $created_profile = $Profile->create([
            'user_id' => Helper::api_current_userID($request->api_token),
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'country_id' => $request->country_id,
            'mob_code' => $request->mob_code,
            'mobile' => $request->mobile,
            'city' => $request->city,
            'state' => $request->state,
            'profile' => 'temporary'
        ]); //take every key except the api_token key
        if ($created_profile) { // created
            return Helper::api_response_format(true, $created_profile);
        } else { // else not created

            return Helper::api_response_format(false, '', 'error in creating profile');
        }


    }

    public function update_profile(Request $request)
    {
        //current user
        $current_user = User::whereApi_token($request->api_token)->first();
        //current user id
        $user_id = $current_user->id;

        $user_profile = UserProfile::whereUser_id($user_id)->whereProfile('temporary')->with('offices')->first();

        if (!$user_profile)//if it's empty
            return Helper::api_response_format(false, '', 'No user profile for this user');

        if ($user_profile->editable == 0) {
            return Helper::api_response_format(false, '', 'Currently you are not allowed to edit your profile');
        }

        $data = $request->except(['annual_output_value', 'contract_manufacturings', 'export_percentage', 'api_token', '_method', 'image']);

        // $user = $current_user;
        // foreach (\App\User::$types as $type => $label) {
        //     if (isset($data['user_type']) && in_array($type, $data['user_type'])) {
        //         $user->$type = 1;
        //     } else {
        //         $user->$type = 0;
        //     }
        // }
        // $user->save();


        //Divisional Offices
        if (isset($data['divoffice'])) {
            $dataOffices = $data['divoffice'];
            $officesArr = [];
            foreach ($dataOffices['division'] as $index => $div) {
                if ($index == 0) {
                    continue;
                }
                foreach (UserOffice::$fields_arr as $field => $action) {
                    if (isset($dataOffices[$field][$index])) {
                        $officesArr[$index][$field] = $dataOffices[$field][$index];
                    }
                }
            }
            foreach ($officesArr as $index => $dataRow) {
                $dataRow['user_id'] = auth()->id();
                $dataRow['user_profile_id'] = $user_profile->id;
                if (isset($dataRow['id']) && !empty($dataRow['id'])) {
                    $office_row = UserOffice::whereId($dataRow['id'])->first();
                    if ($office_row) {
                        $office_row->update($dataRow);
                    }
                } else {
                    UserOffice::create($dataRow);
                }
            }
            unset($data['divoffice']);
        }//check if

        if ($request->has('image')) {
            $data['co_logo'] = $request->image;
        }

        if (isset($request->annual_output_value)) {

            $factory = UserFactoryInfo::whereUser_id($user_id)->whereProfile("temporary")->first();
            if ($factory == null) {
                $factory = UserFactoryInfo::create([
                    'user_id' => $user_id,
                ]);
            }
            if($request->input('contract_manufacturings'))
            {
                $contract_manufacturings = implode(',', $request->input('contract_manufacturings'));
                $factory->contract_manufacturings = $contract_manufacturings;
            }
            $factory->annual_output_value = $request->annual_output_value;
            $factory->save();

        }

        if (isset($request->export_percentage)) {
            $trade = UserTradeInfo::whereUser_id($user_id)->whereProfile("temporary")->first();
            if ($trade == null) {
                $trade = UserTradeInfo::create([
                    'user_id' => $user_id,
                ]);
            }
            $trade->export_percentage = $request->export_percentage;
            $trade->save();
        }
        //end Trade Info data
        if ($user_profile->update($data)) {

            //Update Template Steps
            \App\TemplateSteps::update_step(['personal_profile', 'company_profile'], $current_user);

            return Helper::api_response_format(true, 'your data has updated successfully');
        }
        return Helper::api_response_format(true, '', 'Some Error Occurred');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_profile($id)
    {
        //get profile by id
        $profile = UserProfile::find($id);
        //delete the profile
        return Helper::api_delete($profile,'profile not found');
    }
}
