<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Deals\Deal;
use App\Deals\QuoteRequest;
use App\Deals\QuoteRequestImage;
use App\quote_request_seen;
use App\Template;
use App\TemplateSteps;
use App\UserProfile;
use App\UserWebsite;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helper;
use Validator;
use App;

class ExportOpportunitiesController extends Controller
{
    /*
     * get specific export opportunities
     * input : could be slug or id of the deals
     * output: the deals info
     */
    public function get_export_opportunities($id)
    {
        $offer = new QuoteRequest();
        //if it provide slug or id
        if (is_numeric($id)) {
            $offer = $offer->whereId($id);
        } else {
            $offer = $offer->whereSlug($id);

        }
        //get the deal
        $offer = $offer->whereActive(1)->whereStatus(2)->with('images', 'quotes')->first();
        // it exist or not
        if (!$offer) {
            return Helper::api_response_format(false, '', 'Export opportunities not found');
        }
        $category_parents = [];
        if ($offer) {
            $category_parents = ($offer->category_id) ? Category::get_parents($offer->category_id) : '';
            $offer->supplier_location = QuoteRequest::supplier_locations($offer);

            //get main category id and save seen for this product if this your category
            $cat_id = 0;
            if ($category_parents) {
                $counter = count($category_parents) - 1; // used to get the last index in the array
                if ($category_parents[$counter]) { // if the $category u selected is not = null
                    $cat_id = $category_parents[$counter]->id;
                } else {//take the the second one from down
                    $cat_id = $category_parents[$counter - 1]->id;
                }

            } else
                $cat_id = $offer->parent_cat_id;

            $user_profile = UserProfile::whereUser_id(\Auth::id())->orderBy('id', 'asc')->first();
            if ($user_profile) {
                if ($user_profile->other_products == $cat_id) {
                    $check_seen_before = quote_request_seen::whereUser_id(Auth::id())->whereQuoute_req_id($offer->id)->get();
                    if ($check_seen_before->count() > 0) ;//nothing
                    else
                        $quote = quote_request_seen::create(['user_id' => Auth::id(), 'quoute_req_id' => $offer->id]);
                }//if this your category
            }//end check if has profile

            //get importer country object
            $offer->im_country = QuoteRequest::get_im_country($offer);
        }
        return Helper::api_response_format(true, ['export_opp' => $offer, 'category_parents' => $category_parents]);
    }

    /*
     * get all the export opportunities
     * input : optional limit number of rows
     * output : a json with the data
     */
    public function get_all_export_opportunities(Request $request, $limit=0, $specific_user=false, $rfq_id=0 , $rfq_status = 2)
    {
        $limit = $request->limit;
        if(!isset($request->limit))
            $limit = 0;
        $status = [2 => 'approved', 1 => 'pending', 3 => 'disapproved', 4 => 'expired'];

        $returend_colums = ['id' , 'user_id', 'unit_price', 'status', 'name_ar', 'name_en', 'details_ar', 'details_en', 'order_quantity', 'duration_from', 'duration_to'
            , 'expired_at', 'supplier_location','status','unit','port','annual_purchase', 'purchase_unit', 'frequency', 'payment_terms', 'brand', 'currency',  'category_id', 'im_country'];
        //limit 0 mean get all the data inside the table
        $user_id = Helper::api_current_userID($request->api_token);
        //get to specifc user
        
        $deals = new QuoteRequest;
        
        if ($specific_user) {
            $deals = $deals->whereUser_id($user_id);
        }

        if ($rfq_id)
            $deals = $deals->whereId($rfq_id);
        if($rfq_status)
             $deals = $deals->whereStatus($rfq_status);

        //check if it's empty or not
        if($deals->get()->isEmpty()) return Helper::api_response_format(true,$deals->get());

        if (isset($request->offset))
            $offset = $request->offset;
        else
            $offset = 0;
        //check for filteration
        if (isset($request->sub_category)) //if it's already sub then get the category directly
        {
            $deals = $deals->whereCategory_id($request->sub_category);
        }
        if (isset($request->parent_category)) //if it's parent then go and check if it's parent or not
        {
            if ($request->parent_category < 42) // then it's parent
            {
                $category = Category::whereParent_category_id($request->parent_category); // get all the sub category for this parent
                $category = $category->lists('id')->toArray();
                array_push($category, $request->parent_category);
                $deals = $deals->whereIn('category_id', $category);
            } else { // then it's sub
                $deals = $deals->whereCategory_id($request->parent_category);
            }
        }
        $search = urldecode($request->search);
        if (isset($request->search)) {
            $deals = $deals->where(function ($q) use ($search) {
              $q->where('name_ar', 'LIKE', '%' . $search. '%')
                  ->orWhere('name_en', 'LIKE', '%' . $search . '%')
                  ->orWhere('details_en', 'LIKE', '%' . $search. '%')
                  ->orWhere('details_ar', 'LIKE', '%' . $search. '%');
            });
        }
        if (isset($request->country_id)) {
            $deals = $deals->whereIm_country($request->country_id);
        }
        // filter by status
        if (isset($request->status)) {
            $deals = $deals->whereStatus(array_search($request->status, $status));
        }
        $deals= $deals->with('images', 'category')->orderBy('id', 'DESC');
        $deals = Helper::check_collection_limit($deals, $limit, $offset)->get($returend_colums);

        //attach the full url to the images
        foreach ($deals as $deal)
        {
            $deal->user = User::find($deal->user_id);
            $deal->category = Category::find($deal->category_id);
            $deal->status = $status[$deal->status];
            $country = App\Country::whereId($deal->im_country)->first();
            $deal->supplier_location = QuoteRequest::supplier_locations($deal);
            $deal->im_country = QuoteRequest::get_im_country($deal);
            $deal->payment_terms = explode(',' , $deal->payment_terms);
            $deal->lock = self::GetRFQStatus($deal->id, $user_id);
            if($country){
                $deal->flag = url('flags/24/' . $country->iso2 . '.png');
            }
            foreach ($deal->images as $image) {
                $image->image = url('public/uploads/' . $image->image);
            }
        }
        return Helper::api_response_format(true, $deals);
    }

    /*
     * get all the export opportunities associated with the importer id
     * input : optional limit number of rows
     * output : a json with the data
     */
    public function get_my_export_opportunities(Request $request, $limit = 0)
    {
        $limit = 0;
        if(isset($request->limit)) $limit = $request->limit;

        return self::get_all_export_opportunities($request, $limit, true, 0, null);
    }

    /*open the rfq and get it details
     * input : RFQ ID
     * output: the opened RFQ
     */
    public function open_Rfq(Request $request, $rfq_id)
    {
        $user_id = Helper::api_current_userID($request->api_token);
        $quote_exist = quote_request_seen::whereUser_id($user_id)->whereQuoute_req_id($rfq_id)->first();
        if ($quote_exist)
            return Helper::api_response_format(false, '', 'RFq opened before');

        $quote_seens = quote_request_seen::create([
            'user_id' => $user_id,
            'quoute_req_id' => $rfq_id
        ]);
        App\PackageUsage::create([
            'user_id' => $user_id,
            'package_id' => App\Packages\UserPackage::whereUser_id($user_id)->first()->package_id,
            'deal_id' => $rfq_id,
            'item' => 'unlock_contact',
            'value' => '1' ,
            'year' =>  date("Y"),
            'month' =>  date("M")
        ]);
        if (!$quote_seens) return Helper::api_response_format(false, '', 'failed to open RFQ');

        //now return the rfq with it's details
        return self::get_all_export_opportunities($request, 0, false, $rfq_id);
    }

    /*create export opportunities
     * input : none
     *  output: json with the opp created
     */
    public function create_export_opportunities(Request $request)
    {
        //make validate
        $validator = Validator::make($request->all(), [
            'name_ar' => 'required|max:60',
            'name_en' => 'required|max:60',
//            'details_ar' => 'required|max:255',
            'details_en' => 'required|max:255',
            'payment_terms' => 'required',
            'order_quantity' => 'required|numeric',
            'unit' => 'required|max:60',
            'duration_from' => 'required|date|date_format:Y-m-d',
            'duration_to' => 'required|date|date_format:Y-m-d',
            'expired_at' => 'required|date|date_format:Y-m-d',
            'category_id' => 'required',
            'supplier_location' => 'required',
            'frequency' => 'required',
            'port' => 'required',
            'unit_price' => 'required',
            'purchase_unit' => 'required',
            'annual_purchase' => 'required',
            'im_country' => 'required',

        ]);

        //if the validation fails
        if ($validator->fails()) {
            //make response with the validations fails message and succ false
            return Helper::api_response_format(false, '', $validator->messages());
        }

        $current_userid = Helper::api_current_userID($request->api_token);
        $request_data['user_id'] = $current_userid;

        $request_data = $request->except('add_images', 'api_token');

        if (isset($request_data['payment_terms'])) {
            //if the payment type not array
            if (!is_array(($request_data['payment_terms']))) return Helper::api_response_format(false, '', 'payment terms must be array');
            $request_data['payment_terms'] = implode(',', $request_data['payment_terms']);
        }

        if($request_data['supplier_location']) {
            $request_data['supplier_location'] = implode(',', $request_data['supplier_location']);
        }

        $request_data['status'] = 1; //pending
        $request_data['slug'] = str_slug($request_data['name_' . App::getLocale()]);
        $request_data['user_id'] = $current_userid;
        $quote_request = QuoteRequest::create($request_data);
        
        //failed in create quote
        if (!$quote_request) {
            return Helper::api_response_format(false, '', 'Quote didn\'t created successfully');
        }

        if ($quote_request)
        {
            //Upload & Save Additional Images
            if ($request->has('add_images')) {
                $images = self::upload_RFQ_files($request->add_images, $quote_request->id);
            }

            //if the mobile app didn't provide the request_for colum
            if (!isset($request_data['request_for'])) $request_data['request_for'] = 'product';
            //Save As Common Deal
            $deal = Deal::create([
                'user_id' => $current_userid,
                'section' => 'quote_request',
                'record_id' => $quote_request->id,
                'type' => $request_data['request_for'],
                'status' => $quote_request->status,
                'expired_at' => $request_data['expired_at']
            ]);
        }
        $come_from = $request->headers->get('referer');
        $quote_request->images;
        return Helper::api_response_format(true, $quote_request);
    }

    /* update specifc export opportunities
     * input :id of specifc opp
     */
    public function update_export_opportunities(Request $request, $id)
    {
        //make validate
        $validator = Validator::make($request->all(), [
            'name_ar' => 'required|max:60',
            'name_en' => 'required|max:60',
            'details_ar' => 'required|max:255',
            'details_en' => 'required|max:255',
            'payment_terms' => 'required',
            'order_quantity' => 'required|numeric',
            'unit' => 'required|max:60',
            'duration_from' => 'required|date|date_format:Y-m-d',
            'duration_to' => 'required|date|date_format:Y-m-d',
            'expired_at' => 'required|date|date_format:Y-m-d',
        ]);

        //if the validation fails
        if ($validator->fails()) {
            //make response with the validations fails message and succ false
            return Helper::api_response_format(false, '', $validator->messages());
        }

        //get the current user id
        $current_userid = Helper::api_current_userID($request->api_token);

        $quote_request = QuoteRequest::whereId($id)->with('images')->first();
        $request_data = $request->except('add_images', 'api_token', '_method' , 'image');

        if ($request->hasFile('image')) {
            $request_data['image'] = Helper::upload($request, 'image');
        }
        if (isset($request_data['supplier_location'])) {
            $request_data['supplier_location'] = implode(',', $request_data['supplier_location']);
        }
        if (isset($request_data['payment_terms'])) {
            //if the payment type not array
            if (!is_array(($request_data['payment_terms']))) return Helper::api_response_format(false, '', 'payment terms must be array');
            $request_data['payment_terms'] = implode(',', $request_data['payment_terms']);
        }
        if (isset($request_data['selected_category_id'])){
            $request_data['category_id'] = $request_data['selected_category_id'];
        }

        unset($request_data['selected_category_id']);
        $request_data['slug'] = str_slug($request_data['name_' . App::getLocale()]);
        $request_data['user_id'] = $current_userid;
        if ($request->has('add_images')){
            $this->upload_RFQ_files($request->get('add_images') , $quote_request->id);
        }
        unset($request_data['add_images']);

        $quote_request->update($request_data);


        //if the mobile app didn't provide the request_for colum
        if (!isset($request_data['request_for'])) $request_data['request_for'] = 'product';
        //Save As Common Deal
        $deal = Deal::whereSection('quote_request')->whereRecord_id($id)->first();
        if (!$deal) {
            $deal = new Deal;
            $deal->user_id = $current_userid;
            $deal->section = 'quote_request';
            $deal->record_id = $id;
            $deal->status = $quote_request->status;
        }
        $deal->type = $request_data['request_for'];
        $deal->expired_at = $request_data['expired_at'];
        $deal->save();

        //return json
        return Helper::api_response_format(true, $quote_request);
    }

    /*
     * put the export opp into trash by set status to 4
     * input : id of the export opp
     * output: deleted row
     */
    public function delete_export_opportunities($id)
    {
        // get the quote
        $quote_request = QuoteRequest::find($id);
        //if we didn't find it return error
        if (!$quote_request) return Helper::api_response_format(false, '', 'export opportunities not found');

        if ($quote_request->status == 4) return Helper::api_response_format(false, '', 'quote already in trash');

        $quote_request->restored_as = $quote_request->status; //If restored set its status as this

        $quote_request->status = 4; //4: trash

        if ($quote_request->save()) {
            return Helper::api_response_format(true, $quote_request);
        }
        //else
        return Helper::api_response_format(false, '', 'some error happened while deleting');
    }

    /*
     * put the export opp into trash by set status to 4
     * input : id of the export opp
     * output: deleted row
     */
    public function restore_export_opportunities($id)
    {
        // get the quote
        $quote_request = QuoteRequest::find($id);
        //if we didn't find it return error
        if (!$quote_request) return Helper::api_response_format(false, '', 'export opportunities not found');
        //check if it's already not trashed
        if ($quote_request->status != 4) return Helper::api_response_format(false, '', 'quote is not in trash to restore it');
        $quote_request->status = $quote_request->restored_as; //The status was before trash

        if ($quote_request->save()) {
            return Helper::api_response_format(true, $quote_request);
        }
        //else
        return Helper::api_response_format(false, '', 'some error happened while deleting');
    }

    public function GetNewRFQs(Request $request)
    {
        if(!isset($request->limit)) $request->limit = 10;
        if(!isset($request->offset)) $request->offset = 0;
        return self::get_all_export_opportunities($request);
    }

    public function GetNotOpenRequests(Request $request)
    {
        $seen = quote_request_seen::whereUser_id(Helper::api_current_userID($request->api_token))->get(['quoute_req_id']);
        return Helper::api_response_format(true, QuoteRequest::whereNotIn('id', $seen)->whereUser_id(Helper::api_current_userID($request->api_token))->get());
    }

    public function AvailableRFQForImpoerter(Request $request)
    {
        $profile = UserProfile::whereUser_id(Helper::api_current_userID($request->api_token))->first();
        $limit = 0;
        $offset = 0;

        $business_category = $profile->business_category;
        $preferred_category = $profile->preferred_category;

        $rfqs = new QuoteRequest;

        $rfqs = $rfqs->where('parent_cat_id' , '!=' , $profile->other_products);
        
        $rfqs = $rfqs->whereStatus(2)->whereActive(1);

        if ($request->has('search') && !empty($request->get('search')))
        {
            $q = $request->get('search');
            $keyword = urldecode($q);
            $keyword = '%'.$keyword.'%';

            $rfqs = $rfqs->where(function ($q) use ($keyword) {
                $q->where('name_ar', 'LIKE', $keyword)
                ->orWhere('name_en', 'LIKE', $keyword)
                ->orWhere('unit', 'LIKE', $keyword)
                ->orWhere('frequency', 'LIKE', $keyword)
                ->orWhere('details_en', 'LIKE', $keyword)
                ->orWhere('details_ar', 'LIKE', $keyword)
                ->orWhere('id', 'LIKE', $keyword);
            });
        }

        if ($request->has('sub_category') && !empty($request->get('sub_category')))
        {
            $rfqs = $rfqs->whereCategory_id($request->get('sub_category'));
        }
        if (isset($request->country_id)) {
            $rfqs = $rfqs->whereIm_country($request->country_id);
        }
        if (isset($request->limit)) {
            $limit = $request->limit;
        }
        if (isset($request->offset)) {
            $offset = $request->offset;
        }
        $rfqs = $rfqs->orderBy('id', 'DESC');

        $rfq = self::HandleRFQ($rfqs, $limit, $offset, Helper::api_current_userID($request->api_token));
        return Helper::api_response_format(true, $rfq);
    }

    private function HandleRFQ($rfq, $limit, $offset, $user_id)
    {
        if ($limit) {
            $rfq = $rfq->skip($offset)->take($limit);
        }
        
        $rfq = $rfq->with('category')->get();

        foreach ($rfq as $item) {
            $item->user = User::find($item->user_id);
            $item->lock = self::GetRFQStatus($item->id, $user_id);
            $item->im_country = self::getCountry($item->im_country);
            
            if ($item->im_country){
            }    
            $item->payment_terms = explode(',' , $item->payment_terms);
            $item->supplier_location = QuoteRequest::supplier_locations($item);
            foreach ($item->images as $rfq_img) {
                $rfq_img->image = url('public/uploads/' . $rfq_img->image);
            }
        }
        return $rfq;
    }

    private function GetRFQStatus($rfq_id, $user_id)
    {
        $quote = quote_request_seen::whereUser_id($user_id)->whereQuoute_req_id($rfq_id)->first();
        if ($quote)
            return 0;
        return 1;
    }

    private function getCountry($country_id)
    {
        if (isset($country_id))
            return App\Country::find($country_id);
        return null;
    }

    public function upload_RFQ_files($files_request, $record_id)
    {
        QuoteRequestImage::destroy(QuoteRequestImage::whereQuote_request_id($record_id)->delete());
        $images = [];
        $upload_path = Helper::upload_path();
        $name_sub = date('Y/m');

        foreach ($files_request as $file) {

            $images[] = QuoteRequestImage::create(['quote_request_id' => $record_id, 'image' => $file]);
        }

        return $images;
    }

    public function rfq_delete_img(Request $request)
    {
        $id = $request->get('id');
        $model = '\App\Deals\QuoteRequestImage';

        if(Helper::postDeleteimage($id, $model))
        {
            return Helper::api_response_format(true, 'Deleted');
        }

        return Helper::api_response_format(false, '', 'An error occured while deleting image');
    }
}