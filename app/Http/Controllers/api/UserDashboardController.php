<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TemplateSteps;
use App\UserProfile;
use App\User;
use App\Packages\UserPackage;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Product;
use App\Deals\QuoteRequest;
use App\Deals\Quote;
use App\Messages\Message;
use App\Advs;
use App\quote_request_seen;
use App\Country;
use App\Blog;
use Session;
use App\BlogCategory;
use App\Helper;
use App\Menu;
class UserDashboardController extends Controller
{
    public function get_dashboard($id)
    {
        $constant_packages_products = array('Yalla Start' => 25 ,'Yalla Free' => 10,'Yalla Go' => 50,'Yalla VIP' => 100);
        $constant_packages_price_offer = array('Yalla Start' => 20 ,'Yalla Free' => 0,'Yalla Go' => 50,'Yalla VIP' => 1000000);
        $package_no_product  = 1;
        $package_price_offer = 1;

        $dash_adv = Advs::whereActive(1)->get();
        $products = Product::whereUser_id($id)->get();
        $all_admins = User::whereAdmin(1)->get();
        $user_profile = UserProfile::whereUser_id($id)->first();
        $user = User::whereId($id)->first();
        
        if($user)
        {
            if($user_profile)
            {
	        $user_package = UserPackage::whereUser_id($id)->orderBy('id', 'desc')
	        ->first();
	        $quote = Quote::whereFrom_user_id($id)->get();
	        
	        $template_steps = TemplateSteps::whereUser_id($id)->first();
	       
	        $quoterequest   = QuoteRequest::whereParent_cat_id($user_profile->other_products)->where('expired_at',">=",date('Y-m-d'))->count();
	        
	        if($quoterequest==0)$quoterequest=1;
	        $items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
	        $quote_request_seen = quote_request_seen::whereUser_id($id)->get();
	        $sections = Package::$sections;

	        $blogs_cat = BlogCategory::all();

	        $blogs = Blog::whereActive(1)->get();
	        if($user->first_time == 0)
	        {
	             Session::flash('flash_message', "تم تسجيلك بنجاح وفي انتظار موافقة الادارة علي عضويتك");
	            $user->first_time=1;
	            $user->save();
	        }
	        if($user_package)
	        { 
	            $packages = Package::where('id',"=",$user_package->package_id)->first();
	            $package_no_product = $constant_packages_products[$packages->name_en];
	            $package_price_offer = $constant_packages_price_offer[$packages->name_en];
	            if($products->count() > 0)
	            {
	                $date_package =$user_package->created_at;
	                $products = Product::whereUser_id($id)->Where('created_at',">=",$date_package)->get();
	                // $quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
	                $quote = Quote::whereFrom_user_id($id)->Where('created_at',">=",$date_package)->get();
	            }//end has products
	        }//end user_package
	        else
	            $user_package = 0;
	        //******************************************************************************************************************
	        //create dashboard data array
	        $dashboard_data = array();
	        //get company logo
	        if($user_profile->co_logo)
	        	$dashboard_data = array_add($dashboard_data , 'company_logo' , $user_profile->co_logo);
	    	else
	    		$dashboard_data = array_add($dashboard_data , 'company_logo' , 'company_logo.jpg');
	    	//get company name
	    	$dashboard_data 	= array_add($dashboard_data , 'company_name' , $user_profile->company_name);
	    	//-------------------------------------------------------------------------------------------------------------------
	    	//get package details
	    	if($user_package)
	    	{
	    		$dashboard_data = array_add($dashboard_data , 'package_name' , $packages->name_ar);
	    		$dashboard_data = array_add($dashboard_data , 'package_type' , $packages->brief_en);
	    		$dashboard_data = array_add($dashboard_data , 'package_start_date' , date('d-m-Y',strtotime($user_package->updated_at)));
	    		$end_date 		= strtotime('+ 1 year', strtotime($user_package->updated_at));
	    		$dashboard_data = array_add($dashboard_data , 'package_end_date' , date('d-m-Y', $end_date));
	    	}//end if user has package
	    	else
	    	{
	    		$dashboard_data = array_add($dashboard_data , 'package_name' , "");
	    		$dashboard_data = array_add($dashboard_data , 'package_type' , "");
	    		$dashboard_data = array_add($dashboard_data , 'package_start_date' , "");
	    		$dashboard_data = array_add($dashboard_data , 'package_end_date' , "");
	    	}//if user has not package
	    	//-------------------------------------------------------------------------------------------------------------------
	    	//get user details
	    	$dashboard_data 	= array_add($dashboard_data , 'user_reg_date' , date('d-m-Y', strtotime($user->created_at)));
	    	$user_reg_perc  	= ($template_steps->personal_profile+$template_steps->add_products+$template_steps->select_template+$template_steps->completed)/4*100;
	    	$dashboard_data  	= array_add($dashboard_data , 'user_reg_percentage' ,$user_reg_perc.'%');
	    	//get account_overview values
	    	$dashboard_data  	= array_add($dashboard_data , 'package_no_product' , $package_no_product);
	    	$dashboard_data  	= array_add($dashboard_data , 'package_no_offers' , $package_price_offer);
	    	$dashboard_data  	= array_add($dashboard_data , 'no_product' , $products->count());
	    	$dashboard_data  	= array_add($dashboard_data , 'no_export_opportunities' ,$quoterequest);
	    	$dashboard_data  	= array_add($dashboard_data , 'no_seen_export' , $quote_request_seen->count());
	    	$dashboard_data  	= array_add($dashboard_data , 'no_post_offers' ,$quote->count() );
	    	//-------------------------------------------------------------------------------------------------------------------
	    	//get Adv
	    	$dashboard_data  	= array_add($dashboard_data , 'Advertisement_news_events' ,	$dash_adv );
	    	$dashboard_data  	= array_add($dashboard_data , 'blogs' , $blogs);
	        //******************************************************************************************************************
	        return Helper::api_response_format(true , $dashboard_data);
            }
            else
            {
                return Helper::api_response_format(true ,["you have not user profile"]);
            }
    	}//if user found
    	else
    	{
    		return Helper::api_response_format(false,'','User Not Found');
    	}// if user not found
    }//end get dashboard and accountoverview
}