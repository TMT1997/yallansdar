<?php

namespace App\Http\Controllers\api;

use App\Helper;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Packages\UserPackage;
use App\PackageUsage;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    //        return $products->count() .'/'. $constant_packages_products[$packages->name_en];
    public function GetPackageDetails(Request $request){

        $constant_packages_products = array('Yalla Start' => 25 ,'Yalla Free' => 10,'Yalla Go' => 50,'Yalla VIP' => 100);
        $user_id = Helper::api_current_userID($request->api_token);

        $user_package = UserPackage::whereUser_id($user_id)->first();
        if($user_package) $user_package = $user_package->package_id;

        $packages = Package::where('id', "=",$user_package )->first();

        $products = Product::whereUser_id(Helper::api_current_userID($request->api_token))->get();


        $key = 'unlock_contact';         //item code is unlock contact
        $user_package_info = null;              // object to hold all the info we need to return to the ajax

        //get the newest updated package
        $user_package = UserPackage::where([["user_id","=",Helper::api_current_userID($request->api_token)]
            ,["confirmed","=","1"]])->orderby('created_at','desc')->with('package')->first(); //get the package the user assigned to
        //get the whole package
        if(!$user_package)
            return Helper::api_response_format(false,'','You don\'t  have package yet');
            $package_date = UserPackage::where("user_id","=",$user_id)->pluck('updated_at'); //get the package the user assigned to

            $user_package_info['id']=$user_package->package_id; // get the package from the user package then the package name
            //add it to our object
            $user_package_info['package']=$user_package['package']['name_en']; // get the package from the user package then the package name

            //add the start date to our object
            //$user_package_info['added_on_2'] = $package_date[0]; // the date was in array so we need [0] to get it
            //calculate the year from the start date
            $user_package_info['expire_at'] = PackageUsage::getAge($package_date[0]);
            //Get item info
            $unlock_item = PackageItem::whereActive(1)->where('code', $key)->first(); // get the package item with code $key ( add product or ..)
            
          
            $date_package =$user_package->created_at; // the time the package created in


            $item_info = PackageUsage::get_item($user_package->package, $unlock_item); //
            
            //Get user usage
            $user_usage = PackageUsage::get_usage($key,Helper::api_current_userID($request->api_token),null,null,$date_package); // get the usage of specifc user
            
            $item_total = $item_info['value'];

            $user_total = $user_usage->count();
            
            if(!empty($item_info['custom_value'])) {
                $item_total = $item_info['custom_value'];
            }
            $user_package_info['used_offers'] = $user_usage->count();
            $user_package_info['allowed_offers'] = $item_total; // add the number of unlocks to the item
            $user_package_info['number_of_allowed_products'] = $constant_packages_products[$packages->name_en];
            $user_package_info['number_of_used_products'] = $products->count();

        return Helper::api_response_format(true , $user_package_info);
    }

    public function SelectPackage(Request $request)
    {
        $package = Package::whereId($request->package_id)->first();
        if ($package) {
            $paid = 0;
            $confirmed = 0;
            if($package->name_en == 'Yalla Free') {
                $paid = 1;
                $confirmed = 1;
            }

            if (!isset($request->paymentMethod)) {
                return Helper::api_response_format(false , '' , 'You must provide a payment method');
            }

            UserPackage::create([
                'user_id' => Helper::api_current_userID($request->api_token),
                'package_id' => $package->id,
                'payment_method' => $request->paymentMethod,
                'amount' => $package->price,
                'currency' => 'EGP',
                'paid' => $paid,
                'confirmed' => $confirmed,
            ]);
            return Helper::api_response_format(true , 'Created successfully');
        }
        return Helper::api_response_format(false , '' , 'No Package found');
    }

    public function GetPackageAllDetails(Request $request)
    {
        return Helper::api_response_format(true , Package::packages_with_items());
    }
}
