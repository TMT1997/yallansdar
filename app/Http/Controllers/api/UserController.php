<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests;

use App\PackageUsage;
use App\Country;
use App\PasswordReset;
use App\UserProfile;
use App\User;
use App\Helper;
use App\Packages\UserPackage;
use Validator;
use Mail;

class UserController extends Controller
{
    public function send_activation(Request $request)
    {
        $user = User::whereApi_token($request->api_token)->first();
        
        if($user)
        {    
            $data['activation_url'] = url('registration/activate?activation='.Hash::make($user->email).'&email='.$user->email.'&mobapp=1');
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $data['indentifier'] = Hash::make($user->id);
            
            /*$Profile = new UserProfile();
            $Profile->create([
                'user_id' => $user->id
            ]);*/

            if(!$user->profiles) {
                $user_profile = UserProfile::init($user);
            }

            Mail::send('activate',  ['data' => $data] , function($message) {
                $message->to(User::whereApi_token(request()->api_token)->first()->email , 'Yallansadar')
                    ->subject(trans('front.activate_acc_onyallansadar'));
                $message->from('yallansadar@gmail.com','Yallansadar');
            });

            return Helper::api_response_format(true , "Done" , '');
        }
    }

    public function Reset(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email'
        ]);
        //dd($validator);
        if(! $validator->fails())
        {

            if( $user = User::where('email', $request->email)->first() )
            {
                $token = str_random(64);

                PasswordReset::create([
                    'email' => $request->email,
                    'token' => $token
                ]);
                $data = [
                    'email' => $request->email,
                    'token' => $token
                ];

                Mail::send('bl7', ['data' => $data], function($message) {
                    $message->to(request()->email, 'Yallansadar')
                        ->subject('Yalla Testing Mail');
                    $message->from('yallansadar@gmail.com','Yallansadar');
                });

                return Helper::api_response_format(true , "Done" , '');
            }
        }
        return Helper::api_response_format(false , "" , $validator->errors());
    }

    public function login(Request $request){
        //make validation on the email and password
        $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:80',
            'password' => 'required|max:80',
        ]);
        //if the validation fails
        if($validator->fails()){
            //make response with the validations fails message and status false
            return Helper::api_response_format(false,'',$validator->messages());
        }//end if
        else{
            //search if the email exist or not
            $user = User::whereEmail($request->email)->first();
            //if the email not exist
            if(empty($user)){
                return Helper::api_response_format(false,'','Email doesn\'t exist');
            }
            else{//user exist
                //get the incrypted password
                $incrypt_password = $user->password;
                //check if the password sent match the existing one
                if(password_verify($request->password , $incrypt_password)){
                    //matched

                    //Set token if empty
                    if(!$user->api_token || empty($user->api_token)) {
                        $user->api_token = str_random(64);
                        $user->save();
                    }

                    //return with the user token
                    return Helper::api_response_format(true,['token' => $user->api_token,'id' => $user->id]);
                }
                else{//not matched
                    return Helper::api_response_format(false,'','password isn\'t matched');
                }
            }


        }
    }
    public function register(Request $request){

        //make validate
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|max:80|unique:users',
            'mobile'=>'required|numeric',
            'password' => 'required|min:6|confirmed',
        ]);

        //if the validation fails
        if($validator->fails()){

            //make response with the validations fails message and succ false
            return Helper::api_response_format(false,'',$validator->messages());
        }//end if
        else{//if the validation holds
            //create user
            $request['slug'] = \App\Helper::generate_slug('\App\User', $request['name']);
            $user = User::create([
                'last_name' => $request['last_name'],
                'name' => $request['name'],
                'email' => $request['email'],
                'mobile' => $request['mobile'],
                'slug' => $request['slug'],
                'password' => bcrypt($request['password']),
                'verification_code' => str_random(50),
                'api_token' => str_random(60),// for security of the api
            ]);

        }
        PackageUsage::create([
                'package_id' => 4 ,
                'user_id' => $user->id,
        ]);
        UserPackage::create([
            'user_id' => $user->id ,
            'package_id' => 4 ,
            "paid" => 1,
            "confirmed" => 1
        ]);
        //return the response with the token
        return Helper::api_response_format(true,['token' => $user->api_token , 'id' => $user->id]);
    }
    /*
     * get a specific user
     */
    public function get_user($id){
        //get user by id
        $user = User::find($id);
        //if we found the user
        if($user){
            //return the whole user data
            return Helper::api_response_format(true,$user);
        }
        else{
            //return an error
            return Helper::api_response_format(false,'','user not found');
        }
    }
    /*
     * update a specific user
     */
    public function update_user(Request $request ,$id){
        //get user by id
        $user = User::find($id);
        // update the fields specified in the request
        return Helper::api_update($request,$user,'User not found');


    }
    
     public function setUserRole(Request $request){
        $user = User::whereApi_token($request->api_token)->first();
        if(isset($request->type)){
            switch ($request->type){
                case 'importer':
                    $user->importer = 1;
                    $user->exporter = 0;
                    $user->service_provider = 0;
                    break;
                case 'exporter':
                    $user->importer = 0;
                    $user->exporter = 1;
                    $user->service_provider = 0;
                    break;
                case 'service_provider':
                    $user->importer = 0;
                    $user->exporter = 0;
                    $user->service_provider = 1;
                    break;
                default:
                    return Helper::api_response_format(false , '' , 'type must be importer or exporter or service_provider');
            }
            if($user->save()){
                return Helper::api_response_format(true,'User role set successfully','');
            }
            return Helper::api_response_format(false , '' , 'something went wrong please try again');
        }
        return Helper::api_response_format(false , '' , 'type must be provided');
    }
}
