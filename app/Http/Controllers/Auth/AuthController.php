<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use App\Settings;
use Session;
use Mail;
use Hash;
use View;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'registration/registered';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);

        if(Session::has('yalla_locale')) {
            \App::setLocale(\Session::get('yalla_locale'));
        } else {
            \App::setLocale('ar');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:60|regex:/^[a-zA-Z ]+$/u',
            'last_name' => 'required|max:60|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|email|max:80|unique:users',
            'mobile'=>'required|numeric',
            'password' => 'required|min:6|confirmed',
        ],
        [
            'name.regex' => 'Name must be in english'
        ]
            );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if(isset($data['terms_checker'])) {
            return false;
        }

        $data['slug'] = \App\Helper::generate_slug('\App\User', $data['name']);

        $user = User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'slug' => $data['slug'], 
            'password' => bcrypt($data['password']),
            'verification_code' => str_random(50),
            'api_token' => str_random(60),// for security of the api
        ]);

        if(User::sendVerificationEmail($user)) {
            Session::flash('new_account', true);
            Session::flash('send_status', true);
        }

        return $user;
    }
}
