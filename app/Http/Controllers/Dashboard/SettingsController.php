<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Settings;
use Session;

class SettingsController extends InitController
{
    public function __construct() {
        $this->side_menu = \App\Menu::$settings_sidebar;
        parent::__construct();
    }

    public function getIndex() {
        $settings['notifications'] = Settings::section('general');
    	return view('dashboard.settings.account', compact('settings'));
    }

    public function postIndex(Request $request)
    {
        $result = Settings::save_settings($request->get('settings'));
        if( $result ) {
            Session::flash('message', 'General Settings saved successfully');
        } else {
            Session::flash('message', 'Error saving settings');
        }
        return redirect()->back();
    }

    public function getAccountVerification() {
        return view('dashboard.settings.verification');
    }

    public function postDeleteimage(Request $request) {
    	$record_id = $request->get('id');
    	$model = $request->get('model');
    	$field = $request->get('field');

    	$record = $model::findOrFail($record_id);
    
    	if(!$field || $field=='undefined') {
    		$field='image';
    	}
    	
        $sub_message = '';
        if($model::destroy($record_id)) {
            if(file_exists(public_path('uploads/'.$record->$field))) {
            	if(unlink(public_path('uploads/'.$record->$field))) {
                } else {
                    $sub_message .= ', File not exist, but record deleted';
                }
            }
        	return json_encode(['status'=>'success', 'msg'=>$sub_message]);
        }
    	return json_encode(['status'=>'error']);
    }

    public function getCategorySearch(Request $request)
    {
        $q = $request->get('q');
        if(empty($q)) {
            return '';
        }

        $results = Category::where('name_ar','like','%'.$q.'%')
        ->orWhere('name_en','like','%'.$q.'%')
        ->orWhere('description','like','%'.$q.'%')
        ->get();

        $output = '';

        foreach($results as $category)
        {
            $parents = Category::get_parents($category->id);
            $output .= Category::draw_cat_path($parents);
        }

        return $output;
    }
}
