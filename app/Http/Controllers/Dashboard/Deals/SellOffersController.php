<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Category;
use App\Country;
use App\Product;
use App\Deals\Deal;
use App\Deals\SellOffer;
use App\Deals\SellOfferLang;
use App\Deals\SellOfferImage;
use View;
use Auth;
use Session;

class SellOffersController extends InitController
{
    public function __construct() {
    	$this->side_menu = \App\Menu::$deals_sidebar;
    	parent::__construct();
    }

    public function index() {
    	return redirect('dashboard/deals');
    }

    public function create() {
    	$products = Product::whereUser_id(Auth::id())->get();
    	return view('dashboard.deals.sell_offers.create', compact('products'));
    }

    public function store(Request $request)
    {
        $request_data = $request->except('lang', 'add_images');
        $request_data['user_id'] = Auth::id();
        if(!empty($request_data['market_country'])){
            $request_data['market_country'] = implode(',', $request_data['market_country']);
        }
        if(!empty($request_data['payment_type'])){
            $request_data['payment_type'] = implode(',', $request_data['payment_type']);
        }
        if(!empty($request_data['brands'])){
            $request_data['brands'] = implode(',', $request_data['brands']);
        }
        $request_data['status'] = 1; //pending
        $request_data['category_id'] = Product::find($request_data['product_id'])->category_id;
        $offer = SellOffer::create($request_data);

        //Save Language Data
        foreach ($request->get('lang') as $lang_id=>$data) {
            $row = new SellOfferLang;
            $row->sell_offer_id = $offer->id;
            $row->language_id = $lang_id;
            $row->description = $data['description'];
            $row->shipping_terms = $data['shipping_terms'];
            $row->save();
        }

        //Upload & Save Additional Images
        $images = SellOfferImage::upload_multi_images($request->file('add_images'), $offer->id);

        //Save As Common Deal 
        $deal = Deal::create([
            'user_id'=>Auth::id(),
            'section'=>'sell_offer',
            'record_id'=>$offer->id,
            'type'=>$request_data['type'],
            'status'=>$offer->status,
            'expired_at'=>$request_data['expired_at']
        ]);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$offer]);
        }
        Session::flash('flash_message', 'Sell Offer added!');
        return redirect('dashboard/deals');
    }

    public function edit($id) {
        $products = Product::whereUser_id(Auth::id())->with('lang')->get();
        $sell_offer = SellOffer::whereId($id)->with('lang')->first();
        $sell_offer->market_country = explode(',', $sell_offer->market_country);
        $sell_offer->payment_type = explode(',', $sell_offer->payment_type);

        $lang=[];
        foreach($sell_offer->lang as $item) {
            $lang[$item->language_id] = $item;
        }
        $sell_offer->lang = $lang;
        
        return view('dashboard.deals.sell_offers.edit', compact('sell_offer', 'products'));
    }

    public function update($id, Request $request)
    {
        $sell_offer = SellOffer::whereId($id)->with('images', 'lang')->first();

        $request_data = $request->except('lang', 'add_images');
        $request_data['market_country'] = implode(',', $request_data['market_country']);
        $request_data['payment_type'] = implode(',', $request_data['payment_type']);
        $request_data['category_id'] = Product::find($request_data['product_id'])->category_id;
        $sell_offer->update($request_data);

        //Save Language Data
        foreach ($request->get('lang') as $lang_id=>$data) {
            $row = SellOfferLang::whereSell_offer_id($id)->whereLanguage_id($lang_id)->first();
            $row->description = $data['description'];
            $row->brands = $data['brands'];
            $row->shipping_terms = $data['shipping_terms'];
            $row->save();
        }

        //Upload & Save Additional Images
        $images = SellOfferImage::upload_multi_images($request->file('add_images'), $id);

        //Save As Common Deal 
        $deal = Deal::whereSection('sell_offer')->whereRecord_id($id)->first();
        if(!$deal) {
            $deal = new Deal;
            $deal->user_id=Auth::id();
            $deal->section='sell_offer';
            $deal->record_id=$id;
            $deal->status=$sell_offer->status;
        }
        $deal->type = $request_data['type'];
        $deal->expired_at = $request_data['expired_at'];
        $deal->save();

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$sell_offer]);
        }
        Session::flash('flash_message', 'Sell Offer Deal updated!');
        return redirect('dashboard/deals');
    }

    public function destroy($id)
    {
        $product = SellOffer::findOrFail($id);
        SellOffer::destroy($id);

        //Delete all other images
        $images = SellOfferImage::whereSell_offer_id($id)->get();
        foreach($images as $image) {
            unlink('uploads/'.$image->image);            
        }
        Session::flash('flash_message', 'Sell Offer deleted!');
        return redirect('dashboard/products');
    }

    public function trash(Request $request)
    {
        $product = SellOffer::findOrFail($request->get('id'));
        $product->restored_as = $product->approved; //If restored set its status as this
        $product->approved = 4; //4: trash
        if($product->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', 'Product trashed successfully');
        return redirect('dashboard/deals');
    }

    public function restore(Request $request)
    {
        $product = SellOffer::findOrFail($request->get('id'));
        $product->approved = $product->restored_as; //The status was before trash
        if($product->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', 'Product restored successfully');
        return redirect('dashboard/deals');
    }
}
