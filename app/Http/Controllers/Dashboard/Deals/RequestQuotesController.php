<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\UserWebsite;
use App\Template;
use App\TemplateSteps;
use App\Category;
use App\Country;
use App\Deals\Deal;
use App\Deals\QuoteRequest;
use App\Deals\QuoteRequestImage;
use View;
use Auth;
use Session;
use App;

class RequestQuotesController extends InitController
{
    public function __construct() {
    	$this->side_menu = \App\Menu::$deals_sidebar;
    	parent::__construct();

        $categories = Category::whereParent_category_id(0)->orderBy('name_ar', 'ASC')->get();
        View::share('categories', $categories);
    }

    public function index() {
    	return redirect('dashboard/deals');
    }

    public function create() {
    	return view('dashboard.deals.request_quotes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Deal::$rules);
        $request_data = $request->except('add_images');
        $request_data['user_id'] = Auth::id();

        if($request_data['supplier_location']) {
            $request_data['supplier_location'] = implode(',', $request_data['supplier_location']);
        }
        if($request_data['payment_terms']) {
            $request_data['payment_terms'] = implode(',', $request_data['payment_terms']);            
        }
        // $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        
        $request_data['status'] = 1; //pending
        $request_data['slug'] = str_slug($request_data['name_ar']);
        //$request_data['name_ar']= $request_data['name_ar'];
        //$request_data['details_ar']= $request_data['details_en'];
        $quote_request = QuoteRequest::create($request_data);

        if($quote_request)
        {
            //Upload & Save Additional Images
            $images = QuoteRequestImage::upload_multi_images($request->file('add_images'), $quote_request->id);

            //Save As Common Deal 
            $deal = Deal::create([
                'user_id'=>Auth::id(),
                'section'=>'quote_request',
                'record_id'=>$quote_request->id,
                'type'=>@$request_data['request_for'],
                'status'=>$quote_request->status,
                'expired_at'=>$request_data['expired_at']
            ]);

            //Fire creation event 
            event(new \App\Events\RfqCreated($quote_request));            
        }
        $come_from = $request->headers->get('referer');
        if (strpos($come_from, 'reg') !== false) {
            $steps = TemplateSteps::whereUser_id(Auth::id())->first();
            $steps->add_products=1;
            $steps->save();
             Session::flash('prod_done','prod_done');
             //choose template
             $template = Template::find(4);
            $user_website = UserWebsite::whereUser_id(Auth::id())->first();
            if(!$user_website) {
                $user_website = UserWebsite::init(Auth::user());
            }

            $user_website->template_id = 4;
            if($user_website->save()) {
                $steps = TemplateSteps::whereUser_id(Auth::id())->first();
                $steps->select_template=1;
                if($steps->save()) {
                    Session::flash('flash_message', 'تم اختيار القالب التصميمي بنجاح');
                }
            }
             //end choose template
             return "done";
        }
        else
        {
           
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$quote_request]);
        }
        Session::flash('flash_message', 'Request for Quote Deal added!');
        return redirect('dashboard/deals');
    }

    public function edit($id)
    {
        $quote_request = QuoteRequest::whereId($id)->with('images')->first();

        if(!$quote_request){
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'Record not found');
            return redirect()->back();
        }
        if($quote_request->supplier_location) {
            $quote_request->supplier_location = explode(',', $quote_request->supplier_location);
        }
        $quote_request->payment_terms = explode(',', $quote_request->payment_terms);

        /*$lang=[];
        foreach($quote_request->lang as $item) {
            $lang[$item->language_id] = $item;
        }
        $quote_request->lang = $lang;*/
        
        return view('dashboard.deals.request_quotes.edit', compact('quote_request'));
    }

    public function update($id, Request $request)
    {
        $quote_request = QuoteRequest::whereId($id)->with('images')->first();
        $request_data = $request->except('add_images');

        if($request_data['supplier_location']) {
            $request_data['supplier_location'] = implode(',', $request_data['supplier_location']);            
        }
        if($request_data['payment_terms']) {
            $request_data['payment_terms'] = implode(',', $request_data['payment_terms']);            
        }
        $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        $request_data['slug'] = str_slug($request_data['name_'.App::getLocale()]);

        $quote_request->update($request_data);

        //Upload & Save Additional Images
        $images = QuoteRequestImage::upload_multi_images($request->file('add_images'), $id);

        //Save As Common Deal 
        $deal = Deal::whereSection('quote_request')->whereRecord_id($id)->first();
        if(!$deal) {
            $deal = new Deal;
            $deal->user_id=Auth::id();
            $deal->section='quote_request';
            $deal->record_id=$id;
            $deal->status=$quote_request->status;
        }
        $deal->type = $request_data['request_for'];
        $deal->expired_at = $request_data['expired_at'];
        $deal->save();

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$quote_request]);
        }
        Session::flash('flash_message', 'Quote Request Deal updated!');
        return redirect('dashboard/deals');
    }

    public function destroy($id)
    {
        $product = QuoteRequest::findOrFail($id);
        QuoteRequest::destroy($id);

        //Delete all other images
        $images = QuoteRequestImage::whereQuote_request_id($id)->get();
        foreach($images as $image) {
            unlink('uploads/'.$image->image);            
        }
        Session::flash('flash_message', 'Request for Quote deleted!');
        return redirect('dashboard/deals');
    }

    public function trash(Request $request)
    {
        $quote_request = QuoteRequest::findOrFail($request->get('id'));
        $quote_request->restored_as = $quote_request->status; //If restored set its status as this
        $quote_request->status = 4; //4: trash
        if($quote_request->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$quote_request]);
        }
        Session::flash('flash_message', 'Product trashed successfully');
        return redirect('dashboard/quote_requests');
    }

    public function restore(Request $request)
    {
        $quote_request = QuoteRequest::findOrFail($request->get('id'));
        $quote_request->status = $quote_request->restored_as; //The status was before trash
        if($quote_request->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$quote_request]);
        }
        Session::flash('flash_message', 'Product restored successfully');
        return redirect('dashboard/deals');
    }
}
