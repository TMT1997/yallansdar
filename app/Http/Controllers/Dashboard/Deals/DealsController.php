<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Deals\Deal;
use App\Deals\SellOffer;
use App\Deals\QuoteRequest;
use App\Deals\Partnership;
use Auth;
use View;
use Session;

class DealsController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$deals_sidebar;
        parent::__construct();
    }

    public function index()
    {
        $deals = new Deal;
        $deals = $deals->whereUser_id(Auth::id());
        $deals = $deals->get();

        return view('dashboard.deals.index', compact('deals'));
    }

    public function grid(Request $request)
    {
        $status = $request->get('status');
        $deals = new Deal;
        $deals = $deals->whereUser_id(Auth::id());
        if ($status!=0) {
            $deals = $deals->whereStatus($status);
        }

        $deals = $deals->with('offer','request','partnership')->get();
        return view('dashboard.deals.grid', compact('deals'));
    }

    public function create()
    {
        return view('dashboard.groups.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        
        $result = Deal::create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Deal added!');
        return redirect('dashboard/deals');
    }

    public function show($id)
    {
        $deal = Deal::findOrFail($id);
        $view = 'sell_offers.show';
        if($deal->section=='quote_request') {
            $view = 'request_quotes.show';
            $deal = QuoteRequest::whereId($deal->record_id)->first();
        } else if($deal->section=='partnership') {
            $view = 'partnerships.show';
        }
        return view('dashboard.deals.'.$view, compact('deal'));
    }

    public function edit($id)
    {
        $deal = Deal::findOrFail($id);
        if($deal->section=='sell_offer') {
            return redirect()->route('dashboard.sell_offers.edit', $deal->record_id);
        } else if($deal->section=='quote_request') {
            return redirect()->route('dashboard.request_quotes.edit', $deal->record_id);
        } else if($deal->section=='partnership') {
            return redirect()->route('dashboard.partnerships.edit', $deal->record_id);
        }
        return view('dashboard.deals.index');
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();

        $deal = Deal::findOrFail($id);
        $deal->update($requestData);

        Session::flash('flash_message', 'Deal updated!');
        return redirect('dashboard/deals');
    }

    public function destroy($id)
    {
        $deal = Deal::findOrFail($id);

        $model = null;
        if($deal->section=='sell_offer') {
            SellOffer::destroy($deal->record_id);
        }
        else if($deal->section=='quote_request') {
            QuoteRequest::destroy($deal->record_id);
        }
        else if($deal->section=='partnership') {
            Partnership::destroy($deal->record_id);
        }

        Deal::destroy($id);
        Session::flash('flash_message', 'Deal deleted!');
        return redirect('dashboard/deals');
    }

    public function trash(Request $request)
    {
        $deal = Deal::findOrFail($request->get('id'));

        $model = null;
        if($deal->section=='sell_offer') {
            $model = SellOffer::whereId($deal->record_id)->first();
        }
        else if($deal->section=='quote_request') {
            $model = QuoteRequest::whereId($deal->record_id)->first();
        }
        else if($deal->section=='partnership') {
            $model = Partnership::whereId($deal->record_id)->first();
        }

        if($model) {
            $model->restored_as = $deal->status; //If restored set its status as this
            $model->save();
        }
        $deal->status = 5; //5: trash

        if($deal->save()) {
            Session::flash('flash_message', 'Deal trashed successfully');            
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$deal]);
        }
        return redirect('dashboard/deals');
    }

    public function restore(Request $request)
    {
        $deal = Deal::findOrFail($request->get('id'));

        $model = null;
        if($deal->section=='sell_offer') {
            $model = SellOffer::whereId($deal->record_id)->first();
        }
        else if($deal->section=='quote_request') {
            $model = QuoteRequest::whereId($deal->record_id)->first();
        }
        else if($deal->section=='partnership') {
            $model = Partnership::whereId($deal->record_id)->first();
        }

        if($model) {
            $deal->status = $model->restored_as;
        } else {
            $deal->status = 1;
        }

        if($deal->save()) {
            Session::flash('flash_message', 'Deal restored successfully');
        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$deal]);
        }
        return redirect('dashboard/deals');
    }
}
