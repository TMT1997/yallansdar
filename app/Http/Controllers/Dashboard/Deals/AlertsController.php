<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Category;
use App\Country;
use App\Deals\Deal;
use App\Deals\Alert;
use App\Deals\AlertLang;
use App\Deals\AlertImage;
use View;
use Auth;
use Session;

class AlertsController extends InitController
{
    public function __construct() {
    	$this->side_menu = \App\Menu::$deals_sidebar;
    	parent::__construct();
    }

    public function index() {
    	$alerts = Alert::whereUser_id(Auth::id())->get();
    	return view('dashboard.deals.alerts.index', compact('alerts'));
    }

    public function create() {
    	return view('dashboard.deals.alerts.create');
    }

    public function store(Request $request)
    {
        $request_data = $request->all();
        $request_data['user_id'] = Auth::id();
        $request_data['type'] = implode(',', $request_data['type']);
        $request_data['countries'] = implode(',', $request_data['countries']);
        $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);

        $request_data['status'] = 1; //pending
        $alert = Alert::create($request_data);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$alert]);
        }
        Session::flash('flash_message', 'Alert Deal added!');
        return redirect('dashboard/alerts');
    }

    public function edit($id) {
        $alert = Alert::whereId($id)->first();
        $alert->countries = explode(',', $alert->countries);
        
        return view('dashboard.deals.alerts.edit', compact('alert'));
    }

    public function update($id, Request $request)
    {
        $alert = Alert::whereId($id)->first();

        $request_data = $request->all();
        $request_data['type'] = implode(',', $request_data['type']);
        $request_data['countries'] = implode(',', $request_data['countries']);
        $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        $alert->update($request_data);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$alert]);
        }
        Session::flash('flash_message', 'Alert Deal updated!');
        return redirect('dashboard/alerts');
    }

    public function destroy($id)
    {
        $product = Alert::findOrFail($id);
        Alert::destroy($id);

        //Delete all other images
        $images = AlertImage::whereQuote_request_id($id)->get();
        foreach($images as $image) {
            unlink('uploads/'.$image->image);            
        }
        Session::flash('flash_message', 'Alert deleted!');
        return redirect('dashboard/alerts');
    }

    public function trash(Request $request)
    {
        $alert = Alert::findOrFail($request->get('id'));
        $alert->restored_as = $alert->status; //If restored set its status as this
        $alert->status = 4; //4: trash
        if($alert->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$alert]);
        }
        Session::flash('flash_message', 'Item trashed successfully');
        return redirect('dashboard/alerts');
    }

    public function restore(Request $request)
    {
        $alert = Alert::findOrFail($request->get('id'));
        $alert->status = $alert->restored_as; //The status was before trash
        if($alert->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$alert]);
        }
        Session::flash('flash_message', 'Item restored successfully');
        return redirect('dashboard/alerts');
    }
}
