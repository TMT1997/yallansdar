<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Deals\Quote;
use App\Deals\QuoteRequest;
use Auth;

class QuotesController extends InitController
{
    public function __construct() {
    	$this->side_menu = \App\Menu::$deals_sidebar;
    	parent::__construct();
    }

    public function getIndex()
    {
    	$quotations = Quote::whereUser_id(Auth::id())->with('request')->get();
    	return view('dashboard.deals.quotations.index', compact('quotations'));
    }
    public function get_sent_request()
    {
        $quotations = Quote::whereFrom_user_id(Auth::id())->with('request')->get();
        return view('dashboard.deals.quotations.index', compact('quotations'));
    }
    public function getShow($id)
    {
    	$quotation = Quote::whereId($id)->with('request')->first();
    	return view('dashboard.deals.quotations.show', compact('quotation'));
    }
    public function gride(Request $request)
    {
        $status = $request->get('status');
        $quotations = new Quote;
        $quote_req = new QuoteRequest;
        $quotations = $quotations->whereFrom_user_id(Auth::id())->get();
        if ($status!=3) {
            $quotations = Quote::whereFrom_user_id(Auth::id())->whereStatus($status)->get();
        }
         return view('dashboard.deals.quotations.grid', compact('quotations'));
        $quotations = $quote_req->with('lang')->orderBy('id','DESC')->get();
        return view('dashboard.deals.quotations.grid', compact('quotations'));
    }
}
