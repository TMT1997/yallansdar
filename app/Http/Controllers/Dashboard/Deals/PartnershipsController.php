<?php

namespace App\Http\Controllers\Dashboard\Deals;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Category;
use App\Country;
use App\Deals\Deal;
use App\Deals\Partnership;
use App\Deals\PartnershipLang;
use App\Deals\PartnershipImage;
use View;
use Auth;
use Session;

class PartnershipsController extends InitController
{
    public function __construct() {
    	$this->side_menu = \App\Menu::$deals_sidebar;
    	parent::__construct();
    }

    public function index() {
    	return redirect('dashboard/deals');
    }

    public function create() {
    	return view('dashboard.deals.partnerships.create');
    }

    public function store(Request $request)
    {
        $request_data = $request->except('lang', 'add_images');
        $request_data['user_id'] = Auth::id();
        if(!empty($request_data['countries'])) {
            $request_data['countries'] = implode(',', $request_data['countries']);
        }
        $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        
        $request_data['status'] = 1; //pending
        $partnership = Partnership::create($request_data);

        //Save Language Data
        foreach ($request->get('lang') as $lang_id=>$data) {
            $row = new PartnershipLang;
            $row->partnership_id = $partnership->id;
            $row->language_id = $lang_id;
            $row->name = $data['name'];
            $row->details = $data['details'];
            $row->expertise = $data['expertise'];
            $row->save();
        }

        //Upload & Save Additional Images
        $images = PartnershipImage::upload_multi_images($request->file('add_images'), $partnership->id);

        //Save As Common Deal 
        $deal = Deal::create([
            'user_id'=>Auth::id(),
            'section'=>'partnership',
            'record_id'=>$partnership->id,
            'type'=>$request_data['type'],
            'status'=>$partnership->status,
            'expired_at'=>$request_data['expired_at']
        ]);

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$partnership]);
        }
        Session::flash('flash_message', 'Partnership Deal added!');
        return redirect('dashboard/deals');
    }

    public function edit($id) {
        $partnership = Partnership::whereId($id)->with('lang', 'images')->first();
        $partnership->countries = explode(',', $partnership->countries);

        $lang=[];
        foreach($partnership->lang as $item) {
            $lang[$item->language_id] = $item;
        }
        $partnership->lang = $lang;
        
        return view('dashboard.deals.partnerships.edit', compact('partnership'));
    }

    public function update($id, Request $request)
    {
        $partnership = Partnership::whereId($id)->with('images', 'lang')->first();

        $request_data = $request->except('lang', 'add_images');
        if(!empty($request_data['countries'])) {
            $request_data['countries'] = implode(',', $request_data['countries']);
        }
        $request_data['category_id'] = $request_data['selected_category_id'];
        unset($request_data['selected_category_id']);
        $partnership->update($request_data);

        //Save Language Data
        foreach ($request->get('lang') as $lang_id=>$data) {
            $row = PartnershipLang::wherePartnership_id($id)->whereLanguage_id($lang_id)->first();
            $row->name = $data['name'];
            $row->details = $data['details'];
            $row->expertise = $data['expertise'];
            $row->save();
        }

        //Upload & Save Additional Images
        $images = PartnershipImage::upload_multi_images($request->file('add_images'), $id);

        //Save As Common Deal 
        $deal = Deal::whereSection('partnership')->whereRecord_id($id)->first();
        if(!$deal) {
            $deal = new Deal;
            $deal->user_id=Auth::id();
            $deal->section='partnership';
            $deal->record_id=$id;
            $deal->status=$partnership->status;
        }
        $deal->type = $request_data['type'];
        $deal->expired_at = $request_data['expired_at'];
        $deal->save();

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$partnership]);
        }
        Session::flash('flash_message', 'Partnership Deal updated!');
        return redirect('dashboard/deals');
    }

    public function destroy($id)
    {
        $product = Partnership::findOrFail($id);
        Partnership::destroy($id);

        //Delete all other images
        $images = PartnershipImage::whereQuote_request_id($id)->get();
        foreach($images as $image) {
            unlink('uploads/'.$image->image);            
        }
        Session::flash('flash_message', 'Partnership deleted!');
        return redirect('dashboard/deals');
    }

    public function trash(Request $request)
    {
        $partnership = Partnership::findOrFail($request->get('id'));
        $partnership->restored_as = $partnership->status; //If restored set its status as this
        $partnership->status = 4; //4: trash
        if($partnership->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$partnership]);
        }
        Session::flash('flash_message', 'Item trashed successfully');
        return redirect('dashboard/partnerships');
    }

    public function restore(Request $request)
    {
        $partnership = Partnership::findOrFail($request->get('id'));
        $partnership->status = $partnership->restored_as; //The status was before trash
        if($partnership->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$partnership]);
        }
        Session::flash('flash_message', 'Item restored successfully');
        return redirect('dashboard/deals');
    }
}
