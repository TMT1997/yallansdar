<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserProfile;
use App\UserTradeInfo;
use App\UserFactoryInfo;
use App\UserCompanyRegistration;
use App\UserProfileDoc;
use App\Product;
use App\TemplateSteps;
use App\Template;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Packages\UserPackage;
use App\UserWebsite;
use App\Helper;
use Auth;
use Hash;
use View;
use Session;

class WebsiteController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        $template_steps = TemplateSteps::whereUser_id(Auth::id())->first();
        if(!$template_steps) {
            $template_steps = TemplateSteps::init(Auth::user());
        }

        //Profile Step
        $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->with('docs','offices')->first();
        if(!$user_profile) {
            $user_profile = UserProfile::init(Auth::user());
        } else {
            $template_steps->personal_profile=1;
            $template_steps->company_profile=1;
            $template_steps->save();
        }

        foreach(UserProfile::$fields_arr as $field=>$action) {
            if(isset($user_profile->$field) && !empty($user_profile->$field)) {
                if($action=='serialize')
                    $user_profile->$field = unserialize($user_profile->$field);
                else
                    $user_profile->$field = explode(',', $user_profile->$field);
            }
        }

        //Select Template Step
        $templates = Template::whereActive(1)->get();
        $user_website = UserWebsite::whereUser_id(Auth::id())->first();
        if(!$user_website) {
            $user_website = UserWebsite::init(Auth::user());
        }

        //Add products Step
        $products = Product::whereUser_id(Auth::id())->with('lang', 'category')->get();
        if($products->count()>0) {
            $template_steps->add_products=1;
            $template_steps->save();
        }

        // TemplateSteps::is_completed($template_steps);
        $packages = Package::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $sections = Package::$sections;
        //==============================================================================
        //get data of factory data
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_factory_info) {
            $user_factory_info = UserFactoryInfo::init(Auth::user());
        }
        foreach(UserFactoryInfo::$array_fields as $field) {
            if(isset($user_factory_info->$field) && !empty($user_factory_info->$field)) {
                $user_factory_info->$field = explode(',', $user_factory_info->$field);
            }
        }
        //end get data of factory
        //get data of trade
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_trade_info) {
            $user_trade_info = UserTradeInfo::init(Auth::user());
        }
        foreach(UserTradeInfo::$array_fields as $field) {
            if(!empty($user_trade_info->$field)) {
                if($field == 'nearest_port') {
                    $user_trade_info->$field = unserialize($user_trade_info->$field);
                } else {
                    $user_trade_info->$field = explode(',', $user_trade_info->$field);
                }
            }
        }
        //end get data of trade
        //==============================================================================
        $confirmed_user_package = (Auth::check()) ? UserPackage::whereUser_id(Auth::id())->whereConfirmed(1)->first() : null;
    	return view('dashboard.website.index', compact('user_profile', 'template_steps', 'templates', 'user_website', 'products','packages','items','sections','confirmed_user_package','user_factory_info','user_trade_info'));
    }

    public function getChecklist($type='website')
    {
        $steps = TemplateSteps::whereUser_id(Auth::id())->first();
    	return view('dashboard.website.checklist', compact('steps', 'type'));
    }

    public function getTemplateSelect($template_id)
    {
        $template = Template::find($template_id);
        $user_website = UserWebsite::whereUser_id(Auth::id())->first();
        if(!$user_website) {
            $user_website = UserWebsite::init(Auth::user());
        }

        $user_website->template_id = $template->id;
        if($user_website->save()) {
            $steps = TemplateSteps::whereUser_id(Auth::id())->first();
            $steps->select_template=1;
            if($steps->save()) {
                Session::flash('flash_message', 'تم اختيار القالب التصميمي بنجاح');
            }
        }
        Session::flash('done_template','done_template');
        return redirect()->back();
    }

    public function postLaunch(Request $request)
    {
        // $this->validate($request, [
        //     'domain'=>'required|alpha_dash|string'
        // ]);
        $domain = str_slug($request->get('domain'));

        $check = UserWebsite::whereDomain($domain)->where('user_id', '<>', Auth::id())->count();
        if($check>0) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'عفوا اسم المتجر الذي ادخلته موجود بالفعل, يرجى إدخال اسم اخر');
            return redirect()->back();            
        }
        $user_website = UserWebsite::whereUser_id(Auth::id())->first();
        if(!$user_website) {
            return redirect()->back();
        }
        $user_website->domain = $domain;
        $user_website->status = 1;
        if($user_website->save()) {
            UserWebsite::setDefaultData($user_website);
            Session::flash('flash_message', 'لقد تم إرسال طلب نشر متجرك بنجاح إلى إدارة الموقع');
        }
        $steps = TemplateSteps::whereUser_id(Auth::id())->first();
        $steps->completed=1;
        $steps->save();
        //=================================select free package
        $check_package = UserPackage::whereUser_id(Auth::id())->first();
        if(!$check_package)
        {
            $data['package_id'] = 4;
            $data['user_id'] = Auth::id();
            $data['amount'] = 0;
            $data['currency'] = '';
            $data['payment_method'] = 'bank_transfer';
            $data['confirmed']= 1;

            $user_package = UserPackage::create($data);
        }
        //=================================
        Session::flash('done_publish','done_publish');
        //=====================================
        $user = User::whereid(auth::user()->id)->first();
        if(User::sendwelcomeEmail($user));
        //=====================================
        // return redirect()->back();
        return "done";
    }

    public function postProfile(Request $request)
    {
        // $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->first();
        // // if($user_profile->editable==0) {
        // //     Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
        // //     Session::flash('status', 'danger');
        // //     Session::flash('back','back');
        // //     return redirect()->back();
        // // }
        // $data = $request->all();
        // unset($data['btn_save']);
        // unset($data['btn_submit']);

        // //Save User Type
        // $user = Auth::user();
        // foreach(\App\User::$types as $type=>$label) {
        //     if(isset($data['user_type']) && in_array($type, $data['user_type'])) {
        //         $user->$type = 1;
        //     } else {
        //         $user->$type = 0;
        //     }
        // }
        // $user->save();

        // unset($data['files']);
        // foreach(UserProfile::$fields_arr as $field=>$action) {
        //     if(isset($data[$field])) {
        //         if($action=='serialize')
        //             $data[$field] = serialize($data[$field]);
        //         else
        //             $data[$field] = implode(',', $data[$field]);
        //     }
        // }
        // foreach(UserProfile::$fields_upload as $field=>$type) {
        //     if(!$request->hasFile($field)) {
        //         continue;
        //     }
        //     $thumb=true;
        //     if($type=='document') {
        //         $thumb=false;
        //     }
        //     $data[$field] = Helper::upload($request, $field, $thumb);
        // }

        // $msg = trans('dash.profile_saved');
        // if($request->has('btn_save')) {
        //     $data['status'] = -1; //just a save
        // }
        // if($request->has('btn_submit')) {
        //     $data['status'] = 0; //pending
        //     $data['editable'] = 0; //nOT allowed to edit after submission, till approved
        //     $msg = trans('dash.profile_submitted');
        // }
        // if($user_profile->update($data)) {
        //     //Update Template Steps
        //     TemplateSteps::update_step(['personal_profile', 'company_profile']);

        //     Session::flash('flash_message', $msg);
        //     Session::flash('back','back');
        //     return redirect()->back();
        // }

        // Session::flash('flash_message', 'An error occured, please try again');
        // Session::flash('alert', 'danger');
        // return redirect()->back();
        $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->with('offices')->first();
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if($user_profile->editable==0) {
            Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
            Session::flash('status', 'danger');
            Session::flash('back','back');
            return redirect()->back();
        }
        $data = $request->except(['annual_output_value','contract_manufacturings','export_percentage']);
        unset($data['btn_save']);
        unset($data['btn_submit']);

        //Save User Type
        $user = Auth::user();
        foreach(\App\User::$types as $type=>$label) {
            if(isset($data['user_type']) && in_array($type, $data['user_type'])) {
                $user->$type = 1;
            } else {
                $user->$type = 0;
            }
        }
        $user->save();
        unset($data['files']);
        foreach(UserProfile::$fields_arr as $field=>$action) {
            if(isset($data[$field])) {
                if($action=='serialize')
                    $data[$field] = serialize($data[$field]);
                elseif($field == "other_products")
                    $data[$field] = $data[$field];
                else
                    $data[$field] = implode(',', $data[$field]);
            }
        }

        foreach(UserProfile::$fields_upload as $field=>$type) {
            if(!$request->hasFile($field)) {
                continue;
            }
            $thumb=true;
            if($type=='document') {
                $thumb=false;
            }
            $data[$field] = Helper::upload($request, $field, $thumb);
        }

        //Divisional Offices
        $dataOffices = $data['divoffice'];
        $officesArr = [];
        foreach($dataOffices['division'] as $index=>$div)
        {
            if($index==0) { continue; }
            foreach(UserOffice::$fields_arr as $field=>$action) {
                if(isset($dataOffices[$field][$index])) {
                    $officesArr[$index][$field] = $dataOffices[$field][$index];
                }
            }
        }
        foreach($officesArr as $index=>$dataRow) {
            $dataRow['user_id'] = auth()->id();
            $dataRow['user_profile_id'] = $user_profile->id;
            if(isset($dataRow['id']) && !empty($dataRow['id'])) {
                $office_row = UserOffice::whereId($dataRow['id'])->first();
                if($office_row) {
                    $office_row->update($dataRow);
                }
            } else {
                UserOffice::create($dataRow);
            }
        }
        unset($data['divoffice']);

        $msg = trans('dash.profile_saved');
        if($request->has('btn_save')) {
            $data['status'] = 0; //pending
        }
        if($request->has('btn_submit')) {
            $data['status'] = 0; //pending
            $data['editable'] = 0; //nOT allowed to edit after submission, till approved
            $msg = trans('dash.profile_submitted');
        }
        //factory info data 
        $user_factory_info->annual_output_value = $request->input('annual_output_value');
        if($request->input('contract_manufacturings'))
        {
            $contract_manufacturings =  $tags = implode(',', $request->input('contract_manufacturings'));
            $user_factory_info->contract_manufacturings = $contract_manufacturings;
        }
        $user_factory_info->save();
        //end factory info data
        //Trade Info data
        $user_trade_info->export_percentage = $request->input('export_percentage');
        $user_trade_info->save();
        //end Trade Info data
        if($user_profile->update($data)) {
            //Update Template Steps
            \App\TemplateSteps::update_step(['personal_profile', 'company_profile' ]);

            Session::flash('flash_message', $msg);
            Session::flash('back','back');
            // return redirect()->back();
            return "done_prof";
        }
        Session::flash('flash_message', 'An error occured, please try again');
        Session::flash('alert', 'danger');
        // return redirect()->back();
        return "done_prof";
    }
}
