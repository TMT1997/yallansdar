<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use App\Category;
use App\ProductGroup;
use App\UserWebsite;
use App\Template;
use App\TemplateSteps;
use App\ProductImage;
use App\ServiceLang;
use App\Country;
use App\Helper;
use App\Media;
use Auth;
use View;
use Session;

class ServicesController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$services_sidebar;
        parent::__construct();

        $categories = Category::whereParent_category_id(0)->orderBy('name_ar', 'ASC')->get();
        $groups = ProductGroup::whereActive(1)->orderBy('name', 'ASC')->lists('name', 'id');

        View::share('categories', $categories);
        View::share('groups', $groups);
    }

    public function index()
    {
        $services = Service::whereUser_id(Auth::id())->get();
        return view('dashboard.services.index', compact('services'));
    }

    public function grid(Request $request)
    {
        $status = $request->get('status');
        $services = new Service;
        $services = $services->whereUser_id(Auth::id());
        if ($status!=0) {
            $services = $services->whereStatus($status);
        }

        $services = $services->with('lang')->get();
        return view('dashboard.services.grid', compact('services'));
    }

    public function create()
    {
        return view('dashboard.services.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Service::$rules);
        $data = $request->all();

        $data['user_id'] = Auth::id();
        $data['image'] = Helper::upload($request, 'image');
        $data['document'] = Helper::upload($request, 'document', false);
        // $data['category_id'] = $request->get('selected_category_id');
        $data['name'] = $request->get('name');
        //$data['english_name'] = $request->get('english_name');
        $data['locations'] = implode(',', $request->get('locations'));
        $data['price_min'] = $request->get('price_min');
        $data['price_max'] = $request->get('price_max');
        $data['expired_at'] = $request->get('expired_at');
        $langData = $request->get('lang');

        unset($data['lang']);
        unset($data['selected_category_id']);
        $service = Service::create($data);

        if($service)
        {
            //Save Service Language
            foreach ($langData as $lang_id=>$lndata) {
                $row = new ServiceLang;
                $row->service_id = $service->id;
                $row->language_id = $lang_id;
                $row->name = $data['name'];
                $row->english_name = $data['english_name'];
                $row->description = $lndata['description'];
                $row->details = $lndata['details'];
                $row->save();
            }
        }
        $come_from = $request->headers->get('referer');
        if (strpos($come_from, 'reg') !== false) {
            $steps = TemplateSteps::whereUser_id(Auth::id())->first();
            $steps->add_products=1;
            $steps->save();
             Session::flash('prod_done','prod_done');
             //choose template
             $template = Template::find(4);
            $user_website = UserWebsite::whereUser_id(Auth::id())->first();
            if(!$user_website) {
                $user_website = UserWebsite::init(Auth::user());
            }

            $user_website->template_id = 4;
            if($user_website->save()) {
                $steps = TemplateSteps::whereUser_id(Auth::id())->first();
                $steps->select_template=1;
                if($steps->save()) {
                    Session::flash('flash_message', 'تم اختيار القالب التصميمي بنجاح');
                }
            }
             //end choose template
             return "done";
        }
        else
        {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$service]);
        }

        Session::flash('flash_message', 'Service added!');
        return redirect('dashboard/services');
    }

    public function show($id)
    {
        $service = Service::findOrFail($id);
        return view('dashboard.services.show', compact('product'));
    }

    public function edit($id)
    {
        $service = Service::whereId($id)->with('lang')->first();
        $category_tree = Category::get_parents($service->category_id);
        $category_tree = array_reverse($category_tree);
        $service->locations = explode(',', $service->locations);

        $lang=[];
        foreach($service->lang as $item) {
            $lang[$item->language_id] = $item;
        }
        $service->lang = $lang;

        return view('dashboard.services.edit', compact('service', 'category_tree'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Service::$rules);
        $service = Service::whereId($id)->with('lang')->first();

        if($request->hasFile('image')) {
            $data['image'] = Helper::upload($request, 'image');
        }
        if($request->hasFile('document')) {
            $data['document'] = Helper::upload($request, 'document', false);
        }
        $data['category_id'] = $request->get('selected_category_id');
        $data['name'] = $request->get('name');
        $data['discount'] = $request->get('discount');
        $data['expired_at'] = $request->get('expired_at');
        $data['locations'] = implode(',', $request->get('locations'));
        $langData = $request->get('lang');

        unset($data['lang']);
        unset($data['selected_category_id']);
        $service = $service->update($data);

        if($service)
        {
            foreach ($langData as $lang_id=>$lnData) {
                $row = ServiceLang::whereService_id($id)->whereLanguage_id($lang_id)->first();
                if($row) {
                    $row->save();
                } else {
                    $row = new ServiceLang;
                    $row->service_id = $id;
                    $row->language_id = $lang_id;
                }
                $row->description = $lnData['description'];
                $row->details = $lnData['details'];
                $row->save();
            }
        }

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$service]);
        }
        Session::flash('flash_message', 'Service updated!');
        return redirect('dashboard/services');
    }

    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        Service::destroy($id);

        //Delete Service image
        if(file_exists(public_path().'/uploads/'.$service->image)) {
            unlink('uploads/'.$service->image);
        }

        Session::flash('flash_message', 'Service deleted!');
        return redirect('dashboard/services');
    }

    public function trash(Request $request)
    {
        $service = Service::findOrFail($request->get('id'));
        $service->restored_as = $service->approved; //If restored set its status as this
        $service->approved = 4; //4: trash
        if($service->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$service]);
        }
        Session::flash('flash_message', 'Service trashed successfully');
        return redirect('dashboard/services');
    }

    public function restore(Request $request)
    {
        $service = Service::findOrFail($request->get('id'));
        $service->approved = $service->restored_as; //The status was before trash
        if($service->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$service]);
        }
        Session::flash('flash_message', 'Service restored successfully');
        return redirect('dashboard/services');
    }

    public function get_sub($category_id)
    {
        $parent_cat_id = $category_id;
        $childs = Category::whereParent_category_id($category_id)->get();
        $output = '';
        foreach($childs as $child) {
            $output .='<option value="'.$child->id.'">'.$child->name_en.'</option>';
        }
        return $output;
    }
}
