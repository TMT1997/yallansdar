<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helper;
use App\ProductImage;

class UploadController extends InitController
{
    public function __construct() {
        parent::__construct();
    }

    public function getIndex(){
    	return 'Nothing here';
    }

    public function postIndex(Request $request)
    {
    	$output = [];
        $upload_path = Helper::upload_path();
        $upload_url = Helper::upload_path(true);
        $name_sub = date('Y/m');
        $set_thumbnail = false;
        $files_request = $request->file('files');

        foreach($files_request as $file)
        {
        	//check file upload
            if(empty($file)) {
                continue;
            }

            //Check file size
            if($file->getClientSize() > ProductImage::$max_upload_doc_size) {
                continue;
            }

            //Check if file extension if allowed
            $extension = $file->getClientOriginalExtension();
            if(!in_array($extension, ProductImage::$allowed_docs_types)) {
                continue;
            }

            $file_code = rand(1000000000000000000, 999999999999999999999999);
            $fileName = $file_code.'.'.$extension;
            $file->move($upload_path, $fileName);

            $fullFileName = $name_sub.'/'.$fileName;

            if($set_thumbnail)
            {
	            if(Helper::setThumbnail(['name'=>$file_code, 'extension'=>$extension], $upload_path)) {
	                $uploaded_file = $name_sub.'/'.$fileName;
	            }
            }

            //Save for output
            $output['files'][] = array('name'=>$fileName, 'full_name'=>$fullFileName, 'url'=>$upload_url.'/'.$fileName); 
        }

        return json_encode($output);
    }

    public function postServe(Request $request)
    {
    	$output = [];
    	$actionStr = $request->get('action');
    	$fileName = $request->get('file');

    	$actionArr = explode('|', $actionStr);
    	if($actionArr[0]=='UserProfileDoc')
    	{
            $section = $actionArr[1];
            $user_profile_id = $actionArr[2];
    		$profile = $actionArr[3];

    		\App\UserProfileDoc::create([
                'profile'=>$profile,
                'user_profile_id'=>$user_profile_id,
                'section'=>$section,
                'filename'=>$fileName,
    		]);

    		$output['messages'][] = ['file'=>$fileName, 'status'=>'success'];
    	}

    	return json_encode($output);
    }
}
