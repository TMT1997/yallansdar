<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helper;
use Carbon\Carbon;
use Session;
use Image;
use File;
use View;
use Auth;
use App\UserWebsitePage;
use App\UserWebsite;

class WebsitePagesController extends InitController
{
    public function __construct() {
        parent::__construct();
        $user_website = UserWebsite::whereUser_id(Auth::id())->first();
        View::share('user_website', $user_website);
    }

    public function index() {
        $pages = new UserWebsitePage;
        $pages = $pages->whereUser_id(Auth::id())->get();
        return view('dashboard.website.pages.index', compact('pages'));
    }

    public function create() {
        return view('dashboard.website.pages.create');
    }

    public function store(Request $request) {
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();

        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        $model = UserWebsitePage::create($requestData);

        Session::flash('flash_message', 'تم اضافة الصفحة بنجاح');
        return redirect('dashboard/websitepages');
    }

    public function show($id) {
        $page = UserWebsitePage::findOrFail($id);
        return view('dashboard.website.pages.show', compact('page'));
    }

    public function edit($id) {
        $page = UserWebsitePage::findOrFail($id);
        return view('dashboard.website.pages.edit', compact('page'));
    }

    public function update($id, Request $request) {        
        $requestData = $request->all();
        if($request->hasFile('image')) {
            $requestData['image'] = Helper::upload($request, 'image');
        }
        $page = UserWebsitePage::findOrFail($id);
        $page->update($requestData);

        Session::flash('flash_message', 'تم التعديل بنجاح');
        return redirect('dashboard/websitepages');
    }

    public function destroy($id) {
        UserWebsitePage::destroy($id);
        Session::flash('flash_message', 'تم حذف الصفحة بنجاح');
        return redirect('dashboard/websitepages');
    }
}