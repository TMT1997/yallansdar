<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;
use Auth;

class ReviewsController extends InitController
{
    public function __construct()
    {
    	$this->side_menu = \App\Menu::$settings_sidebar;
        parent::__construct();
    }

    public function getIndex()
    {
    	return view('dashboard.reviews.index');
    }

    public function grid(Request $request)
    {
        $reviews = new Review;
        $type = $request->get('type');
        $direction = $request->get('direction');
        
        if($direction=='Received') {
        	$reviews = $reviews->whereUser_id(Auth::id());
        }
        if($direction=='Given') {
        	$reviews = $reviews->whereFrom_user_id(Auth::id());        	
        }
        if(!empty($type)) {
            $reviews = $reviews->whereType($type);
        }
        $reviews = $reviews->with('product')->get();

        return view('dashboard.reviews.grid', compact('reviews', 'type'));
    }

    public function getShow($id)
    {
        $review = Review::whereUser_id(Auth::id())->whereId($id)->with('product')->first();
        return view('dashboard.reviews.show', compact('review'));
    }
}
