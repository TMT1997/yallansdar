<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Media;
use App\MediaAlbum;
use Auth;
use View;
use Session;

class MediaAlbumsController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$products_sidebar;
        parent::__construct();
    }

    public function index()
    {
        $albums = MediaAlbum::whereUser_id(Auth::id())->get();
        return view('dashboard.albums.index', compact('albums'));
    }

    public function create()
    {
        return view('dashboard.albums.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        
        $result = MediaAlbum::create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Album added!');
        return redirect('dashboard/albums');
    }

    public function show($id)
    {
        $album = MediaAlbum::findOrFail($id);
        return view('dashboard.albums.show', compact('album'));
    }

    public function edit($id)
    {
        $album = MediaAlbum::findOrFail($id);
        return view('dashboard.albums.edit', compact('album'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();

        $album = MediaAlbum::findOrFail($id);
        $album->update($requestData);

        Session::flash('flash_message', 'Album updated!');
        return redirect('dashboard/albums');
    }

    public function destroy($id)
    {
        MediaAlbum::destroy($id);
        Session::flash('flash_message', 'Album deleted!');
        return redirect('dashboard/albums');
    }
}
