<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TemplateSteps;
use App\UserProfile;
use App\User;
use App\UserWebsite;
use App\Packages\UserPackage;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Product;
use App\Deals\QuoteRequest;
use App\Deals\Quote;
use App\Messages\Message;
use App\Advs;
use App\quote_request_seen;
use App\Country;
use App\Blog;
use Session;
use App\BlogCategory;



use Auth;
class DashboardController extends InitController
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function getIndex()
    {
        $constant_packages_products = array('Yalla Start' => 25 ,'Yalla Free' => 10,'Yalla Go' => 50,'Yalla VIP' => 100);
        $constant_packages_price_offer = array('Yalla Start' => 20 ,'Yalla Free' => 0,'Yalla Go' => 50,'Yalla VIP' => 1000000);
        $package_no_product  = 1;
        $package_price_offer = 1;

        $dash_adv = Advs::whereActive(1)->get();
        $products = Product::whereUser_id(\Auth::id())->get();
        $all_admins = User::whereAdmin(1)->get();
        $user_profile = UserProfile::whereUser_id(\Auth::id())->first();
        $user = User::whereId(\Auth::id())->first();
        $user_package = UserPackage::whereUser_id(\Auth::id())->orderBy('id', 'desc')
        ->first();
        $quote = Quote::whereFrom_user_id(\Auth::id())->get();
        $template_steps = TemplateSteps::whereUser_id(\Auth::id())->first();
        $quoterequest   = QuoteRequest::whereParent_cat_id($user_profile->other_products)->where('expired_at',">=",date('Y-m-d'))->count();
        if($quoterequest==0)$quoterequest=1;
        $items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $quote_request_seen = quote_request_seen::whereUser_id(\Auth::id())->get();
        $sections = Package::$sections;
        $my_website = UserWebsite::whereUser_id(\Auth::id())->first();
        $blogs_cat = BlogCategory::all();

        $blogs = Blog::whereActive(1)->orderBy('id' , 'desc')->take(3)->get();
        if($user->first_time == 0)
        {
             Session::flash('flash_message', "تم تسجيلك بنجاح وفي انتظار موافقة الادارة علي عضويتك");
            $user->first_time=1;
            $user->save();
        }
        if($user_package)
        { 
            $packages = Package::where('id',"=",$user_package->package_id)->first();
            $package_no_product = $constant_packages_products[$packages->name_en];
            $package_price_offer = $constant_packages_price_offer[$packages->name_en];
            if($products->count() > 0)
            {
                $date_package =$user_package->created_at;
                $products = Product::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
                // $quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
                $quote = Quote::whereFrom_user_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
            }//end has products
        }//end user_package
        else
            $user_package = 0;
    	return view('dashboard.index', compact('template_steps','user_profile','user_package','packages','user','all_admins','products','quoterequest','quote','items','sections','dash_adv','quote_request_seen','package_no_product','package_price_offer','blogs','blogs_cat','my_website'));
    }

    public function getWelcome()
    {
    	// return view('dashboard.steps.welcome');
        return redirect('dashboard');
    }

     public function getSendMessage()
    {
        $all_admins = User::whereAdmin(1)->get();
        $user = User::whereId(\Auth::id())->first();
        foreach ($all_admins as $index=>$admin) 
        {
            if($index==0)
            Message::notify($admin->id,'من فضلك كلمني ','اريد التواصل مع الادمن وهذا رقمي   '.$user->mobile);
        }
    }
     public  function country_code(Request $request )
    {
        $country = $request->get('country');

       $call_code = Country::whereId($country)->first();

        return $call_code;
    }
}
