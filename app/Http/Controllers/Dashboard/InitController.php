<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Auth;
use View;
use Session;
use App\Language;
use App\Country;
use App\Messages\Message;

class InitController extends Controller
{
	public $side_menu;
    public $current_lang_id;
    public $c_name;
    
    public function __construct()
    {
    	if(Session::has('yalla_locale')) {
    		App::setLocale(Session::get('yalla_locale'));
    	}
    	$languages = Language::whereActive(1)->orderBy('id', 'DESC')->get();
        $current_language = $languages->filter(function($lang){ return $lang->iso2==\App::getLocale(); })->first();
        $this->current_lang_id = $current_language->id;
        \Carbon\Carbon::setLocale($current_language->iso2);

        //Get unread messages
        $unread_messages = Message::whereTo_user_id(\Auth::id())->whereRead(0)->whereDraft(0)->whereStatus(1)->whereTrashed(0)->whereSpammed(0)->orderBy('id','DESC')->get();

        $c_name = (App::getLocale()=='ar')?'name_ar':'name_en';
        $this->c_name = $c_name;
        $selected_country = 66;

        $countries = new Country;
        $countries = $countries->whereActive(1);
        if(App::getLocale()=='en') {
            $countries = $countries->orderBy('name_english', 'ASC')->select('id','name_english AS name')->lists('name', 'id');
        } else {
            $countries = $countries->orderBy('country_name', 'ASC')->select('id','country_name AS name')->lists('name', 'id');
        }

        $steps = \App\TemplateSteps::whereUser_id(Auth::id())->first();
        $langs = Language::$langs;

        //Get product rejection messages
        $reject_reasons = \App\UserStatusReason::whereUser_id(Auth::id())->whereRead(0)->orderBy('created_at','DESC')->with('product', 'reason')->get();

		View::share('side_menu', $this->side_menu);
		View::share('languages', $languages);
        View::share('current_language', $current_language);
        View::share('lng_id', $current_language->id);
        View::share('unread_messages', $unread_messages);
        View::share('c_name', $c_name);
        View::share('countries', $countries);
        View::share('selected_country', $selected_country);
        View::share('steps', $steps);
        View::share('langs', $langs);
        View::share('reject_reasons', $reject_reasons);
    }
}
