<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ActivitiesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
    	return view('dashboard.activities.log');
    }
}