<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Favorite;
use Auth;

class FavoritesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        $favorites = Favorite::whereUser_id(Auth::id())->get();
        return view('dashboard.favorites.index', compact('favorites'));
    }

    public function getGrid(Request $request)
    {
        $type = $request->get('type');
        $favorites = new Favorite;
        $favorites = $favorites->whereUser_id(Auth::id());
        if ($type!=0 || !empty($type)) {
            $favorites = $favorites->whereType($type);
        } else {
        	$favorites = $favorites->whereType('Products');
        }

        $favorites = $favorites->with('product', 'category', 'service')->get();
        return view('dashboard.favorites.grid', compact('favorites'));
    }
}
