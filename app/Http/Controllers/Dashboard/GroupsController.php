<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductGroup;
use App\Product;
use Auth;
use View;
use Session;

class GroupsController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$products_sidebar;
        parent::__construct();

        $parents = ProductGroup::whereActive(1)->whereUser_id(Auth::id())->whereParent_group_id(0)->orderBy('name', 'ASC')->lists('name', 'id');
        View::share('parents', $parents);
    }

    public function index(Request $req)
    {
        $groups = new ProductGroup;
        if($req->has('parent')) {
            $groups = $groups->whereParent_group_id($req->get('parent'));
        }
        $groups = $groups->whereUser_id(Auth::id())->with('products')->get();
        return view('dashboard.groups.index', compact('groups'));
    }

    public function create()
    {
        return view('dashboard.groups.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        
        $result = ProductGroup::create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Group added!');
        return redirect('dashboard/groups');
    }

    public function show($id)
    {
        $group = ProductGroup::findOrFail($id);
        return view('dashboard.groups.show', compact('group'));
    }

    public function edit($id)
    {
        $group = ProductGroup::findOrFail($id);
        return view('dashboard.groups.edit', compact('group'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();

        $group = ProductGroup::findOrFail($id);
        $group->update($requestData);

        Session::flash('flash_message', 'Product Group updated!');
        return redirect('dashboard/groups');
    }

    public function destroy($id)
    {
        ProductGroup::destroy($id);
        Session::flash('flash_message', 'Product Group deleted!');
        return redirect('dashboard/groups');
    }

    public function get_sub($category_id)
    {
        $parent_cat_id = $category_id;
        $childs = Category::whereParent_category_id($category_id)->get();
        $output = '';
        foreach($childs as $child) {
            $output .='<option value="'.$child->id.'">'.$child->name_en.'</option>';
        }
        return $output;
    }
}
