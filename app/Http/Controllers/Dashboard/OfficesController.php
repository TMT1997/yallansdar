<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserOffice;

class OfficesController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $req, $id)
    {
        UserOffice::destory($id);
        if($req->ajax()) {
        	return json_encode(['status'=>'success']);
        }
        return redirect('dashboard');
    }
}
