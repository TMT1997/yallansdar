<?php
namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helper;
use Auth;
use Session;

class QuickAddController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getGroup() {
        return view('dashboard.quickadd.group');
    }

    public function getAlbum() {
    	return view('dashboard.quickadd.album');
    }

    public function getProduct() {
        return view('dashboard.quickadd.product');
    }

    public function getContact() {
        return view('dashboard.quickadd.contact');
    }

    public function postProduct(Request $request)
    {
        $product_data = $request->except('lang');
        $product_data['user_id'] = Auth::id();
        $product_data['image'] = Helper::upload($request, 'image');
        $product_data['category_id'] = $request->get('selected_category_id');
        $product_data['name_en'] = $request->get('lang')[1]['name'];
        $product_data['name_ar'] = $request->get('lang')[2]['name'];
        $ref = $request->get('ref');
    
        unset($product_data['selected_category_id']);
        unset($product_data['ref']);
        unset($product_data['addToProds']);

        $product = \App\Product::create($product_data);
        if($product)
        {
            //Save uploaded image to media
            $media = \App\Media::create([
                'user_id'=>Auth::id(), 
                'type'=>'images',
                'media_album_id'=>1,
                'file'=>$product_data['image'],
                'name'=>'no_name'
            ]);

            //Save Product Language
            foreach ($request->get('lang') as $lang_id=>$data) {
                $row = new \App\ProductLang;
                $row->product_id = $product->id;
                $row->language_id = $lang_id;
                $row->name = $data['name'];
                $row->description = $data['description'];
                $row->save();
            }
        }

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', 'Product <a href="'.route('dashboard.products.edit',$product->id).'">'.$product_data['name_ar'].'</a> added');
        Session::flash('prod_done','prod_done');
        if(isset($ref) && $ref=='sell_offer'){
            return redirect('dashboard/sell_offers/create?pid='.$product->id);
        }
        return redirect()->back();
    }
}