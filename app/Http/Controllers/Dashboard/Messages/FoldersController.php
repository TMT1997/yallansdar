<?php

namespace App\Http\Controllers\Dashboard\Messages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Messages\Folder;
use Auth;
use View;
use Session;

class FoldersController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$messages_sidebar;
        parent::__construct();
        $groups = \App\ProductGroup::whereUser_id(Auth::id())->get();
        View::share('groups', $groups);
    }

    public function index()
    {
        $folders = Folder::whereUser_id(Auth::id())->get();
        return view('dashboard.folders.index', compact('folders'));
    }

    public function create()
    {
        return view('dashboard.folders.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Folder::$validation);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        
        $result = Folder::create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Folder added!');
        return redirect('dashboard/folders');
    }

    public function show($id)
    {
        $folder = Folder::findOrFail($id);
        return view('dashboard.folders.show', compact('folder'));
    }

    public function edit($id)
    {
        $folder = Folder::findOrFail($id);
        return view('dashboard.folders.edit', compact('folder'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Folder::$validation);
        $requestData = $request->all();

        $folder = Folder::findOrFail($id);
        $folder->update($requestData);

        Session::flash('flash_message', 'Folder updated!');
        return redirect('dashboard/folders');
    }

    public function destroy($id)
    {
        Folder::destroy($id);
        Session::flash('flash_message', 'Folder deleted!');
        return redirect('dashboard/folders');
    }
}
