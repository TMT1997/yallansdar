<?php

namespace App\Http\Controllers\Dashboard\Messages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Messages\Message;
use App\Messages\Contact;
use App\Messages\Draft;
use App\Messages\Folder;
use App\TemplateSteps;
use App\UserProfile;
use App\User;
use App\Packages\UserPackage;
use App\Packages\Package;
use App\Packages\PackageItem;
use App\Product;
use App\Deals\QuoteRequest;
use App\Deals\Quote;
use App\Advs;
use Auth;
use View;
USE Session;

class MessagesController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$messages_sidebar;
        parent::__construct();
        $folders = Folder::whereUser_id(Auth::id())->orderBy('name','ASC')->get();
        View::share('folders', $folders);
    }

    public function getInbox() {
        return $this->getIndex();
    }

    public function getIndex() {
        $dash_adv = Advs::whereActive(1)->get();
        $products = Product::whereUser_id(\Auth::id())->get();
        $all_admins = User::whereAdmin(1)->get();
        $user_profile = UserProfile::whereUser_id(\Auth::id())->first();
        $user = User::whereId(\Auth::id())->first();
        $user_package = UserPackage::whereUser_id(\Auth::id())->orderBy('id', 'desc')
        ->first();
        $quote = Quote::whereFrom_user_id(\Auth::id())->get();
        $template_steps = TemplateSteps::whereUser_id(\Auth::id())->first();
        $quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->get();
        $items = PackageItem::whereActive(1)->orderBy('sort_order', 'ASC')->get();
        $sections = Package::$sections;
        if($user_package)
        { 
            $packages = Package::where('id',"=",$user_package->package_id)->first();
            if($products->count() > 0)
            {
                $date_package =$user_package->created_at;
                $products = Product::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
                $quoterequest   = QuoteRequest::whereUser_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
                $quote = Quote::whereFrom_user_id(\Auth::id())->Where('created_at',">=",$date_package)->get();
            }//end has products
        }//end user_package
        else
            $user_package = 0;
        $messages = Message::whereTo_user_id(Auth::id())->whereDraft(0)->whereTrashed(0)->whereStatus(1)->whereSpammed(0)->orderBy('id','DESC')->with('attachments')->get();
        return view('dashboard.messages.inbox', compact('messages','template_steps','user_profile','user_package','packages','user','all_admins','products','quoterequest','quote','items','sections','dash_adv'));
    }

    public function getSpam() {
        $messages = Message::whereTo_user_id(Auth::id())->whereTrashed(0)->whereSpammed(1)->orderBy('id','DESC')->get();
        return view('dashboard.messages.spam', compact('messages'));
    }

    public function getSent() {
        $messages = Message::whereFrom_user_id(Auth::id())->whereTrashed(0)->orderBy('id','DESC')->with('contact')->get();
        return view('dashboard.messages.sent', compact('messages'));
    }

    public function getDraft() {
        $messages = Draft::whereUser_id(Auth::id())->orderBy('id','DESC')->get();
        return view('dashboard.messages.draft', compact('messages'));
    }

    public function getTrash() {
        $messages = Message::whereTo_user_id(Auth::id())->whereTrashed(1)->orderBy('id','DESC')->with('contact')->get();
        return view('dashboard.messages.trash', compact('messages'));
    }

    public function getShow($message_id, $slug=null) {
        $message = Message::whereId($message_id)->with('contact', 'attachments')->first();

        //Check if message not exist or not belongs to this user
        if(!$message || ($message->to_user_id!=Auth::id() && $message->from_user_id!=Auth::id())) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'Message not found, or has been deleted');
            return redirect()->to('dashboard/messages');
        }
        $message->read=1;
        $message->save();
        return view('dashboard.messages.show', compact('message'));
    }

    public function postMove(Request $request, $goto) {
        $id = $request->get('id');
        $message = Message::findOrFail($id);
    	if($goto=='trash') {
            $message->trashed=1;
        }
        if($goto=='inbox') {
            $message->trashed=0;
        }
        if($goto=='spam') {
            $message->spammed=1;
        }
        if($goto=='notspam') {
            $message->spammed=0;
        }

        $message->save();
    	if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$message]);
        }
        Session::flash('flash_message', 'Item set successfully');
        return redirect('dashboard/messages');
    }

    public function postSaveDraft(Request $request)
    {
        $data = $request->all();
        $message_data = [
            'user_id'=>Auth::id(),
            'contacts'=>implode(',', $data['contacts']),
            'subject'=>$data['subject'],
            'body'=>$data['body'],
        ];
        if($request->has('id')) {
            $draft_message = Draft::findOrFail($request->get('id'));
            if($draft_message) {
                $draft_message->update($message_data);
            }
        } else {
            $message = Draft::create($message_data);            
        }

        Session::flash('flash_message', 'Message has been saved to draft');
        return redirect('dashboard/messages/draft');
    }

    public function getCompose($message_id=null) {
        $message = null;
        if(isset($message_id)) {
            $message = Draft::findOrFail($message_id);
            $message->contacts =explode(',', $message->contacts);
        }
        $contacts = Contact::whereUser_id(Auth::id())->get();
    	return view('dashboard.messages.compose', compact('contacts', 'message'));
    }

    public function postCompose(Request $request)
    {
        if($request->has('btn_draft')) {
            return $this->postSaveDraft($request);
        }

        $data = $request->all();
        $contacts = Contact::whereIn('id', $request->get('contacts'))->get();
        $output = '';
        if($contacts->count() > 0)
        {
            foreach($contacts as $contact)
            {
                $message_data = [
                    'from_user_id'=>Auth::id(),
                    'to_user_id'=>$contact->contact_user_id,
                    'section'=>'inbox',
                    'subject'=>$data['subject'],
                    'body'=>$data['body'],
                ];
                $message = Message::create($message_data);
                $output .= $contact->email.', ';
            }
        }
        Session::flash('flash_message', 'Message has been sent to: '.$output);
        return redirect('dashboard/messages');
    }

    public function postReply(Request $request)
    {
        $data = $request->all();
        $message = Message::findOrFail($data['replied_on']);
        $output = '';

        $message_data = [
            'repied_on'=>$message->id,
            'from_user_id'=>Auth::id(),
            'to_user_id'=>$message->from_user_id,
            'contact_id'=>$message->contact_id,
            'subject'=>'Re: '.$message->subject,
            'body'=>$data['body'],
        ];
        $message = Message::create($message_data);
            
        Session::flash('flash_message', 'Message Reply has been sent');
        return redirect('dashboard/messages');
    }

    public function postPermaDelete(Request $req, $id=null)
    {
        $id = $req->get('id');
        Message::destroy($id);
        Session::flash('flash_message', 'Message has been deleted');
        if($req->ajax()) {
            return json_encode(['status'=>'success']);
        }
        return redirect('dashboard/messages');
    }

}