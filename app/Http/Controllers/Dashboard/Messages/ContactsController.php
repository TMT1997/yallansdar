<?php

namespace App\Http\Controllers\Dashboard\Messages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Dashboard\InitController;
use App\Messages\Contact;
use Auth;
use View;
use Session;

class ContactsController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$messages_sidebar;
        parent::__construct();
        $groups = \App\ProductGroup::whereUser_id(Auth::id())->get();
        View::share('groups', $groups);
    }

    public function index()
    {
        $contacts = Contact::whereUser_id(Auth::id())->with('user')->get();
        return view('dashboard.contacts.index', compact('contacts'));
    }

    public function create()
    {
        return view('dashboard.contacts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Contact::$validation);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        
        $contact = Contact::create($requestData);

        //Check if this email already have an account
        $user = \App\User::whereEmail($contact->email)->first();
        if($user) {
            $contact->contact_user_id = $user->id;
            $contact->save();
        }

        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$contact]);
        }
        Session::flash('flash_message', 'Contact added!');
        return redirect('dashboard/contacts');
    }

    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('dashboard.contacts.show', compact('contact'));
    }

    public function edit($id)
    {
        $contact = Contact::findOrFail($id);
        return view('dashboard.contacts.edit', compact('contact'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, Contact::$validation);
        $requestData = $request->all();

        $contact = Contact::findOrFail($id);
        $contact->update($requestData);

        //Check if this email already have an account
        $user = \App\User::whereEmail($contact->email)->first();
        if($user) {
            $contact->contact_user_id = $user->id;
            $contact->save();
        }

        Session::flash('flash_message', 'Contact updated!');
        return redirect('dashboard/contacts');
    }

    public function destroy($id)
    {
        Contact::destroy($id);
        Session::flash('flash_message', 'Contact deleted!');
        return redirect('dashboard/contacts');
    }
}