<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserProfile;
use App\UserTradeInfo;
use App\UserFactoryInfo;
use App\UserCompanyRegistration;
use App\UserProfileDoc;
use App\UserOffice;
use App\Helper;
use Auth;
use Hash;
use View;
use Session;
use App;

class ProfileController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$profile_sidebar;
        parent::__construct();

        $categories = \App\Category::whereParent_category_id(0)->orderBy('name_'.App::getLocale(), 'ASC')->get();
        View::share('categories', $categories);
    }

    public function getIndex($id) {
        if($id=="personal")
            $check_form=0;
        else if($id == "company")
            $check_form=1;
        $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->with('docs','offices')->first();
        if(!$user_profile) {
            $user_profile = UserProfile::init(Auth::user());
        }

        foreach(UserProfile::$fields_arr as $field=>$action) {
            if(isset($user_profile->$field) && !empty($user_profile->$field)) {
                if($action=='serialize')
                    $user_profile->$field = unserialize($user_profile->$field);
                else
                    $user_profile->$field = explode(',', $user_profile->$field);
            }
        }
        //get data of factory data
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_factory_info) {
            $user_factory_info = UserFactoryInfo::init(Auth::user());
        }
        foreach(UserFactoryInfo::$array_fields as $field) {
            if(isset($user_factory_info->$field) && !empty($user_factory_info->$field)) {
                $user_factory_info->$field = explode(',', $user_factory_info->$field);
            }
        }
        //end get data of factory
        //get data of trade
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_trade_info) {
            $user_trade_info = UserTradeInfo::init(Auth::user());
        }
        foreach(UserTradeInfo::$array_fields as $field) {
            if(!empty($user_trade_info->$field)) {
                if($field == 'nearest_port') {
                    $user_trade_info->$field = unserialize($user_trade_info->$field);
                } else {
                    $user_trade_info->$field = explode(',', $user_trade_info->$field);
                }
            }
        }
        //end get data of trade
        return view('dashboard.profile.business_profile', compact('user_profile','user_factory_info','user_trade_info','check_form'));
    }
    public function postIndex(Request $request)
    {

        //check if edit image only in dashboard
        $data_photo = $request->all();
        $check_profile_only =count($data_photo);
        if($check_profile_only === 3)
        {
            $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->with('offices')->first();
            unset($data_photo['btn_save']);
            unset($data_photo['btn_submit']);
            foreach(UserProfile::$fields_upload as $field=>$type) 
            {
                if(!$request->hasFile($field)) {
                    continue;
                }
                $thumb=true;
                if($type=='document') {
                    $thumb=false;
                }
                $data_photo[$field] = Helper::upload($request, $field, $thumb);
            }
            if($user_profile->update($data_photo))
            {
                return redirect()->back();
            }
        }
        //else
        $user_profile = UserProfile::whereUser_id(Auth::id())->whereProfile('temporary')->with('offices')->first();
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if($user_profile->editable==0) {

            Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
            Session::flash('status', 'danger');
            return redirect()->back();
        }
        $data = $request->except(['annual_output_value','contract_manufacturings','export_percentage']);
        unset($data['btn_save']);
        unset($data['btn_submit']);

        //Save User Type
        $user = Auth::user();
        foreach(\App\User::$types as $type=>$label) {
            if(isset($data['user_type']) && in_array($type, $data['user_type'])) {
                $user->$type = 1;
            } else {
                $user->$type = 0;
            }
        }
        $user->save();
        unset($data['files']);
        foreach(UserProfile::$fields_arr as $field=>$action) {

            if(isset($data[$field])) {
                if($action=='serialize')
                    $data[$field] = serialize($data[$field]);
                elseif($field == "other_products")
                    $data[$field] = $data[$field];
                else
                    $data[$field] = implode(',', $data[$field]);
            }
        }

        foreach(UserProfile::$fields_upload as $field=>$type) {
            if(!$request->hasFile($field)) {
                continue;
            }
            $thumb=true;
            if($type=='document') {
                $thumb=false;
            }
            $data[$field] = Helper::upload($request, $field, $thumb);
        }

        //Divisional Offices
        if(isset($data['divoffice']))
        {
            $dataOffices = $data['divoffice'];
            $officesArr = [];
            foreach($dataOffices['division'] as $index=>$div)
            {
                if($index==0) { continue; }
                foreach(UserOffice::$fields_arr as $field=>$action) {
                    if(isset($dataOffices[$field][$index])) {
                        $officesArr[$index][$field] = $dataOffices[$field][$index];
                    }
                }
            }
            foreach($officesArr as $index=>$dataRow) {
                $dataRow['user_id'] = auth()->id();
                $dataRow['user_profile_id'] = $user_profile->id;
                if(isset($dataRow['id']) && !empty($dataRow['id'])) {
                    $office_row = UserOffice::whereId($dataRow['id'])->first();
                    if($office_row) {
                        $office_row->update($dataRow);
                    }
                } else {
                    UserOffice::create($dataRow);
                }
            }
            unset($data['divoffice']);
        }//check if 
        $msg = trans('dash.profile_saved');
        if($request->has('btn_save')) {
            $data['status'] = -1; //just a save
        }
        if($request->has('btn_submit')) {
            $data['status'] = 0; //pending
            $data['editable'] = 0; //nOT allowed to edit after submission, till approved
            $msg = trans('dash.profile_submitted');
        }
        //factory info data 
        $user_factory_info->annual_output_value = $request->input('annual_output_value');
        if($request->input('contract_manufacturings'))
        {
            $contract_manufacturings =  $tags = implode(',', $request->input('contract_manufacturings'));
            $user_factory_info->contract_manufacturings = $contract_manufacturings;
        }
        $user_factory_info->save();
        //end factory info data
        //Trade Info data
        $user_trade_info->export_percentage = $request->input('export_percentage');
        $user_trade_info->save();

        //end Trade Info data
        if($user_profile->update($data)) {
            //Update Template Steps
            \App\TemplateSteps::update_step(['personal_profile', 'company_profile' ]);

            Session::flash('flash_message', $msg);
            return redirect()->back();
        }
        Session::flash('flash_message', 'An error occured, please try again');
        Session::flash('alert', 'danger');
        return redirect()->back();
    }

    public function getTrade()
    {
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_trade_info) {
            $user_trade_info = UserTradeInfo::init(Auth::user());
        }
        foreach(UserTradeInfo::$array_fields as $field) {
            if(!empty($user_trade_info->$field)) {
                if($field == 'nearest_port') {
                    $user_trade_info->$field = unserialize($user_trade_info->$field);
                } else {
                    $user_trade_info->$field = explode(',', $user_trade_info->$field);
                }
            }
        }
        return view('dashboard.profile.trade_info', compact('user_trade_info'));
    }

    public function postTrade(Request $request)
    {
        $user_trade_info = UserTradeInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if($user_trade_info->editable==0) {
            Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
            Session::flash('status', 'danger');
            return redirect()->back();
        }

        $data = $request->all();
        foreach(UserTradeInfo::$array_fields as $field) {
            if(isset($data[$field])) {
                if($field == 'nearest_port') {
                    $data[$field] = serialize($data[$field]);
                } else {
                    $data[$field] = implode(',', $data[$field]);
                }
            }
        }        

        $msg = 'Trade Information saved successfully';
        if($request->has('btn_save')) {
            $data['status'] = -1; //just a save
        }
        if($request->has('btn_submit')) {
            $data['status'] = 0; //pending
            $data['editable'] = 0; //nOT allowed to edit after submission, till approved
            $msg = 'Profile submitted successfully, please wait for approval';
        }
        if($user_trade_info->update($data)) {
            Session::flash('flash_message', $msg);
            return redirect()->back();
        }

        return 'Error';
    }

    public function getFactory() {
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_factory_info) {
            $user_factory_info = UserFactoryInfo::init(Auth::user());
        }
        foreach(UserFactoryInfo::$array_fields as $field) {
            if(isset($user_factory_info->$field) && !empty($user_factory_info->$field)) {
                $user_factory_info->$field = explode(',', $user_factory_info->$field);
            }
        }

        return view('dashboard.profile.factory_info', compact('user_factory_info'));
    }

    public function postFactory(Request $request)
    {
        $user_factory_info = UserFactoryInfo::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if($user_factory_info->editable==0) {
            Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
            Session::flash('status', 'danger');
            return redirect()->back();
        }

        $data = $request->all();
        unset($data['files']);
        foreach(UserFactoryInfo::$array_fields as $field) {
            if(isset($data[$field])) {
                $data[$field] = implode(',', $data[$field]);
            }
        } 

        $msg = 'Factory Information saved successfully';

        if($request->has('btn_save')) {
            $data['status'] = -1; //just a save
        }
        if($request->has('btn_submit')) {
            $data['status'] = 0; //pending
            $data['editable'] = 0; //nOT allowed to edit after submission, till approved
            $msg = 'Profile submitted successfully, please wait for approval';
        }
        if($user_factory_info->update($data)) {
            Session::flash('flash_message', $msg);
            return redirect()->back();
        }

        return 'Error';
    }

    public function getRegistration() {
        $user_company_reg = UserCompanyRegistration::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if(!$user_company_reg) {
            $user_company_reg = UserCompanyRegistration::init(Auth::user());
        }
        return view('dashboard.profile.registration_details',compact('user_company_reg'));
    }

    public function postRegistration(Request $request)
    {
        $user_company_reg = UserCompanyRegistration::whereUser_id(auth()->id())->whereProfile('temporary')->first();
        if($user_company_reg->editable==0) {
            Session::flash('flash_message', trans('dash.profile_edit_prohibited'));
            Session::flash('status', 'danger');
            return redirect()->back();
        }

        $data = $request->all();

        $msg = 'Company Information saved successfully';
        $data['status'] = -1; //just a save

        if($request->has('btn_submit')) {
            $data['status'] = 0; //pending
            $data['editable'] = 0; //nOT allowed to edit after submission, till approved
            $msg = 'Profile submitted successfully, please wait for approval';
        }
        if($user_company_reg->update($data)) {
            Session::flash('flash_message', $msg);
            return redirect()->back();
        }

        return 'Error';
    }

    public function getPassword() {
    	return view('dashboard.profile.change_password');
    }
    public function postPassword(Request $request) {
    	$this->validate($request, [
    		'old_password'=>'required',
    		'new_password'=>'required|min:6',
    		'confirm_password'=>'required',
    	]);

    	$user  = Auth::user();
    	if(Hash::check($request->get('old_password'), $user->password)){
	    	$user->password = Hash::make($request->get('new_password'));
	    	if($user->save()) {
	    		return json_encode(['status'=>'success', 'message'=>'Password changed successfully']);
	    	}
    	} else {
    		return json_encode(['status'=>'error', 'message'=>'Current Password is not correct']);
    		
    	}
    	return json_encode(['status'=>'error', 'message'=>'Unknown Error']);
    }

    public function getChangeEmail() {
    	return view('dashboard.profile.change_email');
    }

    public function postChangeEmail(Request $request) {
    	$this->validate($request, [
    		'new_email'=>'required|email',
    		'password'=>'required'
    	]);

    	$user  = Auth::user();
    	if(Hash::check($request->get('password'), $user->password)){
	    	if($request->has('alter_email')) {
	    		$user->sec_email = $user->email;
	    	}
	    	$user->email = $request->get('new_email');
	    	if($user->save()) {
	    		return json_encode(['status'=>'success', 'message'=>'Email changed successfully']);
	    	}
    	} else {
    		return json_encode(['status'=>'error', 'message'=>'Current Password is not correct']);
    		
    	}
    	return json_encode(['status'=>'error', 'message'=>'Unknown Error']);
    }

    public function getDeactivate() {
    	return view('dashboard.profile.deactivate_account');
    }
    public function postDeactivate(Request $request) {
    	$this->validate($request, [
    		'reason'=>'required',
    	]);
    	$data = $request->all();
    	$user  = Auth::user();
    	$user->deactivated = 1;
    	if($user->save()) {
    		$user_deactivate = new \App\UserDeactivate;
    		$data['user_id'] = Auth::id();
    		$user_deactivate->create($data);
    	}
    	return redirect()->back();
    }
}