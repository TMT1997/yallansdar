<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Media;
use App\MediaAlbum;
use App\Helper;
use View;
use Session;
use Auth;

class MediaController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$products_sidebar;
        parent::__construct();
        $albums = MediaAlbum::whereUser_id(Auth::id())->get();
        View::share('albums', $albums);
    }

    public function index(Request $request)
    {
        $media = new Media;
        $media = $media->whereUser_id(Auth::id())->get();
        return view('dashboard.media.index', compact('media'));
    }

    public function create()
    {
        return view('dashboard.media.create');
    }

    public function store(Request $request)
    {
        if($request->get('type')=='documents') {
            Media::$rules['file'] = 'mimes:xls,xlsx,ppt,pptx,txt,pdf,doc,docx|required|file';
        }
        $this->validate($request, Media::$rules);
        $requestData = $request->all();

        $requestData['file'] = Media::upload_doc($request, 'file');
        $requestData['name'] = $request->file('file')->getClientOriginalName();
        $requestData['user_id'] = Auth::id();
        
        $result = Media::create($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Media File added!');
        return redirect('dashboard/media');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $media = Media::findOrFail($id);
        return view('dashboard.media.edit', compact('media'));
    }

    public function update(Request $request, $id)
    {
        $media = Media::findOrFail($id);
        $this->validate($request, Media::$rules);

        $requestData = $request->all();
        $requestData['file'] = Helper::upload($request, 'file');
        $requestData['name'] = $request->file('file')->getClientOriginalName();
        
        $result = $media->update($requestData);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$result]);
        }
        Session::flash('flash_message', 'Media File Updated!');
        return redirect('dashboard/media');
    }

    public function destroy($id)
    {
        $media = Media::findOrFail($id);
        if(Media::destroy($id)) {
            if(file_exists(public_path('uploads/'.$media->file))) {
                unlink(public_path('uploads/'.$media->file));                
            }
        }
        Session::flash('flash_message', 'Media File deleted!');
        return redirect('dashboard/media');
    }

    public function grid(Request $request)
    {
        $media = new Media;
        $type = $request->get('type');
        $media = $media->whereUser_id(Auth::id());

        if(!empty($type)) {
            $media = $media->whereType($type);
        }
        if($request->has('album_id')) {
            $media = $media->whereMedia_album_id($request->get('album_id'));
        }
        $media = $media->with('album')->get();
        return view('dashboard.media.grid', compact('media', 'type'));
    }
}
