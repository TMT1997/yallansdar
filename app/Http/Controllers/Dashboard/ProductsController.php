<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\ProductGroup;
use App\UserWebsite;
use App\Template;
use App\ProductDetails;
use App\TemplateSteps;
use App\ProductImage;
use App\ProductLang;
use App\Country;
use App\Helper;
use App\Media;
use Auth;
use View;
use Session;

class ProductsController extends InitController
{
    public function __construct()
    {
        $this->side_menu = \App\Menu::$products_sidebar;
        parent::__construct();

        $categories = Category::whereParent_category_id(0)->orderBy('name_ar', 'ASC')->get();
        $groups = ProductGroup::whereActive(1)->whereUser_id(Auth::id())->orderBy('name', 'ASC')->lists('name', 'id');

        View::share('categories', $categories);
        View::share('groups', $groups);
    }

    public function index()
    {
        $products = new Product;
        $products = $products->whereUser_id(Auth::id());
        $products = $products->with('category');
        $products = $products->get();

        return view('dashboard.products.index', compact('products'));
    }

    public function grid(Request $request)
    {
        $status = $request->get('status');
        $products = new Product;
        $products = $products->whereUser_id(Auth::id());
        if ($status!=0) {
            $products = $products->whereApproved($status);
        }

        $products = $products->with('lang')->orderBy('id','DESC')->get();
        return view('dashboard.products.grid', compact('products'));
    }

    public function create()
    {
        return view('dashboard.products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, Product::$rules);
        //=========================
        $data = $request->except('lang', 'add_images');
        $data['user_id'] = Auth::id();
        $data['slug'] = Helper::generate_slug('\App\Product', $data['name_ar']);
        if($request->hasFile('image')) {
            $data['image'] = Helper::upload($request, 'image');
        }
        $data['category_id'] = $request->get('selected_category_id');
        if(isset($data['payment_type'])) {
            $data['payment_type'] = implode(',', $data['payment_type']);
        }
        
        unset($data['selected_category_id']);
        $product = Product::create($data);

        if($product)
        {
            $media = Media::create([
                'user_id'=>Auth::id(),
                'type'=>'images',
                'media_album_id'=>1,
                'file'=>$data['image'],
                'name'=>'no_name'
            ]);

            \App\TemplateSteps::update_step('add_products');
        }
        $images = ProductImage::upload_multi_images($request->file('add_images'), $product->id);
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', trans('dash.prod_added',['prod'=>'<a href="'.route('dashboard.products.edit',$product->id).'" class="btn btn-default">'.$product->name_ar.'</a>']));
        $come_from = $request->headers->get('referer');
        if (strpos($come_from, 'reg') !== false) {
            $steps = TemplateSteps::whereUser_id(Auth::id())->first();
            $steps->add_products=1;
            $steps->save();
             Session::flash('prod_done','prod_done');
             //choose template
             $template = Template::find(4);
            $user_website = UserWebsite::whereUser_id(Auth::id())->first();
            if(!$user_website) {
                $user_website = UserWebsite::init(Auth::user());
            }

            $user_website->template_id = 4;
            if($user_website->save()) {
                $steps = TemplateSteps::whereUser_id(Auth::id())->first();
                $steps->select_template=1;
                if($steps->save()) {
                    Session::flash('flash_message', 'تم اختيار القالب التصميمي بنجاح');
                }
            }
             //end choose template
             return "done";
        }
        else
        {
           return redirect('dashboard/products');
        }
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('dashboard.products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::whereId($id)->with('images')->first();
        $product->more_details = unserialize($product->more_details);
        $product->payment_type = explode(',',$product->payment_type);

        /*$lang=[];
        if($product->lang) {
            foreach($product->lang as $item) {
                $lang[$item->language_id] = $item;
            }
        }
        $product->lang = $lang;*/
        return view('dashboard.products.edit', compact('product', 'category_tree'));
    }

    public function update($id, Request $request)
    {
        $validate_rules = Product::$rules;
        $validate_rules['image'] = '';

        $this->validate($request, $validate_rules);

        $product = Product::whereId($id)->with('images')->first();
        $product_data = $request->except('lang', 'add_images');

        $product_data['user_id'] = Auth::id();
        $product_data['category_id'] = $request->get('selected_category_id');

        $products_data['slug'] = Helper::generate_slug('\App\Product', $product_data['name_ar'], $id);

        if($request->hasFile('image')) {
            $product_data['image'] = Helper::upload($request, 'image');
        }
        if(isset($product_data['more_details'])) {
            $product_data['more_details'] = serialize($product_data['more_details']);
        }
        if(isset($product_data['payment_type'])) {
            $product_data['payment_type'] = implode(',', $product_data['payment_type']);
        }
        unset($product_data['selected_category_id']);

        $product->update($product_data);
        $product_name = '';

        if($product)
        {
            //Save Product Language
            /*foreach ($request->get('lang') as $lang_id=>$data) {
                $row = ProductLang::whereProduct_id($id)->whereLanguage_id($lang_id)->first();
                if(!$row) {
                    $row = new ProductLang;
                    $row->product_id = $id;
                    $row->language_id = $lang_id;
                }
                if(isset($data)) {
                    $row->name = $data['name'];
                    $row->keywords = $data['keywords'];
                    $row->description = $data['description'];
                    $row->details = $data['details'];
                    $row->packaging = $data['packaging'];
                    $product_name = $row->name;
                }
                $row->save();
            }*/

            //Upload & Save Additional Images
            ProductImage::upload_multi_images($request->file('add_images'), $id);
        }


        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }

        Session::flash('flash_message', trans('dash.prod_updated',['prod'=>'<a class="btn btn-default" href="'.route('dashboard.products.edit',$id).'">'.$product->name_ar.'</a>']));
        return redirect('dashboard/products');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if(!$product) {
            Session::flash('alert', 'danger');
            Session::flash('flash_message', 'Product not found');
            return redirect('dashboard/products');
        }
        Product::destroy($id);

        //Delete Product image
        if(!empty($product->image) && file_exists(public_path().'/uploads/'.$product->image)) {
            unlink(public_path().'/uploads/'.$product->image);
        }

        //Delete all other images
        $product_images = ProductImage::whereProduct_id($id)->get();
        foreach($product_images as $image) {
            if(!empty($image->image)) {
                unlink(public_path().'uploads/'.$image->image);
            }
        }
        Session::flash('flash_message', 'Product deleted');
        return redirect('dashboard/products');
    }

    public function trash(Request $request)
    {
        $product = Product::findOrFail($request->get('id'));
        $product->restored_as = $product->approved; //If restored set its status as this
        $product->approved = 4; //4: trash
        if($product->save()) {

        }
        if($request->ajax()) {
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', 'Product trashed successfully');
        return redirect('dashboard/products');
    }

    public function restore(Request $request)
    {
        $product = Product::findOrFail($request->get('id'));
        $product->approved = $product->restored_as; //The status was before trash
        if($product->save()) {

        }
        if($request->ajax()){
            return json_encode(['status'=>'success', 'data'=>$product]);
        }
        Session::flash('flash_message', 'Product restored successfully');
        return redirect('dashboard/products');
    }

    public function get_sub($category_id)
    {
        $parent_cat_id = $category_id;
        $childs = Category::whereParent_category_id($category_id)->orderBy('sort', 'ASC')->get();
        $output = '';
        $c_name = $this->c_name;
        foreach($childs as $child) {
            $output .='<option value="'.$child->id.'">'.$child->$c_name.'</option>';
        }
        return $output;
    }

    public function readReason(Request $request)
    {
        $id = $request->get('id');
        $reject_reason = \App\UserStatusReason::findOrFail($id);
        $reject_reason->read = 1;

        if($reject_reason->save()) {
            if($request->ajax()) {
                return json_encode(['status'=>'success']);
            }
            Session::flash('flash_message', 'Read reason done');
            return redirect('dashboard');
        }

        return 0;
    }
}
