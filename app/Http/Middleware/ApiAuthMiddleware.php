<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Helper;


class ApiAuthMiddleware
{
    /**
     check if the request come from authorized person
     * input: api_token
     * output: pass to next route or return unauthorized jason
     */
    public function handle($request, Closure $next)
    {

        // the passed api
        if($request->api_token){ // if the api token passed with the request
            $api_token = $request->api_token; // store it
            //select the user with that api_token
            $user = User::whereApi_token($api_token)->first();
            //check if it's existed or not
            if(empty($user)){
                return Helper::api_response_format(false,'','unauthorized user');
            }//
            else{ //  the token exist then  pass to the other route
                return $next($request);
            }

        }
        else{
            //return response with false and error message
            return Helper::api_response_format(false,'','unauthorized you must provide the api token');
        }

    }
}
