<?php

namespace App\Http\Middleware;

use App\Template;
use App\TemplateSteps;
use App\User;
use App\UserWebsite;
use Closure;
use Illuminate\Support\Facades\Auth;

class RegistrationSteps
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::id();
        $user = TemplateSteps::whereUser_id($id)->first();
        $user_website = UserWebsite::whereUser_id($id)->first();
        if($user_website)
        {
            $domain = $user_website->domain;

            if(empty($domain) && !$user->completed){
                return redirect('dashboard/reg');
            }
        }
        else
        {
            return redirect('dashboard/reg');
        }
        return $next($request);
    }
}
