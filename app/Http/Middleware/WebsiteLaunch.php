<?php

namespace App\Http\Middleware;

use Closure;
use App\UserWebsite;

class WebsiteLaunch
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $domain = $request->segment(2);
        if(!$domain) {
            return view('templates.not_found'); die;
        }
        if($domain=='master') { //special domain to avoid check
            return $next($request);            
        }
        $website = UserWebsite::whereDomain($domain)->with('template', 'user')->first();

        if(auth()->check() && auth()->user()->admin==1) {
            return $next($request);            
        }

        if(!$website) {
            $status = 'not_found';
            return view('templates.not_found', compact('status')); die;
        } else if($website->status!=2 || $website->active!=1) {
            $status = 'not_active';
            return view('templates.not_found', compact('status')); die;
        }

        return $next($request);
    }
}
