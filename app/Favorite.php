<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';
    protected $guarded = [];

    public static $types = [
    	'Products',
    	'Services',
    	'Deals',
    	'Categories',
    ];

    public function product() {
    	return $this->belongsTo('App\Product', 'record_id')->with('lang');
    }

    public function service() {
    	return $this->belongsTo('App\Service');
    }

    public function category() {
    	return $this->belongsTo('App\Category');
    }
}
