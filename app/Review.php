<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $guarded = [];

	public static $rate_max = 5;
    public static $types =[
    	'Product'=>'Product',
    	'Company'=>'Company',
   	];

   	public static $direction =[
    	'Received'=>'Reviews Received',
    	'Given'=>'Reviews Given',
   	];

    public static $status = [
      '1'=>'Approved',
      '0'=>'Pending',
      '-1'=>'Disapproved',
    ];

    public static $rates = [
      '5'=>['Excellent', 'smile-o'],
      '4'=>['Very Good', 'smile-o'],
      '3'=>['Good', 'smile-o'],
      '2'=>['Poor', 'meh-o'],
      '1'=>['Very Poor', 'frown-o'],
    ];

   	public function product() {
      return $this->belongsTo('App\Product');
    }

    public function user() {
      return $this->belongsTo('App\User');
    }

    public function from_user() {
   		return $this->belongsTo('App\User', 'from_user_id');
   	}

    public static function rate_stars($reviews)
    {
      $total = $reviews->count();
      $rates = 0;
      foreach($reviews as $review) {
        $rates += $review->rate;
      }
      $percentage = ($total*5) / $rates;

      return $rates . '/'. $total*5; 
    }
}
