<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReason extends Model
{
    protected $guarded = [];
}
