<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use Hash;

class User extends Authenticatable
{
    use HasRoles;
    
    protected $fillable = [
        'type', 
        'name',
        'slug',
        'email',
        'sec_email',
        'password',
        'api_token',
        'mobile',
        'address',
        'package',
        'company_name',
        'email_verified',
        'active',
        'exporter',
        'importer',
        'service_provider',
        'last_name',
        'super_admin'
    ];
    protected $hidden = [
        'password', 'remember_token', 'admin'
    ];
    public static $types = [
        'importer'=>'importer',
        'exporter'=>'exporter',
        'service_provider'=>'service_provider',
    ];
    public static $prof_types = [
        'importer'=>'importer_profile',
        'exporter'=>'exporter_profile',
        'service_provider'=>'service_provider_profile',
    ];
    public static $leaving_reasons = [
        'I don\'t feel safe on Yalla Nsadar',
        'I don\'t understand how to use Yalla Nsadar',
        'My account was hacked',
        'I get too many emails and requests from Yalla Nsadar',
    ];

    public static $checklist = [
        'email_verified'=>['#', 'Email address verified'],
        'personal_profile'=>['dashboard/profile', 'Complete your personal profile'],
        'company_profile'=>['dashboard/profile#company', 'Complete your company info'],
        'add_products'=>['dashboard/products/create', 'Add Products'],
        'select_template'=>['dashboard/website', 'Choose Template'],
    ];

    public function company() {
        return $this->hasOne('App\Company');
    }
    public function products() {
        return $this->hasMany('App\Product');
    }
    public function profile() {
        return $this->hasOne('App\UserProfile')->whereProfile('permanent');
    }
    public function profiles() {
        return $this->hasMany('App\UserProfile');
    }
    public function user_package() {
        return $this->hasOne(Packages\UserPackage::class)->with('package');
    }
    public function store() {
        return $this->hasOne(UserWebsite::class);
    }

    public function access_pages() {
        return $this->hasMany(UserAccessPage::class);
    }

    public static function generateRandomPassword() {
        return rand(10000, 99999);
    }

    public static function user($item, $name=null, $user_id_col='user_id') {
        if(!$name) {
            $name = (isset($item->user)) ? $item->user->name: $item->user_id;
        }
        return '<a href="'.route('admin.users.show', $item->$user_id_col).'" class="btn btn-default btn-sm popup">'.$name.'</a>';
    }

    public static function website($user) {
        return \App\Company::products_url($user);
        //return url('company/site/'.$user->id.'/'.str_slug($user->profile->company_name));
    }
    public static function url_contact($user) {
        if(!$user){
            return '';
        }
        if($user->profile) {
            return url('company/contact/'.$user->id.'/'.str_slug($user->profile->company_name));            
        }
        return '-';
    }

    public static function unlock_url($ref, $id, $unlock_id) {
        return url('interact/unlock?ref='.$ref.'&id='.$id.'&unid='.$unlock_id);
    }

    public static function contact_info($user) {
        if(!$user){
            return '';
        }
        if($user->profile) {
            return url('contact-info/'.$user->slug);
            //return url('company/contact-info/'.$user->id.'/'.str_slug($user->profile->company_name));
        }
        return '-';
    }

    public static function flag($user, $attr=[], $with_name=false, $size=24)
    {
        if(isset($user->profile) && !empty($user->profile)) {

            $country = Country::get_country($user->profile->country_id);
            if($country) {
                $country_name = $country->name;
                $iso_code = $country->iso2;
                $attr['title'] = $country->name;

                $str = \HTML::image('flags/'.$size.'/'.$iso_code.'.png',$country_name,$attr);
                if($with_name) {
                    return '<i class="flag">'.$str.'</i> <span class="flgname">'.$country_name.'</span>';
                }                
                return $str;
            }
        } else {
            return \HTML::image('flags/'.$size.'/EG.png','Egypt',['title'=>'Egypt']);
        }
    }

    public static function isVerified($user, $add_label=true)
    {
        if($user && $user->verified==1) {
            $label = trans('front.verified');
            $str = ($add_label)?$label:'';
            return '<a href="javascript:void(0)" class="uverified"><i class="fa fa-check-circle text-success" title="'.$label.'"></i></a> '.$str;
        }
        $label = trans('front.not_verified');
        $str = ($add_label)?$label:'';
        return '<a href="javascript:void(0)" class="unotverified"><i class="fa fa-exclamation-circle nt-vrfid" title="'.$label.'"></i></a> '.$str;
    }

    public static function sendVerificationEmail($user, $include_password=false, $password=null)
    {
        //send verification mail to user
        $data['verification_code']  = $user->verification_code;
        $data['activation_url'] = url('registration/activate?activation='.Hash::make($user->email).'&email='.$user->email);
        $data['indentifier'] = Hash::make($user->id);
        if($include_password) {
            $data['password'] = $password;
        }

        Mail::send('front.emails.activate_account',['user'=>$user,'data'=>$data],function($message) use($user)
        {
            $message->from('no-reply@yallansadar.com', "Yalla Nsadar");
            $message->subject(trans('front.activate_acc_onyallansadar'));
            $message->to($user->email);
        });

        return true;
    }
     public static function sendwelcomeEmail($user, $include_password=false, $password=null)
    {
        //send verification mail to user
        $data['activation_url'] = url('http://yallansadar.com/dashboard');
        $data['indentifier'] = Hash::make($user->id);
        if($include_password) {
            $data['password'] = $password;
        }

        Mail::send('front.emails.end_reg',['user'=>$user,'data'=>$data],function($message) use($user)
        {
            $message->from('no-reply@yallansadar.com', "Yalla Nsadar");
            $message->subject(trans('front.activate_acc_onyallansadar'));
            $message->to($user->email);
        });

        return true;
    }
    public static function createVisitorAccount($data)
    {
        $user_password = $data['password'];
        $data['password'] = bcrypt($user_password);
        $user = self::create($data);
        if($user) {
            self::sendVerificationEmail($user, true, $user_password);
            return $user;
        }

        return false;
    }

    public static function hasAccess($url, $user_access_pages=null)
    {
        if(auth()->user()->super_admin) {
            return true;
        }

        if(!$user_access_pages) {
            $user_access_pages = auth()->user()->access_pages;
        }

        if(is_array($user_access_pages)) {
            if(in_array($url, $user_access_pages)) {
                return true;
            }
        } else {
            if($user_access_pages->contains('page',$url)) {
                return true;
            }
        }

        return false;
    }

    public static function hasAcessSection($menu, $user_access_pages=null)
    {
        if(auth()->user()->super_admin==1) {
            return true;
        }

        if(!$user_access_pages) {
            $user_access_pages = auth()->user()->access_pages;
        }

        foreach($menu as  $url=>$page) {
            if($user_access_pages->contains('page',$url)) {
                return true;
                break;
            }   
        }

        return false;
    }
}