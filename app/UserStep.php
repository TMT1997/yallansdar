<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserStep extends Model
{
    protected $guarded = [];
   
    public static $steps = [
    	'personal_profile'=>'الحساب الشخصي',
    	'company_profile'=>'بيانات الشركة',
    	'add_products'=>'اضافة منتجات',
    	'create_website'=>'المتجر',
    ];

    /* Initialize steps for a user */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}

    	foreach(self::$steps as $step=>$label)
    	{
    		$data = [];
    		$data['user_id'] = $user->id;
    		$data['step'] = $step;
    		$data['completed'] = 0;
    		$user_steps = self::create($data);
    	}
    }

    public static function isCompleted($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}

    	$user_steps = self::whereUser_id($user->id)->whereCompleted(0)->get();
    	if($user_steps->count() > 0) {
    		return false; //not completed
    	}
    	return true; //all completed
    }
}
