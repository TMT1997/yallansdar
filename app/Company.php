<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HTML;
use App\Helper;

class Company extends Model
{
    public static $profile_nav = [
        'profile'=>'Company Info',
        'capacity'=>'Producation Capacity',
        'trade'=>'Trade Details',
    ];
	public function country() {
		return $this->belongsTo('App\Country');
	}

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function user_products() {
        return $this->belongsTo('App\User')->with('products');
    }

    public static function url($company) {
    	return url('company/site/'.$company->id.'/'.str_slug($company->company_name));
    }

    public static function products_url($user) {
        if($user) {
            if($user->slug) {
                return url('product/supplier/'.$user->slug);
            }
            return url('product/supplier/'.$user->id.'/'.str_slug($user->company_name));
        }
        return '#';
    }

    public static function logo($company, $attr=[]) {
        if($company && isset($company->profile)) {
            return Helper::image2($company->profile->picture, $attr);            
        }
        return '';
    }

    public static function flag($company, $size=24) {
    	$country = (isset($company->country))?$company->country->country_name : $company->company_name;
    	$iso = $company->country->iso2;

    	return HTML::image('flags/'.$size.'/'.$iso.'.png', $country, ['title'=>$country]);
    }
}