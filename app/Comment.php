<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comments';
    protected $fillable = ['record_id', 'model', 'name', 'email', 'comment'];

    public static $sections = [];

}
