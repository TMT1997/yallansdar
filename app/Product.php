<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HTML;
use App\Helper;

class Product extends Model
{
	protected $table = 'products';
	protected $guarded = [];
	public static $rules = [
        'name_ar'=> 'required',
        'name_en'=> 'required',
        'description_ar'=> 'required|max:250',
        'description_en'=> 'required|max:250',
        /*'details_ar'=> 'required',
        'details_en'=> 'required',
        'lang.*.name'=>'required',
        'lang.*.description'=>'required',
        'lang.*.details'=>'required',
        'image'=>'required|file',*/
        'production_capacity'=>'required',
        'capacity_unit'=>'required',
        'capacity_unit_per'=>'required',
        'min_quantity'=>'required',
        'fob_price_type'=>'required',
        'fob_price_from'=>'required',
        'fob_price'=>'required',
        'category_id'=>'required',
        'image'=>'required',
        'port'=>'required',
    ];

	public static $product_status = [
		'0'=>'All',
		'1'=>'Approvel Pending',
		'2'=>'Approved',
		'3'=>'Disapproved',
		'4'=>'Trashed',
	];

	public function details() {
		return $this->hasOne('App\ProductDetails');
	}

	public function category() {
		return $this->belongsTo('App\Category');
	}

	public function user() {
        return $this->belongsTo('App\User');
    }

    public function group() {
		return $this->belongsTo('App\ProductGroup');
	}

	public function lang() {
		return $this->hasMany('App\ProductLang');
	}

	public function images() {
		return $this->hasMany('App\ProductImage');
	}

	public function reviews() {
		return $this->hasMany('App\Review')->whereActive(1);
	}

	public function primary_image() {
        return $this->hasMany('App\ProductImage')->wherePrimary(1);
    }

    public function status_reason() {
		return $this->hasOne(UserStatusReason::class)->with('reason');
	}

    public static function url($product) {
        return url('products/'.$product->slug);
    }

    public static function print_url($product) {
        return url('product/print/'.$product->id.'/'.$product->slug);
    }

    public static function cat_url($category) {
    	return url('product/category/'.$category->id);
    }

    public static function name($product, $lang_id = null) {
        if(!$product) {
            return '';
        }
        return \App\Helper::field($product, 'name');
    	if($lang_id) {
	        $lang_data = $product->lang->filter(function($row) use ($lang_id){
                return $row->language_id==$lang_id;
	        })->first();
            if($lang_data) {
                return $lang_data->name;
            }
    	}
    	if(!empty($product->lang)) {
    		return @$product->lang->first()->name;
    	}
    	return $product->name;
    }

    public static function image($product, $attr=[]) {
    	return Helper::image2($product->image, $attr);
    }

    public static function capacity($product) {
    	if($product) {
            $unit = (isset($product->capacity_unit))? trans('dash.units')[$product->capacity_unit]:'';
            $duration = (isset($product->capacity_unit_per))? trans('dash.capacity_durations')[$product->capacity_unit_per]:'';
            $per = ' '.trans('front.per').' ';
            if($product->production_capacity) {
    		  return $product->production_capacity.' '.$unit.$per.$duration;
            }
    	}
    	return '';
    }
    public static function MOQ($product) {
    	if($product) {
            $unit = ($product->min_quantity_unit)? trans('dash.units')[$product->min_quantity_unit]:'';
    		return $product->min_quantity.' '.$unit;
    	}
    	return '';
    }
    public static function FOB_price($product) {
    	if($product) {
    		if(!empty($product->fob_price)) {
                $unit = (isset($product->fob_price_unit))? trans('dash.units')[$product->fob_price_unit]:'';
                $currency = (isset($product->fob_price_type))? trans('dash.fob_price_list')[$product->fob_price_type]:'';
                $per = ' '.trans('front.per').' ';
                if($product->fob_price) {
    			 return $product->fob_price.' '.$currency.$per.$unit;
                }
    		}
    		return 'NA';
    	}
    	return '';
    }

    public static function cart_items($cart)
    {
        $counter = 0;
        if($cart) {
            foreach($cart as $supplier=>$items) {
                $counter+=count($items);
            }
        }
        return $counter;
    }

    public static function get_youtube_video_id($video_embed_url)
    {
        $video_id = "";
        if($video_embed_url!="") {

            if(strstr($video_embed_url, 'youtu.be')) {
                $vv = explode("youtu.be",$video_embed_url);
                $video_id = $vv[1];
            } else {
                $vv = explode("v=",$video_embed_url);
                $video_id = $vv[1];
                if(strstr($video_id, '&')) {
                    $video_str = explode('&',$video_id);
                    $video_id = $video_str[0];
                }
                else {
                    $video_id = trim($video_id);
                }
            }
        }
        return $video_id;
    }

    public static function youtube_url($url)
    {
        $video_id = self::get_youtube_video_id($url);
        if($video_id) {
            return url('http://www.youtube.com/embed/'.$video_id.'?enablejsapi=1&wmode=opaque');
        }
    }

    public static function get_youtube_video_img($video_id, $img_size="small", $attr=[], $alt='')
    {
        if($img_size!="") {
            switch ($img_size)
            {
                case "small":
                    $size_str = "default.jpg"; break;
                case "medium":
                    $size_str = "hqdefault.jpg"; break;
                case "big":
                    $size_str = "maxresdefault.jpg"; break;
                default:
                    $size_str = "hqdefault.jpg"; break;
            }
        }
        else {
            $size_str = "hqdefault.jpg";
        }
        $img_url = "https://i.ytimg.com/vi/".$video_id."/".$size_str;
        return HTML::image($img_url, $alt, $attr);
    }

    public static function youtube_image($video_url, $size = 'medium', $attr=[], $alt='')
    {
        $video_id = self::get_youtube_video_id($video_url);
        if($video_id)
        {
            return self::get_youtube_video_img($video_id, $size, $attr, $alt);
        }
    }
}
