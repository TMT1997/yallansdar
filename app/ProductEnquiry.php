<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductEnquiry extends Model
{
    protected $table = 'product_enquiries';
    protected $guarded = [];

    public function product() {
    	return $this->belongsTo('App\Product');
    }

    public function from_user() {
    	return $this->belongsTo('App\User', 'from_user_id');
    }

    public function to_user() {
    	return $this->belongsTo('App\User', 'to_user_id');
    }
}
