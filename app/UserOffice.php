<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOffice extends Model
{
    protected $table = 'user_offices';
    protected $guarded = [];

    public static $fields_arr = [
        'id'=>'',
        'division'=>'explode',
        'contact_person'=>'explode',
        'address_line1'=>'explode',
        'address_line2'=>'explode',
        'email'=>'explode',
        'country_id'=>'explode',
        'state'=>'explode',
        'city'=>'explode',
        'zipcode'=>'explode',
        'tel_code'=>'explode',
        'telephone'=>'explode',
        'mob_code'=>'explode',
        'mobile'=>'explode',
        'fax_code'=>'explode',
        'fax'=>'explode',
    ];

    public static $divisions = [
      	''=>'Please Select',
        '1'=>'Branch Office',
        '2'=>'Corporate Office',
        '3'=>'Customer Care',
        '4'=>'Distributor',
        '5'=>'Factory Office',
        '6'=>'Manufacturing unit',
        '7'=>'Head Office',
        '8'=>'Sales Office',
        '9'=>'Technical Support',
        '10'=>'Trading office',
        '11'=>'Warehouse',
    ];
}
