<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PackageUsage extends Model
{
    protected $table = 'package_usage';
    protected $guarded = [];

    public static $items_match = [
        'unlock_contact'=>'Access to Buying Leads',
        'unlock_deal'=>'Access to Deals',
    ];

    //check if user already unlocked it
    public static function unlocked($unlocked_user_id, $item='product')
    {
        if(!Auth::check()) {
            return false;
        }
        $package_usage = self::whereUnlocked_user_id($unlocked_user_id)->whereUser_id(Auth::id())->first();
        if($package_usage) {
            return true;
        }

        return false;
    }

    //check if user already unlocked RFQ deal
    public static function unlocked_deal($deal_id=null, $item=null)
    {
    	if(!Auth::check()) {
    		return false;
    	}
    	$package_usage = self::whereDeal_id($deal_id)->whereUser_id(Auth::id())->first();
    	if($package_usage) {
    		return true;
    	}

    	return false;
    }

    public static function get_item($package, $item)
    {
        if(!$item) {
            return 0;
        }
        $values = $item->values; // get the values number of adding products in specific item
        if($values) {

            $info = unserialize($values);
            $pack_info = $info[$package->id];  //
            return $pack_info;
        }

        return 0;
    }
    //calculate one year from now
    public static function getAge($then) {
        return $date = date('Y-m-d', strtotime('+1 year', strtotime($then)));
    }

    //Get user package usage, for specific item, year, month
    public static function get_usage($item, $user_id=null, $year=null, $month=null , $package_date = null)
    {
        if(!$year)
            $year = date('Y');

        if(!$month)
            $month = date('m');

        if(!$user_id)
            $user_id = Auth::id();
        if(!$package_date) { // if there's no created at send then do the regular behaviour
            $usage = self::whereUser_id($user_id)->whereItem($item)->where('year', $year)->where('month', $month)->get();
            return $usage;
        }
        else{//u send package date  u need to compare it with with the current usage
            $usage = self::whereUser_id($user_id)->whereItem($item)->where('year', $year)->where('month', $month)->where("created_at" ,">=",$package_date)->get();
            return $usage;
        }
    }
}
