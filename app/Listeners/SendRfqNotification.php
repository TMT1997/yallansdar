<?php

namespace App\Listeners;

use App\Events\RfqCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\UserProfile;
use App\Messages\Message;
use App\Deals\QuoteRequest;

class SendRfqNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RfqCreated  $event
     * @return void
     */
    public function handle(RfqCreated $event)
    {
        $rfq = $event->rfq;

        //get parent category
        $cat_id = $rfq->parent_cat_id;

        //get all users has interes in parent category
        $user_profiles = UserProfile::whereProfile('permanent')->whereBusiness_category($cat_id)->with('user')->get();

        //send notification to them all
        $subject = 'فرصة تصدير جديدة تم اضافتها في نفس مجال عملك';
        $body = 'لقد تم اضافه فرصة تصدير جديدة في نفس مجال عملك <a href="'.QuoteRequest::url($rfq).'" target="_blank">قم بالاطلاع عليها الأن</a>';

        foreach($user_profiles as $user) {
             $message_data = [
                'from_user_id'=>1,
                //'contact_id'=>$message->contact_id,
                'to_user_id'=>$user->user_id,
                'subject'=>'Re: '.$subject,
                'body'=>$body,
            ];
            $message = Message::create($message_data);
        }

        return true;
    }
}
