<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatusReason extends Model
{
    protected $guarded = [];

    public function product() {
    	return $this->belongsTo(Product::class);
    }

    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function reason() {
    	return $this->belongsTo(ProductReason::class, 'product_reason_id');
    }
}
