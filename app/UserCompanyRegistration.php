<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompanyRegistration extends Model
{
    protected $table = 'user_company_registration';
	protected $guarded = [];

	public static $fields = [
		'registration_no'=>'text',
		'service_tax_no'=>'text',
		'vat_no'=>'text',
		'others'=>'textarea',
	];

    /* Initialize an empty records */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}
    	$profile_temp = [
    		'user_id'=>$user->id,
    		'profile'=>'temporary',
    		'status'=>1, //approved
		];
    	$user_profile_temp = self::create($profile_temp);
    	$profile_perma = $profile_temp;
		$profile_perma['profile'] = 'permanent';
        $user_profile_perma = self::create($profile_perma);

		return $user_profile_temp;
    }
}
