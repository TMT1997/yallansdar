<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogAuthor extends Model
{
	protected $guarded = [];
	
    public static $rules = [
        'name_ar' => 'min:3|required',
        'name_en' => 'min:3|required',
    ];

    public function articles()
    {
    	return $this->hasMany(Blog::class);
    }
}
