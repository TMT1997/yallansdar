<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class PartnershipLang extends Model
{
    protected $table = 'deals_partnerships_lang';
    protected $guarded = [];
}
