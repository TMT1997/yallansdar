<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $table = "deals_alerts";
    protected $guarded = [];

    public static $types = [
    	'Special offers'=>'Special offers',
    	'Partnership Request'=>'Partnership Request',
    	'Partnership Proposal'=>'Partnership Proposal',
    ];

    public static $relevancy = [
    	'Exact Category'=>'Exact Category',
    	'Similar Category'=>'Similar Category',
    	'General Category'=>'General Category',
    ];

    public static $frequency = [
        ''=>'Please Select',
        'Everyday'=>'Everyday',
        'Everyday 1 Week'=>'Everyday 1 Week',
        'Everyday 2 Weeks'=>'Everyday 2 Weeks',
    ];

    public static $receive_from = [
        'All'=>'All Countries',
        'Specific'=>'Specific Countries'
    ];
}
