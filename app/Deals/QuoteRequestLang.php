<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class QuoteRequestLang extends Model
{
    protected $table = 'deals_quote_requests_lang';
    protected $guarded = [];
}
