<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Deal extends Model
{
	protected $guarded = [];
	public static $rules = [
		'name_en'=>'required' 
	, 'name_ar'=>'required'
	, 'order_quantity'=>'required'
	, 'duration_from'=>'required'
	, 'duration_to'=>'required'
	, 'expired_at'=>'required'];

    public static $statues = [
		'0'=>'All',
		'1'=>'Pending',
		'2'=>'Approved',
		'3'=>'Disapproved',
		'4'=>'Expired',
		'5'=>'Trashed',
		'6'=>'Active',
	];

	public static $sections = [
		'sell_offer'=>'Sale Offers',
		'quote_request'=>'Request for Quote',
		'partnership'=>'Partnership Deals',
	];

	public static $types = [
		'offers'=>[
			'url'=>'deals/browse/offers',
			'title'=>'Sale Offers', 
			'class'=>'offers text-success',
			'abbr'=>'So'
		],
		'requests'=>[
			'url'=>'deals/browse/requests',
			'title'=>'Request for Quote',
			'class'=>'requests text-warning',
			'abbr'=>'RFQ'
		],
		'partnerships'=>[
			'url'=>'deals/browse/partnerships',
			'title'=>'Partnership Deals',
			'class'=>'partnerships text-primary',
			'abbr'=>'PD'
		],
	];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function offer() {
		return $this->belongsTo('App\Deals\SellOffer', 'record_id')->with('product','images');
	}

	public function request() {
		return $this->belongsTo('App\Deals\QuoteRequest', 'record_id')->with('images');
	}

	public function partnership() {
		return $this->belongsTo('App\Deals\Partnership', 'record_id')->with('images');
	}

	public static function expiry($item) {
		$today = Carbon::today();
		$expireDate = Carbon::parse($item->expired_at);
		$diff = $today->diffInDays($expireDate, false);

		return $diff;
	}

	public static function expire_format($item) {
		$days = self::expiry($item);
		if($days > 0) {
			return $days. ' '.trans('front.days_left');
		} else if($days==0) {
			return trans('front.ends_today');
		} else {
			return trans('front.expired');
		}
	}

	public static function flag($deal, $size=24)
	{
		if(isset($deal->user->profile->country)) {
			$country = $deal->user->profile->country;
			$country_name = $country->name_english;
			$iso_code = $country->iso2;
			return \HTML::image('flags/'.$size.'/'.$iso_code.'.png',$country_name,['title'=>$country_name]);
		} else {
			return \HTML::image('flags/'.$size.'/EG.png','Egypt',['title'=>'Egypt']);
		}
	}

	public static function deal_date($deal) {
		$date = Carbon::parse($deal->update_at)->format('d M, Y');
		return $date;
	}

	public static function date_on($deal) {
		$date = Date::parse($deal->updated_at)->format('l j F Y');
		return $date;
	}
    /*Desc : get expired time from the db
     * input : refrence from the deal db
     * output: the expired time
     */
    public static function expired_at($deal){
        $date = Date::parse($deal->expired_at)->format('l j F Y');
        return $date;
    }
	public static function get_phone($user, $which='telephone', $code='tel_code') {
		if(!empty($user->profile->telephone)){
			$phones = explode(',', $user->profile->$which);
			$phone_codes = explode(',', $user->profile->$code);
			$phone_list = '';
			foreach($phones as $index=>$no) {
				$code_no = (isset($phone_codes[$index])) ? $phone_codes[$index]:'';
				$phone_list .= $code_no.' '.$no;
			}
			return $phone_list;
		}
		return '';
	}
}