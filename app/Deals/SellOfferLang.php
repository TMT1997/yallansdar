<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class SellOfferLang extends Model
{
    protected $table = 'deals_sell_offers_lang';
    protected $guarded = [];
}
