<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = "deals_quotes";
    protected $guarded = [];
    public static $status = [
        '0'=>'Pending',
        '1'=>'Approved',
        '2'=>'Disapproved',

    ];
    public static $all_status = [
        '0'=>'Pending',
        '1'=>'Approved',
        '2'=>'Disapproved',
        '3'=>'All',
    ];
    public function request() {
    	return $this->belongsTo('App\Deals\QuoteRequest', 'quote_request_id');
    }

    public function user() {
    	return $this->belongsTo(\App\User::class);
    }
    public function user_from() {
        return $this->belongsTo('App\User','from_user_id');
    }
    public function lang() {
        return $this->belongsTo('App\Deals\QuoteRequestLang');
    }
}
