<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;
use App\Helper;

class PartnershipImage extends Model
{
    protected $table = 'deals_partnerships_images';
	protected $guarded = [];

	public static $max_upload_doc_size = 3145728; //Size in Byte (10485760 = 10MB)
    public static $max_files_per_application = 5;
	public static $allowed_types = [
    	'jpg','jpeg','png','gif',
    ];
    public static $allowed_docs_types = [
    	'pdf',
    	'doc','docx','odt',
    	'xls','xlsx',
    	'ppt','pptx','pps','ppsx',
    	'jpg','jpeg','png','gif',
    ];

    public static function upload_multi_images($files_request, $record_id)
    {
        $images = [];
        $upload_path = Helper::upload_path();
        $name_sub = date('Y/m');

        foreach($files_request as $file)
        {
            if(empty($file)) {
                continue;
            }
            //Check file size
            if($file->getClientSize() > self::$max_upload_doc_size) {
                continue;
            }
            //Check if file extension if allowed
            $extension = $file->getClientOriginalExtension();
            if(!in_array($extension, self::$allowed_types)) {
                continue;
            }
            $file_code = rand(1000000000000000000, 9999999999999999999999);
            $fileName = $file_code.'.'.$extension;

            $file->move($upload_path, $fileName);
            if(Helper::setThumbnail(['name'=>$file_code, 'extension'=>$extension], $upload_path)) {
                $uploaded_image = $name_sub.'/'.$fileName;

                //Save to Product Images Table
                $images[] = self::create(['partnership_id'=>$record_id, 'image'=>$uploaded_image]);
            }
        }

        return $images;
    }
}
