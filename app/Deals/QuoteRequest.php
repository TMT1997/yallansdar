<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;
use App\UserStatusReason;
class QuoteRequest extends Model
{
    protected $table = "deals_quote_requests";
    protected $guarded = [];
    public static $request_for = [
        'product' => 'Request for a Product',
        'service' => 'Request for a Service',
    ];

    public static $source_frequency = [
        '' => 'Please Select',
        'One Time' => 'One Time',
        'Weekly' => 'Weekly',
        '2 Weeks Once' => '2 Weeks Once',
        'Monthly' => 'Monthly',
        '3 Months Once' => '3 Months Once',
        '6 Months Once' => '6 Months Once',
        'Yearly' => 'Yearly',
    ];
    public static $quote_req_status = [
        '0' => 'All',
        '1' => 'Approvel Pending',
        '2' => 'Approved',
        '3' => 'Disapproved',
        '4' => 'expired',
        '5' => 'Trashed',
        '6' => 'active',
    ];

    public function lang()
    {
        return $this->hasMany('App\Deals\QuoteRequestLang');
    }

    public function images()
    {
        return $this->hasMany('App\Deals\QuoteRequestImage');
    }

    public function quotes()
    {
        return $this->hasMany('App\Deals\Quote');
    }

    public function user()
    {
        return $this->belongsTo('App\User')->with('profile');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function parent_category()
    {
        return $this->belongsTo('App\Category', 'parent_cat_id');
    }

    public static function url($item)
    {
        return url('فرص-تصدير/' . $item->slug);
    }

    public static function category_url($category)
    {
        if ($category) {
            return url($category->id . '/فرص-تصدير?' . str_slug(\App\Helper::field($category, 'name')));
        }
        return '#';
    }

    public function status_reason() {
        return $this->hasOne(UserStatusReason::class)->with('reason');
    }

    public static function supplier_locations($offer)
    {
        if (!empty($offer->supplier_location)) {
            $offer_suppliers = explode(',', $offer->supplier_location);
            $locations = array();

            foreach ($offer_suppliers as $id) {
                $country = \App\Country::get_country($id, true);
                $country->flag = url('flags/24/' . $country->iso2 . '.png');
                $country->country_icon = $country->flag;
                $locations[] = $country;
            }
            $offer->supplier_location = collect($locations);
        } else {
            $offer->supplier_location = [];
        }

        return $offer->supplier_location;
    }

    public static function get_im_country($offer)
    {
        if (!empty($offer->im_country)) {
            $country = \App\Country::get_country($offer->im_country, true);
            $country->flag = url('flags/24/' . $country->iso2 . '.png');
            $country->country_icon = $country->flag;
            $offer->im_country = $country;
            return $offer->im_country;
        }

        return null;

    }
}
