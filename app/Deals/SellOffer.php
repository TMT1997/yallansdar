<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class SellOffer extends Model
{
    protected $table = "deals_sell_offers";
    protected $guarded = [];
    public static $types = [
    	'discount'=>'Discount on a Product',
    	'clearance'=>'Stock Clearance',
        'Seasonal'=>'Seasonal Product',
    	'service'=>'Offer a Service',
    ];
    public static $offer_for = [
    	'existing'=>'Existing Product',
    	'new'=>'New Product',
    ];

    public static $currencies = [
        ''=>'Select Currency',
        'INR'=>'INR',
        'USD'=>'USD',
        'GBP'=>'GBP',
        'RMB'=>'RMB',
        'EUR'=>'EUR',
        'AUD'=>'AUD',
        'CAD'=>'CAD',
        'CHF'=>'CHF',
        'JPY'=>'JPY',
        'HKD'=>'HKD',
        'NZD'=>'NZD',
        'SGD'=>'SGD',
        'NTD'=>'NTD',
        'Other'=>'Other',
    ];

    public function lang() {
        return $this->hasMany('App\Deals\SellOfferLang');
    }

    public function images() {
        return $this->hasMany('App\Deals\SellOfferImage');
    }

    public function user() {
        return $this->belongsTo('App\User')->with('profile');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function product() {
        return $this->belongsTo('App\Product');
    }

    public static function url($item) {
        return url('deals/offer/'.$item->id);
    }

    public static function prod_name($item) {
        if(isset($item->product)) {
            return \App\Helper::field($item->product,'name');
        }
        return '';
    }
}