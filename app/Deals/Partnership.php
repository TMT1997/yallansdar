<?php

namespace App\Deals;

use Illuminate\Database\Eloquent\Model;

class Partnership extends Model
{
    protected $table = "deals_partnerships";
    protected $guarded = [];
    public static $types = [
    	'propose'=>'Propose a Partnership',
    	'request'=>'Request for Partnership',
    ];
    public static $offer_as = [
    	''=>'Please Select',
    	'Co-owner'=>'Co-owner',
    	'Distributer'=>'Distributer',
    	'Dealer'=>'Dealer',
    	'Representative'=>'Representative',
    	'Franchise'=>'Franchise',
    	'Investor'=>'Investor',
    ];
    public static $request_for = [
    	'product'=>'Product',
    	'service'=>'Service',
    ];

    public function lang() {
        return $this->hasMany('App\Deals\PartnershipLang');
    }

    public function images() {
        return $this->hasMany('App\Deals\PartnershipImage');
    }

    public function user() {
        return $this->belongsTo('App\User')->with('profile');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public static function url($item) {
        return url('deals/partnership/'.$item->id);
    }
}
