<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDeactivate extends Model
{
    protected $guarded = [];
}
