<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public static $adminRules = [
        'name_en'=>'required|min:2',
        'name_ar'=>'required|min:2',
    ];

    public function parent() {
        return $this->belongsTo('App\Category', 'parent_category_id');
    }

    public function childs() {
    	return $this->hasMany('App\Category', 'parent_category_id')->whereActive(1)->whereTrashed(0);
    }

    public function childs_tree() {
    	return $this->hasMany('App\Category', 'parent_category_id')->whereActive(1)->with('childs');
    }

    public static function url($category, $get_parent=false)  {
        if($category) {
            $category_id = ($get_parent) ? $category->parent_category_id: $category->id;
            return url('category/'.$category_id.'/'.str_slug($category->name_en));            
        }
        return '#';
    }

    public static function url_all($category) {
        return url('category/all/'.$category->id.'/'.str_slug($category->name_en));
    }

    public static function url_deals($category)  {
        if($category) {
    	   return url('deals/category/'.$category->id.'/'.str_slug($category->name_en));            
        }
        return '#';
    }

    public static function name($category, $lang='ar') {
    	return ($lang=='ar')? $category->name_ar : $category->name_en;
    }

    public static function image($category, $type='thumb') {
        if($type=='banner') {
            return url('uploads/categories/'.$category->banner);
        }
        return url('uploads/categories/'.$category->image);
    }

    public static function get_tree($parent_id=0, $take=14, $main_nav=true)
    {
        $categories = new self;
        if($main_nav) {
            $categories = $categories->whereMain_nav(1);
        }
        if($take!=0) {
            $categories = $categories->take($take);
        }
        $categories = $categories->whereActive(1)->whereTrashed(0)->whereParent_category_id($parent_id)->orderBy('sort', 'ASC')->with('childs')->get();
        return $categories;
    }

    public static function get_category_childs($parent_id=0, $take=0)
    {
        $categories = new self;
        $categories = $categories->whereActive(1)->whereTrashed(0)->whereParent_category_id($parent_id);
        if($take!=0) {
            $categories = $categories->take($take);
        }
        $categories = $categories->with('childs')->get();

        return $categories;
    }

    public static function get_parents($category_id, $tree=array())
    {
        $category = self::whereId($category_id)->first();
        $tree[] = $category;
        if(!empty($category) && $category->parent_category_id!=0){
            $tree = self::get_parents($category->parent_category_id, $tree);
        }

        return $tree;
    }

    public static function draw_cat_path($parents, $lang=null) 
    {
        if(!$lang) {
            $lang = App::getLocale();
        }
        $output = '';
        $category_id = 0;
        $tree = array_reverse($parents);
        foreach($tree as $cat)
        {
            if(!$cat) {
                continue;
            }
            $category_name = ($lang=='en')? $cat->name_en: $cat->name_ar;
            $category_id = $cat->id;
            $output .= '<strong>'.$category_name.'</strong> >> ';
        }

        $output = '<div class="alert alert-info cat_single_path"><a href="javascript:void(0)" class="cat_set_fsearch" data-id="'.$category_id.'"><span class="cpath"><input type="hidden" name="category_id" value="'.$category_id.'" /> '.$output.'</span></a></div>';
        return $output;
    }

}