<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advs extends Model
{
    protected $table = 'adv_blocks';
    protected $primaryKey = 'id';
    protected $fillable = ['title','subtitle', 'link', 'type', 'image', 'content', 'is_linked'];
}
