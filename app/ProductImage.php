<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helper;
use App\Media;
use Auth;
use Form;

class ProductImage extends Model
{
	protected $table = 'product_images';
	protected $guarded = [];

	public static $max_upload_doc_size = 3145728; //Size in Byte (10485760 = 10MB)
    public static $max_files_per_application = 5;
	public static $allowed_types = [
    	'jpg','jpeg','png','gif',
    ];

    public static $allowed_docs_types = [
    	'pdf',
    	'doc','docx','odt',
    	'xls','xlsx',
    	'ppt','pptx','pps','ppsx',
    	'jpg','jpeg','png','gif',
    ];

    public static function upload_multi_images($files_request, $product_id)
    {
        $images = [];
        $upload_path = Helper::upload_path();
        $name_sub = date('Y/m');

        foreach($files_request as $file)
        {
            if(empty($file)) {
                continue;
            }
            //Check file size
            if($file->getClientSize() > self::$max_upload_doc_size) {
                continue;
            }
            //Check if file extension if allowed
            $extension = $file->getClientOriginalExtension();
            if(!in_array($extension, self::$allowed_types)) {
                continue;
            }
            $file_code = rand(10000000, 99999999);
            $fileName = $file_code.'.'.$extension;

            $file->move($upload_path, $fileName);
            if(Helper::setThumbnail(['name'=>$file_code, 'extension'=>$extension], $upload_path)) {
                $uploaded_image = $name_sub.'/'.$fileName;

                //Save to Product Images Table
                $images[] = self::create(['product_id'=>$product_id, 'image'=>$uploaded_image]);

                //Save uploaded image to media
                $media = Media::create([
                    'user_id'=>Auth::id(), 
                    'type'=>'images',
                    'media_album_id'=>1,
                    'file'=>$uploaded_image,
                    'name'=>'no_name'
                ]);
            }
        }

        return $images;
    }

    public static function generate_preview($images, $section, $delete_btn=true)
    {
        $output = '';
        if(!$images) {
            return $output;
        }

        if(isset($images)):
            foreach($images as $item):
                $delete_ctrl = ($delete_btn) ? Form::button('<i class="fa fa-trash" title="Delete file"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\ProductImage").'","image")']) : '';
                $output.='
                <div class="col-xs-6 col-sm-4 col-md-3 infile">
                    <div class="thumbnail">
                        '.Helper::image($item->image,['style'=>'max-height:60px']).'
                        <div class="caption">
                            <p>'.$item->title.'</p> '.$delete_ctrl.'
                        </div>
                    </div>
                </div>';
            endforeach;
        endif;

        return '<div class="row">'.$output.'</div>';
    }
}
