<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersOld extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'users';
    protected $guarded = [];
}
