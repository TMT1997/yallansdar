<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWebsitePage extends Model
{
	protected $table = "user_website_pages";
    protected $guarded = [];

    public function website() {
    	return $this->belongsTo('App\UserWebsite');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
