<?php

namespace App\Packages;

use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    protected $table = 'user_packages';
    protected $guarded = [];

    public static $status = [
        0=>'Not Confirmed',
        1=>'Confirmed',
    ];

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function package() {
    	return $this->belongsTo('App\Packages\Package');
    }
}
