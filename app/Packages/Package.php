<?php

namespace App\Packages;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';
    protected $guarded = [];
    public static $periods = [
    	'Annually'=>'Annually',
    	'Monthly'=>'Monthly',
    ];

    public static $sections = [
    	'General'=>'General',
    	'Product Presentation & Branding'=>'Product Presentation & Branding',
    	'Promotion'=>'Promotion',
    	'Buyer Trust'=>'Buyer Trust',
    	'Business Tools'=>'Business Tools',
    	'Buyer Info Access'=>'Buyer Info Access',
    	'Customer Support'=>'Customer Support',
    ];

    public function items() {
    	return $this->hasMany('App\Packages\PackageItem');
    }


    public static function packages_with_items()
    {
        $items = PackageItem::whereActive(1)->get();
        $packages = self::whereActive(1)->orderBy('sort_order', 'ASC')->get();

        foreach($packages as $package)
        {
            $pack_item = [];
            foreach ($items as $item)
            {
                $item_vals = unserialize($item->values);
                $item_in = $item_vals[$package->id];

                $pack_item[$item->code] = [
                    'name_ar'=>$item->name_ar,
                    'name_en'=>$item->name_en,
                    'item_data'=>$item_in,
                ];
            }
            $package['items'] = $pack_item;
        }

        return $packages;
    }
}
