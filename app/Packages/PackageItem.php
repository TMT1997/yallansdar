<?php

namespace App\Packages;

use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    protected $table = 'package_items';
    protected $guarded = [];

    public static $values = [
    	''=>'Select',
    	'0'=>'0',
        '1'=>'1',
        '10'=>'10',
        'Yes'=>'Yes',
    	'No'=>'No',
    	'Unlimited'=>'Unlimited',
    	'Standard'=>'Standard',
    	'Templates'=>'Templates',
    	'Customized'=>'Customized',
    	'Market place'=>'Market place',
    	'Exclusive'=>'Exclusive',
    	'Sub domain Website'=>'Sub domain Website',
    	'Own Domain Website'=>'Own Domain Website',
    	'Basic'=>'Basic',
    	'Advanced'=>'Advanced',
    	'Premium'=>'Premium',
    	'Level 1'=>'Level 1',
    	'Level 2'=>'Level 2',
    	'Level 3'=>'Level 3',
    	'Monthly once'=>'Monthly once',
    ];

    public function package() {
    	return $this->belongsTo('App\Packages\Package');
    }
}
