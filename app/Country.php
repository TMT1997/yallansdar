<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Country extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public static function cname($country_id)
    {
        if(Cache::has('countries')) {
            $country = Cache::get('countries')->filter(function($row) use ($country_id){
                return $row->id==$country_id;
            })->first();

            if($country) {
                return $country->name;                
            }
            return 'NA';
        }

        return self::find($country_id)->english_name;
    }

    public static function get_country($country_id, $get_db=false)
    {
        if(!$get_db)
        {
        	if(Cache::has('countries')) {
        		$country = Cache::get('countries')->filter(function($row) use ($country_id){
        			return $row->id==$country_id;
        		})->first();

        		return $country;
        	}
        }

    	return self::find($country_id);
    }

    public static function get_country_code($country_name)
    {
       $call_code = Country::whereCountry_name($country_name)->get('calling_code');

        return $call_code;
    }
    

    public static function flag($country, $attr=[])
    {
        if($country) {
            $country_name = $country->name;
            $iso_code = $country->iso2;
            $attr['title'] = $country->name;
            return \HTML::image('flags/24/'.$iso_code.'.png',$country_name,$attr);
        }
        return '';
    }

    public static function flag_by_id($country_id, $attr=[], $with_name=false, $size=24)
    {
        if(isset($country_id) && !empty($country_id)) {

            $country = Country::get_country($country_id);
            if($country) {
                $country_name = $country->name;
                $iso_code = $country->iso2;
                $attr['title'] = $country->name;

                $str = \HTML::image('flags/'.$size.'/'.$iso_code.'.png',$country_name,$attr);
                if($with_name) {
                    return '<i class="flag">'.$str.'</i> <span class="flgname">'.$country_name.'</span>';
                }                
                return $str;
            }
        } else {
            return \HTML::image('flags/'.$size.'/EG.png','Egypt',['title'=>'Egypt']);
        }
    }
}
