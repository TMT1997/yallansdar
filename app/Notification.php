<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = [];

    public static $NOTIFICATION_EVENTS = [
        'Add_Product'		=>['key' =>'Add_Product' ,'route' => 'admin/products/' , 'value_en' => 'New Product Added' , 'value_ar'=>'اضافة منتج جديد'],
        'Add_User'			=>['key' =>'Add_User' ,'route' => 'admin/profiles/profile/' , 'value_en' => 'New User Added' , 'value_ar'=>'اضافة عضو جديد'],
        'Profile_Completed'	=>['key' =>'Profile_Completed' , 'route' => 'admin/profiles/profile/' , 'value_en' => 'Profile is Completed' , 'value_ar'=>'تم اكتمال بروفايل'],
        'Select_Package'	=>['key' =>'Select_Package' , 'route' => Null , 'value_en' => 'User Select a Package' , 'value_ar'=>'تم اختيار باقة جديدة'],
        'Rfq_Seen'			=>['key' =>'Rfq_Seen' , 'route' => Null , 'value_en' => 'User See a new Rfq' , 'value_ar'=>'تم مشاهدة فرص تصدير'],
        'Member_Message'	=>['key' =>'Member_Message' ,'route' => Null , 'value_en' => 'Contact Betweem Users' , 'value_ar'=>'تواصل بين عضوين'],
    ];

}
