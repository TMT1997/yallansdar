<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';
    protected $guarded = [];

    public static $rules = [
        'title_ar' => 'min:5|required',
        'title_en' => 'min:5|required',
    ];

    public static function url($row) {
        if(!$row) {
            return '#';
        }
        if(!empty($row->slug)) {
            return url('blog/'.$row->slug);
        }
        return url('blog/'.$row->id);
    }

    public static function cat_url($row) {
        if(!$row) {
            return '#';
        }
        if(!empty($row->category->slug)) {
            return url('blog/category/'.$row->category->slug);
        }
        return url('blog/category/'.$row->id);
    }

    public static function tag_url($tag) {
        if(!empty($tag)) {
            return url('blog/tag/'.$tag);
        }
        return '#';
    }

    public function category() {
        return $this->belongsTo(BlogCategory::class, 'category_id');
    }

    public function author() {
        return $this->belongsTo(BlogAuthor::class, 'author_id');
    }

    public function comments() {
        return $this->hasMany(Comment::class,'record_id')->whereModel('Blog')->whereActive(1)->orderBy('id','DESC');
    }
}
