<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $primaryKey = 'id';
    protected $fillable = ['tag','title', 'subtitle', 'slug', 'image','banner', 'details','url', 'pdf_file','pdf_file_title','active', 'addtomenu','sort'];
    
    public static $tags = [
    	'about'=>'About Us',
        'articles'=>'Articles',
        'faq'=>'FAQ',
    ];

    public static $page_docs = array('image','banner','menuimage');
    public static function url($page) {
        return url('y/'.$page->id.'/'.str_slug($page->title));
    }

    public static function getTag($tag, $collection, $single=false)
    {
        if(!$collection) {
            return false;
        }
        $output = $collection->filter(function($row) use ($tag) {
            return $row->tag == $tag;
        });
        if($single) {
            $output = $output->first();
        }
        return $output;
    }
}