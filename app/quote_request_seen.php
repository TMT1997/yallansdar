<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class quote_request_seen extends Model
{
    protected $table = 'quote_request_seens';
    protected $primaryKey = 'id';
    protected $fillable = ['id','user_id', 'quoute_req_id'];    
}