<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailBulk extends Model
{
    protected $table = 'emails_bulk';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public static $email_verified_filter = [
        'all'=>'الكل',
        '1'=>'المشتركين ذو الحساب المفعل',
        '0'=>'المشتركين ذو الحساب الغير مفعل',
    ];

    public static $products_filter = [
    	'all'=>'الكل',
    	'1'=>'من لديهم منتجات',
    	'0'=>'من ليس لديهم منتجات'
    ];
}
