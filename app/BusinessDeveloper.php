<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessDeveloper extends Model
{
    protected $guarded = [];
}
