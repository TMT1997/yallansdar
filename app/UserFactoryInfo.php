<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFactoryInfo extends Model
{
    protected $table = 'user_factory_info';
	protected $guarded = [];

    public static $array_fields = [
        'contract_manufacturings',
        'annual_product_name',
        'annual_qty',
        'annual_unit',
    ];

    public static $factory_size = [
        ''=>'Please Select',
        'below 1000 square meters'=>'below 1000 square meters',
        '1000 - 5000 square meters'=>'1000 - 5000 square meters',
        '5000 -10000 square meters'=>'5000 -10000 square meters',
        '10000 -50000 square meters'=>'10000 -50000 square meters',
        '50000 -100000 square meters'=>'50000 -100000 square meters',
        'above 100000 square meters'=>'above 100000 square meters',
    ];

    public static $contract = [
    	'OEM Service'=>'OEM Service',
    	'ODM Service'=>'ODM Service',
    	'Private Label'=>'Private Label',
    	'Sub-Contractor'=>'Sub-Contractor',
    ];

    public static $qc_staff_no = [
	     ''=>'Please Select',
	     'Less than 5 People'=>'Less than 5 People',
	     '5 - 10 People'=>'5 - 10 People',
	     '11 - 20 People'=>'11 - 20 People',
	     '21 - 30 People'=>'21 - 30 People',
	     '31 - 40 People'=>'31 - 40 People',
	     '41 - 50 People'=>'41 - 50 People',
	     'Above 50 People'=>'Above 50 People',
    ];

    public static $annual_output_value = [
		''=>'Please Select',
		'Below Rs.1 Crore (Below US$2,00,000)'=>'Below Rs.1 Crore (Below US$2,00,000)',
		'Rs. 1 Crore - Rs. 5 Crore (US$2,00,000 - US$ 1 Million )'=>'Rs. 1 Crore - Rs. 5 Crore (US$2,00,000 - US$ 1 Million )',
		'Rs. 5 Crore - Rs. 50 Crore (US$1 Million- US$ 10 Million )'=>'Rs. 5 Crore - Rs. 50 Crore (US$1 Million- US$ 10 Million )',
		'Rs. 50 Crore - Rs. 500 Crore (US$10 Million- US$ 100 Million )'=>'Rs. 50 Crore - Rs. 500 Crore (US$10 Million- US$ 100 Million )',
		'Above Rs. 500 Crore (Above US$100 Million)'=>'Above Rs. 500 Crore (Above US$100 Million)',
    ];

    public function docs() {
        return $this->hasMany('App\UserProfileDoc', 'user_profile_id')->whereProfile('factory');
    }

    /* Initialize an empty profile for user including Temp, Perma */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}
    	$profile_temp = [
    		'user_id'=>$user->id,
    		'profile'=>'temporary',
    		'status'=>1, //approved
		];
    	$user_profile_temp = self::create($profile_temp);
    	$profile_perma = $profile_temp;
		$profile_perma['profile'] = 'permanent';
        $user_profile_perma = self::create($profile_perma);

		return $user_profile_temp;
    }
}
