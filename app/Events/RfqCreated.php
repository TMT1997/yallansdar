<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Deals\QuoteRequest;

class RfqCreated extends Event
{
    use SerializesModels;

    public $rfq;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(QuoteRequest $rfq)
    {
        $this->rfq = $rfq;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
