<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public static $dashboard_main = [
    	'dashboard/messages'=>['class'=>'mssgs-tab','label'=>'Messages'],
    	'dashboard/profile'=>['class'=>'pofl-tab','label'=>'Profile'],
    	'dashboard/products'=>['class'=>'prdcts-tab','label'=>'Products'],
        'dashboard/services'=>['class'=>'services-tab','label'=>'Services'],
    	'dashboard/deals'=>['class'=>'dl-tab','label'=>'Deals'],
    	'dashboard/website'=>['class'=>'ws-tab','label'=>'Website', 'disabled'=>false],
//    	'dashboard/promots'=>['class'=>'promt-tab','label'=>'Promots', 'disabled'=>true],
//    	'dashboard/listings'=>['class'=>'lstng-tab','label'=>'Listings', 'disabled'=>true],
//    	'dashboard/analytics'=>['class'=>'anltcs-tab','label'=>'Analytics', 'disabled'=>true],
//    	'dashboard/support'=>['class'=>'suprt-tab','label'=>'Support', 'disabled'=>true],
//        'dashboard/ppp'=>['class'=>'ppp-tab','label'=>'PPP', 'disabled'=>true],
    	'dashboard/settings'=>['class'=>'stngs-tab','label'=>'Settings'],
    ];

    public static $settings_sidebar = [
        'header'=>'My Settings',
        'items'=>[
            'settings'=>['icon'=>'adprdct-btn','label'=>'Account Settings'],
            'settings/account-verification'=>['icon'=>'mngprdct-btn','label'=>'Account Verification'],
            'reviews'=>['icon'=>'mnggrp-btn','label'=>'Reviews & Feedback'],
        ]
    ];

    public static $profile_sidebar = [
        'header'=>'My Profile',
        'items'=>[
            'profile'=>['icon'=>'adprdct-btn','label'=>'Profile'],
            'profile/trade'=>['icon'=>'mngprdct-btn','label'=>'Trade Information'],
            'profile/factory'=>['icon'=>'mngprdct-btn','label'=>'Factory Information'],
            'profile/registration'=>['icon'=>'mnggrp-btn','label'=>'Registration Details'],
        ]
    ];

    public static $products_sidebar = [
        'header'=>'My Products',
        'items'=>[
            'products/create'=>['icon'=>'adprdct-btn','label'=>'Add Product'],
            'products'=>['icon'=>'mngprdct-btn','label'=>'Manage Products'],
            'groups'=>['icon'=>'mnggrp-btn','label'=>'Groups'],
            'media'=>['icon'=>'mdybnk-btn','label'=>'Media'],
        ]
    ];

    public static $services_sidebar = [
		'header'=>'My Services',
		'items'=>[
			'services/create'=>['icon'=>'adprdct-btn','label'=>'Add Service'],
			'services'=>['icon'=>'mngprdct-btn','label'=>'Manage Services'],
			'groups'=>['icon'=>'mnggrp-btn','label'=>'Groups'],
			'media'=>['icon'=>'mdybnk-btn','label'=>'Media'],
		]
	];

    public static $deals_sidebar = [
    	'header'=>'My Deals',
		'items'=>[
			'sell_offers/create'=>['icon'=>'adprdct-btn','label'=>'Post Sale Offer'],
			'request_quotes/create'=>['icon'=>'mngprdct-btn','label'=>'Request for Quote'],
			'partnerships/create'=>['icon'=>'mnggrp-btn','label'=>'Partnership Deals'],
			'deals'=>['icon'=>'mdybnk-btn','label'=>'Manage My Deals'],
			'alerts/create'=>['icon'=>'mdybnk-btn','label'=>'Create Deal Alert'],
			'alerts'=>['icon'=>'mdybnk-btn','label'=>'Manage Deal Alerts'],
            'quotations'=>['icon'=>'mdybnk-btn','label'=>'Quotations'],
		]
    ];

    public static $messages_sidebar = [
        'header'=>'My Messages',
        'items'=>[
            'messages/compose'=>['icon'=>'adprdct-btn','label'=>'Compose'],
            'messages/inbox'=>['icon'=>'mngprdct-btn','label'=>'Inbox'],
            'messages/sent'=>['icon'=>'mnggrp-btn','label'=>'Sent'],
            'messages/draft'=>['icon'=>'mdybnk-btn','label'=>'Draft'],
            'messages/spam'=>['icon'=>'mdybnk-btn','label'=>'Spam'],
            'messages/trash'=>['icon'=>'mdybnk-btn','label'=>'Trash'],
            'separator'=>'',
            'folders'=>['icon'=>'mdybnk-btn','label'=>'My Folders'],
            'contacts'=>['icon'=>'mdybnk-btn','label'=>'My Contacts'],
        ]
    ];

    public static $admin_menu = [
        's1'=>['icon'=>'sep','label'=>'main_menu'],
        'admin/categories'=>['icon'=>'file','label'=>'categories'],
        'admin/products'=>['icon'=>'file','label'=>'products'],
        'admin/services'=>['icon'=>'file','label'=>'services'],
        'admin/deals'=>['icon'=>'file','label'=>'deals'],
        'admin/userwebsites'=>['icon'=>'globe','label'=>'userwebsites'],
        'admin/reviews'=>['icon'=>'smile-o','label'=>'reviews'],
        //'admin/profiles'=>['icon'=>'smile-o','label'=>'profile'],
        
        's2'=>['icon'=>'sep','label'=>'members'],
        'members_menu'=>true,

        's5'=>['icon'=>'sep','label'=>'messages'],
        'admin/messages'=>['icon'=>'envelope-o','label'=>'messages'],
        'admin/compose'=>['icon'=>'envelope-o','label'=>'messenger'],
        
        's3'=>['icon'=>'sep','label'=>'content'],
        'admin/templates'=>['icon'=>'tv', 'label'=>'templates'],
        'admin/ads'=>['icon'=>'file', 'label'=>'ads'],
        'admin/pages'=>['icon'=>'file', 'label'=>'pages'],
        'admin/sliders'=>['icon'=>'file', 'label'=>'sliders'],
        'admin/packages'=>['icon'=>'file', 'label'=>'packages'],
        'admin/package-items'=>['icon'=>'file', 'label'=>'package_items'],
        'admin/testimontials'=>['icon'=>'comment-o', 'label'=>'testimontials'],
        'admin/countries'=>['icon'=>'flag-o', 'label'=>'countries'],
        'admin/product_reasons'=>['icon'=>'book', 'label'=>'product_reasons'],
        'admin/advertisement'=>['icon'=>'file', 'label'=>'my_advs'],
        
        's6'=>['icon'=>'sep','label'=>'blog'],
        'admin/blog'=>['icon'=>'bold','label'=>'blog'],
        'admin/blog_authors'=>['icon'=>'bold','label'=>'blog_authors'],
        'admin/blog_categories'=>['icon'=>'bold','label'=>'blog_categories'],
        
        's4'=>['icon'=>'sep','label'=>'settings'],
        'admin/settings'=>['icon'=>'cog', 'label'=>'settings'],
        'admin/admins'=>['icon'=>'user', 'label'=>'admins'],
    ];

    public static $admin_access_menu = [
        '1'=>['icon'=>'sep','label'=>'admin.services', 'menu'=>[
                'admin/business-developers'=>['icon'=>'eye', 'label'=>'front.business_developers', 'user_counts'=>'business_developers'],
                'admin/trade-managers'=>['icon'=>'user', 'label'=>'front.trade_managers', 'user_counts'=>'trade_managers'],
                'admin/pick-describes'=>['icon'=>'user', 'label'=>'front.pick_describe', 'user_counts'=>'pick_describes'],
            ],
        ],
        '2'=>['icon'=>'sep','label'=>'admin.members', 'menu'=>[
                'admin/users'=>['icon'=>'eye', 'label'=>'front.all_users', 'user_counts'=>'all'],
                'admin/users?type=importer'=>['icon'=>'file', 'label'=>'front.importer', 'user_counts'=>'importer'],
                'admin/users?type=exporter'=>['icon'=>'file', 'label'=>'front.exporter', 'user_counts'=>'exporter'],
                'admin/users?type=service_provider'=>['icon'=>'file', 'label'=>'front.service_provider', 'user_counts'=>'service_provider'],
            ],
        ],
        '3'=>['icon'=>'sep','label'=>'admin.messages', 'menu'=>[
                'admin/messages'=>['icon'=>'inbox', 'label'=>'dash.inbox'],
                'admin/compose'=>['icon'=>'share-square', 'label'=>'dash.sent'],
                'admin/compose/bulk'=>['icon'=>'share-square', 'label'=>'dash.send_message'],
            ],
        ],
        '4'=>['icon'=>'sep','label'=>'admin.content', 'menu'=>[
                'admin/blog'=>['icon'=>'bold', 'label'=>'admin.blog'],
                'admin/pages'=>['icon'=>'file', 'label'=>'admin.pages'],
                'admin/packages'=>['icon'=>'file', 'label'=>'admin.packages'],
                'admin/countries'=>['icon'=>'bold', 'label'=>'admin.countries'],
                'admin/categories'=>['icon'=>'file', 'label'=>'admin.categories'],
                'admin/blog_categories'=>['icon'=>'file', 'label'=>'admin.blog_categories'],
                'admin/advertisement'=>['icon'=>'file', 'label'=>'admin.my_advs'],
                'admin/templates'=>['icon'=>'file', 'label'=>'admin.templates'],
                'admin/sliders'=>['icon'=>'file', 'label'=>'admin.sliders'],
            ],
        ],
        '5'=>['icon'=>'sep','label'=>'admin.activity_member', 'menu'=>[
                'admin/products'=>['icon'=>'bold', 'label'=>'admin.products'],
                'admin/deals'=>['icon'=>'file', 'label'=>'admin.deals'],
                'admin/services'=>['icon'=>'file', 'label'=>'admin.services'],
                'admin/userwebsites'=>['icon'=>'globe', 'label'=>'admin.userwebsites'],
                'admin/package_upgrades'=>['icon'=>'smile-o', 'label'=>'dash.upgrade'],
                'admin/reviews'=>['icon'=>'smile-o', 'label'=>'admin.reviews'],
                'admin/testimontials'=>['icon'=>'comment-o', 'label'=>'admin.testimontials'],
                'admin/profiles'=>['icon'=>'user', 'label'=>'dash.profile'],
            ],
        ],
        '6'=>['icon'=>'sep','label'=>'admin.settings', 'menu'=>[
                'admin/admins'=>['icon'=>'user', 'label'=>'admin.admins'],
                'admin/settings'=>['icon'=>'cog', 'label'=>'admin.settings'],
                'admin/adv_general'=>['icon'=>'cog', 'label'=>'dash.adv'],
            ],
        ],
        '7'=>['icon'=>'sep','label'=>'admin.privilege', 'menu'=>[
            'admin/privilege/add-user'=>['icon'=>'user', 'label'=>'admin.add_new_user'],
            'admin/privilege/add-product'=>['icon'=>'user', 'label'=>'admin.add_new_product'],
            'admin/privilege/add-rfq'=>['icon'=>'user', 'label'=>'admin.add_new_rfq'],
            'admin/privilege/add-service'=>['icon'=>'user', 'label'=>'admin.add_new_service'],
            ],
        ],
    ];
}
