<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adv extends Model
{
    protected $table = 'advs';
    protected $primaryKey = 'id';
    protected $fillable = ['link','image', 'type'];
}
