<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'templates';
    protected $primaryKey = 'id';
    protected $guarded = [];
	
	public static $skins = [
		'basic'=>'Free',
		'default'=>'Default الافتراضي',
		'black'=>'Black أسود',
		'olive'=>'Olive زيتوني',
		'wood'=>'wood خشبي',
	];

	public static $folders = [
		'default'=>'default',
		'globals'=>'globals',
		'premium'=>'premium',
	];
	public static function product_url($product, $domain=null) {
        return url('store/'.$domain.'/product?id='.$product->id.'&'.$product->slug);
    }    
}
