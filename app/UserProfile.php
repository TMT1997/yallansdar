<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserProfile extends Model
{
	protected $guarded = [];

    public static $status = [
        '-1'=>'saved_only', 
        '0'=>'pending', 
        '1'=>'approved', 
        '2'=>'disapproved',
    ];
    public static $fields_arr = [
        'user_type'=>'explode',
        'tel_code'=>'explode',
        'telephone'=>'explode',
        'mob_code'=>'explode',
        'mobile'=>'explode',
        'business_type'=>'explode',
        'preferred_supplier_type'=>'explode',
        'certificates'=>'explode',
        'main_products'=>'explode',
        'other_products'=>'explode',
        'website'=>'explode',
        'product_we_source'=>'explode',
        'quantity'=>'explode',
        'qtyunit'=>'explode'

    ];
    public static $fields_upload = [
        'picture'=>'image',
        'co_logo'=>'image',
        'co_brochure'=>'document',
        'identity_proof'=>'document',
        'identity_proof2'=>'document',
    ];

	public static $profile_tabs = [
        '1'=>['Personal Profile','dashboard.profile.forms.personal'],
        '2'=>['Company Profile','dashboard.profile.forms.company'], 
    ];

    public static $admin_profile_tabs = [
        '1'=>['Personal Profile','admin.profiles.tabs.personal'],
        '2'=>['Company Profile','admin.profiles.tabs.company'],
    ];

    public static $front_profile_tabs = [
        '1'=>['Company Profile','front.products.company.company'],
        '2'=>['Sourcing Information','front.products.company.sourcing'],
        '3'=>['Fact Details','front.products.company.fact'],
        '4'=>['Profile Details','front.products.company.details'],
    ];
    public static $website_profile_tabs = [
		'1'=>['Personal Profile','dashboard.website.steps.profile.personal'],
		//'2'=>['Company Profile','dashboard.website.steps.profile.company'],
	];

    public static $department = [
        ''=>'Please Select',
        '1'=>'Sales &amp; Marketing',
        '2'=>'Purchasing',
        '3'=>'Technical &amp; Engineering',
        '4'=>'Administrative',
        '5'=>'Management',
    ];

    public static $identity_proof = [
        ''=>'Please Select',
        '1'=>'Company Registration',
        '2'=>'Sales/Service Tax Registration',
        '3'=>'Partnership Deed',
        '4'=>'Income Tax Proof',
        '5'=>'PAN Card',
        'other'=>'Others',
    ];

    public static $buying_frequency = [
      ''=>'Select Buying Frequency',
      '< 1 Month'=>'&lt; 1 Month',
      'Monthly'=>'Monthly',
      'Quartely'=>'Quartely',
      'Half-yearly'=>'Half-yearly',
      'Yearly'=>'Yearly',
      '> 1 year'=>'&gt; 1 year',
    ];

    public static $certificates = [
        'ISO 9001:2000'=>'ISO 9001:2000',
        'ISO 9001:2008'=>'ISO 9001:2008',
        'QS-9000'=>'QS-9000',
        'ISO 14001:2004'=>'ISO 14001:2004',
        'ISO/TS 16949'=>'ISO/TS 16949',
        'SA8000'=>'SA8000',
        'ISO 17799'=>'ISO 17799',
        'HACCP'=>'HACCP',
        'OHSAS 18001'=>'OHSAS 18001',
        'TL 9000'=>'TL 9000',
        'Others'=>'Others',        
    ];

    public static $employees_no = [
        '','Please Select',
        '1 -  9 People'=>'1 -  9 People',
        '10 - 49 People'=>'10 - 49 People',
        '50 - 249 People'=>'50 - 249 People',
        '250 - 499 People'=>'250 - 499 People',
        '500 - 999 People'=>'500 - 999 People',
        'Above 1000 People'=>'Above 1000 People',
    ];

    public static $turnover = [
        '','Please Select',
        'Upto Rs. 50 Lakh ( or Upto USD 100 K)'=>'Upto Rs. 50 Lakh ( or Upto USD 100 K)',
        'Rs. 50 Lakh - 5 Crore ( or USD 100 K - 1 Million)'=>'Rs. 50 Lakh - 5 Crore ( or USD 100 K - 1 Million)',
        'Rs. 5 - 50 Crore ( or USD 1 Million - USD 10 Million)'=>'Rs. 5 - 50 Crore ( or USD 1 Million - USD 10 Million)',
        'Rs. 50 - 500 Crore ( or USD 10 Million - USD 100 Million)'=>'Rs. 50 - 500 Crore ( or USD 10 Million - USD 100 Million)',
        'More than Rs. 500 Crore ( or More than USD 100 Million)'=>'More than Rs. 500 Crore ( or More than USD 100 Million)',
    ];

    public static $legal_status = [
        ''=>'Please Select',
        'Public Limited Company'=>'Public Limited Company',
        'Private Limited Company'=>'Private Limited Company',
        'Sole Proprietorship'=>'Sole Proprietorship',
        'Professional Association'=>'Professional Association',
        'Limited Liability Partnership'=>'Limited Liability Partnership',
        'Partnership'=>'Partnership',
        'others'=>'Others',
    ];

    public static $office_size = [
        ''=>'Please Select',
        'below 100 square meters'=>'below 100 square meters',
        '101 - 500 square meters'=>'101 - 500 square meters',
        '501 - 1000 square meters'=>'501 - 1000 square meters',
        '1001 -2000 square meters'=>'1001 -2000 square meters',
        'above 2000 square meters'=>'above 2000 square meters',
    ];

    public static $social = [
        'facebook'=>'Facebook',
        'twitter'=>'twitter',
        'google-plus'=>'Google+',
        'linkedin'=>'linkedin',
        'pinterest'=>'pinterest',
        'youtube'=>'youtube',
    ];
    
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function docs() {
        return $this->hasMany('App\UserProfileDoc')->whereProfile('profile');
    }

    public function offices() {
        return $this->hasMany('App\UserOffice');
    }

    public function country() {
        $name = (\App::getLocale()=='ar') ? 'country_name':'name_english';
        return $this->belongsTo('App\Country')->orderBy($name, 'ASC');
    }

    public function co_country() {
        $name = (\App::getLocale()=='ar') ? 'country_name':'name_english';
        return $this->belongsTo('App\Country', 'co_country_id')->orderBy($name, 'ASC');
    }
    public function business_category() {
        return $this->belongsTo('App\Category', 'business_category');
    }

    /* Initialize an empty profile for user including Temp, Perma */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}

    	$profile_temp = [
    		'user_id'=>$user->id,
    		'profile'=>'temporary',
    		'status'=>1, //approved
    		'first_name'=>$user->name,
    		'mobile'=>$user->mobile,
    		'primary_email'=>$user->email,
            'editable'=>1
		];

    	$user_profile_temp = self::create($profile_temp);

    	$profile_perma = $profile_temp;
		$profile_perma['profile'] = 'permanent';
        $user_profile_perma = self::create($profile_perma);

		return $user_profile_temp;
    }
}
