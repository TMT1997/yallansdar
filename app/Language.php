<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Language extends Model
{
    public static $lang_session = 'yalla_locale';

    public static $langs = [
        'ar'=>['id'=>2, 'icon'=>'EG', 'iso2'=>'ar', 'name'=>'العربية'],
        'en'=>['id'=>1, 'icon'=>'GB', 'iso2'=>'en', 'name'=>'English'],
    ];

    public static function switch_language($locale='en')
    {
        if (!empty($locale))
        {
            $lang = static::where('iso2', $locale)->first();

            if ($lang->count() > 0)
            {
                Session::remove(self::$lang_session, $lang->iso2);
                Session::put(self::$lang_session, $lang->iso2);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function set_language($symbol='en', $sess_name='yalla_locale')
    {
        if(!$sess_name) {
            $sess_name = self::$lang_session;
        }

        $lang = static::where('iso2', $symbol)->first();
        if($lang) {
            Session::put($sess_name, $lang);
        }
        return $lang;
    }

    public static function switch_url($iso2='en') {
        return url('lang/switch/'.$iso2);
    }
}
