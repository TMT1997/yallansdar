<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaAlbum extends Model
{
    protected $table = 'media_albums';
    protected $guarded = [];

    public function media() {
    	return $this->hasMany('App\Media');
    }
}