<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HTML;

class Service extends Model
{
    protected $table = 'services';
	protected $guarded = [];
	public static $rules = ['name'=>'required' , 'locations'=>'required'];

	public function category() {
		return $this->belongsTo('App\Category');
	}

	public function user() {
		return $this->belongsTo('App\User')->with('company');
	}

	public function lang() {
		return $this->hasMany('App\ServiceLang');
	}

	public function images() {
		return $this->hasMany('App\ServiceImage');
	}

    public static function url($service) {
    	return url('services/show/'.$service->id.'/'.str_slug($service->name));
    }

    public static function print_url($service) {
    	return url('services/print/'.$service->id.'/'.str_slug($service->name));
    }

    public static function name($service) {
    	return $service->name;
    }

    public static function Image($service, $attr=[]) {
    	return Helper::image2($service->image, $attr);
    }
}
