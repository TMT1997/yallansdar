<?php

namespace App\Messages;

use Illuminate\Database\Eloquent\Model;
use Mail;
use Carbon\Carbon;
use App\Settings;
use App\User;

class Message extends Model
{
    protected $guarded = [];

    public function contact() {
    	return $this->belongsTo('App\Messages\Contact');
    }

    public function user_from() {
        return $this->belongsTo('App\User', 'from_user_id');
    }

    public function user_to() {
        return $this->belongsTo('App\User', 'to_user_id');
    }

    public function attachments() {
        return $this->hasMany('App\Messages\Attachment');
    }

    public static function date($message)  {
        return Carbon::parse($message->created_at)->format('M. d, Y h:i a');
    }

    public static function since($message)  {
        return Carbon::parse($message->created_at)->diffForHumans();
    }

    public static function url($message) {
    	return url('dashboard/messages/show/'.$message->id.'/'.str_slug('$message->subject'));
    }

    public static function starred($message) {
    	$icon = ($message->star==1)? 'star':'star-o';
    	return '<a href="#" class="start_msg" data-id="'.$message->id.'"><i class="fa fa-'.$icon.'"></i></a>';
    }

    public static function contact_name($message) {
    	$name = (isset($message->contact)) ? $message->contact->first_name : $message->to_name;
    	return $name;
    }

    public static function from($message) {
        $name = '';
        if(isset($message->user_from)) {
            $name = $message->user_from->name;            
        } else if(isset($message->contact)) {
            $name = (isset($message->contact)) ? $message->contact->first_name : $message->to_name;
        } else if(!empty($message->from_name)) {
            $name = $message->from_name . ' ('.$message->from_email.')';
        } else {
            $name = '<Unknown>';
        }
        return $name;
    }

    public static function star_sw($object, $field='active', $model=null)
    {
        $color = '';
        $icon = 'star-o';

        $val = $object->$field;
        if($val == 1) {
            $color = 'text-warning';
            $icon = 'star';
        }

        return '<span><a href="javascript:void(0)" class="'.$color.' btn_starred elem'.$field.$object->id.'" data-id="'.$object->id.'" data-model="'.$model.'" data-field="'.$field.'"><i class="fa fa-'.$icon.'"></i></a></span>';
    }

    public static function notify($user_id, $subject, $body=null, $send_mail=false)
    {
        $messageData = [
            'from_admin'=>1,
            'from_user_id'=>\Auth::id(),
            'to_user_id'=>$user_id,
            'subject' =>$subject,
            'body'=>$body,
        ];

        if(self::create($messageData)) {
            if($send_mail) {
                $user = User::findOrFail($user_id);
            }
            return true;
        }
        return false;
    }

    public static function sendEnquiry($user_id, $user_data=[], $subject, $body=null, $send_mail=false)
    {
        $messageData = [
            'from_admin'=>0,
            'to_user_id'=>$user_id,
            'from_user_id'=>$user_data['from_user_id'],
            'from_email'=>$user_data['email'],
            'from_name'=>$user_data['name'],
            'subject' =>$subject,
            'body'=>$body,
        ];

        $record = self::create($messageData);
        if($record) {
            if($send_mail) {
                $user = User::findOrFail($user_id);
                self::sendMail();
            }
            return $record;
        }
        return false;
    }

    public static function sendMail($contact_message, $reply_message=null)
    {
        $settings = Settings::section('mail');
        $to['email'] = Settings::fetch('email_to', $settings);
        $to['from'] = Settings::fetch('email_from', $settings);
        $to['name'] = Settings::fetch('email_name', $settings);

        $view = 'front.emails.contact.message';
        $subject = 'New Message';

        Mail::send($view, ['contact_message' => $contact_message, 'user'=>$to], function ($m) use ($to, $contact_message) {
            $m->from($to['from'], 'Website');
            $m->to($to['email'], $to['name'])->subject('رسالة جديدة من الموقع');
        });
    }
    public static $status = [
        '0'=>'Pending',
        '1'=>'Approved',
        '2'=>'Disapproved',

    ];
}
