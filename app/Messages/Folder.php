<?php

namespace App\Messages;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = 'message_folders';
    protected $guarded = [];

    public static $validation = [
    	'name'=>'required|max:24'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
