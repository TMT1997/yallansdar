<?php

namespace App\Messages;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 'message_attachments';
    protected $guarded = [];
}
