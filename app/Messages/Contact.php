<?php

namespace App\Messages;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'message_contacts';
    protected $guarded = [];

    public static $validation = [
        'first_name' => 'required',
        'email' => 'required|email',
    ];

    public static $gender = [
        ''=>'Select Gender',
        'Male'=>'Male',
        'Female'=>'Female',
    ];

    public static $business_type = [
        ''=>'Select Business Type',
        'Manufacturer'=>'Manufacturer',
        'Trading Company'=>'Trading Company',
        'Distributor / Wholesaler'=>'Distributor / Wholesaler',
        'Retailer'=>'Retailer',
        'Broker / Consultant / Commission-Based'=>'Broker / Consultant / Commission-Based',
        'Exporter'=>'Exporter',
        'Government Agency'=>'Government Agency',
        'Non Government Agency'=>'Non Government Agency',
        'NGO / Non- Profit Organization'=>'NGO / Non- Profit Organization',
        'Business Services'=>'Business Services',
        'Importer'=>'Importer',
        'Private Sector'=>'Private Sector',
        'Business Sector'=>'Business Sector',
        'Shareholding Company'=>'Shareholding Company',
        'Shipping'=>'Shipping',
        'Training'=>'Training',
        'Marketing'=>'Marketing',
        'Consultations'=>'Consultations',
        'Packaging'=>'Packaging',
        'Sorting Station'=>'Sorting Station',
        'Investement'=>'Investement',
        'Clearance'=>'Clearance',
        'Transfer Contractor'=>'Transfer Contractor',
    ];

    public static $contact_type = [
        'Supplier'=>'Supplier',
        'Buyer'=>'Buyer',
        'Both'=>'Both',
    ];

    public static $source = [
        ''=>'Select Source',
        'Advertisement'=>'Advertisement',
        'Cold Call'=>'Cold Call',
        'Employee Referral'=>'Employee Referral',
        'External Referral'=>'External Referral',
        'OnlineStore'=>'OnlineStore',
        'Partner'=>'Partner',
        'Public Relations'=>'Public Relations',
        'Sales Mail Alias'=>'Sales Mail Alias',
        'Seminar-Internal'=>'Seminar-Internal',
        'Trade Show'=>'Trade Show',
        'Web Download'=>'Web Download',
        'Web Research'=>'Web Research',
        'Chat'=>'Chat',
    ];

    public function user() {
    	return $this->belongsTo('App\User', 'contact_user_id');
    }

    public static function flag($contact) {
    	if(isset($contact->country)) {
    		return '<span>'.$contact->country->country_name.' '.HTML::image('flags/24/'.$contact->country->iso2).'</span>';
    	}
    	return '';
    }
}
