<?php

namespace App\Messages;

use Illuminate\Database\Eloquent\Model;

class Draft extends Model
{
    protected $table = 'message_drafts';
    protected $guarded = [];
}
