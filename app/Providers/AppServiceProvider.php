<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use App\Notification;
use App\Product;
Use App\TemplateSteps;
use App\Packages\UserPackage;
use App\Packages\Package;
use App\quote_request_seen;
use App\Deals\QuoteRequest;
use App\Messages\Message;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //define a user created event to put it in table notification
        User::created(function ($user) {
            Notification::create([
                'section'=>Notification::$NOTIFICATION_EVENTS['Add_User']['key'],
                'user_id'=>$user->id,
                'section_id'=>$user->id,
                'message'=>'User '.$user->name.' Added',
            ]);
        });
        //define a product created event
        Product::created(function ($product) {
            $user = User::whereId($product->user_id)->first();
            Notification::create([
                'section'=>Notification::$NOTIFICATION_EVENTS['Add_Product']['key'],
                'user_id'=>$product->user_id,
                'section_id'=>$product->id,
                'message'=>'Product '.$product->name_ar.' Added',
            ]);
        });
        //When Profile Completed
        TemplateSteps::saved(function ($templates_steps) {
            if($templates_steps->completed)
            {
                $user = User::whereId($templates_steps->user_id)->first();
                Notification::create([
                    'section'=>Notification::$NOTIFICATION_EVENTS['Profile_Completed']['key'],
                    'user_id'=>$templates_steps->user_id,
                    'section_id'=>$templates_steps->user_id,
                    'message'=>'The Registeration Steps Have Been Completed for '.$user->name,
                ]);
            }//end if completed registered
        });
        //When User Selecet a new Package 
        UserPackage::created(function ($package){
            $selected_package = Package::whereId($package->package_id)->first();
            $user = User::whereId($package->user_id)->first();
            Notification::create([
                    'section'=>Notification::$NOTIFICATION_EVENTS['Select_Package']['key'],
                    'user_id'=>$package->user_id,
                    'section_id'=>$package->id,
                    'message'=>'User '.$user->name.' Select a New Package '.$selected_package->name_ar,
            ]);
        });
        //when user see a RFQ
        quote_request_seen::created(function ($rfq_seen){
            $rfq = QuoteRequest::whereId($rfq_seen->quoute_req_id)->first();
            $user = User::whereId($rfq_seen->user_id)->first();
            Notification::create([
                    'section'=>Notification::$NOTIFICATION_EVENTS['Rfq_Seen']['key'],
                    'user_id'=>$rfq_seen->user_id,
                    'section_id'=>$rfq->id,
                    'message'=>'User '.$user->name.' See Rfq '.$rfq->name_ar,
            ]);
        });
        //when User Send message to another
        Message::created(function ($message){
            if($message->from_admin == 0)
            {
                if($message)
                {
                    $user = User::whereId($message->from_user_id)->first();
                    $user_to = User::whereId($message->to_user_id)->first();

                    if($user && $user_to)
                    {
                        Notification::create([
                            'section'=>Notification::$NOTIFICATION_EVENTS['Member_Message']['key'],
                            'user_id'=>$message->from_user_id,
                            'section_id'=>$message->id,
                            'message'=>'User '.$user->name.' Sent message to  '.$user_to->name . ' said '
                            .strip_tags($message->body),
                        ]);
                    }
                }
            }//if message not sent from admin
            else
            {
                if($message->status == 0)
                {
                    $approve = Message::find($message->id);
                    $approve->status = '1';
                    $approve->save();
                }//if pending
            }//if message come from admin
        });
    }//end boot function

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
