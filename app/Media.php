<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $guarded = [];
    public static $types = [
    	''=>'All',
        'images'=>'Images',
    	'documents'=>'Documents',
    ];

    public static $rules = [
    	'media_album_id'=>'required',
        'file' => 'mimes:jpeg,jpg,gif,bmp,png|required|file'
	];

    public function album() {
    	return $this->belongsTo('App\MediaAlbum');
    }

    public static function upload_doc($request, $name)
    {
        $fileName = '';
        $upload_path = \App\Helper::upload_path();
        $file_sub = date('Y/m');

        if ($request->hasFile($name)) {
            $extension = $request->file($name)->getClientOriginalExtension();
            $file_code = time();
            $fileName = $file_code.'.'.$extension;
            $request->file($name)->move($upload_path, $fileName);
        }

        return $file_sub.'/'.$fileName;
    }
}
