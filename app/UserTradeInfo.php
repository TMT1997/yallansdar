<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserTradeInfo extends Model
{
    protected $table = 'user_trade_info';
    protected $guarded = [];

    public static $export_percentage = [
      ''=>'Please Select',
      'Domestic'=>'Domestic',
      '1% - 10%'=>'1% - 10%',
      '11% - 20%'=>'11% - 20%',
      '21% - 30%'=>'21% - 30%',
      '31% - 40%'=>'31% - 40%',
      '41% - 50%'=>'41% - 50%',
      '51% - 60%'=>'51% - 60%',
      '61% - 70%'=>'61% - 70%',
      '71% - 80%'=>'71% - 80%',
      '81% - 90%'=>'81% - 90%',
      '91% - 100%'=>'91% - 100%',
    ];
    public static $market_region = ['Region','Country'];
    public static $regions = [
    	'North America',
    	'Oceania',
    	'Northern Europe',
    	'South America',
    	'Mid East',
    	'Southern Europe',
    	'Eastern Europe',
    	'Eastern Asia',
    	'South Asia',
    	'Southeast Asia',
    	'Western Europe',
    	'Africa',
    	'Central America'
    ];
    public static $avg_lead = [
    	''=>'Please Select',
    	'Months'=>'Months',
    	'Weeks'=>'Weeks',
    	'Days'=>'Days',
    ];

    public static $bool = [
    	'1'=>'Yes',
    	'0'=>'No',
    ];

    public static $no_trade_staff = [
      ''=>'Please Select',
      '&lt; 10 People'=>'&lt; 10 People',
      '10 - 50 People'=>'10 - 50 People',
      '50 - 100 People'=>'50 - 100 People',
      '100 - 1000 People'=>'100 - 1000 People',
      '> 1000 People'=>'&gt; 1000 People',
    ];

    public static $minimum_order_value = [
        ''=>'Please Select',
        '> US$ 100'=>'&gt; US$ 100',
        '> US$ 500'=>'&gt; US$ 500',
        '> US$ 1000'=>'&gt; US$ 1000',
        '> US$ 3000'=>'&gt; US$ 3000',
        '> US$ 5000'=>'&gt; US$ 5000',
        '> US$ 10000'=>'&gt; US$ 10000',
        '> US$ 20000'=>'&gt; US$ 20000',
    ];
    public static $shipment_mode = [
    	'Cargo','Road','Air','Sea'
    ];
    public static $languages = [
    	'English'=>'English',
    	'Arabic'=>'Arabic',
    	'French'=>'French',
        'Chinese'=>'Chinese',
        'Spanish'=>'Spanish',
        'German'=>'German',
        'Russian'=>'Russian',
        'Italian'=>'Italian',
        'Korean'=>'Korean',
        'Hindi'=>'Hindi',
        'Japanese'=>'Japanese',
        'Portuguese'=>'Portuguese',
        'Tamil'=>'Tamil',
    ];

    public static $array_fields = [
    	'main_markets',
    	'region_countries',
    	'nearest_port',
    	'shipment_mode',
    	'accepted_delivery_terms',
    	'accepted_payment_currency',
    	'accepted_payment_type',
    	'language_spoken',
    ];

    public static $delivery_terms = [
        'FOB'=>'Free On Board',
        'CIP'=>'Carriage and Insurance Paid',
        'DDU'=>'Delivered Duty Unpaid',
        'CFR'=>'Cost and Freight',
        'FCA'=>'Free Carrer Responsibilities',
        'Express Delivery'=>'Express Delivery',
        'CIF'=>'Cost Insurance Freight',
        'CPT'=>'Carriage Paid To',
        'DAF'=>'Delivered at Frontier',
        'EXW'=>'Ex Works',
        'DEQ'=>'Delivered Ex Quay',
        'DES'=>'Delivered Ex Ship',
        'FAS'=>'Free Alongside Ship',
        'DDP'=>'Delivered Duty Paid',
        'C&F'=>'Cost and Freight',
    ];

    /* Initialize an empty profile for user including Temp, Perma */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}

    	$profile_temp = [
    		'user_id'=>$user->id,
    		'profile'=>'temporary',
    		'status'=>1, //approved
		];

    	$user_profile_temp = self::create($profile_temp);

    	$profile_perma = $profile_temp;
		$profile_perma['profile'] = 'permanent';
        $user_profile_perma = self::create($profile_perma);

		return $user_profile_temp;
    }
}
