<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWebsite extends Model
{
    protected $guarded = [];
    
	public static $mainmenu = [
        '/'=>'Home',
        'profile'=>'Profile',
        'products'=>'Our Products',
        'contact'=>'Contact Us',
    ];

    public static $status = [
        '1'=>'Approvel Pending',
        '2'=>'Approved',
        '3'=>'Disapproved',
    ];

    public static $status_desc = [
        '0'=>'غير مكتمل',
        '1'=>'الموافقة معلقة',
        '2'=>'تم الموافقة عليه',
        '3'=>'تم رفضه',
    ];

    public static $active_desc = [
	    '0'=>'غير منشور',
        '1'=>'منشور',
	];

    public static $template_default_slides = [
        ['type'=>'Slider', 'image'=>'1.jpg', 'inslider'=>1, 'default'=>1],
        ['type'=>'Slider', 'image'=>'2.jpg', 'inslider'=>1, 'default'=>1],
        ['type'=>'Slider', 'image'=>'3.jpg', 'inslider'=>1, 'default'=>1],

        ['type'=>'Banner', 'title'=>'profile', 'image'=>'profile.jpg', 'default'=>1],
        ['type'=>'Banner', 'title'=>'products', 'image'=>'products.jpg', 'default'=>1],
        ['type'=>'Banner', 'title'=>'contact', 'image'=>'contact.jpg', 'default'=>1],

        ['type'=>'Page', 'tag'=>'profile',      'title'=>'Profile', 'image'=>'', 'default'=>1],
        ['type'=>'Page', 'tag'=>'production',   'title'=>'Production Capacity', 'image'=>'', 'default'=>1],
        ['type'=>'Page', 'tag'=>'trade',        'title'=>'Trade Details', 'image'=>'', 'default'=>1],
    ];

    public function template() {
    	return $this->belongsTo('App\Template');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function pages() {
    	return $this->belongsTo('App\UserWebsitePage');
    }

    public static function url($route=null, $domain) {
        return url('store/'.$domain.'/'.$route);
    }

    public static function website_url($user_website) {
    	return url('store/'.$user_website->domain);
    }

    /* Initialize an empty steps for user */
    public static function init($user) {
        if(!$user) {
            $user = Auth::user();
        }
        $website = [
            'user_id'=>$user->id,
            'active'=>0
        ];
        $user_website = self::create($website);
        return $user_website;
    }

    public static function setDefaultData($website) {
        foreach(self::$template_default_slides as $item) {
            $item['user_id'] = $website->user_id;
            $item['user_website_id'] = $website->id;
            $page = UserWebsitePage::create($item);            
        }

        return true;
    }
}
