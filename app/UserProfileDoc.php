<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helper;
use Form;

class UserProfileDoc extends Model
{
    protected $table = 'user_profile_docs';
    protected $guarded = [];

    public static function generate_preview($docs, $section)
    {
    	$output = '';
        if(!$docs) {
            return $output;
        }

        $files = $docs->filter(function($row) use ($section) {
            return $row->section==$section;
        });
        if(isset($files)):
            foreach($files as $item):
                $output.='
                <div class="col-xs-6 col-sm-4 col-md-3 infile">
                    <div class="thumbnail">
                        '.Helper::image($item->filename,['style'=>'max-height:60px']).'
                        <div class="caption">
                            <p>'.$item->title.'</p>
                            '.Form::button('<i class="fa fa-trash" title="Delete file"></i>',['class'=>'btn btn-danger btn-sm','data-id'=>$item->id,'onclick'=>'delete_img($(this),"'.addslashes("\App\UserProfileDoc").'","filename")']).'
                        </div>
                    </div>
                </div>';
            endforeach;
        endif;

    	return '<div class="row">'.$output.'</div>';
    }
}