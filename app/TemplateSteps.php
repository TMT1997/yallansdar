<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TemplateSteps extends Model
{
    protected $table = 'template_steps';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public static $steps = [
    	'email_verified'=>'تفعيل البريد الإلكتروني',
    	'personal_profile'=>'الحساب الشخصي',
    	'company_profile'=>'بيانات الشركة',
    	'add_products'=>'اضافة منتجات',
    	'select_template'=>'اختيار تصميم المتجر',
    ];

    /* Initialize an empty steps for user */
    public static function init($user)
    {
    	if(!$user) {
    		$user = Auth::user();
    	}
    	$steps = [
    		'user_id'=>$user->id,
    		'email_verified'=>$user->email_verified,
    		'personal_profile'=>0,
    		'company_profile'=>0,
    		'add_products'=>0,
    		'select_template'=>0,
            'completed'=>0
		];
    	$user_steps = self::create($steps);
		return $user_steps;
    }

    public static function is_completed($user_steps)
    {
    	$completed = true;
    	foreach(self::$steps as $step=>$label)
    	{
    		if($user_steps->$step==0) {
    			return false; break;
    		}
    	}
    	$user_steps->completed = 1;
    	if($user_steps->save()) {
    		return true;
    	}
    	return false;
    }

    public static function update_step($step, $user=null) {
        if(!$user) {
            $user = Auth::user();
        }
        $template_steps = self::whereUser_id($user->id)->first();
        if(!$template_steps) {
            $template_steps = self::init($user);
        }
        if(is_array($step)) {
            foreach($step as $sgl) {
                $template_steps->$sgl = 1;        
            }
        } else {
            $template_steps->$step = 1;            
        }
        if($template_steps->save()){
            return true;
        }
        return false;
    }
}
